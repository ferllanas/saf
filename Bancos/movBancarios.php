<?php
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Alta de Movimientos Bancarios</title>
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript/bancos_funciones.js"></script>
<script type="text/javascript" src="../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script  type="text/javascript">
    $(document).ready(function() {
        $("#Concepto").limit({
            limit: 50,
            id_result: "counter",
            alertClass: "alert"
            });
         });

(function($){
    $.fn.limit  = function(options) {
        var defaults = {
        limit: 200,
        id_result: false,
        alertClass: false
        }
        var options = $.extend(defaults,  options);
        return this.each(function() {
            var characters = options.limit;
            if(options.id_result != false)
            {
                $("#"+options.id_result).append("Usted tiene <strong>"+  characters+"</strong> Caracteres restantes.");
            }
            $(this).keyup(function(){
                if($(this).val().length > characters){
                    $(this).val($(this).val().substr(0, characters));
                }
                if(options.id_result != false)
                {
                    var remaining =  characters - $(this).val().length;
                    $("#"+options.id_result).html("Usted tiene <strong>"+  remaining+"</strong> Caracteres restantes.");
                    if(remaining <= 10)
                    {
                        $("#"+options.id_result).addClass(options.alertClass);
                    }
                    else
                    {
                        $("#"+options.id_result).removeClass(options.alertClass);
                    }
                }
            });
        });
    };
})(jQuery);
</script>
<link type="text/css" href="../css/tablesscrollbody450.css" rel="stylesheet">
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <table width="101%" border="0">
  	<tr>
    	<td class="TituloDForma">Alta de Movimientos Bancarios
   	      <hr class="hrTitForma"></td>
    </tr>
    <tr>
      <td class="texto8"><table width="100%" border="0">
        <tr>
          <td class="texto8"><table width="100%" border="0">
            <tr>
              <td class="texto8" width="6%"><label>Fecha:</label></td class="texto8">
              <td  width="10%"><input tabindex="1" class="texto8" name="fecmov" type="text" id="fecmov" maxlength="10" style="width:70px" />
                <img src="../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer" title=""%y/%m/%d"" align="absmiddle" />
                <!--{literal}-->
                <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecmov",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script></td>
              <td colspan="2" class="texto8" width="30%"><div align="left" style="z-index:1; position:relative; width:600px">tipo de Movimiento:
                <input tabindex="6"  class="texto8" name="tmov" type="text" id="tmov" size="50" style="width:300px;"  onkeyup="searchtmov();" autocomplete="off"/>
                <input type="hidden" class="texto8" name="ntmov" id="ntmov" size="50" style="width:100px;" readonly="readonly" />
                <div id="search_suggesttmov" style="z-index:2; position:absolute;" > </div>
              </div></td>
            </tr>            
            <tr>
              <td class="texto8" colspan="3"><div align="left" style="z-index:0; position:relative; width:600px">Banco:
                  <input tabindex="6"  class="texto8" name="nombanco" type="text" id="nombanco" size="50" style="width:300px;"  onkeyup="searchSuggest();" autocomplete="off"/>
                <input type="text" class="texto8" name="numbanco" id="numbanco" size="50" style="width:100px;" readonly="readonly" />
                <div id="search_suggest" style="z-index:2; position:absolute;" > </div>
              </div></td>
              <td class="texto8" colspan="2"><label>Monto: $</label>
                <input tabindex="7"  class="texto8" type="text" name="monto" id="monto" style="width:120px" onkeypress="return aceptarSoloNumeros(this,event);" onkeydown="asignaFormatoSiesNumerico(this, event)" /></td>
            </tr>          
            <tr>
              <td class="texto8"><label>Concepto:</label></td>
              <td class="texto8" colspan="4"><textarea tabindex="11" name="Concepto" id="Concepto" style="width:100%" ></textarea><div  id="counter"></div></td >
              <td class="texto8" width="49%" valign="bottom" align="center"><input tabindex="12" class="texto8" type="button" value="agregar" onclick="agregarMovimiento()" /></td>
            </tr>
          </table></td class="texto8">
        </tr>
        <tr>
          <td class="texto8" width="100%">
          <table class="tableone" id="sortable" border="0" style="width:800px">
          <thead>
            <tr>
              <th style="width:60px">Banco</th>
              <th style="width:120px">Descripcion</th>
              <th style="width:250px">Concepto</th>
              <th style="width:60px">Monto</th>              
              <th style="width:40px">&nbsp;</th>
            </tr>
          </thead>
           <tbody >
                    <tr>
                        <td colspan="6">
                            <div class="innerb" style="width:800px;height:20em;">
                                <table class="tabletwo"  style="width:800px;">
                                    <tbody  name="tfacturas" id="tfacturas" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr>	
    	<td class="texto8" valign="middle" align="center"><strong><label>Monto Total: $</label><input name="totmonto" id="totmonto"  style="width:100px; text-align:right" readonly="readonly" value='0'/></strong></td>
    </tr>
    <tr>
    	<td align="center">
        	<input type="button" onclick="guardarMovimientos();"  value="Guardar Movimientos" class="texto8"/>
        </td>
    </tr>
  </table>
</form>
</body>
</html>