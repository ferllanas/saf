<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();

$nom1='';
$nom2='';

$banco=$_REQUEST['query'];
$fecini=$_REQUEST['fecini'];
$fecfin=$_REQUEST['fecfin'];
//echo $fecini."---".$fecfin;
$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);

//$tsql_callSP ="{call sp_contab_poliza_diario_paso1(?,?,?)}";//Arma el procedimeinto almacenado
$tsql_callSP ="{call sp_bancos_c_movspdesaplicar(?,?,?)}";//Arma el procedimeinto almacenado
$params = array(&$banco,&$fini,&$ffin);//Arma parametros de entrada
$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
//
//echo $tsql_callSP;
//print_r($params);
//print_r($stmt);
$i=0;
while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
	// Comienza a realizar el arreglo, trim elimina espacios en blanco	
	//$datos[$i]['id']=trim($row['id']);
	$datos[$i]['id']=$row['id'];
	$datos[$i]['banco']=trim($row['banco']);
	$datos[$i]['tipomov']=trim($row['tipomov']);					
	$datos[$i]['descrip']=trim($row['descrip']);
	$datos[$i]['concepto']=utf8_decode($row['concepto']);
	$datos[$i]['fecha']=$row['fecha'];
	$datos[$i]['entsal']=$row['entsal'];
	$datos[$i]['monto']=trim($row['monto']);
	$datos[$i]['montof']=number_format($row['monto'],2);
	
	$i++;
}
//print_r($row);
echo json_encode($datos);   // Los codifica con el jason
?>