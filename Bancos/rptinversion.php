<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../cheques/css/style.css">         
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF-Reporte de Inversi&oacute;n</title>
        <script src="../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../cheques/javascript/divhide.js"></script>
        <script src="../cheques/javascript/jquery.tmpl.js"></script>
        <script src="javascript/polizas_funciones.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
        <script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>           		
			$(document).ready(function() {
				$(".botonExcel").click(function(event) {
					$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
					$("#FormularioExportacion").submit();
				});
			});
			function busca_recursos()
			{
				var fecha= document.getElementById('fecini').value;
				if(fecha=="")
				{
					alert("No se capturo la Fecha, Favor de revisarla");
					return false
				}
				var data = 'query='+fecha
				$.post('rptinversion_ajax.php',data, function(resp)
				{ 
					$('#proveedor').empty();
					$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 						
				}, 'json');  // Json es una muy buena opcion 
            }
			function calcular()
			{
				var numpoliza=0;				
				var saldos = document.getElementsByName('saldo');
				var tasas = document.getElementsByName('tasa');
				var dias = document.getElementsByName('dia');
				var sms = document.getElementsByName('sm');	
				var invers = document.getElementsByName('inver');
				var bancos = document.getElementsByName('banco');	
				for(i=0;i<saldos.length;i++)
				{	
						
						//alert(saldos[i].value+"-"+sms[i].value);
						var tot=parseFloat(saldos[i].value)-parseFloat(sms[i].value);
						if(tot<0)
						{	
							alert("El Banco "+bancos[i].value+" No cuenta con fondos para Invertir")
							tot=0;
						}
						invers[i].value=tot;
						numpoliza++;
				}
				document.getElementById('invertir').disabled=false;
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>  
					<td>${banco}</td>										
					<td>${nombanco}</td>
					<td>${cuenta_ban}</td>
					<td style="text-align:right">${disp_ant}</td>
					<td style="text-align:right">${c1}</td>	
					<td style="text-align:right">${c2}</td>	
					<td style="text-align:right">${c3}</td>	
					<td style="text-align:right">${c4}</td>	
					<td style="text-align:right">${c5}</td>	
					<td style="text-align:right">${c6}</td>	
					<td style="text-align:right">${c7}</td>	
					<td style="text-align:right">${c8}</td>	
					<td style="text-align:right">${saldo}</td>					
					<td style="text-align:right">${inversion}</td>
					<td style="text-align:right">${disponible}</td>
            </tr>
        </script>   
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body >
    <span class="TituloDForma">Reporte de Invesi&oacute;n de Recursos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>


<table>
<tr>
<td>

<div align="left" id="rangofecha" style="z-index:1; position:relative;  width:367px; top: 0px; left: 0px; height: 26px;">
  Dia:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_recursos()">
</div>

<input type="hidden" name="ren" id="ren">
    <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
<tr>
	<td>
		<table  id="Exportar_a_Excel">
        	<thead>         		   
              	<th><p>C.C.</p></th>                
                <th><p>Banco</p></th>
                 <th><p>Num. Cta.</p></th> 
                 <th><p>S.I.</p></th>					                                     
                <th><p>+ Entradas Por Transferencias</p></th> 
        		<th><p>+ Cobranza Origen de Cta</p></th> 
        		<th><p>+ Capital Mesa cierre dia anterior</p></th>
                <th><p>+ Productos financieros Netos</p></th>  
                <th><p>+ Cheques Cancelados D. A.</p></th>
                <th><p>+/- Otros</p></th>     
                <th><p>- Cheques Expedidos (Totales-Cancelados M.D.)</p></th>
                <th><p>- Salidas por Transferencia</p></th>
                <th><p>S.A.I</p></th>                
                <th><p>Inversion Mesa</p></th>
                <th><p>- Saldo sin Invertir</p></th>
                              
             </thead>
            <tbody id="proveedor" name='proveedor'>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Encontrar Resultados</td>
                </tr>
            </tbody>
          </table>    
      </td>
</tr>
</table>
</table>
    </div>
    </body>
</html>
