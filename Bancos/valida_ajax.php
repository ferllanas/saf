<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();

//Cambios -9 08/07/2014 por ajuste
//Cambios -10 09/07/2014 por ajuste
$command= "select id from bancosdinversion where estatus=10 and CONVERT(VARCHAR(11),fecha,103)=CONVERT(VARCHAR(11),DATEADD(day,-1,getdate()),103)";					
$getProducts = sqlsrv_query($conexion,$command);
//echo $command;
$i=0;
while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
{	
	$datos[$i]['id']=$row['id'];	
	$i++;
}
//print_r($row);
echo json_encode($datos);   // Los codifica con el jason
?>