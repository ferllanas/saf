<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$tipo="";
if(isset($_REQUEST['tipo']))
	$tipo= $_REQUEST['tipo'];
	
$command= "select * FROM contabilidad ON tipo=";

	///echo $command;
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$datos[$i]['folio']=trim($row['folio']);
		$datos[$i]['falta']=trim($row['falta']);					
		$datos[$i]['nomprov']=trim($row['nomprov']);
		$datos[$i]['importe']=trim($row['total']);
		$datos[$i]['concepto']=utf8_decode($row['descrip']);
		$datos[$i]['estatus']=trim($row['estatus']);
		$i++;
	}

echo json_encode($datos);   // Los codifica con el jason
?>