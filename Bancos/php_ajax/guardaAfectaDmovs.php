﻿<?php
	require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");
	$fecha_poliza="";
	$tipo_poliza="";
	$nombreusuario="";
	$tfacturas="";
	
	$fecha="";
	$conceptos="";
	//$concepto="";
	$numero_poliza="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$partidas=array();//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$descripcion="";
	$id_facts="";
	$fecpoliza="";
	if(isset($_POST['polizas']))
			$partidas=$_POST['polizas'];
			
	if(isset($_POST['banco']))
			$banco=$_POST['banco'];
		else
			$banco=0;
	$fechapolizanormal=date("Ymd"); 
	if(isset($_POST['tipo']))
		$tipo_poliza=$_POST['tipo'];
	
	if(isset($usuario))
	{
		if ( is_array( $partidas ) ) // Pregunta se es una array la variable facturas
		{		
			for( $i=0 ; $i  < count($partidas) ; $i++ )
			{
				$fails = func_bancos_A_dmovs($partidas[$i]['id'], $usuario,$banco,$partidas[$i]['monto'],$partidas[$i]['entsal'],$partidas[$i]['tipomov']);										
			}
		}
		else
		{
			$fails=true; $msgerror=" No es un array la variable";
		}
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	

	if($fecha_poliza=="true" )
		$datos[0]['id']=trim($id);// regresa el id de la
	else
		$datos[0]['id']=$id_facts;
		
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array

//Funcion para registrar el p´roductoa  la requisicion
function func_bancos_A_dmovs($id,$usuario,$banco,$monto,$entsal,$tipomov)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	$params = $tsql_callSP ="{call sp_bancos_A_daplica(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$usuario,&$banco,&$monto,&$entsal,&$tipomov);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_bancos_A_daplica". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}


?>