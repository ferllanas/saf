﻿<?php
	require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$usuario = "001981";//$_COOKIE['ID_my_site'];//	
	
	$fechapolizanormal=date("Ymd"); 
	$command="select a.id,a.banco,a.tipomov,a.monto,b.entsal,a.faplica from  bancosdmovs a 
	left join bancosmtipomov b on a.tipomov=b.id where a.estatus=10 and CONVERT(VARCHAR(11),a.faplica,103)='02/01/2013' order by a.faplica";
	$fecha='2013/01/02';
	$stmt = sqlsrv_query($conexion, $command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco	
		//$datos[$i]['id']=trim($row['id']);
		$datos[$i]['id']=$row['id'];
		$datos[$i]['banco']=trim($row['banco']);
		$datos[$i]['tipomov']=trim($row['tipomov']);					
		$datos[$i]['entsal']=$row['entsal'];
		$datos[$i]['monto']=trim($row['monto']);
		$fails = func_bancos_A_dmovs($row['id'],$usuario,$row['banco'],$row['monto'],$row['entsal'],$row['tipomov'],$fecha);	
		$i++;	
	}	
	

	$datos[0]['id']=1;		
	$datos[0]['cont']=$i;
	
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array

//Funcion para registrar el p´roductoa  la requisicion
function func_bancos_A_dmovs($id,$usuario,$banco,$monto,$entsal,$tipomov,$fecha)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	//$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	$params = $tsql_callSP ="{call sp_bancos_A_daplica_pruebas(?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado	
	$params = array(&$id,&$usuario,&$banco,&$monto,&$entsal,&$tipomov,$fecha);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_bancos_A_daplica". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}


?>