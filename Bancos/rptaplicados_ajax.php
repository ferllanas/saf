<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini=$_REQUEST['query'];
$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
//$fecini = "";
$datos=array();

$nom1='';
$nom2='';

$tsql_callSP ="select a.id,a.concepto,a.banco,c.nombanco, a.tipomov,a.monto,b.descrip, b.entsal,a.faplica from  bancosdmovs a 
	left join bancosmtipomov b on a.tipomov=b.id 
	left join egresosmbancos c on a.banco=c.banco 
	where a.estatus=10 and CONVERT(VARCHAR(11),a.faplica,103)='$fecini' order by a.banco,a.tipomov";//Arma el procedimeinto almacenado
$params = array(&$fini);
$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
$stmt = sqlsrv_query($conexion,$tsql_callSP);
//echo $tsql_callSP;
//print_r($params);

$i=0;
while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
	//echo "pasa";
	// Comienza a realizar el arreglo, trim elimina espacios en blanco	
	$datos[$i]['banco']=$row['banco'];
	$datos[$i]['nombanco']=$row['nombanco'];
	$datos[$i]['id']=$row['id'];	
	$datos[$i]['concepto']=$row['concepto'];
	$datos[$i]['monto']=number_format($row['monto'],2);
	$datos[$i]['tipomov']=$row['tipomov'];					
	$datos[$i]['descrip']=$row['descrip'];
	$datos[$i]['entsal']=$row['entsal'];					
	$datos[$i]['faplica']=$row['faplica'];	
	$i++;
}
//print_r($datos);
echo json_encode($datos);   // Los codifica con el jason
?>