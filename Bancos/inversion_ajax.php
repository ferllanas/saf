<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();

$nom1='';
$nom2='';

$tsql_callSP ="select id,banco,nombanco,cuenta_ban,saldo,saldo_parcial,tasa,sdo_minimo,dias_inv from egresosmbancos where estatus<9000 order by banco";//Arma el procedimeinto almacenado
$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
$stmt = sqlsrv_query($conexion, $tsql_callSP);

$i=0;
while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
	// Comienza a realizar el arreglo, trim elimina espacios en blanco	
	$datos[$i]['banco']=$row['banco'];
	$datos[$i]['nombanco']=$row['nombanco'];
	$datos[$i]['cuenta_ban']=$row['cuenta_ban'];	
	$datos[$i]['saldo']=$row['saldo'];					
	$datos[$i]['saldo_parcial']=$row['saldo_parcial'];
	$datos[$i]['saldo_parcialf']=number_format($row['saldo_parcial'],2);	
	$datos[$i]['tasa']=$row['tasa'];
	$datos[$i]['sdo_minimo']=$row['sdo_minimo'];
	$datos[$i]['dia_inv']=$row['dias_inv'];	
	$i++;
}
//print_r($row);
echo json_encode($datos);   // Los codifica con el jason
?>