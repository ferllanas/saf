
function valida_tipoPoliza()
{
	var Credito=document.getElementById('Credito').value;
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					tipo		: 		numcuenta
				};
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/valida_tipopoliza.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								document.getElementById('nomsubcuenta').value=myArray[0].nombre;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}


function getNomCuenta()
{
	//alert("Me lleva");
	var numcuenta=document.getElementById('numcuenta').value;
	var numsubcuenta=document.getElementById('numsubcuenta').value;
	
	if(numcuenta.length<=0)
	{
		alert("Debe teclear numero de cuenta");
		//document.getElementById('numcuenta').focus();
		return( false);
		
	}
	
	if(numsubcuenta.length<=0)
	{
		alert("Debe teclear numero de sub-cuenta");
		//document.getElementById('numsubcuenta').focus();
		return( false);
		
	}
	
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					cuenta		: 		numcuenta,
					subcuenta   : numsubcuenta
    			};
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/getNomCuenta.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								document.getElementById('nomsubcuenta').value=myArray[0].nombre;
								document.getElementById('numcuentacompleta').value=myArray[0].cuenta;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}




var partidasPoliza=new Array(); 
var countpartidasPoliza =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

function objfacturaDProv(Concepto, Credito ,	numcuentacompleta ,  nomsubcuenta , Cargo , Concepto,Referencia){  
	this.Concepto = Concepto;
	this.Credito = Credito;
	this.numcuentacompleta = numcuentacompleta;
    this.nomsubcuenta = nomsubcuenta;
	this.Cargo = Cargo;
	this.Referencia = Referencia;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }

function verificarExistePartidaEnFacturas(numDoc)
{
	var retval=false;
	for(i=0;i<partidasPoliza.length &&  retval==false;i++)
	{
		for(j=0;j<partidasPoliza[i]['partidas'].length &&  retval==false;j++)
			if(partidasPoliza[i]['partidas'][j]['numDoc']==numDoc)
			{	retval=true; }
	}
	return retval;
}
//funcion que busca un numero de factura
function getIndexFactura(numcuentacompleta)
{
	var retval=false;
	var index=-1;
	for(i=0;i<partidasPoliza.length && retval==false;i++)
	{
		//alert(partidasPoliza[i]['numcuentacompleta']+"=="+numcuentacompleta);
		if(numcuentacompleta==partidasPoliza[i]['numcuentacompleta'])
		{	retval=true;
			index=i;
		}
	}	
	return index;
}

function verificaSiExisteFactura(numfact)
{
	var retval=false;
	
	for(i=0;i<partidasPoliza.length && retval==false;i++)
	{
		if(numfact==partidasPoliza[i]['numcuentacompleta'])
			retval=true;
	}	
	return retval;
}

function agregarCargoCredito()
{
	var numcuentacompleta= document.getElementById('numcuentacompleta').value;
	var nomsubcuenta= document.getElementById('nomsubcuenta').value;
	var Cargo= document.getElementById('Cargo').value;
	var Concepto =document.getElementById('Concepto').value;
	var Credito= document.getElementById('Credito').value;
	var Referencia= document.getElementById('Referencia').value;
	var Origen= $('#origen').val();
	var tCargo =document.getElementById('totcargo').value;
	var tCredito =document.getElementById('totcredito').value;
	
	if(numcuentacompleta.length <= 0 )
	{
		alert("Favor de ingresar numero de cuenta.");
		return(false);
	}	
	
	if(nomsubcuenta.length <= 0)
	{
		alert("Favor de ingresar nombre de cuenta.");
		return(false);
	}	
	if(Concepto.length <= 0)
	{
		Concepto=nomsubcuenta;
	}	
	
	if(Origen.length > 0)
	{
		if(Referencia.length <= 0)
		{
			alert("Favor de ingresar la Referencia.");
			return(false);
		}	
	}	
	//Valida 
	if(Cargo.length <= 0)
	{
		alert("Favor de ingresar cargo.");
		return(false);
	}	
	if(tCargo=='')
	{
		tCargo='0';
	}
	//var a=tCargo.replace(/,/g,"");
	//var b=Cargo.replace(/,/g,"");
	//totcargo = parseInt(a) + parseInt(b);
	//document.getElementById('totcargo').value=totcargo;
	
	if(Credito.length <= 0 )
	{
		alert("Favor de ingresar Credito");
		return(false);
	}	
	if(tCredito=='')
	{
		tCredito='0';
	}
	//var c=tCredito.replace(/,/g,"");
	//var d=Credito.replace(/,/g,"");
	//totcredito = parseInt(c) + parseInt(d);
	//document.getElementById('totcredito').value=totcredito;
	partidasPoliza[countpartidasPoliza]= new objfacturaDProv(Concepto,Credito ,numcuentacompleta ,  nomsubcuenta , Cargo, 	Concepto,Referencia,Origen);
	countpartidasPoliza++;
	agregarFacturaATablaFacturas(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo,Origen);
	actualiza_totales();
	//sumacargos(Cargo,tCargo);
	//document.getElementById('numcuenta').value="";
	//document.getElementById('numsubcuenta').value="";
	document.getElementById('numcuentacompleta').value="";
	document.getElementById('nomsubcuenta').value="";
	document.getElementById('Cargo').value="";
	document.getElementById('Concepto').value="";
	document.getElementById('Credito').value="";
	document.getElementById('Referencia').value="";
	document.getElementById('origen').selectedIndex=0;
}
function actualiza_mtotales()
{
	
	//
	document.getElementById('totcargo').value = 0;
	document.getElementById('totcredito').value = 0;
	
	var tabla= document.getElementById('tfacturas');
	var numren=tabla.rows.length;
	//alert(tabla.rows.length);
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
			var mynode = cell.childNodes[k];
			//
			if(mynode.name=="vCargo")
			{
				//
				cargo2=mynode.value;
				if(cargo2 =='')
				{
					cargo2=0;
					alert(cargo2);
				}
				document.getElementById('totcargo').value = parseFloat(document.getElementById('totcargo').value.replace(/,/g,'')) + parseFloat( cargo2.replace(/,/g,''));
				document.getElementById('totcargo').value=Math.round(document.getElementById('totcargo').value*100)/100;
				document.getElementById('totcargo').value=NumberFormat(document.getElementById('totcargo').value, '2', '.', ',');
			}
			if(mynode.name=="vCredito")
			{
				credito2=mynode.value;				
				document.getElementById('totcredito').value = parseFloat(document.getElementById('totcredito').value.replace(/,/g,'')) + parseFloat( credito2.replace(/,/g,''));
				document.getElementById('totcredito').value=Math.round(document.getElementById('totcredito').value*100)/100;
				document.getElementById('totcredito').value=NumberFormat(document.getElementById('totcredito').value, '2', '.', ',');				
				
			}
		}
	}
}
}

function actualiza_totales()
{
	
	//
	document.getElementById('totcargo').value = 0;
	document.getElementById('totcredito').value = 0;
	
	var tabla= document.getElementById('tfacturas');
	var numren=tabla.rows.length;
	
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
			var mynode = cell.childNodes[k];
			//
			if(mynode.name=="vCargo")
			{
				//alert(mynode.name);
				cargo2=mynode.value;
				document.getElementById('totcargo').value = parseFloat(document.getElementById('totcargo').value.replace(/,/g,'')) + parseFloat( cargo2.replace(/,/g,''));
				document.getElementById('totcargo').value=Math.round(document.getElementById('totcargo').value*100)/100;
				document.getElementById('totcargo').value=NumberFormat(document.getElementById('totcargo').value, '2', '.', ',');
			}
			if(mynode.name=="vCredito")
			{
				credito2=mynode.value;
				document.getElementById('totcredito').value = parseFloat(document.getElementById('totcredito').value.replace(/,/g,'')) + parseFloat( credito2.replace(/,/g,''));
				document.getElementById('totcredito').value=Math.round(document.getElementById('totcredito').value*100)/100;
				document.getElementById('totcredito').value=NumberFormat(document.getElementById('totcredito').value, '2', '.', ',');				
				
			}
		}
	}
}
}



function cargarpoliza()
{
	var totcargo=0.00;
	var	totcredito=0.00;
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}
	var documento= document.getElementById('pathtexto').value;
	//alert(documento);
	var datas = { ppath: documento	};
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'php_ajax/cargapoliza.php',
						dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(request);
						 alert ( " Can't do because: " + error );
						},
						success: function(resp)
						{
							//alert(resp);
							var myObj = resp;
							for(i=0;i<resp.length;i++)
							{								
								if(resp[i].nomcuenta == null)
								{
									resp[i].nomcuenta='****** Error en Cuenta ******';									
									error=1;
								}
								if(resp[i].nomcuenta.match(/Error.*/) )
								{	
									error=1;
								}
								agregarpolizaMTablaFacturas(resp[i].cuenta,resp[i].nomcuenta,resp[i].cargo,resp[i].credito,resp[i].referencia,resp[i].concepto,resp[i].origen);
								partidasPoliza[countpartidasPoliza]= new objpolizaDctas(resp[i].cuenta,resp[i].cargo,resp[i].credito,resp[i].referencia,resp[i].concepto,resp[i].origen);
								
								countpartidasPoliza++;
								//totcargo=totcargo + parseFloat(resp[i].cargo);
								//totcredito=totcredito + parseFloat(resp[i].credito);
							}
							
        		            ///NOT how to print the result and decode in html or php///
							//document.getElementById('tcargo').value=totcargo;
							//document.getElementById('tcredito').value=totcredito;
							//document.getElementById('asientos').value=countpartidasPoliza;
		                    console.log(myObj);
							//
							if(resp.error==1)
							{
								alert("No se Pudo Cragar el Archivo");								
							}
							if(error==1)
							{
								alert("Se encontaron errores en algunas cuentas favor de verificarlas");
								//document.getElementById('gralguarda').disabled=true;
								//actualiza_mtotales();
							}
							else
							{
								//actualiza_mtotales();
								document.getElementById('gralguarda').disabled=false;
							}
						}
					
					});
	
}
function agregarpolizaMTablaFacturas(cuenta,nomcuenta,cargo,credito,referencia,descripcion,origen)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = cuenta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell1.className ="td1";
	newcell1.style.width = "70px";

	var newcell2 = newrow.insertCell(1);
	newcell2.innerHTML = nomcuenta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell2.className ="td8";
	newcell2.style.width = "150px";
	
	var newcell3 = newrow.insertCell(2);
	newcell3.innerHTML  ="<input type='hidden' id='vCargo' name='vCargo' readonly  value='"+cargo+"' >"+cargo;
	//"+addCommas(nomsubcuenta);
	newcell3.className ="td3";
	newcell3.style.width = "70px";

	var newcell4 = newrow.insertCell(3);
	newcell4.innerHTML ="<input type='hidden' id='vCredito' name='vCredito' readonly  value='"+credito+"' >"+credito;
	newcell4.className ="td4";
	newcell4.style.width = "70px";

	var newcell5 = newrow.insertCell(4);
	newcell5.innerHTML =referencia;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell5.className ="td5";
	newcell5.style.width = "70px";
	
	var newcell6 = newrow.insertCell(5);
	newcell6.innerHTML =descripcion;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell6.className ="td6";
	newcell6.style.width = "150px";
	
	var newcell7 = newrow.insertCell(6);
	newcell7.innerHTML =origen;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell7.className ="td6";
	newcell7.style.width = "50px";
	
	
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}
function agregarcuentasATablapolizas(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = numcuentacompleta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell1.className ="td1";
	newcell1.style.width = "100px";

	var newcell5 = newrow.insertCell(1);
	newcell5.innerHTML  =nomsubcuenta;//"<input type='hidden' id='vnomsubcuenta' name='vnomsubcuenta' readonly value='"++"'>"+addCommas(nomsubcuenta);
	newcell5.className ="td2";
	newcell5.style.width = "200px";

	var newcell6 = newrow.insertCell(2);
	newcell6.innerHTML = Cargo;//"<input type='hidden' id='vCargo' name='vCargo' readonly  value='"++"' >"+Cargo;
	newcell6.className ="td3";
	newcell6.style.width = "100px";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML =Credito;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell3.className ="td4";
	newcell3.style.width = "100px";

	var newcell4 = newrow.insertCell(4);
	newcell4.innerHTML =Concepto;
	newcell4.className ="td5";
	newcell4.style.width = "200px";
	
	var newcell6 = newrow.insertCell(5);
	var functiona="borrarFactura('"+numcuentacompleta+"',"+contRow+" )";
	newcell6.innerHTML ='<input type="button" id="borrarfact" name="borrarfact" value="-" onClick="'+functiona+'" >';
	newcell6.style.width = "50px";
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}

function agregarFacturaATablaFacturas(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo,Origen)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = numcuentacompleta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	//newcell1.className ="td1";
	newcell1.style.width = "100px";

	var newcell5 = newrow.insertCell(1);
	newcell5.innerHTML  =nomsubcuenta;//"<input type='hidden' id='vnomsubcuenta' name='vnomsubcuenta' readonly value='"++"'>"+addCommas(nomsubcuenta);
	//newcell5.className ="td2";
	newcell5.style.width = "250px";

	var newcell6 = newrow.insertCell(2);
	newcell6.innerHTML = "<input type='hidden' id='vCargo' name='vCargo' readonly  value='"+Cargo+"' >"+Cargo;
	//newcell6.className ="td3";
	newcell6.style.width = "100px";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML ="<input type='hidden' id='vCredito' name='vCredito' readonly  value='"+Credito+"' >"+Credito;
	//newcell3.className ="td4";
	newcell3.style.width = "100px";

	var newcell4 = newrow.insertCell(4);
	newcell4.innerHTML =Concepto;
	//newcell4.className ="td5";
	newcell4.style.width = "250px";
	
	//var newcell5 = newrow.insertCell(5);
	//newcell5.innerHTML =Origen;
	
	//newcell5.className ="td6";
	//newcell5.style.width = "100px";
	
	var newcell5 = newrow.insertCell(5);
	var functiona="borrarFactura('"+numcuentacompleta+"',"+contRow+" )";
	//newcell6.className ="td7";
	newcell5.innerHTML ='<input type="button" id="borrarfact" name="borrarfact" value="-" onClick="'+functiona+'" >';
	newcell5.style.width = "50px";
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}

function borrarFactura(numfac, contRow)
{
	var answer = confirm("¿Seguro deseas borrar esta partida?")
	if (answer)
	{
		var index = getIndexFactura(numfac);
		//alert(contRow+","+index);
		if( contRow ==index)
		{
			partidasPoliza.splice( index ,1 );
			countpartidasPoliza--;
			borrado=true;
			
			if(borrado)
			{
				refreshTablaFacturas();
				alert("Partida eliminada.");
			}
		}
	}
	actualiza_totales();
}

function refreshTablaFacturas()
{
	//partidasPoliza
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}

	for (var i = 0; i < partidasPoliza.length; i++) 
	{ 
		agregarFacturaATablaFacturas(partidasPoliza[i]['Concepto'] ,
									partidasPoliza[i]['Credito'],
									partidasPoliza[i]['numcuentacompleta'],
									partidasPoliza[i]['nomsubcuenta'],
									partidasPoliza[i]['Cargo']);
	}
	
}

function guardarPoliza()
{
	
	var tCargo =document.getElementById('totcargo').value;
	var tCredito =document.getElementById('totcredito').value;
	//Valida 
	var a=tCargo.replace(/,/g,"");	
	var c=tCredito.replace(/,/g,"");
	if(a != c)
	{
		alert("No coinciden los Cargos y Los Creditos");
		return(false);
	}	
		
	var fecpoliza= document.getElementById('fecpoliza').value;
	var tpoliza= document.getElementById('tpoliza').value;
	var numpoliza= document.getElementById('numpoliza').value;
	var descrip= document.getElementById('descrip').value;
	
	
	var datas = {
					partidas	: partidasPoliza,
					fecha_poliza   : fecpoliza,
					tipo_poliza : tpoliza,
					numero_poliza : numpoliza,
					descripcion : descrip
    			};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				dataType: "json",
				url:      'php_ajax/guardaPoliza.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						console.log(resp);
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								document.getElementById('nomsubcuenta').value=resp[0].nombre;
								document.getElementById('numcuentacompleta').value=resp[0].cuenta;
								window.location = resp[0].path;
							}
							else
							{
								alert( "Error: "+resp[0].msgerror);
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
					}
				}
			});
}

function guardarMPoliza()
{
	var fecpoliza= document.getElementById('fecpoliza').value;
	var tpoliza= document.getElementById('tpoliza').value;
	var numpoliza= document.getElementById('numpoliza').value;
	var descrip= document.getElementById('descrip').value;
	
	
	var datas = {
					partidas	: partidasPoliza,
					fecha_poliza   : fecpoliza,
					tipo_poliza : tpoliza,
					numero_poliza : numpoliza,
					descripcion : descrip
    			};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/guardaMPoliza.php',
				dataType: "json",
				data:     datas,
				error: function(request,error) 
				{
				 console.log(request);
				 alert ( " Can't do because: " + error );
				},
				success: function(resp) 
				{
					//alert(resp)
					console.log(resp);
					if( resp.length ) 
					{						
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								//document.getElementById('nomsubcuenta').value=resp[0].nombre;
								//document.getElementById('numcuentacompleta').value=resp[0].cuenta;
								window.location = resp[0].path;
							}
							else
							{
								alert( "Error: "+resp[0].msgerror);
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
					}
				}
			});
}


function getNomCuenta2(object, numVcuenta)
{
	//alert("Me lleva");
	//var numcuenta = object.value;
	var numsubcuenta = object.value;
	

	if(numsubcuenta.length<=0)
	{
		alert("Debe teclear numero de sub-cuenta");
		//document.getElementById('numsubcuenta').focus();
		return( false);
		
	}
	
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					subcuenta   : numsubcuenta
    			};//folio_ImpCheque : folio_ImpCheque,
	//alert('php_ajax/crear_chequespdf.php?'+prods);
	//location.href = 'php_ajax/crear_chequespdf.php?'+datas;
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/edicion_getNomCuenta.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								var dnomcuentas = document.getElementsByName('dnomcuenta[]');//  getElementById('dnomcuenta');
								
								//alert(dnomcuentas.length+","+numVcuenta);
								
								dnomcuentas[numVcuenta].value = myArray[0].nombre;
								//document.getElementById('dnomcuenta').value=myArray[0].cuenta;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}


function agregarCargoCredito_edicion()
{
	var numcuentacompleta= document.getElementById('numcuentacompleta').value;
	var nomsubcuenta= document.getElementById('nomsubcuenta').value;
	var Cargo= document.getElementById('Cargo').value;
	var Concepto =document.getElementById('Concepto').value;
	var Credito= document.getElementById('Credito').value;
	var Referencia= document.getElementById('Referencia').value;
	
	/*if(Referencia.length <= 0 )
	{
		alert("Favor de ingresar Referencia");
		return(false);
	}	*/

	if(Credito.length <= 0 )
	{
		alert("Favor de ingresar Credito");
		return(false);
	}	

	if(numcuentacompleta.length <= 0 )
	{
		alert("Favor de ingresar numero de cuenta.");
		return(false);
	}	
	
	if(nomsubcuenta.length <= 0)
	{
		alert("Favor de ingresar nombre de cuenta.");
		return(false);
	}	

	if(Cargo.length <= 0)
	{
		alert("Favor de ingresar cargo.");
		return(false);
	}	

/*	if(verificaSiExisteFactura(numcuentacompleta))
	{	
		alert("El número de factura corresponde a una agregada anteriormente.");
		return(false);
	}*/
	//alert(Referencia);Concepto, Credito ,	numcuentacompleta ,  nomsubcuenta , Cargo , Concepto,Referencia

	agregarFacturaATablaFacturas_edicion(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo, Referencia);

	document.getElementById('numcuenta').value="";
	document.getElementById('numsubcuenta').value="";
	document.getElementById('numcuentacompleta').value="";
	document.getElementById('nomsubcuenta').value="";
	document.getElementById('Cargo').value="";
	document.getElementById('Concepto').value="";
	document.getElementById('Credito').value="";
	document.getElementById('Referencia').value="";
}


function agregarFacturaATablaFacturas_edicion(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo, referencia)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = '<input type="hidden" name="idpartida[]" id="idpartida[]" value="0"/><input type="text" id="dcuenta[]" name="dcuenta[]" value="'+numcuentacompleta+'"   style="width:100%;" onKeyPress="return aceptarSoloNumeros(this,event);" onblur="getNomCuenta2(this, '+numcuentacompleta+');" class="texto8"/>';
	newcell1.className ="td1";
	newcell1.style.width = "100px";

	var newcell5 = newrow.insertCell(1);
	newcell5.innerHTML  ='<input type="text" id="dnomcuenta[]" name="dnomcuenta[]" value="'+nomsubcuenta+'"  style="width:100%;" class="texto8"/>';
	newcell5.className ="td2";
	newcell5.style.width = "200px";

	var newcell6 = newrow.insertCell(2);
	newcell6.innerHTML = '<input type="text" id="dcargo[]" name="dcargo[]" value="'+Cargo+'"  style="width:100%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"/>';//"<input type='hidden' id='vCargo' name='vCargo' readonly  value='"++"' >"+Cargo;
	newcell6.className ="td3";
	newcell6.style.width = "100px";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML ='<input type="text" id="dcredito[]" name="dcredito[]" value="'+Credito+'"  style="width:100%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"/>';//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell3.className ="td4";
	newcell3.style.width = "100px";

	var newcell4 = newrow.insertCell(4);
	newcell4.innerHTML ='<textarea style="width:100%;" id="dconcepto[]" name="dconcepto[]"  class="texto8" >'+Concepto+'</textarea>';
	newcell4.className ="td5";
	newcell4.style.width = "200px";
	
	var newcell9 = newrow.insertCell(5);
	newcell9.innerHTML ='<input  style="width:100%;" id="dreferencia[]" name="dreferencia[]" value="'+referencia+'"  class="texto8">';
	newcell9.className ="td5";
	newcell9.style.width = "100px";
	
	var newcell6 = newrow.insertCell(6);
	var functiona="borrardPoliza("+contRow+" )";
	newcell6.innerHTML ='<img src="../../imagenes/eliminar.jpg" onClick="'+functiona+'">';
	newcell6.style.width = "50px";
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}


function borrardPoliza(contRow)
{
	var answer = confirm("¿Seguro deseas borrar esta partida?")
	if (answer)
	{
		//partidasPoliza.splice( index ,1 );
		//countpartidasPoliza--;
		//borrado=true;
			var Table = document.getElementById('tfacturas');

			Table.deleteRow(contRow) ;
			//refreshTablaFacturas();
			alert("Partida eliminada.");

	}
}

function cancela_partidapoliza(idpartida, row)
{
	var pregunta = confirm("¿Esta seguro que desea eliminar la partida? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					id		: 		idpartida
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/cancela_partidapoliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									var Table = document.getElementById('tfacturas');
									Table.deleteRow(row) ;								
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}


var partidasPoliza=new Array(); 
var countpartidasPoliza =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

function objfacturaDProv(Concepto, Credito ,	numcuentacompleta ,  nomsubcuenta , Cargo , Concepto,Referencia,Origen){  
	this.Concepto = Concepto;
	this.Credito = Credito;
	this.numcuentacompleta = numcuentacompleta;
    this.nomsubcuenta = nomsubcuenta;
	this.Cargo = Cargo;
	this.Referencia = Referencia;
	this.Origen = Origen;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }
 function objpolizaDctas(cuenta, cargo ,credito , referencia,concepto,origen )
 {  
	this.numcuentacompleta = cuenta;
	this.Cargo = cargo;
	this.Credito = credito;
    this.Referencia = referencia;
	this.Concepto = concepto;
	this.Origen = origen;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }
function afectar_polizas()
{
	var polizasAAfectar=new Array();
	var numpoliza=0;
	var polizas = document.getElementsByName("afectacion");
	var tipops = document.getElementsByName("tipop");
	
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizasAAfectar[numpoliza]=polizas[i].value;
			numpoliza++;
			//alert(polizas[i].value);
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea afectar a la(s) polizas seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					polizas		: 		polizasAAfectar
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/afecta_poliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									 busca_cheque();						
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}

function desafectar_polizas()
{
	var polizasAAfectar=new Array();
	var numpoliza=0;
	var polizas=document.getElementsByName("afectacion");
	//alert("AAAA");
	
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizasAAfectar[numpoliza]=polizas[i].value;
			numpoliza++;
			//alert(polizas[i].value);
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea desafectar a la(s) polizas seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		//alert("BBB");
		var datas = {
					polizas		: 		polizasAAfectar
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/desafecta_poliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									//alert("WS");
									 busca_cheque();
									//alert("ZA");
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}

// busuqeda incrementl *****
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
	//alert("error");
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('nomsubcuenta').value);
		searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
       if(searchReq.responseText.length<1);
		{
			document.getElementById('numcuentacompleta').value = '';	
			//document.getElementById('nomsubcuenta').value = '';
		}
	   ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+'; '+tok[0]+' ">' + tok[1] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    //document.getElementById('nomsubcuenta').value = idprod;
	var tok =idprod.split(";");	
	document.getElementById('numcuentacompleta').value = tok[0];	
	document.getElementById('nomsubcuenta').value = tok[1];
	//$('#Concepto').text(tok[1]);
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
	document.getElementById('Cargo').focus();
}

function addslashes (str)
{
   
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  

}
function aceptarSoloNumeros(evt)
{
//asignamos el valor de la tecla a keynum
	if(window.event)
	{// IE
		keynum = evt.keyCode;
	}
	else
	{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if(keynum>47 && keynum<58)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function gen_polizapasivo()
{
	
	var fecini = document.getElementById('fecini').value;
	var fecfin = document.getElementById('fecfin').value;
	var polizaspasivo=new Array();
	var numpoliza=document.getElementById('npoliza').value;	
	var polizas = document.getElementsByName('afectacion');
	var tipo='D';
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizaspasivo[numpoliza]=polizas[i].value;
			numpoliza++;			
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea generar la poliza D-"+numpoliza +" con los asientos seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					fecini:fecini,
					fecfin: fecfin,
					tipo: tipo,
					numpoliza: numpoliza,
					polizas	: polizaspasivo
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/guardaPolizaPasivo.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									 busca_cheque();						
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}
