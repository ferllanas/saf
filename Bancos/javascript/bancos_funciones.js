
function valida_tipoPoliza()
{
	var Credito=document.getElementById('Credito').value;
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					tipo		: 		numcuenta
				};
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/valida_tipopoliza.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								document.getElementById('nomsubcuenta').value=myArray[0].nombre;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}


function getNomCuenta()
{
	//alert("Me lleva");
	var numcuenta=document.getElementById('numcuenta').value;
	var numsubcuenta=document.getElementById('numsubcuenta').value;
	
	if(numcuenta.length<=0)
	{
		alert("Debe teclear numero de cuenta");
		//document.getElementById('numcuenta').focus();
		return( false);
		
	}
	
	if(numsubcuenta.length<=0)
	{
		alert("Debe teclear numero de sub-cuenta");
		//document.getElementById('numsubcuenta').focus();
		return( false);
		
	}
	
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					cuenta		: 		numcuenta,
					subcuenta   : numsubcuenta
    			};
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/getNomCuenta.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								document.getElementById('nomsubcuenta').value=myArray[0].nombre;
								document.getElementById('numcuentacompleta').value=myArray[0].cuenta;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}




var partidasPoliza=new Array(); 
var countpartidasPoliza =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

function objfacturaDProv(Concepto, Credito ,	numcuentacompleta ,  nomsubcuenta , Cargo , Concepto,Referencia){  
	this.Concepto = Concepto;
	this.Credito = Credito;
	this.numcuentacompleta = numcuentacompleta;
    this.nomsubcuenta = nomsubcuenta;
	this.Cargo = Cargo;
	this.Referencia = Referencia;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }

function verificarExistePartidaEnFacturas(numDoc)
{
	var retval=false;
	for(i=0;i<partidasPoliza.length &&  retval==false;i++)
	{
		for(j=0;j<partidasPoliza[i]['partidas'].length &&  retval==false;j++)
			if(partidasPoliza[i]['partidas'][j]['numDoc']==numDoc)
			{	retval=true; }
	}
	return retval;
}
//funcion que busca un numero de factura
function getIndexFactura(banco)
{
	var retval=false;
	var index=-1;
	for(i=0;i<partidasmovimientos.length && retval==false;i++)
	{
		//alert(partidasmovimientos[i]['numbanco']+"=="+banco);
		if(banco==partidasmovimientos[i]['numbanco'])
		{	retval=true;
			index=i;
		}
	}	
	return index;
}

function verificaSiExisteFactura(numfact)
{
	var retval=false;
	
	for(i=0;i<partidasPoliza.length && retval==false;i++)
	{
		if(numfact==partidasPoliza[i]['numcuentacompleta'])
			retval=true;
	}	
	return retval;
}

function agregarMovimiento()
{
	var numbanco= document.getElementById('numbanco').value;
	var nombanco= document.getElementById('nombanco').value;
	var monto= document.getElementById('monto').value;
	var Concepto =document.getElementById('Concepto').value;	
	var tmonto =document.getElementById('totmonto').value;
	
	if(numbanco.length <= 0 )
	{
		alert("Favor de ingresar el Banco.");
		return(false);
	}	
	
	
	if(Concepto.length <= 0)
	{
		alert("Favor de ingresar el Concepto.");
		return(false);
	}	
	
	
	//Valida 
	if(monto.length <= 0)
	{
		alert("Favor de ingresar el Monto.");
		return(false);
	}	
	if(tmonto=='')
	{
		tmonto='0';
	}
	
	partidasmovimientos[countpartidasmovimientos]= new objfacturaDProv(numbanco,nombanco ,Concepto,monto);
	countpartidasmovimientos++;
	agregarmovATablamoviminetos(numbanco,nombanco ,Concepto,monto);
	actualiza_totales();	
	document.getElementById('nombanco').value="";
	document.getElementById('numbanco').value="";	
	document.getElementById('Concepto').value="";
	document.getElementById('monto').value="";
	document.getElementById('nombanco').focus();
	
}
function actualiza_totales()
{
	
	//
	document.getElementById('totmonto').value = 0;

	var tabla= document.getElementById('tfacturas');
	var numren=tabla.rows.length;
	
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
			var mynode = cell.childNodes[k];
			//
			if(mynode.name=="vMonto")
			{
				//alert(mynode.name);
				monto2=mynode.value;
				document.getElementById('totmonto').value = parseFloat(document.getElementById('totmonto').value.replace(/,/g,'')) + parseFloat( monto2.replace(/,/g,''));
				document.getElementById('totmonto').value=Math.round(document.getElementById('totmonto').value*100)/100;
				document.getElementById('totmonto').value=NumberFormat(document.getElementById('totmonto').value, '2', '.', ',');
			}			
		}
	}
}
}
function cargarpoliza()
{
	var totcargo=0.00;
	var	totcredito=0.00;
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}
	var documento= document.getElementById('pathtexto').value;
	//alert(documento);
	var datas = { ppath: documento	};
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'php_ajax/cargapoliza.php',
						dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(request);
						 alert ( " Can't do because: " + error );
						},
						success: function(resp)
						{
							//alert(resp);
							var myObj = resp;
							for(i=0;i<resp.length;i++)
							{								
								if(resp[i].nomcuenta == null)
								{
									resp[i].nomcuenta='****** Error en Cuenta ******';									
									error=1;
								}
								if(resp[i].nomcuenta.match(/Error.*/) )
								{	
									error=1;
								}
								agregarpolizaMTablaFacturas(resp[i].cuenta,resp[i].nomcuenta,resp[i].cargo,resp[i].credito,resp[i].referencia,resp[i].concepto,resp[i].origen);
								partidasPoliza[countpartidasPoliza]= new objpolizaDctas(resp[i].cuenta,resp[i].cargo,resp[i].credito,resp[i].referencia,resp[i].concepto,resp[i].origen);
								
								countpartidasPoliza++;
								//totcargo=totcargo + parseFloat(resp[i].cargo);
								//totcredito=totcredito + parseFloat(resp[i].credito);
							}
							
        		            ///NOT how to print the result and decode in html or php///
							//document.getElementById('tcargo').value=totcargo;
							//document.getElementById('tcredito').value=totcredito;
							//document.getElementById('asientos').value=countpartidasPoliza;
		                    console.log(myObj);
							//
							if(resp.error==1)
							{
								alert("No se Pudo Cragar el Archivo");								
							}
							if(error==1)
							{
								alert("Se encontaron errores en algunas cuentas favor de verificarlas");								
							}
							else
							{
								//actualiza_mtotales();
								document.getElementById('gralguarda').disabled=false;
							}
						}
					
					});
	
}
function agregarpolizaMTablaFacturas(cuenta,nomcuenta,cargo,credito,referencia,descripcion,origen)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = cuenta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell1.className ="td1";
	newcell1.style.width = "70px";

	var newcell2 = newrow.insertCell(1);
	newcell2.innerHTML = nomcuenta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell2.className ="td8";
	newcell2.style.width = "150px";
	
	var newcell3 = newrow.insertCell(2);
	newcell3.innerHTML  ="<input type='hidden' id='vCargo' name='vCargo' readonly  value='"+cargo+"' >"+cargo;
	//"+addCommas(nomsubcuenta);
	newcell3.className ="td3";
	newcell3.style.width = "70px";

	var newcell4 = newrow.insertCell(3);
	newcell4.innerHTML ="<input type='hidden' id='vCredito' name='vCredito' readonly  value='"+credito+"' >"+credito;
	newcell4.className ="td4";
	newcell4.style.width = "70px";

	var newcell5 = newrow.insertCell(4);
	newcell5.innerHTML =referencia;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell5.className ="td5";
	newcell5.style.width = "70px";
	
	var newcell6 = newrow.insertCell(5);
	newcell6.innerHTML =descripcion;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell6.className ="td6";
	newcell6.style.width = "150px";
	
	var newcell7 = newrow.insertCell(6);
	newcell7.innerHTML =origen;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell7.className ="td6";
	newcell7.style.width = "50px";
	
	
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}
function agregarcuentasATablapolizas(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = numcuentacompleta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell1.className ="td1";
	newcell1.style.width = "100px";

	var newcell5 = newrow.insertCell(1);
	newcell5.innerHTML  =nomsubcuenta;//"<input type='hidden' id='vnomsubcuenta' name='vnomsubcuenta' readonly value='"++"'>"+addCommas(nomsubcuenta);
	newcell5.className ="td2";
	newcell5.style.width = "200px";

	var newcell6 = newrow.insertCell(2);
	newcell6.innerHTML = Cargo;//"<input type='hidden' id='vCargo' name='vCargo' readonly  value='"++"' >"+Cargo;
	newcell6.className ="td3";
	newcell6.style.width = "100px";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML =Credito;//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell3.className ="td4";
	newcell3.style.width = "100px";

	var newcell4 = newrow.insertCell(4);
	newcell4.innerHTML =Concepto;
	newcell4.className ="td5";
	newcell4.style.width = "200px";
	
	var newcell6 = newrow.insertCell(5);
	var functiona="borrarFactura('"+numcuentacompleta+"',"+contRow+" )";
	newcell6.innerHTML ='<input type="button" id="borrarfact" name="borrarfact" value="-" onClick="'+functiona+'" >';
	newcell6.style.width = "50px";
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}

function agregarmovATablamoviminetos(numbanco,nombanco ,Concepto,monto)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = numbanco;
	newcell1.style.width = "60px";

	var newcell2 = newrow.insertCell(1);
	newcell2.innerHTML  =nombanco;
	newcell2.style.width = "120px";

	var newcell3 = newrow.insertCell(2);
	newcell3.innerHTML =Concepto;
	newcell3.style.width = "250px";
	
	
	var newcell4 = newrow.insertCell(3);
	newcell4.innerHTML = "<input type='hidden' id='vMonto' name='vMonto' readonly  value='"+monto+"' >"+monto;
	newcell4.style.width = "60px";

	
	var newcell5 = newrow.insertCell(4);
	var functiona="borrarFactura('"+numbanco+"',"+contRow+" )";
	newcell5.innerHTML ='<input type="button" id="borrarfact" name="borrarfact" value="-" onClick="'+functiona+'" >';
	newcell5.style.width = "40px";
}

function borrarFactura(numbanco, contRow)
{
	var answer = confirm("¿Seguro deseas borrar esta partida?")
	if (answer)
	{
		
		var index = getIndexFactura(numbanco);
		if( contRow ==index)
		{
			partidasmovimientos.splice( index ,1 );
			countpartidasmovimientos--;
			borrado=true;
			
			if(borrado)
			{
				refreshTablaFacturas();
				alert("Partida eliminada.");
			}
		}
	}
	actualiza_totales();
}

function refreshTablaFacturas()
{
	//partidasPoliza
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}

	for (var i = 0; i < partidasmovimientos.length; i++) 
	{ 
		//numbanco,nombanco ,Concepto,monto
		agregarmovATablamoviminetos(partidasmovimientos[i]['numbanco'] ,
									partidasmovimientos[i]['nombanco'],
									partidasmovimientos[i]['Concepto'],
									partidasmovimientos[i]['monto']);
	}
	
}

function guardarMovimientos()
{
	
	var tmonto =document.getElementById('totmonto').value;
	//Valida 
	var a=tmonto.replace(/,/g,"");	
	
		
	var fecmov= document.getElementById('fecmov').value;
	var ntmov= document.getElementById('ntmov').value;
	if(ntmov==0)
   {
	   alert("No se selecciono ningun tipo de Movimiento Valido favor de revisarlo");
	   return false;
   }
	
	
	var datas = {
					partidas	: partidasmovimientos,
					fecha_mov   : fecmov,
					numero_tmov : ntmov,					
    			};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				dataType: "json",
				url:      'php_ajax/guardaMovimientos.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						console.log(resp);
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								document.getElementById('fecmov').value="";								
								document.getElementById('tmov').value="";
								document.getElementById('ntmov').value="";									
								document.getElementById('nombanco').value="";
								document.getElementById('numbanco').value="";	
								document.getElementById('Concepto').value="";
								document.getElementById('monto').value="";
								document.getElementById('totmonto').value="";
								var Table = document.getElementById('tfacturas');
								var numren=Table.rows.length;
								for(var i = numren-1; i >=0; i--)
								{
									Table.deleteRow(i);
								}
								partidasmovimientos=new Array(); 
								countpartidasmovimientos =0;								
								document.getElementById('fecmov').focus();
								alert(resp[0].msgerror);
							}
							else
							{
								alert( "Error: "+resp[0].msgerror);
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
					}
				}
			});
}

function guardarMPoliza()
{
	var fecpoliza= document.getElementById('fecpoliza').value;
	var tpoliza= document.getElementById('tpoliza').value;
	var numpoliza= document.getElementById('numpoliza').value;
	var descrip= document.getElementById('descrip').value;
	
	
	var datas = {
					partidas	: partidasPoliza,
					fecha_poliza   : fecpoliza,
					tipo_poliza : tpoliza,
					numero_poliza : numpoliza,
					descripcion : descrip
    			};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/guardaMPoliza.php',
				dataType: "json",
				data:     datas,
				error: function(request,error) 
				{
				 console.log(request);
				 alert ( " Can't do because: " + error );
				},
				success: function(resp) 
				{
					//alert(resp)
					console.log(resp);
					if( resp.length ) 
					{						
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								//document.getElementById('nomsubcuenta').value=resp[0].nombre;
								//document.getElementById('numcuentacompleta').value=resp[0].cuenta;
								window.location = resp[0].path;
							}
							else
							{
								alert( "Error: "+resp[0].msgerror);
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
					}
				}
			});
}


function getNomCuenta2(object, numVcuenta)
{
	//alert("Me lleva");
	//var numcuenta = object.value;
	var numsubcuenta = object.value;
	

	if(numsubcuenta.length<=0)
	{
		alert("Debe teclear numero de sub-cuenta");
		//document.getElementById('numsubcuenta').focus();
		return( false);
		
	}
	
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					subcuenta   : numsubcuenta
    			};//folio_ImpCheque : folio_ImpCheque,
	//alert('php_ajax/crear_chequespdf.php?'+prods);
	//location.href = 'php_ajax/crear_chequespdf.php?'+datas;
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/edicion_getNomCuenta.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								var dnomcuentas = document.getElementsByName('dnomcuenta[]');//  getElementById('dnomcuenta');
								
								//alert(dnomcuentas.length+","+numVcuenta);
								
								dnomcuentas[numVcuenta].value = myArray[0].nombre;
								//document.getElementById('dnomcuenta').value=myArray[0].cuenta;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}


function agregarCargoCredito_edicion()
{
	var numcuentacompleta= document.getElementById('numcuentacompleta').value;
	var nomsubcuenta= document.getElementById('nomsubcuenta').value;
	var Cargo= document.getElementById('Cargo').value;
	var Concepto =document.getElementById('Concepto').value;
	var Credito= document.getElementById('Credito').value;
	var Referencia= document.getElementById('Referencia').value;
	
	/*if(Referencia.length <= 0 )
	{
		alert("Favor de ingresar Referencia");
		return(false);
	}	*/

	if(Credito.length <= 0 )
	{
		alert("Favor de ingresar Credito");
		return(false);
	}	

	if(numcuentacompleta.length <= 0 )
	{
		alert("Favor de ingresar numero de cuenta.");
		return(false);
	}	
	
	if(nomsubcuenta.length <= 0)
	{
		alert("Favor de ingresar nombre de cuenta.");
		return(false);
	}	

	if(Cargo.length <= 0)
	{
		alert("Favor de ingresar cargo.");
		return(false);
	}	

/*	if(verificaSiExisteFactura(numcuentacompleta))
	{	
		alert("El número de factura corresponde a una agregada anteriormente.");
		return(false);
	}*/
	//alert(Referencia);Concepto, Credito ,	numcuentacompleta ,  nomsubcuenta , Cargo , Concepto,Referencia

	agregarFacturaATablaFacturas_edicion(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo, Referencia);

	document.getElementById('numcuenta').value="";
	document.getElementById('numsubcuenta').value="";
	document.getElementById('numcuentacompleta').value="";
	document.getElementById('nomsubcuenta').value="";
	document.getElementById('Cargo').value="";
	document.getElementById('Concepto').value="";
	document.getElementById('Credito').value="";
	document.getElementById('Referencia').value="";
}


function agregarFacturaATablaFacturas_edicion(Concepto,Credito,numcuentacompleta,nomsubcuenta,Cargo, referencia)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = '<input type="hidden" name="idpartida[]" id="idpartida[]" value="0"/><input type="text" id="dcuenta[]" name="dcuenta[]" value="'+numcuentacompleta+'"   style="width:100%;" onKeyPress="return aceptarSoloNumeros(this,event);" onblur="getNomCuenta2(this, '+numcuentacompleta+');" class="texto8"/>';
	newcell1.className ="td1";
	newcell1.style.width = "100px";

	var newcell5 = newrow.insertCell(1);
	newcell5.innerHTML  ='<input type="text" id="dnomcuenta[]" name="dnomcuenta[]" value="'+nomsubcuenta+'"  style="width:100%;" class="texto8"/>';
	newcell5.className ="td2";
	newcell5.style.width = "200px";

	var newcell6 = newrow.insertCell(2);
	newcell6.innerHTML = '<input type="text" id="dcargo[]" name="dcargo[]" value="'+Cargo+'"  style="width:100%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"/>';//"<input type='hidden' id='vCargo' name='vCargo' readonly  value='"++"' >"+Cargo;
	newcell6.className ="td3";
	newcell6.style.width = "100px";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML ='<input type="text" id="dcredito[]" name="dcredito[]" value="'+Credito+'"  style="width:100%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"/>';//"<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell3.className ="td4";
	newcell3.style.width = "100px";

	var newcell4 = newrow.insertCell(4);
	newcell4.innerHTML ='<textarea style="width:100%;" id="dconcepto[]" name="dconcepto[]"  class="texto8" >'+Concepto+'</textarea>';
	newcell4.className ="td5";
	newcell4.style.width = "200px";
	
	var newcell9 = newrow.insertCell(5);
	newcell9.innerHTML ='<input  style="width:100%;" id="dreferencia[]" name="dreferencia[]" value="'+referencia+'"  class="texto8">';
	newcell9.className ="td5";
	newcell9.style.width = "100px";
	
	var newcell6 = newrow.insertCell(6);
	var functiona="borrardPoliza("+contRow+" )";
	newcell6.innerHTML ='<img src="../../imagenes/eliminar.jpg" onClick="'+functiona+'">';
	newcell6.style.width = "50px";
	/*newrow.onclick  = function(){
									mostrarPartidasdFactura(this,event,numcuentacompleta,'sumDocdFact'+contRow)
								}*/
}


function borrardPoliza(contRow)
{
	var answer = confirm("¿Seguro deseas borrar esta partida?")
	if (answer)
	{
		//partidasPoliza.splice( index ,1 );
		//countpartidasPoliza--;
		//borrado=true;
			var Table = document.getElementById('tfacturas');

			Table.deleteRow(contRow) ;
			//refreshTablaFacturas();
			alert("Partida eliminada.");

	}
}

function cancela_partidapoliza(idpartida, row)
{
	var pregunta = confirm("¿Esta seguro que desea eliminar la partida? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					id		: 		idpartida
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/cancela_partidapoliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									var Table = document.getElementById('tfacturas');
									Table.deleteRow(row) ;								
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}


var partidasmovimientos=new Array(); 
var countpartidasmovimientos =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

function objfacturaDProv(numbanco,nombanco ,Concepto,monto){  
	this.Concepto = Concepto;
	this.numbanco = numbanco;
	this.nombanco = nombanco;
    this.monto = monto;
	//this.Cargo = Cargo;
	//this.Referencia = Referencia;
	//this.Origen = Origen;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }
 function objpolizaDctas(cuenta, cargo ,credito , referencia,concepto,origen )
 {  
	this.numcuentacompleta = cuenta;
	this.Cargo = cargo;
	this.Credito = credito;
    this.Referencia = referencia;
	this.Concepto = concepto;
	this.Origen = origen;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }
function afectar_polizas()
{
	var polizasAAfectar=new Array();
	var numpoliza=0;
	var polizas = document.getElementsByName("afectacion");
	var tipops = document.getElementsByName("tipop");
	
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizasAAfectar[numpoliza]=polizas[i].value;
			numpoliza++;
			//alert(polizas[i].value);
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea afectar a la(s) polizas seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					polizas		: 		polizasAAfectar
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/afecta_poliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									 busca_cheque();						
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}

function desafectar_polizas()
{
	var polizasAAfectar=new Array();
	var numpoliza=0;
	var polizas=document.getElementsByName("afectacion");
	//alert("AAAA");
	
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizasAAfectar[numpoliza]=polizas[i].value;
			numpoliza++;
			//alert(polizas[i].value);
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea desafectar a la(s) polizas seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		//alert("BBB");
		var datas = {
					polizas		: 		polizasAAfectar
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/desafecta_poliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									//alert("WS");
									 busca_cheque();
									//alert("ZA");
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}

// busuqeda incrementl *****
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
	//alert("error");
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('nombanco').value);
		searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
       if(searchReq.responseText.length<1);
		{
			document.getElementById('numbanco').value = '';	
			//document.getElementById('nomsubcuenta').value = '';
		}
	   ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+'; '+tok[0]+' ">' + tok[1] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    //document.getElementById('nomsubcuenta').value = idprod;
	var tok =idprod.split(";");	
	document.getElementById('numbanco').value = tok[0];	
	document.getElementById('nombanco').value = tok[1];
	//$('#Concepto').text(tok[1]);
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
	document.getElementById('monto').focus();
}
//Funciones para la busqueda del tipo de movimineto bancario

var searchtipo = getXmlHttpRequestObject(); 

function searchtmov()
{
	var reg = partidasmovimientos.length;	
	if(reg>0)
	{
		var pregunta = confirm("¿Cuenta con "+reg+" Capturado desea Borrarlos? ");//+idpartida);//
		if (pregunta)
		{
			partidasmovimientos=new Array(); 
			countpartidasmovimientos =0;
			refreshTablaFacturas();
		}
	}
    if (searchtipo.readyState == 4 || searchtipo.readyState == 0) {
        var str = escape(document.getElementById('tmov').value);
		searchtipo.open("GET",'php_ajax/querytmov.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchtipo.onreadystatechange = handleSearchSuggesttmov;
        searchtipo.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggesttmov() {
	 var ss = document.getElementById('search_suggesttmov')
    if (searchtipo.readyState == 4) 
	{
       
       if(searchtipo.responseText.length<1);
		{
			document.getElementById('ntmov').value = '';	
			//document.getElementById('nomsubcuenta').value = '';
		}
	   ss.innerHTML = '';
        var str = searchtipo.responseText.split("\n");
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOvertmov(this);" ';
				suggest += 'onmouseout="javascript:suggestOuttmov(this);" ';
				suggest += "onclick='javascript:setSearchtmov(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+'; '+tok[0]+' ">' + tok[1] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggesttmov').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggesttmov').innerHTML = '';
			document.getElementById('search_suggesttmov').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggesttmov').innerHTML = '';
		document.getElementById('search_suggesttmov').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOvertmov(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOuttmov(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchtmov(value,idprod) {
    //document.getElementById('nomsubcuenta').value = idprod;
	var tok =idprod.split(";");			
	document.getElementById('ntmov').value = tok[0];	
	document.getElementById('tmov').value = tok[1];
	//$('#Concepto').text(tok[1]);
    document.getElementById('search_suggesttmov').innerHTML = '';
	document.getElementById('search_suggesttmov').className='sin_sugerencia_deBusqueda';
	document.getElementById('nombanco').focus();
}
//Terminan funciones para la busqueda del tipom de moviminetos


function addslashes (str)
{
   
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  

}
function aceptarSoloNumeros(evt)
{
//asignamos el valor de la tecla a keynum
	if(window.event)
	{// IE
		keynum = evt.keyCode;
	}
	else
	{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if(keynum>47 && keynum<58)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function gen_polizapasivo()
{
	
	var fecini = document.getElementById('fecini').value;
	var fecfin = document.getElementById('fecfin').value;
	var polizaspasivo=new Array();
	var numpoliza=document.getElementById('npoliza').value;	
	var polizas = document.getElementsByName('afectacion');
	var tipo='D';
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizaspasivo[numpoliza]=polizas[i].value;
			numpoliza++;			
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea generar la poliza D-"+numpoliza +" con los asientos seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					fecini:fecini,
					fecfin: fecfin,
					tipo: tipo,
					numpoliza: numpoliza,
					polizas	: polizaspasivo
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/guardaPolizaPasivo.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									 busca_cheque();						
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}
