<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini=$_REQUEST['query'];
$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
//$fecini = "";
$datos=array();

$nom1='';
$nom2='';

$tsql_callSP ="{call sp_bancos_c_inversion(?)}";//Arma el procedimeinto almacenado
$params = array(&$fini);
$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
$stmt = sqlsrv_query($conexion,$tsql_callSP, $params);
//echo $tsql_callSP;
//print_r($params);

$i=0;
while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
	//echo "pasa";
	// Comienza a realizar el arreglo, trim elimina espacios en blanco	
	$datos[$i]['banco']=$row['banco2'];
	$datos[$i]['nombanco']=$row['nombanco'];
	$datos[$i]['cuenta_ban']=$row['cuenta_ban'];	
	$datos[$i]['disp_ant']=number_format($row['disp_ant'],2);
	$datos[$i]['saldo_ant']=number_format($row['saldo_ant'],2);
	$datos[$i]['c1']=number_format($row['c1'],2);					
	$datos[$i]['c2']=number_format($row['c2'],2);
	$datos[$i]['c3']=number_format($row['inv_ant'],2);	
	$datos[$i]['c4']=number_format($row['c4'],2);
	$datos[$i]['c5']=number_format($row['c5'],2);
	$datos[$i]['c6']=number_format($row['c6'],2);
	$datos[$i]['c7']=number_format($row['c7'],2);
	$datos[$i]['c8']=number_format($row['c8'],2);
	$datos[$i]['saldo']=number_format($row['saldo'],2);	
	$datos[$i]['disponible']=number_format($row['disponible'],2);
	$datos[$i]['inversion']=number_format($row['inversion'],2);					
	$datos[$i]['dias']=$row['dias'];
	$i++;
}
//print_r($datos);
echo json_encode($datos);   // Los codifica con el jason
?>