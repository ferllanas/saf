<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../cheques/css/style.css">         
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Aplicaci&oacute;n de Recursos</title>
        <script src="../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../cheques/javascript/divhide.js"></script>
        <script src="../cheques/javascript/jquery.tmpl.js"></script>
        <script src="javascript/polizas_funciones.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
        <script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
           		
			function invierte()
			{
				if(!calcular_grabar())
				{
					return false;
				}
				var inversioninicial= new Array();
				var numinv=0;
				//var polizas = document.getElementsByName('saldo');
				var saldos = document.getElementsByName('saldo');
				var tasas = document.getElementsByName('tasa');
				var dias = document.getElementsByName('dia');
				var sms = document.getElementsByName('sm');	
				var bancos = document.getElementsByName('banco');					
				var invers = document.getElementsByName('inver');
				for(i=0;i<saldos.length;i++)
				{	
					inversioninicial[numinv]= new Object;
					
					inversioninicial[numinv]['banco']=bancos[i].value;			
					inversioninicial[numinv]['tasa']=tasas[i].value;
					inversioninicial[numinv]['dia']=dias[i].value;
					inversioninicial[numinv]['sm']=sms[i].value;
					inversioninicial[numinv]['inver']=invers[i].value;					
					numinv++;			
				}	
				if(numinv<=0)
				{
					alert("No existen Inversiones");
					return false;
				}
				
				var pregunta = confirm("¿Esta seguro que desea Invertir?");
				if (pregunta)
				{
					
					var datas = {
								
								inversion	: inversioninicial
							};
				
					jQuery.ajax({
								type:     'post',
								cache:    false,
								url:      'php_ajax/guardaInversion.php',
								dataType: 'json',
								data:     datas,	
								error: function(xhr, status, error){
									console.log(xhr.responseText);
									console.log(status);
									console.log(error);
									alert(xhr.responseText+" No cierre este dialogo, por favor comuniquese al departamento de sistemas.");
								},
								success: function(resp) 
								{
									console.log(resp);
									if(resp.length>0)
									{
										//alert(resp[0].fallo);
										if(resp[0].fallo==false)
										{
											alert("Se realizo la Inversion Satisfactoriamente");
											busca_recursos();
											//document.getElementById('totcredito').value=0.00;
										}
										else
										{
											alert( "No se Invirtio.");
										}
									}
									else
										alert( "Error no se pudo realizar al Inversion");
								}
								
							});
					
				}
			}
			function validacierre()
			{
        			//SE OMITIO VALIDACION PETICION GABY REPORTO MAGDA APLICO FERLLANAS 15/03/2013
					//SE OMITIO VALIDACION PETICION GABY REPORTO MAGDA APLICO FERLLANAS 01/09/2015
					//SE OMITIO VALIDACION PETICION GABY REPORTO MAGDA APLICO FERLLANAS 29/09/2015
					$.post('valida_ajax.php', function(resp)
					{ 	
						//console.log(resp);	
						if(resp.length<=0)
						{
							alert("No se ha cerrado el Dia anterior favor de Revisarlo");
							document.getElementById('calcula').disabled =true;
							document.getElementById('invertir').disabled =true;
						}
							
					}, 'json');  // Json es una muy buena opcion 
					/*
					document.getElementById('calcula').disabled =	false;
					document.getElementById('invertir').disabled =	false;*/
            }
			function busca_recursos()
			{
				$.post('inversion_ajax.php', function(resp)
				{ 
					$('#proveedor').empty();
					$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 						
				}, 'json');  // Json es una muy buena opcion 
				validacierre();
            }
			function calcular()
			{
				var numpoliza=0;				
				var saldos = document.getElementsByName('saldo');
				var tasas = document.getElementsByName('tasa');
				var dias = document.getElementsByName('dia');
				var sms = document.getElementsByName('sm');	
				var invers = document.getElementsByName('inver');
				var bancos = document.getElementsByName('banco');	
				for(i=0;i<saldos.length;i++)
				{	
						
						//alert(saldos[i].value+"-"+sms[i].value);
						var tot=parseFloat(saldos[i].value)-parseFloat(sms[i].value);
						if(tot<0)
						{	
							alert("El Banco "+bancos[i].value+" No cuenta con fondos para Invertir")
							tot=0;
							sms[i].value=0;
						}
						invers[i].value=tot.toFixed(2);
						numpoliza++;
				}
				document.getElementById('invertir').disabled=false;
			}
			function calcular_grabar()
			{
				var numpoliza=0;				
				var saldos = document.getElementsByName('saldo');
				var tasas = document.getElementsByName('tasa');
				var dias = document.getElementsByName('dia');
				var sms = document.getElementsByName('sm');	
				var invers = document.getElementsByName('inver');
				var bancos = document.getElementsByName('banco');	
				for(i=0;i<saldos.length;i++)
				{	
						
						//alert(saldos[i].value+"-"+sms[i].value);
						var tot=parseFloat(saldos[i].value)-parseFloat(sms[i].value);
						if(tot<0)
						{	
							alert("El Banco "+bancos[i].value+" No cuenta con fondos para Invertir")
							tot=0;
							sms[i].value=0;
							return false;
						}
						invers[i].value=tot.toFixed(2);
						numpoliza++;
						return true;
				}
				document.getElementById('invertir').disabled=false;
			}
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>          									
					
					<td>${banco}<input style="text-align:right" name="banco" id="banco" type="hidden" value=${banco}></td>
					<td>${cuenta_ban}</td>					
					<td>${nombanco}</td>
					<td><input style="text-align:right" name="tasa" id="tasa" type="text" value=${tasa}></td>	
					<td><input style="text-align:right" name="dia" id="dia" type="text" value=${dia_inv}></td>
					<td>${saldo_parcialf}<input style="text-align:right" name="saldo" id="saldo" type="hidden" value=${saldo_parcial}></td>
					<td><input style="text-align:right" name="sm" id="sm" type="text" value=${sdo_minimo}></td>					
					<td ><input style="text-align:right" name="inver" id="inver" type="text" value=${saldo_parcialf} readonly=readonly></td>
            </tr>
        </script>   
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body onLoad="busca_recursos()">
    <span class="TituloDForma">Invesi&oacute;n de Recursos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>

<form >
<table>
<tr>
	<td>
		<table>
        	<thead>         		   
              	<th><p>C.C.</p></th> 
                <th><p>Num. Cta.</p></th> 
                <th><p>Banco</p></th>
                 <th><p>Tasa %</p></th>					                                     
                <th><p>Dias</p></th> 
        		<th><p>Saldo</p></th> 
        		<th><p>Mesa de Dinero</p></th>
                <th><p>Inverison Mesa</p></th>                
             </thead>
            <tbody id="proveedor" name='proveedor'>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Encontrar Resultados</td>
                </tr>
            </tbody>
          </table>    
      </td>
</tr>
<tr>
	<td align="center">
     <input type="button" value="Calcular" name="calcula" id="calcula" onClick="calcular()" style="text-align:left"> <input type="button" id="invertir" name="invertir" value="Invertir" onClick="invierte()" disabled>
    </td>
</tr>
</table>
</form>

</table>
    </div>
    </body>
</html>
