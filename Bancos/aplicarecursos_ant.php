<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../cheques/css/style.css">         
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Aplicaci&oacute;n de Recursos</title>
         <script src="../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../cheques/javascript/divhide.js"></script>
        <script src="../cheques/javascript/jquery.tmpl.js"></script>
        <script src="javascript/polizas_funciones.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
        <script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
           		
			function afectadmovs()
			{
				
				var banco = document.getElementById('opc').value;
				var polizasinicial= new Array();
				var numpoliza=0;
				var polizas = document.getElementsByName('afectacion');
				for(i=0;i<polizas.length;i++)
				{	
					if(	polizas[i].checked)
					{	
						//var polizaspasivo=new Array();
			
						polizasinicial[numpoliza]= new Object;
						
						var tok=polizas[i].value.split("¬");
						polizasinicial[numpoliza]['id']=tok[1];			
						polizasinicial[numpoliza]['monto']=tok[0];
						polizasinicial[numpoliza]['entsal']=tok[2];
						polizasinicial[numpoliza]['tipomov']=tok[3];
						numpoliza++;			
					}		
				}	
				if(numpoliza<=0)
				{
					alert("No existen Movimientos seleccionados");
					return false;
				}
				
				var pregunta = confirm("¿Esta seguro que desea Afectar los Movimientos seleccionados?");
				if (pregunta)
				{
					
					var datas = {
								banco: banco,
								polizas	: polizasinicial
							};
				
					jQuery.ajax({
								type:     'post',
								cache:    false,
								url:      'php_ajax/guardaAfectaDmovs.php',
								dataType: 'json',
								data:     datas,								
								success: function(resp) 
								{
									console.log(resp);
									if(resp.length>0)
									{
										//alert(resp[0].fallo);
										if(resp[0].fallo==false)
										{
											alert("Se Afectaron los Movimientos Satisfactoriamente");
												$('#proveedor').empty();
											document.getElementById('totcredito').value=0.00;
										}
										else
										{
											alert( "No existe moviminetos Afectados.");
										}
									}
									else
										alert( "Error no se pudo obtener la partida de la poliza ."+resp[0].msgerror);
								}
								
							});
					
				}
			}
			function busca_recursos()
			{
        		var banco = document.getElementById('opc').value;
				if(banco>0)
				{
					var data = 'query=' + banco;     // Toma el valor de los datos, que viene del input						
					//alert(data);

					$.post('aplicarecursos_ajax.php',data, function(resp)
					{ //Llamamos el arch ajax para que nos pase los datos
					
					//alert(resp.response);
					$('#proveedor').empty();
					$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
					calculatotal();
					// Checa el plugin de templating para Java, tomando los productos del script de abajo y 	calculatotal()
							 // los va colocando en la forma de la tabla
					}, 'json');  // Json es una muy buena opcion 
				}
            }
			function calculatotal()
			{
				var numpoliza=0;				
				var totcredito=0.00;
				var polizas = document.getElementsByName('afectacion');
				for(i=0;i<polizas.length;i++)
				{	
					if(	polizas[i].checked)
					{	
						var tok=polizas[i].value.split("¬");
						if(tok[2]==1)
						{
							totcredito=parseFloat(totcredito)+parseFloat(tok[0]);
						}
						else
						{
								totcredito=parseFloat(totcredito)-parseFloat(tok[0]);
						}
						numpoliza++;			
					}		
				}	
				if(numpoliza<=0)
				{
					alert("No existen Moviminetos Seleccionados");
					document.getElementById('totcredito').value=0.00;
					return false;
				}
				else
				{
					//alert('si entra');
					document.getElementById('totcredito').value=totcredito.toFixed(2);
				}
			}
			function cancela_poliza(vale, tipo)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					location.href="php_ajax/cancela_poliza.php?poliza="+vale+"&tipo="+tipo;
				}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>          									
					{{if tipomov==15 }}
						<td><input type="checkbox" id="afectacion[]" name="afectacion" value="${monto}¬${id}¬${entsal}¬${tipomov}" onClick="calculatotal()" ></td>
					{{else}}
						<td><input type="checkbox" id="afectacion[]" name="afectacion" value="${monto}¬${id}¬${entsal}¬${tipomov}" onClick="calculatotal()" checked></td>
					{{/if}}
					<td>${descrip}</td>
					<td>${fecha}</td>					
					<td>${concepto}</td>
					<td style="text-align:right">${montof}</td>				
					
            </tr>
        </script>   
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Aplicaci&oacute;n de Recursos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table> 

<tr>
<td width="23%"><select name="opc" id="opc" style="z-index:2" >
<?php
 	$command= "select banco,nombanco from egresosmbancos WHERE  estatus<'9000' order by banco";					
	
	$getProducts = sqlsrv_query($conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{			
			echo "<option value='".$row['banco']."'>".$row['banco']." - ".$row['nombanco']."</option>";
			
		}
	}	
 ?>
</select>
</td>
<td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_recursos()">
</td>
</tr>
</table>
<form >
<table>
<tr>
	<td>
		<table>
        	<thead>         		   
              	<th><p>Aplicar</p></th> 
                <th><p>Tipo</p></th> 
                <th><p>Fecha</p></th>					                                     
                <th><p>Concepto</p></th> 
        		<th><p>Monto</p></th>
             </thead>
            <tbody id="proveedor" name='proveedor'>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Encontrar Resultados</td>
                </tr>
            </tbody>
          </table>    
      </td>
</tr>
<tr>
	<td align="center">
      Total de Afectaci&oacute;n: $<input type="text" value="" name="totcredito" id="totcredito" style="text-align:right"/>
    </td>
</tr>
<tr>
	<td align="center">
      <input type="button" value="Afectar Movimientos" onClick="afectadmovs()">
    </td>
</tr>
</table>
</form>

</table>
    </div>
    </body>
</html>
