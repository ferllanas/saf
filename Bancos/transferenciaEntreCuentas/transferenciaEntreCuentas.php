<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd "/xhtml1-transitional.dtd >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin1" />
<title>Transferencia entre Cuentas Propias</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<link type="text/css" href="../css/tablesscrollbody450.css" rel="stylesheet">

<script type="text/javascript" src="javascript/incrementalBancoPropios.js"></script>
<script type="text/javascript" src="javascript/guardarMovimientos.js"></script>
</head>
<body>
<form id="form1" name="form1" method="post" action="">
<table width="101%" border="0">
	<tr>
    	<td class="TituloDForma">Transferencia entre Cuentas Propias<hr class="hrTitForma"></td>
    </tr>
    <tr>
		<td>
      		<table width="100%" border="0">
        		<tr>
              		<td class="texto8" width="6%"><label>Fecha:</label></td class="texto8">
              		<td  width="10%"><input tabindex="1" class="texto8" name="fecmov" type="text" 
                            					id="fecmov" maxlength="10" style="width:70px" readonly="readonly" 
                                                value="<?php echo date('d/m/Y')//;"17/07/2014";//?>" />
              		</td>
              		<td class="texto8" colspan="2"><label>Monto: $</label>
                											<input tabindex="2"  class="texto8" 
                                                            type="text" name="monto" id="monto" style="width:120px" 
                                                            onkeypress="return aceptarSoloNumeros(this,event);"
                                                            onkeydown="asignaFormatoSiesNumerico(this, event)" />
              		</td>
                    <td width="59%">
                    	<label>Referencia: </label>
                											<input tabindex="2"  class="texto8" 
                                                            type="text" name="referencia" id="referencia" style="width:120px" 
                                                            onkeypress="return aceptarSoloNumeros(this,event);"/>
                    </td>
            	</tr>         
			</table>
    	</td>
    </tr>
    <tr>
    	<td width="100%">
        	<table width="100%">
            	<tr>
                    <td class="texto8" width="100%">
                        <div align="left" style="z-index:7; position:relative; width:600px">Cuenta Origen:
                          <input tabindex="3"  class="texto8" name="origen" type="text" id="origen" 
                                size="50" style="width:300px;"  onkeyup="incrementalBancoPropioOrigen(this);" autocomplete="off"/>
                        <input type="text" class="texto8" name="numbancoorigen" id="numbancoorigen" size="50" 
                                style="width:100px;" readonly="readonly" />
                        <div id="search_suggestorigen" style="z-index:8; position:absolute;" > </div>
                      </div>
                    </td>
                </tr>       
                <tr>
                    <td class="texto8" width="100%">
                        <div align="left" style="z-index:4; position:relative; width:600px">Cuenta Destino:
                          <input tabindex="4"  class="texto8" name="destino" type="text" id="destino" 
                                size="50" style="width:300px;"  onkeyup="incrementalBancoPropioOrigen(this);" autocomplete="off"/>
                        <input type="text" class="texto8" name="numbancodestino" id="numbancodestino" size="50" 
                                style="width:100px;" readonly="readonly" />
                        <div id="search_suggestdestino" style="z-index:5; position:absolute;" > </div>
                      </div>
                    </td>
                </tr>          
            </table>
        </td>
    </tr>
    <tr>
    	<td width="100%">
        	<table width="100%">
            	<tr>
                    <td class="texto8" width="10%" style="vertical-align:top;"><label>Concepto:</label></td>
                    <td class="texto8" width="80%"><textarea tabindex="5" name="Concepto" id="Concepto" style="width:100%"></textarea>
                    </td >
           		</tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<input type="button" onclick="guardarMovimientos();"  value="Guardar Transferencia" class="texto8" tabindex="6"/>
        </td>
    </tr>
  </table>
</form>
</body>
</html>