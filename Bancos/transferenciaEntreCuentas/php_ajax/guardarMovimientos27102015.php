<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
require_once("../../../dompdf/dompdf_config.inc.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$usuario = "";
if(isset($_COOKIE['ID_my_site']))
	$usuario = $_COOKIE['ID_my_site'];
	
$sct="";
$datosFin= array();
$bancoorigen=0;
$bancodestino=0;
$monto=0.00;
$concepto="";
$fecha=date('Y/m/d');//"2014/07/17";//
$mes=date('m');

if(isset($_REQUEST['in_numbancoorigen']))
	$bancoorigen=$_REQUEST['in_numbancoorigen'];
	
if(isset($_REQUEST['in_numbancodestino']))
	$bancodestino=$_REQUEST['in_numbancodestino'];
	
if(isset($_REQUEST['in_monto']))
	$monto=$_REQUEST['in_monto'];

$monto=str_replace(",","",$monto);
//echo $monto;
if(isset($_REQUEST['in_concepto']))
	$concepto=$_REQUEST['in_concepto'];

$referencia=0;
if(isset($_REQUEST['in_referencia']))
	$referencia=$_REQUEST['in_referencia'];


//echo $bancoorigen.",14,".$fecha.",".$concepto.",".$monto.",6,0,". $usuario."<br>";
$retva="0";
$retval=sp_bancos_A_dmovs($bancoorigen, 14, $fecha, $concepto, $monto, 6, $referencia, $usuario );
if($retval=="OK")
	$retval=sp_bancos_A_dmovs($bancodestino, 5, $fecha, $concepto, $monto, 6, $referencia, $usuario);

$htmlstr="";
if($retval=="OK")
{
	$htmlstr=createPageFomat2($bancoorigen,$bancodestino, $concepto, $monto, $usuario, $referencia);
	
	if(strlen($htmlstr)>0)
	{
		$Allpdfs = new DOMPDF();	
		$Allpdfs->set_paper('letter');
		
		$Allpdfs->load_html($htmlstr);
		$Allpdfs->render();
		$pdfalls = $Allpdfs->output(); 
		
		$datosFin['FILE']="pdf_files/trans_CuentasProp".$referencia.".pdf";//.date("dmYHisu").".pdf";
		file_put_contents("../pdf_files/trans_CuentasProp".$referencia.".pdf",$pdfalls);//.date("dmYHisu").".pdf", $pdfalls);
		$datosFin['fallo']=false;
		$datosFin['msg']="La solicitud ha sido generada exitosamente.";
	}
}
else
{
	$datosFin['fallo']=false;
	$datosFin['msg']="Ha ocurrido un fallo al intentar guardar el movimiento.";
}

	
echo json_encode($datosFin);
	

function sp_bancos_A_dmovs($banco, $numconcepto, $fecha, $concepto, $monto, $otro, $otro2, $usuario)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$folio="";
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_bancos_A_dmovs(?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$banco, &$numconcepto, &$fecha, &$concepto, &$monto, &$otro, &$otro2, &$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$folio="OK";
	}
	
	
	return $folio;
}

function getDatosBanco($banco)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$nombanco="";
	$cuenta_ban="";
	$alias="";
	$command="SELECT banco, nombanco, cuenta_ban, alias FROM egresosmbancos WHERE banco=$banco";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r(sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$nombanco=trim($row['nombanco']);
			$cuenta_ban=trim($row['cuenta_ban']);
		}
	}
	
	return array($nombanco,$cuenta_ban);
}


function getCoordEgresos()
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$coord_egresos="";

	$command="SELECT coord_egresos FROM configmconfig WHERE estatus=0";
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r(sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$coord_egresos=trim($row['coord_egresos']);
		}
	}
	
	return $coord_egresos;
}

function getNomUsuario($usuario)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$nombre="";

	$command="SELECT nombre FROM menumusuarios WHERE usuario='$usuario'";
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r(sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$nombre=trim($row['nombre']);
		}
	}
	
	return $nombre;
}

function createPageFomat2($bancoorigen,$bancodestino, $concepto, $monto, $usuario, $referencia)
{
	global $imagenPDFPrincipal, $imagenPDFSecundaria;
		//global  $username_db, $password_db, $odbc_name, $server;
		//$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
		//$conexion = sqlsrv_connect($server,$infoconexion);
		
		$ffecha=date('d/m/Y');//"17/07/2014";//
		list($nombanco , $cuenta )=getDatosBanco($bancoorigen);
		list($nombancodestino , $cuentadestino )=getDatosBanco($bancodestino);
		$CoordinadorEgresos=getCoordEgresos();
		$nomusuario= getNomUsuario($usuario);
		
		$elabora="";
		$htmlstr='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<style>
		body 
		{
			font-family: "Arial";
			font-size: 9;
			width: 601px; 
			margin-top: 30px;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body >
		<table style="width: 100%;">
			<tr style="heigth:6px;">
            	<td style="width: 15%;" align="center">
                	<img src="'.getDirAtras(getcwd())."".$imagenPDFPrincipal.'" style="width:80px;">
                </td>
				<td style="width: 70%;" >
					<table style="width: 100%;" >
						<tr><td style="width: 50%; text-align:center;">'.utf8_encode("COORDINACI�N DE EGRESOS Y CONTROL PATRIMONIAL").'</td></tr>
                        <tr><td style="width: 50%; text-align:center;"><u>TRANSFERENCIA DE CUENTAS PROPIAS</u></td></tr>
					</table>
				</td>
				<td style="width: 15%;" align="center"><img src="'.getDirAtras(getcwd())."".$imagenPDFSecundaria.'" style="width:80px;">
                </td>
			</tr>
            <tr>
            	<td colspan="3">
                	<table style="width:100%;">
                    	<tr><td style="text-align:left; width:50%;">'.$ffecha.'</td>
                			<td style="text-align:left; width:50%;">
                            	<table style="width:100%;">
                                	<tr><td style="text-align:right;">'.utf8_encode("N�mero de Transferencia").'</td>
                                    	<td style="text-align:center;"><b>'.$referencia.'</b></td>
                                     </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3" style="width:100%; text-align:right; font-size:8px;">
					<div style="position:absolute; left:0px; top:150px; width:95%; ">&nbsp;<div></td>
            </tr>
			<tr>
            	<td colspan="3" style="width:100%;">
					<div style="position:absolute; left:0px; top:150px; width:95%; height:300px; border: 1pt solid black;">
                	<table style="width:100%;vertical-align:top;">
                    	<thead style="vertical-align:top;">
                    	<tr>
                        	<th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE ORIGEN</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">BENEFICIARIO</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">IMPORTE</th>
                        </tr>
                        </thead>
                        <tbody >
						<tr><td colspan="3"><hr></td></tr>';
						
			
			
			$autoriza="";
			$solicita="";

				
			$htmlstr.='<tr>
							<td style="width:100/3%">
								<table style="width:100%;">
									<tr>
										<td style="text-align:center; width:30%;">'.trim($bancoorigen).'</td>
										<td style="text-align:justify; width:70%;">'.trim($cuenta)."<br>".trim($nombanco).'</td>
									</tr>
								</table>
							</td>
			<td style="width:100/3%">
							<table style="width:100%;">
								<tr>
									<td style="text-align:center; width:30%">'.$bancodestino.'</td>
									<td style="text-align:justify; width:70%">'.trim($cuentadestino)."<br>".trim($nombancodestino).'</td>
								</tr>
							</table>
			</td>
			<td style="text-align:center; vertical-align:right; width:100/3%">$'.number_format($monto,2).'</td></tr>
			';
				
				$solicita = "";
				$autoriza = "";
			
                        $htmlstr.='</tbody>
                    </table>
					</div>
                </td>
            </tr>	
            <tr>
            	<td colspan="3"><div style="position:absolute; left:0px; top:500px; width:95%%; height:18px;  border-color:#000; border-width:medium;"><b>POR CONCEPTO</b><div>
				</td>
            </tr>
            <tr><td colspan="3">
					<div style="position:absolute; left:0px; top:518px; width:95%; height:70px; border-color:#000; border-width:medium;border: 1pt solid black;">
						<table style="width:100%; height:60px; border:#000; border-width:1px; vertical-align:top">
							<tr><td style="vertical-align:top;">'.utf8_encode($concepto).'</td></tr>
						</table>
					</div>
				</td>
			</tr>
            <tr>
            	<td colspan="3">
					<div style="position:absolute; left:0px; top:708px; width:95%; height:70px;">
                	<table style="width:100%;">
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
                        </tr>
                        <tr>
                        	<td style="text-align:center; width:30%;">ELABORA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">AUTORIZA</td>
                        </tr>
                         <tr>
                        	<td style="text-align:center; width:30%;">'.utf8_encode($nomusuario).'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.utf8_encode($CoordinadorEgresos).'</td>
                        </tr>
                    </table>
					</div>
                </td>
            </tr>
			
		</table>
	</body>
</html> ';
	
		return $htmlstr;
	}


function getValesOSolCheFromCheque($folio)
{
	global $conexion_srv;
	$returned="<table width='400px' align='center'>
					<thead>
						<tr>
							<th width='150px'>Solitud/Factura</th>
							<th width='150px'>Importe</th>
						</tr>
					</thead>
					<tbody>";
	$command="SELECT a.numdocto, a.tipodocto, a.factura, b.descrip, 
		CASE WHEN a.tipodocto = 6 THEN
					(SELECT e.importe FROM egresosmsolche e WHERE e.folio=a.numdocto )
				ELSE
					(SELECT f.total FROM egresosmvale f WHERE f.vale=a.numdocto)  END as importe,
		CASE WHEN a.tipodocto = 6 THEN
					(SELECT e.concepto FROM egresosmsolche e WHERE e.folio=a.numdocto )
				ELSE
					(SELECT f.concepto FROM egresosmvale f WHERE f.vale=a.numdocto)  END as concepto
		FROM egresosdcheque a 
		INNER JOIN egresostipodocto b ON a.tipodocto=b.id 
		WHERE a.folio=".$folio."  AND a.estatus<9000 ORDER BY a.tipodocto";

			$autoriza="";
			$solicita="";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r(sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					if($row['tipodocto']=='7')
						$returned.=  "<tr><td style='text-align=left;' align='left'>*Factura:".$row['factura']." de Vale: <b>".$row['numdocto']."</b></td><td align='right'><b>$".number_format($row['importe'],2)."</b></td></tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Concepto:</b>".$row['concepto']."</td></tr>";//." Factura: ".$factura;//
					else
						$returned.=  "<tr><td style='text-align=left;' align='left'>*".$row['descrip'].": <b>".$row['numdocto']."</b></td><td align='right'><b>$".number_format($row['importe'],2)."</b></td></tr><tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Concepto:</b>".$row['concepto']."</td></tr>";
				}
			}
			$returned.="</tbody></table>";
			sqlsrv_free_stmt( $stmt);
   return $returned;
}
?>