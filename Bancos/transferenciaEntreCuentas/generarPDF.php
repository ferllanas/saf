<?php
/**/
// Vamos a mostrar un PDF
/*header('Content-type: application/pdf');
// Se llamar� downloaded.pdf
header('Content-Disposition: attachment; filename="downloaded.pdf"');
header("Pragma: no-cache");
header("Expires: 0");
*/
require_once("../../dompdf-master/dompdf_config.inc.php");
require_once("../../connections/dbconexion.php");

$id=$_REQUEST['id'];

$bancoorigen="";//$_POST['bancoorigen'];
$bancodestino="";
$concepto="";
$monto="";
$usuario="";
$referencia="";
$fecha="";
$hora="";

list($bancoorigen,$bancodestino, $concepto, $monto, $usuario, $referencia, $fecha, $hora) = getBancosDMovs($id);

$fechaExploded = explode("/",$fecha);

$table=str_replace("color:#FFF","color:black", $table);
$table=str_replace("background:#006600","background:gren", $table);
$table=str_replace('class="fila"','class="filaPDF"', $table);

$date= $fecha;
$time= $hora;

$anio=$fechaExploded[2];
$dompdf = new DOMPDF();

$htmlstr = createPageFomat2($bancoorigen,$bancodestino, $concepto, $monto, $usuario, $referencia,$date);
//echo $htmlstr;
$dompdf->load_html($htmlstr);

$dompdf->render();
	  
$pdf = $dompdf->output(); 

$dompdf->stream("downloaded.pdf");


function createPageFomat2($bancoorigen,$bancodestino, $concepto, $monto, $usuario, $referencia,$ffecha)
{
		global  $imagenPDFPrincipal, $imagenPDFSecundaria;
		//$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
		//$conexion = sqlsrv_connect($server,$infoconexion);
		
		//$ffecha=date('d/m/Y');//"17/07/2014";//
		list($nombanco , $cuenta )=getDatosBanco($bancoorigen);
		list($nombancodestino , $cuentadestino )=getDatosBanco($bancodestino);
		$CoordinadorEgresos=getCoordEgresos($ffecha);
		$nomusuario= getNomUsuario($usuario);
		
		$elabora="";
		$htmlstr='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../../css/estilosPDF.css" rel="stylesheet" type="text/css">
		</head>
		
		<body >
		<table style="width: 100%;">
			<tr style="heigth:6px;">
            	<td style="width: 15%;" align="center">
                	<img src="'.getDirAtras(getcwd())."".$imagenPDFPrincipal.'" style="width:140px;">
                </td>
				<td style="width: 70%;" >
					<table style="width: 100%;" >
						<tr><td style="width: 50%; text-align:center;">'.utf8_encode("COORDINACI&Oacute;N DE EGRESOS Y CONTROL PATRIMONIAL").'</td></tr>
                        <tr><td style="width: 50%; text-align:center;"><u>TRANSFERENCIA DE CUENTAS PROPIAS</u></td></tr>
					</table>
				</td>
				<td style="width: 15%;" align="center"><img src="'.getDirAtras(getcwd())."".$imagenPDFSecundaria.'" style="width:80px;">
                </td>
			</tr>
            <tr>
            	<td colspan="3">
                	<table style="width:100%;">
                    	<tr><td style="text-align:left; width:50%;">'.$ffecha.'</td>
                			<td style="text-align:left; width:50%;">
                            	<table style="width:100%;">
                                	<tr><td style="text-align:right;">'.utf8_encode("N&uacute;mero de Transferencia").'</td>
                                    	<td style="text-align:center;"><b>'.$referencia.'</b></td>
                                     </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3" style="width:100%; text-align:right; font-size:8px;">
					<div style="position:absolute; left:0px; top:150px; width:95%; ">&nbsp;<div></td>
            </tr>
			<tr>
            	<td colspan="3" style="width:100%;">
					<div style="position:absolute; left:0px; top:150px; width:95%; height:300px; border: 1pt solid black;">
                	<table style="width:100%;vertical-align:top;">
                    	<thead style="vertical-align:top;">
                    	<tr>
                        	<th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE ORIGEN</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">BENEFICIARIO</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">IMPORTE</th>
                        </tr>
                        </thead>
                        <tbody >
						<tr><td colspan="3"><hr></td></tr>';
						
			
			
			$autoriza="";
			$solicita="";

				
			$htmlstr.='<tr>
							<td style="width:100/3%">
								<table style="width:100%;">
									<tr>
										<td style="text-align:center; width:30%;">'.trim($bancoorigen).'</td>
										<td style="text-align:justify; width:70%;">'.trim($cuenta)."<br>".trim($nombanco).'</td>
									</tr>
								</table>
							</td>
			<td style="width:100/3%">
							<table style="width:100%;">
								<tr>
									<td style="text-align:center; width:30%">'.$bancodestino.'</td>
									<td style="text-align:justify; width:70%">'.trim($cuentadestino)."<br>".trim($nombancodestino).'</td>
								</tr>
							</table>
			</td>
			<td style="text-align:center; vertical-align:right; width:100/3%">$'.number_format($monto,2).'</td></tr>
			';
				
				$solicita = "";
				$autoriza = "";
			
                        $htmlstr.='</tbody>
                    </table>
					</div>
                </td>
            </tr>	
            <tr>
            	<td colspan="3"><div style="position:absolute; left:0px; top:500px; width:95%%; height:18px;  border-color:#000; border-width:medium;"><b>POR CONCEPTO</b><div>
				</td>
            </tr>
            <tr><td colspan="3">
					<div style="position:absolute; left:0px; top:518px; width:95%; height:70px; border-color:#000; border-width:medium;border: 1pt solid black;">
						<table style="width:100%; height:60px; border:#000; border-width:1px; vertical-align:top">
							<tr><td style="vertical-align:top;">'.utf8_encode($concepto).'</td></tr>
						</table>
					</div>
				</td>
			</tr>
            <tr>
            	<td colspan="3">
					<div style="position:absolute; left:0px; top:708px; width:100%; height:70px;">
                	<table style="width:100%;">
						<tr>
                        	<td colspan="4">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="4">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="4">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="4">&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
                        </tr>
                        <tr>
                        	<td style="text-align:center; width:30%;">ELABORA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">AUTORIZA</td>
                        </tr>
                         <tr>
                        	<td style="text-align:center; width:30%;">'.utf8_encode($nomusuario).'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.utf8_encode($CoordinadorEgresos).'</td>
                        </tr>
                    </table>
					</div>
                </td>
            </tr>
			
		</table>
	</body>
</html> ';
	
		return $htmlstr;
	}


function getDatosBanco($banco)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$nombanco="";
	$cuenta_ban="";
	$alias="";
	$command="SELECT banco, nombanco, cuenta_ban, alias FROM egresosmbancos WHERE banco=$banco";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r(sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$nombanco=trim($row['nombanco']);
			$cuenta_ban=trim($row['cuenta_ban']);
		}
	}
	
	return array($nombanco,$cuenta_ban);
}


function getCoordEgresos($ffecha)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$coord_egresos="";
	$command="";

	$date = date('d/m/Y', time());
	
	$fechaSep=explode('/',$ffecha);
	
	$fechaEnglish= $fechaSep[2]."-".$fechaSep[1]."-".$fechaSep[0];
	
	if($ffecha==$date)
		$command="SELECT coord_egresos FROM configmconfig WHERE estatus=0";
	else
		$command="SELECT coord_egresos FROM configmconfig WHERE  falta<'$fechaEnglish' AND (fbaja>'$fechaEnglish' OR fbaja IS NULL)";
		
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r(sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$coord_egresos=trim($row['coord_egresos']);
		}
	}
	
	return $coord_egresos;
}

function getNomUsuario($usuario)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$nombre="";

	$command="SELECT nombre FROM menumusuarios WHERE usuario='$usuario'";
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r(sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$nombre=trim($row['nombre']);
		}
	}
	
	return $nombre;
}

function getBancosDMovs($id)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	if(!$conexion)
	{
		echo "Error de conexion.";
	}
	else
	{
		$bancoorigen="";
		$bancodestino="";
		$concepto="";
		$monto="";
		$usuario="";
		$referencia="";
		$fecha="";
		$hora="";

		
		$command="SELECT id, banco, tipomov, convert(varchar(10),fecha,103) as fecha, concepto,monto, origen, referencia, usuario, estatus, fbaja, ubaja, gpo_cpi, ugpo_cpi, fgpo_cpi, codifica_presup, faplica, usuaplica, saldo_minimo 
	FROM bancosdmovs a 
		WHERE  a.tipomov IN( 5,14) AND 
		a.monto = (SELECT monto FROM [fomeadmin].[dbo].[bancosdmovs] WHERE id=$id) AND 
		a.referencia = (SELECT referencia FROM [fomeadmin].[dbo].[bancosdmovs] WHERE id=$id) AND 
		a.fecha = (SELECT fecha FROM [fomeadmin].[dbo].[bancosdmovs] WHERE id=$id) AND a.concepto = (SELECT concepto FROM [fomeadmin].[dbo].[bancosdmovs] WHERE id=$id) ORDER BY id DESC";
		//echo $command."<br>";
		$getProducts = sqlsrv_query( $conexion,$command);
		if ( $getProducts === false)
		{ 
			die($command."". print_r(sqlsrv_errors(), true));
		}
		else
		{
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				//print_r($row);
				if($row['tipomov']==5)
					$bancodestino= 	$row['banco'];
				else
					$bancoorigen=	$row['banco'];
					
				$concepto=	$row['concepto'];
				$monto=	$row['monto'];
				$usuario=	$row['usuario'];
				$referencia=	$row['referencia'];
				$fecha= $row['fecha'];
			}
		}
	}
	
	return array($bancoorigen,$bancodestino, $concepto, $monto, $usuario, $referencia, $fecha, $hora);
}
?>