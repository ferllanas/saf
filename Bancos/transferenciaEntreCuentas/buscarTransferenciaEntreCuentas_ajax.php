<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);

$banco="";
if(isset($_REQUEST['banco']))
	$banco = $_REQUEST['banco'];
	
$tipo="";
if(isset($_REQUEST['concepto']))
	$tipo = $_REQUEST['concepto'];
	
$nom1='';
$nom2='';

		
	if($opcion==2)
	{
		$cuenta = $_REQUEST['banco'];
		
	
		$command= "SELECT a.id, a.banco, b.nombanco, b.nomcorto, a.concepto as descrip, a.tipomov, a.monto as total, a.estatus,
					 a.referencia,
					convert(varchar(10),a.fecha, 103)as fecha,convert(varchar(10),a.falta, 103)as falta, a.origen
					FROM bancosdmovs a LEFT JOIN egresosmbancos b ON a.banco=b.banco 
					WHERE tipomov IN (14,5) AND a.banco = '".$banco."' AND tipomov IN(5) AND a.referencia>0";
	}
	
	if($opcion==4)
	{
				$command= "SELECT a.id, a.banco, b.nombanco, b.nomcorto, a.concepto as descrip, a.tipomov, a.monto as total, a.estatus,
					 a.referencia,
					convert(varchar(10),a.fecha, 103)as fecha,convert(varchar(10),a.falta, 103)as falta, a.origen
					FROM bancosdmovs a LEFT JOIN egresosmbancos b ON a.banco=b.banco 
					WHERE tipomov IN (14,5) AND a.concepto LIKE '%" . substr($_REQUEST['query'],1) . "%' AND tipomov IN(5) AND a.referencia>0";								
	}		
	//	echo $opcion;
	
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "SELECT a.id, a.banco, b.nombanco, b.nomcorto, a.concepto as descrip, a.tipomov, a.monto as total, 
					a.estatus, a.referencia,
					convert(varchar(10),a.fecha, 103)as fecha,convert(varchar(10),a.falta, 103)as falta, a.origen
					FROM bancosdmovs a LEFT JOIN egresosmbancos b ON a.banco=b.banco 
					WHERE tipomov IN (14,5) AND a.falta >= '" . $fini . "' and a.falta <='" . $ffin . "' AND tipomov IN(5) AND a.referencia>0";
				 
	}	
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		//$datos[$i]['num_transf']=trim($row['num_transf']);
		$datos[$i]['tipo']=	trim($row['tipomov']);
		$datos[$i]['folio']=	trim($row['id']);
		$datos[$i]['referencia']=	trim($row['referencia']);
		$datos[$i]['falta']=	trim($row['falta']);					
		$datos[$i]['nomprov']=	trim(utf8_encode($row['nomcorto']));
		$datos[$i]['importe']=	"$".number_format($row['total'],2);
		$datos[$i]['concepto']=	utf8_encode($row['descrip']);
		$datos[$i]['estatus']=	trim($row['estatus']);
		$i++;
	}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>