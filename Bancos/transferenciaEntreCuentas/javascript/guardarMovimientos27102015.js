// JavaScript Document
function guardarMovimientos()
{
	var numbancoorigen= document.getElementById('numbancoorigen').value;
	if(numbancoorigen.length<=0)
	{
		alert("Favor de seleccionar Cuenta Origen.");
		return false;
	}
	var numbancodestino= document.getElementById('numbancodestino').value;
	if(numbancodestino.length<=0)
	{
		alert("Favor de seleccionar Cuenta Destino.");
		return false;
	}
	
	var fecmov= document.getElementById('fecmov').value;
	if(fecmov.length<=0)
	{
		alert("Favor de seleccionar fecha.");
		return false;
	}
	
	var monto= document.getElementById('monto').value;
	if(monto.length<=0)
	{
		alert("Favor de seleccionar monto.");
		return false;
	}
	else
	{
		monto.replace(/,/g,"");
	}
	var concepto= document.getElementById('Concepto').value;
	if(concepto.length<=0)
	{
		alert("Favor de seleccionar concepto.");
		return false;
	}
	
	var referencia= document.getElementById('referencia').value;
	if(referencia.length<=0)
	{
		alert("Favor de ingresar el numero de referencia.");
		return false;
	}
	else
	{
		referencia.replace(/,/g,"");
	}	
	
var datas = {
					in_numbancoorigen : numbancoorigen,
					in_numbancodestino: numbancodestino,
					in_fecmov: fecmov,
					in_monto: monto,
					in_concepto: concepto,
					in_referencia: referencia

    			};
	//alert(datas.cajero+", "+datas.banco+", "+datas.CajeroGral+", "+datas.total);
			
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/guardarMovimientos.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
					console.log(resp);
					alert(resp.msg);
					if(resp.fallo==false)
					{
						location.href='transferenciaEntreCuentas.php';
						window.open(resp.FILE,'REPORTE DE COBRANZA DIARIO','width=800,height=600');
					}
				}
			});
}