<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Encuesta</title>

<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/fun_encuesta.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>


<table width="99%" border="0">
  <tr>
    <td width="127" class="texto9"><div align="right"> Encuestador</div></td>
    <td width="391">
		<div align="left"   style="z-index:7; position:relative; width:400px; ">      
			<input name="enc" type="text" class="texto8" id="enc" tabindex="1" onKeyUp="searchempleado(this);" style="width:390px; height:16px; " autocomplete="off">
			<div id="search_suggestempleado" style="z-index:8; "></div>
		</div>	
	</td>
    <td width="95"><div align="right"><span class="texto9">Fraccionamiento </span></div></td>
    <td width="360"><span class="texto9">
      <input type="text" name="fracc" id="fracc" size="60">
    </span></td>
  </tr>
  <tr>
    <td class="texto9"><div align="right">Estatus Juridico del fracc. </div></td>
    <td><input type="text" name="est_jur" id="est_jur" size="60"></td>
    <td><div align="right"><span class="texto9">Ubicaci&oacute;n &oacute; # lote </span></div></td>
    <td><input type="text" name="ubica" id="ubica" size="60"></td>
  </tr>
</table>
<hr class="hrTitForma">

<table width="100%" border="0">
  <tr class="texto9">
    <td width="100"><input type="hidden" name="numemp" id="numemp"></td>
    <td width="32" class="texto8"><div align="center"></div></td>
    <td width="41">&nbsp;</td>
    <td width="20" class="texto8"><div align="center"></div></td>
    <td width="80">&nbsp;</td>
    <td width="20" class="texto8"><div align="center"></div></td>
    <td width="60">&nbsp;</td>
    <td width="60" class="texto8"><div align="center"></div></td>
    <td width="114" class="texto9">El lote que habita es: </td>
    <td width="132" class="texto9">El lote esta Escriturado: </td>
    <td width="116" class="texto9">Desde cuando habita este lote:</td>
  </tr>
  <tr class="texto9">
    <td width="100"><div align="right">Lote cercado:</div></td>
    <td>
        <input type="checkbox" name="cercado" id="cercado">
   </td>
    <td width="41"><div align="right">Lote baldio:</div></td>
    <td><input type="checkbox" name="baldio" id="baldio"></td>
    <td width="80"><div align="right">Lote deshabitado:</div></td>
    <td><input type="checkbox" name="deshab" id="deshab"></td>
    <td width="60"><div align="right">Lote Habitado:</div></td>
    <td><input type="checkbox" name="habitado" id="habitado"></td>
    <td class="texto9"><select name="lote_habita" id="lote_habita">
	  <option value=0></option>
      <option value=1>De su propiedad</option>
      <option value=2>Rentado</option>
      <option value=3>Prestado</option>
      <option value=4>En posesi�n</option>
    </select></td>
    <td class="texto9"><select name="lote_escrit" id="lote_escrit">
	  <option value=0></option>
      <option value=1>A su nombre</option>
      <option value=2>De un familiar</option>
      <option value=3>De otra persona</option>
      <option value=4>No esta escriturado</option>
    </select></td>
    <td class="texto9"><select name="hab" id="hab">
	  <option value=0></option>
      <option value=1>Mas de un a�o</option>
      <option value=2>Mas de 3 a�os</option>
      <option value=3>Mas de 5 a�os</option>
      <option value=4>Mas de 10 a�os</option>
    </select></td>
  </tr>
  <tr class="texto9">
    <td width="100">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td>&nbsp;</td>
    <td>&nbsp;
   </td>
    <td colspan="2"><div align="center">No Pago </div></td>
    <td colspan="2"><div align="center">&iquest; A quien ?</div></td>
	<td><div align="center">Costo total</div></td>
    <td><div align="center">Enganche</div></td>
    <td><div align="center">Mensualidad</div></td>
    <td><div align="center">Periodo o Plazo (meses)</div></td>
	 <td><div align="center">A�o que adquirio el lote</div></td>
  </tr>
  <tr class="texto9">
    <td colspan="2"><div align="right"><strong> monto aprox. que pago por este lote</strong></div></td>
    <td colspan="2"><div align="center">
      <input type="checkbox" name="nopago" id="nopago">
    </div></td>
	<td colspan="2"><div align="center"></div>	  <div align="center">
	  <select name="aquien" id="aquien">
        <option value=0></option>
        <option value=1>FOMERREY</option>
        <option value=2>A un lider</option>
        <option value=3>A un tercero</option>
      </select>
	</div></td>
	<td><div align="center">
      <input name="costo" type="text" id="costo" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="10" >
    </div></td>
    <td><div align="center">
      <input name="eng" type="text" id="eng" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="10" >
    </div></td>
    <td><div align="center">
      <input name="mens" type="text" id="mens" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="10" >
    </div></td>
    <td><div align="center">
      <input name="plazo" type="text" id="plazo" style="height:20px; text-align:right" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="0" size="10">
    </div></td>
	<td><div align="center">
	  <input name="adqui" type="text" id="adqui" style="height:20px; text-align:right" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="0" size="10">
    </div></td>
  </tr>
</table>
<hr class="hrTitForma">

<table width="99%" border="0">
  <tr>
    <td width="143">&nbsp;</td>
    <td width="72" class="texto9"><div align="center">Mts. de frente </div></td>
    <td width="56" class="texto9"><div align="center">Mts de fondo </div></td>
    <td width="181" class="texto9"><div align="center"></div></td>
    <td width="273" class="texto9"><div align="center"></div></td>
  </tr>
  <tr>
    <td class="texto10"><div align="right">&iquest;extensi&oacute;n del lote?
      <input name="area" type="text" id="area" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8"> 
      </div></td>
    <td><div align="center">
      <input name="frente" type="text" id="frente" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8">
    </div></td>
    <td><div align="center">
      <input name="fondo" type="text" id="fondo" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8">
    </div></td>
    <td class="texto9"><div align="center">
        <span class="texto9">Extension que tiene su casa</span> 
        <input name="ext" type="text" id="ext" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8">
    </div></td>
    <td class="texto9">
      <div align="left">De que ancho es la calle donde habita
        <input name="ancho" type="text" id="ancho" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8">
      </div></td>
  </tr>
  <tr class="texto9">
    <td><div align="right">Existe alguna Subdivision
          <input type="checkbox" name="subdiv" id="subdiv">
    </div></td>
    <td><div align="center">
      <input name="mts_div" type="text" id="mts_div" style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8">
    </div></td>
    <td colspan="3">No. personas viven en el hogar
		<input name="no_pers" type="text" id="no_pers" style="height:20px; text-align:right" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="0" size="8">
		No. Familias 
		<input name="no_fam" type="text" id="no_fam" style="height:20px; text-align:right" onKeypress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" value="0" size="8">
	El Jefe de Familia es 
	  <input type="text" name="jefe_fam" id="jefe_fam" size="30" maxlength="20">
    </td>
  </tr>
</table>
<table width="99%" height="142" border="0">
  <tr>
    <td width="70" class="texto9"><div align="right">No. Habitantes del hogar con empleo</div></td>
    <td width="107" class="texto9"><select name="trabajo" id="trabajo">
      <option value=0></option>
      <option value=1>Uno</option>
      <option value=2>Mas de uno</option>
      <option value=3>Ninguno</option>
      <option value=4>Buscan Empleo</option>
    </select></td>
    <td width="103" class="texto9"><div align="right">Es derechohabiente del Infonavit</div></td>
    <td width="39" class="texto9"><input type="checkbox" name="infonavit" id="infonavit" ></td>
    <td width="126" class="texto9"><div align="right">Ingresos mensuales que tiene el hogar </div></td>
    <td width="52" class="texto9"><input name="ingmen" type="text" id="ingmen" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8"></td>
    <td width="200" class="texto9">Alguno de los habitantes del hogar ahorra o planea hacerlo en el futuro    </td>
    <td width="97" class="texto9">
	<select name="ahorra" id="ahorra">
		<option value=0></option>
		<option value=1>Si</option>
		<option value=2>No</option>
		<option value=3>En el Futuro</option>
		<option value=4>No Sabe</option>
		
    </select>	</td>
  </tr>
  <tr class="texto9">
    <td colspan="2" class="texto9"><div align="center"><strong>Caracteristicas de su hogar</strong></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td><div align="right"><em>Techo:</em></div></td>
    <td colspan="7"><table width="757" border="0">
      <tr>
        <td width="145"><div align="right">Lamina o asbesto</div></td>
        <td width="76"><input type="checkbox" name="techo1" id="techo1"></td>
        <td width="53"><div align="right">Madera</div></td>
        <td width="69"><input type="checkbox" name="techo2" id="techo2"></td>
        <td width="72"><div align="right">Cemento</div></td>
        <td width="58"><input type="checkbox" name="techo3" id="techo3"></td>
        <td width="34"><div align="right">Otra</div></td>
        <td width="56"><input type="checkbox" name="techo4" id="techo4"></td>
		<td width="156"></td>
      </tr>
    </table></td>
  </tr>
  <tr class="texto9">
    <td><div align="right"><em>Paredes:</em></div></td>
    <td colspan="7"><table width="757" border="0">
      <tr>
        <td width="146"><div align="right">Lamina o asbesto</div></td>
        <td width="76"><input type="checkbox" name="pared1" id="pared1"></td>
        <td width="53"><div align="right">Madera</div></td>
        <td width="69"><input type="checkbox" name="pared2" id="pared2"></td>
        <td width="72"><div align="right">Cemento</div></td>
        <td width="58"><input type="checkbox" name="pared3" id="pared3"></td>
        <td width="34"><div align="right">Otra</div></td>
        <td width="55"><input type="checkbox" name="pared4" id="pared4"></td>
		<td width="156"></td>
      </tr>
    </table></td>
  </tr>
  <tr class="texto9">
    <td><div align="right"><em>Piso:</em></div></td>
    <td colspan="7"><table width="757" border="0">
      <tr>
        <td width="146"><div align="right">Tierra</div></td>
        <td width="76"><input type="checkbox" name="piso1" id="piso1"></td>
        <td width="53"><div align="right">Madera</div></td>
        <td width="69"><input type="checkbox" name="piso2" id="piso2"></td>
        <td width="72"><div align="right">Cemento</div></td>
        <td width="58"><input type="checkbox" name="piso3" id="piso3"></td>
        <td width="34"><div align="right">Otra</div></td>
        <td width="55"><input type="checkbox" name="piso4" id="piso4"></td>
		<td width="156"><div align="right">No. de Cuartos 
		      <input name="no_cuartos" type="text" id="no_cuartos" value="0" size="3">
		  </div></td>
      </tr>
    </table></td>
  </tr>
</table>
<hr class="hrTitForma">
<table width="99%" border="0">
  <tr>
    <td class="texto9"><div align="right"><strong>Que Tipo de Servicios tiene su hogar </strong></div></td>
    <td class="texto9"><div align="right">Cuenta con agua entubada
        <select name="agua" id="agua">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td colspan="2" class="texto9"><div align="center">Si no la tiene, cuanto paga mensualmente por suministro 
        <input name="pago_agua" type="text" id="pago_agua"  style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="8" >
    </div></td>
  </tr>
  <tr>
    <td class="texto9"><div align="right"></div></td>
    <td class="texto9"><div align="right">Drenaje
        <select name="dren" id="dren">
            <option value=0></option>
            <option value=1>Si</option>
            <option value=2>No</option>
            <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Energia Electrica
        <select name="enelec" id="enelec">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Lugar de Estacionamiento 
        <select name="estac" id="estac">
            <option value=0></option>
            <option value=1>Si</option>
            <option value=2>No</option>
            <option value=3>Ocasionalmente</option>
          </select>
    </div></td>
  </tr>  
  <tr>
    <td class="texto9"><div align="right"><strong>El Fraccionamiento Cuenta con</strong></div></td>
    <td class="texto9"><div align="right">Alumbrado P&uacute;blico
          <select name="alum" id="alum">
            <option value=0></option>
            <option value=1>Si</option>
            <option value=2>No</option>
            <option value=3>Ocasionalmente</option>
          </select>
    </div></td>
    <td class="texto9"><div align="right">Cordones y Banquetas
        <select name="banq" id="banq">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Acceso a transporte p&uacute;blico
          <select name="trans" id="trans">
            <option value=0></option>
            <option value=1>Si</option>
            <option value=2>No</option>
            <option value=3>Ocasionalmente</option>
          </select>
    </div></td>
  </tr>
  <tr>
    <td width="125" class="texto9"><div align="right"><strong>Su hogar tiene acceso cercano a: </strong></div></td>
    <td width="112" class="texto9"><div align="right">Escuelas 
        <select name="escuela" id="escuela">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td width="112" class="texto9"><div align="right">Clinicas y hospitales 
        <select name="hospital" id="hospital">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td width="112" class="texto9"><div align="right">Parque y Jardines
        <select name="parque" id="parque">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
  </tr>
  <tr>
    <td class="texto9">&nbsp;</td>
    <td class="texto9"><div align="right">Canchas deportivas
        <select name="cancha" id="cancha">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Salones de eventos o reuniones
        <select name="salon" id="salon">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Centros comerciales
        <select name="centros_com" id="centros_com">
          <option value=0></option>
          <option value=1>Si</option>
          <option value=2>No</option>
          <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
  </tr>
  <tr>
    <td class="texto9">&nbsp;</td>
    <td class="texto9"><div align="right">Cines
        <select name="cine" id="cine">
              <option value=0></option>
              <option value=1>Si</option>
              <option value=2>No</option>
              <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Centros de esparcimiento
        <select name="centro" id="centro">
              <option value=0></option>
              <option value=1>Si</option>
              <option value=2>No</option>
              <option value=3>Ocasionalmente</option>
        </select>
    </div></td>
    <td class="texto9"><div align="right">Fuentes de empleo
  cerca
          <select name="empleo" id="empleo">
            <option value=0></option>
            <option value=1>Si</option>
            <option value=2>No</option>
            <option value=3>Ocasionalmente</option>
          </select>
    </div></td>
  </tr>
</table>
<hr class="hrTitForma">
<table width="99%" border="0">
  <tr class="texto9">
    <td width="256"><div align="right"><strong>Antes de habitar su actual vivienda &iquest; Donde viv&iacute;a ? </strong></div></td>
    <td width="133">&nbsp;</td>
    <td width="300">&nbsp;</td>
    <td width="198">&nbsp;</td>
    <td>&nbsp;</td>
	<td>&nbsp;</td>
	
  </tr>
  <tr class="texto9">
    <td><div align="right">Zona Metropolitana de Mty. </div></td>
    <td colspan="3"><input type="checkbox" name="mty" id="mty"></td>
    <td width="91">&nbsp;</td>
    <td width="26">&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td><div align="right">En el interior del Edo. de N.L.</div></td>
    <td colspan="3"><input type="text" name="int_edo" id="int_edo" size="100"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td><div align="right">Fuera del Edo. de N.L.</div></td>
    <td colspan="3"><input type="text" name="fuera_edo" id="fuera_edo" size="100"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td><div align="right"><strong>Por que Raz&oacute;n decidi&oacute; cambiarse de vivienda </strong></div></td>
    <td><select name="razon" id="razon">
      <option value=0></option>
      <option value=1>Para no pagar renta</option>
      <option value=2>Hacinamiento</option>
      <option value=3>Zona de riesgo</option>
      <option value=4>Otro Escribir</option>
    </select></td>
    <td><div align="left">
      <input type="text" name="desc_razon" id="desc_razon" size="50">
    </div></td>
    <td colspan="2">Origen de la posesion de su lote 
      <select name="origen" id="origen">
        <option value=0></option>
        <option value=1>Invasi�n</option>
        <option value=2>Compra-Venta</option>
        <option value=3>Traspaso</option>
		<option value=4>Prestado/Rentado</option>
      </select></td>
	  <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td><div align="right"><strong>&iquest; Realiza alguna aportaci&oacute;n voluntaria ? </strong></div></td>
    <td>
    <input type="checkbox" name="aporta" id="aporta"></td>
    <td>de que tipo 
        <input type="text" name="tipo" id="tipo" size="35">    </td>
    <td>&iquest; Cuanto ? 
        <input name="cantidad" type="text" id="cantidad"  style="height:20px; text-align:right" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyPress="EvaluateText('%f', this);" onKeyUp="asignaFormatoSiesNumerico(this, event)" value="0" size="15" >
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto9">
    <td><div align="right"><strong>&iquest; Alg&uacute;n miembro de la familia es beneficiario de uno o m&aacute;s programas sociales ? </strong></div></td>
    <td><input type="checkbox" name="benef" id="benef"></td>
    <td colspan="3">&iquest; De cu&aacute;l o cu&aacute;les ? 
    <input type="text" name="programa" id="programa" size="75"></td>
    <td><input type="button" name="guarda" value="Guardar" onClick="guardar()"></td>
  </tr>
</table>

</body>
</html>
