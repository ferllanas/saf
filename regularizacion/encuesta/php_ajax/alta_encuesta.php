<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$enc = strtoupper($_REQUEST['enc']);
	$fracc = strtoupper($_REQUEST['fracc']);
	$est_jur = strtoupper($_REQUEST['est_jur']);
	$ubica = strtoupper($_REQUEST['ubica']);
	$cercado = $_REQUEST['cercado'];
	$baldio = $_REQUEST['baldio'];
	$deshab = $_REQUEST['deshab'];
	$habitado = $_REQUEST['habitado'];
	$lote_habita = $_REQUEST['lote_habita'];
	$lote_escrit = $_REQUEST['lote_escrit'];
	$hab = $_REQUEST['hab'];
	$nopago = $_REQUEST['nopago'];
	$aquien = $_REQUEST['aquien'];
	$costo = str_replace(',','',$_REQUEST['costo']);
	$eng = str_replace(',','',$_REQUEST['eng']);
	$mens = str_replace(',','',$_REQUEST['mens']);
	$plazo = $_REQUEST['plazo'];
	$adqui = $_REQUEST['adqui'];
	$area = str_replace(',','',$_REQUEST['area']);	
	$frente = str_replace(',','',$_REQUEST['frente']);	
	$fondo = str_replace(',','',$_REQUEST['fondo']);	
	$ext = str_replace(',','',$_REQUEST['ext']);	
	$ancho = str_replace(',','',$_REQUEST['ancho']);	
	$subdiv = $_REQUEST['subdiv'];
	$mts_div = str_replace(',','',$_REQUEST['mts_div']);	
	$no_pers = $_REQUEST['no_pers'];
	$no_fam = $_REQUEST['no_fam'];
	$jefe_fam = $_REQUEST['jefe_fam'];
	$trabajo = $_REQUEST['trabajo'];
	$infonavit = $_REQUEST['infonavit'];
	$ingmen = str_replace(',','',$_REQUEST['ingmen']);
	$ahorra = $_REQUEST['ahorra'];
	$techo1 = $_REQUEST['techo1'];
	$techo2 = $_REQUEST['techo2'];
	$techo3 = $_REQUEST['techo3'];
	$techo4 = $_REQUEST['techo4'];
	$pared1 = $_REQUEST['pared1'];
	$pared2 = $_REQUEST['pared2'];
	$pared3 = $_REQUEST['pared3'];
	$pared4 = $_REQUEST['pared4'];
	$piso1 = $_REQUEST['piso1'];
	$piso2 = $_REQUEST['piso2'];
	$piso3 = $_REQUEST['piso3'];
	$piso4 = $_REQUEST['piso4'];
	$no_cuartos = $_REQUEST['no_cuartos'];
	$agua = $_REQUEST['agua'];
	$pago_agua = str_replace(',','',$_REQUEST['pago_agua']);	
	$alum = $_REQUEST['alum'];
	$dren = $_REQUEST['dren'];
	$enelec = $_REQUEST['enelec'];
	$estac = $_REQUEST['estac'];
	$escuela = $_REQUEST['escuela'];
	$cancha = $_REQUEST['cancha'];
	$cine = $_REQUEST['cine'];
	$banq = $_REQUEST['banq'];
	$hospital = $_REQUEST['hospital'];
	$salon = $_REQUEST['salon'];
	$centro = $_REQUEST['centro'];
	$trans = $_REQUEST['trans'];
	$parque = $_REQUEST['parque'];
	$centros_com = $_REQUEST['centros_com'];
	$empleo = $_REQUEST['empleo'];
	$mty = strtoupper($_REQUEST['mty']);
	$int_edo = strtoupper($_REQUEST['int_edo']);
	$fuera_edo = strtoupper($_REQUEST['fuera_edo']);
	$razon = $_REQUEST['razon'];
	$desc_razon = strtoupper($_REQUEST['desc_razon']);
	$origen = $_REQUEST['origen'];
	$aporta = $_REQUEST['aporta'];
	$tipo = strtoupper($_REQUEST['tipo']);
	$cantidad = str_replace(',','',$_REQUEST['cantidad']);	
	$benef = $_REQUEST['benef'];
	$programa = strtoupper($_REQUEST['programa']);

if ($conexion)
{		
	$consulta = "insert into reguladencuesta (enc,fracc,est_jur,ubica,cercado,baldio,deshab,habitado,lote_habita,lote_escrit,";
	$consulta .= "hab,nopago,aquien,costo,eng,mens,plazo,adqui,frente,fondo,ext,ancho,subdiv,mts_div,no_pers,no_fam,";
	$consulta .= "jefe_fam2,trabajo,infonavit,ingmen,ahorra,techo1,techo2,techo3,techo4,pared1,pared2,pared3,pared4,piso1,";
	$consulta .= "piso2,piso3,piso4,no_cuartos,agua,drenaje,energiae,estaciona,alumbrado,escuela,cancha,cine,banqueta,hospital,salon,centro,";
	$consulta .= "transporte,parque,centros_com,empleo,mty,int_edo,fuera_edo,razon,desc_razon,origen,aporta,tipo,cantidad,";
	$consulta .= "benef,programa,pago_agua,area) ";
	$consulta .= "values ('$enc','$fracc','$est_jur','$ubica',$cercado,$baldio,$deshab,$habitado,$lote_habita,$lote_escrit,";
	$consulta .= "$hab,$nopago,$aquien,$costo,$eng,$mens,$plazo,$adqui,$frente,$fondo,$ext,$ancho,$subdiv,$mts_div,$no_pers,$no_fam,";
	$consulta .= "'$jefe_fam',$trabajo,$infonavit,$ingmen,$ahorra,$techo1,$techo2,$techo3,$techo4,$pared1,$pared2,$pared3,$pared4,$piso1,";
	$consulta .= "$piso2,$piso3,$piso4,$no_cuartos,$agua,$dren,$enelec,$estac,$alum,$escuela,$cancha,$cine,$banq,$hospital,$salon,$centro,";
	$consulta .= "$trans,$parque,$centros_com,$empleo,'$mty','$int_edo','$fuera_edo',$razon,'$desc_razon',$origen,$aporta,'$tipo',$cantidad,";
	$consulta .= "$benef,'$programa',$pago_agua,$area) ";
	$R = sqlsrv_query( $conexion,$consulta);
	if(!$R)
		echo "Error de conexion.".sqlsrv_errors($conexion)."<br>".$consulta;
}	
	header('Location: ../encuesta_reg.php');
	//echo $consulta;
?>