<?php
// Archivo de consultas de Solicitud de Cheques
//include_once 'lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$query=$_REQUEST['query'];
$fecini=substr($_REQUEST['query'],1,10);
$fecfin=substr($_REQUEST['query'],11,11);

$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
				$command= "SELECT  a.numeco, YEAR(a.fecha) as anio, MONTH(a.fecha) as mes,SUM(a.subtotal) as subtotal, SUM(a.iva) as iva,
		SUM(a.total) as total
  FROM [fomeadmin].[dbo].[egresosdsolchegas] a 
  LEFT JOIN [fomeadmin].[dbo].egresosmsolchegas b  ON a.folio=b.folio
  WHERE a.estatus<90000 AND b.estatus<9000 AND a.numeco=" . substr($_REQUEST['query'],1) . "
  group by numeco, YEAR(fecha), MONTH(fecha) ORDER BY   YEAR(fecha), MONTH(fecha)";
	}
	if($opcion==2)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
				

				$command= "select a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomprov, a.c_definitivo as concepto,a.importe, a.estatus from egresosmsolche a ";
				$command .="LEFT JOIN compramprovs b ON a.prov=b.prov WHERE nomprov LIKE '%" . $nom1 . "%' and tipo=3 ";
											if($cuenta>1)
											{
												$command .="and b.nomprov LIKE '%" . $nom2 . "%'";
											}
											$command .=" order by a.folio";
	}
	
	//echo $command;
	if($opcion==3)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
	
				$command= "select a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomdepto, a.c_definitivo as concepto,a.importe, a.estatus, c.nomprov from egresosmsolche a 
								LEFT join compramprovs c ON  a.prov=c.prov 
								LEFT join nomemp.dbo.nominamdepto b ON a.depto=b.depto 
								WHERE b.nomdepto LIKE '%" . substr($_REQUEST['query'],1) . "%' and tipo=3 order by a.folio";
	}		
	//	echo $opcion;
	if($opcion==4)
 	{
				$command= "select a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomprov, a.c_definitivo as concepto,a.importe, a.estatus from egresosmsolche a 
											INNER JOIN compramprovs b ON a.prov=b.prov WHERE a.concepto LIKE '%" . substr($_REQUEST['query'],1) . "%' and tipo=3 order
											by a.folio";

     }
	//echo "Antes de pasar al 5";
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!

				//$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				//$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				//echo $fini." _ ".$ffin;
				list($dd,$mm,$aa)= explode('/',$fecini);
				$fecini=$aa.'-'.$mm.'-'.$dd;
				
				list($dd2,$mm2,$aa2)= explode('/',$fecfin);
				$fecfin=$aa2.'-'.$mm2.'-'.$dd2;
				
				$command= "SELECT  a.numeco, YEAR(a.fecha) as anio, MONTH(a.fecha) as mes,SUM(a.subtotal) as subtotal, SUM(a.iva) as iva,
		SUM(a.total) as total
  FROM [fomeadmin].[dbo].[egresosdsolchegas] a 
  LEFT JOIN [fomeadmin].[dbo].egresosmsolchegas b  ON a.folio=b.folio
  WHERE a.estatus<90000 AND b.estatus<9000 AND  a.fecha >= CONVERT(varchar(12),'$fecini', 103) and a.fecha <=CONVERT(varchar(12),'$fecfin', 103)
  group by numeco, YEAR(fecha), MONTH(fecha) ORDER BY   YEAR(fecha), MONTH(fecha)";
				 
	}
	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				$xSubtotalTotal=0;
				$xIVA=0;
				$xTotal=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$datos[$i]['numeco']=trim($row['numeco']);
					$datos[$i]['anio']=trim($row['anio']);					
					$datos[$i]['mes']=$row['mes'];//utf8_decode(trim());
					//$datos[$i]['importe']=trim($row['importe']);
					$datos[$i]['subtotal'] = number_format(trim($row['subtotal']), 2);
					$datos[$i]['iva'] = number_format(trim($row['iva']), 2);
					$datos[$i]['total'] = number_format(trim($row['total']), 2);
					
					$xSubtotalTotal+=$row['subtotal'];
					$xIVA +=$row['iva'];
					$xTotal+=$row['total'];
					$i++;
				}
				
				$datos[$i]['numeco']="&nbsp;";
				$datos[$i]['anio']="";					
				$datos[$i]['mes']="Totales: ";//utf8_decode(trim());
				$datos[$i]['subtotal'] = number_format($xSubtotalTotal, 2);
				$datos[$i]['iva'] = number_format($xIVA, 2);
				$datos[$i]['total'] = number_format($xTotal, 2);
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>