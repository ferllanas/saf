<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$datos=array();
$usuario="1981";//$_COOKIE['ID_my_site'];
$id=0;
if(isset($_REQUEST['pid']))
{
	$id=$_REQUEST['pid'];
}
$nombre="";
if(isset($_POST['pnombre']))
{
	$nombre=$_POST['pnombre'];
}
$tipo="";
if(isset($_POST['ptipo']))
{
	$tipo=$_POST['ptipo'];
}
$grupo="";
if(isset($_POST['pgrupo']))
{
	$grupo=$_POST['pgrupo'];
}
$pestatus="";
if(isset($_POST['pestatus']))
{
	$pestatus=$_POST['pestatus'];
}
if($pestatus)
	$estatus=0;
else
	$estatus=90;
$preguntas="";//variable que almacena la cadena de los productos
	if(isset($_POST['ppreguntas']))
		$preguntas=$_POST['ppreguntas'];

list($ids, $fecha)= fun_encuesta_A_mencuesta($id,$estatus,$nombre,$tipo,$grupo,$usuario); 
if(strlen($ids)>0)//Para ver si en realidad se genero un servicio
{
	$fails=false;
	$msgerror="Encuesta Registrada Satisfactoriamente";
	for($i=0;$i<count($preguntas);$i++)
	{
		//Agrega producto a requisicion
		$fails=func_guardapreguntas($ids, 
								$preguntas[$i]['descpreg'], 
								$preguntas[$i]['ordenpreg'],
								$preguntas[$i]['tipopreg']);
	}
}
else
{	
	$fails=true; 
	$msgerror="No pudo generar la Encuesta";
}
	$datos['id']=trim($ids);
	$datos['fecha']=trim($fecha);
	$datos['fallo']=$fails;	
	$datos['error']=$msgerror;
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	
function fun_encuesta_A_mencuesta($id,$estatus,$nombre,$tipo,$grupo,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_encuesta_A_mencuesta(?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$nombre,&$tipo,&$grupo,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_encuesta_A_mencuesta".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];		
		}
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return array($id,$fecha);
}
function func_guardapreguntas($id,$descripcion,$orden,$tipo)
{
	global $server,$odbc_name,$username_db ,$password_db;
	$fails=false;
	$fecha = "";
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	//ECHO 
	$tsql_callSP ="{call sp_encuesta_A_pregunta(?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$descripcion,&$orden,&$tipo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_encuesta_A_pregunta".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return ;
}

?>
