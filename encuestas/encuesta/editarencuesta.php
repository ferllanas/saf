<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$titulo="Agregar Pregunta";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	$titulo="Editar Pregunta";
}


$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM encuestamencuesta where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$descripcion = trim($row['nombre']);
			$tipo = $row['tipo'];
			$grupo = $row['grupo'];
			$estatus=$row['estatus'];
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script type="text/javascript" src="../encuesta/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../encuesta/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../encuesta/javascript/encuesta.js"></script>

<!-- Load jQuery build -->
<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Encuesta</title>
</head>

<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Sección De Proyecto -------- -->
<table width="100%">
<tr>
            <td width="9%"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /></td>
    </tr> 
    <tr>
    	<td  class="titles">Nombre:</td>
        <td colspan="5"><input style="width:350px" type="text" name="nombre" id="nombre" value="<?php echo $descripcion;?>"/></td>
    </tr>
    <tr>
    	<td class="titles">Tipo:</td>
        <td colspan="5"><select name="tipo" id="tipo" size="1">
        	<option value="0">Seleccionar</option>
        	<option value="1" <?php if($tipo==1) echo "selected"; ?>>Anonima</option>
            <option value="2" <?php if($tipo==2) echo "selected"; ?>>Personal</option>		
            </select>
        </td>
    </tr>
    <tr>
    	<td class="titles">Grupo:</td>
        <td colspan="5"><select name="grupo" id="grupo" size="1">
        	<option value="0">Seleccionar</option>
        	<option value="1" <?php if($grupo==1) echo "selected"; ?>>Interna</option>
        	<option value="2" <?php if($grupo==1) echo "selected"; ?>>Externa</option>	
            </select>
        </td>
    </tr>
     	<tr><td colspan="5">&nbsp;</td></tr>
    <tr>
    	<td  class="titles">Pregunta:</td>
        <td colspan="10"  class="titles"><input style="width:350px" type="text" name="descpreg" id="descpreg" />
    	&nbsp; Orden: &nbsp;<input style="width:50px" maxlength="2" type="text" name="orden" id="orden" />&nbsp; Tipo:
        &nbsp;<select name="tipopreg" id="tipopreg" size="1">
        	<option value="0">Seleccionar</option>
        	<option value="1">Abierta</option>
        	<option value="2">Multiple</option>	
            </select>
        &nbsp;Valor:&nbsp;<input style="width:50px" maxlength="2" type="text" name="valor" id="valor" />
    	&nbsp;Obligatoria: &nbsp;<select name="obliga" id="obliga" size="1">
        	<option value="0">Seleccionar</option>
        	<option value="1">Si</option>
        	<option value="2">No</option>	
            </select>
        </td>
        <td width="10%">
        <input type="button" value="+" id="ad_colin2" name="ad_colin2" onClick="agrega_PreguntaEncuesta();" class="texto8">
							<input type="hidden" value="" name="treqvar" id="treqvar">
        </td>

    </tr>
    	<tr>
		<td colspan="6" align="center">
			<table border="1" id="tmenudusuario" name="tmenudusuario" width="100%">
				<tr>
					<th class="subtituloverde" width="50%">Pregunta</th>
					<th class="subtituloverde" width="10%">Orden</th>
					<th class="subtituloverde" width="10%">Tipo</th>
                    <th class="subtituloverde" width="10%">Valor</th>
                    <th class="subtituloverde" width="10%">Obligatoria</th>
					<th class="subtituloverde" width="10%">&nbsp;</th>
				</tr>
				<tbody id="menus">
					<!--<?php  if( $productosdeusuario!=null) for($j=0;$j<count($productosdeusuario);$j++){ ?>
						<tr id="filaOculta" >
						<td width="14%">
                          <input type="hidden" id="idprod" name="idprod"  value="">
							<input type="text" id="producto" name="producto"  value=""  class="caja_toprint"  style="width:100% " >
						</td>
						<td width="14%"><input type="text" id="desc" name="desc"  value="<?php echo $productosdeusuario[$j]['posicion'];?>"></td>
						<td width="14%"><input type="text" id="cantidad" name="cantidad"  value="<?php echo $productosdeusuario[$j]['posicion'];?>" ></td>
						
						<td width="14%">									   
							<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_ProductoARequisicion(event,this);" >
						</td width="25%"></tr>
					<?php }else{ ?>
					<div style="position:absolute ">
					<tr id="filaOculta" style="visibility:hidden; ">
					 
					  	<td><input type="hidden" id="idprod" name="idprod"  value="">
							<input type="text" id="producto" name="producto"  value=""  class="caja_toprint" style="width:100% " >
						</td>
						<td><input type="text" id="desc" name="desc"  value=""  ></td>
						<td><input type="text" id="cantidad" name="cantidad"  value=""  ></td>
						
						<td width="14%"><input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_ProductoARequisicion(event,this);" >
										  <input type="hidden" id="id_modulo" name="id_modulo" value="">
										  
						</td></tr></div>
					<?php } ?>-->
				</tbody>
			</table>
		</td>
	</tr>
    	<tr><td colspan="3">&nbsp;</td></tr>

        <tr>
    	<td class="titles">Situaci&oacute;n:</td>
        <td width="60%" class="texto10"><input type="checkbox" name="estatus" id="estatus" <?php if($estatus<90) echo "checked";?>>Activo</td>
    </tr>   
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Guardar" onclick="javascript: guardaencuesta()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='encuesta.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>