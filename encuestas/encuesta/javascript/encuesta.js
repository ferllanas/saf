function agrega_PreguntaEncuesta()
{	
	var descrip=document.getElementById('descpreg').value;
	var orden=document.getElementById('orden').value;
	var tipo=document.getElementById('tipopreg').value;
	var valor=document.getElementById('valor').value;
	var obliga=document.getElementById('obliga').value;
	
	//alert("!"+$('busProd').value+"!")
	if(descrip.length<=0)
	{
		alert('Ingresa la Descripción de la Pregunta de la encuesta.!');
		return false;
	}
	
	if(orden.length<=0)
	{
		alert('Ingresa el Orden de la Pregunta.!');
		return false;
	}
	if(tipo==0)
	{
		alert('Ingresa el tipo de la Pregunta.!');
		return false;
	}
	if(valor<=0)
	{
		alert('Ingresa el valor de la Pregunta.!');
		return false;
	}
	if(obliga==0)
	{
		alert('Seleccione si la Pregunta es Obligatoria.!');
		return false;
	}
	var Table = document.getElementById('menus');
	
	//Get reference to table body.
	//var TableBody = Table.firstChild;
	
	//Create the new elements
	var NewRow = document.createElement("tr");
	
	var celldescpreg = document.createElement("td");
	var cellorden = document.createElement("td");
	//var celltipo = document.createElement("td");
	var celldesctipo = document.createElement("td");
	var cellvalor = document.createElement("td");
	//var cellobliga = document.createElement("td");
	var celldescobliga = document.createElement("td");
	var cellQuitarCell = document.createElement("td");
	
	var inputdescpreg = document.createElement("input");
	inputdescpreg.type="text";
	inputdescpreg.id="descpreg";
	inputdescpreg.name="descpreg";
	inputdescpreg.readOnly = true;
	inputdescpreg.className="caja_toprint9";
	inputdescpreg.style.width="250px";
	inputdescpreg.style.align="center";
	inputdescpreg.value=descrip;
	
	var inputorden = document.createElement("input");
	inputorden.type="text";
	inputorden.id="orden";
	inputorden.readOnly = false;
	inputorden.name="orden";
	inputorden.className="texto10";
	inputorden.style.width="100%";
	inputorden.value=orden;
	
	var inputTipo = document.createElement("input");
	inputTipo.type="text";
	inputTipo.id="tipo";
	inputTipo.readOnly = true;
	inputTipo.name="tipo";
	inputTipo.className ="caja_toprint9";
	inputTipo.style.width="98%";
	inputTipo.style.align="center";
	inputTipo.value = tipo;
	if(tipo==1)
	{
		var inputdescTipo = document.createElement("input");
		inputdescTipo.type="text";
		inputdescTipo.id="desctipo";
		inputdescTipo.name="desctipo";
		inputdescTipo.className ="caja_toprint9";
		inputdescTipo.style.width="98%";
		inputdescTipo.style.align="center";
		inputdescTipo.value = "Abierta";
	}
	else
	{
		var inputdescTipo = document.createElement("input");
		inputdescTipo.type="text";
		inputdescTipo.id="desctipo";
		inputdescTipo.name="desctipo";
		inputdescTipo.className ="caja_toprint9";
		inputdescTipo.style.width="98%";
		inputdescTipo.style.align="center";
		inputdescTipo.value = "Multiple";
	}
	var inputvalor = document.createElement("input");
	inputvalor.type="text";
	inputvalor.id="valor";
	inputvalor.readOnly = true;
	inputvalor.name="valor";
	inputvalor.className ="texto9";
	inputvalor.style.width="98%";
	inputvalor.style.align="center";
	inputvalor.value = valor;

	var inputobliga = document.createElement("input");
	inputobliga.type="text";
	inputobliga.id="obliga";
	inputobliga.name="obliga";
	inputobliga.className ="texto9";
	inputobliga.style.width="98%";
	inputobliga.style.align="center";
	inputobliga.value = obliga;
	
	if(obliga==1)
	{
		var inputdescobliga = document.createElement("input");
		inputdescobliga.type="text";
		inputdescobliga.id="descobliga";
		inputdescobliga.name="descobliga";
		inputdescobliga.className ="caja_toprint9";
		inputdescobliga.style.width="98%";
		inputdescobliga.style.align="center";
		inputdescobliga.value = "Si";
	}
	else
	{
		var inputdescobliga = document.createElement("input");
		inputdescobliga.type="text";
		inputdescobliga.id="descobliga";
		inputdescobliga.name="descobliga";
		inputdescobliga.className ="caja_toprint9";
		inputdescobliga.style.width="98%";
		inputdescobliga.style.align="center";
		inputdescobliga.value = "No";
	}
	var inputbotonBorrar = document.createElement("input");
	inputbotonBorrar.type="button";
	inputbotonBorrar.style.width="10px";
	inputbotonBorrar.style.align="center";
	inputbotonBorrar.setAttribute("onclick", "javascript:eliminar_PreguntaEncuesta(event,this);");
	inputbotonBorrar.value="-";
	
	//Add textboxes to cells
	celldescpreg.appendChild(inputdescpreg);
	cellorden.appendChild(inputorden);
	//celltipo.appendChild(inputTipo);
	celldesctipo.appendChild(inputdescTipo);
	cellvalor.appendChild(inputvalor);
	//cellobliga.appendChild(inputobliga);
	celldescobliga.appendChild(inputdescobliga);
	//cellCantidadProd.appendChild(inputCantidadProd);
	cellQuitarCell.appendChild(inputbotonBorrar);
	
	//Add elements to row.
	NewRow.appendChild(celldescpreg);
	NewRow.appendChild(cellorden);
	NewRow.appendChild(celldesctipo);
//	NewRow.appendChild(celltipo);
	NewRow.appendChild(cellvalor);
	//NewRow.appendChild(cellobliga);
	NewRow.appendChild(celldescobliga);
	
	//NewRow.appendChild(cellCantidadProd);
	NewRow.appendChild(cellQuitarCell);
	
	//Add row to table
	Table.appendChild(NewRow);

	 document.getElementById('descpreg').value="";
	 document.getElementById('orden').value="";
	 document.getElementById('tipopreg').value=0;
	 document.getElementById('valor').value=0;
	 document.getElementById('obliga').value=0;
	//$('can_ent').value="";
	
	//document.getElementById('tiporeq').readOnly=true;
	//var idtipoRequiSelected = document.getElementById('tiporeq').selectedIndex;//document.getElementById('tiporeq').options[].value;
	//document.getElementById('treqvar').value = idtipoRequiSelected;
	
}
function eliminar_PreguntaEncuesta( e , s )
{
	//var table=$( 'tmenus' );
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	//alert(rowindexA+","+ $('menus').rows.length);
	document.getElementById('menus').deleteRow( rowindexA );
	
	if(document.getElementById('menus').rows.length<=0)
	{
		//document.getElementById('tiporeq').readOnly=false;
	 	document.getElementById('treqvar').value ="";

	}
}

function editarcatservicios()
{
	var id = document.getElementById("id").value;
	location.href="editarcatservicios.php?id="+id;
}

function guardaencuesta()
{
	var id="0";
	//id= $('#id').val();
	var nombre=document.getElementById('nombre').value;
	if(nombre.length<=0)
	{
		alert("Favor el Nombre de la Encuesta.");
		return false;
	}
	var tipo=document.getElementById('tipo').value;
	if(tipo.length<=0)
	{
		alert("Favor de ingresar el Tipo de Encuesta.");
		return false;
	}
	var grupo=document.getElementById('grupo').value;
	if(grupo.length<=0)
	{
		alert("Favor de ingresar el Grupo al que va dirigida la Encuesta.");
		return false;
	}	
	var estatus="";
	estatus= $('#estatus').val();
	if(estatus.length<=0)
	{
		alert("Favor de ingresar la Situacion de la Encuesta.");
		return false;
	}	
	
	var table=document.getElementById('menus');
	var Preguntas= new Array( );
	var countPreguntas=0;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="descpreg")
				{
					pregunta=mynode.value;
				}
					
													
				if(mynode.name=="orden")
				{
					if(mynode.value.length>0)
					{
						ordenpreg=mynode.value;
						if(parseFloat(orden)<=0)
						{
							alert("Existe una pregunta sin especificar el orden. Favor ingresar el orden.");
							return false;
						}
					}
					else
					{
						alert("Existe una pregunta sin especificar el orden. Favor ingresar el orden.");
						return false;
					}
				}
			
				
				if(mynode.name=="tipo")
				{
					tipopreg=mynode.value;
				}
				if(mynode.name=="valor")
				{
					valorpreg=mynode.value;
				}
				if(mynode.name=="obliga")
				{
					pregobliga=mynode.value;
				}
				
			}
		}
		
		
		Preguntas[countPreguntas] = new Object;
		Preguntas[countPreguntas]['descpreg']= pregunta;
		Preguntas[countPreguntas]['ordenpreg']= ordenpreg;			
		Preguntas[countPreguntas]['tipopreg']= tipopreg;
		countPreguntas++;
		
	}
	

	
	//Verifica que halla agregado productos a la requisicion
	if(countPreguntas<=0)
	{
		alert('Favor de agregar al menos una pregunta.');
		return false;
	}
	//alert(id);
	var datas = {
		 pid: id,
		 pnombre: nombre,
		 ptipo:tipo,
		 pgrupo:grupo,
		 pestatus: estatus,
		 ppreguntas: Preguntas
		 };
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'guardarencuesta.php',
						//dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(arguments);
						 alert ("Can't do because: " + error );
						},
						success: function(resp)
						{
							var myObj = resp;
        		            ///NOT how to print the result and decode in html or php///
		                    console.log(myObj);
							//
							if(resp.error!=0)
							{
								alert(resp.error);
								if(resp.id!=0)
									location.href="encuesta.php";
							}
							
						}
					
					});
}