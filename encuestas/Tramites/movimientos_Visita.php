<?php

require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
		
	$usuario = $_COOKIE['ID_my_site'];	
	
	$idvisita="";
	if(isset($_REQUEST['idvisita']))	
		$idvisita=$_REQUEST['idvisita'];
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
		<!--<link rel="stylesheet" type="text/css" href="body.css">-->
		<script language="javascript" src="../javascript/prototype.js"></script>
		<script language="javascript" src="../javascript/funciones_visitas.js"></script>
		<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
		<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
		<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<title>Historial de Visitas</title>
</head>

<body>
 <table widtd="90%">
              <tr>
					<td align="center" colspan="12" class="TituloDForma">Visitas Anteriores<hr class="hrTitForma"></td>
              </tr> 			
			 <tr>
			 	<td width='6%' align='center' class='texto8'>Folio</td>
				<td width='10%' align='center' class='texto8'>Fecha</td>
				<td width='10%' align='center' class='texto8'>Estatus</td>
				<td width='30%' align='left' class='texto8'>Tramite</td>
				<td width='30%' align='left' class='texto8'>Observaciones</td>
			</tr>
				
			  <?php
			  

			  
			  $consulta2="SELECT a.mov as idregistro, CONVERT(varchar(50),a.fecha,103) as fecham,convert(varchar(12),a.fecha,103) as fecha,
							a.idtramite , b.nomtramite, a.observa, c.nomestatus, a.hora
							  FROM Movimientos a 
							  left join cattramite b on b.idtramite=a.idtramite
							  left join CatEstatusVisita c on a.estatus=c.estatus
							   WHERE idregistro=$idvisita ORDER BY mov ";//AND c.nomtramite IS NOT Null
			// echo $consulta2;
			  $getProducts2 = sqlsrv_query( $conexion,$consulta2);
			  $nomtram="";
			  while( $row = sqlsrv_fetch_array( $getProducts2, SQLSRV_FETCH_ASSOC))
				{
					//print_r($row);
					$folio =  $row['idregistro'];//$row->ididvisita;
					$fecha =  $row['fecham']." ".$row['hora'];//substr($row->fecha,0,10);
					$mov =  $row['nomestatus'];//$row->mov;
					$observa =  trim($row['observa']);//$row->observa;
					
					if(trim($row['nomtramite'])!="")
						$nomtram = utf8_decode(trim($row['nomtramite']));//$row->nomtramite;
					else
						$nomtram = "";
						
					echo "<tr>";
					if($usuario=='mgonzalez')
					{
				?>
						<td width='6%' align='center' class='texto8'><!--<a href='edita_visitabrigadas.php?idvisita=<?php echo $idvisita;?>' title='click para ver/editar Tramite'></a>--<?php echo $folio;?></td>
				<?php
					}
					else
					{
					?>
                    	<td width='6%' align='center' class='texto8'><!--<a href='../mod_visita.php?idfolio=<?php echo $idvisita;?>' title='click para Editar Visita '></a>--><?php echo $folio;?></td>
					<?php
					}
					?>
					<td width='10%' align='center' class='texto8'><?php echo $fecha;?></td>
					<td width='10%' align='center' class='texto8'><?php echo $mov;?></td>			
					<td width='30%' align='left' class='texto8'><?php echo $nomtram;?></td>
					<td width='30%' align='left' class='texto8'><?php echo $observa;?></td>
					</tr>	
			<?php
				} 
			  ?>
	</table>
</body>
</html>