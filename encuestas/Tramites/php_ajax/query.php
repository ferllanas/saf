<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo $_GET['q'];
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	//echo $terminos;//=$_GET['q'];
	$command= "SELECT idtramite, nomtramite, idarea  FROM catTramite";
	
	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if(!$paso)
		{
			$command.=" WHERE nomtramite LIKE '%".$terminos[$i]."%'";
			$paso=true;	
		}
		else
			$command.=" AND desctramite LIKE '%".$terminos[$i]."%'";	
	}
	
	$command.=" and estatus<'90' ORDER BY desctramite ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{			
			$sct.= mb_convert_encoding( trim($row['nomtramite']),"ISO-8859-1", "UTF-8").";".trim($row['idtramite']).";".trim($row['idarea']). "\n";
			$i++;
		}
	}
	echo $sct;

}
?>