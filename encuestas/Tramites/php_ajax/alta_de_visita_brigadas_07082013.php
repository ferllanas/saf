<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	
	$conexion = sqlsrv_connect($server,$infoconexion);


$usuario=$_COOKIE['ID_my_site'];

$email = "";
$responsable = "";
					
//include 'evaluser.php';
$usuariolog=$_REQUEST['usuariolog'];	//id usuario que levanta la visita
$idAreaDeUsuarioLog="";					//Area a la que pertenece el usuario logeado
$tipoVisita=$_REQUEST['tipoVisita'];	//id del tipo de visita
$idtramite = $_REQUEST['idtramite']; 	//id del tramite
$idResponsableDeArea ="";							//id del reponsable del area
$emailCopias= $_REQUEST['emailCopias'];

$id_TramiteXVisita = "";
//echo $emailCopias;


$nomAreaDeTramite = $_REQUEST['nomarea'];	//Nombre del area que pertenece el tramite
//echo "melleva ".$nomAreaDeTramite;
$idarea = $_REQUEST['idarea'];			//id del area al que pertenece el tramite

$contacto = $_REQUEST['contacto'];		//id del Cliente u contacto
$visitante="";							//Nombre del Contacto

$observacionDeVisita = $_REQUEST['observ'];		//Observaciones de la vista
$estatus= $_REQUEST['estatus'];			//Estatus de la visita
$enviaremail=false;							//Ve�rifica si debe enviar el email
if(isset($_REQUEST['enviaremail']))
	$enviaremail= $_REQUEST['enviaremail'];

$visita = $_REQUEST['visita'];	// visista pero este siempre llega vacio tambien
$idfolio=$_REQUEST['idfolio']; //Este creo que nunca se llena

$enviaremailAResponsable=false;
if(isset($_POST['enviaremailAResponsable']))
	$enviaremailAResponsable= $_POST['enviaremailAResponsable'];

//if(!$enviaremailAResponsable)
	//echo "<br>SSSSSS<br>";

if(isset($_REQUEST['visitante']))
	$visitante= $_REQUEST['visitante'];

if(isset($_COOKIE['areaActual']))
	$idAreaDeUsuarioLog = $_COOKIE['areaActual'];
	
$fecha=date("Y/m/d");
if(isset($_REQUEST['fin']))
{
	list($d,$m,$a) = explode("/", $_REQUEST['fin']);
	$fecha=$a."/".$m."/".$d;
}
	
if(isset($_COOKIE['Responsable']))
	$idResponsableDeArea = str_pad($_COOKIE['Responsable'],6,'0',STR_PAD_LEFT);	
		
$idRegistroVisitas=0;

$fails=false;

	if ($nomAreaDeTramite == "" | $idtramite == "" | $observacionDeVisita == ""  ) 
	{
		exit("No se llenaron campos necesarios");
	}
	else
	{
		if ($conexion)
		{ 			
		
			$tsql_callSP ="{call sp_Visitas_altaValida(?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
			$params = array(&$usuario, &$contacto, &$estatus, &$tipoVisita, &$idtramite, &$observacionDeVisita,0,&$fecha);//Arma parametros de entrada
			$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
			
			$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
			if( $stmt === false )
			{
				 $fails= "Error in statement execution.\n";
				 $fails=true;
				 die( print_r( sqlsrv_errors(), true)." sp_Visitas_alta ".print_r($params,true));
			}
			
			//echo " sp_Visitas_alta ".print_r($params,true);
			if(!$fails)
			{
				// Retrieve and display the first result. 
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
				{
					$idRegistroVisitas = trim($row['folioVisita']);
					$idAreaDeUsuarioLog = trim($row['areaDUsuario']);
					//$nomdepto = $row['numempResponsable'];
					$visita = trim($row['idvista']);
					$idarea = trim($row['idAreaDestino']);
					$nomdir = trim($row['idResponsableAreaDestino']);
					$email = trim($row['emailEnvio']);
					$responsable = trim($row['responsable']);
					$id_TramiteXVisita = trim($row['id_TramiteXVisita']);
					
				}
				sqlsrv_free_stmt( $stmt);
			}
			
			if($enviaremail)
			{
				//echo "PASO enviar email";
				//echo "<br>".$idarea.", ".$observacionDeVisita.", ".$idRegistroVisitas.", ".$usuariolog.", ". ''.", ". $visitante.", ". $email.", ". $responsable.", ".$emailCopias.", ".$enviaremailAResponsable."<br>";
				enviaremail($idarea,$observacionDeVisita, $idRegistroVisitas, $usuariolog, '', $visitante, $email , $responsable,$emailCopias,$enviaremailAResponsable);
			}
				
			//echo "A<br>";
			//Yo digo que aqui nunca va a pasar pero vamos a dejarlo para ver que pasa
			/*if(strlen($idfolio)>0)
			{
				
				
				$consulta = "UPDATE visitas SET idareadestino='0100', fecha=GETDATE(),hora=GETDATE(),estatus='0500' WHERE idregistro=$idfolio ";
				sqlsrv_query( $conexion,$consulta);
				
				$consulta = "UPDATE movimientos SET festatus=GETDATE(), fecha=GETDATE(), hora=GETDATE(), idarea='0100', idareadestino='0100',
								observa='Modificado se envio a las brigadas', estatus='0500',prog='0044'	WHERE idregistro='$idfolio'";
								
				sqlsrv_query( $conexion,$consulta);
						
				
			}
			//echo "B<br>";
			// Obtiene el ultimo Registro de Visitas
			$mov=0;
			$consulta = "select max(mov) as maxmov from movimientos where idvisita=$visita and idcontacto=$contacto";
			$getProducts=sqlsrv_query( $conexion,$consulta);
			
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$mov = $row['maxmov'];									
			}
			if($mov==0)
			{
				$wultmov=1;
				$wmov=1;
			}
			else
			{
				$wultmov=$mov;
				$wmov=$mov + 1;
			}
			//echo "C<br>";
			// consecutivo de registro
			$consulta = "select max(idregistro) as mregistro from visitas";
			$getProducts=sqlsrv_query( $conexion,$consulta);
			
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$idRegistroVisitas = $row['mregistro'];									
			}
			
			if($idRegistroVisitas ==0)
			{
				$idRegistroVisitas=1;
			}
			else
			{
				$idRegistroVisitas=$idRegistroVisitas + 1;
			}		
			
			//echo "D<br>";
			
			
			//echo "F<br>";
			$consulta = "insert into visitas (idregistro, idcontacto, idvisita,mov, idareadestino, fecha, hora, estatus, festatus,tipo)
							values ($idRegistroVisitas, $contacto, $visita ,$wmov ,'$idarea' ,GETDATE() ,CONVERT(varchar(10),GETDATE(),108) 
							,'$estatus' ,GETDATE() ,$tipoVisita)";		
			//echo $consulta;	
			sqlsrv_query( $conexion,$consulta);
			$consulta = "insert into movimientos (idregistro, idcontacto, idvisita, mov, idarea, idtramite, idresp, fecha, hora,
												 idareadestino, fechaenvio, horaenvio, observa, estatus, festatus) 
								values ($idRegistroVisitas, $contacto, $visita, $wmov, '$idAreaDeUsuarioLog', '$idtramite', '$idResponsableDeArea', 
										GETDATE(), CONVERT(varchar(10),GETDATE(),108), 			
										'$idarea', GETDATE(), CONVERT(varchar(10),GETDATE(),108), '$observacionDeVisita', '$estatus', 
										GETDATE())";*/	
			//echo $consulta;	
								
			//No existe una tabla y para que se utiliza?
			
			
			//sqlsrv_query( $conexion,$consulta);
			/*$consulta = "insert into visita_contacto (idregistro,idcontacto,orden,estatus)
						values ($idRegistroVisitas,$contacto,0,'$estatus')";		
			//echo $consulta;	
			sqlsrv_query( $conexion,$consulta);
			*/

			$mensajeAcciones="";
			if(count($_FILES['uploadFiles']['name'])>0) 
			{
				//obtenemos todos los archivos uno por uno
				
				for($i=0; $i<count($_FILES['uploadFiles']['name']);$i++)
				{
               		$Ext = pathinfo($_FILES['uploadFiles']['name'][$i], PATHINFO_EXTENSION); 
					$Max = $_FILES['uploadFiles']['size'][$i];
					$tmp_name = $_FILES['uploadFiles']['tmp_name'][$i];
					if($tmp_name=="")
					{
						//echo $tmp_name." No trae nada";
						continue; 
					}
					//else
						//echo "<br>".$tmp_name;
						
					$path_parts = $tmp_name;//pathinfo("../".$docdigitales[$i]['path']);
					$fextension =  $Ext;
						
					//Carpeta del nuevo archivo
					$dirNewFile= "../../../".$imagenes_folder."".$contacto;
					$dirNewFile = substr( $dirNewFile,3,strlen($dirNewFile)-3);
					
					
					//echo $dirNewFile;//."<br>".$imagenes_folder."<br>".$path_parts;
					if(!file_exists($dirNewFile))
						if(!mkdir ( $dirNewFile, 0700 ))
						{
							$datos[$i]['error']="01";
							$datos[$i]['string']="Error no pudo crearse la carpeta $dirNewFile .";
							//echo $datos[$i]['string'];
							//exit($mensajeAcciones.="Error no pudo crearse la carpeta $dirNewFile .");
						}
						
					$tipoVisitaDoc=$_REQUEST["documento"][$i];
					$observaciones= $_REQUEST["descripcionFile"][$i];
					//Arma la ruta o path del nuevo archivo
					$newfile = $dirNewFile."/".$contacto."_".$tipoVisitaDoc.".".$fextension;
					//Path del archivo para guardar en la base de datos
					$pathtoDB = "/".$contacto."/".$contacto."_".$tipoVisitaDoc.".".$fextension;
					
					if(!file_exists($newfile)) //Si no existe el archivo previamente
					{
						if(move_uploaded_file($tmp_name, $newfile))//if(rename("../".$docdigitales[$i]['path'], $newfile))
							$mensajeAcciones.="<br>El documento ".$tipoVisitaDoc." a sido enviado exitosamente.";
						else
							$mensajeAcciones.="<br>El documento ".$tipoVisitaDoc." tuvo errores al tratar de enviarlo.";
						
						//echo $mensajeAcciones;
							
					}
					else
					{
						if(move_uploaded_file($tmp_name, $newfile))//if(rename("../".$docdigitales[$i]['path'], $newfile))
							$mensajeAcciones.="<br>El documento ".$tipoVisitaDoc." a sido enviado exitosamente.";
						else
							$mensajeAcciones.="<br>El documento ".$tipoVisitaDoc." tuvo errores al tratar de enviarlo.";
						
						//echo $mensajeAcciones;
					}
					
					$consulta = "insert into contdoc(nts,folioese, fml, iddoc,  nomarch, nomreg, feccap,estatus,observaciones, idcontacto, idVisita) 
							values('','','','".$tipoVisitaDoc."','$pathtoDB','',CONVERT(char(8),GETDATE(),112),'0000','$observaciones', $contacto, $idRegistroVisitas);  
							SELECT SCOPE_IDENTITY()  as id";
					
					//echo "<br>".$consulta."<br>";
					$s = sqlsrv_prepare( $conexion, $consulta);
					if (sqlsrv_execute($s) === false)
					{ 
						//Hubo errores dirante la  consulta
						$resoponsecode="02";
						$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
						
						$datos[0]['error']=$resoponsecode;
						$datos[0]['string']=$descriptioncode;
						
						//echo "<br>".$datos[0]['string'];
					}
					else
					{
						sqlsrv_next_result($s);
						$r = sqlsrv_fetch_array($s, SQLSRV_FETCH_ASSOC);
						$datos[$i]['string']="".count($s);
						$datos[$i]['idef']=$r['id'];
						$datos[$i]['error']="0";
						
						//print_r($datos);
					}
	
					$newrch = $dirNewFile."/".$contacto."_".$tipoVisitaDoc."_".$datos[$i]['idef'].".".$fextension;
					$pathtoDB = "/".$contacto."/".$contacto."_".$tipoVisitaDoc."_".$datos[$i]['idef'].".".$fextension;
					if(rename ( $newfile , $newrch ))
					{
						$consulta = "UPDATE contdoc SET nomarch = '$pathtoDB' WHERE id=".$datos[$i]['idef'];
						//echo $consulta;
						$s = sqlsrv_prepare( $conexion, $consulta);
						sqlsrv_execute($s);
					}
   
					
           		 }
			}
       

			//echo $consulta;
			echo "<span class='texto8' scope='row'>La visita $visita fue agregada satisfactoriamente. <br>Le fue asignado el folio $idRegistroVisitas!!. Se ha generado un Folio para el tramite seleccionado $id_TramiteXVisita.</span>";
			echo "<br><br>";
			echo "<br>";
			echo "<br>";
			echo "<br>";
			echo "<br><br><a href='../visita_brigadas.php?idcontacto=$contacto#top' target='centro'>Agregar nueva visita a este usuario</a> <a href='../busca_visitante.php' target='centro'>Buscar otra persona.</a>  <a href='../../agregavisitantes.php?idvisita=$id_TramiteXVisita' target='centro'>Agregar Visitante a Visita.</a>";

			//include ('busca_visitante.php');
		}
		else
		{ 
			exit ("Falló la conexión de base de datos") ;
	    }
		sqlsrv_close($conexion);
    }

		//enviaremail($area,$observaciones, $numvisita, $usuariolog, $nomareaenvia, $nombre)
function enviaremail($idarea,$observaciones, $numvisita, $usuariolog, $nomAreaDeTramite, $nombre, $email , $responsable, $emailCopias, $enviaremailAResponsable)
{
	global $server,$infoconexion;
	
	include("../../../PHPMailer_v5.1/class.phpmailer.php");
	include("../../../PHPMailer_v5.1/class.smtp.php"); // note, this is optional - gets called from main class if not already loaded

$emails= array('fernando.llanas@fomerrey.gob.mx');


$conexion = sqlsrv_connect($server,$infoconexion);

//Obtiene area que envia
//echo "!".$nomAreaDeTramite."!";
if($nomAreaDeTramite=="")
{
	$consulta = "SELECT d.nomdepto, c.email, c.email_fome ,c.Nombre 
					FROM nomemp.dbo.nominadempleados b 
					INNER JOIN fomedbe.dbo.usuarios c ON b.numemp=c.numemp COLLATE Traditional_Spanish_CI_AS
					INNER JOIN nomemp.dbo.nominamdeptoNew d ON d.depto=b.depto
						WHERE c.usuario ='".$_COOKIE['ID_my_site']."'";
	//echo $consulta;
	$stmt = sqlsrv_query($conexion, $consulta);
	if( $stmt === false )
	{
		 die( print_r( sqlsrv_errors(), true));
	}	
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))
		{
			$nomAreaDeTramite=trim($row['nomdepto']);
		}
	}
}


$envioVarios=false;
if($email==""  && $responsable=="" && $enviaremailAResponsable)
{
	$tsql_callSP = "SELECT c.email, c.email_fome ,c.Nombre FROM nomemp.dbo.nominadempleados b INNER JOIN fomedbe.dbo.usuarios c ON b.numemp=c.numemp COLLATE Traditional_Spanish_CI_AS
		WHERE b.depto =$idarea AND c.status='S'";
	//echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP);
	if( $stmt === false )
	{
		 die( print_r( sqlsrv_errors(), true));
	}	
	$envioVarios=true;
}

$envioSolocopias=false;
if( strlen($emailCopias)>0 && !$enviaremailAResponsable)
{
	//echo "Conmadre";
	$envioSolocopias=true;
}

$fails=false;


$body             = /*file_get_contents('invitacion.php?condPagoDias=30&adqparcial=si&requisi=$requisi');$mail->getFile('contents.html');"/*/
"
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
</head>
<body>
<table width='100%'>
	<tr>
    	<td><H1 align='center'>Tecnologia de la informaci&oacute;n</H1></td>
    </tr>
    <tr>
    	<td><h3 align='center'>Modulo de Atenci&oacute;n a clientes</h3></td>
    </tr>
    <tr>
      <td>Usted tiene un tramite pendiente con n&uacute;mero &quot;$numvisita&quot; de $nombre.<br> A continuaci&oacute;n las observaciones/indicaciones del Tramite:			     </td>
    </tr>
    <tr>
    	<td>$observaciones</td>
    </tr>
     <tr>
    	<td>&nbsp;</td>
    </tr>
     <tr>
    	<td>&nbsp;</td>
    </tr>
	<tr>
		<td>Atte.  $usuariolog</td>
	<tr>
    	<td>$nomAreaDeTramite</td>
    </tr></td>
	</tr>
    <tr>
    	<td bgcolor='#FFCC00' style='font-size:11px'>Usted puede atender esta solicitud ingresando <a href='http://200.94.201.196/fome/'>al SIIF en linea</a>. En el modulo de Visitantes, en la opci&oacute;n &quot;Buscar Visitante&quot; o &quot;Control de Visitas&quot;.</td>
    </tr>
    <tr>
    	<td bgcolor='#FFCC99' style='font-size:11px'>Si usted no tiene usuario o contraseña para ingresar al sistema favor de solicitarlo al area de Informatica.</td>
    </tr>
</table>
</body>
</html>
";
	//$body             = eregi_replace("[\]",'',$body);
	$body =str_replace("[\]",'',$body);

	
	$emailsenviado="";
	//$mail->AddAttachment("/path/to/file.zip");             // attachment
	//$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment
	$mail             = new PHPMailer();
	$numcuenta=0;
	$idssList="";
	//for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) 
	//{
		$i=1;
		if($envioVarios)
		{
			while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))//for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) 
			{
				
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
				$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 2525;//465;// set the SMTP port
				
				$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
														//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
				$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
				
				$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
				$mail->FromName   = "Tecnologia de la Informacion";
				$mail->Subject    = "Tecnologia de la Informacion";
				$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
				$mail->WordWrap   = 50; // set word wrap
				
				$mail->MsgHTML($body);
				
				$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
				//"First Last");
				//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
				$mail->IsHTML(true); // send as HTML
				$mail->AddAddress(trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
				$emailsenviado .= trim($row['email_fome'])." XXX<br>";
				//echo $emailsenviado;
				$word = split(';',$emailCopias);
				for($j=0;$j<count($word);$j++)
				{	
					//echo "<br>ZZZ: ".$word[$j];
					$mail->AddCC($word[$j]);
				}
				//$idssList.= trim($row['id']);
				if(!$mail->Send()) 
				{
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} 
				
				$mail  = new PHPMailer();
				
				$numcuenta++;
				if($numcuenta>=count($emails))
				{
					$numcuenta=0;
				}
				
				$i++;
			}
		}
		else
		{
			if($envioSolocopias)
			{
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
				$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 2525;//465;// set the SMTP port
				
				$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
														//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
				$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
				
				$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
				$mail->FromName   = "Tecnologia de la Informacion";
				$mail->Subject    = "Tecnologia de la Informacion";
				$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
				$mail->WordWrap   = 50; // set word wrap
				
				$mail->MsgHTML($body);
				
				$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
				//"First Last");
				//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
				$mail->IsHTML(true); // send as HTML
				//$mail->AddAddress($email, $responsable);//trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
				
				$word = split(';',$emailCopias);
				for($j=0;$j<count($word);$j++)
				{	
					if($j==0)
					{
						if(strlen($word[$j])>0)
						{
							$mail->AddAddress($word[$j], $word[$j]);
							//$mail->AddCC($word[$j]);
						}
					}
					else
					{
						if(strlen($word[$j])>0)
						{
							//echo "<br>JJJ".$word[$j];
							$mail->AddCC($word[$j]);
						}
					}
				}
				
				//$emailsenviado .= trim($email)." XXX<br>";
				//echo $emailsenviado;
				//$idssList.= trim($row['id']);
				if(!$mail->Send()) 
				{
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} 
				
				$mail  = new PHPMailer();
				
				$numcuenta++;
				if($numcuenta>=count($emails))
				{
					$numcuenta=0;
				}
			}
			else
			{
				if($enviaremailAResponsable)
				{
					$mail->IsSMTP();
					$mail->SMTPAuth   = true;                  // enable SMTP authentication
					//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
					$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
					$mail->Port       = 2525;//465;// set the SMTP port
					
					$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
															//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
					$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
					
					$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
					$mail->FromName   = "Tecnologia de la Informacion";
					$mail->Subject    = "Tecnologia de la Informacion";
					$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
					$mail->WordWrap   = 50; // set word wrap
					
					$mail->MsgHTML($body);
					
					$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
					//"First Last");
					//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
					$mail->IsHTML(true); // send as HTML
					$mail->AddAddress($email, $responsable);//trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
					
					$word = split(';',$emailCopias);
					for($j=0;$j<count($word);$j++)
					{	
						
						if(strlen($word[$j])>0)
						{
							//echo "<br>JJJ".$word[$j];
							$mail->AddCC($word[$j]);
						}
					}
					
					$emailsenviado .= trim($email)." XXX<br>";
					//echo $emailsenviado;
					//$idssList.= trim($row['id']);
					if(!$mail->Send()) 
					{
					  echo "Mailer Error: " . $mail->ErrorInfo;
					} 
					
					$mail  = new PHPMailer();
					
					$numcuenta++;
					if($numcuenta>=count($emails))
					{
						$numcuenta=0;
					}
				}
			}
		}
}
?>
