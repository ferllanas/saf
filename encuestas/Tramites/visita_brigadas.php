<?php
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	$usuariolog ="";
	if(isset($_COOKIE['Nombre']))
		$usuariolog = $_COOKIE['Nombre'];

	$AgregarNuevaVisitaAlTramite=false;
	if(isset($_REQUEST['AgregarNuevaVisitaAlTramite']))
	{
		$AgregarNuevaVisitaAlTramite=$_REQUEST['AgregarNuevaVisitaAlTramite'];
	}
	
	$id_TramiteXVisita="";
	if(isset($_REQUEST['id_TramiteXVisita']))
		$id_TramiteXVisita=$_REQUEST['id_TramiteXVisita'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta content="no-cache" http-equiv="Pragma" />
		<meta content="no-cache" http-equiv="Cache-Control" />
		<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
		<style type="text/css">
			<!--Estilo1 {font-family: Arial, Helvetica, sans-serif}.Estilo3 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; }.Estilo4 {font-size: 24px}			-->
				</style>
			<font face="Arial">
				<title>Alta Visita</title>
				
			<style type="text/css">
					#altas span.errorphp{color:red; margin:0px; padding:0px; text-align:center;}
				h1 {
					margin: 0;
					background-color: ;
					color: white;
					font-size: 25px;
					padding: 3px 5px 3px 10px;
					border-bottom-widtd: 1px; 
					border-bottom-style: solid; 
					border-bottom-color: white;
					border-top-widtd: 1px; 
					border-top-style: solid; 
					border-top-color: white;
					background-repeat: repeat;
					font-weight: bolder;
					background-image: nombre.png;
					font-family: Georgia, "Times New Roman", Times, serif;
				} 
			</style>
		<!--<link rel="stylesheet" type="text/css" href="body.css">-->
		<!-- Calendario -->
		<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
		<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
		<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
		<script language="javascript" type="text/javascript"></script>
		<script language="javascript" src="javascript/prototype.js"></script>
		<script language="javascript" src="javascript/funciones_visitas.js"></script>
          <script type="text/javascript">
			targetElement = null;
			function makeSelection(frm, id) {
				var area =  document.getElementById('idarea').value;
				
				if(area.lenght<=0 || area=="")
				{	alert("Debe seleccionar un tramite");
					return;
				}
				
				if(!frm || !id)
					return;
				targetElement = frm.elements[id];
				var handle = window.open('../php_ajax/selectEmailsParaCopia.php?area='+area);
			}
       </script>
	</head>
	<body align="center">
	<?php
		$idfolio="";
		if(isset($_GET['idfolio']))
			$idfolio=$_GET['idfolio'];
		//echo "Folio".$idfolio;
		if(isset($_GET['idcontacto']))			
		{
			$idcontacto=$_GET['idcontacto'];
			
			$consulta ="SELECT * FROM Contactos WHERE IDContacto=$idcontacto";
			$getProducts = sqlsrv_query($conexion,$consulta);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				$NombreContacto = $row['nombre'];				
/*
			$dbRes = odbc_exec($conexion_db, $consulta);
			while($row = odbc_fetch_object($dbRes))
			{
				$NombreContacto = $row->nombre;																
			}
*/
			
			$widvisita=0;
			$consulta = "select max(idvisita) as mvisita from visitas where idcontacto=$idcontacto";
			$getProducts = sqlsrv_query( $conexion,$consulta);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				$widvisita = $row['mvisita'];
/*			$dbRes = odbc_exec($conexion_db, $consulta);
			while($row = odbc_fetch_object($dbRes))
			{
				$widvisita = $row->mvisita;																
			}
*/
			if($widvisita==0)
			{
				$widvisita=1;
			}
			else
			{
				$widvisita= $widvisita + 1;
			}
		}
		else
		{
			exit("No Existe el Contacto");
		}				
	?>
    <table width="100%" align="center">
    	<tr>
			<td align="center" colspan="12" class="TituloDForma">Alta de Visita<hr class="hrTitForma">
           </td>
           
        </tr> 
    </table >
<form name="altas" id="altas" class="altas" method="post" action="php_ajax/alta_de_visita_brigadas.php" align="center" enctype="multipart/form-data">
    <table widtd="90%" align="center">
        <tr>
        	<td class="subtituloverde" colspan="2"><div align="center"><span >INFORMACION GENERAL  
            <input id= 'usuariolog'  name='usuariolog' type='hidden' value='<?php echo $usuariolog;?>' /></span></div></td>                 
        </tr>
        <tr>
            <input name='contacto' type='hidden' id= 'contacto' value='<?php echo $idcontacto;?>' />
            <input name='visita' type='hidden' id= 'visita' value='<?php echo $widvisita; ?>' />
            <input name='id_TramiteXVisita' type="hidden" id='id_TramiteXVisita' value="<?php $id_TramiteXVisita;?>" />
            <input name='AgregarNuevaVisitaAlTramite' type="hidden" id='AgregarNuevaVisitaAlTramite' value="<?php $AgregarNuevaVisitaAlTramite;?>" />
        </tr>
        <tr>
            <td class="texto8" scope="row">Tipo de Visita:</td> 
            <td><select name="tipoVisita" id="tipoVisita" size="1" >
                    <option value=1>Oficinas</option>
                    <option value=2>Brigada</option>
                    <option value=3>Visita en Campo</option>
                    <option value=4>Delegaci&oacute;n</option>
          		</select>
            </td> 
        </tr>
        <tr>
            <td class="texto8" scope="row">Visitante:</td><td class="texto8" scope="row"><input type="text" name="visitante" id="visitante" value="<?php echo $NombreContacto;?>" readonly="readonly" style="width:300px" /></td>
        </tr>
        <tr><td class="texto8" scope="row">Fecha visita:</td>
        <td><input name="fin" type="text" size="7" id="fin" value="<?php echo date('d/m/Y',time());?>" class="required" maxlength="10"  style="width:70">&nbsp;<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger_fin" style="cursor: pointer;" title="Date selector" align="absmiddle">
                                    <!--{literal}-->
                                    <script type="text/javascript">
                                        Calendar.setup({
                                            inputField     :    "fin",		// id of the input field
                                            ifFormat       :    "%d/%m/%Y",		// format of the input field
                                            button         :    "f_trigger_fin",	// trigger for the calendar (button ID)
                                            //onClose        :    fecha_cambio,
                                            singleClick    :    true
                                        });
                                    </script>
        </td></tr>           
        <td class="texto8" style="width:220px; ">
                    Tramite:</td>
                    <td><div align="left" style="z-index:1; position:static; left:4px; top: 200px; width:250px">  <input class="texto8" name="txtSearch" type="text" id="txtSearch" size="60" style="width:150px;"  onKeyUp="searchSuggest();" autocomplete="off"/><input  name="idtramite" type="hidden" id= "idtramite" size="68" maxlength="70" />  
                    <div id="search_suggest" style="z-index:2;" > </div>
                  </div>
                </td>
        <tr>				
          <td class="texto8" scope="row" >Area:</td>
          <td>	<input  name="nomarea" type="text" id= "nomarea" size="68" maxlength="70" readonly="readonly"/>
          		<input  name="idarea" type="hidden" id= "idarea" size="68" maxlength="70" />
          </td>                  
        </tr>
        <tr>				
          <td class="texto8" scope="row" >Descripci&oacute;n:</td>
          <td ><textarea  name="desctramite" type="text" id= "desctramite" rows="5" cols="50" readonly="readonly" style="background-color:#FFC" /></textarea> </td>                  
        </tr>  
        <tr>				
          <td class="texto8" scope="row" >Requisitos: </td>
          <td ><textarea  name="reqtramite" type="text" id= "reqtramite"rows="5" cols="50" readonly="readonly"  style="background-color:#FFC"  /></textarea> </td>                  
        </tr>                
        
        <tr>
          <td class="texto8" scope="row">Descripci&oacute;n de la visita:</td>
          <td ><textarea name="observ" type="text" id= "observ" rows="5" cols="50" /></textarea></td>
        </tr>
        
         <tr>
          <td class="texto8" scope="row">Situaci&oacute;n:</td>
          <td ><select name="estatus" id="estatus" size="1" class="texto8" >
    				<option value='0000'>------------------------</option>
             <?php 
                    $consulta="select * from CatEstatusVisita ";
                    $getProducts = sqlsrv_query( $conexion,$consulta);
                    while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                    {
                        $descrip = $row['nomestatus'];//$row->nomestatus ;
                        $id = $row['estatus'];//$row->estatus;												
?>
                        <option value='<?php echo $id;?>'><?php echo $descrip;?></option>
 <?php                   
					}
                ?>     
          </select><input type="hidden" id="idfolio" name="idfolio" value="<?php echo $idfolio;?>"></td>               
        </tr>
        <tr>
        	<td colspan="2" class="subtituloverde">Archivos Adjuntos</td>
        </tr>
        	<td colspan="2">
           		<table id="uploadsTable" name="uploadsTable">
        		<tr>
                    <td colspan="1"><input type="file" name="uploadFiles[]" id="uploadFiles[]" class="texto8"  /></td>
                    <td><select name="documento[]" id="documento" size="1" class="texto8" >
                        <?php 
                            $consulta="select iddoc, descrip from catdoc ";
                            $getProducts = sqlsrv_query( $conexion,$consulta);
                            while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                            {
                                $descrip = trim($row['descrip']);
                                $id = trim($row['iddoc']);												
                                if($id ==='0013')
                                {
                        ?>
                                 <option value='<?php echo $id;?>' SELECTED><?php echo $descrip;?></option>
                        <?php
                                }else{
                        ?>
                                <option value='<?php echo $id;?>'><?php echo $descrip;?></option>
                               
                        <?php                   
                                }
                            }
                        ?>     
                        </select> <input type="text" maxlength="1000" name="descripcionFile[]" width="100" />
                        <input type="button" value="+" onclick="javascript: addRowClone('uploadsTable')" />
                        <input type="button" value="-" onclick="javascript: deleteRow('uploadsTable',this)" />
                    </td>
                   </tr>
            	</table>
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
                <table>
                    <tr>
                        <td class="texto8"><input type="checkbox"  name="enviaremail" id="enviaremail" />Enviar Email.</td>
                        <td colspan="1" class="texto8"><input type="checkbox" name="enviaremailAResponsable" id="enviaremailAResponsable"  />Enviar email a jefe o responsable</td>
                                <td class="texto8">
                                    Copia a:<input type="text" id="emailCopias" name="emailCopias" value="" /><input type="button" onclick="javascript: makeSelection(this.form, 'emailCopias')" value="Seleccionar" />                    	
                                </td>
                    </tr>
				</table>
          	</td>
        </tr>
        <tr>
          <td scope="row"><input tabindex="6" type="submit" id="Enviar" name="Enviar" value="Guardar " class="texto8" /></td>
          <td width="225" scope="row"><input tabindex="7" name="restablecer2" type="reset" id="restablecer2" value="Restablecer" class="texto8" /></td>
        </tr>
        
    </table>
</form>



<!-- <table widtd="90%">
    <tr>
     	<td class="subtituloverde" colspan="9"><div align="center"><span >Visitas Anteriores</span></div></td>                 
    </tr> 			
	<tr>
        <td width='6%' align='center'><font class='texto8'>Folio</font >
        <td width='1%' align='right'>&nbsp;</td>
        <td width='10%' align='center'><font class='texto8'>Fecha</font >
        <td width='1%' align='right'>&nbsp;</td>
        <td width='6%' align='left'><font class='texto8' >Movimientos</font></td>
        <td width='1%' align='right'>&nbsp;</td>
        <td width='30%' align='center'><font class='texto8' >Tramite</font >
        <td width='1%' align='left'>&nbsp;</td>
        <td width='30%' align='left'><font class='texto8' >Observaciones</font></td>
    </tr>
<?php
  
  $usuario = $_COOKIE['ID_my_site'];	
  
  $consulta="select a.idregistro,CONVERT(varchar(50),a.fecha,103) as fecham,a.fecha,a.mov,b.idtramite,b.observa,c.nomtramite 
    from visitas a 
    left join movimientos b on a.idregistro=b.idregistro
    left join cattramite c on b.idtramite=c.idtramite 
    where a.idcontacto='$idcontacto' and a.mov=b.mov order by a.fecha desc";
 // echo $consulta;
  $getProducts = sqlsrv_query( $conexion,$consulta);
  while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
 // $dbRes = odbc_exec($conexion_db, $consulta);			//while($row = odbc_fetch_object($dbRes))					
    {
        $folio =  $row['idregistro'];//$row->idregistro;
        $fecha =  $row['fecham'];//substr($row->fecha,0,10);
        $mov =  $row['mov'];//$row->mov;
        $observa =  $row['observa'];//$row->observa;
        $nomtram = $row['nomtramite'];//$row->nomtramite;
        echo "<tr>";
        if($usuario=='mgonzalez')
            echo "<td width='6%' align='center'><font class='texto8' > <a href='edita_visitabrigadas.php?registro=$folio' title='click para Editar Visita'>$folio</a></font></td>";
        else
            echo "<td width='6%' align='center'><font class='texto8' > $folio</font></td>";
            
        echo "	<td width='1%' align='right'>&nbsp;</td>";
        echo "	<td width='10%' align='center'><font class='texto8' >$fecha</font></td>";
        echo "	<td width='1%' align='right'>&nbsp;</td>";
        echo "	<td width='6%' align='center'><font class='texto8' >$mov</font></td>";			
        echo "	<td width='1%' align='left'>&nbsp;</td>";
        echo "	<td width='30%' align='left'><font class='texto8' >$nomtram</font></td>";
        echo "	<td width='1%' align='left'>&nbsp;</td>";
        echo "	<td width='30%' align='left'><font class='texto8' >$observa</font></td>";
        echo "</tr>";		
    } 
  ?>
 
 
</table>-->
	</body>
</html>
