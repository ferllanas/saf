<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$usuario="";
if(isset($_COOKIE['ID_my_site']))
	$usuario=$_COOKIE['ID_my_site'];
else
	die("No se ha registrado apropiadamente.");

//echo $usuario;
$email = "";
$responsable = "";

$usuariolog=$_REQUEST['nomresp'];
$visitante = $_REQUEST['visitante'];

$visita = $_REQUEST['visita'];
$estatus= $_REQUEST['estatus'];
$observ = $_REQUEST['observ'];
$narea=	$_REQUEST['idarea'];
$ntramite= $_REQUEST['idtramite'];
$emailCopias= $_REQUEST['emailCopias'];

$email="";
$enviaremail=false;
if(isset($_REQUEST['enviaremail']))
	$enviaremail= $_REQUEST['enviaremail'];
	
$fecha = date('Y-m-d',time());
$hora =  date('H:i',time());
//$prog = $_REQUEST['prog'];
if(isset($_COOKIE['areaActual']))
		$widarea = $_COOKIE['areaActual'];
		
if(isset($_COOKIE['Responsable']))
		$widresp = str_pad($_COOKIE['Responsable'],6,'0',STR_PAD_LEFT);

$id_TramiteXVisita="";
if(isset($_REQUEST['id_TramiteXVisita']))
	$id_TramiteXVisita=$_REQUEST['id_TramiteXVisita'];

$AgregarNuevaVisitaAlTramite="";
if(isset($_REQUEST['AgregarNuevaVisitaAlTramite']))
	$AgregarNuevaVisitaAlTramite=$_REQUEST['AgregarNuevaVisitaAlTramite'];
	
$fails=false;	
	//echo "AAAA sss";
	if ($estatus == "" || $observ == ""  ) 
	{
		if($observ == "")
			exit("Debe ingresar alguna observaci&oacute;n.");
		else
			exit("Debe seleccionar un estatus.");
	}
	else
	{
		if ($conexion)
		{ 		
			$consulta="select TOP 1 idcontacto from movimientos where idregistro=$visita ORDER BY ID DESC";
			$getProducts = sqlsrv_query( $conexion,$consulta);//$dbRes = odbc_exec($conexion_db, $consulta);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))//while($row = odbc_fetch_object($dbRes))
			{
				$midcontacto = $row['idcontacto'];
						
			}
		
			$tsql_callSP ="{call sp_Visitas_NuevoMovimiento(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
			$params = array(&$visita,&$usuario, &$estatus,  &$ntramite, &$observ,&$id_TramiteXVisita);//Arma parametros de entrada
			$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
			
			$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
			if( $stmt === false )
			{
				 $fails= "Error in statement execution.\n";
				 $fails=true;
				 die( print_r( sqlsrv_errors(), true)." sp_Visitas_NuevoMovimiento ".print_r($params,true));
			}
			
			//echo " sp_Visitas_alta ".print_r($params,true);
			if(!$fails)
			{
				// Retrieve and display the first result. 
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
				{
					$idRegistroVisitas = trim($row['folioVisita']);
					$idAreaDeUsuarioLog = trim($row['areaDUsuario']);
					//$nomdepto = $row['numempResponsable'];
					$visita = trim($row['idvista']);
					$idarea = trim($row['idAreaDestino']);
					$nomdir = trim($row['idResponsableAreaDestino']);
					$email = trim($row['emailEnvio']);
				}
				sqlsrv_free_stmt( $stmt);
			}
			
			if($enviaremail)
			{
				enviaremail($idarea,$observ, $idRegistroVisitas, $usuariolog, '', $visitante, $email , $responsable, $emailCopias);
			}
			$mensajeAcciones="";
			$datos=array();
			if(count($_FILES['uploadFiles']['name'])>0) 
			{
				//obtenemos todos los archivos uno por uno
				
				for($i=0; $i<count($_FILES['uploadFiles']['name']);$i++)
				{
               		$Ext = pathinfo($_FILES['uploadFiles']['name'][$i], PATHINFO_EXTENSION); 
					$Max = $_FILES['uploadFiles']['size'][$i];
					$tmp_name = $_FILES['uploadFiles']['tmp_name'][$i];
					if($tmp_name=="")
					{
						//echo $tmp_name." No trae nada";
						continue; 
					}
					//else
						//echo "<br>".$tmp_name;
						
					$path_parts = $tmp_name;//pathinfo("../".$docdigitales[$i]['path']);
					$fextension =  $Ext;
						
					//Carpeta del nuevo archivo
					$dirNewFile= "../".$imagenes_folder."".$midcontacto;
					$dirNewFile = substr( $dirNewFile,3,strlen($dirNewFile)-3);
					
					
					//echo $dirNewFile;//."<br>".$imagenes_folder."<br>".$path_parts;
					if(!file_exists($dirNewFile))
						if(!mkdir ( $dirNewFile, 0700 ))
						{
							$datos[$i]['error']="01";
							$datos[$i]['string']="Error no pudo crearse la carpeta $dirNewFile .";
							//echo $datos[$i]['string'];
							//exit($mensajeAcciones.="Error no pudo crearse la carpeta $dirNewFile .");
						}
						
					$tipoDoc=$_REQUEST["documento"][$i];
					$observaciones= $_REQUEST["descripcionFile"][$i];
					//Arma la ruta o path del nuevo archivo
					$newfile = $dirNewFile."/".$midcontacto."_".$tipoDoc.".".$fextension;
					//Path del archivo para guardar en la base de datos
					$pathtoDB = "/".$midcontacto."/".$midcontacto."_".$tipoDoc.".".$fextension;
					
					if(!file_exists($newfile)) //Si no existe el archivo previamente
					{
						if(move_uploaded_file($tmp_name, $newfile))//if(rename("../".$docdigitales[$i]['path'], $newfile))
							$mensajeAcciones.="<br>El documento ".$tipoDoc." a sido enviado exitosamente.";
						else
							$mensajeAcciones.="<br>El documento ".$tipoDoc." tuvo errores al tratar de enviarlo.";
						
						//echo $mensajeAcciones;
							
					}
					else
					{
						if(move_uploaded_file($tmp_name, $newfile))//if(rename("../".$docdigitales[$i]['path'], $newfile))
							$mensajeAcciones.="<br>El documento ".$tipoDoc." a sido enviado exitosamente.";
						else
							$mensajeAcciones.="<br>El documento ".$tipoDoc." tuvo errores al tratar de enviarlo.";
						
						//echo $mensajeAcciones;
					}
					
					$consulta = "insert into contdoc(nts,folioese, fml, iddoc,  nomarch, nomreg, feccap,estatus,observaciones, idcontacto, idVisita) 
							values('','','','".$tipoDoc."','$pathtoDB','',CONVERT(char(8),GETDATE(),112),'0000','$observaciones', $midcontacto, $idRegistroVisitas);  
							SELECT SCOPE_IDENTITY()  as id";
					
					//echo "<br>".$consulta."<br>";
					$s = sqlsrv_prepare( $conexion, $consulta);
					if (sqlsrv_execute($s) === false)
					{ 
						//Hubo errores dirante la  consulta
						$resoponsecode="02";
						$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
						
						$datos[0]['error']=$resoponsecode;
						$datos[0]['string']=$descriptioncode;
						
					//	echo "<br>".$datos[0]['string'];
					}
					else
					{
						sqlsrv_next_result($s);
						$r = sqlsrv_fetch_array($s, SQLSRV_FETCH_ASSOC);
						$datos[$i]['string']="".count($s);
						$datos[$i]['idef']=$r['id'];
						$datos[$i]['error']="0";
						
					//	print_r($datos);
					}
	
					$newrch = $dirNewFile."/".$midcontacto."_".$tipoDoc."_".$datos[$i]['idef'].".".$fextension;
					$pathtoDB = "/".$midcontacto."/".$midcontacto."_".$tipoDoc."_".$datos[$i]['idef'].".".$fextension;
					if(rename ( $newfile , $newrch ))
					{
						$consulta = "UPDATE contdoc SET nomarch = '$pathtoDB' WHERE id=".$datos[$i]['idef'];
						echo $consulta;
						$s = sqlsrv_prepare( $conexion, $consulta);
						sqlsrv_execute($s);
					}
				}
			}
			sqlsrv_close($conexion);
			header("Location: ../seg_visitas.php#top");
		}
		else
		{ 
			exit ("FallÃ³ la conexiÃ³n de base de datos") ;
	    }
		//odbc_close($conexion_db);
    }
//}
	
function enviaremail($idarea,$observaciones, $numvisita, $usuariolog, $nomAreaDeTramite, $nombre, $email , $responsable, $emailCopias)
{
	global $server,$infoconexion;
	
	include("../PHPMailer_v5.1/class.phpmailer.php");
	include("../PHPMailer_v5.1/class.smtp.php"); // note, this is optional - gets called from main class if not already loaded

$emails= array('fernando.llanas@fomerrey.gob.mx');

echo "A1";
$conexion = sqlsrv_connect($server,$infoconexion);

//Obtiene area que envia
//echo "!".$nomAreaDeTramite."!";
if($nomAreaDeTramite=="")
{
	$consulta = "SELECT d.nomdepto, c.email, c.email_fome ,c.Nombre 
					FROM nomemp.dbo.nominadempleados b 
					INNER JOIN fomedbe.dbo.usuarios c ON b.numemp=c.numemp COLLATE Traditional_Spanish_CI_AS
					INNER JOIN nomemp.dbo.nominamdeptoNew d ON d.depto=b.depto
						WHERE c.usuario ='".$_COOKIE['ID_my_site']."'";
	//echo $consulta;
	$stmt = sqlsrv_query($conexion, $consulta);
	if( $stmt === false )
	{
		 die( print_r( sqlsrv_errors(), true));
	}	
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))
		{
			$nomAreaDeTramite=trim($row['nomdepto']);
		}
	}
}


$envioVarios=false;
if($email==""  && $responsable=="")
{
	$tsql_callSP = "SELECT c.email, c.email_fome ,c.Nombre FROM nomemp.dbo.nominadempleados b INNER JOIN fomedbe.dbo.usuarios c ON b.numemp=c.numemp COLLATE Traditional_Spanish_CI_AS
		WHERE b.depto =$idarea";
	echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP);
	if( $stmt === false )
	{
		 die( print_r( sqlsrv_errors(), true));
	}	
	$envioVarios=true;
}


$fails=false;


$body             = /*file_get_contents('invitacion.php?condPagoDias=30&adqparcial=si&requisi=$requisi');$mail->getFile('contents.html');"/*/
"
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
</head>
<body>
<table width='100%'>
	<tr>
    	<td><H1 align='center'>Tecnologia de la informaci&oacute;n</H1></td>
    </tr>
    <tr>
    	<td><h3 align='center'>Modulo de Atenci&oacute;n a clientes</h3></td>
    </tr>
    <tr>
      <td>Usted tiene un tramite pendiente con n&uacute;mero &quot;$numvisita&quot; de $nombre.<br> A continuaci&oacute;n las observaciones/indicaciones del Tramite:			     </td>
    </tr>
    <tr>
    	<td>$observaciones</td>
    </tr>
     <tr>
    	<td>&nbsp;</td>
    </tr>
     <tr>
    	<td>&nbsp;</td>
    </tr>
	<tr>
		<td>Atte.  $usuariolog</td>
	<tr>
    	<td>$nomAreaDeTramite</td>
    </tr></td>
	</tr>
    <tr>
    	<td bgcolor='#FFCC00' style='font-size:11px'>Usted puede atender esta solicitud ingresando <a href='http://200.94.201.196/fome/'>al SIIF en linea</a>. En el modulo de Visitantes, en la opci&oacute;n &quot;Buscar Visitante&quot; o &quot;Control de Visitas&quot;.</td>
    </tr>
    <tr>
    	<td bgcolor='#FFCC99' style='font-size:11px'>Si usted no tiene usuario o contraseÃ±a para ingresar al sistema favor de solicitarlo al area de Informatica.</td>
    </tr>
</table>
</body>
</html>
";
	//$body             = eregi_replace("[\]",'',$body);
	$body =str_replace("[\]",'',$body);

	//echo $body;
	$emailsenviado="";
	//$mail->AddAttachment("/path/to/file.zip");             // attachment
	//$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment
	$mail             = new PHPMailer();
	$numcuenta=0;
	$idssList="";
	//for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) 
	//{
		echo "AAAAA";
		
		echo $emailsenviado."ME LLEVA";
		$i=1;
		if($envioVarios)
		{
			while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))//for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) 
			{
				
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
				$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 2525;//465;// set the SMTP port
				
				$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
														//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
				$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
				
				$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
				$mail->FromName   = "Tecnologia de la Informacion";
				$mail->Subject    = "Tecnologia de la Informacion";
				$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
				$mail->WordWrap   = 50; // set word wrap
				
				$mail->MsgHTML($body);
				
				$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
				//"First Last");
				//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
				$mail->IsHTML(true); // send as HTML
				$mail->AddAddress(trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
				
				$word = split(';',$emailCopias);
				for($j=0;$j<count($word);$j++)
				{	
					
					if(strlen($word[$j])>0)
					{
						//echo "<br>JJJ".$word[$j];
						$mail->AddCC($word[$j]);
					}
				}
				

				$emailsenviado .= trim($row['email_fome'])." XXX<br>";
				
				
				
				//$idssList.= trim($row['id']);
				if(!$mail->Send()) 
				{
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} 
				
				$mail  = new PHPMailer();
				
				$numcuenta++;
				if($numcuenta>=count($emails))
				{
					$numcuenta=0;
				}
				
				$i++;
			}
		}
		else
		{
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
			$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
			$mail->Port       = 2525;//465;// set the SMTP port
			
			$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
													//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
			$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
			
			$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
			$mail->FromName   = "Tecnologia de la Informacion";
			$mail->Subject    = "Tecnologia de la Informacion";
			$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
			$mail->WordWrap   = 50; // set word wrap
			
			$mail->MsgHTML($body);
			
			$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
			//"First Last");
			//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
			$mail->IsHTML(true); // send as HTML
			$mail->AddAddress($email, $responsable);//trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
			
			$word = split(';',$emailCopias);
			for($j=0;$j<count($word);$j++)
			{	
				if(strlen($word[$j])>0)
				{
					//echo "<br>JJJ".$word[$j];
					$mail->AddCC($word[$j]);
				}
			}
			
			$emailsenviado .= trim($email)." XXX<br>";
			//echo $emailsenviado;
			//$idssList.= trim($row['id']);
			if(!$mail->Send()) 
			{
			  echo "Mailer Error: " . $mail->ErrorInfo;
			} 
			
			$mail  = new PHPMailer();
			
			$numcuenta++;
			if($numcuenta>=count($emails))
			{
				$numcuenta=0;
			}
		}
}
?>
