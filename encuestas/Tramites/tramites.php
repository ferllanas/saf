<?php 

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");

	$conexion = sqlsrv_connect($server,$infoconexion);
	
	
	
	$usuario ="";
	if(isset( $_COOKIE['ID_my_site']))
		$usuario = $_COOKIE['ID_my_site'];
	else
		die("Debe ingresar al sistema.");
	
	$areaUsuarioLog="";
	if(isset( $_COOKIE['Area']))
		$areaUsuarioLog = $_COOKIE['Area'];	
	
	$depto='';
	if(isset($_REQUEST['depto']))
		$depto= $_REQUEST['depto'];
	else
		$depto= $_COOKIE['areaActual'];
	
	$tramiteregistrado=false;
	if(isset($_REQUEST['tramiteregistrado']))
		$tramiteregistrado=$_REQUEST['tramiteregistrado']=true;
	
	$tramiteactualizadoo=false;
	if(isset($_REQUEST['tramiteactualizadoo']))
		$tramiteactualizadoo=$_REQUEST['tramiteactualizadoo'];
		
	if($depto!='')
	{
		$consulta ="SELECT a.idtramite, a.nomtramite,a.estatus, c.nomdepto, a.desctramite, a.reqtramite , c.depto, a.idarea
						FROM [fomedbe].[dbo].[CatTramite] a
						LEFT JOIN [nomemp].dbo.nominamdeptoNew c ON c.depto=a.idarea COLLATE Traditional_Spanish_CI_AS WHERE c.depto='$depto'";
	}
	else
	{
		$consulta ="SELECT a.idtramite, a.nomtramite,a.estatus,
			c.nomdepto, a.desctramite, a.reqtramite , a.idarea
		  FROM [fomedbe].[dbo].[CatTramite] a 
		  LEFT JOIN [nomemp].dbo.nominadempleados b ON b.depto=a.idarea COLLATE Traditional_Spanish_CI_AS
		  LEFT JOIN  [nomemp].dbo.nominamdeptoNew c ON c.depto=b.depto  COLLATE Traditional_Spanish_CI_AS 
		  LEFT JOIN [fomedbe].[dbo].[usuarios] d ON d.numemp=b.numemp COLLATE Traditional_Spanish_CI_AS
		  WHERE d.usuario='$usuario'";
	}
	
			
//echo $consulta;
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tramites</title>
<link href="../../javascript/sortableWScroll/example.css" rel="stylesheet" type="text/css"/>
<link href="../../css/tablesscrollbodyFracc.css"          rel="stylesheet" type="text/css">
<link href="../../css/estilosReportesCartera.css"         rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../javascript/jQuery.js"></script> 
<script type="text/javascript" src="../../javascript/sortableWScroll/sortable.js"></script>
<script type="text/javascript" src="javascript/funciones_visitas.js"></script>
<script type="text/javascript">
function tramregistrado()
{
	alert("El trámite se ha registrado satisfactoriamente.");
}
function tramactualizado()
{
	alert("El trámite se ha actualizado satisfactoriamente");
}
</script>

</head>
<body onload="<?php if($tramiteregistrado) echo "javascript: tramregistrado();"; else if($tramiteactualizadoo) echo "javascript: tramactualizado();"; ?>">


<?php
if($areaUsuarioLog=='1232' || $areaUsuarioLog='1230' || $areaUsuarioLog='1231')
{
?>
<table width="100%">
    <tr>
        <td align="center" colspan="12" class="TituloDForma">Administraci&oacute;n de tramites
                <input type="hidden" id="depto" name="depto" value="<?php echo $depto;?>"  />
              <hr class="hrTitForma"></td>
    </tr> 	
</table>
<form name="buscarform" id="buscarform" action="tramites.php">
<table align="center">

  <tr>
		<td class="texto8">Selecciona un departamento:</td>
        <td>
        <?php
             $consulta1="SELECT depto, nomdepto FROM nomemp.dbo.nominamdeptoNew WHERE estatus='00' ORDER BY nomdepto";
			$getProducts = sqlsrv_query($conexion,$consulta1);	 
			if($depto==1232 || $depto==1231 || $depto==1230)//$usuario=='fllanas' || $usuario=='egonzalez')
			{
				?>
			<select id="depto" name="depto" class="texto8" onChange="javascript: document.getElementById('buscarform').submit();">
<?php			
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					if($depto==trim($row['depto']))
						echo '<option value="'.$row['depto'].'" SELECTED title="'.$row['depto']." ". $row['nomdepto'].'">'.$row['nomdepto'].'</option>';
					else
						echo '<option value="'.$row['depto'].'" title="'.$row['depto']." ". $row['nomdepto'].'">'.$row['nomdepto'].'</option>';
				}
   
	?>
			</select>	
            <?php  
			} 
			else
			{
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					//echo $depto."==".trim($row['depto'])."; ";;
					if($depto==trim($row['depto']))
						echo trim($row['depto']).'<input type="hidden" name="depto" id="depto" value="'.$row['depto'].'" />'.$row['nomdepto'];
				}
			}
			?>
        </td>
      <td>
            <img  src="../../imagenes/agregar2.jpg" style="width:22px; height:22px;" title="Agregar nuevo tramite a departamento." onClick="javascript: irAltaVisita()" />
   	  </td>
	</tr>
</table>
</form>
<?php
}
?>


<table class="tableone" id="sortable" name="sortable" style="width:900px;">
<caption></caption>	
<thead>
	<tr>
    	<td class="th5">Num. Tramite</td>
        <td class="th3">Nombre</td>
        <td class="th3">Departamento</td>
        <td class="th5">Descripción</td>  
        <td class="th5">Requisitos</td>
        <td class="th5">&nbsp;</td>
    </tr>
</thead>
<tbody>
	<tr>
		<td colspan="6"><div class="innerb">
		  <table class="tabletwo" id="bodyF" style="width:900px;">
		    <tbody >
		      <?php
	$getProducts = sqlsrv_query($conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
	{
?>
		      <tr class="<?php echo "d".($i%2);?>">
		        <td class="td5"><?php echo $row['idtramite'];?></td>
		        <td class="th3"><?php echo $row['nomtramite'];?></td>
		        <td class="th3"><?php echo $row['nomdepto'];?></td>
		        <td class="th5"><?php echo $row['desctramite'];?></td>
		        <td class="th5"><?php echo $row['reqtramite'];?></td>
		        <td class="th5"><?php   if($row['estatus']<90)
						  			{
							?>
		          <img src="../../imagenes/edit-icon.png" 
                  		onclick="javascript:location.href='editarTramite.php?idtramite=<?php echo trim($row['idtramite']);?>&depto=<?php echo  trim($row['idarea']);?>'"}
                        title='editarTramite.php?idtramite=<?php echo trim($row['idtramite']);?>&depto=<?php echo  trim($row['idarea']);?>' /> <img src="../../imagenes/eliminar.jpg"  onclick="javascript:location.href='borrarTramite.php?idtramite=<?php echo  $row['idtramite'];?>&depto=<?php echo  trim($row['idarea']);?>'"/>
		          <?php 
									}
									else
									{
							?>
		          <img src="../../imagenes/borrar.jpg" />
		          <?php
									}
							  ?></td>
	          </tr>
		      <?php
	$i++;
	}
?>
	        </tbody>
	      </table>
      </div></td>
    	</tr>
	</tbody>
</table>
</body>
</html>