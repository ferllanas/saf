<?php
	require_once("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	$usuario = $_COOKIE['ID_my_site'];	
	
	$idtramite="";
	if(isset($_REQUEST['idtramite']))
		$idtramite = $_REQUEST['idtramite'];
		
	$depto="";
	if(isset($_REQUEST['depto']))
		$depto = $_REQUEST['depto'];
	
	$consulta ="SELECT a.*  FROM [fomedbe].[dbo].[CatTramite] a WHERE a.idtramite='$idtramite'";
	
	$nomtramite="";
	$destramite="";
	$reqtramite="";
	
	$conexion = sqlsrv_connect($server,$infoconexion);
	$getProducts = sqlsrv_query($conexion,$consulta);
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
	{
		$nomtramite=$row['nomtramite'];
		$destramite=$row['desctramite'];
		$reqtramite=$row['reqtramite'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta content="no-cache" http-equiv="Pragma" />
		<meta content="no-cache" http-equiv="Cache-Control" />
		<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
		<title>Registro de Tramites</title>
		<style type="text/css">
			<!--
			.Estilo1 {font-family: Arial, Helvetica, sans-serif}
			.Estilo3 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
			.Estilo4 {font-size: 24px}
			-->
		</style>
		<style type="text/css">
					#altas span.errorphp{color:red; margin:0px; padding:0px; text-align:center;}
				h1 {
					margin: 0;
					background-color: ;
					color: white;
					font-size: 25px;
					padding: 3px 5px 3px 10px;
					border-bottom-widtd: 1px; 
					border-bottom-style: solid; 
					border-bottom-color: white;
					border-top-widtd: 1px; 
					border-top-style: solid; 
					border-top-color: white;
					background-repeat: repeat;
					font-weight: bolder;
					background-image: nombre.png;
					font-family: Georgia, "Times New Roman", Times, serif;
				} 
		</style>
		<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
		<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
		<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
	</head>

	<body>		
	 
			<form name="altas" class="altas" metdod="post" action="valEditarTramite.php">
               	<table>
					<tr>
						<td colspan="2" class="subtituloverde12" scope="row" ><input id='idtramite' name="idtramite" type="hidden" value="<?php echo $idtramite;?>" />
                        <input id='depto' name="depto" type="hidden" value="<?php echo $depto;?>" /><strong></strong>
						<div align="center">Edici&oacute;n.- Datos Generales del Tramite </div></td>					  	
					</tr>
					<tr class="texto9">
						<td class="texto9" scope="row" ><div align="left">Nombre del Tramite(s): * </div></td>
					  	<td ><font class="texto8">
					  	  <input tabindex="1" name="nomtramite" type="text" id="nomtramite" size="60" maxlength="60" value="<?php echo $nomtramite;?>"/>
					  	</font></td>
					</tr>
					<tr class="texto9">
						<td width="200px" height="100px" class="texto9" scope="row"><div align="left">Descripci&oacute;n del Tramite: </div></td>
                  		<td ><textarea tabindex="2" name="destramite" id="destramite" rows="10" cols="45"><?php echo $destramite;?></textarea></td>
					</tr>		
					<tr>
						<td width="200px" height="100px" class="texto9" scope="row">Requisitos del Tramite: </td>
                  		<td ><textarea tabindex="3" name="reqtramite"  id="reqtramite" rows="10" cols="45"><?php echo $reqtramite;?></textarea></td>
					</tr>						
			  <tr class="texto9">
                  <td scope="row"><div align="right">
                      <input name="Enviar" type="submit" class="caja_entrada" id="Enviar" tabindex="4" value="Guardar" />
                  </div></td>
                  <td width="225" scope="row"><div align="left">
                      <input name="restablecer" type="reset" class="caja_entrada" id="restablecer" tabindex="5" value="Restablecer" />
                  </div></td>
                </tr>
              </table>
			</form>
	</body>
</html>
