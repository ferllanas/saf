<?php

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	
$conexion = sqlsrv_connect($server,$infoconexion);

$folio = "";
if(isset($_REQUEST['idtramite']))
	$folio = $_REQUEST['idtramite'];

$depto = "";
if(isset($_REQUEST['depto']))
	$depto = $_REQUEST['depto'];
	
$nomtramite = "";
if(isset($_REQUEST['nomtramite']))
	$nomtramite = $_REQUEST['nomtramite'];

$destramite = "";
if(isset($_REQUEST['destramite']))
	$destramite = $_REQUEST['destramite'];

$reqtramite = "";
if(isset($_REQUEST['reqtramite']))
	$reqtramite = $_REQUEST['reqtramite'];
	
$docreq = "";
if(isset($_REQUEST['docreq']))
	$reqtramite = $_REQUEST['docreq'];

if ($nomtramite == "" ||$destramite == "" ) 
{
	exit("No se llenaron campos necesarios");
}
else
{
	if ($conexion)
	{ 				
		$consulta = "UPDATE cattramite SET nomtramite='$nomtramite', desctramite='$destramite', reqtramite='$reqtramite', docreq='$docreq' WHERE idtramite=$folio";
		$stmt=sqlsrv_query( $conexion,$consulta);
		if( $stmt === false )
			die("Error al intentar Actualiza Tramites". sqlsrv_errors(). "\n $consulta");
		
		sqlsrv_close($conexion);
		
		header("Location: tramites.php?tramiteactualizadoo=true");
		//echo "Tramite actualizado.<br><a href='tramites.php?depto=$depto'>Regresar a Tramites</a>";
	}
	else
	{ 
		exit("Falló la conexión de base de datos") ;
	}	
}
?>