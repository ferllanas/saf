<?php

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	
$conexion = sqlsrv_connect($server,$infoconexion);

$folio = "";
if(isset($_REQUEST['idtramite']))
	$folio = $_REQUEST['idtramite'];

$depto = "";
if(isset($_REQUEST['depto']))
	$depto = $_REQUEST['depto'];
	


if ($conexion)
{ 				
	$consulta = "UPDATE cattramite SET estatus='90' WHERE idtramite=$folio";
	$stmt=sqlsrv_query( $conexion,$consulta);
	if( $stmt === false )
		die("Error al intentar Actualiza Tramites". sqlsrv_errors(). "\n $consulta");
	
	sqlsrv_close($conexion);
	
	echo "Tramite a sido cancelado.<br><a href='tramites.php?depto=$depto'>Regresar a Tramites</a>";
}
else
{ 
	exit("Falló la conexión de base de datos") ;
}	

?>