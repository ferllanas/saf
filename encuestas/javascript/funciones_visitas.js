function seleccionar_EmailDPersonas()
{
	var visita =  document.getElementById('visita').value;
	var emailCopias=  document.getElementById('emailCopias').value;
	if(visita.length<=0)
	{
		alert("Error no a entrado al sistema adecuadamente");
	}
	else
	{
		newwindow=window.open('php_ajax/selectEmailsParaCopia.php?visita='+visita,'name','height=500,width=500');
		if (window.focus) {newwindow.focus()}
		return false;

	}
}

function validaVisita(estatusini)
{
	//alert(estatusini);
	var descripcion="";
	//
	if( $('observ').value.length <= 0 )
	{
		alert('Debe de ingresar almenos un comentario u observaci�n.');
		return false;
	}
	
	var estatus="";
	if(document.getElementById('estatus').selectedIndex >= 0)
		estatus = document.getElementById('estatus').options[document.getElementById('estatus').selectedIndex].value;
	
	//alert(estatus+"=="+estatusini);
	if(estatus == estatusini)
	{
		if(!confirm("No ha modificado el estado de la visita.\n �Deseas mantener el estado de la visita actual?"))
			return;
	}
	$('altas').submit();
}


function validastatus()
{
	var estatus="";
	if($('estatus').selectedIndex >= 0)
		estatus = $('estatus').options[$('estatus').selectedIndex].value;
		
	if( parseInt(estatus)>=500 )
	{
		$('txtSearch').setAttribute('readonly','readonly');
		$('idtramite').setAttribute('readonly','readonly');
	}
	else
	{
		$('txtSearch').removeAttribute('readonly');
		$('idtramite').removeAttribute('readonly');	
	}
}

function enviarRecordatorio()
{
	var visita =  document.getElementById('visita').value;
	var emailCopias=  document.getElementById('emailCopias').value;
	//alert('php_ajax/envio_recordatorios.php?visita='+visita+'&emailCopias='+emailCopias);
	new Ajax.Request('php_ajax/envio_recordatorios.php?visita='+visita+'&emailCopias='+emailCopias,
					 {onSuccess : function(resp) {
						if( resp.responseText ) {
							//alert(resp.responseText)
							var myArray = eval(resp.responseText);
							 if(myArray[0].error!=0)
							 {
								 alert("A ocurrido un error: "+resp.responseText);
							 }
							 else
							 {
								 alert("El recordatorio ha sido enviado.");
								 window.location='seg_visitas.php';
							 }
						}
					  }
				});
}

function cargarComboProgramaVisita()
{
	//alert($('area').options[$('area').selectedIndex].value);
	var area="";
	//if($('area').selectedIndex >= 0)
		//area = $('area').options[$('area').selectedIndex].value;
	
	var tramite="";
	if($('tramite').selectedIndex >= 0)
		tramite = $('tramite').options[$('tramite').selectedIndex].value;

	var crtlacambiar=$('prog');
	//alert('php_ajax/consulta_programas.php?area='+area+'&tramite='+tramite);
	new Ajax.Request('php_ajax/consulta_programas.php?area='+area+'&tramite='+tramite,
					 {onSuccess : function(resp) {
						if( resp.responseText ) {
							// got an array of suggestions.
							//alert(resp.responseText);
							var myArray = eval(resp.responseText);
							crtlacambiar.options.length = 0; 
							for( var ii = 0; ii < myArray.length; ii++ ) 
							{
								var popular1 = new Option( myArray[ii].nomtramite,myArray[ii].id,"","");
								crtlacambiar[ii] =popular1;
							}
						}
					  }
				});
}
function busquedaIncrementalProductos(inputTermino)
{
	//new Ajax.Updater('coches', 'php_ajax/query.php?q='+this.value, {method: 'get' })
	var termino=inputTermino.value;
	var productoDom=$('producto');
	productoDom.options.length = 0;
	
	//alert('php_ajax/query.php?q='+termino);
	new Ajax.Request('php_ajax/query.php?q='+termino,
					{onSuccess : function(resp) 
						{
							if( resp.responseText ) 
							{
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									for( var ii = 0; ii < myArray.length; ii++ ) 
									{
										var popular1 = new Option( myArray[ii].valor,myArray[ii].id,"","");
										popular1.title=myArray[ii].valor;
										productoDom[ii] =popular1;
									}
									
									productoDom.size =10;
								}
							}
						}
					});
}


function tomarValorDComboyCambiarInput(combo,input)
{
	if(combo.selectedIndex >= 0)
		input.value = combo.options[combo.selectedIndex].text;
		
	combo.size=0;
}

//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
   
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('txtSearch').value);
        //alert('php_ajax/query.php?q=' + str);
	   searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
        ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split(";");
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+';'+tok[3]+'">' + tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    document.getElementById('txtSearch').value = value;
	var tok =idprod.split(";");
	$('idtramite').value = tok[0];
	$('idarea').value = tok[1];
	llenacampos($('idtramite').value);
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
	
}
function llenacampos(idtramite)
{
	//alert('php_ajax/datos.php?idtramite='+idtramite);
	new Ajax.Request('php_ajax/datos.php?idtramite='+idtramite,
					{onSuccess : function(resp) 
						{
							if( resp.responseText ) 
							{
								//alert(resp.responseText);
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									//alert(myArray[0].idarea);
									$('idarea').value = myArray[0].idarea;
									$('nomarea').value = myArray[0].nomarea;
									
								}
							}
						}
					});
}
function addslashes (str) {
    // Escapes single quote, double quotes and backslash characters in a string with backslashes  
    // 
    // version: 1102.614
    // discuss at: http://phpjs.org/functions/addslashes    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +   improved by: marrtins
    // +   improved by: Nate
    // +   improved by: Onno Marsman    // +   input by: Denny Wardhana
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   improved by: Oskar Larsson H�gfeldt (http://oskar-lh.name/)
    // *     example 1: addslashes("kevin's birthday");
    // *     returns 1: 'kevin\'s birthday'    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  

}

function addRowClone(tblId)
{
  var tblBody = document.getElementById(tblId).tBodies[0];
  var newNode = tblBody.rows[0].cloneNode(true);
  tblBody.appendChild(newNode);
}

function deleteRow(tblId,r){
	var tblBody = document.getElementById(tblId).tBodies[0];
	var i=r.parentNode.parentNode.rowIndex;
	tblBody.deleteRow(i-1);
}


