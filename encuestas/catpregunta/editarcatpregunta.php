<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$titulo="Agregar Pregunta";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	$titulo="Editar Pregunta";
}


$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM encuestacatpreguntas where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$descripcion = trim($row['descripcion']);
			$idsecretaria = trim($row['idsecretaria']);
			$idrubro = trim($row['idrubro']);
			$estatus=$row['estatus'];
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script type="text/javascript" src="../catpregunta/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../catpregunta/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../catpregunta/javascript/catpregunta.js"></script>

<!-- Load jQuery build -->
<script type="text/javascript" src="../javascript/tinymce_3.5.7_jquery/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init
({
	 mode : "textareas",
	theme : "advanced",
	        theme_advanced_buttons1 : "bold, italic, underline, bullist,numlist ",
	        theme_advanced_toolbar_location: "top",
	        theme_advanced_toolbar_align: "left"	
})
</script>

<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Pregunta</title>
</head>

<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Sección De Proyecto -------- -->
<table width="100%">
<tr>
            <td width="13%"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /></td>
    </tr> 
    <tr>
    	<td  class="titles">Descripción:</td>
        <td width="87%"><input style="width:350px" type="text" name="descripcion" id="descripcion" value="<?php echo $descripcion;?>"/></td>
    </tr>
    <tr>
    	<td class="titles">Secretaria:</td>
        <td><select name="idsecretaria" id="idsecretaria" size="1">
        	<option value="0">Ninguna</option>
        	<?php
				$consulta = "select id,descripcion from encuestacatsecretaria where estatus=0";
				$R = sqlsrv_query( $conexion_srv,$consulta);
				while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
				{
					if($idsecretaria== $row['id'])
						echo "<option value='".$row['id']."' selected>".utf8_encode($row['descripcion'])."</option>";
					else
						echo "<option value='".$row['id']."' >".utf8_encode($row['descripcion'])."</option>";
				}
			?>	
            		
            </select>
        </td>
    </tr>
    <tr>
    	<td class="titles">Rubro:</td>
        <td><select name="idrubro" id="idrubro" size="1">
        	<option value="0">Ninguno</option>
        	<?php
				$consulta = "select id,descripcion from encuestacatrubro where estatus=0";
				$R = sqlsrv_query( $conexion_srv,$consulta);
				while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
				{
					if($idrubro== $row['id'])
						echo "<option value='". $row['id']."' selected>".$row['descripcion']."</option>";
					else
						echo "<option value='". $row['id']."' >".$row['descripcion']."</option>";
				}
			?>	
            </select>
        </td>
    </tr>
     	<tr><td colspan="3">&nbsp;</td></tr>
        <tr>
    	<td class="titles">Situaci&oacute;n:</td>
        <td><input type="checkbox" name="estatus" id="estatus" <?php if($estatus<90) echo "checked";?>>Activo</td>
    </tr>   
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Guardar" onclick="javascript: guardacatpregunta()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='catpregunta.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>