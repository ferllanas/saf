<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$datos=array();
$usuario="1981";//$_COOKIE['ID_my_site'];
$id=0;
if(isset($_REQUEST['pid']))
{
	$id=$_REQUEST['pid'];
}
$descripcion="";
if(isset($_POST['pdescripcion']))
{
	$descripcion=$_POST['pdescripcion'];
}
$idsecretaria="";
if(isset($_POST['pidsecretaria']))
{
	$idsecretaria=$_POST['pidsecretaria'];
}
$idrubro="";
if(isset($_POST['pidrubro']))
{
	$idrubro=$_POST['pidrubro'];
}
$pestatus="";
if(isset($_POST['pestatus']))
{
	$pestatus=$_POST['pestatus'];
}
if($pestatus)
	$estatus=0;
else
	$estatus=90;


list($ids, $fecha)= fun_serv_A_mservicio($id,$estatus,$descripcion,$idsecretaria,$idrubro,$usuario); 
if(strlen($ids)>0)//Para ver si en realidad se genero un servicio
{
	$fails=false;
	$msgerror="Pregunta Registrada Satisfactoriamente";
}
else
{	
	$fails=true; 
	$msgerror="No pudo generar ningun Diagnostico";
}
	$datos['id']=trim($ids);
	$datos['fecha']=trim($fecha);
	$datos['fallo']=$fails;	
	$datos['error']=$msgerror;
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	
function fun_serv_A_mservicio($id,$estatus,$descripcion,$idsecretaria,$idrubro,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_encuesta_A_catpregunta(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$estatus,&$descripcion,&$idsecretaria,&$idrubro,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_encuesta_A_catpregunta".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];		
		}
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return array($id,$fecha);
}

?>
