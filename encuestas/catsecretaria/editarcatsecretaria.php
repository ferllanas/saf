<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$titulo="Agregar Servicio";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	$titulo="Editar Servicio";
}


$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM encuestacatsecretaria where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$descripcion = trim($row['descripcion']);
			$titular=$row['titular'];
			$calle=$row['calle'];
			$colonia=$row['colonia'];
			$numero=$row['numero'];
			$tel1=$row['tel1'];
			$tel2=$row['tel2'];
			$tel3=$row['tel3'];
			$contacto=$row['contacto'];
			$estatus=$row['estatus'];
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script type="text/javascript" src="../catsecretaria/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../catsecretaria/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../catsecretaria/javascript/catsecretaria.js"></script>

<!-- Load jQuery build -->
<script type="text/javascript" src="../javascript/tinymce_3.5.7_jquery/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init
({
	 mode : "textareas",
	theme : "advanced",
	        theme_advanced_buttons1 : "bold, italic, underline, bullist,numlist ",
	        theme_advanced_toolbar_location: "top",
	        theme_advanced_toolbar_align: "left"	
})
</script>

<!-- Rutina para cargar imagen previa  -->
<script type="text/javascript" >
$(document).ready(function() { 
		    $('#photoimg1').live('change', function()
			{ 
				$("#preview1").html('');
				$("#preview1").html('<img src="ajaximage/loader.gif" alt="Uploading...."/>');
				$("#imageform1").ajaxForm({
					target: '#preview1'
					}).submit();

				});
			});
</script>
<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Discapacidad</title>
</head>

<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Sección De Proyecto -------- -->
<table width="100%">
<tr>
            <td width="5%" ><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /></td>
    </tr> 
    <tr>
    	<td  class="titles">Descripción:</td>
        <td><input style="size:300px" type="text" name="descripcion" id="descripcion" value="<?php echo $descripcion;?>"/></td>
       	<td class="titles">Titular:</td>
        <td ><input type="text" name="titular" id="titular" value="<?php echo $titular;?>"/></td>
    </tr>
     <tr>
    	<td class="titles">Colonia:</td>
        <td><input type="text" name="colonia" id="colonia" value="<?php echo $colonia;?>"/></td>
    	<td class="titles">Calle:</td>
        <td><input type="text" name="calle" id="calle" value="<?php echo $calle;?>"/></td>
    	<td class="titles">Numero:</td>
        <td><input type="text" name="numero" id="numero" value="<?php echo $numero;?>"/></td>
    </tr>
     <tr>
    	 <td class="titles">Telefono:</td>
        <td><input type="text" name="tel1" id="tel1" value="<?php echo $tel1;?>"/></td>
    	<td class="titles">Telefono:</td>
        <td><input type="text" name="tel2" id="tel2" value="<?php echo $tel2;?>"/></td>
    	<td class="titles">Telefono:</td>
        <td><input type="text" name="tel3" id="tel3" value="<?php echo $tel3;?>"/></td>
    </tr>
    <tr>
    	<td  class="titles">Contacto:</td>
        <td ><input type="text" name="contacto" id="contacto" value="<?php echo $contacto;?>"/></td>
    </tr>
     	<tr><td colspan="8">&nbsp;</td></tr>
        <tr>
    	<td class="titles">Situaci&oacute;n:</td>
        <td class="titles"><input type="checkbox" name="estatus" id="estatus" <?php if($estatus<90) echo "checked";?>>Activo</td>
    </tr>   
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Guardar" onclick="javascript: guardacatsecretaria()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='catsecretaria.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>