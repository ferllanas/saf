<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');


$usuario=$_COOKIE['ID_my_site'];

$id="";
if(isset($_REQUEST['pid']))
{
	$id=$_REQUEST['pid'];
}
$iddiagnostico="0";

$observa="";
if(isset($_POST['pobserva']))
{
	$observa=$_POST['pobserva'];
}

$command="select id from serviciomdiagnostico where idservicio=$id";
$R = sqlsrv_query( $conexion_srv,$command);
while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
{
	$iddiagnostico= $row['id'];
			
}
list($id_asig, $fecha)= fun_serv_A_final($id,$iddiagnostico,				
				$observa,
				$usuario);
if(strlen($id_asig)>0)//Para ver si en realidad se genero un servicio
{	
	
	$fails=false;
	$msgerror="Servicio $id Reactivado";
}
else
{	
	$fails=true; 
	$msgerror="No se pudo Reactivar el Servicio $id";
}
$datos['fecha']=trim($fecha);
$datos['fallo']=$fails;	
$datos['error']=$msgerror;
echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	
function fun_serv_A_final($id,$iddiagnostico,$observa,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_servicio_A_reactiva(?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$iddiagnostico,&$observa,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_servicio_A_reactiva".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];		
		}
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return array($id,$fecha);
}						
?>
