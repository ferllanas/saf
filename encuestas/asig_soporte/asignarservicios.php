<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$idant="0";
$pre="";

if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	
	if(isset($_REQUEST['estatus']) && $_REQUEST['estatus']>0)
	{
		$titulo="Reasignar Tecnico a la Solicitud $id";
		$command="select id,numemp from servicioasigna where idservicio=$id and estatus=0";
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$idant = trim($row['id']);
				$tecant = trim($row['numemp']);
				$pre="Reasignado Por";
			}
		}
	}
	else
		$titulo="Agregar Tecnico a la Solicitud $id";
}
$nombre="";

$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="select a.id,a.numemp,b.nomemp as nombre,
a.numpatrimonio,a.estatus,a.observa,CONVERT(varchar(10),a.falta,103) as fecha,
CONVERT(varchar(10),a.hora,108) as hora,rtrim(ltrim(c.descrip)) + ' ' + rtrim(ltrim(c.marca)) as descripcion from serviciomservicios a 
left join v_nomina_empleadosactivos b on a.numemp=b.numemp collate database_default
left join fome2011.dbo.activodmuebles c on a.numpatrimonio=c.folio where a.id=$id and a.estatus<90";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$id=trim($row['id']);
			
			$nombreS = trim($row['nombre']);
			$detalle = trim($row['observa']);
			$equipo = trim($row['descripcion']);			
			$estatus=$row['estatus'];
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script type="text/javascript" src="../servicios/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../servicios/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../asig_soporte/javascript/asigservicios.js"></script>


<!-- Load jQuery build -->
<script type="text/javascript" src="../javascript/tinymce_3.5.7_jquery/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init
({
	 mode : "textareas",
	theme : "advanced",
	        theme_advanced_buttons1 : "bold, italic, underline, bullist,numlist ",
	        theme_advanced_toolbar_location: "top",
	        theme_advanced_toolbar_align: "left"	
})
</script>

<!-- Rutina para cargar imagen previa  -->

<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->

<div id="" class="titles"><?php echo $titulo;?></div><br />


<!-- -------- Sección De Proyecto -------- -->
<table width="100%">	 
   
    <tr>
            <td width="121"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /><input name="idant" id="idant" type="hidden" value="<?php echo $idant;?>" /><input name="tecant" id="tecant" type="hidden" value="<?php echo $tecant;?>" /></td>
    </tr> 
   
   
<tr>
      <td width="121" height="39" class="titles">Tecnico: </td> 
      <td class="titles"><select name="tec" id="tec" size="7" style="width:350px">
      <?php
		if ($conexion_srv)
		{
			$descrip="";
			$command= "SELECT a.numemp,rtrim(ltrim(a.nombre)) + ' ' + rtrim(ltrim(a.appat)) + ' ' + rtrim(ltrim(a.apmat)) as nombres,COUNT(b.id) as pendientes 
						FROM nomemp.dbo.nominadempleados a left join servicioasigna b on a.numemp collate database_default =b.numemp and b.estatus=0 
						where a.coord='1230' and a.estatus<'90'  group by a.numemp,a.nombre,a.appat,a.apmat order by pendientes desc,nombres asc";
			$R = sqlsrv_query( $conexion_srv,$command);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$numempT= $row['numemp'];
				$descripT= $row['pendientes']." - ".$row['nombres'];				
				echo "<option class='titles' value='$numempT'>$descripT</option>";							
			}			
		}
	?>	
      </select></td>
     
          
</td>
     </tr>  
     <tr>
    	<td class="titles">Solicitante:</td>
        <td colspan="8" width="1094"><input type="text" tabindex="4" style="width:350px" name="sol" id="sol" value="<?php echo $nombreS;?>" readonly="readonly"/> </td>
    </tr>    
      <tr>
    	<td class="titles">Equipo:</td>
        <td colspan="8" width="1094"><input type="text" tabindex="5" style="width:350px" name="dpat" id="dpat" value="<?php echo $equipo;?>" readonly="readonly"/> </td>
    </tr>  
    <tr>
    <td class="titles">Posible Falla:</td>
    <?php
		if ($conexion_srv)
		{
			$descrip="";
			$consulta = "select a.id,b.Descripcion from serviciodservicios a left join catservicios b on a.idcatservicio=b.id where idservicio=$id";
			$R = sqlsrv_query( $conexion_srv,$consulta);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id= $row['id'];
				$descrip.= $row['Descripcion']." - ";				
			}
			echo "<td class='titles'>$descrip </td>";
		}
	?>					
    </tr>    
      <tr>
    	<td class="titles">Detalle:</td>
        <td colspan="8" width="1094"><textarea cols="100" tabindex="6" rows="3" name="detalle" id="detalle" readonly="readonly"><?php echo $detalle;?></textarea></td>
    </tr>   
     <tr>
    	<td class="titles">Observaciones:</td>
        <td colspan="8" width="1094"><textarea tabindex="2" cols="100" rows="3" name="observa" id="observa" ><?php echo $pre;?></textarea></td>
    </tr>   
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Guardar" onclick="javascript: guardaservicios()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='../servicios/servicios.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>