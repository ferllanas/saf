<?php 
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$conexion = sqlsrv_connect($server,$infoconexion);	
	

$visita="";
$area="";
if(isset($_REQUEST['visita']) && !isset($_REQUEST['area']))
{
	$visita=$_REQUEST['visita'];
	$consulta="SELECT a.numemp, a.titulo, a.nombre, a.appat, a.apmat, a.nivel, b.nomnivel, a.depto, a.coord, a.dir , c.nomdepto, d.email, d.email_fome
 FROM nomemp.dbo.nominadempleados a 
 LEFT JOIN nomemp.dbo.nominamnivel b ON a.nivel = b.nivel
 LEFT JOIN nomemp.dbo.nominamdepto c ON a.depto=c.depto
 LEFT JOIN fomedbe.dbo.usuarios d ON d.numemp=a.numemp COLLATE Modern_Spanish_CI_AS
 WHERE a.depto=( SELECT  TOP 1 idareadestino COLLATE Traditional_Spanish_CI_AS FROM [fomedbe].[dbo].Movimientos 
					WHERE idregistro=$visita ORDER BY id DESC ) AND
					a.estatus<90 
				OR 
				( a.dir=(SELECT dir FROM nomemp.dbo.nominamdepto WHERE depto=(SELECT  TOP 1 idareadestino COLLATE Traditional_Spanish_CI_AS FROM [fomedbe].[dbo].Movimientos 
					WHERE idregistro=$visita ORDER BY id DESC) ) AND a.nivel='0200' AND
					a.estatus<90 )
				OR ( a.coord=(SELECT coord FROM nomemp.dbo.nominamdepto WHERE depto=(SELECT  TOP 1 idareadestino COLLATE Traditional_Spanish_CI_AS FROM [fomedbe].[dbo].Movimientos 
					WHERE idregistro=$visita ORDER BY id DESC) ) AND a.nivel='0300' AND
					a.estatus<90 )  GROUP by a.numemp, a.titulo, a.nombre, a.appat, a.apmat, a.nivel, b.nomnivel, a.depto, a.coord, a.dir , c.nomdepto, d.email, d.email_fome
					
					ORDER BY a.nivel where status='S'";
}
else
{
	$area=$_REQUEST['area'];
	$consulta="SELECT a.numemp, a.titulo, a.nombre, a.appat, a.apmat, a.nivel, b.nomnivel, a.depto, a.coord, a.dir , c.nomdepto, d.email, d.email_fome
 FROM nomemp.dbo.nominadempleados a 
 LEFT JOIN nomemp.dbo.nominamnivel b ON a.nivel = b.nivel
 LEFT JOIN nomemp.dbo.nominamdepto c ON a.depto=c.depto
 LEFT JOIN fomedbe.dbo.usuarios d ON d.numemp=a.numemp COLLATE Modern_Spanish_CI_AS
 WHERE a.depto= $area AND
					a.estatus<90 
				OR 
				( a.dir=(SELECT dir FROM nomemp.dbo.nominamdepto WHERE depto=$area ) AND a.nivel='0200' AND
					a.estatus<90 )
				OR ( a.coord=(SELECT coord FROM nomemp.dbo.nominamdepto WHERE depto=$area ) AND a.nivel='0300' AND
					a.estatus<90 )  
				GROUP by a.numemp, a.titulo, a.nombre, a.appat, a.apmat, a.nivel, b.nomnivel, a.depto, a.coord, a.dir , c.nomdepto, d.email, d.email_fome
				ORDER BY a.nivel where estatus='S'";
}





$getProducts = sqlsrv_query( $conexion,$consulta);//$dbRes = odbc_exec($conexion_db, $consulta);$idvisita
			
//echo $consulta;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<title>Selección para envio de correo</title>
<script type="text/javascript">
function makeSelection(frm, id) {
	
	if(!frm || !id)
		return;
	
	var val ="";
	for(i=0;i<document.getElementsByName(id).length;i++)
	{
		if(document.getElementsByName(id)[i].checked)
			val += document.getElementsByName(id)[i].value+";";
	}
	//alert(val);
	//var val = elem.options[elem.selectedIndex].value;
	opener.targetElement.value = val;
	this.close();
}
</script>
</head>

<body>
 <table width="100%" align="center">
    	<tr>
			<td align="center" colspan="12" class="TituloDForma">Selección para envio de correo<hr class="hrTitForma"></td>
        </tr> 
    </table >
 <form>
<table align="center" width="100%">
	<?php
	$yapaso=false;
	$lasnivel="0200";
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))//while($row = odbc_fetch_object($dbRes))
	{
		
		if($lasnivel!=trim($row['nivel']))
		{
			?>
            	</tr>
                </table>
                </td>
                </tr>
            <?php
			$yapaso=false;
			$lasnivel=trim($row['nivel']);
		}
		
		if($yapaso)
		{
			?>
			 <td align="center" class="texto8"><u><?php if(strlen(trim($row['email_fome']))>0){ ?>
             											<input type="checkbox" name="emails" id="emails" value="<?php echo trim($row['email_fome']);?>" />
													<?php } ?>
													<?php echo $row['titulo']." ".$row['nombre']." ".$row['appat']." ".$row['apmat']." ";?></u><br /><?php if($lasnivel>'0400') echo trim($row['nomnivel']); else echo trim($row['nomdepto']);?></td>
			<?php
		}
		else
		{
			?>
			<tr>
				<td>
					<table  width="100%">
						<tr>
							<td align="center" class="texto8"><u><?php if(strlen( trim($row['email_fome']))>0){ ?><input type="checkbox" name="emails" id="emails" value="<?php echo  trim($row['email_fome']);?>" /><?php } ?><?php echo $row['titulo']." ".$row['nombre']." ".$row['appat']." ".$row['apmat']." ";?></u><br /><?php if($lasnivel>'0400') echo trim($row['nomnivel']); else echo trim($row['nomdepto']);?></td>
			<?php
			$yapaso=true;
		}
			
	}
	?>
</table>
<table width="100%">
	<tr>
    	<td align="center" ><input type="button" value="Aceptar" onclick="makeSelection(this.form, 'emails');" align="middle"></td>
    </tr>
</table>

</form>
</body>
</html>