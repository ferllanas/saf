<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo $_GET['q'];
if($_REQUEST['visita'] && $conexion)
{	
	$visita=$_REQUEST['visita'];
	$emailCopias=$_REQUEST['emailCopias'];
	
	$command= " UPDATE [fomedbe].[dbo].[Movimientos] SET [recordatorios]=[recordatorios]+1 WHERE id=(SELECT TOP 1 id visitas FROM Movimientos WHERE idregistro=$visita ORDER BY id DESC)";			
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	sqlsrv_free_stmt( $getProducts );
	
	
	$idareadestino="";
	$observaciones="";
	$numvisita="";
	$usuariolog=$_COOKIE['Nombre'];
	$nomAreaDeTramite="";
	$nombre="";
	$email="";
	$responsable="";
	$emailCopias="";
	
	$command= "SELECT TOP 1 a.idarea, a.observa, idvisita, c.nomdepto, a.idareadestino,
			(LTRIM(d.nombres)+' '+LTRIM(d.appat)+' '+LTRIM(d.apmat)) as nombre 
		FROM fomedbe.dbo.Movimientos a 
		LEFT JOIN fomedbe.dbo.CatTramite b ON a.idtramite=b.idtramite 
		LEFT JOIN nomemp.dbo.nominamdepto c ON b.idarea=c.depto COLLATE Modern_Spanish_CI_AS 
		LEFT JOIN fomedbe.dbo.Contactos d ON d.IDContacto=a.idcontacto
		WHERE a.idregistro=$visita ORDER BY a.id DESC";			
	
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		echo $command;
	}
	else
	{	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$idareadestino = trim($row['idareadestino']);
			$observaciones =  trim($row['observa']);
			$numvisita = trim($row['idvisita']);
			$nomAreaDeTramite =  trim($row['nomdepto']);
			$nombre =  trim($row['nombre']);
			//echo $nombre;
			//$datos[$i]['nomarea'] =  trim($row['nomdepto']);
			$i++;
		}	
		
	}
	
	enviaremail($idareadestino,$observaciones, $visita, $usuariolog, '', $nombre, $email , $responsable, $emailCopias);
	
	$datos[0]['error']="0";
	$datos[0]['string']="OK";
}
else
{
	$datos[0]['error']="1";
	$datos[0]['string']="no hay conexion";
}
echo json_encode($datos);


function enviaremail($idarea,$observaciones, $numvisita, $usuariolog, $nomAreaDeTramite, $nombre, $email , $responsable, $emailCopias)
{
	global $server,$infoconexion;
	
	include("../../PHPMailer_v5.1/class.phpmailer.php");
	include("../../PHPMailer_v5.1/class.smtp.php"); // note, this is optional - gets called from main class if not already loaded

$emails= array('fernando.llanas@fomerrey.gob.mx');

//echo "A1";
$conexion = sqlsrv_connect($server,$infoconexion);

//Obtiene area que envia
//echo "!".$nomAreaDeTramite."!";
if($nomAreaDeTramite=="")
{
	$consulta = "SELECT d.nomdepto, c.email, c.email_fome ,c.Nombre 
					FROM nomemp.dbo.nominadempleados b 
					INNER JOIN fomedbe.dbo.usuarios c ON b.numemp=c.numemp COLLATE Traditional_Spanish_CI_AS
					INNER JOIN nomemp.dbo.nominamdeptoNew d ON d.depto=b.depto
						WHERE c.usuario ='".$_COOKIE['ID_my_site']."'";
	//echo $consulta;
	$stmt = sqlsrv_query($conexion, $consulta);
	if( $stmt === false )
	{
		 die( print_r( sqlsrv_errors(), true));
	}	
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))
		{
			$nomAreaDeTramite=trim($row['nomdepto']);
		}
	}
}


$envioVarios=false;
if($email==""  && $responsable=="")
{
	$tsql_callSP = "SELECT c.email, c.email_fome ,c.Nombre FROM nomemp.dbo.nominadempleados b INNER JOIN fomedbe.dbo.usuarios c ON b.numemp=c.numemp COLLATE Traditional_Spanish_CI_AS
		WHERE b.depto =$idarea";
	//echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP);
	if( $stmt === false )
	{
		 die( print_r( sqlsrv_errors(), true));
	}	
	$envioVarios=true;
}


$fails=false;


$body             = /*file_get_contents('invitacion.php?condPagoDias=30&adqparcial=si&requisi=$requisi');$mail->getFile('contents.html');"/*/
"
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
</head>
<body>
<table width='100%'>
	<tr>
    	<td><H1 align='center'>Tecnologia de la informaci&oacute;n</H1></td>
    </tr>
    <tr>
    	<td><h3 align='center'>Modulo de Atenci&oacute;n a clientes</h3></td>
    </tr>
    <tr>
      <td>Usted tiene un tramite pendiente con n&uacute;mero &quot;$numvisita&quot; de $nombre.<br> A continuaci&oacute;n las observaciones/indicaciones del Tramite:			     </td>
    </tr>
    <tr>
    	<td>$observaciones</td>
    </tr>
     <tr>
    	<td>&nbsp;</td>
    </tr>
     <tr>
    	<td>&nbsp;</td>
    </tr>
	<tr>
		<td>Atte.  $usuariolog</td>
	<tr>
    	<td>$nomAreaDeTramite</td>
    </tr></td>
	</tr>
    <tr>
    	<td bgcolor='#FFCC00' style='font-size:11px'>Usted puede atender esta solicitud ingresando <a href='http://200.94.201.196/fome/'>al SIIF en linea</a>. En el modulo de Visitantes, en la opci&oacute;n &quot;Buscar Visitante&quot; o &quot;Control de Visitas&quot;.</td>
    </tr>
    <tr>
    	<td bgcolor='#FFCC99' style='font-size:11px'>Si usted no tiene usuario o contrase&ntilde;a para ingresar al sistema favor de solicitarlo al area de Informatica.</td>
    </tr>
</table>
</body>
</html>
";
//echo $body ;
	//$body             = eregi_replace("[\]",'',$body);
	$body =str_replace("[\]",'',$body);

	//echo $body;
	$emailsenviado="";
	//$mail->AddAttachment("/path/to/file.zip");             // attachment
	//$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment
	$mail             = new PHPMailer();
	$numcuenta=0;
	$idssList="";
	//for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) 
	//{
		
		$i=1;
		if($envioVarios)
		{
			//echo "AAQ";
			while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))//for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) 
			{
				//print_r($row);
				$mail->IsSMTP();
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
				$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
				$mail->Port       = 2525;//465;// set the SMTP port
				
				$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
														//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
				$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
				
				$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
				$mail->FromName   = "Tecnologia de la Informacion";
				$mail->Subject    = "Tecnologia de la Informacion";
				$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
				$mail->WordWrap   = 50; // set word wrap
				
				$mail->MsgHTML($body);
				
				$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
				//"First Last");
				//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
				$mail->IsHTML(true); // send as HTML
				$mail->AddAddress(trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
				
				//echo $emailCopias;
				$word = split(';',$emailCopias);
				for($j=0;$j<count($word);$j++)
				{	
					
					if(strlen($word[$j])>0)
					{
						//echo "<br>JJJ".$word[$j];
						$mail->AddCC($word[$j]);
					}
				}
				

				$emailsenviado .= trim($row['email_fome'])." XXX<br>";
				
				
				
				//$idssList.= trim($row['id']);
				if(!$mail->Send()) 
				{
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} 
				
				$mail  = new PHPMailer();
				
				$numcuenta++;
				if($numcuenta>=count($emails))
				{
					$numcuenta=0;
				}
				
				$i++;
			}
		}
		else
		{
			//echo "BBQ";
			$mail->IsSMTP();
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
			$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
			$mail->Port       = 2525;//465;// set the SMTP port
			
			$mail->Username   = "ti@fomerrey.gob.mx";//$emails[$numcuenta];//"jfllanas@gmail.com";//"amigosdegustavocaballero@gmail.com";
													//"fernando.llanas@fomerrey.gob.mx";// GMAIL username
			$mail->Password   = "F150ti";//"fomerrey27";//"salvadorllanas27";//"F163fer";            // GMAIL password
			
			$mail->From       = $emails[$numcuenta];//"fernando.llanas@fomerrey.gob.mx"; //
			$mail->FromName   = "Tecnologia de la Informacion";
			$mail->Subject    = "Tecnologia de la Informacion";
			$mail->AltBody    = "Tecnologia de la Informacion"; //Text Body
			$mail->WordWrap   = 50; // set word wrap
			
			$mail->MsgHTML($body);
			
			$mail->AddReplyTo("ti@fomerrey.gob.mx" , "Tecnologia de la Informacion");
			//"First Last");
			//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
			$mail->IsHTML(true); // send as HTML
			$mail->AddAddress($email, $responsable);//trim($row['email_fome']), trim($row['Nombre']));//$data->sheets[0]['cells'][$i][$j],$data->sheets[0]['cells'][$i][$j]);
			//echo $emailCopias;
			$word = split(';',$emailCopias);
			for($j=0;$j<count($word);$j++)
			{	
				
				if(strlen($word[$j])>0)
				{
					echo "<br>JJJ".$word[$j];
					$mail->AddCC($word[$j]);
				}
			}
			
			$emailsenviado .= trim($email)." XXX<br>";
			//echo $emailsenviado;
			//$idssList.= trim($row['id']);
			if(!$mail->Send()) 
			{
			  echo "Mailer Error: " . $mail->ErrorInfo;
			} 
			
			$mail  = new PHPMailer();
			
			$numcuenta++;
			if($numcuenta>=count($emails))
			{
				$numcuenta=0;
			}
		}
}

?>