<?php
require_once("../connections/dbconexion.php");

//consulta_BuscaContactoOClientes('','','','Yolanada','Leal','','true','tipobnombres','Clientes',1); 
function consulta_BuscaContactoOClientes($fomerrey,$manzana,$lote,$nombre,$appat,$apmat,$solicitante, $tipobusqueda, $contactosOClientes, $pasadas)
{
	global $server,$infoconexion;
	$datosFLL=array();
	$entro=false;
	$comando="";
	$pasadasX = $pasadas+1;
	
	
	if($pasadasX>3)
	{
		//echo "me lleva ". $pasadasX;
		$datosFLL[0]['noResults']="noResults";
		//print_r($datosFLL);
		return array($pasadasX,$datosFLL);
	}
	else
	{
		//echo "me llev<br>";
		if($fomerrey=="" && $manzana=="" && $lote=="" && $nombre=="" && $appat=="" && $apmat=="" && $solicitante=="")
		{
			return array($pasadasX,$datosFLL);
		}


		$PreguntaPorfml=false;
		//echo $contactosOClientes;
		$conexion = sqlsrv_connect($server,$infoconexion);
		if($conexion)
		{
			//echo $pasadasX."<br>";
			if($contactosOClientes=='Clientes' && $tipobusqueda=='tipobfml')
			{
				$comando= armaQueryClinetesfml($fomerrey,$manzana, $lote, $comando, $entro, $PreguntaPorfml);
				//echo $comando;
			}
			
			//if($contactosOClientes=='Contactos' && $tipobusqueda=='tipobfml')
			//{
			//}
			
			if($contactosOClientes=='Clientes' && $tipobusqueda=='tipobnombres')
			{
				$comando= armaQueryClientesNombres($solicitante, $entro,$nombre, $appat, $apmat);
				//echo "BBB";
			}
			
			if($contactosOClientes=='Contactos' )//&& $tipobusqueda=='tipobnombres'
			{
				$comando=armaqueryContactosnombres($solicitante, $PreguntaPorfml, $nombre, $appat, $apmat,$fomerrey,$manzana, $lote);
				//echo "AAA";
			}
			//echo $comando;
			//if($entro==false)
			//	$comando="SELECT TOP 100 * from [fomedbe].[dbo].rpt_fml_solicitante_conyuge";
			
			if($contactosOClientes=='Clientes')
				$datosFLL = ObtenResultadosClientes( $conexion,$comando,$solicitante );
			else
				$datosFLL = ObtenResultadosContactos( $conexion,$comando,$solicitante );
			
			//print_r($datosFLL);
			
			if(count($datosFLL)<=0)
			{
					//echo "XXXX".$contactosOClientes.",".$tipobusqueda;
				if($contactosOClientes=='Clientes' && $tipobusqueda=='tipobfml')
				{	
					//echo "Debe pasar por aca " . $pasadasX."<br>";
					list($a,$b)=consulta_BuscaContactoOClientes($fomerrey,$manzana,$lote,$nombre,$appat,$apmat,$solicitante, $tipobusqueda, "Contactos", $pasadasX);
					return array($a,$b);
				}
				if($contactosOClientes=='Clientes' && $tipobusqueda=='tipobnombres')
				{	
					//echo "Debe pasar por aca " . $pasadasX."<br>";
					list($a,$b)=consulta_BuscaContactoOClientes($fomerrey,$manzana,$lote,$nombre,$appat,$apmat,$solicitante, $tipobusqueda, "Contactos", $pasadasX);
					return array($a,$b);
				}
				if($contactosOClientes=='Contactos' && $tipobusqueda=='tipobnombres')
				{	
					//echo "Clientes Debe pasar por aca " . $pasadasX."<br>";
					list($a,$b)=consulta_BuscaContactoOClientes($fomerrey,$manzana,$lote,$nombre,$appat,$apmat,$solicitante, $tipobusqueda, "Clientes", $pasadasX);
					return array($a,$b);
				}
				
				if($contactosOClientes=='Contactos' && $tipobusqueda=='tipobfml')
				{
					//echo "asasas";
					list($a,$b)=consulta_BuscaContactoOClientes($fomerrey,$manzana,$lote,$nombre,$appat,$apmat,$solicitante, $tipobusqueda, "Clientes", $pasadasX);
					return array($a,$b);
				}
				
			}
			else
				return array($pasadasX,$datosFLL);
		}
		else
		{
				$datosFLL[0]['error']="1";
				$datosFLL[0]['string']="no hay conexion";
		}
	}
}
	
function armaQueryClinetesfml($fomerrey,$manzana, $lote, $comando, $entro, $PreguntaPorfml)
{
				$comando="SELECT * from [fomedbe].[dbo].rpt_fml_solicitante_conyuge";
				
				$entro=false;
				if(isset($fomerrey) && strlen($fomerrey)>0)
				{
					if(isset($manzana) && strlen($manzana)>0)
					{
						if(isset($lote) && strlen($lote)>0)
						{
							$comando.=" WHERE fml='$fomerrey$manzana$lote'";
							 $entro=true;
							 $PreguntaPorfml=true;
						}
						else
						{
							$comando.=" WHERE fml LIKE '%$fomerrey$manzana%'";
							 $entro=true;
							 $PreguntaPorfml=true;
						}
					}
					else
					{
						$comando.=" WHERE fml LIKE '%$fomerrey%'";
						$entro=true;
						$PreguntaPorfml=true;
					}
				}
	return $comando;
}

function armaQueryClientesNombres($solicitante, $entro,$nombre, $appat, $apmat )
{
	$comando="SELECT TOP 100 * from [fomedbe].[dbo].rpt_fml_solicitante_conyuge";
	if(isset($solicitante) && strlen($solicitante)>0)
	{
		if($entro==true)
		{
			if($solicitante=='AMBOS')
			{
				$comando.=" WHERE (solnombre ='$nombre' AND solappat = '$appat' AND solapmat = '$apmat' ) OR (conynombre ='$nombre' AND conyappat ='$appat' AND conyapmat = '$apmat')";
			}
			else
			{
				if($solicitante=='true')
				{
					if(isset($nombre) && strlen($nombre)>0 )
					{
						$comando.=" AND solnombre like '%$nombre%'";
						$entro=true;
					}
					
					if(isset($appat) && strlen($appat)>0 )
					{
						$comando.=" AND solappat like '%$appat%'"; 
						$entro=true;
					}
					
					if(isset($apmat) && strlen($apmat)>0 )
					{
						$comando.=" AND solapmat like '%$apmat%'"; 
						$entro=true;
					}
				}
				else
				{
					if(isset($nombre) && strlen($nombre)>0 )
					{
						$comando.=" AND conynombre like '%$nombre%'";
						$entro=true;
					}
					
					if(isset($appat) && strlen($appat)>0 )
					{
						$comando.=" AND conyappat like '%$appat%'"; 
						$entro=true;
					}
					
					if(isset($apmat) && strlen($apmat)>0 )
					{
						$comando.=" AND conyapmat like '$apmat'"; 
						$entro=true;
					}
				}
			}
		}
		else
		{
			if($solicitante=='AMBOS')
			{
				$comando.=" WHERE (solnombre like '%$nombre%' AND solappat like '%$appat%' AND solapmat like '%$apmat%' ) OR (conynombre like '%$nombre%' AND conyappat like '%$appat%' AND conyapmat like '%$apmat%')";
				$entro=true;
			}
			else
			{
				if($solicitante=='true')
				{
					if(isset($nombre) && strlen($nombre)>0 )
					{
						$comando.=" WHERE solnombre like '%$nombre%'";
						$entro=true;
					}
					
					if(isset($appat) && strlen($appat)>0 )
					{
						if($entro==true)
							$comando.=" AND solappat like '%$appat%'"; 
						else
							$comando.=" WHERE solappat like '%$appat%'"; 
						$entro=true;
					}
					
					if(isset($apmat) && strlen($apmat)>0 )
					{
						if($entro==true)
							$comando.=" AND solapmat like '%$apmat%'"; 
						else
							$comando.=" WHERE solapmat like '%$apmat%'"; 
						$entro=true;
					}
				}
				else
				{
					if(isset($nombre) && strlen($nombre)>0 )
					{
						$comando.=" WHERE conynombre like '%$nombre%'";
						$entro=true;
					}
					
					if(isset($appat) && strlen($appat)>0 )
					{
						if($entro==true)
							$comando.=" AND conyappat like '%$appat%'"; 
						else
							$comando.=" WHERE conyappat like '%$appat%'"; 
							$entro=true;
					}
					
					if(isset($apmat) && strlen($apmat)>0 )
					{
						if($entro==true)
							$comando.=" AND conyapmat like '%$apmat%'"; 
						else
							$comando.=" WHERE conyapmat like '%$apmat%'"; 
							$entro=true;
					}
				}
			}
		}
	}
	return $comando;
}

function armaqueryContactosnombres($solicitante, $PreguntaPorfml, $nombre, $appat, $apmat, $fomerrey,$manzana, $lote)
{
	if($solicitante!='AMBOS' )//&& $PreguntaPorfml==false
	{
		$comando="SELECT *, CONVERT(VARCHAR(10), fec_nac, 120) as fechanacimiento from contactos a ";
		//echo $comando;
		$entro=false;
		
		if(isset($nombre) && strlen($nombre)>0 )
		{
			$comando.=" WHERE nombre like '%$nombre%'";
			$entro=true;
		}
					
		if(isset($appat) && strlen($appat)>0 )
		{
			if($entro==true)
				$comando.=" AND appat like '%$appat%'"; 
			else 
				$comando.=" WHERE appat like '%$appat%'"; 
			$entro=true;
		}
					
		if(isset($apmat) && strlen($apmat)>0 )
		{
			if($entro==true)
				$comando.=" AND apmat like '%$apmat%'"; 
			else 
				$comando.=" WHERE apmat like '%$apmat%'"; 
			$entro=true;
		}
		
		if(strlen($fomerrey)>0 && strlen($manzana) && strlen($lote))
		{
			if($entro==true)
				$comando.=" AND fml = '".$fomerrey."".$manzana."".$lote."'"; 
			else 
				$comando.=" WHERE fml = '".$fomerrey."".$manzana."".$lote."'"; //0043-0134-0027
			$entro=true;
		}
		
		if($entro==false)
			$comando="SELECT TOP 100 *, CONVERT(VARCHAR(10), fec_nac, 120) as fechanacimiento from contactos ";
		
		//echo $comando;
	}
	return $comando;
}
function ObtenResultadosClientes( $conexion,$comando, $solicitante )
{
	$datos=array();
			$getProducts = sqlsrv_query( $conexion,$comando);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				$i=0;
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					if($solicitante=='AMBOS')
					{
						$datos[$i]['fml'] = rtrim(ltrim($row['fml']));
							$datos[$i]['nts'] =  rtrim(ltrim($row['nts']));
							$datos[$i]['folioese'] =  rtrim(ltrim($row['folioese']));
							$datos[$i]['solnombre'] =  rtrim(ltrim($row['solnombre']));
							$datos[$i]['solappat'] =  rtrim(ltrim($row['solappat']));
							$datos[$i]['solapmat'] =  rtrim(ltrim($row['solapmat']));
							$fec_nac = rtrim(ltrim($row['fechnacsol']));
							$fec_nac = substr($fec_nac,0,4)."/".substr($fec_nac,4,2)."/".substr($fec_nac,6,2);
							$datos[$i]['solfecnac'] =  $fec_nac ;
							$datos[$i]['IDContacto'] =  "0";
					}
					else
					{
						if($solicitante=='true')
						{
							$datos[$i]['fml'] = rtrim(ltrim($row['fml']));
							$datos[$i]['nts'] =  rtrim(ltrim($row['nts']));
							$datos[$i]['folioese'] =  rtrim(ltrim($row['folioese']));
							$datos[$i]['solnombre'] =  rtrim(ltrim($row['solnombre']));
							$datos[$i]['solappat'] =  rtrim(ltrim($row['solappat']));
							$datos[$i]['solapmat'] =  rtrim(ltrim($row['solapmat']));
							$fec_nac = rtrim(ltrim($row['fechnacsol']));
							$fec_nac = substr($fec_nac,0,4)."/".substr($fec_nac,4,2)."/".substr($fec_nac,6,2);
							$datos[$i]['solfecnac'] =  $fec_nac ;
							$datos[$i]['IDContacto'] =  "0";
							$datos[$i]['direccion'] =  rtrim(ltrim($row['calle']))." ".rtrim(ltrim($row['colonia'])) ;
						}
						else
						{
							$datos[$i]['fml'] =  rtrim(ltrim($row['fml']));
							$datos[$i]['nts'] =  rtrim(ltrim($row['nts']));
							$datos[$i]['folioese'] =  rtrim(ltrim($row['folioese']));
							$datos[$i]['solnombre'] = rtrim(ltrim( $row['conynombre']));
							$datos[$i]['solappat'] =  rtrim(ltrim($row['conyappat']));
							$datos[$i]['solapmat'] =  rtrim(ltrim($row['conyapmat']));
							$fec_nac = rtrim(ltrim($row['fechnacony']));
							$fec_nac = substr($fec_nac,0,4)."/".substr($fec_nac,4,2)."/".substr($fec_nac,6,2);
							$datos[$i]['solfecnac'] =  $fec_nac ;
							$datos[$i]['IDContacto'] =  "0";
							$datos[$i]['direccion'] =  rtrim(ltrim($row['calle']))." ".rtrim(ltrim($row['colonia'])) ;
						}
					}
					$i++;
				}
			}
			sqlsrv_free_stmt( $getProducts );
	return $datos;
}

function ObtenResultadosContactos( $conexion,$comando, $solicitante )
{
	$i=0;
	$datos=array();
	$getProducts = sqlsrv_query( $conexion,$comando);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$counta=$i;
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$encontro=false;
			
			for( $j=0;$j<$counta;$j++ )//for($j=0 ; $j<$i && $encontro==false; $j++)
			{
				//echo "!".$datos[$j]['fml']."!==?".rtrim(ltrim($row['fml']))."?";
				if($datos[$j]['fml'] ==  rtrim(ltrim($row['fml'])))
				{
					$datos[$j]['IDContacto'] =  rtrim(ltrim($row['IDContacto']));
					$encontro=true;
				}
			}
			
			if($encontro==false)
			{
				$datos[$i]['fml'] = rtrim(ltrim($row['fml']));//"N";//
				$datos[$i]['nts'] =  rtrim(ltrim($row['nts']));//"N";//
				$datos[$i]['folioese'] = "N";//rtrim(ltrim($row['folioese']));// 
				$datos[$i]['solnombre'] =  rtrim(ltrim($row['nombres']));
				$datos[$i]['solappat'] =  rtrim(ltrim($row['appat']));
				$datos[$i]['solapmat'] =  rtrim(ltrim($row['apmat']));
				$datos[$i]['solfecnac'] = rtrim(ltrim($row['fechanacimiento']));
				$datos[$i]['IDContacto'] =  rtrim(ltrim($row['IDContacto']));
				$datos[$i]['direccion'] =  rtrim(ltrim($row['calle']))." ".rtrim(ltrim($row['exterior']))." ".rtrim(ltrim($row['colonia'])) ;
				$i++;
			}
		}
	}
	sqlsrv_free_stmt( $getProducts );
	return $datos;
}
?>