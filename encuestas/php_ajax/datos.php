<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo $_GET['q'];
if($_GET['idtramite'] && $conexion)
{	
	$tramite=$_GET['idtramite'];
	$command= "SELECT a.idtramite, a.nomtramite, a.idarea,a.desctramite,a.reqtramite,b.nomdepto  as nomarea
				FROM [fomedbe].[dbo].[catTramite] a 
				left join [nomemp].[dbo].[nominamdeptoNew] b on a.idarea=b.depto 
				where a.idtramite=$tramite AND a.estatus<90";			

	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['idtramite'] = trim($row['idtramite']);
			$datos[$i]['nomtramite'] =  trim($row['nomtramite']);
			$datos[$i]['desctramite'] =trim($row['desctramite']);
			$datos[$i]['reqtramite'] =  trim($row['reqtramite']);
			$datos[$i]['idarea'] =  trim($row['idarea']);
			$datos[$i]['nomarea'] =  trim($row['nomarea']);
			$i++;
		}	
		
	}
	sqlsrv_free_stmt( $getProducts );
}
else
{
	$datos[0]['error']="1";
	$datos[0]['string']="no hay conexion";
}
echo json_encode($datos);
?>