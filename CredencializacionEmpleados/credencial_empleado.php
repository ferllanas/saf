<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");

validaSession(getcwd());
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$usuario = '';
$opc=0;
if(isset($_REQUEST['usuario']))
	$usuario = $_REQUEST['usuario'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];
$datos=array();
if(isset($_REQUEST['opc']))
	$opc = $_REQUEST['opc'];
if ($conexion)
{		
	//if ($opc==1)
		$consulta = "SELECT TOP 1000 [numemp]
      ,[nombre]
      ,[appat]
      ,[apmat]
      ,[nompuesto]
      ,[fecha]
      ,[nomdepto]
  		FROM [nomemp].[dbo].[v_gafet_union]";
	/*   else	
		$consulta = "SELECT TOP 1000 [numemp]
      ,[nombre]
      ,[appat]
      ,[apmat]
      ,[nompuesto]
      ,[fecha]
      ,[nomdepto]
  		FROM [nomemp].[dbo].[v_gafet_union] ";*/

	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['numemp']= trim($row['numemp']);
		$datos[$i]['nombre']= trim($row['nombre'])." ".trim($row['appat'])."".trim($row['apmat']);
		$datos[$i]['nompuesto']= trim($row['nompuesto']);
		$datos[$i]['nomdepto']= trim($row['nomdepto']);
		$datos[$i]['fecha']= trim($row['fecha']);
		$i++;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Impresi&oacute;n de Gafete Institucional</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="javascript" src="../javascript_globalfunc/jQuery.js"></script>

<?php  if($dependencia=="fomerrey") { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>css/estilos.css">
 <?php  } ?>

<script language="javascript" src="javascript/usuariofuncion.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>
<!--  MENU NUEVO -->
<?php  if($dependencia!="fomerrey") { ?>
<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/component.css" />
<script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/modernizr.custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/jquery.dlmenu.js"></script>
 <script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>x-jquery-plugins/jq-cookie/jquery.cookie.js"></script>
<script>
    $(function() {
		
        $( '#dl-menu' ).dlmenu({
            animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
        });
    });
</script>
<?php } ?>
<!--  MENU NUEVO -->

<style type="text/css">    
	#logo_inicial{
		position:absolute;
		left:250px;
		z-index:-100;
	}
</style>
</head>
<?php if($dependencia!="fomerrey")include_once( getDirAtras(getcwd())."indexMenu.php");?>
<body>

<form name="form1" method="post" action="">
<table width="100%" height="43" border="0">
<tr>
    <td colspan="8" align="center" class="TituloDForma">Impresi&oacute;n de Gafete Institucional
      <hr class="hrTitForma">	</tr>
</tr>
  <tr>
    <td width="460" height="39" class="texto8">Nombre del Usuario  
      <div align="left" style="z-index:1; position:absolute; width:311px; top: 50px; left: 148px;">     
        <input name="provname1" type="text" class="texto8" id="provname1" style="width:100%;" tabindex="4"  onKeyUp="searchProveedor1(this);" value="<?php echo $nombre;?>" size="60" autocomplete="off">        
        <div id="search_suggestProv" style="z-index:2;" > </div>
		</div>
		</div>  
        <input class="texto8" tabindex="2" type="hidden" id="nombre" name="nombre" size="10">
      <td width="128"></td>
      <td width="102">
      <td width="58">
      <input name="button" type="button" class="texto8" onClick="busqueda();" value="Buscar">
</td>
  </tr>
</table>
</form>
<table width="100%" border="0" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
  <tr>
  	<!--<td width="64" class="subtituloverde"><div align="center"><input type="checkbox" id="clickALL"></div></td>-->
    <td width="64" class="subtituloverde"><div align="center"><strong>Num. Emp.</strong></div></td>
    <td width="411" class="subtituloverde"><div align="center"><strong>Nombre Empleado</strong></div></td>
  
  </tr>
  <tbody id="provsT" class="resultadobusqueda">
  <?php 
  for ($i=0;$i<count($datos);$i++)
  	  {
  ?>
		<tr class="fila" >
     <!-- <td><input type="checkbox" id="<?php echo $datos[$i]['numemp'];?>"></td>-->
      <td scope="row"><div align="center"><?php echo $datos[$i]['numemp']; $usuario=$datos[$i]['numemp'];?></div>
      <td onClick="javascript: location.href='jpegcam/htdocs/ProgramEspeciales.php?editar=true&usuario=<?php echo $datos[$i]['numemp']?>'"><div align="left"><?php echo $datos[$i]['nombre'];?>
      </div>  </td>
    </tr>
     <?php 
   }
   ?>	
	</tbody>
</table>

</body>
</html>

