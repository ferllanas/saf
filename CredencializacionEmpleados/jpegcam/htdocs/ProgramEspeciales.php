<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');

//print_r($_REQUEST);
//print_r($_COOKIE);
require_once("../../../connections/dbconexion.php");


$conexion = sqlsrv_connect($server,$infoconexion);


$usuario="";
if(isset($_REQUEST['usuario']))
	$usuario=trim($_REQUEST['usuario']);
	
//echo $usuario;
$nombres=	"";
$apps=		"";
$apmat=		"";
$numemp=	"";
$nompuesto=	"";
$nomdepto=	"";

$meses=array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
		
//$idprograma=substr('0000'.$idprograma,strlen('0000'.$idprograma)-4,4);
if($conexion)
{
	$tsql="SELECT TOP 1000 [numemp]
      ,[nombre]
      ,[appat]
      ,[apmat]
      ,[nompuesto]
      ,[fecha]
      ,[nomdepto]
  		FROM [nomemp].[dbo].[v_gafet_union] 
				WHERE numemp='$usuario'";
	//echo $tsql;
	$s = sqlsrv_query( $conexion,$tsql);
	if ($s)
	{ 
		if(count($s)>0)
		{
			while( $row = sqlsrv_fetch_array( $s, SQLSRV_FETCH_ASSOC))
			{
				//print_r($row);
				$numemp=trim(utf8_encode($row['numemp']));
				$nombres=	trim(utf8_encode($row['nombre']));
				$apps	=	trim(utf8_encode($row['appat']))." ".trim(utf8_encode($row['apmat']));
				$fecing=	trim(utf8_decode($row['fecha']));
				$nompuesto=	trim(utf8_encode($row['nompuesto']));
				$nomdepto=	trim(utf8_encode($row['nomdepto']));
			}
		}
	}
}
//echo $numemp;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>JPEGCam Test Page</title>
	<meta name="generator" content="TextMate http://macromates.com/">
	<meta name="author" content="Fernando Llanas">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta content="no-cache" http-equiv="Pragma" />
    <script language="javascript" src="../../../javascript_globalfunc/jQuery.js"></script>
    <style type='text/css'>
body{
	font-family:Verdana, Geneva, sans-serif;
	color:#000;
	font-size:8px;
	margin-left:0px;
}
#frente{
	background-image: url(../../imagenesGAfete/gafetesrh-08.png);
	/*url(../../imagenesGAfete/gafetesrh-10.png); */
	background-size:cover;
	background-repeat:no-repeat;
	background-position:center;
	position:relative;
	height:312px;
	width:198px;
	border:0px;
}
.nombres{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight:bold;
	color:#000;
	position:absolute;
	width:180px;
	max-width:180px;
	top:55px;
	left: 10px;
	vertical-align:middle;
	text-align:center;
}

.apps{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight:bold;
	color:#000;
	position:absolute;
	width:180px;
	max-width:180px;
	top:75px;
	left: 10px;
	vertical-align:middle;
	text-align:center;
}
#upload_results{
	position:absolute; 
	top: 98px; 
	left:0px;
	width:100%;
	text-align:center;
}

#upload_results img{
	width:75px;
	height:100px;
}




.nompuesto
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:14px;
	font-weight:bold;
	color:#000;
	position:absolute;
	width:180px;
	max-width:180px;
	top: 205px;
	left: 10px;
	vertical-align:middle;
	text-align:center;
}

.fecha{
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#000;
	position:relative;
	top:68px;
	left: 0px;
	vertical-align:middle;
	text-align:center;
	z-index:3;
}
.numemp{
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#000;
	position:relative;
	top:87px;
	left: 0px;
	vertical-align:middle;
	text-align:center;
	z-index:4;
}
.depto{
	font-family:Arial, Helvetica, sans-serif;
	font-size:8px;
	color:#000;
	position:absolute;
	top: 133px;
	left: 10px;
	width:180px;
	min-width:180px
	vertical-align:middle;
	text-align:center;
	z-index:5;
	
}
.dir{
	font-family:Arial, Helvetica, sans-serif;
	font-size:8px;
	color:#000;
	position:absolute;
	top: 164px;
	left: 0px;
	width:198px;
	min-width:198px
	vertical-align:middle;
	text-align:center;
	z-index:6;
}
.tel{font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
	color:#000;
	position:absolute;
	top:195px;
	left: 0px;
	width:198px;
	min-width:198px
	vertical-align:middle;
	text-align:center;
	z-index:7;
}



#frente td{
	font-size:14px;
}
#atras{
	background-image:url(../../imagenesGAfete/gafetesrh-07.png); /*FONDOF2.jpg*/
	background-size:cover;
	background-repeat:no-repeat;
	background-position:center;
	position:relative;
	text-align:left;
	height:312px;
	width:198px;
	border:0px;
}
#atras th{
	text-align:left;
	font-weight:bold;
	font-size:8px;
}
#atras td{
	text-align:left;
	font-size:10px;
	
}
#atras tr{ border-spacing:0px;padding-bottom:0px;}
.textochico
{
	font-size: 5px; 
}

</style>
<script language="javascript">
	$(document ).ready(function(){
		
			$("input[name='fondoFrente']").click(function(){
					$.each( $("input[name='fondoFrente']") ,function(index,value){
						if($(this).is(":checked"))
						{	console.log($(this).val());
							$nombreIMG = $(this).val();
							$nombreParts=$nombreIMG.split("-");
							console.log($nombreParts);
							console.log(parseInt($nombreParts[1].split(".")[0]));
							if(parseInt($nombreParts[1].split(".")[0])<8)
							{
								console.log("cambia atras");
								$('#atras').css("background-image","url(../../imagenesGAfete/gafetesrh-03.png)");
								if(parseInt($nombreParts[1].split(".")[0])<6)
								{
									$("#upload_results").css("top","93px");
									
									$(".tel").hide();
									$(".dir").hide();
									$(".depto").hide();
									$(".numemp").hide();
									$(".fecha").hide();
									$(".nompuesto").hide();
									$(".apps").show();
									$(".nombres").show ();
									$("img").show();
									if(parseInt($nombreParts[1].split(".")[0])<=5){
										$(".apps").hide();
										$(".nombres").hide();
										$("img").hide();
									}
								}
							}
							else
							{
								$('#atras').css("background-image","url(../../imagenesGAfete/gafetesrh-01.png)");
							
								$("#upload_results").css("top","98px");
								
								$(".tel").show();
								$(".dir").show();
								$(".depto").show();
								$(".numemp").show();
								$(".fecha").show();
								$(".nompuesto").show();
								
								if(parseInt($nombreParts[1].split(".")[0])>=5)
								{
									$(".nombres").show();
									$(".apps").show();
									$("img").show();
								}
							}
							
							
							
								
							
							
							$('#frente').css("background-image", "url(../../imagenesGAfete/" + $(this).val() + ")");
						}
					});									
			});
			
			$("input[name='fondoAtras']").click(function(){
					$.each( $("input[name='fondoAtras']") ,function(index,value){
						
						if($(this).is(":checked"))
						{	console.log($(this).val());
							$('#atras').css("background-image", "url(../../imagenesGAfete/" + $(this).val() + ")");
						}
					});									
			});
			
	});
</script>
            
</head>
<body>

	<table>
		<tr>
			<td height="188pts" valign=top>
			<!--<h1>Fomeredes width="188pts"</h1>-->
			<h3>Captura de imagen </h3>
			
			<!-- First, include the JPEGCam JavaScript Library -->
			<script type="text/javascript" src="../../jpegcam/htdocs/webcam.js"></script>
			
			<!-- Configure a few settings -->
			<script language="JavaScript">
				function gup( name )
				{
					var results = (new RegExp("[\\?&]"+name+"=([^&#]*)")).exec(window.location.href);
					if ( results == null ) {return ""}
					else {return results[1]}
				}
				
				webcam.set_api_url( '../../jpegcam/htdocs/test.php?idtarjeta='+ gup('usuario'));
				webcam.set_quality( 90 ); 
				webcam.set_shutter_sound( true ); 
			</script>
			
			<!-- Next, write the movie to the page at 320x240 -->
			<script language="JavaScript">
				document.write( webcam.get_html(280, 180) );
			</script>
			
			
			<!-- Some buttons for controlling things -->
			<br/><form>
				<input type="button" value="Configurar..." onClick="webcam.configure()">
				&nbsp;&nbsp;
				<input type="button" value="Tomar imagen" onClick="take_snapshot()">
				
			</form>
			<form action="uploadfotografia.php" method="post" enctype="multipart/form-data">
				<h4>Agregar manualmente fotografia</h4>
				<input type="file" id="userfile" name="userfile" value="Agregar fotografia" width="100pt">
				<input type="submit" value="Enviar Fotografia" > 
				<input type="HIDDEN" id="idtarjeta" name="idtarjeta" value="<?php echo $numemp;?>">
			</form>
			<!-- Code to handle the server response (see test.php) -->
			<script language="JavaScript">
				webcam.set_hook( 'onComplete', 'my_completion_handler' );
				
				function take_snapshot() {
					// take snapshot and upload to server
					document.getElementById('upload_results').innerHTML = 'Cargando...';
					webcam.snap();
				}
				
				function my_completion_handler(msg) {
					console.log(msg);
					// extract URL out of PHP output
					if (msg.match(/(http\:\/\/\S+)/)) {
						var image_url = RegExp.$1;
						// show JPEG image in page
						document.getElementById('upload_results').innerHTML = 
							'<img src="' + image_url + ' " height="103" width="90"  align="center">'; 
						// guardar imagen en carpeta imagenes/targetaFomerred
							
						// reset camera for another shot
						webcam.reset();
					}
					else alert("PHP Error: " + msg);
				}
				
				function printTable()
				{
					var printContent = document.getElementById("frontt");
					//var printContent22 = document.getElementById("atras");
					var windowUrl = 'about:blank';
					var uniqueName = new Date();
					var windowName = 'Print' + uniqueName.getTime();
					var printWindow = window.open(windowUrl, windowName, 'left=0,top=0,width=420,height=500');//
					  var css="<html><head><style type='text/css'>body{	font-family:Verdana, Geneva, sans-serif;	color:#000;	font-size:8px;	margin-left:0px; margin-top:0px;}#frente{	background-image:url(../../imagenesGAfete/gafetesrh-08.png); 	background-size:cover;	background-repeat:no-repeat;	background-position:center;	position:relative;	height:312px;	width:198px;	border:0px;}.nombres{	font-family:Arial, Helvetica, sans-serif;	font-size:12px;	font-weight:bold;	color:#000;	position:absolute;	width:180px;	max-width:180px;	top:55px;	left: 10px;	vertical-align:middle;	text-align:center;}.apps{	font-family:Arial, Helvetica, sans-serif;	font-size:12px;	font-weight:bold;	color:#000;	position:absolute;	width:180px;	max-width:180px;	top:75px;	left: 10px;	vertical-align:middle;	text-align:center;}#upload_results{	position:absolute; 	top: 98px; 	left:0px;	width:100%;	text-align:center;}#upload_results img{	width:75px;	height:100px;}.nompuesto{	font-family:Arial, Helvetica, sans-serif;	font-size:14px;	font-weight:bold;	color:#000;	position:absolute;	width:180px;	max-width:180px;	top: 205px;	left: 10px;	vertical-align:middle;	text-align:center;}.fecha{	font-family:Arial, Helvetica, sans-serif;	font-size:10px;	color:#000;	position:relative;	top:68px;	left: 0px;	vertical-align:middle;	text-align:center;	z-index:3;}.numemp{	font-family:Arial, Helvetica, sans-serif;	font-size:10px;	color:#000;	position:relative;	top:87px;	left: 0px;	vertical-align:middle;	text-align:center;	z-index:4;}.depto{	font-family:Arial, Helvetica, sans-serif;	font-size:8px;	color:#000;	position:absolute;	top: 133px;	left: 10px;	width:180px;	min-width:180px	vertical-align:middle;	text-align:center;	z-index:5;	}.dir{	font-family:Arial, Helvetica, sans-serif;	font-size:8px;	color:#000;	position:absolute;	top: 164px;	left: 0px;	width:198px;	min-width:198px	vertical-align:middle;	text-align:center;	z-index:6;}.tel{font-family:Arial, Helvetica, sans-serif;	font-size:10px;	color:#000;	position:absolute;	top:195px;	left: 0px;	width:198px;	min-width:198px	vertical-align:middle;	text-align:center;	z-index:7;}#frente td{	font-size:14px;}#atras{	background-image:url(../../imagenesGAfete/gafetesrh-07.png);background-size:cover;	background-repeat:no-repeat;	background-position:center;	position:relative;	text-align:left;	height:312px;	width:198px;	border:0px;}#atras th{	text-align:left;	font-weight:bold;	font-size:8px;}#atras td{	text-align:left;	font-size:10px;	}#atras tr{ border-spacing:0px;padding-bottom:0px;}.textochico{	font-size: 5px; }</style></head>";					printWindow.document.write( css+"<body>" +printContent.innerHTML+"</body></html>" );//,printContent22.innerHTML					printWindow.document.close();
					printWindow.focus();
					printWindow.print();
					//printWindow.close();
				}

			</script>
		</td>
		<td width="50pts">&nbsp;</td>
		<td width="198pts" valign=top>
        	<div id="frontt" name='frontt'>
        	<table>
            <tr>
            	<td>
            	<div id="frente">
                	<div class="nombres"><?php echo $nombres;?></div>
					<div class="apps"><?php echo $apps;?></div>
                     <div id="upload_results"  >
                       		<img src="../../../imagenes/fotos_acceso/<?php echo $numemp;?>.jpg" id="pathphoto" name="pathphoto" >
                      </div>
					<div class="nompuesto"><?php echo $nompuesto;?></div>
                </div>
                </td>
          	</tr>
			<tr>
				<td>
					<div id="atras" style="position:relative;">
                            <div class="fecha"><?php echo $fecing;?></div>
                            <div class="numemp"><?php echo $numemp;?></div>
                            <div class="depto"><?php echo $nomdepto;?></div>
                            <div class="dir">GONZALITOS 292 NORTE COL. URDIALES, MTY, N.L.</div>
                            <div class="tel">20338500</div>
					</div>
				</td>
			</tr>
		</table>
          </div>
	</td>
    <td>
    	<table>
        	<tr>
            	<td>
                	<table><tr>
                    	<td><input type="radio" name="fondoFrente" value="gafetesrh-08.png"></td>
                        <td><input type="radio" name="fondoFrente" value="gafetesrh-09.png"></td>
                        <td><input type="radio" name="fondoFrente" value="gafetesrh-10.png"></td>
                    	<td><input type="radio" name="fondoFrente" value="gafetesrh-06.png"></td>
                        <td><input type="radio" name="fondoFrente" value="gafetesrh-02.png"></td>
                        <td><input type="radio" name="fondoFrente" value="gafetesrh-04.png"></td>
                        <td><input type="radio" name="fondoFrente" value="gafetesrh-05.png"></td>
                    </tr></table>                
    			</td>
           	</tr>
            <tr><td><input type="button" onClick="printTable()" value="Imprimir"> </td></tr>
            <tr>
            	<td>
                	<table><tr>
                    	<td><input type="radio" name="fondoAtras" value="gafetesrh-01.png"></td>
                        <td><input type="radio" name="fondoAtras" value="gafetesrh-07.png"></td>
                        <td><input type="radio" name="fondoAtras" value="gafetesrh-03.png"></td>
                    </tr></table>                
    			</td>
           	</tr>
        </table>
    </td>
	</tr>
	</table>
	</body>
</html>
