<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');

require_once("../../connections/dbconexion.php");


$conexion = sqlsrv_connect($server,$infoconexion);

$idprograma=0;
$fondo_delante="";
$fondo_atras="";

$num_targeta="";
if(isset($_REQUEST['num_targeta']))
{
	$num_targeta=$_REQUEST['num_targeta'];
}

if(isset($_REQUEST['idprograma']))
{
	$idprograma=$_REQUEST['idprograma'];
}

$idprograma=substr('0000'.$idprograma,strlen('0000'.$idprograma)-4,4);
if($conexion)
{
	$tsql="SELECT * FROM ProgEspTargetaFomerred WHERE id=$idprograma";
	
	$s = sqlsrv_query( $conexion,$tsql);
	if ($s)
	{ 
		if(count($s)>0)
		{
			while( $row = sqlsrv_fetch_array( $s, SQLSRV_FETCH_ASSOC))
			{
				$nombreprograma=$row['nombre'];
				$fondo_delante=$row['fondo_delante'];
				$fondo_atras=$row['fondo_atras'];
			}
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>JPEGCam Test Page</title>
	<meta name="generator" content="TextMate http://macromates.com/">
	<meta name="author" content="Fernando Llanas">
	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta content="no-cache" http-equiv="Pragma" />
	<!--<meta content="no-cache" http-equiv="Cache-Control" />-->
	<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="body.css">
	<link href="../css/estilos.css" rel="stylesheet" type="text/css">	
	<script language="javascript" type="text/javascript"></script>
	<script language="javascript" src="../../javascript/prototype.js"></script>
	<script language="JavaScript">
			function gup( name )
				{
					var results = (new RegExp("[\\?&]"+name+"=([^&#]*)")).exec(window.location.href);
					if ( results == null ) 
					{
						return "";
					}
					else 
					{
						return results[1];
					}
				}
				
			</script>
	<!-- Date: 2008-03-15 -->
</head>
<body>
	<table>
		<!--<tr>
			<td colspan="3">
				<div align="center" style="width:100%">
					<IFRAME scrolling="no" frameborder="0" name="editc" id="editc" style="width: 500px; height: 380px; background-color:#005200 border: 0px none #000000;" ></IFRAME>
					<script language="javascript">
						if(gup("contacto").lenght>0)
						{
							document.getElementById('editc').src="../../altavisitante.php?fml="+ gup("fomerrey") +"&Nombre="+gup("nombre")+"&Appat="+gup("appat")+"&Appmat="+gup("apmat");
						}
						else
						{
							document.getElementById('editc').src="../../editarcontanto_fomerred.php?fml="+ gup("fomerrey") +"&Nombre="+gup("nombre")+"&Appat="+gup("appat")+"&Appmat="+gup("apmat")+"&idcontacto="+gup("contacto");
						}
						document.getElementById('editc').refresh;
					</script>
    			</div>
			</td>
		</tr>-->
		<tr>
			<td width="188pts" valign=top>
			<h1>Fomeredes</h1>
			<h3>Captura de imagen </h3>
			
			<!-- First, include the JPEGCam JavaScript Library -->
			<script type="text/javascript" src="webcam.js"></script>
			
			<!-- Configure a few settings -->
			<script language="JavaScript">
			function gup( name )
				{
					var results = (new RegExp("[\\?&]"+name+"=([^&#]*)")).exec(window.location.href);
					if ( results == null ) {return ""}
					else {return results[1]}
				}
				
				idtarjeta=gup("num_targeta");
				fml=gup("fml");
				//document.getElementById('nombre').value = gup("nombre")+" "+gup("appat")+" "+gup("apmat");
				webcam.set_api_url( 'test.php?idtarjeta='+ idtarjeta +'&nombre='+gup("nombre")+'&appat='+gup("appat")+'&apmat='+gup("apmat")+'&fml='+fml);
				webcam.set_quality( 90 ); // JPEG quality (1 - 100)
				webcam.set_shutter_sound( true ); // play shutter click sound
			</script>
			
			<!-- Next, write the movie to the page at 320x240 -->
			<script language="JavaScript">
				document.write( webcam.get_html(280, 180) );
			</script>
			
			
			<!-- Some buttons for controlling things -->
			<br/><form>
				<input type=button value="Configurar..." onClick="webcam.configure()">
				&nbsp;&nbsp;
				<input type=button value="Tomar imagen" onClick="take_snapshot()">
			</form>
			
			<!-- Code to handle the server response (see test.php) -->
			<script language="JavaScript">
				webcam.set_hook( 'onComplete', 'my_completion_handler' );
				
				function take_snapshot() {
					// take snapshot and upload to server
					document.getElementById('upload_results').innerHTML = 'Cargando...';
					webcam.snap();
				}
				
				function my_completion_handler(msg) {
					// extract URL out of PHP output
					if (msg.match(/(http\:\/\/\S+)/)) {
						var image_url = RegExp.$1;
						// show JPEG image in page
						document.getElementById('upload_results').innerHTML = 
							'<img src="' + image_url + ' " height="60" width="90"  align="center">'; 
						// guardar imagen en carpeta imagenes/targetaFomerred
							
						// reset camera for another shot
						webcam.reset();
					}
					else alert("PHP Error: " + msg);
				}
				
				function printTable()
				{
					var printContent = document.getElementById("frontt");
					//var printContent22 = document.getElementById("atras");
					var windowUrl = 'about:blank';
					var uniqueName = new Date();
					var windowName = 'Print' + uniqueName.getTime();
					var printWindow = window.open(windowUrl, windowName, 'left=0,top=0,width=420,height=528');
				
					printWindow.document.write( printContent.innerHTML );//,printContent22.innerHTML
					printWindow.document.close();
					printWindow.focus();
					printWindow.print();
					//printWindow.close();
				}

			</script>
		</td>
		<td width="50pts">&nbsp;</td>
		<td width="369pts" valign=top>
			<div id="frontt" name='frontt'>
			<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="body.css">
			<link href="../css/estilos.css" rel="stylesheet" type="text/css">	
			<script language="javascript" type="text/javascript"></script>
			<script language="javascript" src="../../javascript/prototype.js"></script>
			<script language="JavaScript">
				function gup( name )
				{
					var results = (new RegExp("[\\?&]"+name+"=([^&#]*)")).exec(window.location.href);
					if ( results == null ) {return ""}
					else {return results[1]}
				}
			</script>
			<table border="0"  >
			<tr><td>
				<table background="<?php echo $fondo_delante;?>"align="left" width="320" height="198" border="0">
					<tr>
						<td colspan="1"><div id="upload_results" style="background-color:; width:50px" >
						 	<img src="" id='pathphoto' name='pathphoto' height="60" width="90"  align="center">
							<script language="JavaScript">
									var textpath= gup("pathfoto");
									document.getElementById('pathphoto').src = gup("pathfoto");
							</script>
						</div></td>
						<td colspan="2"><table width="100%">
								<tr><td width="100%" class="credenciastexto"><input type="text" id="nombre" name="nombre" class="caja_toprint" width="100%" style=" width:100%">
									<script language="JavaScript">
											document.getElementById('nombre').value = gup("nombre")+" "+gup("appat")+" "+gup("apmat");
									</script>
								</td></tr>
								<!--<tr><td width="100%" class="credenciastexto"><input type="text" id="cargo" name="cargo" class="caja_toprint" width="100%" style=" width:100%">
									<script language="JavaScript">
											document.getElementById('cargo').value= gup("cargo");
									</script></td></tr>-->
								<tr><td width="100%" class="credenciastexto"><input type="hidden" id="fomerrey" name="fomerrey" class="caja_toprint" width="100%" style=" width:100%">
									<script language="JavaScript">
											document.getElementById('fomerrey').value = gup("num_targeta");
									</script></td></tr>
								<tr><td width="100%" class="credenciastexto"><input type="text" id="dir1" name="dir1" class="caja_toprint" width="100%" style=" width:100%">
									<script language="JavaScript">
											document.getElementById('dir1').value = gup("calle")+" #"+gup("ext");
									</script></td></tr>
								<tr><td width="100%" class="credenciastexto"><input type="text" id="dir2" name="dir2" class="caja_toprint" width="100%" style=" width:100%">
									<script language="JavaScript">
											document.getElementById('dir2').value = gup("col");
									</script></td></tr>
									<tr><td width="100%" class="credenciastexto"><input type="text" id="dir3" name="dir3" class="caja_toprint" width="100%" style=" width:100%">
									<script language="JavaScript">
											document.getElementById('dir3').value = gup("mun")+" N.L.";
									</script></td></tr>
							</table></td>
					</tr>
					<!--	<td colspan="3"  valign="top">
											<script language="javascript">
												 document.getElementById('barcodesuperior').src="../../barcodegen/barcodeH.php?barcode=00"+ gup("num_targeta")+"07002&Tamfuente=6";
											</script>						  </td>
					<tr>
					</tr>-->
					
				</table>
			</td></tr>
			<tr><td>
				<table background="<?php echo $fondo_atras;?>"  align="left" width="320" height="198" border="0" >
				<tr height="50%" valign="top">
					<td width="5%" align="right" class="texto8" colspan="3"><input type="text" id="nomprog" name="nomprog" class="caja_toprint" width="100%" style=" width:100%" value="<?php echo $nombreprograma;?>">
									<!--<script language="JavaScript">
											//alert(gup("programa"));
											document.getElementById('nomprog').value = gup("programa");
									</script>--></td>
				</tr>
				<!--<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>-->
				<tr align="left" valign="middle" height="20%">
						<td>&nbsp;</td>
						<td width="90%" class="texto4">ESTA TARJETA NO ES BANCARIA VALIDA UNICAMENTE EN LAS EMPRESAS AFILIADAS, ESTA TARJETA ES PROPIEDAD DEL EMISOR ACEPTA LOS TERMINOS Y CONDICIONES DEL PROGRAMA</td>
						<td>&nbsp;</td>
				</tr>
					<tr align="center" valign="baseline">
						<td align="center" valign="bottom" colspan="3">
							<img id="barcodeinferior" align="center" src="../../barcodegen/barcodeH.php?barcode=<?php echo '0070'.$idprograma.''.$num_targeta;?>&Tamfuente=8">
							<!--<script language="javascript">
								var idprograma=gup("idprograma");
								 document.getElementById('barcodeinferior').src="../../barcodegen/barcodeH.php?barcode=99"+document.getElementById('fomerrey').value+""+idprograma+"&Tamfuente=8";//+"03001
							</script>-->
						</td>
					</tr>
				</table>
			</td></tr>
			</table>
			</div>
	</td>
	</tr>
	</table>
	<table><tr><td><input type="button" onClick="printTable()" value="Imprimir"> </td></tr></table>
	</body>
</html>
