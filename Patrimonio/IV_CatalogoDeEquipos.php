<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Busqueda de equipos del Instituto de la Vivienda</title>
<link rel="stylesheet" href="../cheques/css/style.css"					/>         
<link rel="stylesheet" href="../css/estilos.css" 					type="text/css" />   
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' >
<link rel="stylesheet" href="../css/admin.css">
<script src="../javascript_globalfunc/191jquery.min.js"></script>
<script src="divhide.js"></script>
<script src="jquery.tmpl.js"></script>
<script>

    $(function()
    {
		<?php if(isset($_REQUEST['message']))
				echo "alert('".$_REQUEST['message']."');"
				?>
		$("#btnbuscar").click(function(){
					
				var tabla_muebles= $('#tabla_muebles').val();
				var tabla_equipos= $('#tabla_equipos').val();
				var folio= $('#folioAbuscar').val(); // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
                var data = 'folio=' + folio+'&tabla_muebles='+tabla_muebles+'&tabla_equipos='+tabla_equipos ;     // Toma el valor de los datos, que viene del input						
				console.log(data);
                $.post('busqueda_por_folio.php',data, function(resp)
                { //Llamamos el arch ajax para que nos pase los datos
               		console.log(resp);
					$('#bodyRes').empty();
					$('#tmpl_proveedor').tmpl(resp).appendTo('#bodyRes'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							 // los va colocando en la forma de la tabla
				}, 'json');  // Json es una muy buena opcion
		});
       
    });
   
    function editar(folio, tabla_muebles ,tabla_equipos)
    {
		//alert(folio);
		location.href="equipo_editar.php?folio="+folio+"&tabla_muebles="+tabla_muebles+"&tabla_equipos="+tabla_equipos;
       /* var pregunta = confirm("¿Esta seguro que desea eliminar la Poliza?")
        
        if (pregunta)
        {
            location.href="php_ajax/cancela_poliza.php?poliza="+folio+"&tipo="+tipo;
        }*/
    }
    
</script>

<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
    <tr>
        {{if folio}}
            <td  align="center">${folio}</td>
            <td>${marca}</td>
            <td>${descrip}</td>
             <td>${responsable}</td> 
		<!--	<td>${observa_2}</td> -->
			<td>${ip}</td>
			<td>{{if editable=='1'}}
				<img src="../imagenes/edit-icon.png" onclick="javascript:editar('${folio}', $('#tabla_muebles').val(), $('#tabla_equipos').val())"></td>
				{{/if}}
        {{else}}
            <td colspan="2">No existen resultados</td>
        {{/if}}
    </tr>
</script>   
</head>
<body>
<table width="100%">
<tr>
    <td colspan="8" align="center" class="TituloDForma">Busqueda de equipos del Instituto de la Vivienda<hr class="hrTitForma">	<input type="hidden" value="<?php echo $usuario;?>" id="usuario" name="usuario"></td>
</tr>
<tr>
    <td style="font-size:14px;">
    	<table>
        	<tr>
            	<td>Folio:</td><td><input type="text" id="folioAbuscar" name="folioAbuscar" /></td><td><input name="btnbuscar" id="btnbuscar" type="button" value="buscar" /><input type="hidden" name="tabla_muebles" id="tabla_muebles" value="activodmuebles_iv"/>
    <input type="hidden" name="tabla_equipos" id="tabla_equipos" value="activodequipos_iv"/></td>
    		</tr>
        </table>
     </td>
</tr>
</table>
<table style="width:90%;" id="thetable" cellspacing="0" border="1" align="center" style="left:25px;">
  <thead>
    	<tr>
        	<td class="titles" align="center">Folio</td>
            <td class="titles" align="center">Marca</td>
            <td class="titles" align="center">Descripcion</td>
            <td class="titles" align="center">Responsable</td>
        <!--    <td class="titles" align="center">Observa. 2</td>-->
            <td class="titles" align="center">IP</td>
            <td class="titles" align="center"></td>
        </tr>
  	<tbody name="bodyRes" id="bodyRes">
    	<tr <?php if($i==0) echo 'class="first"';?>>
        </tr>
    </tbody>
</table>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>-->
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	//$('#thetable').tableScroll({height:300});
	//$('#thetable2').tableScroll();
});
</script>
</body>
</html>