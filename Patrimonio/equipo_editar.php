<?php
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$folio="";
if(isset($_REQUEST['folio']))
	$folio = $_REQUEST['folio'];
	
$tabla_equipos="";
if(isset($_REQUEST['tabla_equipos']))
	$tabla_equipos = $_REQUEST['tabla_equipos'];
	
	
$tabla_muebles="";
if(isset($_REQUEST['tabla_muebles']))
	$tabla_muebles = $_REQUEST['tabla_muebles'];

$unico=0;
$tipo="";
$tipo_ant="";
$folio_ant="";
$edifcio_cve="";
$edifcio_desc="";
$piso="";
$c_ip="";
$tipo_cve="";
$tipo_desc="";
$sub_tipo_cve="";
$sub_tipo_desc="";
$Tipo_Adq_cve="";
$Tipo_Adq_desc="";
$Cond_Fisicas__cve="";
$Cond_Fisicas__desc="";
$Responsable_cve="";
$Responsable_desc="";
$c_segmento="";
$c_operador="";
$Nombre_del_Equipo="";
$c_nivelinternet="";
$c_windows="";
$c_office="";
$c_mem="";
$c_tarjetain="";
$c_capacidad="";
$c_unidad_dd="";
$c_antivirus="";
$Estatus_cve="";
$Estatus_desc="";
$Fecha="";
$estado_descripcion="";
$c_procesador="";
$c_unidad="";
$Serie= "";
$Factura= "";
$Valor= "";
$Fecha_de_Adq= "";

$c_nomequipo="";
$c_tarjetain="";

$entro=false;
$datos= array();
if($conexion)
{
	$command= "select  A.*,B.TIPO,c.ip,c.segmento,c.operador,c.nomequipo,c.nivelinternet,c.windows as verwindows,c.office,
c.mem,c.tarjetain,c.antivirus,c.capacidad,c.procesador,c.unidad,c.unidad_dd ,
D.nomedif, E.nomtipo, F.nomsubtipo, G.descrip as Tipo_Adq_desc, I.nomemp,  J.descrip as Estatus_desc, CONVERT(varchar(10),A.fcaptura, 103) as fecha, CONVERT(varchar(10),A.fechaadq, 103) as Fecha_de_Adq
from [fome2011].[dbo].".$tabla_muebles." A 
LEFT JOIN [fome2011].[dbo].ACTIVOMSUBTIPOS B ON A.SUBTIPO=B.SUBTIPO 
LEFT JOIN [fome2011].[dbo].".$tabla_equipos." C ON a.folio=c.folio 
LEFT JOIN [fome2011].[dbo].activomedificios D ON A.edif=D.edif
LEFT JOIN [fome2011].[dbo].activomtipos E ON E.tipo=B.TIPO
LEFT JOIN [fome2011].[dbo].activomsubtipos F ON F.subtipo=A.subtipo
LEFT JOIN [fome2011].[dbo].activomtipoadq G ON G.tipoadq=A.tipoadq
LEFT JOIN [fome2011].[dbo].[activomcondfis] H ON H.condfis=A.condfis
LEFT JOIN [fomeadmin].[dbo].v_nomina_nomiv_nomhon6_nombres_activos_depto_dir I ON  I.numemp collate DATABASE_DEFAULT =A.respbien  collate DATABASE_DEFAULT
LEFT JOIN[fomeadmin].[dbo].activomestatus J ON J.estatus=A.estatus
where a.folio ='$folio' and a.ESTATUS<9000 order by A.unico desc";

//echo $command;

	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."<br>". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$tipo= substr(trim($folio),0,2);//trim();//$row['TIPO'] 
			$folio = substr(trim($folio),2);
			$unico =  trim($row['unico'] );
			
			$folio_ant= trim($row['folioant'] );
			//echo strlen($folio_ant);
			//$folio_ant="";
			if(strlen($folio_ant)>2){
				$tipo_ant= substr(trim($folio_ant),0,2);
				$folio_ant = substr(trim($folio_ant),2);
			}
			else
			{
				$tipo_ant= substr(trim($folio_ant),0,2);
				$folio_ant = "";
			}
			
			$edifcio_cve= trim($row['edif'] );
			$edifcio_desc= trim($row['nomedif'] );
			$piso= trim($row['piso'] );
			$tipo_cve= trim($row['TIPO'] );
			$tipo_desc= trim($row['nomtipo'] );
			$sub_tipo_cve= trim($row['subtipo'] );
			$sub_tipo_desc= trim($row['nomsubtipo'] );
			$Tipo_Adq_cve= trim($row['tipoadq'] );
			$Tipo_Adq_desc= trim($row['Tipo_Adq_desc'] );
			$Cond_Fisicas__cve= trim($row['condfis'] );
			$Cond_Fisicas__desc= trim($row['Tipo_Adq_desc'] );
			$Responsable_cve= trim($row['respbien'] );
			$Responsable_desc= trim($row['nomemp'] );
			$Nombre_del_Equipo= trim($row['descrip'] );
			$Estatus_cve= trim($row['estatus'] );
			$Estatus_desc= trim($row['Estatus_desc'] );
			$Fecha= trim($row['fecha'] );
			$estado_descripcion= trim($row['observa_1'] );
			$marca=trim($row['marca'] );
			$descrip=trim($row['descrip'] );
			$Serie= trim($row['serie'] );
			$Factura= trim($row['factura'] );
			$Valor= trim($row['valor'] );
			$Fecha_de_Adq= trim($row['Fecha_de_Adq'] );
			$Fecha_de_Adq=substr($Fecha_de_Adq,6,2)."/".substr($Fecha_de_Adq,4,2)."/". substr($Fecha_de_Adq,0,4);
			
			////////////////////////
			$c_ip= 				trim($row['ip'] );
			$c_segmento= 		trim($row['segmento'] );
			$c_operador= 		trim($row['operador'] );
			$c_nomequipo= 		trim($row['nomequipo'] );
			$c_nivelinternet= 	trim($row['nivelinternet'] );
			$c_windows= 		trim($row['verwindows'] );
			$c_office= 			trim($row['office'] );
			$c_mem= 			trim($row['mem'] );
			$c_tarjetain= 		trim($row['tarjetain'] );
			$c_antivirus= 		trim($row['antivirus'] );
			$c_capacidad= 		trim($row['capacidad'] );
			$c_procesador= 		trim($row['procesador'] );
			$c_unidad_dd= 		trim($row['unidad_dd'] );
			$c_unidad= 			trim($row['unidad'] );
		
		}
	}
}
        
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../javascript_globalfunc/191jquery.min.js"></script>
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<script type="text/javascript" src="../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
<script>
$(document).ready(function() {
						   
	
    //set initial state.
    $('#c_tarjetain').change(function() {
        if($(this).is(":checked")) {
           $(this).val(true)
        }
		else
		{
			$(this).val(false)
		}
        $('#c_tarjetain').val($(this).is(':checked'));        
    });
});

</script>
<link rel="stylesheet" href="../calendario/skins/aqua/theme.css" 	type="text/css" media="all"  title="Aqua" />
<title>Edicion de datos de Equipo Instituto de la Vivienda</title>
</head>

<body>
<form action="guardar_resguardo.php" method="post">
<table>
<td>
</td>
<td align="center">
    <table>
        <tr>	
            <td colspan="2"><h1>Patrimonio</h1></td>
        </tr>
        <tr>
            <td colspan="4">
                <table>
                    <tr>
            <td>Folio:</td><td><input type="text" name="tipo" id="tipo" style="width:30px;" value="<?php echo $tipo;?>" readonly="readonly" maxlength="2"/></td><td><input type="text" name="folio" id="folio" value="<?php echo $folio;?>" style="width:100px;" readonly="readonly" maxlength="12"/></td>
            <td>Anterior:</td><td><input type="text" name="tipo_ant" id="tipo_ant"  style="width:30px;" value="01" readonly="readonly" maxlength="2"/></td><td><input type="text" name="folio_ant" id="folio_ant" value="<?php echo $folio_ant;?>" style="width:100px;" readonly="readonly" maxlength="12"/><input type="hidden" id="unico" name="unico" value="<?php echo $unico;?>" />
            <input type="hidden" name="tabla_muebles" id="tabla_muebles" value="<?php echo $tabla_muebles;?>"/>
            <input type="hidden" name="tabla_equipos" id="tabla_equipos" value="<?php echo $tabla_equipos;?>"/>
            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    

    <table>
        <tr>
            <td colspan="6"><h1>Equipo edición</h1></td>
        </tr>
        <tr>
            <td>Operador:</td>
            <td><input type="text" name="c_operador" id="c_operador" value="<?php echo $c_operador;?>" maxlength="30"/></td> 
            <td>Nombre de Equipo:</td>
            <td><input type="text" name="c_nomequipo" id="c_nomequipo" value="<?php echo $c_nomequipo;?>" maxlength="30"/></td>
                    
        </tr>
        <tr>
            <td><h4>SOFTWARE</h4></td>
        </tr>
        <tr>
            <td>IP:</td><td><input type="text" name="c_ip" id="c_ip" value="<?php echo $c_ip;?>" style="width:60px;" maxlength="3"/></td>
            <td>Segmento:</td>
            <td><input type="text" name="c_segmento" id="c_segmento" value="<?php echo $c_segmento;?>" style="width:30px;" maxlength="3"/></td>
            <td>Nivel de Internet:</td>
            <td><input type="text" name="c_nivelinternet" id="c_nivelinternet" value="<?php echo $c_nivelinternet;?>" style="width:30px;" maxlength="1"/></td>
        </tr>
    
         <tr>
            <td>Ver. Windows:</td>
            <td><input type="text" name="c_windows" id="c_windows" value="<?php echo $c_windows;?>" style="width:60px;" maxlength="30"/></td> 
            <td>Ver. Office:</td>
            <td><input type="text" name="c_office" id="c_office" value="<?php echo $c_office;?>" style="width:60px;" maxlength="30"/></td> 
            <td>Antivirus.:</td>
            <td><input type="text" name="c_antivirus" id="c_antivirus" value="<?php echo $c_antivirus;?>"  style="width:120px;" maxlength="30"/></td>   
        </tr>
       
         <tr>
            <td><h4>HARDWARE:</h4></td>
        </tr>
        <tr>
            <td>Procesador:</td>
            <td><input type="text" name="c_procesador" id="c_procesador" value="<?php echo $c_procesador;?>" maxlength="30"/></td>
            <td>Memoria Ram:</td>
            <td><input type="text" name="c_mem" id="c_mem" value="<?php echo $c_mem;?>" style="width:60px;" maxlength="9"/></td>
            <td>Unidad Ram:</td>
            <td><input type="text" name="c_unidad" id="c_unidad" value="<?php echo $c_unidad;?>" style="width:60px;" maxlength="10"/></td> 
        </tr>
        <tr>
            <td>Capacidad de DD.:</td>
            <td><input type="text" name="c_capacidad" id="c_capacidad" value="<?php echo $c_capacidad;?>" style="width:60px;" maxlength="10"/></td>
            <td>Unidad de DD.:</td>
            <td><input type="text" name="c_unidad_dd" id="c_unidad_dd" value="<?php echo $c_unidad_dd;?>" style="width:60px;" maxlength="10"/></td> 
            <td>Tarjeta Internet:</td>
             <td><input type="checkbox" name="c_tarjetain" id="c_tarjetain" <?php if($c_tarjetain=="1") echo "checked";?>  value="<?php echo $c_tarjetain;?>" /></td>
        </tr>
         <tr>
            <td colspan="6"><hr /></td>
        </tr>
    
        <tr>
            <td>
                <input type="button" value="Cancelar"  onclick="location.href='CatalogoDeEquipos.php'"/>
            </td>
            <td>
                <input type="submit" value="Guardar"  />
            </td>
        </tr>   
    </table>
    </td>
    <td>
    </td>
    </table>

</form>
</body>
</html>