<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$prov = 0;
$iva = 0;
$subtotal = 0;
$numdepto="";
$descdepto="";
$cvedepto="";

$usuario = $_COOKIE['ID_my_site'];
$depto = $_COOKIE['depto'];
$privsolche = $_COOKIE['privsolche'];

//$usuario='001349';
//$depto ='1232';
//$privsolche = 20;

$datos=array();
if(isset($_REQUEST['numprov']))
	$prov = $_REQUEST['numprov'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];

if(isset($_REQUEST['iva']))
	$iva = $_REQUEST['iva'];

if(isset($_REQUEST['subtotal']))
	$subtotal = $_REQUEST['subtotal'];

if(isset($_REQUEST['total']))
	$total = $_REQUEST['total'];

if(isset($_REQUEST['icic']))
	$icic = $_REQUEST['icic'];

if(isset($_REQUEST['neto']))
	$neto = $_REQUEST['neto'];
//$tipo_sol = "Solicitud de Obra Publica";
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/provfuncion.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/busqueda2.js"></script>
<script language="javascript" src="javascript/obrafun.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/comrequisicion.js"></script>


<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>
<form name="form1" method="post" action="">
<body>
  	<tr>
    <td colspan="4" align="center" class="TituloDForma">      </tr>
    <tr>
    <table width="95%"  border="0">
      <tr>
        <th scope="row"><div align="left" class="TituloDForma">Solicitud de Pago de Obra Publica
        </div>
        <hr class="hrTitForma"></th>
      </tr>
    </table>
<tr>
      <table width="94%"  border="0">
    <tr>
      <th class="texto10" align="right" scope="row"><span class="texto8 Estilo1">
        <input class="texto8" type="hidden" id="nomcoord" name="nomcoord" size="10" value="<?php echo $datos[$i]['nomcoord'];?>">
      </span>Area:</th>
      <td>
	  <div align="left" style="z-index:3; position:absolute; width:517px; top: 45px; left: 144px; height: 24px;">
        <input class="texto8" type="text" name="cvedepto" id="cvedepto" style="width:90%;" onKeyUp="searchdepto(this);" autocomplete="off" tabindex="1" value="<?php echo $cvedepto;?>">	  
        <div id="search_suggestdepto" style="z-index:4;" > </div>
      
	</div>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th class="texto10" scope="row"><span class="texto8 Estilo1">
        <input class="texto8" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
        <input class="texto8" type="hidden" id="usuario" name="usuario" size="10" value="<?php echo $usuario;?>">
        <input class="texto8" type="hidden" id="depto" name="depto" size="10" value="<?php echo $depto;?>">
        <input class="texto8" type="hidden" id="nomdir" name="nomdir" size="10" value="<?php echo $datos[$i]['nomdir'];?>">
      </span></th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><div align="right"><span class="texto10"></span>
      </div></td>
    </tr>
    <tr>
      <th class="texto10" scope="row" align="right">No. Factura:</th>
      <td><input type="text" name="factura" id="factura" tabindex="2" onKeyPress="return aceptarSoloNumeros(this, event);"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th width="15%" class="texto10" scope="row" align="right">Contrato:</th>
      <td width="24%"><input type="text" name="contrato" id="contrato" tabindex="3" onKeyPress="return aceptarSoloNumeros(this, event);"></td>
      <td width="11%">&nbsp;</td>
      <td width="12%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td width="36%">&nbsp;</td>
    </tr>
    <tr>
      <th height="30" class="texto10" scope="row" align="right">Beneficiario:</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><div align="left" style="z-index:1; position:absolute; width:315px; top: 139px; left: 148px; height: 24px;">
        <input class="texto8" type="text" id="provname1" name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="4" onBlur="valida_factura()"  value="<?php echo $nombre;?>">        
        <div id="search_suggestProv" style="z-index:2;" > </div>
      </div></td>
    </tr>
    <tr>
      <th class="texto10" scope="row" align="right">Tipo Estimaci&oacute;n:</th>
      <td><select name="tipoestima" id="tipoestima" tabindex="5">
        <option value=1>Anticipo</option>
        <option value=2>Ordinaria</option>
        <option value=3>Aditiva</option>
        <option value=4>Extraordinaria</option>
        <option value=5>Deductivas</option>
      </select></td>
      <td><span class="texto10"></span></td>
      <td>&nbsp;</td>
      <td><span class="texto10"></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th class="texto10" scope="row" align="right">No. Estimaci&oacute;n: </th>
      <td><input name="numestima" type="text" id="numestima" tabindex="6" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" maxlength="4" ></td>
      <th class="texto10" scope="row" align="right">Concepto de Obra</th>
      <td colspan="3" rowspan="2">
		  <div style="position: static; width:130px left: 308px; top: 213px; height: 58px; left: 343px; width: 120px;">
    	    <textarea class="mayus" name="concepto" tabindex="7" id="concepto"  style="width:380px; top:inherit; "></textarea>
	    </div>	
     </td>
      </tr>
    <tr>
      <th align="right" class="texto10 Estilo1" scope="row">Tipo de Recurso</th>
      <td><select name="tiporecurso" tabindex="8" id="tiporecurso">
        <option value=1>Estatal</option>
        <option value=2>Propio</option>
      </select></td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right"><strong>Tipo de Bien</strong></th>
      <td><select name="tipobien" tabindex="9" id="tipobien">
        <option value=1>Publico</option>
        <option value=2>Propio</option>
      </select></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th colspan="6" align="right" class="texto10" scope="row"><hr class="hrTitForma"></th>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right">Importe:</th>
      <td><input type="text" tabindex="10" name="importe" id="importe" onkeypress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <td colspan="2" class="texto10"><div align="right"><strong>Iva</strong></div></td>
      <td colspan="2"><input type="text" tabindex="13" name="iva" id="iva" value="<?php echo $iva;?>" onkeypress="EvaluateText('%f', this);" onBlur="cal_iva2()" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right">Amortizaci&oacute;n</th>
      <td><input type="text" tabindex="11" name="amortiza" id="amortiza" onBlur="cal_iva()" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <td colspan="2" class="texto10"><div align="right"><strong>Total</strong></div></td>
      <td colspan="2"><input type="text" tabindex="14" name="total" id="total" readonly onkeypress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right">Subtotal</th>
      <td><input type="text" tabindex="12" name="subtotal" id="subtotal" readonly onKeyPress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <td colspan="2" class="texto10"><div align="right"><strong>ICIC</strong></div></td>
      <td colspan="2"><input type="text" tabindex="15" name="icic" id="icic" onblur="cal_neto()" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" ></td>
      </tr>
    <tr>
      <th class="texto10" scope="row"><div align="right"></div></th>
      <td>&nbsp;</td>
      <td colspan="2" class="texto10"><div align="right"><strong>Neto</strong></div></td>
      <td colspan="2"><input type="text" tabindex="16" name="neto" id="neto" readonly onkeypress="EvaluateText('%f', this);" onFocus="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row">&nbsp;</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;      </td>
    </tr>
    <tr>
      <td colspan="6" scope="row">        <p>&nbsp;        </p></td>
    </tr>
    <tr>
      <th colspan="6" scope="row" align="center"><input type="button" name="boton" tabindex="17" value="Guardar" onClick="guardar()">
      </th>
    </tr>
        </table>
      <p align="left">&nbsp;</p>
      <p align="left">
        </form>
  </p>
</body>
</html>
