<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$prov = 0;
$iva = 0;
$subtotal = 0;

$depto = '1200';
$usuario = '001349';
$contrato="";

if(isset($_REQUEST['folio']))
	$folio = $_REQUEST['folio'];



	$consulta = "select a.*,b.nomprov from egresosmsolopublica a left join compramprovs b on a.prov=b.prov where a.folio=$folio";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$contrato = trim($row['contrato']);
		$concepto = trim($row['concepto']);
		$prov = trim($row['prov']);
		$nombre = trim($row['nomprov']);
		$tipoestima = trim($row['tipoestima']);
		$numestima = trim($row['numestima']);
		$tiporecurso = trim($row['tiporecurso']);
		$tipobien = trim($row['tipobien']);
		$importe = trim($row['importe']);
		$amortiza = trim($row['amortiza']);
		$subtotal = trim($row['subtotal']);
		$iva = trim($row['iva']);
		$total = trim($row['total']);
		$icic = trim($row['icic']);
		$neto = trim($row['neto']);
	
		//$i++;
	}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/provfuncion.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/obrafun.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/comrequisicion.js"></script>


<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>
<form name="form1" method="post" action="">
<body>
  	<tr>
    <td colspan="4" align="center" class="TituloDForma">      </tr>
    <tr>
    <table width="95%"  border="0">
      <tr>
        <th scope="row"><div align="left" class="TituloDForma">Solicitud de Pago de Obra Publica
        </div>
        <hr class="hrTitForma"></th>
      </tr>
    </table>
<tr>
      <table width="94%"  border="0">
    <tr>
      <th class="texto10" scope="row"><span class="texto8 Estilo1">
        <input class="texto8" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
        <input class="texto8" type="hidden" id="usuario" name="usuario" size="10" value="<?php echo $usuario;?>">
        <input class="texto8" type="hidden" id="depto" name="depto" size="10" value="<?php echo $depto;?>">
      </span></th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right" class="texto10"><span class="Estilo1">Folio</span>
	  <input type="text" readonly name="folio" id="folio" value="<?php echo $folio;?>"></td>
    </tr>
    <tr>
      <th width="11%" class="texto10" scope="row"><div align="right" class="texto10">Contrato:</div></th>
      <td width="18%"><input type="text" name="contrato" id="contrato" tabindex="1" value="<?php echo $contrato;?>"></td>
      <td width="9%">&nbsp;</td>
      <td width="3%">&nbsp;</td>
      <td width="6%">&nbsp;</td>
      <td width="53%">&nbsp;</td>
    </tr>
    <tr>
      <th height="30" class="texto10" scope="row"><div align="right">Beneficiario:</div></th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><div align="left" style="z-index:1; position:absolute; width:402px; top: 100px; left: 108px; height: 24px;">
        <input class="texto8" type="text" id="provname1" name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="2"  value="<?php echo $nombre;?>">        
        <div id="search_suggestProv" style="z-index:2;" > </div>
      </div></td>
    </tr>
    <tr>
      <th class="texto10" scope="row"><div align="right">Tipo Estimaci&oacute;n: </div></th>
      <td><select name="tipoestima" id="tipoestima" tabindex="3" value="<?php echo $tipoestima;?>">
        <option value=1>Anticipo</option>
        <option value=2>Ordinaria</option>
        <option value=3>Aditiva</option>
        <option value=4>Extraordinaria</option>
        <option value=5>Deductivas</option>
      </select></td>
      <td><span class="texto10"></span></td>
      <td>&nbsp;</td>
      <td><span class="texto10"></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th class="texto10" scope="row" align="right">No. Estimaci&oacute;n: </th>
      <td><input type="text" tabindex="4" name="numestima" id="numestima" value="<?php echo $numestima;?>" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <th class="texto10" scope="row"><div align="right">Concepto de Obra</div></th>
      <td colspan="3" rowspan="2">
        <textarea name="concepto" tabindex="5" id="concepto"  style="width:480px;"><?php echo $concepto;?></textarea>
     </td>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right"><strong>Tipo de Recurso</strong></th>
      <td><select name="tiporecurso" tabindex="6" id="tiporecurso" value="<?php echo $tiporecurso;?>">
        <option value=1>Estatal</option>
        <option value=2>Propio</option>
      </select></td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right"><strong>Tipo de Bien</strong></th>
      <td><select name="tipobien" tabindex="7" id="tipobien">
        <option value=1>Publico</option>
        <option value=2>Propio</option>
      </select></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th colspan="6" align="right" class="texto10" scope="row"><hr class="hrTitForma"></th>
      </tr>
    <tr>
      <th class="texto10" scope="row" align="right">Importe:</th>
      <td><input type="text" tabindex="8" name="importe" id="importe" value="<?php echo $importe;?>" onkeypress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <td colspan="2" class="texto10"><div align="right"><strong>Iva</strong></div></td>
      <td colspan="2"><input type="text" tabindex="11" name="iva" id="iva" value="<?php echo $iva;?>" onkeypress="EvaluateText('%f', this);" onBlur="cal_iva2()" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row"><div align="right">Amortizaci&oacute;n</div></th>
      <td><input type="text" tabindex="9" name="amortiza" id="amortiza" value="<?php echo $amortiza;?>" onBlur="cal_iva()" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <td colspan="2" class="texto10"><div align="right"><strong>Total</strong></div></td>
      <td colspan="2"><input type="text" tabindex="12" name="total" id="total" value="<?php echo $total;?>" readonly onkeypress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row"><div align="right">Subtotal</div></th>
      <td><input type="text" tabindex="10" name="subtotal" id="subtotal" value="<?php echo $subtotal;?>" readonly onKeyPress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      <td colspan="2" class="texto10"><div align="right"><strong>ICIC</strong></div></td>
      <td colspan="2"><input type="text" tabindex="13" name="icic" id="icic" value="<?php echo $icic;?>" onChange="cal_neto()" onkeypress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row"><div align="right"></div></th>
      <td>&nbsp;</td>
      <td colspan="2" class="texto10"><div align="right"><strong>Neto</strong></div></td>
      <td colspan="2"><input type="text" tabindex="14" name="neto" id="neto" value="<?php echo $neto;?>" readonly onkeypress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"></td>
      </tr>
    <tr>
      <th class="texto10" scope="row">&nbsp;</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;      </td>
    </tr>
    <tr>
      <td colspan="6" scope="row">        <p>&nbsp;        </p></td>
    </tr>
    <tr>
      <th colspan="6" scope="row" align="center"><input type="button" name="Submit" tabindex="15" value="Actualizar" onClick="guardar_cambios()"></th>
    </tr>
  </table>
      <p align="left">&nbsp;</p>
      <p align="left">
        </form>
  </p>
</body>
</html>
