function valida_factura()
{
	var prov = $('numprov').value;
	var factura = $("factura").value;
	if(factura.length<=0)
	{
		alert ("Capturar Num. factura");return false;
	}
	new Ajax.Request('php_ajax/busca_factura.php?factura='+factura+"&prov="+prov,
	{onSuccess : function(resp) 
		{
			//alert(resp.responseText);
			if( resp.responseText )
			{
				var myArray = eval(resp.responseText);
				//alert("22 "+resp.responseText);
				if(myArray.length>0)
				{
					alert ("Num. de Factura no valida, ya existe con otro proveedor");
					return false
				}
				else
					return true;
			}
		}
	});
	//////////////////////////////
}

function cancela_sol(folio)
{
	
	var pregunta = confirm("Esta seguro que desea Eliminar esta Solicitud")
    if (pregunta)
	{
		location.href="php_ajax/cancela_sol.php?folio="+folio;
    }
}

function guardar()
{
	var contrato=$('contrato').value;
	var prov=$('numprov').value;
	var nomprov=$('provname1').value;
	var tipoestima=$('tipoestima').value;
	switch(tipoestima)
	{
		case '1':
			tipoestima = 'ANTICIPO';
			wtipoestima = 1;
			break;
		case '2':
			tipoestima = 'ORDINARIA';
			wtipoestima = 2;
			break;
		case '3':
			tipoestima = 'ADITIVA';
			wtipoestima = 3;
			break;
		case '4':
			tipoestima = 'EXTRAORDINARIA';
			wtipoestima = 4;
			break;
		case '5':
			tipoestima = 'DEDUCTIVAS';
			wtipoestima = 5;
			break;
	}
	var numestima=$('numestima').value;
	var importe=$('importe').value;
	var amortiza=$('amortiza').value;
	var subtotal=$('subtotal').value;
	var iva=$('iva').value;
	var total=$('total').value;
	var icic=$('icic').value;
	var neto=$('neto').value;
	var tipobien=$('tipobien').value;
	var tiporecurso=$('tiporecurso').value;
	switch(tiporecurso)
	{
		case '1':
			tiporecurso = 'ESTATAL';
			wtiporecurso = 1;
			break;
		case '2':
			tiporecurso = 'PROPIO';
			wtiporecurso = 2;
			break;
	}	
	var concepto=$('concepto').value;
	var factura=$('factura').value;
	var usuario=$('usuario').value;
	var depto=$('depto').value;
	var nomcoord=$('nomcoord').value;
	var nomdir=$('nomdir').value;
	var concepto_con = contrato+', '+tiporecurso+', '+tipoestima+', '+numestima+', '+concepto;	
//	alert('php_ajax/agrega_sol.php?contrato='+contrato+"&prov="+prov+"&tipoestima="+tipoestima+
//	"&numestima="+numestima+"&importe="+importe+"&amortiza="+amortiza+"&subtotal="+subtotal+
//	"&iva="+iva+"&total="+total+"&icic="+icic+"&neto="+neto+"&tipobien="+tipobien+
//	"&tiporecurso="+tiporecurso+"&concepto="+concepto+"&usuario="+usuario+"&depto="+depto+"&nomprov="+nomprov+"&concepto_con="+concepto_con+"&factura="+factura+"&nomcoord="+nomcoord+"&nomdir="+nomdir);
	
	new Ajax.Request('php_ajax/agrega_sol.php?contrato='+contrato+"&prov="+prov+"&tipoestima="+tipoestima+"&wtipoestima="+wtipoestima+
	"&numestima="+numestima+"&importe="+importe+"&amortiza="+amortiza+"&subtotal="+subtotal+"&wtiporecurso="+wtiporecurso+
	"&iva="+iva+"&total="+total+"&icic="+icic+"&neto="+neto+"&tipobien="+tipobien+
	"&tiporecurso="+tiporecurso+"&concepto="+concepto+"&usuario="+usuario+"&depto="+depto+"&nomprov="+nomprov+
	"&concepto_con="+concepto_con+"&factura="+factura+"&nomcoord="+nomcoord+"&nomdir="+nomdir,
		{onSuccess : function(resp)
			{
				//alert(resp.responseText);
				if( resp.responseText ) 
				{
					var myArray = eval(resp.responseText);
					//alert("22 "+resp.responseText);
					resp.responseText=resp.responseText.replace('"','');
					resp.responseText=resp.responseText.replace('"','');

					//alert(resp.responseText);
					window.open("pdf_files/sol_pago_"+resp.responseText+".pdf");
				}
					else
						return true;
			}

		});
}


function cal_iva()
{
	if ( parseFloat($('amortiza').value.replace(/,/g,'')) >  parseFloat($('importe').value.replace(/,/g,'')))
		{
			alert("Amortización no debe ser mayor al importe");
			$('amortiza').value=0;
		}
	
	$('subtotal').value = parseFloat($('importe').value.replace(/,/g,'')) - parseFloat($('amortiza').value.replace(/,/g,''));
	
	new Ajax.Request('php_ajax/cal_iva.php',
		{onSuccess : function(resp) 
			{
				//alert(resp.responseText);
				if( resp.responseText ) 
				{
					var myArray = eval(resp.responseText);
					//alert("22 "+resp.responseText);
					if(myArray.length==0)
						{
						alert ("No existe en el catalogo");
						$("iva").value=0;
						return false
						}
					  else
					  	{
					  	 var totiva = parseFloat((myArray[0].iva*parseFloat($('subtotal').value.replace(/,/g,'')))/100);
						 $('iva').value = totiva; // parseFloat(totiva);
						 $('total').value = parseFloat( $('subtotal').value).replace(/,/g,'') + parseFloat(totiva).replace(/,/g,'');
						}
					}
					else
						return true;
				}

		});
} 
	
function cal_iva2()
{
	$('total').value = parseFloat( $('subtotal').value.replace(/,/g,'')) + parseFloat($('iva').value.replace(/,/g,''));
	
}

function cal_neto()
{
	
	if ( parseFloat($('icic').value.replace(/,/g,'')) >  parseFloat($('total').value.replace(/,/g,'')))
		{
			alert("ICIC no debe ser mayor al total");
			$('icic').value=0;
		}
	   else
	    {
			if ( parseFloat( $('icic').value.replace(/,/g,'') ) >0.00)
				{
				$('neto').value = parseFloat($('total').value.replace(/,/g,'')) - parseFloat($('icic').value.replace(/,/g,''));
				}
			   else
				{
				$('neto').value = parseFloat($('total').value.replace(/,/g,''));
				}
		}

}

function imprime()
{
	var contrato=$('contrato').value;
	var prov=$('numprov').value;
	var nomprov=$('provname1').value;
	var tipoestima=$('tipoestima').value;
	switch(tipoestima)
	{
		case '1':
			tipoestima = 'ANTICIPO';
			break;
		case '2':
			tipoestima = 'ORDINARIA';
			break;
		case '3':
			tipoestima = 'ADITIVA';
			break;
		case '4':
			tipoestima = 'EXTRAORDINARIA';
			break;
		case '5':
			tipoestima = 'DEDUCTIVAS';
			break;
	}	
	var numestima=$('numestima').value;
	var importe=$('importe').value;
	var amortiza=$('amortiza').value;
	var subtotal=$('subtotal').value;
	var iva=$('iva').value;
	var total=$('total').value;
	var icic=$('icic').value;
	var neto=$('neto').value;
	var tipobien=$('tipobien').value;
	var tiporecurso=$('tiporecurso').value;
	switch(tiporecurso)
	{
		case '1':
			tiporecurso = 'ESTATAL';
			break;
		case '2':
			tiporecurso = 'PROPIO';
			break;
	}	
	var concepto=$('concepto').value;
	var factura=$('factura').value;
	var usuario=$('usuario').value;
	var depto=$('depto').value;
	var nomcoord=$('nomcoord').value;
	var nomdir=$('nomdir').value;
	var concepto_con = contrato+', '+tiporecurso+', '+tipoestima+', '+numestima+', '+concepto;
	location.href='php_ajax/impsol_pago.php?contrato='+contrato+"&tiporecurso="+
	tiporecurso+"&tipoestima="+tipoestima+"&numestima="+numestima+"&concepto_con="+
	concepto_con+"&nomprov="+nomprov+"&neto="+neto+"&factura="+factura+"&nomcoord="+nomcoord+"&nomdir="+nomdir;
}

 function Div_visible()
 {
	 var tabla=$('datos');
	 var count = tabla.rows.length;
	for(var i = count-1; i >=0; i--)
	{
		tabla.deleteRow(i);
	}
	 var busca = $('buscar').value;
     var divstyle = new String();
	 document.getElementById("master").style.visibility = "hidden";
	
	if (busca==1)
	{
 	 document.getElementById("folio").value ="";
	  divstyle = document.getElementById("fol").style.visibility = "visible";
	  divstyle = document.getElementById("search_suggestProv").style.visibility = "hidden";
	  divstyle = document.getElementById("search").style.visibility = "hidden";
	  divstyle = document.getElementById("fecha").style.visibility = "hidden";
	}
	if (busca==2)
	{
	  document.getElementById("provname1").value ="";
	  divstyle = document.getElementById("fol").style.visibility = "hidden";
	  divstyle = document.getElementById("search_suggestProv").style.visibility = "visible";
	  divstyle = document.getElementById("search").style.visibility = "visible";
	  divstyle = document.getElementById("fecha").style.visibility = "hidden";
	}
	if (busca==3)
	{
	  document.getElementById("fi").value ="";
	  document.getElementById("ff").value ="";
	  divstyle = document.getElementById("fol").style.visibility = "hidden";
	  divstyle = document.getElementById("search_suggestProv").style.visibility = "hidden";
  	  divstyle = document.getElementById("search").style.visibility = "hidden";
	  divstyle = document.getElementById("fecha").style.visibility = "visible";
	}
}



function busca_solpago()
{
	var busca = $('buscar').value;

//	 tabla.deleteRow();

	var folio = $('folio').value;
	
	var provname1 = $('provname1').value;
	var fi = $('fi').value;
	var ff = $('ff').value;
	var prov = $('numprov').value;
//	location.href="php_ajax/busqueda.php?busca="+busca+"&folio="+folio+"&provname1="+provname1+"&fi="+fi+"&ff="+ff;
	new Ajax.Request('php_ajax/busqueda.php?busca='+busca+'&folio='+folio+'&provname1='+provname1+'&fi='+fi+'&ff='+ff+'&prov='+prov,
					 {onSuccess : function(resp) 
					 {
						//alert(resp.responseText);
						 if( resp.responseText ) 
							{
								var tabla=$('datos');
								var numren=tabla.rows.length;
								for(var i = numren-1; i >=0; i--)
								{
									tabla.deleteRow(i);
								}								
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									for(var i = 0; i <myArray.length; i++)
									{
										var newrow = tabla.insertRow(i);
										newrow.className ="fila";
										//newrow.className ="d"+(i%2);
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.width="5%";
										newcell.className ="texto8";
										newcell.innerHTML =  myArray[i].folio;
										newcell.align ="center";
										
										var newcell1 = newrow.insertCell(1);
										newcell1.width="8%";
										newcell1.className ="texto8";
										newcell1.innerHTML =  myArray[i].contrato;
										newcell1.align ="center";
								////////////////////////////////////
										var tipoestima = ' ';
										switch(myArray[i].tipoestima)
											{
											case '1':
												tipoestima = 'ANTICIPO';
												break;
											case '2':
												tipoestima = 'ORDINARIA';
												break;
											case '3':
												tipoestima = 'ADITIVA';
												break;
											case '4':
												tipoestima = 'EXTRAORDINARIA';
												break;
											case '5':
												tipoestima = 'DEDUCTIVAS';
												break;
											}
								///////////////////////////////////
										var newcell2 = newrow.insertCell(2);
										newcell2.width="8%";
										newcell2.className ="texto8";
										newcell2.innerHTML = tipoestima;
										newcell2.align ="right";  
										
										var newcell3 = newrow.insertCell(3);
										newcell3.width="8%";
										newcell3.className ="texto8";
										newcell3.innerHTML =myArray[i].numestima;
										newcell3.align ="center"; 
								////////////////////////////////////
										var tiporecurso = ' ';
										switch(myArray[i].tiporecurso)
											{
											case '1':
												tiporecurso = 'ESTATAL';
												break;
											case '2':
												tiporecurso = 'PROPIO';
												break;
											}
								///////////////////////////////////
										var newcell4 = newrow.insertCell(4);
										newcell4.width="8%";
										newcell4.className ="texto8";
										newcell4.innerHTML = tiporecurso;
										newcell4.align ="center";
								////////////////////////////////////
										var tipobien = ' ';
										switch(myArray[i].tipobien)
											{
											case '1':
												tipobien = 'PUBLICO';
												break;
											case '2':
												tipobien = 'PROPIO';
												break;
											}
								///////////////////////////////////
										var newcell5 = newrow.insertCell(5);
										newcell5.width="8%";
										newcell5.className ="texto8";
										newcell5.innerHTML = tipobien;
										newcell5.align ="center"; 

										var newcell6 = newrow.insertCell(6);
										newcell6.width="8%";
										newcell6.className ="texto8";
										newcell6.innerHTML =myArray[i].importe;
										newcell6.align ="center"; 

										var newcell7 = newrow.insertCell(7);
										newcell7.width="8%";
										newcell7.className ="texto8";
										newcell7.innerHTML =myArray[i].iva;
										newcell7.align ="center"; 

										var newcell8 = newrow.insertCell(8);
										newcell8.width="8%";
										newcell8.className ="texto8";
										newcell8.innerHTML =myArray[i].neto;
										newcell8.align ="center"; 

										//var newcell9 = newrow.insertCell(9);
										//newcell9.width="3%";
										//newcell9.align ="center"; 
										//var folio = myArray[i].folio;//cambiousuario('+nombre+');"
										//newcell9.innerHTML = '<img src="../../imagenes/edit-icon.png" onClick="editar_sol(\''+folio+'\')">';

										var newcell9 = newrow.insertCell(9);
										newcell9.width="3%";
										newcell9.align ="center"; 
										var folio = myArray[i].folio;
										newcell9.innerHTML = '<img src="../../imagenes/consultar.jpg" onClick="mostrarPdf(\''+folio+'\')">' ;

										var newcell10 = newrow.insertCell(10);
										newcell10.width="3%";
										newcell10.align ="center"; 
										newcell10.innerHTML = '<img src="../../imagenes/eliminar.jpg" onClick="cancela_sol(\''+folio+'\')">' ;

									}
									document.getElementById("master").style.visibility = "visible";
								}
								else
								{
									alert("No existen resultados para estos criterios.");
								}
						}
					  }
				});
}


function mostrarPdf(folio) 
{
    window.location.href = "pdf_files/sol_pago_"
     + folio + ".pdf";
}   


function editar_sol(folio) 
{
    location.href = "edita_solpago.php?folio="+folio;
}   