<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../dompdf/dompdf_config.inc.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();

$contrato = strtoupper($_REQUEST['contrato']);
$prov = $_REQUEST['prov'];
$nomprov = $_REQUEST['nomprov'];
$tipoestima = str_replace(',','',$_REQUEST['tipoestima']);
$numestima = str_replace(',','',$_REQUEST['numestima']);
$importe = str_replace(',','',$_REQUEST['importe']);
$amortiza = str_replace(',','',$_REQUEST['amortiza']);
$subtotal = str_replace(',','',$_REQUEST['subtotal']);
$iva = str_replace(',','',$_REQUEST['iva']);
$total = str_replace(',','',$_REQUEST['total']);
$icic = str_replace(',','',$_REQUEST['icic']);
$neto = str_replace(',','',$_REQUEST['neto']);
$tipobien = $_REQUEST['tipobien'];
$tiporecurso = $_REQUEST['tiporecurso'];
$concepto = strtoupper($_REQUEST['concepto']);
$factura = $_REQUEST['factura'];
$usuario = strtoupper($_REQUEST['usuario']);
$depto = $_REQUEST['depto'];
$nomcoord = $_REQUEST['nomcoord'];
$nomdir = $_REQUEST['nomdir'];

$wtipoestima = $_REQUEST['wtipoestima'];
$wtiporecurso = $_REQUEST['wtiporecurso'];

$concepto_con = strtoupper($_REQUEST['concepto_con']);
$netox = str_replace(',','',$_REQUEST['neto']);
$neto_let = num2letras($netox);

$fails=false;

if ($conexion)
{
	$ejecuta ="{call sp_egresos_A_msolche_opublica(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$contrato,&$prov,&$wtipoestima,&$numestima,
						&$importe,&$amortiza,&$subtotal,&$iva,&$total,
						&$icic,&$neto,&$tipobien,&$wtiporecurso,&$depto,
						&$concepto,&$usuario,&$factura);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
//	echo $ejecuta;
//	print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
		
			$error = $row['error'];
			$folio = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$uresp = $row['uresp'];
			$nomresp = $row['nomresp'];
			$arearesp = $row['arearesp'];
			$upatrimonio = $row['upatrimonio'];
			$nompatrimonio = $row['nompatrimonio'];
			$areapatrimonio = $row['areapatrimonio'];
			$usolicita = $row['usolicita'];
			$nomsolicita = $row['nomsolicita'];
			$areasolicita = $row['areasolicita'];
			
			
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
		}
		sqlsrv_free_stmt( $R);
	}
}

$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
	{
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Documento sin t&iacute;tulo</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../../prototype/prototype.js'></script>
</head>

<body>
";

	$contrato = strtoupper($_REQUEST['contrato']);
	$prov = $_REQUEST['prov'];
	$nomprov = $_REQUEST['nomprov'];
	$tipoestima = str_replace(',','',$_REQUEST['tipoestima']);
	$numestima = str_replace(',','',$_REQUEST['numestima']);
	$importe = str_replace(',','',$_REQUEST['importe']);
	$amortiza = str_replace(',','',$_REQUEST['amortiza']);
	$subtotal = $_REQUEST['subtotal'];
	$iva = $_REQUEST['iva'];
	$total = $_REQUEST['total'];
	$icic = $_REQUEST['icic'];
	//$neto = str_replace(',','',$_REQUEST['neto']);
	$neto = $_REQUEST['neto'];
	$tipobien = $_REQUEST['tipobien'];
	$tiporecurso = $_REQUEST['tiporecurso'];
	$concepto = strtoupper($_REQUEST['concepto']);
	$factura = $_REQUEST['factura'];
	$usuario = strtoupper($_REQUEST['usuario']);
	$depto = $_REQUEST['depto'];
	$nomcoord = $_REQUEST['nomcoord'];
	$nomdir = $_REQUEST['nomdir'];
	$concepto_con = strtoupper($_REQUEST['concepto_con']);
	$netox = str_replace(',','',$_REQUEST['neto']);
	$neto_let = num2letras($netox);
	$detalle = "Subtotal: ".$subtotal.", Iva: $".$iva.", Factura: $".$total.", ICIC: $".$icic;

$html.="

<table width='600' border='0' align='center'>
   	    <tr>
		  <td width='100%'>
             <table width='100%' border='0'>
				<p></p>
				  <tr>
					<td align='left' width='20'><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                ".htmlentities("SOLICITUD DE PAGO DE OBRA PBLICA")." - </b></td>
					<td align='right' width='20'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
				  </tr>
		      </table> 
						<table width='100%'>
						   <tr>
							<td width='100%' align='left' class='texto9' colspan='2'><b>DIRECCION :</b> ". $nomdir ."</td><br>
						   </tr>
						   <tr>
							<td width='80%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
						  	<td width='20%' align='right' class='texto9'><b>FECHA:</b>".$fecha."</td>
						   </tr>
						</table>			
            <table width='100%' border='0'>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto12'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto10'><b>NUM. DE FACTURA:</b> ".$factura."</td>
              </tr>
            </table>	
            <table width='100%' border='0'>
              <tr>
                <td width='80%' align='left' class='texto10'><b>BENEFICIARIO:</b> ".$nomprov."</td>
                <td width='20%' align='right' class='texto10'><b>IMPORTE:</b> ".$neto."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto8'><b>CANTIDAD CON LETRA:</b>". strtoupper($neto_let) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td width='60%' align='left' class='texto7'>".$concepto_con."</td>
				<td width='40%' align='right' class='texto9'>".$detalle."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>_____________________________________</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>_____________________________________</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomsolicita."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' class='texto8'><B>".$areasolicita." </B></td>
                <td width='10%' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>              
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>

            </table>            
	 	  </td>
  	    </tr>  
	    <tr>
			<td width='100%'>
             <table width='100%' border='0'>
              <tr>
                <td align='left' width='20'><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                ".htmlentities("SOLICITUD DE PAGO DE OBRA PBLICA")." - </b></td>
                <td align='right' width='20'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
              </tr>
            </table>
			
			<table width='100%'>
			   <tr>
				<td width='100%' align='left' class='texto9' colspan='2'><b>DIRECCION :</b> ". $nomdir ."</td><br>
				</tr>
				<tr>
					<td width='80%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
					<td width='20%' align='right' class='texto9'><b>FECHA:</b>".$fecha."</td>
				</tr>
			</table>					
            <table width='100%' border='0'>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto12'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto10'><b>NUM. DE FACTURA:</B> ".$factura."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='80%' align='left' class='texto10'><B>BENEFICIARIO:</B> ".$nomprov."</td>
                <td width='20%' align='right' class='texto10'><b>IMPORTE:</b> ".$neto."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto10'><b>CANTIDAD CON LETRA:</b>". strtoupper($neto_let) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td width='60%' align='left' class='texto7'>".$concepto_con."</td>
				<td width='40%' align='right' class='texto9'>".$detalle."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>_____________________________________</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>_____________________________________</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomsolicita."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' class='texto8'><B>".$areasolicita."</B></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>
              
              <tr>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'></td>
             </tr>
            </table>
	 	  </td>
  	    </tr>  
	  </table>
	</body>
</html>";	

  $dompdf = new DOMPDF();
	
	$dompdf->set_paper('"letter","portrait"');
	
	$dompdf->load_html($html);
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/sol_pago_".$folio.".pdf", $pdf);
echo json_encode($folio);
///////////////////////////////////////////////////////////////
?>