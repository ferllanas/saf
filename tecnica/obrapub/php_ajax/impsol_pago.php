<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);


$contrato = strtoupper($_REQUEST['contrato']);
$nomprov = $_REQUEST['nomprov'];
$tipoestima = $_REQUEST['tipoestima'];
$numestima = $_REQUEST['numestima'];
$tiporecurso = $_REQUEST['tiporecurso'];
$neto = $_REQUEST['neto'];
$netox = str_replace(',','',$_REQUEST['neto']);
$folio = "";
////
$fecha="";
////

$concepto_con = $_REQUEST['concepto_con'];

$neto_let = num2letras($netox);

$firma1="Felipe";
$firma2="Said";
$firma3="Cardona";



?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width="97%"  border="0" align="center">
  <tr>
    <th width="24%" scope="row"><img src="../../../<?php echo $imagenPDFPrincipal;?>" width="126" height="49"></th>
    <th align="center" width="57%" height="71" scope="row"><p class="texto10">DIRECCION DE ADMINISTRACION Y FINANZAS
      <br>COORDINACION DE EGRESOS Y CONTROL PATRIMONIAL <br align="center" class="texto10">
    SOLICITUD DE PAGO DE OBRA P&Uacute;BLICA</th>
    <td width="19%"><div align="right"><img src="../../../<?php echo $imagenPDFSecundaria;?>" width="119" height="67"></div></td>
  </tr>
</table>
<table width="95%" height="24"  border="0">
  <tr>
    <th width="10%" scope="row" align="right" class="texto10">FOLIO</th>
    <th width="53%" align="left" scope="row" class="texto10_sub"><?php echo $folio;?></th>
    <th width="21%" scope="row" class="texto10" align="right">FACTURA</th>
    <th width="2%" scope="row">&nbsp;</th>
    <th width="14%" height="20" scope="row" class="texto10_sub"><?php echo $folio;?></th>
  </tr>
</table>
<table width="95%" height="79"  border="1" bordercolor="#000000" style="border-color:#000000; ">
  <tr>
    <th scope="row"><table width="97%"  border="0">
      <tr>
        <th width="4%" scope="row">&nbsp;</th>
        <td width="17%">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        <td width="12%">&nbsp;</td>
        <td width="25%">&nbsp;</td>
        <td width="8%"><div align="right"></div></td>
        <td width="1%"><div align="right"></div></td>
        <td width="13%" class="texto10" align="right">FECHA: </td>
        <td width="11%" class="texto10_sub"><?php echo $fecha;?></td>
      </tr>
      <tr>
        <th height="13" scope="row">&nbsp;</th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <th scope="row" class="texto10">BENEFICIARIO:</th>
        <td colspan="3" align="left" class="texto10_sub"><?php echo $nomprov;?></td>
        <td align="center"></td>
        <td align="center">&nbsp;</td>
        <td class="texto10" align="right">IMPORTE: </td>
        <td class="texto10_sub"><?php echo $neto;?></td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <th scope="row"><span class="Estilo1"></span></th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <th scope="row" class="texto10"><span >CANTIDAD C/LETRA:</span></th>
        <td colspan="6" align="left" class="texto10_sub"><?php echo $neto_let;?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <th scope="row"><span class="Estilo1"></span></th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <th scope="row" class="texto10"><span >CONCEPTO:</span></th>
        <td colspan="6" rowspan="3" align="left" valign="top" class="texto10_sub"><?php echo $concepto_con;?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th scope="row">&nbsp;</th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></th>
  </tr>
</table>
<table width="96%"  border="0">
  <tr>
    <th width="29%" scope="row" align="center" class="texto10">SOLICITA</th>
    <td width="4%">&nbsp;</td>
    <td width="29%">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td width="29%" align="center" class="texto10"><strong>AUTORIZA</strong></td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">____________________________________</div></td>
    <td>&nbsp;</td>
    <td align="center">____________________________________</div></td>
    <td>&nbsp;</td>
    <td align="center">____________________________________</div></td>
  </tr>
  <tr>
    <td scope="row" align="center" class="texto10"><?php echo $firma1;?></td>
    <td>&nbsp;</td>
    <td align="center" class="texto10"><?php echo $firma2;?></td>
    <td>&nbsp;</td>
    <td align="center" class="texto10"><?php echo $firma3;?></td>
  </tr>
</table>
</body>
</html>
