<?php
require_once("connections/dbconexion.php");

if(isset($_REQUEST['clear']))
{
	if($_REQUEST['clear']==='true')
	{
		setcookie('Rec_my_site',null,time()-999999);
		setcookie('ID_my_site',null,time()-999999); 
		setcookie('Key_my_site',null,time()-999999);	
		setcookie('privalmacen',null,time()-999999);
		setcookie('privsolche',null,time()-999999);		
		setcookie('privreq',null,time()-999999);				
		setcookie('depto',null,time()-999999);
		setcookie('puesto',		null,time()-999999);
	}
}
else
{
	$entro=false;
	if(isset($_COOKIE['ID_my_site'])) 
	{ 
		$username = $_COOKIE['ID_my_site']; 
		$pass = $_COOKIE['Key_my_site']; 		
		$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
		$conexion = sqlsrv_connect($server,$infoconexion);
		if ($conexion)
		{ 
			$ssql = "SELECT a.*, c.depto, c.puesto FROM menumusuarios a                     
                    INNER JOIN v_nomina_nomiv_nomhon6_nombres_activos_depto_dir c ON a.usuario COLLATE DATABASE_DEFAULT = c.numemp COLLATE DATABASE_DEFAULT
                    WHERE a.usuario = '".$username."'  and a.estatus<90";
			$getProducts = sqlsrv_query( $conexion,$ssql);		
			while($row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$entro=true;
				$hour = time() + 144000; 
				setcookie('ID_my_site', $username, 		$hour); 
				setcookie('Key_my_site',$_POST['pass'], $hour);	
				setcookie('privalmacen',$row['almacen'],$hour);
				setcookie('privsolche', $row['solche'], $hour);		
				setcookie('privreq', 	$row['req'], 	$hour);				
				setcookie('depto',		$row['depto'], 	$hour);
				setcookie('puesto',		$row['puesto'], $hour);
			}
			
			if($entro)
				header("Location: cuerpo.php");	
		} 
		else
		{ 
			echo "Error en la conexi�n con la base de datos. X s". odbc_errormsg() ; 
		}
	} 	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Acceso de usuarios</title>
<script type="text/javascript" src="javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript_globalfunc/jquery.reveal.js"></script>
<link type="text/css" href="css/estilos.css" rel="stylesheet">
<link type="text/css" href="css/reveal.css" rel="stylesheet">

<script> //Se crea la funci�n que imprime el resendPassword
function cerrarModal(){
	$('.close-reveal-modal').click();
}

$(document).ready(function(){
						  


$('#cambiarContrasena').click(function(e){
	console.log("Entra pero que paso?");
	$('#myModal').reveal();
	$('#modalInfo')
				.hide()
				.html('');
	/*$('#myModalId').html('<h1>Cambiar Contrase�a</h1><h2>Type your email address below to reset your password.</h2><div id="modalInfo"></div><form name="modalFormPass" id="modalFormPass" method="post" action="new-account.php"><div class="modalItem"><span class="label">What is your email address?</span><div class="modalItemLeft"><label>Email:</label></div><div class="modalItemRight"><input type="text" class="modalInput" name="email"></div><div class="clear"></div></div><div class="modalItemButton"><input type="submit" class="modalButton" value="Send"></div></form>');
	*/
	//Se le da acci�n al formulario
	$('#modalFormPass').live('submit',function(e){
		console.log("Envia nuevo password");
		e.preventDefault();
		//Se verifica si el usuario a ingresado algun valor
		var $user = $.trim($('#user',this).val());
		var $actual = $.trim($('#actual',this).val());
		var $nueva = $.trim($('#nueva',this).val());
		var $repnueva = $.trim($('#repnueva',this).val());
		
		console.log( $user +','+ $actual +','+ $nueva +','+ $repnueva);
		if($user.length == 0){
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor ingresa un nombre de usuario.</div>')
				.show(500);
				return false;
		}
		
		if($actual.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor la contrase&ntilde;a actual.</div>')
				.show(500);
				return false;
		}
		
		if($nueva.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor ingrese la contrase&ntilde;a nueva.</div>')
				.show(500);
				return false;
		}
		
		if($repnueva.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor repita la contrase&ntilde;a nueva.</div>')
				.show(500);
				return false;
		}
		
		if($repnueva!= $nueva)
		{
				$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Las contrase&ntilde;as no son iguales.</div>')
				.show(500);
				return false;
		}
		
		var $datos={
			user: $user,
			actual: $actual,
			nueva: $nueva,
			repnueva: $repnueva
		}
		//console.log($datos);
			//Se serializa el formulario
			//var $datos = $(this).serialize();
			$.ajax({
				type: 'post',
				data: $datos,
				url: 'cambiaContrasena.php',
				dataType: 'json',
				beforeSend: function(){
					$('input',this).each(function(){
						$(this).attr('disabled','disabled');
					});
				},
				error: function(xhr, status, error){
				console.log(xhr.responseText);
					$('#modalInfo')
						.hide()
						.html('<div class="infoModal infoError"><b>Warning</b><br>Something went wrong, please try again later.</div>')
						.show(500);
				},
				success: function($msg){
					if($msg.error == 1){
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoError"><b>Precauci&oacute;n</b><br>'+$msg.msg+'</div>')
							.show(500);
					}else{
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoCorrecto"><b>Success!</b><br>La contrase&ntilde;a ha sido actualizada con exito.</div>')
							.show(500);
						//Se cierra el modal
						setTimeout("cerrarModal()",1300);
					}
				}
			});
		
	});
});
 });
</script>
</head>
<body>
<form action="vallogin.php" method="post"> 
<table border="0px" bordercolor="#006600" align="center" style="margin-top:100px"> 
<tr>
	<td rowspan="5" width="50%" >
      <img src="saf.jpg"  width="300" height="200"  />	
	</td>
</tr>
<tr>
	<td  width="50%">
		<table  width="100%">
			<tr>
				<td colspan="2" >&nbsp;</td>				
			</tr>
			<tr>
				<td colspan="2" >&nbsp;</td>				
			</tr>
			<tr>
				<td colspan="2" >&nbsp;</td>				
			</tr>
			<tr>
				<td colspan="2" >&nbsp;</td>				
			</tr>
			<tr>
				<td width="50%" class="texto8"><b>Usuario:</b></td>
				<td> <input type="text" name="IdUsuario" size="20" maxlength="50" style="width:150px;"></td>
			</tr> 
			<tr>
			 	<td width="50%" class="texto8"><b>Contrase&ntilde;a</b></td>
			  	<td width="50%"><input type="password" name="pass" size="20" maxlength="50" style="width:150px;"></td>
			</tr> 
			<tr>
				<td height="68" colspan="2" align="center" class="texto7">
					 <input type="checkbox" name="recordar" value="ON" style="size:auto ">Recordar acceso&nbsp;&nbsp;&nbsp;
			  		<a href="#" id="cambiarContrasena" >Cambiar Contrase&ntilde;a</a>
			  </td>
			</tr>
			<tr height="12">
				<td height="46" colspan="2" class="texto8">
					<input type="submit" name="submit" value="Login" style="float: right">
				</td>
			</tr> 
		</table>
	</td>
 </tr>
 </table> 
</form>
<div id="myModal" class="reveal-modal medium">
	<div class="mywrapper">
    	<div id="modalInfo"></div>
		<div class="modal-logo">
			<img src="imagenes/fome.jpg">
		</div>
		<div id="myModalId">
        	<form name="modalFormPass" id="modalFormPass" method="post">
            	<div class="modalItem">
                	<span class="label">Cambiar Contrase&ntilde;a</span>
                 	<div class="modalItemLeft"><label>Usuario:</label></div>
                    <div class="modalItemRight"><input type="text" id="user" /></div>
                    <div class="clear"></div>
                    <div class="modalItemLeft"><label>Contrase&ntilde;a Actual:</label></div>
                    <div class="modalItemRight"><input type="password" id="actual" size="15"/></div>
                    <div class="clear"></div>
                    <div class="modalItemLeft"><label>Nueva Contrase&ntilde;a:</label></div>
                    <div class="modalItemRight"><input type="password" id="nueva" size="15" /></div>
                    <div class="clear"></div>
                    <div class="modalItemLeft"><label>Repite Nueva Contrase&ntilde;a:</label></div>
                    <div class="modalItemRight"><input type="password" id="repnueva" size="15" /></div>
                    <div class="clear"></div>
                </div>
                <div class="modalItemButton">
                	<input type="submit" class="modalButton" value="Enviar">
                </div>
            </form>
    	</div>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</div>
</body>
</html>
