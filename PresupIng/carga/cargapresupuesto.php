<!DOCTYPE html PUBLIC "-//W3C//Dtd class="texto8" XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd/xhtml1-transitional.dtd ">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Carga de Presupuesto de Ingresos</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript/presupuesto_funciones.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<script type="text/javascript" src="ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="ajaximage/scripts/jquery.form.js"></script>

<script type="text/javascript" >
$(document).ready(function() { 
		    $('#photoimg1').live('change', function()
			{ 
				//$("#preview1").html('');
				//$("#preview1").html('<img src="ajaximage/loader.gif" alt="Uploading...."/>');
				$("#imageform1").ajaxForm({
					target: '#preview1'
					}).submit();
				});
			});
</script>

<script> 
function validar(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) //alert('Tecla pulsada'); 
}  
</script> 

<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
</head>

<body  >
<!--<form id="form1" name="form1" method="post" action="">
<body> 
<input type="text" name="textfield" onkeyup="return validar(event)"> 
</body> 
</html>  -->

  <table width="100%" border="0">
  	<tr>
    	<td class="TituloDForma">Carga de Presupuesto de Ingresos
   	      <hr class="hrTitForma"></td>
    </tr>
    <tr>
      <td class="texto8"><table width="100%" border="0">
        <tr>
          <td class="texto8">
              <table width="100%" border="0">
               <tr>
          <td class="texto8" width="7%"><label>A&ntilde;o:</label></td>
          <td width="22%"><input class="texto8" name="anio" type="text" id="anio" maxlength="4" onkeyup="return validar(event)"  style="width:70px" /></td>
          <td class="texto8" width="49%" colspan="2">&nbsp;</td>
        </tr>         
        <tr>
          <td class="texto8"><label>Archivo(*.txt):</label></td >
          <td class="texto8" valign="bottom" align="left" colspan="3">
                <form id="imageform1" method="post" enctype="multipart/form-data" action='ajaximage/ajaximage.php'>
                       <input type="file" name="photoimg1" id="photoimg1" />
               </form><input class="texto8" type="button" value="agregar" onclick="cargarpresup()" />
               <div id='preview1'>
                </div>
                </td>
        </tr>
          </table>
        </td class="texto8">
        </tr>
        <tr>
          <td class="texto8" width="100%">
          <table class="tableone" id="sortable" border="0" >
          <thead>
            <tr>
              	<th style="width:30px">Cuenta</th>
                <th style="width:150px">Nombre de la Cuenta</th>
                <th style="width:30px; text-align:center">Anual</th>                
              	<th style="width:30px; text-align:center">Enero</th>
                <th style="width:30px; text-align:center">Febrero</th>
                <th style="width:30px; text-align:center">Marzo</th>
                <th style="width:30px; text-align:center">Abril</th>
                <th style="width:30px; text-align:center">Mayo</th>
                <th style="width:30px; text-align:center">Junio</th>
                <th style="width:30px; text-align:center">Julio</th>
                <th style="width:30px; text-align:center">Agosto</th>
                <th style="width:30px; text-align:center">Septiembre</th>
                <th style="width:30px; text-align:center">Octubre</th>
                <th style="width:30px; text-align:center">Noviembre</th>
                <th style="width:30px; text-align:center">Diciembre</th>                
                <th style="width:30px; text-align:center">Rubro</th>
                                
                              				                                            
            </tr>
          </thead>
           <tbody >
                    <tr>
                        <td colspan="16">
                            <div class="innerb" style="width:1300px;height:20em;">
                                <table class="tabletwo"  style="width:1300px;">
                                    <tbody  name="tfacturas" id="tfacturas" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
          </table></td class="texto8">
        </tr>
      </table></td class="texto8">
    </tr>
   <tr>
  <!-- <td >
    <table width="100%" border="0" >
     	<tr>
            <td class="texto8" style="width:250px">0000 No Presupuestales</td>
            <td class="texto8">$<input class="texto8"  type="text" id="np" name="np" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>
        <tr>
            <td class="texto8" style="width:250px">1000 Servicios Personales</td>
            <td class="texto8">$<input class="texto8"  type="text" id="sp" name="sp" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>
        <tr>
            <td class="texto8">2000 Materiales y Suministros</td>
             <td class="texto8">$<input class="texto8" type="text" id="ms" name="ms" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>  
        <tr>
            <td class="texto8">3000 Servicios Generales</td>
             <td class="texto8">$<input class="texto8" type="text" id="sg" name="sg" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>  
        <tr>
            <td class="texto8">4000 Apoyos Sociales</td>
             <td class="texto8">$<input class="texto8" type="text" id="as" name="as" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>  
        <tr>
            <td class="texto8">5000 Inversi&oacute;n en Activo Fijo</td>
             <td class="texto8">$<input class="texto8" type="text" id="af" name="af" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>  
        <tr>
            <td class="texto8">6000 Inversi&oacute;n en Obra Publica</td>
             <td class="texto8">$<input class="texto8" type="text" id="op" name="op" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>
        <tr>
            <td class="texto8">9000 Deuda Publica</td>
             <td class="texto8">$<input class="texto8" type="text" id="dp" name="dp" style="width:100px; text-align:right" value="0.00" readonly="readonly"/></td>
        </tr>
   	</table>-->
    </td>
    </tr>         
    <tr>
    	<td align="center">
        	<input type="button" onclick="guardardpresup();" id="gralguarda" name="gralguarda" value="Cargar Presupuesto" class="texto8" disabled="disabled"/>
        </td>
    </tr>
  </table>
<!--</form>-->
</body>
</html>