﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = "001981";//$_COOKIE['ID_my_site'];//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");
	$tfacturas="";
	
	$fecha="";
	$conceptos="";
	//$concepto="";
	$numero_poliza="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$partidas="";//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$descripcion="";
	$id_facts="";
	$fecpoliza="";
	if(isset($_REQUEST['partidas']))
			$partidas=$_POST['partidas'];
	
	//print_r($partidas);
	if(isset($_REQUEST['anio']))
		$anio=$_POST['anio'];

	if(isset($usuario))
	{
		if ( is_array( $partidas ) ) // Pregunta se es una array la variable facturas
		{		
			list($id, $fecha, $hora)= fun_presup_mpresup($anio,$usuario); 
			if(strlen($id)>0)//Para ver si en realidad se genero un folio de presupuesto
			{
				for( $i=0 ; $i  < count($partidas) ; $i++ )
				{
					
						$partidas[$i]['anual']=str_replace(",","",$partidas[$i]['anual']);
						if(strlen($partidas[$i]['anual'])<1)
							$partidas[$i]['anual']=0;
						$partidas[$i]['ene']=str_replace(",","",$partidas[$i]['ene']);
						if(strlen($partidas[$i]['ene'])<1)
							$partidas[$i]['ene']=0;
						$partidas[$i]['feb']=str_replace(",","",$partidas[$i]['feb']);
						if(strlen($partidas[$i]['feb'])<1)
							$partidas[$i]['feb']=0;
						$partidas[$i]['mar']=str_replace(",","",$partidas[$i]['mar']);
						if(strlen($partidas[$i]['mar'])<1)
							$partidas[$i]['mar']=0;
						$partidas[$i]['abr']=str_replace(",","",$partidas[$i]['abr']);
						if(strlen($partidas[$i]['abr'])<1)
							$partidas[$i]['abr']=0;
						$partidas[$i]['may']=str_replace(",","",$partidas[$i]['may']);
						if(strlen($partidas[$i]['may'])<1)
							$partidas[$i]['may']=0;
						$partidas[$i]['jun']=str_replace(",","",$partidas[$i]['jun']);
						if(strlen($partidas[$i]['jun'])<1)
							$partidas[$i]['jun']=0;
						$partidas[$i]['jul']=str_replace(",","",$partidas[$i]['jul']);
						if(strlen($partidas[$i]['jul'])<1)
							$partidas[$i]['jul']=0;
						$partidas[$i]['ago']=str_replace(",","",$partidas[$i]['ago']);
						if(strlen($partidas[$i]['ago'])<1)
							$partidas[$i]['ago']=0;
						$partidas[$i]['sep']=str_replace(",","",$partidas[$i]['sep']);
						if(strlen($partidas[$i]['sep'])<1)
							$partidas[$i]['sep']=0;
						$partidas[$i]['oct']=str_replace(",","",$partidas[$i]['oct']);
						if(strlen($partidas[$i]['oct'])<1)
							$partidas[$i]['oct']=0;
						$partidas[$i]['nov']=str_replace(",","",$partidas[$i]['nov']);
						if(strlen($partidas[$i]['nov'])<1)
							$partidas[$i]['nov']=0;
						$partidas[$i]['dic']=str_replace(",","",$partidas[$i]['dic']);
						if(strlen($partidas[$i]['dic'])<1)
							$partidas[$i]['dic']=0;
						//print_r($partidas);
						$fails = func_presup_dpresup($id,
								$partidas[$i]['numcuentacompleta'],
								$partidas[$i]['anual'],
								$partidas[$i]['ene'],
								$partidas[$i]['feb'],
								$partidas[$i]['mar'],
								$partidas[$i]['abr'],
								$partidas[$i]['may'],
								$partidas[$i]['jun'],
								$partidas[$i]['jul'],
								$partidas[$i]['ago'],
								$partidas[$i]['sep'],
								$partidas[$i]['oct'],
								$partidas[$i]['nov'],								
								$partidas[$i]['dic']);	
				}				
				
			}
			else
			{	$fails=true; $msgerror="No pudo cargar el presupuesto";}
			
		}
		else
			{	$fails=true; $msgerror=" No es un array la variable";}
		
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	$datos[0]['folio']=trim($id);
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);
	
function fun_presup_mpresup($anio,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	//sword_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	//echo $server.",".$username_db.",".$pas'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	$tsql_callSP ="{call sp_presing_A_mautorizado(?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$anio,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("sp_presing_A_mautorizado".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

//Funcion para registrar el detalle del presupuesto
function func_presup_dpresup($id,$cuenta,$total,$ene,$feb,$mar,$abr,$may,$jun,$jul,$ago,$sep,$oct,$nov,$dic)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	$params = $tsql_callSP ="{call sp_presing_A_dautorizado(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$cuenta,&$total,&$ene,&$feb,								
								&$mar,								
								&$abr,
								&$may,
								&$jun,
								&$jul,
								&$ago,
								&$sep,
								&$oct,
								&$nov,
								&$dic);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_presing_A_dautorizado". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>