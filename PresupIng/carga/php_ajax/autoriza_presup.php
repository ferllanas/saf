﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");	
	
	
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";	
	
	if(isset($_REQUEST['id']))
		$id=$_GET['id'];
	if(isset($usuario))
	{
		
		$tsql_callSP ="{call sp_presup_M_definitivo(?,?)}";//Arma el procedimeinto almacenado
		$params = array(&$id,&$usuario);//Arma parametros de entrada
		$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
		
		$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
		if( $stmt === false )
		{
			 $fails= "Error in statement execution.\n";
			 $fails=true;				
			 die("sp_presup_M_definitivo".  print_r($params)."". print_r( sqlsrv_errors(), true));
		}	
	header('Location: ../presup_autoriza.php?id=$id');//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	}
	else
	{
		echo "No existe el Usuario";
	}


?>