﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	$anio="";
	$mes="";
	$documento="";
	$ndoc="";
	$concepto="";
	$cta="";
	$montoc=0.00;
	$montod=0.00;
	$montoe=0.00;
	$montop=0.00;
	$ffecha=date("Y-m-d");
	$tfacturas="";
	
	$fecha="";
	$conceptos="";
	$msg="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	
	
	if(isset($_REQUEST['anio']))
		$anio=$_POST['anio'];
	//echo $anio;
	if(isset($_REQUEST['mes']))
		$mes=$_POST['mes'];
	if(isset($_REQUEST['documento']))
		$documento=$_POST['documento'];
	if(strlen($documento)<=0)
		$documento='0';
	if(isset($_REQUEST['ndoc']))
		$ndoc=$_POST['ndoc'];
	if(strlen($ndoc)<=0)
		$ndoc='0';
	if(isset($_REQUEST['concepto']))
		$concepto=$_POST['concepto'];
	if(isset($_REQUEST['numcuentacompleta']))
		$cta=$_POST['numcuentacompleta'];
	if(isset($_REQUEST['montoc']))
		$montoc=$_POST['montoc'];	
	if(isset($_REQUEST['montod']))
		$montod=$_POST['montod'];	
	if(isset($_REQUEST['montoe']))
		$montoe=$_POST['montoe'];	
	if(isset($_REQUEST['montop']))
		$montop=$_POST['montop'];	
	

	if(isset($usuario))
	{
		list($msg)= fun_ajustapresup($anio,$mes,$documento,$ndoc,$concepto,$cta,$montoc,$montod,$montoe,$montop,$usuario); 
		//echo "---".$msg;
		if(trim($msg)=="Ok")//Para ver si en realidad se genero un folio de presupuesto
		{
			$fails=false; 
			$msgerror="Ajuste Satisfactorio";
		}
		else
		{	
			$fails=true; 
			$msgerror="No pudo relizar el Ajuste. ".$msg;
		}		
	}
	else
	{	
		$fails=true; $msgerror="No existe usuario Logeado.";
	}	
	$datos[0]['msgerror']=$msgerror;
	$datos[0]['fails']=$fails;
	echo json_encode($datos);
	
function fun_ajustapresup($anio,$mes,$documento,$ndoc,$concepto,$cta,$montoc,$montod,$montoe,$montop,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;
	$msg="";
	//$msg="";
	$fails=false;	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	$tsql_callSP ="sp_presup_A_dajustes $anio,$mes,$documento,$ndoc,'$concepto','$cta',$montoc,$montod,$montoe,$montop,'$usuario'";//Arma el procedimeinto almacenado
	//$params = array(&$anio,&$mes,&$documento,&$ndoc,&$concepto,&$cta,&$montoc,&$montod,&$montoe,&$montop,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		
		//$msg .= implode(",", $params);
		$msg .=$tsql_callSP;
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC ))
		{
			//print_r($row);
			$msg = $row['msg'];			
		}
		sqlsrv_free_stmt($stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($msg);
}

?>