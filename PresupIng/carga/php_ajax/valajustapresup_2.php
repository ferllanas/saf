﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$anio="";
	$mes="";
	$cta="";
	$monto="";	
	
	if(isset($_REQUEST['anio']))
		$anio=$_POST['anio'];
	//echo $anio;
	if(isset($_REQUEST['mes']))
		$mes=$_POST['mes'];	
	if(isset($_REQUEST['numcuentacompleta']))
		$cta=$_POST['numcuentacompleta'];
	if(isset($_REQUEST['montoc']))
		$monto=$_POST['montoc'];	
	
	list($cierre,$disponible)= fun_valajustapresup($anio,$mes,$cta,$monto); 
	$datos[0]['cierre']=$cierre;
	$datos[0]['disponible']=$disponible;
	echo json_encode($datos);
	
function fun_valajustapresup($anio,$mes,$cta,$monto)
{
	global $server,$odbc_name,$username_db ,$password_db;
	$msg="";
	$fails=false;	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	$tsql_callSP ="{call sp_presup_c_disponible_anio_mes(?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$cta,&$monto,&$anio,&$mes);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("sp_presup_c_disponible_anio_mes".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC ))
		{
			//print_r($row);
			$cierre = $row['cierre'];
			$disponible = $row['disponible'];
		}
		sqlsrv_free_stmt($stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($cierre,$disponible);
}

?>