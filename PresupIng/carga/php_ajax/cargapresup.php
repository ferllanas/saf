<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$path="";
if(isset($_POST['ppath']))
{
	$path=$_POST['ppath'];
}
if(strlen($path)>0)
{
	$archivo = file("../".$path);
	if (!$archivo) 
	{
		 $datos['error']=1;
		 echo "<p>No se pudo abrir el archivo.</p>";
	}
	$filas=count($archivo);
	for($i=0;$i<$filas;$i++)
	{
		$campo=explode("\t",trim($archivo[$i]));
		if(isset($campo[0]))
		{
		 	$datos[$i]['cuenta']=$campo[0]; 
			$datos[$i]['nomcuenta']=validacuenta($campo[0]);
		}
		else
			$datos[$i]['cuenta']=""; 
		if(isset($campo[1]))
		 	$datos[$i]['anual']=$campo[1]; 
		else
			$datos[$i]['anual']="0";
		
		
		if(isset($campo[2]))
		 	$datos[$i]['ene']=$campo[2]; 
		else
			$datos[$i]['ene']="0";
		
		if(isset($campo[3]))
		 	$datos[$i]['feb']=$campo[3]; 
		else
			$datos[$i]['feb']="0";
		
		if(isset($campo[4]))
		 	$datos[$i]['mar']=$campo[4]; 
		else
			$datos[$i]['mar']="0";
		
		if(isset($campo[5]))
		 	$datos[$i]['abr']=$campo[5]; 
		else
			$datos[$i]['abr']="0";
		
		if(isset($campo[6]))
		 	$datos[$i]['may']=$campo[6]; 
		else
			$datos[$i]['may']="0";
			
		if(isset($campo[7]))
		 	$datos[$i]['jun']=$campo[7]; 
		else
			$datos[$i]['jun']="0";
		
		if(isset($campo[8]))
		 	$datos[$i]['jul']=$campo[8]; 
		else
			$datos[$i]['jul']="0";
		
		if(isset($campo[9]))
		 	$datos[$i]['ago']=$campo[9]; 
		else
			$datos[$i]['ago']="0";
		
		if(isset($campo[10]))
		 	$datos[$i]['sep']=$campo[10]; 
		else
			$datos[$i]['sep']="0";
		
		if(isset($campo[11]))
		 	$datos[$i]['oct']=$campo[11]; 
		else
			$datos[$i]['oct']="0";

		if(isset($campo[12]))
		 	$datos[$i]['nov']=$campo[12]; 
		else
			$datos[$i]['nov']="0";

		if(isset($campo[13]))
		 	$datos[$i]['dic']=$campo[13]; 
		else
			$datos[$i]['dic']="0";		
		$datos[$i]['canual']=$datos[$i]['ene']+$datos[$i]['feb']+$datos[$i]['mar']+$datos[$i]['abr']+$datos[$i]['may']+$datos[$i]['jun']+$datos[$i]['jul']+$datos[$i]['ago']+$datos[$i]['sep']+$datos[$i]['oct']+$datos[$i]['nov']+$datos[$i]['dic'];
		$datos[$i]['rubro']=buscarubro($campo[0]);
	}
}
//print_r($datos);
echo json_encode($datos);

function validacuenta($cuenta)
{
	global $username_db, $password_db,$odbc_name,$server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$consulta="select nomcta from presingmcuentas where cuenta='$cuenta'";
	$stmt2 = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$nomcuenta=utf8_encode($row['nomcta']);				
	}
	if(isset($nomcuenta))
		return($nomcuenta);
	else
		return('****** Error en Cuenta ******'); 
}
function buscarubro($cuenta)
{
	global $username_db, $password_db,$odbc_name,$server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$consulta="select rubro from v_presing_mcuentas where cuenta='$cuenta'";
	$stmt2 = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$rubro=$row['rubro'];				
	}
	if(isset($rubro))
		return($rubro);
	else
		return('0000'); 
}
	
?>
