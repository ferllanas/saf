<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Busqeda de Presupuestos</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		 <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
									  	
						var data = 'query='+ $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('presup_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
                    }, 'json');  // Json es una muy buena opcion
                });
            });			
			function cancela_poliza(vale, tipo)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					location.href="php_ajax/cancela_poliza.php?poliza="+vale+"&tipo="+tipo;
				}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                	<td>${id}</td>
					<td>${cuenta}</td>
					<td>${nomcta}</td>
					<td>${total}</td> 
					<td>${ene}</td>
					<td>${feb}</td>
					<td>${mar}</td>
					<td>${abr}</td>
					<td>${may}</td>
					<td>${jun}</td>
					<td>${jul}</td>
					<td>${ago}</td>
					<td>${sep}</td>
					<td>${oct}</td>
					<td>${nov}</td>
					<td>${dic}</td>					
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Búsqueda de Pesupuestos</span>
    <hr class="hrTitForma">
    <div id="main" style="width:1200px">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%" class="texto8">Folio del Presupuesto
</td>
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>

</tr>
</table>
<table >
                <thead>
      			<th>Id</th> 
                <th>Cuenta</th> 
				<th>Nombre</th>
                <th>Total</th>
                <th>Enero</th>
                <th>Febrero</th>
                <th>Marzo</th>
                <th>Abril</th>
                <th>Mayo</th>
                <th>Junio</th>
                <th>Julio</th>
                <th>Agosto</th>
                <th>Septiembre</th>
                <th>Octubre</th>
                <th>Noviembre</th>
                <th>Diciembre</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
