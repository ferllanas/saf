<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();

$cta = "";
$monto = 0;
$concepto = "";

//$usuario = $_COOKIE['ID_my_site'];
$usuario=trim($_REQUEST['usuario']);
$cta = trim($_REQUEST['cta']);
$cta2 = trim($_REQUEST['cta2']);
$mes_origen = trim($_REQUEST['mes_origen']);
$mes_destino = trim($_REQUEST['mes_destino']);
$concepto = trim($_REQUEST['concepto']);
$monto = str_replace(',','',$_REQUEST['monto']);


if ($conexion)
{
	$ejecuta ="{call sp_presup_A_dtrasp (?,?,?,?,?,?,?)}";
	$variables = array(&$cta,&$cta2,&$mes_origen,&$mes_destino,&$monto,&$concepto,&$usuario);

	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	//echo $ejecuta;
	//print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
			$datos[0]['msg'] = trim($row['msg']);
		}
		sqlsrv_free_stmt( $R);
	}
}
echo json_encode($datos); 
//header('Location: ../transferencia_pres.php');
?>