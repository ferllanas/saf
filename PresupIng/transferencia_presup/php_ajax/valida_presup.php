<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$cta = "";
$monto = 0;
$mes_origen = 0;
$anio = 0;
$cta = trim($_REQUEST['cta']);
$monto = str_replace(',','',$_REQUEST['monto']);
$anio = date("Y");
$mes_origen = $_REQUEST['mes_origen'];

if ($conexion)
{
	$ejecuta ="{call sp_presup_C_disponible_anio_mes(?,?,?,?)}";
	$variables = array(&$cta,&$monto,&$anio,&$mes_origen);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
//	echo $ejecuta;
//	print_r($variables);
	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;		
	}
	$fails=false;
	if(!$fails)
	{
		while($row = sqlsrv_fetch_array($R,SQLSRV_FETCH_ASSOC))
		{						
			$datos[0]['cierre']= $row['cierre'];
			$datos[0]['disponible']= $row['disponible'];
		}
		sqlsrv_free_stmt($R);
	}
	else
	{
		echo "Error";
	}
}
echo json_encode($datos);
?>