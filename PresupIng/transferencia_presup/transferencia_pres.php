<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();

$usuario = $_COOKIE['ID_my_site'];
//$usuario = "001349";

$cta_origen=0;
$cta_destino=0;

$anio_origen=0;
$anio_destino=0;


$sw1=0;
$sw2=0;

///////
 	$wdatos=array();
 	$anio = date("Y");
 	$mes = date("m");
	//echo $mes;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincPres.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>
<p><span class="seccionNota"><strong>Traspaso de Presupuesto </strong></span></p>
<hr class="hrTitForma">

<table width="97%" border="0">
  <tr>
    <td width="59" height="29"><div align="right"><span class="texto10">Cuenta Destino</span></div></td>
    <td width="664">
	
      <div align="left" style="z-index:4; position:relative; width:600px; height: 24px;">
        <input name="cuenta2" type="text" class="texto8" id="cuenta2" tabindex="1" onkeyup="searchcuenta2(this);" style="width:550px; height:20px; vertical-align:top;  " autocomplete="off">
		<img src="../../imagenes/deshacer.jpg" width="31" height="28" title="Deshacer alta de producto" onClick="deshacer1()">
        <div class="texto8" id="search_suggestcuenta2" style="z-index:3;" > </div>
      </div>
        </td>
    <td width="98">
	  <input type="hidden" name="sw1" id="sw1" value="<?php echo $sw1;?>">
      <input type="hidden" name="sw2" id="sw2" value="<?php echo $sw2;?>"></td>
    <td width="44"><input type="hidden" name="cta" id="cta">
      <input type="hidden" name="cta2" id="cta2">
    <input type="hidden" name="mes" id="mes" >
    <input type="hidden" name="disponible" id="disponible"></td>
  </tr>
  <tr>
    <td class="texto10"><div align="right">Cuenta Origen </div></td>
    <td>
      <div align="left" style="z-index:2; position:relative; width:600px; height: 24px;">
        <input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" onkeyup="searchcuenta(this);" onChange="valida_ctas2()" style="width:550px; height:20px; vertical-align:top; " autocomplete="off">
        <img src="../../imagenes/deshacer.jpg" width="31" height="28" title="Deshacer alta de producto" onClick="deshacer2()">        <div class="texto8" id="search_suggestcuenta" style="z-index:1;" > </div>
      </div>    </td>
    <td>&nbsp;</td>
    <td><input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
	<input type="hidden" name="cta_destino" id="cta_destino">
	<input type="hidden" name="cta_origen" id="cta_origen">  	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<table name="tabla2" id="tabla2" width="101%" border="0" style="visibility:hidden ">
  <tr>
    <td width="118" align="right"><div align="right"><span class="texto10">Mes de Origen</span></div></td>
    <td width="131">
		<select name="mes_origen" id="mes_origen" tabindex="3">
	
    	</select>
	</td>
    <td width="103" align="right"><span class="texto10">Mes Destino</span></td>
    <td width="676">
		<select name="mes_destino" id="mes_destino" tabindex="4" onblur="valida_mes()">

		</select>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"><span class="texto10">Monto</span></div></td>
    <td><span class="texto8">
      <input type="text" name="monto" id="monto" tabindex="5" disabled onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" style="width:90px; height:18px; text-align:right ">
    </span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30" class="texto10"><div align="right">Concepto</div></td>
    <td colspan="3"><span class="texto8">
      <input type="text" name="concepto" id="concepto" tabindex="6" disabled style="width:550px;">
    </span></td>
  </tr>
  <tr>
    <td height="31">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><table width="99%" border="1">
      <tr class="subtituloverde">
        <td width="108"><div align="center">MESES</div></td>
        <td width="52"><div align="center">ENERO</div></td>
        <td width="53"><div align="center">FEBRERO</div></td>
        <td width="55"><div align="center">MARZO</div></td>
        <td width="54"><div align="center">ABRIL</div></td>
        <td width="51"><div align="center">MAYO</div></td>
        <td width="57"><div align="center">JUNIO</div></td>
        <td width="52"><div align="center">JULIO</div></td>
        <td width="49"><div align="center">AGOSTO</div></td>
        <td width="73"><div align="center">SEPTIEMBRE</div></td>
        <td width="55"><div align="center">OCTUBRE</div></td>
        <td width="68"><div align="center">NOVIEMBRE</div></td>
        <td width="62"><div align="center">DICIEMBRE</div></td>
      </tr>
      <tr>
        <td class="texto10">CUENTA DESTINO</td>
        <td><input type="text" name="cd1" id="cd1" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd2" id="cd2" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd3" id="cd3" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd4" id="cd4" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd5" id="cd5" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd6" id="cd6" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd7" id="cd7" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd8" id="cd8" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd9" id="cd9" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd10" id="cd10" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd11" id="cd11" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="cd12" id="cd12" size="8" style="text-align:right " readonly value="0.00"></td>
      </tr>
      <tr>
        <td class="texto10">CUENTA ORIGEN</td>
        <td><input type="text" name="co1" id="co1" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co2" id="co2" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co3" id="co3" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co4" id="co4" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co5" id="co5" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co6" id="co6" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co7" id="co7" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co8" id="co8" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co9" id="co9" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co10" id="co10" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co11" id="co11" size="8" style="text-align:right " readonly value="0.00"></td>
        <td><input type="text" name="co12" id="co12" size="8" style="text-align:right " readonly value="0.00"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="31">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <input type="button" name="grabar" id="grabar" tabindex="7" disabled value="Grabar" onClick="guardar()">
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
