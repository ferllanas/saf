<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Detalle de Movimientos Presupuesta</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script src="../../javascript_globalfunc/funcionesGlobales.js"></script>
        <script src="javascript/busquedaPartida.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
			jQuery(document).ready(function() {
						
				jQuery(".botonExcel").click(function(event) {
					jQuery("#datos_a_enviar").val( jQuery("<div>").append( jQuery("#Exportar_a_Excel").eq(0).clone()).html());
					jQuery("#FormularioExportacion").submit();
				});
			});
			</script>
        <script>
			function validaMes2()
			{
				var mes1= $('#mes1').val();
				var mes2= $('#mes2').val();
				if(mes1>mes2)
				{
					alert("El mes de corte no puede ser anterior al mes inicial.");
					return false;
				}
				return true;
			}
           /* $(function()
			{
                $('#query').live('keyup', function()
				{ 
					var opcion= document.getElementById('opc').value; 
					var date = document.getElementById('fecini').value;
					var data = 'query=' + opcion +"&fecha="+date;     
					$.post('Presupuesto_Reporte_ajax.php',data, function(resp)
					{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
						// Checa el plugin de templating para Java, tomando los productos del script de abajo y 
						// los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });*/
			function buscar()
			{
                
					var tipo = $("#opc option:selected").val();//= document.getElementById('opc');
					if(tipo=="")
					{
						alert("Favor de seleccionar tiempo presupuestal.");
						return false;
					}
					var annio= document.getElementById('annio').value; 
					var mes1 = document.getElementById('mes1').value;
					var mes2 = document.getElementById('mes2').value;
					var partidaOCuenta =$("#dato1:checked").val();
					var cuentapart =$("#cuentapart").val();

					var data = 'tipo=' + tipo +"&annio="+annio+"&mes1="+mes1+"&mes2="+mes2+"&partidaOCuenta="+partidaOCuenta+"&cuentapart="+cuentapart;  
					//alert(data);
					$.post('Presupuesto_Reporte_ajax.php',data, function(resp)
					{ //Llamamos el arch ajax para que nos pase los datos}
						console.log(resp);
						var total=0.00;
						var MPA=0.00;
						var MPS=0.00;
						
						for(var i=0;i<resp.length; i++)
						{
							total += parseFloat(resp[i].importe.replace(/,/g,""));
							
							if(resp[i].xx=="X")
								MPS += parseFloat(resp[i].importe.replace(/,/g,""));
							else
								MPA += parseFloat(resp[i].importe.replace(/,/g,""));
								
						}
						
						//total = Math.round(total );
						//MPA   = Math.round(MPA );
						//MPS   = Math.round(MPS );
						
						totalstr=addCommas(total.toFixed(2));
						MPAstr=addCommas(MPA.toFixed(2));
						MPSstr=addCommas(MPS.toFixed(2));
						
						$('#total').val("$"+totalstr);
						$('#MPA').val("$"+MPAstr);
						$('#MPS').val("$"+MPSstr);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 

                    }, 'json');  // Json es una muy buena opcion    
            }
			function cancela_cheque(folio)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					
					location.href="php_ajax/cancela_cheque.php?folio="+folio;
				}
			}
			
			function cargaPartidas()
			{
                
					var data = '';     
					$.post('php_ajax/cargarPartidas.php',data, function(resp)
					{   
						console.log(resp);
						$('#cuentapart').empty();
                        $('#tmpl_cuentas').tmpl(resp).appendTo('#cuentapart'); 
						
                    }, 'json');  // Json es una muy buena opcion    
            }
			
			function cargaCuentas()
			{
        			var data = '';     
					$.post('php_ajax/cargarCuentas.php',data, function(resp)
					{
						console.log(resp);
                        $('#cuentapart').empty();
                        $('#tmpl_cuentas').tmpl(resp).appendTo('#cuentapart'); 
						
                    }, 'json');  // Json es una muy buena opcion    
            }
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if nomprov}}
					<td align="center">${cuenta}</td>
					<td>${docto}</td>
					<td align="center">${xx}</td>
					<td>${tipodocto}</td>
					<td>${nomprov}</td>
					<td align ="center">${falta}</td>
					<td align="right">$ ${importe}</td>
					<td>${concepto}</td> 
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
        
          <script id="tmpl_cuentas" type="text/x-jquery-tmpl">   
           	<option value="${id}">${nombre}</option>
          </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Detalle de Movimientos Presupuesta</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" >
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Comprometido</option>
              <option value="2" >Devengado</option>
              <option value="3" >Ejercido</option>
              <option value="4" >Pagado</option>
           </select></td>
<td width="8%">Año:<input type="text" name="annio" id="annio" value="<?php echo date("Y")?>" style="width:45px; text-align:right;"></td>
<td width="29%">Mes:<select id="mes1" name="mes1">
			<option value="01">ENE</option>
            <option value="02">FEB</option>
            <option value="03">MAR</option>
            <option value="04">ABR</option>
            <option value="05">MAY</option>
            <option value="06">JUN</option>
            <option value="07">JUL</option>
            <option value="08">AGO</option>
            <option value="09">SEP</option>
            <option value="10">OCT</option>
            <option value="11">NOV</option>
            <option value="12">DIC</option>
		</select></td>
<td width="40%">Mes Corte:<select id="mes2" name="mes2" onChange="validaMes2()">
			<option value="01">ENE</option>
            <option value="02">FEB</option>
            <option value="03">MAR</option>
            <option value="04">ABR</option>
            <option value="05">MAY</option>
            <option value="06">JUN</option>
            <option value="07">JUL</option>
            <option value="08">AGO</option>
            <option value="09">SEP</option>
            <option value="10">OCT</option>
            <option value="11">NOV</option>
            <option value="12">DIC</option>
		</select></td>
</tr>
<tr>
	<td><table><tr><td><input type="radio" value="1" name="dato1" id="dato1" checked onClick="cargaPartidas()">Partida</td>
    <td><input type="radio" value="2" name="dato1" id="dato1" onClick="cargaCuentas()">
    Cuenta</td>
    </tr></table>
    <td colspan="2"><div align="left" style="z-index:4; position:relative; width:250px; top: 5px; left: 15px; height: 24px;"><input name="cuentapart" type="text" class="texto8" id="cuentapart" tabindex="2" onKeyUp="searchdepto2(this);" style="width:250px; height:18px; " autocomplete="off">
      <div class="texto8" id="search_suggestdepto2" style="z-index:3;" > </div>
    </div></td>
    <!--<td><select id="cuentapart" name="cuentapart" style="width:150px">
<?php
	$consulta = "select partida, nompartida from presupmpartidas where estatus<9000";
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			?>
            <option value="<?php echo trim($row['partida']);?>" ><?php echo trim($row['partida']).".-".utf8_encode(trim($row['nompartida']));?></option>
            <?php
		}
		
	}
?>
    <option value="">TODOS</option></select></td>-->
<td align="right"><table width="100%"><tr><td width="50%"><input type="button" value="Buscar" onClick="buscar()"></td>
    							<td width="50%"><form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion" title="Los datos que se muestran en la tabla de abajo son lo que seran enviados a excel.">Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /><input type="hidden" id="datos_a_enviar" name="datos_a_enviar" /></form></td></tr></table>
    </td>
</tr>
</table>
<table  name="Exportar_a_Excel" id="Exportar_a_Excel">
	<thead>
     <caption>
	<tr>
    	<th colspan="8">
        	<table style="width:100%;">
            	<tr>    
                    <th style="text-align:right">Total:<input type="text" id="total" value="0.00" style="text-align:right;" readonly></th>
                    <th style="text-align:right">MPA:  <input type="text" id="MPA"   value="0.00" style="text-align:right;" readonly></th>
                    <th style="text-align:right">MPS:  <input type="text" id="MPS"   value="0.00" style="text-align:right;" readonly></th>
                </tr>
            </table>
        </th>
    </tr>
    <tr>
        <th>Cuenta</th> 
        <th>Folio</th> 
        <th>XX</th>   
        <th>Tipo Docto.</th>                                      
        <th>Proveedor</th>
        <th>Fecha de alta</th>
        <th>Importe</th>
        <th>Concepto</th>
     </tr>
   
</caption>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
