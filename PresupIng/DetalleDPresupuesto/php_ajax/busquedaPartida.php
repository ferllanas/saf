<?php
header("Content-type: text/html; charset=ISO-8859-1");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$q = $_REQUEST['q'];
$partidaOCuenta = $_REQUEST['partidaOCuenta'];
$consulta="";
$stra="";
	if($partidaOCuenta==1)
	{
		$consulta = "select partida, nompartida from presupmpartidas where estatus<9000";
		
		if(strlen($q) >0)
		{
			$tok=explode(" ",$q);
			for($i=0;$i<count($tok);$i++)
			{
				if( is_numeric($tok[$i]))
					$consulta.= " AND partida like '%".$tok[$i]."%'";
				else
					$consulta.= " AND nompartida like '%".$tok[$i]."%'";
			}
		}
	}
	else
	{
		$consulta = "select CUENTA as partida, NOMCTA as nompartida from presupmcuentas where estatus<9000";
		
		if(strlen($q) >0)
		{
			$tok=explode(" ",$q);
			for($i=0;$i<count($tok);$i++)
			{
					$consulta.= " AND (CUENTA like '%".$tok[$i]."%'";
					$consulta.= " OR NOMCTA like '%".$tok[$i]."%')";
			}
		}
	}
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			//$datos[$i]['id']= trim($row['partida']);
			//$datos[$i]['nombre']=trim($row['partida']).".-".utf8_encode(trim($row['nompartida']))
			$stra.=trim($row['partida'])."@".utf8_encode(trim($row['nompartida']))."\n";
           $i++;
		}
		
		//$datos[$i]['id']= "";
		//$datos[$i]['nombre']="TODAS";
		$stra.="@TODAS\n";
	}

echo utf8_decode($stra);
?>