<?php
header("Content-type: text/html; charset=UTF-8");
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= $_REQUEST['opcion'];//substr(,0,1);
$mes= $_REQUEST['mes'];
$year= $_REQUEST['year'];
$cuenta= $_REQUEST['cuenta'];

?>
<html>
<head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Detalle de Presupuesto</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
		<script src="../../cheques/javascript/funciones_cheque.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
</head>
<body>

<span class="TituloDForma">Detalle de Presupuestos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
   <form style="width:100%;" action="php_ajax/crearResumenglobalPresupuestosPDF.php" method="post" target="_blank" 
                                				id="FormularioExportacionPDF" 
                                                title="Los datos que se muestran en la tabla de abajo son lo que seran enviados a excel.">Exportar a PDF <img src="../../imagenes/pdf icon.gif" width="30px" class="botonPDF">
                                                <input type="hidden" id="datos_a_enviarPDF" name="datos_a_enviarPDF" />
                                                <input type="hidden" id="opc2" name="opc2">
                                                <input type="hidden" id="annio2" name="annio2">
                                                <input type="hidden" id="mes12" name="mes12">
                                                <input type="hidden" id="mes22" name="mes22">
                                                <input type="hidden" id="rubro2" name="rubro2">
                                </form>
<table>
                <thead>
                    <th>No. Doc</th> 
                    <th>Tipo Doc.</th>                                      
                    <th>Proveedor</th>
                    <th>Total</th>
                    <th>Concepto</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                   
<?php
	$command= "EXECUTE sp_presupuesto_reporteGlobalDetalleCuentas  $mes, $year, $opcion,'$cuenta'";
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		echo "<tr><td>".trim($row['docto'])."</td>";//$datos[$i]['docto']=
		echo "<td>".trim(utf8_encode($row['tipodoc']))."</td>";//$datos[$i]['tipodocto']=
		//$datos[$i]['falta']=trim($row['falta']);					
		echo "<td>".trim(utf8_encode($row['nomprov']))."</td>";//$datos[$i]['nomprov']=
		echo "<td>$".trim(number_format($row['total'],2))."</td>";//$datos[$i]['importe']=
		echo "<td>".trim(utf8_encode($row['concepto']))."</td>";//$datos[$i]['concepto']=
		//$datos[$i]['estatus']=trim($row['estatus']);
		$i++;
	}

?>
                </tbody>
			</table>
	</body>
</html>