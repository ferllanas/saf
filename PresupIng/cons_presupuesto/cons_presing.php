<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();

$anio=date ("Y");

//$usuario = $_COOKIE['ID_my_site'];
//$usuario = "001349";


$cta = "";
$nomcta = "";
$pa = 0;
$pp = 0;
$rec = 0;
$ene = 0;
$feb = 0;
$mar = 0;
$abr = 0;
$may = 0;
$jun = 0;
$jul = 0;
$ago = 0;
$sep = 0;
$oct = 0;
$nov = 0;
$dic = 0;

$totene = 0;

	$datos=array();
	if ($conexion)
	{
		$consulta = "select * from configmmeses order by mes";
		
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			//echo $consulta;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['mes']= trim($row['mes']);
				$datos[$i]['nombre']= trim($row['nombre']);
				$datos[$i]['corto']= trim($row['corto']);
				$i++;
			}
		}
	}



?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/funcion_cons.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<style type="text/css">
<!--
.Estilo1 {font-size: 8pt}
-->
</style>
</head>

<body>

<p><span class="seccionNota"><strong>Consulta Presupuesto de Ingresos</strong></span></p>
<hr class="hrTitForma">

<table width="851" border="0">
  <tr class="texto10">
    <td width="130"><div align="right">Mes Inicial </div></td>
    <td width="93">
      <select name="mesi" id="mesi" onChange="ver_datos()">
		<option value=0></option>
		<?php
			for($i = 0 ; $i<12;$i++)
			{
				echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
			}
		?>
      </select>
   </td>
    <td width="128"><div align="right">Mes Final </div></td>
    <td width="113"><select name="mesf" id="mesf" >
      <option value=0></option>
      <?php
			for($i = 0 ; $i<12;$i++)
			{
				echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
			}
		?>
    </select></td>
    <td width="112">A&ntilde;o 
         <input type="text" name="anio" id="anio" size="4" maxlength="4">
    </td>
    <td width="249">
    <input type="button" name="busca" value="Buscar" onClick="buscapres()">	</td>
  </tr>
</table>
<p>&nbsp;</p>
	
<table name="tabla_master" id="tabla_master" width="100%" height="19%" border=0 align="center" style="vertical-align:top; border-color: #006600">
  <td width="100%" height="1" style="vertical-align:top;">
      
<div style="overflow:auto; width: 100%; height :350px; align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; visibility:hidden; " >
	 <thead class="subtituloverde8">


	 
		<td width="24">CTA</td>
			<td width="136">NOMCTA</td>
			<td width="49"> <div align="center">PRES. ANUAL </div></td>
			<td width="50"><div align="center">PRES. PERIODO </div></td>
			<td width="45"><div align="center">RECAUDADO</div></td>
			<td width="41"><div align="center">ENE</div></td>
			<td width="38"><div align="center">FEB</div></td>
			<td width="43"><div align="center">MAR</div></td>
			<td width="42"><div align="center">ABR</div></td>
			<td width="41"><div align="center">MAY</div></td>
			<td width="40"><div align="center">JUN</div></td>
			<td width="42"><div align="center">JUL</div></td>
			<td width="41"><div align="center">AGO</div></td>
			<td width="43"><div align="center">SEPT</div></td>
			<td width="42"><div align="center">OCT</div></td>
			<td width="42"><div align="center">NOV</div></td>
			<td width="56"><div align="center">DIC</div></td>

	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
</div>



<p>&nbsp;</p>

<p class="seccionNota" name="letrero" id="letrero" style="visibility:hidden; ">No Presupuestales </p>
	<table name="master2" id="master2" width="100%" height="10%" border=0 align="center" style="overflow:auto; visibility:hidden;" >
	 <thead class="subtituloverde8">
	  <td width="25">CTA</td>
			<td width="148">NOMCTA</td>
			<td width="42"><div align="center">ENE</div></td>
			<td width="42"><div align="center">FEB</div></td>
			<td width="43"><div align="center">MAR</div></td>
			<td width="42"><div align="center">ABR</div></td>
			<td width="43"><div align="center">MAY</div></td>
			<td width="42"><div align="center">JUN</div></td>
			<td width="42"><div align="center">JUL</div></td>
			<td width="43"><div align="center">AGO</div></td>
			<td width="44"><div align="center">SEPT</div></td>
			<td width="42"><div align="center">OCT</div></td>
			<td width="43"><div align="center">NOV</div></td>
			<td width="50"><div align="center">DIC</div></td>
	</thead>

	  <tbody id="datos2" name="datos2">
	  </tbody>
	</table>

</body>
</html>
