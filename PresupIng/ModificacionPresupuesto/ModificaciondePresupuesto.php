<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$wdatos=array();
$cuenta='';
$a�o='';

$cuenta='';
$mes='';
$aut='';
$real='';
$mod='';
$ent_trasp='';
$sal_trasp='';
$com='';
$dev='';
$eje='';
$pag='';
$cob='';
$cierre='';

//$usuario=$_COOKIE['ID_my_site'];
$usuario='001349';

if ($conexion)
{		
	$consulta = "select anio from presupadmin group by anio order by anio desc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['anio']= trim($row['anio']);
		$i++;
	}
}
///////
	
	$consulta = "select mes from presupadmin group by mes order by mes";
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			//$wdatos[$i]['mes']= trim($row['mes']);
			$mes = trim($row['mes']);
			$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
			$xmes=substr($meses,$mes*10-10,10);
			$wdatos[$i]['mes']=$xmes;
			$i++;
		}
		$wdatos[$i]['mes']="TODOS";
	}
$mes='';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Modificaci&oacute;n de Presupuesto</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincPres.js"></script>
<script language="javascript" src="javascript/conscta_fun.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>

<table width="883" border="0">
<tr>
	<td>
    	<span class="TituloDForma">Modificaci&oacute;n de Presupuesto</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
    </td>
</tr>
  <tr>
  	<td>
    	<table>
        	<tr>
                <td width="50px">A&ntilde;o :</td>
                 <td width="100px">
                    <select name="anio" id="anio" onChange="meses()">
                    <?php
                        for($i = 0 ; $i<=count($datos);$i++)
                        {
                        echo "<option value=".$datos[$i]["anio"].">".$datos[$i]["anio"]."</option>\n";
                        }
                    ?>
                </select></td>
                <td width="60px">Cuenta :</td>
                <td width="150px"><div align="left" style="z-index:4; position:absolute; width:300px; top: 71px; left: 200px; height: 24px;">
                  <input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" onkeyup="searchdepto2(this);" style="width:300px; height:18px; " autocomplete="off" onClick="javascript: document.getElementById('cuenta').value=''">
                  <div class="texto8" id="search_suggestdepto2" style="z-index:3;" > </div>
                </div></td>
                <td>&nbsp;</td>
            </tr>
		</table>
    </td>
  </tr>
  <tr>
  	<td name="enc" id="enc" style="text-align:center; width:100%" align="center">
    	<table height="10%" align="center" border="1" style="text-align:center;">
        	<thead>
              <tr>
                <th width="100px" align="center" class="subtituloverde">MES</th>
                <th width="100px" align="center" class="subtituloverde">Autorizado</th>
              </tr>
             </thead>
            <tbody name="datos" id="datos">
            </tbody>
    </table>
    </td>
  </tr>
  <tr>
  	<td align="center">Motivo:<input type="text" value="" id="motivo" name="motivo"></td>
  </tr>
  <tr>
  	<td align="center"><input type="button" value="Modificar Presupuesto" onClick="modificarPresupuesto()">
    </td>
  </tr>
</table>
</body>
</html>
