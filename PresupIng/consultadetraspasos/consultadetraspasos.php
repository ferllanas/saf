<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);


 $datos=array();	
 $anio = date("Y");
 $mes_act = (int)date("m");

 
 	if ($conexion)
	{
		$consulta = "select * from configmmeses order by mes";
		
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			//echo $consulta;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['mes']= trim($row['mes']);
				$datos[$i]['nombre']= trim($row['nombre']);
				$datos[$i]['corto']= trim($row['corto']);
				$i++;
			}
		}
	}

// echo $mes_act;
 //echo $datos[$i]['mes'];

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedainc.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>
<p><span class="seccionNota"><strong>Consulta de Traspasos</strong></span></p>
<hr class="hrTitForma">
<table width="98%" border="0" align="center">
  <tr>
 <!--   <td width="80" class="texto10">Cuenta</td>
    <td width="259">
		<div align="left" style="z-index:2; position:relative; width:600px; height: 24px;">
        	<input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" onkeyup="searchcuenta(this);" style="width:580px; height:20px; " autocomplete="off">
        	<div class="texto8" id="search_suggestcuenta" style="z-index:1;" > </div>
		</div>	
	
	</td>-->
    <td width="70" class="texto10" align="right">A&ntilde;o:</td>
    <td width="70" class="texto10"><input type="text" name="anio" id="anio" size="4px" value="<?php echo $anio;?>" ></td>
    <td width="70" class="texto10" align="right">Mes Inicial:</td>
    <td width="70" class="texto10">
		<select name="mesi" id="mesi">
			<?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
				  	echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
				}
			?>
		</select>
	</td>
    <td width="70" class="texto10" align="right">Mes Final:</td>
    <td width="73" class="texto10">
		<select name="mesf" id="mesf" onChange="busca_polizas()">
			<?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
					echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
				}
			?>
		</select>
	</td>
  </tr>
  <tr>
    <td class="texto10"><input type="hidden" name="cta" id="cta"></td>
    <td class="texto10"><input type="button" onClick="busca_polizas()" value="Buscar"></td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
  </tr>
</table>

<!--<table width="98%" border="0" align="center">
  <tr class="texto10">
    <td width="76" align="right">Saldo Inicial:</td>
    <td width="148"><input type="text" name="sdoini" id="sdoini" size="20" readonly style="text-align:right " class="cajaresultado" tabindex="-1"></td>
    <td width="50" align="right">Cargos:</td>
    <td width="123"><input type="text" name="cargos" id="cargos" size="20" readonly style="text-align:right " class="cajaresultado" tabindex="-1"></td>
    <td width="52" align="right">Creditos:</td>
    <td width="129"><input type="text" name="creditos" id="creditos" size="20" readonly style="text-align:right " class="cajaresultado" tabindex="-1"></td>
    <td width="76" align="right">Saldo Final:</td>
    <td width="152"><input type="text" name="sdofinal" id="sdofinal" size="20" readonly style="text-align:right " class="cajaresultado" tabindex="-1"></td>
  </tr>
</table>-->
<table name="master" id="master" width="98%" height="10%" border=0 align="center" style="overflow:auto; " >
  <thead class="subtituloverde8">
      <td width="70">A&Ntilde;O</td>
      <td width="103">MES</td>
      <td width="127">CUENTA</td>
      <td width="328" align="left">NOMCTA</td>
      <td width="97">SALIDA</td>
      <td width="107">ENTRADA</td>
  <tbody id="datos" name="datos">
  </tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
