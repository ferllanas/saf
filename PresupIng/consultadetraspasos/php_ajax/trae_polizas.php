<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
		
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$mesi = $_REQUEST['mesi'];
$mesf = $_REQUEST['mesf'];
//$cta = $_REQUEST['cta'];
$anio = $_REQUEST['anio'];

$datos=array();

if ($conexion)
{
	$consulta = "EXECUTE sp_presup_C_traspasos $anio,$mesi,$mesf";
	
	//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['anio']= trim($row['a�o']);
			$datos[$i]['mes']= trim($row['mes']);
			$datos[$i]['cuenta']= trim($row['cuenta']);
			$datos[$i]['nomcta']= utf8_encode(trim($row['nomcta']));
			$datos[$i]['entrada']=  number_format(trim($row['entrada']),2);
			$datos[$i]['salida']=  number_format(trim($row['salida']),2);
			$i++;
		}
	}
}
//$datos['consulta']=$consulta;
echo json_encode($datos);
?>