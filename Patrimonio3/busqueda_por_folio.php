<?php
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$folio="";
if(isset($_POST['folio']))
	$folio = $_POST['folio'];

$entro=false;
$datos= array();
if($conexion)
{
	$command= "select A.*,B.TIPO,c.ip,c.segmento,c.operador,c.nomequipo,c.nivelinternet,c.windows as verwindows,c.office,
c.mem,c.tarjetain,c.antivirus,c.capacidad,c.procesador,c.unidad,c.unidad_dd, c.ip, CASE A.subtipo WHEN '0941' THEN 1
	 WHEN '0653' THEN 1
	 WHEN '0647' THEN 1
	 WHEN '0639' THEN 1
	 WHEN '0637' THEN 1
	 WHEN '0628' THEN 1
	ELSE	0
	END as editable
from [fome2011].[dbo].activoDmuebles A 
LEFT JOIN [fome2011].[dbo].ACTIVOMSUBTIPOS B ON A.SUBTIPO=B.SUBTIPO 
LEFT JOIN [fome2011].[dbo].ACTIVODequipos C on a.folio=c.folio 
where a.folio like '%$folio' and a.ESTATUS<9000 order by A.unico desc";
	
/*$datos[0]['folio']="select A.*,B.TIPO,c.ip,c.segmento,c.operador,c.nomequipo,c.nivelinternet,c.windows as verwindows,c.office,
c.mem,c.tarjetain,c.antivirus,c.capacidad,c.procesador,c.unidad,c.unidad_dd from [fome2011].[dbo].activoDmuebles A LEFT JOIN 
[fome2011].[dbo].ACTIVOMSUBTIPOS B ON A.SUBTIPO=B.SUBTIPO LEFT JOIN [fome2011].[dbo].ACTIVODequipos C on a.folio=c.folio 
where a.folio like '%$folio' and a.ESTATUS<9000 order by A.unico desc";*/
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."<br>". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//echo $row['falta'];
			$datos[$i]['folio']= trim($row['folio']);
			$datos[$i]['marca']= utf8_encode(trim($row['marca']));
			$datos[$i]['descrip']= utf8_encode(trim($row['descrip']));
			$datos[$i]['usuario']= trim($row['usuario']);
			$datos[$i]['observa_1']= utf8_encode(trim($row['observa_1']));
			$datos[$i]['observa_2']= utf8_encode(trim($row['observa_2']));
			$datos[$i]['folioant']= trim($row['folioant']);
			$datos[$i]['tipo']= trim($row['tipo']);
			$datos[$i]['nomsubtipo']= utf8_encode(trim($row['nomsubtipo']));
			$datos[$i]['subtipo']= trim($row['subtipo']);
			$datos[$i]['ip']= trim($row['ip']);
			$datos[$i]['editable']= trim($row['editable']);
			
			$i++;
		}
	}
	echo json_encode($datos);
}
?>