<?php
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$folio="";
if(isset($_REQUEST['folio']))
	$folio = $_REQUEST['folio'];

$unico=0;
$tipo="";
$tipo_ant="";
$folio_ant="";
$edifcio_cve="";
$edifcio_desc="";
$piso="";
$c_ip="";
$tipo_cve="";
$tipo_desc="";
$sub_tipo_cve="";
$sub_tipo_desc="";
$Tipo_Adq_cve="";
$Tipo_Adq_desc="";
$Cond_Fisicas__cve="";
$Cond_Fisicas__desc="";
$Responsable_cve="";
$Responsable_desc="";
$c_segmento="";
$c_operador="";
$Nombre_del_Equipo="";
$c_nivelinternet="";
$c_windows="";
$c_office="";
$c_mem="";
$c_tarjetain="";
$c_capacidad="";
$c_unidad_dd="";
$c_antivirus="";
$Estatus_cve="";
$Estatus_desc="";
$Fecha="";
$estado_descripcion="";
$c_procesador="";
$c_unidad="";
$Serie= "";
$Factura= "";
$Valor= "";
$Fecha_de_Adq= "";

$c_nomequipo="";
$c_tarjetain="";

$entro=false;
$datos= array();
if($conexion)
{
	$command= "select  A.*,B.TIPO,c.ip,c.segmento,c.operador,c.nomequipo,c.nivelinternet,c.windows as verwindows,c.office,
c.mem,c.tarjetain,c.antivirus,c.capacidad,c.procesador,c.unidad,c.unidad_dd ,
D.nomedif, E.nomtipo, F.nomsubtipo, G.descrip as Tipo_Adq_desc, I.nomemp,  J.descrip as Estatus_desc, CONVERT(varchar(10),A.fcaptura, 103) as fecha, CONVERT(varchar(10),A.fechaadq, 103) as Fecha_de_Adq
from [fome2011].[dbo].activoDmuebles A 
LEFT JOIN [fome2011].[dbo].ACTIVOMSUBTIPOS B ON A.SUBTIPO=B.SUBTIPO 
LEFT JOIN [fome2011].[dbo].ACTIVODequipos C ON a.folio=c.folio 
LEFT JOIN [fome2011].[dbo].activomedificios D ON A.edif=D.edif
LEFT JOIN [fome2011].[dbo].activomtipos E ON E.tipo=B.TIPO
LEFT JOIN [fome2011].[dbo].activomsubtipos F ON F.subtipo=A.subtipo
LEFT JOIN [fome2011].[dbo].activomtipoadq G ON G.tipoadq=A.tipoadq
LEFT JOIN [fome2011].[dbo].[activomcondfis] H ON H.condfis=A.condfis
LEFT JOIN [fomeadmin].[dbo].v_nomina_nomiv_nomhon6_nombres_activos_depto_dir I ON  I.numemp collate DATABASE_DEFAULT =A.respbien  collate DATABASE_DEFAULT
LEFT JOIN[fomeadmin].[dbo].activomestatus J ON J.estatus=A.estatus
where a.folio ='$folio' and a.ESTATUS<9000 order by A.unico desc";

//echo $command;

	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."<br>". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$tipo= substr(trim($folio),0,2);//trim();//$row['TIPO'] 
			$folio = substr(trim($folio),2);
			$unico =  trim($row['unico'] );
			
			$folio_ant= trim($row['folioant'] );
			//echo strlen($folio_ant);
			//$folio_ant="";
			if(strlen($folio_ant)>2){
				$tipo_ant= substr(trim($folio_ant),0,2);
				$folio_ant = substr(trim($folio_ant),2);
			}
			else
			{
				$tipo_ant= substr(trim($folio_ant),0,2);
				$folio_ant = "";
			}
			
			$edifcio_cve= trim($row['edif'] );
			$edifcio_desc= trim($row['nomedif'] );
			$piso= trim($row['piso'] );
			$tipo_cve= trim($row['TIPO'] );
			$tipo_desc= trim($row['nomtipo'] );
			$sub_tipo_cve= trim($row['subtipo'] );
			$sub_tipo_desc= trim($row['nomsubtipo'] );
			$Tipo_Adq_cve= trim($row['tipoadq'] );
			$Tipo_Adq_desc= trim($row['Tipo_Adq_desc'] );
			$Cond_Fisicas__cve= trim($row['condfis'] );
			$Cond_Fisicas__desc= trim($row['Tipo_Adq_desc'] );
			$Responsable_cve= trim($row['respbien'] );
			$Responsable_desc= trim($row['nomemp'] );
			$Nombre_del_Equipo= trim($row['descrip'] );
			$Estatus_cve= trim($row['estatus'] );
			$Estatus_desc= trim($row['Estatus_desc'] );
			$Fecha= trim($row['fecha'] );
			$estado_descripcion= trim($row['observa_1'] );
			$marca=trim($row['marca'] );
			$descrip=trim($row['descrip'] );
			$Serie= trim($row['serie'] );
			$Factura= trim($row['factura'] );
			$Valor= trim($row['valor'] );
			$Fecha_de_Adq= trim($row['Fecha_de_Adq'] );
			$Fecha_de_Adq=substr($Fecha_de_Adq,6,2)."/".substr($Fecha_de_Adq,4,2)."/". substr($Fecha_de_Adq,0,4);
			
			////////////////////////
			$c_ip= 				trim($row['ip'] );
			$c_segmento= 		trim($row['segmento'] );
			$c_operador= 		trim($row['operador'] );
			$c_nomequipo= 		trim($row['nomequipo'] );
			$c_nivelinternet= 	trim($row['nivelinternet'] );
			$c_windows= 		trim($row['verwindows'] );
			$c_office= 			trim($row['office'] );
			$c_mem= 			trim($row['mem'] );
			$c_tarjetain= 		trim($row['tarjetain'] );
			$c_antivirus= 		trim($row['antivirus'] );
			$c_capacidad= 		trim($row['capacidad'] );
			$c_procesador= 		trim($row['procesador'] );
			$c_unidad_dd= 		trim($row['unidad_dd'] );
			$c_unidad= 			trim($row['unidad'] );
		
		}
	}
}
        
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../javascript_globalfunc/191jquery.min.js"></script>
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
<script type="text/javascript" src="../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
<script>
$(document).ready(function() {
						   
	
    //set initial state.
    $('#c_tarjetain').change(function() {
        if($(this).is(":checked")) {
           $(this).val(true)
        }
		else
		{
			$(this).val(false)
		}
        $('#c_tarjetain').val($(this).is(':checked'));        
    });
});

</script>
<link rel="stylesheet" href="../calendario/skins/aqua/theme.css" 	type="text/css" media="all"  title="Aqua" />
<title>Documento sin título</title>
</head>

<body>
<form action="guardar_resguardo.php" method="post">
<table>
	<tr>	
    	<td colspan="2"><h1>Patrimonio</h1></td>
    </tr>
	<tr>
    	<td colspan="4">
        	<table>
            	<tr>
    	<td>Folio:</td><td><input type="text" name="tipo" id="tipo" style="width:30px;" value="<?php echo $tipo;?>" readonly="readonly" maxlength="2"/></td><td><input type="text" name="folio" id="folio" value="<?php echo $folio;?>" style="width:100px;" readonly="readonly" maxlength="12"/></td>
        <td>Anterior:</td><td><input type="text" name="tipo_ant" id="tipo_ant"  style="width:30px;" value="01" readonly="readonly" maxlength="2"/></td><td><input type="text" name="folio_ant" id="folio_ant" value="<?php echo $folio_ant;?>" style="width:100px;" readonly="readonly" maxlength="12"/><input type="hidden" id="unico" name="unico" value="<?php echo $unico;?>" /></td>
        		</tr>
            </table>
        </td>
    </tr>
 <!--   <tr>
    	<td># Serie:</td><td colspan="5"><input type="text" value="<?php echo $Serie;?>" id="Serie" name="Serie" readonly="readonly"/></td>
    </tr>
    <tr>
    	<td>Edificio:</td>
        <td><select name="edificio_cve" id="edificio_cve" readonly="readonly" disabled="disabled">
        <?php
			$activomedificios= get_FieldsOfSimpleQuery("activomedificios", "edif,nomedif", "ORDER BY edif", "");
			for($i=0;$i<count($activomedificios);$i++)
			{
		?>
        	<option value="<?php echo trim($activomedificios[$i]['edif']);?>" <?php if(trim($activomedificios[$i]['edif'])==$edifcio_cve) echo "SELECTED";?>><?php echo trim($activomedificios[$i]['edif']).".-".trim($activomedificios[$i]['nomedif']);?></option>
		<?php
			}
        ?></select>
        </td>
         <tr>
    	<td>Piso:</td><td><input type="text" name="piso" id="piso" value="<?php echo $piso;?>" style="width:60px;" readonly="readonly"/></td>
    </tr>
        
    </tr>
     <tr>
    	<td>Tipo:</td>
        <td><select name="tipo_cve" id="tipo_cve" readonly="readonly" disabled="disabled">
        <?php
			$activomtipos= get_FieldsOfSimpleQuery("activomtipos", "tipo,nomtipo", "ORDER BY tipo", "");
			for($i=0;$i<count($activomtipos);$i++)
			{
		?>
        	<option value="<?php echo trim($activomtipos[$i]['tipo']);?>" <?php if(trim($activomtipos[$i]['tipo'])==$tipo_cve) echo "SELECTED";?>><?php echo trim($activomtipos[$i]['tipo']).".-".trim($activomtipos[$i]['nomtipo']);?></option>
		<?php
			}
        ?></select>
        </td>
        <td>&nbsp;</td>        
    </tr>
     <tr>
    	<td>Sub_Tipo:</td>
        <td><select name="sub_tipo_cve" id="sub_tipo_cve" readonly="readonly" disabled="disabled">
        <?php
			$activomsubtipos= get_FieldsOfSimpleQuery("activomsubtipos", "subtipo,nomsubtipo", "ORDER BY nomsubtipo", "");
			for($i=0;$i<count($activomsubtipos);$i++)
			{
		?>
        	<option value="<?php echo $activomsubtipos[$i]['subtipo'];?>" <?php if($activomsubtipos[$i]['subtipo']==$sub_tipo_cve) echo "SELECTED";?>><?php echo $activomsubtipos[$i]['subtipo'].".-".$activomsubtipos[$i]['nomsubtipo'];?></option>
		<?php
			}
        ?></select>
      </td>
        <td>&nbsp;</td>        
    </tr>
    <tr>
    	<td>Descripción:</td>
        <td><textarea id="descrip" name="descrip" readonly="readonly"><?php echo $descrip;?></textarea>
        </td>
        <td>&nbsp;</td>        
    </tr>
     <tr>
    	<td>Marca:</td>
        <td><input type="text" id="marca" name="marca" value="<?php echo $marca;?>" readonly="readonly">
        </td>
        <td>&nbsp;</td>        
    </tr>
    
    <tr>
    	<td>Modelo:</td>
        <td><input type="text" id="modelo" name="modelo" value="<?php echo $modelo;?>" readonly="readonly" >
        </td>
        <td>&nbsp;</td>        
    </tr>
    <tr>
   		 <td>Nombre del Equipo:</td>
        <td><input type="text" name="Nombre_del_Equipo" id="Nombre_del_Equipo" value="<?php echo $Nombre_del_Equipo;?>" readonly="readonly" /></td> 
    </tr>
     <tr>
    	<td>Tipo_Adq:</td>
        <td><select name="Tipo_Adq_cve" id="Tipo_Adq_cve" readonly="readonly" disabled="disabled" > 
        <?php
			$activomtipoadq= get_FieldsOfSimpleQuery("activomtipoadq", "tipoadq,descrip", "ORDER BY descrip", "");
			for($i=0;$i<count($activomtipoadq);$i++)
			{
		?>
        	<option value="<?php echo $activomtipoadq[$i]['tipoadq'];?>" <?php if($activomtipoadq[$i]['tipoadq']==$Tipo_Adq_cve) echo "SELECTED";?>><?php echo $activomtipoadq[$i]['tipoadq'].".-".$activomtipoadq[$i]['descrip'];?></option>
		<?php
			}
        ?></select>
        </td>
        <td>&nbsp;</td>        
    </tr>
    <tr>
    	<td>Cond. Fisicas:</td>
        <td><select name="Cond_Fisicas_cve" id="Cond_Fisicas_cve" readonly="readonly" disabled="disabled"> 
        <?php
			$activomcondfis= get_FieldsOfSimpleQuery("activomcondfis", "condfis,descrip", "ORDER BY descrip", "");
			for($i=0;$i<count($activomcondfis);$i++)
			{
		?>
        	<option value="<?php echo $activomcondfis[$i]['condfis'];?>" <?php if($activomcondfis[$i]['condfis']==$Cond_Fisicas__cve) echo "SELECTED";?>><?php echo $activomcondfis[$i]['condfis'].".-".$activomcondfis[$i]['descrip'];?></option>
		<?php
			}
        ?></select>
        </td>
        <td>&nbsp;</td>        
    </tr>
     <tr>
    	<td>Responsable:</td>
        <td colspan="2"><input type="text" name="Responsable_cve" id="Responsable_cve" value="<?php echo $Responsable_cve;?>" style="width:60px;" readonly="readonly" />	<input type="text" name="Responsable__desc" id="Responsable_desc" value="<?php echo $Responsable_desc;?>" style="width:350px" readonly="readonly"/></td>        
    </tr>
</table>
<tr>
    	<td>Factura:</td>
        <td><input type="text" name="Factura" id="Factura" value="<?php echo $Factura;?>" style="width:120px;" readonly="readonly"/></td>
        <td>Valor:</td>
        <td><input type="text" name="Valor" id="Valor" value="<?php echo $Valor;?>" readonly="readonly"/></td> 
        <td>Fecha de Adq.:</td>
        <td><input type="text" name="Fecha_de_Adq" id="Fecha_de_Adq" value="<?php echo $Fecha_de_Adq;?>" style="width:80px;" readonly="readonly"/></td>   
    </tr>
    
    <table>
  <tr>
    	<td>Estatus:</td>
        <td><input type="text" name="Estatus_cve" id="Estatus_cve" value="<?php echo $Estatus_cve;?>" style="width:60px;"/></td>
        <td><input type="text" name="Estatus_desc" id="Estatus_desc" value="<?php echo $Estatus_desc;?>" style="width:60px;"/></td> 
        <td>Fecha:</td>
        <td><input type="text" name="Fecha" id="Fecha" value="<?php echo $Fecha;?>" style="width:80px;" /></td><td>
        <img src="../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><script type="text/javascript">
												Calendar.setup({
													inputField     :    "Fecha",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script></td>        
    </tr>
     <tr>
     	<td>Descripcion:</td>
        <td colspan="6"><textarea name="observa_1" id="observa_1" style="width:100%; height:150px;"><?php echo utf8_encode($estado_descripcion);?></textarea></td>    
    </tr>
     <tr>
     	<td>Observaciones:</td>
        <td colspan="5"><textarea name="observa_2" id="observa_2" style="width:100%; height:150px;"><?php echo utf8_encode($Observaciones);?></textarea></td>    
    </tr>
--></table>

<table>
    <tr>
    	<td colspan="6"><h1>Equipo edición</h1></td>
    </tr>
    <tr>
        <td>Operador:</td>
        <td><input type="text" name="c_operador" id="c_operador" value="<?php echo $c_operador;?>" maxlength="30"/></td> 
        <td>Nombre de Equipo:</td>
        <td><input type="text" name="c_nomequipo" id="c_nomequipo" value="<?php echo $c_nomequipo;?>" maxlength="30"/></td>
                
    </tr>
    <tr>
    	<td><h4>SOFTWARE</h4></td>
    </tr>
    <tr>
    	<td>IP:</td><td><input type="text" name="c_ip" id="c_ip" value="<?php echo $c_ip;?>" style="width:60px;" maxlength="3"/></td>
        <td>Segmento:</td>
        <td><input type="text" name="c_segmento" id="c_segmento" value="<?php echo $c_segmento;?>" style="width:30px;" maxlength="3"/></td>
        <td>Nivel de Internet:</td>
        <td><input type="text" name="c_nivelinternet" id="c_nivelinternet" value="<?php echo $c_nivelinternet;?>" style="width:30px;" maxlength="1"/></td>
    </tr>

     <tr>
        <td>Ver. Windows:</td>
        <td><input type="text" name="c_windows" id="c_windows" value="<?php echo $c_windows;?>" style="width:60px;" maxlength="30"/></td> 
        <td>Ver. Office:</td>
        <td><input type="text" name="c_office" id="c_office" value="<?php echo $c_office;?>" style="width:60px;" maxlength="30"/></td> 
        <td>Antivirus.:</td>
        <td><input type="text" name="c_antivirus" id="c_antivirus" value="<?php echo $c_antivirus;?>"  style="width:120px;" maxlength="30"/></td>   
    </tr>
   
     <tr>
    	<td><h4>HARDWARE:</h4></td>
    </tr>
    <tr>
    	<td>Procesador:</td>
        <td><input type="text" name="c_procesador" id="c_procesador" value="<?php echo $c_procesador;?>" maxlength="30"/></td>
    	<td>Memoria Ram:</td>
        <td><input type="text" name="c_mem" id="c_mem" value="<?php echo $c_mem;?>" style="width:60px;" maxlength="9"/></td>
        <td>Unidad Ram:</td>
        <td><input type="text" name="c_unidad" id="c_unidad" value="<?php echo $c_unidad;?>" style="width:60px;" maxlength="10"/></td> 
    </tr>
    <tr>
        <td>Capacidad de DD.:</td>
        <td><input type="text" name="c_capacidad" id="c_capacidad" value="<?php echo $c_capacidad;?>" style="width:60px;" maxlength="10"/></td>
        <td>Unidad de DD.:</td>
        <td><input type="text" name="c_unidad_dd" id="c_unidad_dd" value="<?php echo $c_unidad_dd;?>" style="width:60px;" maxlength="10"/></td> 
        <td>Tarjeta Internet:</td>
         <td><input type="checkbox" name="c_tarjetain" id="c_tarjetain" <?php if($c_tarjetain=="1") echo "checked";?>  value="<?php echo $c_tarjetain;?>" /></td>
    </tr>
     <tr>
    	<td colspan="6"><hr /></td>
    </tr>
</table>
    <tr>
    	<td>
        	<input type="button" value="Cancelar"  onclick="location.href='CatalogoDeEquipos.php'"/>
        </td>
        <td>
        	<input type="submit" value="Guardar"  />
        </td>
    </tr>
</table>
</form>
</body>
</html>