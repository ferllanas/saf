<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="latin1">
        <link rel="stylesheet" href="../cheques/css/style.css">         
		<link href="../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Aplicaci&oacute;n de Recursos</title>
        <script src="../javascript_globalfunc/144jquery.min.js"></script>
 <!--      
        <script src="../cheques/javascript/divhide.js"></script>
        <script src="../cheques/javascript/jquery.tmpl.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
        <script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
-->
        <script>
				$(document).ready(function() {
							console.log("Ready");
							$(".botonExcel").click(function(event) {
								location.href = "productos_excel.php";
								//console.log("botonExcel");
								//$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
								//console.log("Ready");
								//$("#FormularioExportacion").submit();
						});
				});
			
        </script>
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body> <!--onLoad="validacierre();"> -->
    <span class="TituloDForma">Productos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
  <!--
  datos_a_enviar
  -->
<form >
<table>
<tr>
	<td><form action="productos_excel.php" method="post" target="_blank" id="FormularioExportacion">
            <p>Exportar a Excel  <img src="../imagenes/export_to_excel.gif" class="botonExcel" /></p>
            <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" value="1" />
        </form>
	</td>
</tr>
<tr>
	<td>
		<table id="Exportar_a_Excel">
        	<thead>         		   
              	<th><p><input type="checkbox" id="applicaAll" checked>id</p></th> 
                <th><p>prodold</p></th> 
                <th><p>clave</p></th>					                                     
                <th><p>nombre</p></th> 
        		<th><p>unidad</p></th>
                <th><p>equivalencia almacen</p></th>
                <th><p>IVA</p></th>
                <th><p>Cuenta Presupuestal</p></th>
                <th><p>Almacen</p></th>
             </thead>
            <tbody >
            <?php
				$command= "select * from compradproductos WHERE  estatus<'9000' ";					
				$getProducts = sqlsrv_query($conexion,$command);
				if ( $getProducts === false)
				{ 
					$resoponsecode="02";
					$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
					//echo $descriptioncode;
				}
				else
				{
					while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
					{		
						//print_r($row);	
						?>
                        <tr><td><?php echo trim($row['prod']);?></td><td><?php echo utf8_encode(trim($row['prodold']));?></td><td align="center"><?php echo trim($row['cve']);?></td><td><?php echo utf8_encode(trim($row['nomprod']));?></td><td align="center"><?php echo trim($row['unidad']);?></td><td align="center"><?php echo trim($row['eqalmacen']);?></td><td align="right"><?php echo trim($row['ivapct']);?></td><td align="right"><?php echo trim($row['ctapresup']);?></td><td align="center"><?php echo trim($row['almacen']);?></td></tr>
                        <?php
						
					}
				}	
			 ?>
            </tbody>
          </table>    
      </td>
</tr>
</table>
</form>

    </div>
    </body>
</html>
