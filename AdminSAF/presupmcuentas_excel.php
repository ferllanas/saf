<?php
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=Balanza_gral.xls");
header("Pragma: no-cache");
header("Expires: 0");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo utf8_encode($_POST['datos_a_enviar']);
?>
<table id="Exportar_a_Excel">
    <thead>         		   
              	<th><p><input type="checkbox" id="applicaAll" checked>ID</p></th> 
                <th><p>Cuenta</p></th> 
                <th><p>Nom. Cuenta</p></th>					                                     
                <th><p>Partida</p></th> 
        		<!--<th><p>Cta. Cargo</p></th>
                <th><p>Cta. Almacen</p></th>
                <th><p>Cta. Credito</p></th>-->
                <th><p>Adefas</p></th>
               	<th align="center">com_car_p</th>
                <th align="center">com_cre_p</th>
                <th align="center">dev_car_p</th>
                <th align="center">dev_cre_p</th>
                <th align="center">dev_car_c</th>
                <th align="center">dev_cre_c</th>
                <th align="center">eje_car_p</th>
                <th align="center">eje_cre_p</th>
                <th align="center">pag_car_p</th>
                <th align="center">pag_cre_p</th>
                <th align="center">pag_car_c</th>
                <th align="center">pag_cre_c</th>
             </thead>
    <tbody >
    <?php
        $command= "select * from presupmcuentas WHERE  estatus<'9000' ";					
        $getProducts = sqlsrv_query($conexion,$command);
        if ( $getProducts === false)
        { 
            $resoponsecode="02";
            $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
            //echo $descriptioncode;
        }
        else
        {
            while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
            {		
                //print_r($row);	
                ?>
                 <tr>
                        <td  align="center"><?php echo trim($row['ID']);?></td>
                        <td align="left"><?php echo utf8_encode(trim($row['CUENTA']));?></td>
                        <td align="right"><?php echo utf8_encode(trim($row['NOMCTA']));?></td>
                        <td align="right"><?php echo utf8_encode(trim($row['partida']));?></td>
                      <!--  <td align="right"><?php echo trim($row['ctacargo']);?></td>
                        <td align="right"><?php echo trim($row['ctaalmacen']);?></td>
                        <td align="right"><?php echo trim($row['ctacredito']);?></td>-->
                        <td align="right"><?php echo trim($row['adefas']);?></td>
                        <td align="right"><?php echo trim($row['com_car_p']);?></td>
                        <td align="right"><?php echo trim($row['com_cre_p']);?></td>
                        <td align="right"><?php echo trim($row['dev_car_p']);?></td>
                        <td align="right"><?php echo trim($row['dev_cre_p']);?></td>
                        <td align="right"><?php echo trim($row['dev_car_c']);?></td>
                        <td align="right"><?php echo trim($row['dev_cre_c']);?></td>
                        <td align="right"><?php echo trim($row['eje_car_p']);?></td>
                        <td align="right"><?php echo trim($row['eje_cre_p']);?></td>
                        <td align="right"><?php echo trim($row['pag_car_p']);?></td>
                        <td align="right"><?php echo trim($row['pag_cre_p']);?></td>
                        <td align="right"><?php echo trim($row['pag_car_c']);?></td>
                        <td align="right"><?php echo trim($row['pag_cre_c']);?></td>
                        </tr>
                <?php
                
            }
        }	
     ?>
    </tbody>
</table>    