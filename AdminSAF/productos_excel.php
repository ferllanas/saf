<?php
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=Balanza_gral.xls");
header("Pragma: no-cache");
header("Expires: 0");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo utf8_encode($_POST['datos_a_enviar']);
?>
<table id="Exportar_a_Excel">
    <thead>         		   
        <th><p><input type="checkbox" id="applicaAll" checked>id</p></th> 
        <th><p>prodold</p></th> 
        <th><p>clave</p></th>					                                     
        <th><p>nombre</p></th> 
        <th><p>unidad</p></th>
        <th><p>equivalencia almacen</p></th>
        <th><p>IVA</p></th>
        <th><p>Cuenta Presupuestal</p></th>
        <th><p>Almacen</p></th>
     </thead>
    <tbody >
    <?php
        $command= "select * from compradproductos WHERE  estatus<'9000' ";					
        $getProducts = sqlsrv_query($conexion,$command);
        if ( $getProducts === false)
        { 
            $resoponsecode="02";
            $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
            //echo $descriptioncode;
        }
        else
        {
            while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
            {		
                //print_r($row);	
                ?>
                <tr><td><?php echo trim($row['prod']);?></td><td><?php echo utf8_encode(trim($row['prodold']));?></td><td align="center"><?php echo trim($row['cve']);?></td><td><?php echo utf8_encode(trim($row['nomprod']));?></td><td align="center"><?php echo trim($row['unidad']);?></td><td align="center"><?php echo trim($row['eqalmacen']);?></td><td align="right"><?php echo trim($row['ivapct']);?></td><td align="right"><?php echo trim($row['ctapresup']);?></td><td align="center"><?php echo trim($row['almacen']);?></td></tr>
                <?php
                
            }
        }	
     ?>
    </tbody>
</table>    