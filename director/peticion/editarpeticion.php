<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$modifica=0;
$titulo="Agregar Servicio";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	$titulo="Editar Servicio";
	$modifica=1;
}
$nombre="";
$asuntos="";
$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM direccionmpendientes where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$asunto=trim($row['asuntos']);
			$descripcion = trim($row['descriparea']);
			$doc=$row['patharea'];
			$estatus=$row['estatus'];
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script type="text/javascript" src="../peticion/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../peticion/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../peticion/javascript/peticion.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>

<!-- Load jQuery build -->
<script type="text/javascript" src="../javascript/tinymce_3.5.7_jquery/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init
({
	 mode : "textareas",
	theme : "advanced",
	        theme_advanced_buttons1 : "bold, italic, underline, bullist,numlist ",
	        theme_advanced_toolbar_location: "top",
	        theme_advanced_toolbar_align: "left"	
})
</script>

<!-- Rutina para cargar imagen previa  -->
<script type="text/javascript" >
$(document).ready(function() { 
		    $('#photoimg1').live('change', function()
			{ 
				$("#imageform1").ajaxForm({
					target: '#preview1'
					}).submit();
				});
			});
</script>
<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Servicios</title>
</head>
<body  style="background:#f8f8f8; overflow:scroll;">
<!-- -------- Titulo -------- -->

<div id="" class="titles">Dirección Ejecutiva</div><br />


<!-- -------- Sección De Proyecto -------- -->
<table width="100%">	 
   
    <tr>
            <td width="121"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /></td>
    </tr> 
 	<tr>
      <td width="121" height="39" class="titles">Asunto</td>
      <td>     
        <input type="text" class="texto8" id="asunto" name="asunto" tabindex="1"  value="<?php echo $asunto;?>" size="60" >              
      </td>
     </tr>    
     <tr>
    	<td class="titles">Descripción:</td>
        <td colspan="8" width="1094"><textarea cols="100" rows="6" name="observa" id="observa" tabindex="2"><?php echo $descripcion;?></textarea>
        <?php if(isset($doc)) echo "<a href='../$doc' target='_blank'><img src='../../imagenes/adjuntar.png' /></a>";?></td>
    </tr> 
    	  <td class="titles" align="middle">Ajuntar Documento</td >
     	<td class="texto8"  valign="bottom" align="left" colspan="3" >
            <form id="imageform1" method="post" enctype="multipart/form-data" action='ajaximage/ajaximage.php'>
                   <input type="file" name="photoimg1" id="photoimg1" tabindex="3" />
           </form>
           <div id='preview1'  >
            </div>
            </td>
    </tr>  
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="<?php if($modifica==0) echo "Enviar"; else echo "Modificar";?>" onclick="javascript: guardapeticion()" tabindex="4"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='peticiones.php'" tabindex="5"/></td>    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>