<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$remplaza=0;
$usuario=$_COOKIE['ID_my_site'];
$partidas=array();
if(isset($_REQUEST['servicios']))
			$partidas=$_POST['servicios'];
//print_r($partidas);
$id=0;
if(isset($_REQUEST['pid']))
{
	$id=$_REQUEST['pid'];
}
$observa="";
if(isset($_POST['pobserva']))
{
	$observa=$_POST['pobserva'];
}
$asunto="";
if(isset($_POST['pasunto']))
{
	$asunto=$_POST['pasunto'];
}
$documento="";
if(isset($_REQUEST['pdocumento']))
{
	$documento=$_REQUEST['pdocumento'];
	$remplaza=1;
}
//$msgerror="Error en Procedimineto";
list($id, $fecha)= fun_dir_A_mpeticion($id,$asunto,
				$observa,
				$usuario); 
if(strlen($id)>0)//Para ver si en realidad se genero un servicio
{
	$msgerror="Peticion Registrada Satisfactoriamente";
}
else
{	
	$fails=true; 
	$msgerror="No pudo generar ningun servicio";
}
	if($remplaza==1)
		$datos=moveImageFromUploadFolder($documento,$id);
	$datos['id']=trim($id);
	$datos['fecha']=trim($fecha);
	$datos['fallo']=$fails;	
	$datos['error']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array

function moveImageFromUploadFolder($path,$id)
{
	global $server,$odbc_name,$username_db ,$password_db;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$path_parts = pathinfo($path);
	$newPath= "../../imagenes/director/docsimage$id.".$path_parts['extension'];
	$newPathDB="../imagenes/director/docsimage$id.".$path_parts['extension'];
	if(!is_dir("../../imagenes/director"))
	{
		if(!mkdir ( "../imagenes/empresa", 0777 , true ))
		{
			$datos['createCarpeta']="No pudo crear la carpeta";
		}
	}
	if(rename($path, $newPath))
	{
		chmod($newPath,0777);	
		if(!$conexion)
			die("No pudo conectarse a la base de datos.");	
		
		$consulta="update direccionmpendientes set patharea=? WHERE id=?";
		$params = array(&$newPathDB,&$id);//Arma parametros de entrada
		$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
		//echo $newPathDB;
		$rs3 = sqlsrv_query($conexion, $consulta, $params);
		if($rs3 === false)
		{
			 $fails= "Error in statement execution.\n";
			 $fails=true;				
			 die($consulta." ".  print_r( sqlsrv_errors(), true));
		}
		else
		{
			$datos['Actualizacion']=$consulta;
		}
		sqlsrv_close($conexion);
	}
	else
	{
		$datos['Actualizacion']="No pudo actualizar";
	}
	return $datos;
}	
function fun_dir_A_mpeticion($id,$asunto,$observa,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_direccion_A_mpeticion(?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$asunto,&$observa,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_direccion_A_mpeticion".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];		
		}
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return array($id,$fecha);
}
					
?>
