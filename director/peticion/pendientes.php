<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	$usuario=$_COOKIE['ID_my_site'];
	$dir='0000';
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<script type="text/javascript">
function filtra()
{
	
	var dir =$('#filtro').val();
	//alert("pendientes.php?dir="+dir);
	location.href="pendientes.php?dir="+dir;
	//alert(dir);
}
</script>
<style>
	tr:nth-child(even) { background: #ddd }
	tr:nth-child(odd) { background: #fff}
</style>
<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="40%">
        	<h1>Pendientes &nbsp;&nbsp;<?php if($_REQUEST['dir'])
				$dir=$_REQUEST['dir'];
				?> </h1>
            <select name="filtro" id="filtro" size="1">
            	<option value="0000">TODOS</option>
            <?php
			
			$command="  select dir,nomdir from  nomemp.dbo.nominamdir where estatus<'90' order by nomdir";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				$i=0;
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					if($dir==$row['dir'])
						echo "<option value='".$row['dir']."' selected>".$row['nomdir']."</option>";
					else
						echo "<option value='".$row['dir']."'>".$row['nomdir']."</option>";
				}
			}
			?>
            </select>
        	
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
        <td>
        	<input type="button" value="Buscar" onclick="filtra();"/>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table style="width:100%;" id="thetable" cellspacing="0" align="center">
  <thead>
    	<tr>
        	<td class="titles" align="center">Fecha .</td>
            <td class="titles" align="center">Hora .</td>
            <td class="titles" align="center">Asunto</td>
            <td class="titles" align="center">Solicitante</td>
            <td class="titles" align="center">Dirección.</td>
            <td class="titles" align="center">Departamento.</td>            
            <td class="titles" align="center">&nbsp;</td>
            
        </tr>
    <?php
    	if($dir=='0000')
			$command="  select a.id,a.asuntos,a.descriparea,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.halta,108) as hora,
a.estatus,b.Nombre,d.nomdir,d.nomdepto from direccionmpendientes a 
left join menumusuarios b on a.usuario=b.usuario 
left join v_nomina_empleadosactivos c on a.usuario=c.numemp collate database_default 
left join nomemp.dbo.nominamdepto d on c.depto=d.depto
where a.estatus=0  order by a.falta,a.halta";
		else
			$command="  select a.id,a.asuntos,a.descriparea,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.halta,108) as hora,
a.estatus,b.Nombre,d.nomdir,d.nomdepto,d.dir from direccionmpendientes a 
left join menumusuarios b on a.usuario=b.usuario 
left join v_nomina_empleadosactivos c on a.usuario=c.numemp collate database_default 
left join nomemp.dbo.nominamdepto d on c.depto=d.depto
where a.estatus=0 and  d.dir=$dir order by a.falta,a.halta";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
			
	?>
    	<tr <?php if($i==0) echo 'class="first"';?>>
        	<td align="center"><?php echo trim($row['fecha']);?></td>
            <td align="center"><?php echo trim($row['hora']);?></td>
            <td align="center"><?php echo trim($row['asuntos']);?></td>
            <td align="center"><?php echo trim($row['Nombre']);?></td>
            <td align="center"><?php echo trim($row['nomdir']);?></td>
            <td align="center"><?php echo trim($row['nomdepto']);?></td>            
            <td align="center"><a class="edit" href="respuesta.php?id=<?php echo trim($row['id']);?>"><img src="../../imagenes/edit.png" title="Contestar" />
            <div class="clr"></div>
            <?php if($row['estatus']==0) echo "Contestar"; else echo "Editar";?></a></td>
          	<td align="center"><a class="edit" href="verrespuesta.php?id=<?php echo trim($row['id']);?>&op=2"><img src="../../imagenes/ver.jpg" title="Ver" />
            <div class="clr"></div>
            Ver</a></td>
        </tr> 
        <?php
			$i++;
			}
		}
		?>
</table>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>