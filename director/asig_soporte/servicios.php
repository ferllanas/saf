<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Servicios &nbsp;&nbsp; </h1>
            &nbsp;<input class="btn" type="button" value=" Registar Solicitud de Servicios "  onclick="javascript: location.href='editarcatservicios.php'"/>
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table style="width:100%;" id="thetable" cellspacing="0" align="center">
  <thead>
    	<tr>
        	<td class="titles" align="center">Folio</td>
            <td class="titles" align="center">Solicitante</td>
            <td class="titles" align="center">Falla</td>
            <td class="titles" align="center">Fecha</td>
            <td class="titles" align="center">Hora</td>
            <td class="titles" align="center">Situaci&oacute;n</td>
            <td class="titles" align="center">&nbsp;</td>
            <td class="titles" align="center">&nbsp;</td>
            <td class="titles" align="center">&nbsp;</td>
        </tr>
    <?php
    	$command="select a.id,a.numemp,rtrim(ltrim(b.nombre))+' '+rtrim(ltrim(b.appat))+' '+rtrim(ltrim(b.apmat)) as nombre,a.numpatrimonio,a.estatus,a.observa,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.hora,108) as hora from serviciomservicios a left join nomemp.dbo.nominadempleados b on a.numemp=b.numemp collate database_default where a.estatus<90 order by a.id,fecha,hora";
		//	echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
			
	?>
    	<tr <?php if($i==0) echo 'class="first"';?>>
        	<td align="center"><?php echo $row['id'];?></td>
            <td align="center"><?php echo trim($row['nombre']);?></td>
            <td align="center"><?php echo trim($row['observa']);?></td>
            <td align="center"><?php echo trim($row['fecha']);?></td>
            <td align="center"><?php echo trim($row['hora']);?></td>
            
            <td align="center"><?php if($row['estatus']==0) echo "Si Asignar"; else echo "Asignado";?></td>
            <td align="center"><a class="view" href="verservicios.php?id=<?php echo trim($row['id']);?>"><img src="../../imagenes/view.png" title="Ver" />
            <div class="clr"></div>
            Ver</a></td>
            <td align="center"><a class="edit" href="asignarservicios.php?id=<?php echo trim($row['id']);?>"><img src="../../imagenes/asigna.png" title="Editar" />
            <div class="clr"></div>
            <?php if($row['estatus']==0) echo "Asignar"; else echo "Reasignar";?></a></td>
            <td align="center"><a class="delete" href="eliminaservicios.php?id=<?php echo trim($row['id']);?>" onclick="return confirm('Esta seguro de eliminar el Servicio <?php echo $row['descripcion'];?> ?')"><img src="../../imagenes/delete.png" title="Eliminar" />
            <div class="clr"></div>
            Eliminar</a></td>
        </tr>
    <?php
			
				
			}
		}
	?>
</table>


</body>
</html>