<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$idant="0";
$pre="";
$titulo="Validando la Solicitud";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
}
$nombre="";
$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="
select a.id,a.numemp,a.numpatrimonio,a.observa,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.hora,108) as hora,
b.numemp as tec,CONVERT(varchar(10),b.fasig,103) as fecasig,CONVERT(varchar(10),b.hasig,108) as hasig,
rtrim(ltrim(c.nombre))+' '+rtrim(ltrim(c.appat))+' '+rtrim(ltrim(c.apmat)) as nomemp,
rtrim(ltrim(d.nombre))+' '+rtrim(ltrim(d.appat))+' '+rtrim(ltrim(d.apmat)) as nomtec,
CONVERT(varchar(10),f.falta,103) as fecdiag,CONVERT(varchar(10),f.hora,108) as hdiag,f.observa as diag
from serviciomservicios a 
left join servicioasigna b on a.id=b.idservicio and b.estatus<90
left join nomemp.dbo.nominadempleados c on a.numemp=c.numemp collate database_default
left join nomemp.dbo.nominadempleados d on b.numemp=d.numemp collate database_default
left join serviciomdiagnostico f on a.id=f.idservicio 
where a.id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$id=trim($row['id']);
			
			$nomemp = trim($row['nomemp']);
			$nomtec = trim($row['nomtec']);
			$fecha = trim($row['fecha']);
			$hora = trim($row['hora']);
			$fecasig = trim($row['fecasig']);
			$hasig = trim($row['hasig']);
			$fecdiag = trim($row['fecdiag']);
			$hdiag = trim($row['hdiag']);
			$detalle = trim($row['observa']);
			$diag = trim($row['diag']);
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script type="text/javascript" src="../servicios/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../servicios/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../asig_soporte/javascript/asigservicios.js"></script>


<!-- Load jQuery build -->
<script type="text/javascript" src="../javascript/tinymce_3.5.7_jquery/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init
({
	 mode : "textareas",
	theme : "advanced",
	        theme_advanced_buttons1 : "bold, italic, underline, bullist,numlist ",
	        theme_advanced_toolbar_location: "top",
	        theme_advanced_toolbar_align: "left"	
})
</script>

<!-- Rutina para cargar imagen previa  -->

<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->

<div id="" class="titles"><?php echo $titulo." ".$id ; ?></div><br />


<!-- -------- Sección De Proyecto -------- -->
<table width="100%">	 
   
    <tr>
            <td width="121"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /><input name="idant" id="idant" type="hidden" value="<?php echo $idant;?>" /><input name="tecant" id="tecant" type="hidden" value="<?php echo $tecant;?>" /></td>
    </tr> 
   
 <tr>
      <td width="121" height="39" class="titles">Fecha de Solicitud: </td> 
      <td class="titles"><input style="width:250px" value="<?php echo $fecha." ".$hora;?>" readonly="readonly" /></td>
     
  </tr>  
     <tr>
    	<td class="titles">Solicitante:</td>
        <td colspan="8" width="1094"><input type="text" tabindex="4" style="width:350px" name="sol" id="sol" value="<?php echo $nomemp;?>" readonly="readonly"/> </td>
    </tr>     
<tr>
      <td width="121" height="39" class="titles">Tecnico: </td> 
      <td class="titles"><input style="width:350px" value="<?php echo $nomtec;?>" readonly="readonly" /></td>
     
  </tr>  
     <tr>
      <td width="121" height="39" class="titles">Fecha de Asignacion: </td> 
      <td class="titles"><input style="width:250px" value="<?php echo $fecasig." ".$hasig;?>" readonly="readonly" /></td>
     
  </tr> 
     
    <tr>
    <td class="titles">Posible Falla:</td>
    <?php
		if ($conexion_srv)
		{
			$descrip="";
			$consulta = "select a.id,b.Descripcion from serviciodservicios a left join catservicios b on a.idcatservicio=b.id where idservicio=$id";
			$R = sqlsrv_query( $conexion_srv,$consulta);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id= $row['id'];
				$descrip.= $row['Descripcion']." - ";				
			}
			echo "<td class='titles'>$descrip </td>";
		}
	?>					
    </tr>    
    
      
    </tr>
    <tr>
      <td width="121" height="39" class="titles">Fecha de Diagnostico: </td> 
      <td class="titles"><input style="width:250px" value="<?php echo $fecdiag." ".$hdiag;?>" readonly="readonly" /></td>
     
  </tr> 
    <tr>
    <td class="titles">Acciones Realizadas:</td>
    <?php
		if ($conexion_srv)
		{
			$descrip="";
			$consulta = "select a.id,b.descripcion from servicioddiagnostico a left join catdiagnostico b on a.idcatdiagnostico=b.id 
left join serviciomdiagnostico c on a.iddiagnostico=c.id where c.idservicio=$id and a.estatus<90";
			//echo $consulta;
			$R = sqlsrv_query( $conexion_srv,$consulta);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id= $row['id'];
				$descrip.= $row['descripcion']." - ";				
			}
			echo "<td class='titles'>$descrip </td>";
		}
	?>					
    </tr>    
     <tr>
    	<td class="titles">Comentarios:</td>
        <td colspan="8" width="1094"><textarea tabindex="2" cols="100" rows="2" name="observa_t" id="observa_t" ><?php echo $diag;?></textarea></td>
    </tr> 
     <tr>
    	<td class="titles">Calificaci&oacute;n del Servicio:</td>
        <td colspan="8" width="1094" class="titles">&nbsp;&nbsp;10&nbsp;<input type="radio" name="califica" id="califica" value="10"/>&nbsp;&nbsp;9&nbsp;<input type="radio" name="califica" id="califica" value="9"/>&nbsp;&nbsp;8&nbsp;<input type="radio" name="califica" id="califica" value="8"/>&nbsp;&nbsp;7&nbsp;<input type="radio" name="califica" id="califica" value="7"/>&nbsp;&nbsp;6&nbsp;<input type="radio" name="califica" id="califica" value="6"/> </td>
    </tr>  
    <tr>
    	<td class="titles">Observaciones:</td>
        <td colspan="8" width="1094"><textarea cols="100" tabindex="6" rows="2" name="observa" id="observa" ></textarea></td> 
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Finalizar" onclick="javascript: finalizaservicios()"/></td>
        <td width="22%" align="center"><input class="btn-suprim" type="button" value="No Realizado" onclick="javascript: sinservicios()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='../servicios/servicios.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>