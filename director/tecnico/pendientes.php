<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	$usuario=$_COOKIE['ID_my_site'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Pendientes &nbsp;&nbsp; </h1>
           <!-- &nbsp;<input class="btn" type="button" value=" Registar Solicitud de Servicios "  onclick="javascript: location.href='editarservicios.php'"/>
    		<div class="clr"></div>
            <div id="" class="line"></div>-->
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table style="width:100%;" id="thetable" cellspacing="0" align="center">
  <thead>
    	<tr>
        	<td class="titles" align="center">Folio</td>
            <td class="titles" align="center">Solicitante</td>
            <td class="titles" align="center">Falla</td>
            <td class="titles" align="center">Fecha Sol.</td>
            <td class="titles" align="center">Hora Sol.</td>
            <td class="titles" align="center">Fecha Asig.</td>
            <td class="titles" align="center">Hora Asig.</td>            
            <td class="titles" align="center">&nbsp;</td>
            
        </tr>
    <?php
    	$command="select a.id,a.numemp,b.nomemp as nombre,
a.numpatrimonio,a.estatus,a.observa,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.hora,108) as hora,CONVERT(varchar(10),c.fasig,103) as fechaa,CONVERT(varchar(10),c.hasig,108) as horaa,c.id as idasigna,c.numemp as tec 
from serviciomservicios a 
left join v_nomina_nomiv_nomhon6_nombres_activos_depto_dir b on a.numemp=b.numemp collate database_default 
left join servicioasigna c on a.id=c.idservicio and c.estatus<90
where a.estatus>0 and a.estatus<=20 and c.numemp=$usuario order by a.id,fecha,hora";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
			
	?>
    	<tr <?php if($i==0) echo 'class="first"';?>>
        	<td align="center"><?php echo $row['id'];?></td>
            <td align="center"><?php echo trim($row['nombre']);?></td>
            <td align="center"><?php echo trim($row['observa']);?></td>
            <td align="center"><?php echo trim($row['fecha']);?></td>
            <td align="center"><?php echo trim($row['hora']);?></td>
            <td align="center"><?php echo trim($row['fechaa']);?></td>
            <td align="center"><?php echo trim($row['horaa']);?></td>            
           <?php 
		   	if($row['estatus']==10)
           		{?> <td align="center"><a class="edit" href="../tecnico/diagnosticoservicios.php?id=<?php echo trim($row['id']);?>&idasigna= <?php echo $row['idasigna'];?>"><img src="../../imagenes/servicio.png" title="Diagnostico" />
            <div class="clr"></div>
            Diagnostico</a></td>
           <?php
				}
				else
				{?>
					<td align="center"><a class="edit" href="../tecnico/crear_pdf.php?id=<?php echo trim($row['id']);?>&idasigna= <?php echo $row['idasigna'];?>"><img src="../../imagenes/impresora.png" title="Imprimir Formato" />
            <div class="clr"></div>
            Impresi&oacute;n</a></td>
        </tr> <?php
				}
				?>
    <?php
			
				
			}
		}
	?>
</table>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>