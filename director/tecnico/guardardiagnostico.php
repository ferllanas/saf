<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');


$usuario=$_COOKIE['ID_my_site'];
$partidas=array();
if(isset($_REQUEST['servicios']))
			$partidas=$_POST['servicios'];
//print_r($partidas);
$idservicio="";
if(isset($_REQUEST['pid']))
{
	$idservicio=$_REQUEST['pid'];
}
$idasig="";
if(isset($_REQUEST['pidasig']))
{
	$idasig=$_REQUEST['pidasig'];
}
$observa="";
if(isset($_POST['pobserva']))
{
	$observa=$_POST['pobserva'];
}
$hora="";
if(isset($_POST['phora']))
{
	$hora=$_POST['phora'];
}
//$msgerror="Error en Procedimineto";
list($id, $fecha)= fun_serv_A_mservicio("0",$hora,
				$observa,
				$idservicio,
				$idasig,
				$usuario); 
if(strlen($id)>0)//Para ver si en realidad se genero un servicio
{
	//echo "si paso ".count($partidas);
	for( $i=1 ; $i  < count($partidas) ; $i++ )
	{
		$fails = func_servicio_A_dservicios('0',
					$id,
					$partidas[$i],
					0,
					$usuario);
	}
	
	$msgerror="Diagnostico Registrado Satisfactoriamente";
}
else
{	
	$fails=true; 
	$msgerror="No pudo generar ningun Diagnostico";
}
	$datos['fecha']=trim($fecha);
	$datos['fallo']=$fails;	
	$datos['error']=$msgerror;
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	
function fun_serv_A_mservicio($id,$hora,$observa,$idservicio,$idasig,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_servicio_A_mdiagnostico(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$hora,&$observa,&$idservicio,&$idasig,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_servicio_A_mdiagnostico".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];		
		}
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return array($id,$fecha);
}


function func_servicio_A_dservicios($id,
								$iddiagnostico,
								$idcatdiagnostico, 
								$estatus,
								$usuario)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion	
	$params = $tsql_callSP ="{call sp_servicio_A_ddiagnostico(?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$iddiagnostico,&$idcatdiagnostico, &$estatus,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_servicio_A_ddiagnostico". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
						
?>
