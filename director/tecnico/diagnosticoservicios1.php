<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$titulo="Agregar Servicio";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	$titulo="Editar Servicio";
}
$nombre="";

$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM catservicios where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$descripcion = trim($row['Descripcion']);
			$estatus=$row['estatus'];
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../css/biopop.css">
<script type="text/javascript" src="../servicios/ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="../servicios/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../servicios/javascript/servicios.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>

<!-- Load jQuery build -->
<script type="text/javascript" src="../javascript/tinymce_3.5.7_jquery/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init
({
	 mode : "textareas",
	theme : "advanced",
	        theme_advanced_buttons1 : "bold, italic, underline, bullist,numlist ",
	        theme_advanced_toolbar_location: "top",
	        theme_advanced_toolbar_align: "left"	
})
</script>

<!-- Rutina para cargar imagen previa  -->

<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->

<div id="" class="titles">Alta de Solicitud de Servicio</div><br />


<!-- -------- Sección De Proyecto -------- -->
<table width="100%">	 
   
    <tr>
            <td width="121"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /></td>
    </tr> 
   
   
<tr>
    	  <td width="121" height="39" class="titles">Nombre del Usuario  
      <div align="left" style="z-index:3; position:absolute; width:311px; top: 74px; left: 174px;">     
        <input name="provname1" type="text" class="texto8" id="provname1" style="width:100%;" tabindex="4"  onKeyUp="searchProveedor1(this);" value="<?php echo $nombre;?>" size="60" autocomplete="off">        
        <div id="search_suggestProv" style="z-index:4;" > </div>
		</div>
		</div>  
<input class="texto8" tabindex="2" type="hidden" id="numemp" name="numemp" size="10"></td>
     </tr>    
      <tr>
    	<td class="titles"># Patrimonio:</td>
        <td colspan="8" width="1094"> <div align="left" style="z-index:1; position:absolute; width:495px; top: 114px; left: 175px;">     
         <input name="pat" type="text" id="pat" onblur="buscadescripcion()" maxlength="6"/><input type="text" style="width:250px" name="dpat" id="dpat"/>      
        <div id="search_suggestpat" style="z-index:2;" > </div>
		</div>
		</div> 
       </td>
    </tr>  
    <tr>
    <td class="titles">Tipo de Servicio:</td>
    <?php
		if ($conexion_srv)
		{
			$consulta = "select * from catservicios where estatus=0";
			$R = sqlsrv_query( $conexion_srv,$consulta);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id= $row['id'];
				$descrip= $row['Descripcion'];
				echo "<td class='titles'>$descrip <input type='checkbox' name='$id' id='$id' value=$id /></td>";
				
			}
		}
	?>					
    </tr>    
      <tr>
    	<td class="titles">Observaciones:</td>
        <td colspan="8" width="1094"><textarea cols="100" rows="3" name="observa" id="observa"></textarea></td>
    </tr>   
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Guardar" onclick="javascript: guardaservicios()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='servicios.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>