<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$modifica=0;
$titulo="Agregar Servicio";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id=$_REQUEST['id'];
	$titulo="Editar Servicio";
	$modifica=1;
}
$nombre="";
$asuntos="";
$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM direccionmpendientes where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$asunto=trim($row['asuntos']);
			$descripcion = trim($row['descriparea']);
			$doc=$row['patharea'];
			$estatus=$row['estatus'];
		}
	}
}
	

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

  <script type="text/javascript" src="../../javascript_globalfunc/jquery-timepicker-master/jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="../../javascript_globalfunc/jquery-timepicker-master/jquery.timepicker.css" />

  <script type="text/javascript" src="../../javascript_globalfunc/jquery-timepicker-master/lib/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="../../javascript_globalfunc/jquery-timepicker-master/lib/bootstrap-datepicker.css" />

  <script type="text/javascript" src="../../javascript_globalfunc/jquery-timepicker-master/lib/site.js"></script>
  <link rel="stylesheet" type="text/css" href="../../javascript_globalfunc/jquery-timepicker-master/lib/site.css" />
<!--<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="../peticion/ajaximage/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../peticion/javascript/peticion.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>-->

<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<link rel="stylesheet" type="text/css" media="all" href="../../javascript_globalfunc/jquery-timepicker-master/jquery.timepicker.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<!--<script type="text/javascript" src="../../javascript_globalfunc/jquery-timepicker-master/jquery.timepicker.min.js"></script>
 Rutina para cargar imagen previa  -->
<script type="text/javascript" >
$(document).ready(function() { 
						   
	$('#hora').timepicker({ 'timeFormat': 'h:i A' });
	
	$('#enviaActividad').click(function(){
										
		$datos={ 
					fecha: $('#fecha').val(), 
					descripcion: $('#descripcion').val() ,
					hora: $('#hora').val() 
			};
			
		$.ajax({
					type: "POST",
					url: "registrarActividad.php",
					data: $datos,
					dataType: 'json',
					success: function ($request) 
					{
						console.log($request); 
						if ( $request.error == '1' )
						{
							alert($request.mensaje);
							 location.href='agregarActividad.php';
						}
						else
						{
							 alert($request.mensaje);
							// location.href='Actividad.php';
						}
					}
		});
	});
});
</script>
<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Servicios</title>
</head>
<body  style="background:#f8f8f8; overflow:scroll;">
<!-- -------- Titulo -------- -->

<div id="" class="titles">Direcci&oacute;n Ejecutiva</div><br />


<!-- -------- Secci�n De Proyecto -------- -->
<table width="100%">	 
   
    <tr>
            <td width="121"><input name="id" id="id" type="hidden" value="<?php echo $id;?>" /></td>
    </tr> 
 	  <tr>
            <td class="titles">Fecha:</td>
       		<td colspan="8" width="1094"><input name="fecha" type="text" size="7" id="fecha" value="<?php echo date('d/m/Y');?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:$('optfecha').checked=true">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecha",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger1",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>
          <!--  <input id="timeformatExample2" class="time ui-timepicker-input" type="text" autocomplete="off"></input>
            <input name="hora" type="text" size="7" id="hora" value="" class="texto8" maxlength="10"  style="width:70">--></td>
    </tr> 
     	<tr>
    		<td class="titles">Descripci&oacute;n:</td>
        	<td colspan="8" width="1094"><textarea cols="100" rows="6" name="descripcion" id="descripcion" tabindex="2"><?php echo $descripcion;?></textarea>
        <?php if(isset($doc)) echo "<a href='../$doc' target='_blank'><img src='../../imagenes/adjuntar.png' /></a>";?></td>
 		</tr> 
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input id="enviaActividad" class="btn" type="button" value="<?php if($modifica==0) echo "Enviar"; else echo "Modificar";?>"  tabindex="4"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onClick="javascript:location.href='Actividad.php'" tabindex="5"/></td>    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>