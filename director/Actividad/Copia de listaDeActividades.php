<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	$usuario=$_COOKIE['ID_my_site'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/jquery.tmpl.js"></script>
<style>
	tr:nth-child(even) { background: #ddd }
	tr:nth-child(odd) { background: #fff}
</style>
<script language="javascript" >
function buscaActividadesPorDireccion()
{
	var direccionS=$('#departamento').find(":selected").val();
	var fechas=$('#fecha').val();
	
	var datas={direccion:direccionS, fecha:fechas};
	console.log(datas);
	$.ajax({ type: 'post',
					   data: datas,
					   url: 'buscarActividadesXDia.php',
					   dataType: 'json',
					   success: function(resp)
						{
							console.log(resp);
							$('#datos').empty();
							$('#tmpl_proveedor').tmpl(resp).appendTo('#datos'); 
						}
					
				});  // Json es una muy buena opcion    
}
</script>
<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
		<td style=" vertical-align:top;
		border:#000 1px solid;
		width: 250px;
		min-width:250px;
		font-family: 'Open Sans', sans-serif; 
		font-weight:600;
		font-size:10px; 
		text-transform:capitalize; 
		background:#fff;"><ul>{{html descripcion}}</ul></td>
</script>   
<title>Pendientes Direcci&oacute;n</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td colspan="2" id="titulo" width="100%">
        	<h1>Asuntos Con Director&nbsp;&nbsp; </h1>
         </td>
    </tr>
    <tr>
    	<td>
           Direcci&oacute;n: <select id="departamento" name="departamento" onchange="buscaActividadesPorDireccion()" >
            <?php 
				$command="  SELECT depto, nomdepto FROM [nomemp].[dbo].nominamdepto WHERE (depto % 100)=0";
				//echo $command;
				$getProducts = sqlsrv_query( $conexion_srv,$command);
				
				if ( $getProducts === false)
				{ 
					$resoponsecode="02";
					print_r( sqlsrv_errors()) ; 
				}
				else
				{
					$i=0;
					$resoponsecode="Cantidad rows=".count($getProducts);
					while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
					{
			?>
            		<option  value="<?php echo trim($row['depto']);?>"><?php echo utf8_encode(trim($row['nomdepto']));?></option>
            <?php
					}
				}
			?>
            		<option  value="*" selected="selected">TODAS LAS DIRECCIONES</option>
            </select>
           <?php 	$diasatras=1;
		   			//echo date('w',time() - 60 * 60 * 24);
					if( date('w',time() - 60 * 60 * 24)==0)
						$diasatras=3;
					
					//echo $diasatras;
			?>
            <input name="fecha" type="text" size="7" id="fecha" value="<?php echo date('d/m/Y');?>" class="texto8" maxlength="10"  style="width:80px" onchange="buscaActividadesPorDireccion()">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecha",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
    		
            <div id="" class="line"></div>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<div class="divdirtable">
<table class="dirtable">
  <thead>
  	<?php 
		$dirs=array();
		$i=0;
		$command="SELECT depto, nomdepto FROM nomemp.dbo.nominamdepto WHERE (depto % 100)=0 ORDER BY depto ";
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if($getProducts!==false)
		{
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				if(trim($row['depto'])!='1000' && trim($row['depto'])!='7000' && trim($row['depto'])!='9000' && trim($row['depto'])!='8500'){
					$dirs[$i]['depto']=trim($row['depto']);
					$dirs[$i]['nomdepto']=trim($row['nomdepto']);	
					$i++;
				}
			}
		}
		//print_r($dirs);
		//die("Asta aqui");
		?>
		
    	<tr><?php for($i=0;$i<count($dirs);$i++)
		{
		?>
        	<th ><?php echo $dirs[$i]['nomdepto'];?></th>
        <?php
		}
		?>
        </tr>
   </thead>
       <tbody >
       <tr id="datos">
    <?php
		
		
		
		
		$annio= date('Y');
			$mes= date('m');
			$day= date('d');
		 for($i=0;$i<count($dirs);$i++)
		 {
			?>
            <td>
            <ul>
            <?php
    	$command="select a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre, 
  a.depto, c.dir, c.nomdir 
  from direccionactividades a 
  left join menumusuarios b on a.usuario=b.usuario 
  LEFT JOIN nomemp.dbo.nominamdepto c ON  c.depto COLLATE DATABASE_DEFAULT=a.depto COLLATE DATABASE_DEFAULT 
  where  a.estatus<20 AND YEAR(a.factividad)='$annio' AND  MONTH(a.factividad)='$mes' AND  DAY(a.factividad)='$day' AND c.dir='".$dirs[$i]['depto']."' order by a.depto, fecha, hactividad";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			print_r( sqlsrv_errors()) ; 
		}
		else	
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				echo "<li>".trim($row['descripcion'])."</li>";
			
		?>
        </ul>
        </td>
        <?php
		 }
	?>
    </tr>
    </tbody>
</table>
</div>
</body>
</html>