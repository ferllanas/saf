<?php
 if (version_compare(PHP_VERSION, "5.1.0", ">="))
 	date_default_timezone_set("America/Mexico_City");
 	
	require_once("../../dompdf-master/dompdf_config.inc.php");
	require_once("../../connections/dbconexion.php");
	include("../../Administracion/globalfuncions.php");
	
	
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	

	$usuario=$_COOKIE['ID_my_site'];
	
	$municipio="";
	if(isset($_REQUEST['municipio']))
		$municipio=$_REQUEST['municipio'];
		
	$nommunicipio="";
	
	$annio= date('Y');
	$mes= date('m');
	$day= date('d');	
	$fecha="";
	if(isset($_POST['fecha']))
	{
		$fecha=$_POST['fecha'];
		list($day, $mes, $annio)=explode("/",$fecha);
	}

$municipios=array();
$command="SELECT [scat] ,[descripcion]  FROM [fomedbe].[dbo].[catsmscat] where cat=0310  AND cvesit<21809000 AND cvesit<21809000 AND scat not in(03100031, 03100024, 03100077,03100036, 03100100, 03100088) ORDER BY descripcion  ASC "; 
$getProducts = sqlsrv_query( $conexion_srv,$command);
if ( $getProducts === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
}
else
{
	$i=0;
	$resoponsecode="Cantidad rows=".count($getProducts);
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
	{
		//print_r($row);
		$municipios[$i]['numero'] = $i + 1;
		$municipios[$i]['scat'] = utf8_decode(trim($row['scat']));
		$municipios[$i]['nombre'] = utf8_decode(trim($row['descripcion']));
		
		if($municipio==$municipios[$i]['scat'])
		{
			$nummunicipio=$municipios[$i]['numero'];
			$nommunicipio = utf8_decode(trim($row['descripcion']));
		}
		$i++;
	}
}

/*$command="SELECT [scat] ,[descripcion]  FROM [fomedbe].[dbo].[catsmscat] where scat='$municipio' AND cvesit<21809000 ORDER BY descripcion ASC "; 
$getProducts = sqlsrv_query( $conexion_srv,$command);
if ( $getProducts === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
}
else
{
	
	$resoponsecode="Cantidad rows=".count($getProducts);
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
	{
		$nommunicipio = utf8_decode(trim($row['descripcion']));
	}
}
*/		

$dompdf = new DOMPDF();
$dompdf->set_paper("letter","portrait");



$string ='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<style>
body 
{
	font-size:9;
	margin:  1in 0.10in 0.50in 0.10in;
}
		
.dirtable{ width:100%;
border: 1px solid black;
border-collapse: collapse;
vertical-align:text-top;
}

.dirtable td,th{
	border: 1px solid black;
	border-collapse: collapse;
}

.dirtable thead th{
	font-size:14;
	vertical-align:text-top;
}

.dirtable tbody th{
	font-size:14;
	vertical-align:text-top;
}

.tdgris{
	background-color: gray;
}
		
#header,
#footer {
  position: fixed;
  left: 0;
  right: 0;
  color: #aaa;
  font-size: 0.9em;
}

#header {
  top: 0;
	border-bottom: 0.1pt solid #aaa;
}

#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}

.page-number {
  text-align: right;
}

.page-number:before {
  content:  counter(page);
}

hr {
  page-break-after: always;
  border: 0;
}
</style>

</head>

<body style="background:#f8f8f8;">

<div id="header" style="width:100%;">
  <table style="width:100%;">
    <tr>
      <td style="text-align: left; width:20%;"><img src="../../'.$imagenPDFSecundaria.'" width="120px" /></td>
	  <td style="text-align: center; width:80%;"><h1>'.$nummunicipio.".".$nommunicipio.'</h1></td>
    </tr>
  </table>
</div>

<div id="footer">
  <div class="page-number"></div>
</div>
';

		$dirs=array();
		$i=0;
		$command="SELECT depto, nomdepto FROM nomemp.dbo.nominamdepto WHERE (depto % 100)=0 ORDER BY nomdepto ";
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if($getProducts!==false)
		{
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
				if(trim($row['depto'])!='1000' && trim($row['depto'])!='7000' && trim($row['depto'])!='9000' && trim($row['depto'])!='8500')
				{
					$dirs[$i]['depto']=trim($row['depto']);
					//echo trim($row['nomdepto'])."<br>";
			
					if(trim($row['nomdepto'])=='DIRECCION EJECUTIVA ADJUNTA')
						$dirs[$i]['nomdepto']='DIRECCI&Oacute;N EJECUTIVA ADJUNTA';	
					else
						if(trim($row['nomdepto'])=='SECRETARIA TECNICA')
							$dirs[$i]['nomdepto']='SECRETAR&Iacute;A T&Eacute;CNICA';	
						else
							if(trim($row['nomdepto'])=='UNIDAD DE CONTRALORIA INTERNA')
								$dirs[$i]['nomdepto']='CONTRALOR&Iacute;A INTERNA';	
							else
								if(trim($row['nomdepto'])=='DIRECCION DE ASUNTOS JURIDICOS')
									$dirs[$i]['nomdepto']='JUR&Iacute;DICO';	
								else					
									if(trim($row['nomdepto'])=='DIRECCION DE ADMINISTRACION Y FINANZAS')
									{
										$dirs[$i]['nomdepto']='ADMINISTRACI&Oacute;N';	
										//echo "a caray".$dirs[$i]['nomdepto']."**<br>";
									}
									else
										if(trim($row['nomdepto'])=='DIRECCION DE ASIGNACIONES Y CONTROL DOCUMENTAL')
											$dirs[$i]['nomdepto']='ASIGNACIONES';	
										else
											if(trim($row['nomdepto'])=='DIRECCION DE INFRAESTRUCTURA SOCIAL')
												$dirs[$i]['nomdepto']='INFRAESTRUCTURA SOC.';	
											else
												if(trim($row['nomdepto'])=='DIRECCION DE REGULARIZACION DE LA TENENCIA DE LA TIERRA')
													$dirs[$i]['nomdepto']='REGULARIZACI&Oacute;N';	
												else
												{
													//echo "!".trim($row['nomdepto'])."==DIRECCION PROMOCION Y GESTION SOCIAL";
													if(trim($row['depto'])=='6000')
														$dirs[$i]['nomdepto']='PROMOCI&Oacute;N Y GESTI&Oacute;N';	
													else
														if(trim($row['nomdepto'])=='DIRECCION DE DELEGACIONES')
															$dirs[$i]['nomdepto']='DELEGACIONES';	
														else
															$dirs[$i]['nomdepto']=trim($row['nomdepto']);	
												}
							$dirs[$i]['fomerrey']="";
							$dirs[$i]['vivienda']="";
							//echo "a caray".$dirs[$i]['nomdepto']."*??<br>";
							$i++;
				}
			}
		}
	
		
		 for($i=0;$i<count($dirs);$i++)
		 {
			 $command="SELECT TOP 1 a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre, 
a.depto, c.dir, c.nomdir , a.dependencia FROM direccionactividades a 
LEFT JOIN menumusuarios b on a.usuario=b.usuario 
LEFT JOIN nomemp.dbo.nominamdepto c ON  c.depto COLLATE DATABASE_DEFAULT=a.depto COLLATE DATABASE_DEFAULT 
where a.municipio='$municipio' AND a.dependencia=0 AND c.dir='".$dirs[$i]['depto']."' ORDER BY idactividad DESC

SELECT TOP 1 a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre, 
a.depto, c.dir, c.nomdir , a.dependencia FROM direccionactividades a 
LEFT JOIN menumusuarios b on a.usuario=b.usuario 
LEFT JOIN nomemp.dbo.nominamdepto c ON  c.depto COLLATE DATABASE_DEFAULT=a.depto COLLATE DATABASE_DEFAULT 
where a.municipio='$municipio' AND a.dependencia=1 AND c.dir='".$dirs[$i]['depto']."' ORDER BY idactividad DESC";

		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			print_r( sqlsrv_errors()) ; 
		}
		else	
		{
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		   {
			  if($row['dependencia']==0)
				$dirs[$i]['fomerrey'] .= $row['descripcion'];
			else
				$dirs[$i]['vivienda'] .= $row['descripcion'];
		   }
			   
			$next_result = sqlsrv_next_result($getProducts);
			if( $next_result ) 
			{
			   while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			   {
				  if($row['dependencia']==0)
					$dirs[$i]['fomerrey'] .= $row['descripcion'] ;
				else
					$dirs[$i]['vivienda'] .= $row['descripcion'];
			   }
			} 
			elseif( is_null($next_result)) 
				{
					 echo "No more results.<br />";
				} 
				else 
				{
					 die(print_r(sqlsrv_errors(), true));
				}
			
		}
	}
	

$string.='<table class="dirtable">
<thead>
	<tr><th width="20%">DIRECCION</th><th width="40%"><img src="../../$imagenPDFPrincipal" height="50px" /></th><th width="40%"><img src="../../imagenes/vivienda.jpg"  height="50px" /></th></tr>
</thead>';
  for($i=0;$i<count( $dirs);$i++)
  {
	

  	$string.='<tr>
	  <th width="20%">'.$dirs[$i]['nomdepto'].'</th>
        <td width="40%">';
			if(isset($dirs[$i]['fomerrey']))
				if(strlen($dirs[$i]['fomerrey'])>0) 
					$string.=''.trim($dirs[$i]['fomerrey']);
		$string.='<br>&nbsp;<br></td>';
		
		if($dirs[$i]['depto']=='3000' || $dirs[$i]['depto']=='5000' || $dirs[$i]['depto']=='8000')
        	$string.='<td width="40%" class="tdgris">';
		else
			$string.='<td width="40%">';
			
		if(isset($dirs[$i]['vivienda']))
			if(strlen($dirs[$i]['vivienda'])>0) 
				 $string.=''.trim($dirs[$i]['vivienda']);
		 $string.='</td>
    </tr>';

  }
 $string.='
</table>
</body>
</html>';

//echo $string;

$dompdf->load_html($string);
$dompdf->render();
$dompdf->stream($nommunicipio.".pdf");
//$pdf = $dompdf->output(); 
//file_put_contents($nommunicipio.".pdf", $pdf);
?>