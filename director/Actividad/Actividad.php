<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	$usuario=$_COOKIE['ID_my_site'];
	$depto=$_COOKIE['depto'];
	
	$annio=date('Y');
	$mes=date('m');
	$day=date('d');
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<style>
	tr:nth-child(even) { background: #ddd }
	tr:nth-child(odd) { background: #fff}
</style>
<title>Pendientes Direcci&oacute;n</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Asuntos Con Director&nbsp;&nbsp; </h1>
            &nbsp;&nbsp;&nbsp;<input class="btn" type="button" value=" Agregar "  onclick="javascript: location.href='agregarActividad.php'"/>
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table style="width:90%;" id="thetable" cellspacing="0" align="center">
  <thead>
    	<tr>
        	<!--<td class="titles" align="center">Folio</td>-->
            <td class="titles" align="center">Descripcion</td>
            <td class="titles" align="center">Fecha</td>
            <td class="titles" align="center">Hora</td>
        </tr>
    <?php
		$command="select a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre, 
  a.depto, c.dir, c.nomdir 
  from direccionactividades a 
  left join menumusuarios b on a.usuario=b.usuario 
  LEFT JOIN nomemp.dbo.nominamdepto c ON  c.depto COLLATE DATABASE_DEFAULT=a.depto COLLATE DATABASE_DEFAULT 
  where c.depto='$depto' AND a.estatus<20 order by  a.factividad DESC";// AND  YEAR(a.factividad)='$annio' AND  MONTH(a.factividad)='$mes' AND  DAY(a.factividad)='$day'
    	/*$command="select a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre from direccionactividades a left join menumusuarios b on a.usuario=b.usuario where a.usuario='$usuario' and a.estatus<20 order by a.idactividad,a.factividad,a.hactividad";*/
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			print_r( sqlsrv_errors()) ; 
		}
		else
		{
			$i=0;
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
			
	?>
    	<tr <?php if($i==0) echo 'class="first"';?>>
        	<!--<td align="center"><?php echo $row['idactividad'];?></td>-->
            <td align="left"><?php echo  trim($row['descripcion']);?></td>
            <td align="center"><?php echo trim($row['fecha']);?></td>
            <td align="center"><?php echo trim($row['hora']);?></td>
       </tr>
    <?php
			$i++;
				
			}
		}
	?>
</table>

</body>
</html>