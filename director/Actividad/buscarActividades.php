<?php
$usuario=$_COOKIE['ID_my_site'];
$depto=$_COOKIE['depto'];

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$datos=array();
$direccion="";
if(isset($_POST['direccion']) && $_POST['direccion']!="")
{
	$direccion=$_POST['direccion'];
}
else
{
	$datos['error']=1;
	$datos['mensaje']="No ha ingresado la descipcion de la actividad.";
	die( json_encode($datos) );
}

$fecha="";
$annio="1900";
$mes="01";
$day="01";
if(isset($_POST['fecha']) && $_POST['fecha']!="")
{
	//$fecha=substr($_POST['fecha'],6,4)."/".substr($_POST['fecha'],3,2)."/".substr($_POST['fecha'],0,2);
	$annio=substr($_POST['fecha'],6,4);
	$mes=substr($_POST['fecha'],3,2);
	$day=substr($_POST['fecha'],0,2);
}
else
{
	$datos['error']=1;
	$datos['mensaje']="No ha seleccionado una fecha valida.";
	die( json_encode($datos) );
}



	
	$command="select a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre, 
  a.depto, c.dir, c.nomdir 
  from direccionactividades a 
  left join menumusuarios b on a.usuario=b.usuario 
  LEFT JOIN nomemp.dbo.nominamdepto c ON  c.depto COLLATE DATABASE_DEFAULT=a.depto COLLATE DATABASE_DEFAULT 
  where ";
  if($direccion!=="*")
 	$command.=" c.dir='$direccion' AND";
  
    $command.=" YEAR(a.factividad)='$annio' AND  MONTH(a.factividad)='$mes' AND  DAY(a.factividad)='$day' AND a.estatus<20 order by c.dir, a.factividad";
 // echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$datos['error']=1;
		$datos['mensaje']="Ocurrio un error en: $command.";
	}
	else
	{
		$i=0;
		$otro=true;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			if($i>0)
			{
				if($datos[ $i - 1]['direccion']!=utf8_encode(trim($row['nomdir'])))
				{
					$datos[$i]['folio']=0;
					$datos[$i]['descripcion']='&nbsp;';
					$datos[$i]['fecha']='&nbsp;';
					$datos[$i]['hora']='&nbsp;';
					$datos[$i]['direccion']='&nbsp;';
					$otro=true;
					$i++;
				}
			}
			
			if($i==0)
			{
				$datos[$i]['folio']=$row['idactividad'];
				$datos[$i]['direccion']=utf8_encode(trim($row['nomdir']));
				$otro=false;
			}
			else
			{
				if($otro)
				{
					$datos[$i]['folio']=$row['idactividad'];
					$datos[$i]['direccion']=utf8_encode(trim($row['nomdir']));
					$otro=false;
				}
				else
				{
					$datos[$i]['folio']=$row['idactividad'];
					$datos[$i]['direccion']='';
					$otro=false;
				}
			}
			$datos[$i]['descripcion']=utf8_encode(trim($row['descripcion']));
			$datos[$i]['fecha']=$row['fecha'];
			$datos[$i]['hora']=$row['hora'];
			
			$i++;
		}
	}

	echo json_encode($datos);

?>