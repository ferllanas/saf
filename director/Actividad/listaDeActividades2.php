<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	$usuario=$_COOKIE['ID_my_site'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/jquery.tmpl.js"></script>
<style>
	tr:nth-child(even) { background: #ddd }
	tr:nth-child(odd) { background: #fff}
</style>
<script language="javascript" >
function buscaActividadesPorDireccion()
{
	//var direccionS=$('#departamento').find(":selected").val();
	var fechas=$('#fecha').val();
	//direccion:direccionS,
	var datas={ fecha:fechas};
	console.log(datas);
	$.ajax({ type: 'post',
					   data: datas,
					   url: 'buscarActividadesXDia.php',
					   dataType: 'json',
					   success: function(resp)
						{
							if(resp.length>0)
							{
								$('#fechaString').html(resp[0].fecha);
								$('#datos').empty();
								$('#tmpl_proveedor').tmpl(resp).appendTo('#datos'); 
							}
						}
					
				});  // Json es una muy buena opcion    
}
</script>
<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
		<td style="  font-family: 'Oswald', sans-serif; font-weight:normal; color:#233542; font-size:12px;  margin:0; padding:5px 0;
	width: 210px; vertical-align:top;
	min-width:210px;
	border-top:#000 1px solid;
	border-bottom:#000 1px solid;
	border-right:#000 1px solid;
	border-left:#000 1px solid;">{{html descripcion}}</td>
</script>   
<title>Pendientes Direcci&oacute;n</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->

<div style="position:absolute; left:200px; top:13px;"><input name="fecha" type="text" size="7" id="fecha" value="<?php echo date('d/m/Y');?>" class="texto8" maxlength="10"  style="width:80px" onchange="buscaActividadesPorDireccion()" title="Da Click en calendario para cambiar fecha." readonly="readonly">
					<img src="../../calendario/img.gif" width="18" height="18" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecha",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script></div>
<table width="100%">
	<tr>
    	<td colspan="2" id="titulo" width="100%">
        	<h1>Asuntos Con Director&nbsp;&nbsp; </h1> 
          
         </td>
    </tr>
    <tr>
    	<td align="center" style="font-family: 'Open Sans', sans-serif;font-size:18px;font-weight:600;" id="fechaString">    		
            <?php 
				$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
				$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

				$month=date('m');
				$days=date('d');
				$years=date('Y');
				$diadesem=date("w");
				$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
				echo $ffecha;
			?>
           
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<div class="divdirtable">
<table class="dirtable">
  <thead>
  	<?php 
		$dirs=array();
		$i=0;
		$command="SELECT depto, nomdepto FROM nomemp.dbo.nominamdepto WHERE (depto % 100)=0 ORDER BY depto ";
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if($getProducts!==false)
		{
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				if(trim($row['depto'])!='1000' && trim($row['depto'])!='7000' && trim($row['depto'])!='9000' && trim($row['depto'])!='8500'){
					$dirs[$i]['depto']=trim($row['depto']);
					if(trim($row['nomdepto'])=='UNIDAD DE CONTRALORIA INTERNA')
						$dirs[$i]['nomdepto']='CONTRALORIA INTERNA';	
					else
						if(trim($row['nomdepto'])=='DIRECCION DE ASUNTOS JURIDICOS')
							$dirs[$i]['nomdepto']='JURIDICO';	
						else					
							if(trim($row['nomdepto'])=='DIRECCION DE ADMINISTRACION Y FINANZAS')
								$dirs[$i]['nomdepto']='ADMINISTRACION';	
							else
								if(trim($row['nomdepto'])=='DIRECCION DE ASIGNACIONES Y CONTROL DOCUMENTAL')
									$dirs[$i]['nomdepto']='ASIGNACIONES';	
								else
									if(trim($row['nomdepto'])=='DIRECCION DE INFRAESTRUCTURA SOCIAL')
										$dirs[$i]['nomdepto']='INFRAESTRUCTURA';	
									else
										if(trim($row['nomdepto'])=='DIRECCION DE REGULARIZACION DE LA TENENCIA DE LA TIERRA')
											$dirs[$i]['nomdepto']='REGULARIZACION';	
										else
										{
											//echo "!".trim($row['nomdepto'])."==DIRECCION PROMOCION Y GESTION SOCIAL";
											if(trim($row['depto'])=='6000')
												$dirs[$i]['nomdepto']='PROMOCION Y GESTION';	
											else
												if(trim($row['nomdepto'])=='DIRECCION DE DELEGACIONES')
													$dirs[$i]['nomdepto']='DELEGACIONES';	
												else
													$dirs[$i]['nomdepto']=trim($row['nomdepto']);	
										}
					$i++;
				}
			}
		}
		//print_r($dirs);
		//die("Asta aqui");
		?>
		
    	<tr><?php for($i=0;$i<count($dirs);$i++)
		{
		?>
        	<th  style="background-color:#43fe46;"><?php echo $dirs[$i]['nomdepto'];?></th>
        <?php
		}
		?>
        </tr>
   </thead>
       <tbody>
       <tr id="datos">
    <?php		
		$annio= date('Y');
		$mes= date('m');
		$day= date('d');
		 for($i=0;$i<count($dirs);$i++)
		 {
			?>
            <td>
           
            <?php
    	$command="select a.idactividad,a.descripcion,CONVERT(varchar(10),a.factividad,103) as fecha,CONVERT(varchar(10),a.hactividad,108) as hora,a.estatus,b.Nombre, 
  a.depto, c.dir, c.nomdir 
  from direccionactividades a 
  left join menumusuarios b on a.usuario=b.usuario 
  LEFT JOIN nomemp.dbo.nominamdepto c ON  c.depto COLLATE DATABASE_DEFAULT=a.depto COLLATE DATABASE_DEFAULT 
  where  a.estatus<20 AND YEAR(a.factividad)='$annio' AND  MONTH(a.factividad)='$mes' AND  DAY(a.factividad)='$day' AND c.dir='".$dirs[$i]['depto']."' order by a.depto, fecha, hactividad";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			print_r( sqlsrv_errors()) ; 
		}
		else	
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				echo "".trim($row['descripcion'])."<span style='font-weight:bold;color:#F90;font-size:11px;'> ".trim($row['hora'])."</span><br>&nbsp;<br>";
			
		?>
        
        </td>
        <?php
		 }
	?>
    </tr>
    </tbody>
</table>
</div>
</body>
</html>