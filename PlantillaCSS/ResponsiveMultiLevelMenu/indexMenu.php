<?php

require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
				date_default_timezone_set('America/Mexico_City');

$IdUsuario = $_COOKIE['ID_my_site']; 
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if ($conexion)
{ 
	$consulta = "select * from menumusuarios where usuario= '$IdUsuario' ";
	$rs3 = sqlsrv_query( $conexion,$consulta);
	if (!$rs3)
	{ 
		$mensaje = $mensaje."Error in SQL";
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $rs3, SQLSRV_FETCH_ASSOC))
		{
			$nombre = utf8_encode(trim($row['nombre']));
		}	
	}
}


function debug_to_console( $data ) {

    if ( is_array( $data ) )
        $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
    else
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

    echo $output;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Responsive Multi-Level Menu - Demo 3</title>
		<meta name="description" content="Responsive Multi-Level Menu: Space-saving drop-down menu with subtle effects" />
		<meta name="keywords" content="multi-level menu, mobile menu, responsive, space-saving, drop-down menu, css, jquery" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico"> 
		<link rel="stylesheet" type="text/css" href="css/default.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body>
    <!-- Codrops top bar
			<div class="codrops-top clearfix">
				<a href="http://tympanus.net/Tutorials/AppShowcase/"><strong>&laquo; Previous Demo: </strong>App Showcase</a>
				<span class="right"><a href="http://tympanus.net/codrops/?p=14753"><strong>Back to the Codrops Article</strong></a></span>
	</div> Codrops top bar -->
		<div class="container demo-3">	
			<div class="main clearfix">
				<div class="column">
					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                        
                        	 <?php 
		$paso=true;
		$IdUsuario = $_COOKIE['ID_my_site']; 
		$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
		$conexion = sqlsrv_connect($server,$infoconexion);		 
	
		if ($conexion)
		{ 
 			$consulta = "select a.*,datalength(rtrim(ltrim(a.nivel))) as nivel,b.descrip,b.programa, CONVERT(numeric,a.nivel) as f  from menudnivel a left join menummenu b ON a.nivel = b.nivel where a.usuario= '$IdUsuario' AND a.estatus<'90' AND b.estatus<'90'  ORDER BY f ASC";
			//echo $consulta;
			//debug_to_console($consulta);//	
			$rs3 = sqlsrv_query( $conexion,$consulta);
			if (!$rs3)
			{ 
				$mensaje = $mensaje."Error in SQL";
			}
			else
			{
				$cont="";//&& $cont<5
				while( $row = sqlsrv_fetch_array( $rs3, SQLSRV_FETCH_ASSOC) )
				{
					//debug_to_console( $row );
					$descrip = utf8_encode( strtoupper( trim($row['descrip'])));
					$prog = trim($row['programa']);
					$nivel = trim($row['nivel']);
					if(strlen($prog) <=0)
					{
						if(!$paso)
						{
	?>
                            	</ul></li><li><a href='<?php echo  $prog;?>' target='centro'><?php echo  $descrip;?></a>
                            	<ul class="dl-submenu">
	<?php
						}
						else
						{
	?>
    						<li><a href='<?php echo  $prog;?>' target='centro'><?php echo  $descrip;?></a>
                           		<ul class="dl-submenu">
	<?php 
							$paso=false;				
						}
					}
					else
					{
	?>
                        <li><a href='<?php echo  $prog;?>' target='centro'><?php echo  $descrip;?></a></li>
    <?php  	
							$paso=false;						
					}					
				}
			}
		}	
	?>	
							<!--<li>
								<a href="#">Fashion</a>
								<ul class="dl-submenu">
									<li>
										<a href="#">Men</a>
										<ul class="dl-submenu">
											<li><a href="#">Shirts</a></li>
											<li><a href="#">Jackets</a></li>
											<li><a href="#">Chinos &amp; Trousers</a></li>
											<li><a href="#">Jeans</a></li>
											<li><a href="#">T-Shirts</a></li>
											<li><a href="#">Underwear</a></li>
										</ul>
									</li>
									<li>
										<a href="#">Women</a>
										<ul class="dl-submenu">
											<li><a href="#">Jackets</a></li>
											<li><a href="#">Knits</a></li>
											<li><a href="#">Jeans</a></li>
											<li><a href="#">Dresses</a></li>
											<li><a href="#">Blouses</a></li>
											<li><a href="#">T-Shirts</a></li>
											<li><a href="#">Underwear</a></li>
										</ul>
									</li>
									<li>
										<a href="#">Children</a>
										<ul class="dl-submenu">
											<li><a href="#">Boys</a></li>
											<li><a href="#">Girls</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Electronics</a>
								<ul class="dl-submenu">
									<li><a href="#">Camera &amp; Photo</a></li>
									<li><a href="#">TV &amp; Home Cinema</a></li>
									<li><a href="#">Phones</a></li>
									<li><a href="#">PC &amp; Video Games</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Furniture</a>
								<ul class="dl-submenu">
									<li>
										<a href="#">Living Room</a>
										<ul class="dl-submenu">
											<li><a href="#">Sofas &amp; Loveseats</a></li>
											<li><a href="#">Coffee &amp; Accent Tables</a></li>
											<li><a href="#">Chairs &amp; Recliners</a></li>
											<li><a href="#">Bookshelves</a></li>
										</ul>
									</li>
									<li>
										<a href="#">Bedroom</a>
										<ul class="dl-submenu">
											<li>
												<a href="#">Beds</a>
												<ul class="dl-submenu">
													<li><a href="#">Upholstered Beds</a></li>
													<li><a href="#">Divans</a></li>
													<li><a href="#">Metal Beds</a></li>
													<li><a href="#">Storage Beds</a></li>
													<li><a href="#">Wooden Beds</a></li>
													<li><a href="#">Children's Beds</a></li>
												</ul>
											</li>
											<li><a href="#">Bedroom Sets</a></li>
											<li><a href="#">Chests &amp; Dressers</a></li>
										</ul>
									</li>
									<li><a href="#">Home Office</a></li>
									<li><a href="#">Dining &amp; Bar</a></li>
									<li><a href="#">Patio</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Jewelry &amp; Watches</a>
								<ul class="dl-submenu">
									<li><a href="#">Fine Jewelry</a></li>
									<li><a href="#">Fashion Jewelry</a></li>
									<li><a href="#">Watches</a></li>
									<li>
										<a href="#">Wedding Jewelry</a>
										<ul class="dl-submenu">
											<li><a href="#">Engagement Rings</a></li>
											<li><a href="#">Bridal Sets</a></li>
											<li><a href="#">Women's Wedding Bands</a></li>
											<li><a href="#">Men's Wedding Bands</a></li>
										</ul>
									</li>
								</ul>
							</li>-->
						</ul>
                        </li>
					</div>
				</div>
                
			</div>
		</div><!-- /container -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});
		</script>
	</body>
</html>