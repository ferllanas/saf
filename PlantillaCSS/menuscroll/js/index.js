$("header > a").click(function () {
  $("nav").toggleClass("show");
});

/* Vanilla JS

document.querySelector('header > a').onclick = function () {
  var nav = document.querySelector('nav');
  nav.classList.toggle('show');
}
*/