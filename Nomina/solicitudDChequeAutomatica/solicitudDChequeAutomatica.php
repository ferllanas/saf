<?php 
header("Content-type: text/html; charset=UTF8");
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$usuario = $_COOKIE['ID_my_site']; //Misma variable para Almacen
$depto = $_COOKIE['depto']; //Misma variable para Almacen
$privsolche = $_COOKIE['privsolche']; //Misma variable para Almacen

$catego = "";
$nomcatego = "";
$nomdepto ="";					
$estatus = "";
$uresp = "";	
$depto = "";	
$nomemp = ""; 

$sct="";
$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$nomdispersion=0.00;
$hondispersion=0.00;
$nomProvDispersion="BANORTE, S.A., PAGO NOMINA";
$numProvDispersion="4812";
$nomProvDispersionHon="BANORTE, S.A., PAGO NOMINA";
$numProvDispersionHon="4812";

if( isset($_REQUEST['anno']) && isset($_REQUEST['periodo']))
{
	
	//////////////Monto de Nomina SERVINOMINA
	$command= "select monto from [nomemp].[dbo].v_nomina_dispersion a WHERE periodo='". $_REQUEST['anno']."".$_REQUEST['periodo']."".$_REQUEST['bis']."'";	
	
	$stmt = sqlsrv_query( $conexion_srv,$command);
	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$nomdispersion= number_format(trim($row['monto']),2);
		}
	}
	sqlsrv_free_stmt( $stmt);
	
	//////////////Monto de Nomina Honorarios
	$command= "select monto from [nomemp].[dbo].v_nomhon_dispersion a WHERE periodo='". $_REQUEST['anno']."".$_REQUEST['periodo']."H'";	
	$stmt = sqlsrv_query( $conexion_srv,$command);
	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$hondispersion= number_format(trim($row['monto']),2);
		}
	}
	sqlsrv_free_stmt( $stmt);
	
	//////////////Lista de Fuera de SERVINOMINA
	$fueraservinomina=array();
	$command= "select numemp, monto, nomemp from [nomemp].[dbo].v_nomina_fuera a WHERE periodo='". $_REQUEST['anno']."".$_REQUEST['periodo']."'";	
	
	$stmt = sqlsrv_query( $conexion_srv,$command);
	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$fueraservinomina[$i]['numemp']= trim($row['numemp']); 
			$fueraservinomina[$i]['nomemp'] = trim($row['nomemp']);
			$fueraservinomina[$i]['monto'] = number_format(trim($row['monto']),2);
			$i++;
		}
	}
	sqlsrv_free_stmt( $stmt);
	
	// Fondo de Ahorro
	//select cvepd,sum(importe) as total from DPERDED where periodo='201301' and cvepd='0235' group by cvepd
	$Persepciones=array();
	$command= "select a.cvepd, sum(a.importe) as monto, b.nomper, c.nomded , d.prov
			 from nomemp.dbo.nominadperded a 
		LEFT JOIN nomemp.dbo.nominamcatper b ON a.cvepd=b.cveper 
		LEFT JOIN nomemp.dbo.nominamcatded c ON c.cveded=a.cvepd
		LEFT JOIN nomemp.dbo.nominamsolche d ON d.cvepd=a.cvepd
		where a.periodo='". $_REQUEST['anno']."".$_REQUEST['periodo']."' 
				AND a.cvepd IN('0215','0219', '0222', '0226', '0228', '0235', '0258', '0264', '0265','0202') 
		 group by a.cvepd, b.nomper, c.nomded, d.prov";	
	
	//echo $command;
	$stmt = sqlsrv_query( $conexion_srv,$command);
	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$Persepciones[$i]['prov']= trim($row['prov']); 
			$Persepciones[$i]['nomded'] = trim($row['nomded']);
			$Persepciones[$i]['monto'] = number_format(trim($row['monto']),2);
			$i++;
		}
	}
	sqlsrv_free_stmt( $stmt);

	// Sumario Alimenticio
	//////////////DEL Archivo de omar
	$command= "SELECT *  FROM v_respsolche b";	
	$stmt = sqlsrv_query( $conexion_srv, $command);
	if( $stmt === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	
	if(sqlsrv_has_rows($stmt))
	{
		$i=0;
		// Se valida que la informacion en $stmt
		while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//Esta variable se pone cuando uno va a desplegar una tabla con informacion de detalle, como por ejemplo una factura.
			//$productos[$i]['cve'] =trim($lrow['cve']);
			$catego = trim($lrow['catego']);
			$nomcatego = trim($lrow['nomcatego']);
			$nomdepto = trim($lrow['nomdepto']);					
			$estatus = trim($lrow['estatus']);
			$uresp = trim($lrow['numemp']);	
			$depto = trim($lrow['depto']);	
			$nomemp = trim($lrow['nomemp']); // se cambia de $nomemp a $nomemp
			$i++;
		}
	}
	sqlsrv_free_stmt( $stmt);



	// VALES DESPENSA
	$VALESDEDESPENSA=array();
	$command= "select a.cvepd,sum(a.importe) as total , b.nombre, c.prov, c.descrip
	from  nomemp.dbo.nominadperded a 
	LEFT JOIN fomeadmin.dbo.nominamconceptos_presup b ON b.conc=a.cvepd
	LEFT JOIN nomemp.dbo.nominamsolche c ON c.cvepd=114
where a.periodo='". $_REQUEST['anno']."".$_REQUEST['periodo']."BC' and a.cvepd<>'0210' GROUP BY a.CVEPD, b.nombre, c.prov, c.descrip";	
	$stmt = sqlsrv_query( $conexion_srv, $command);
	if( $stmt === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	$i=0;
	if(sqlsrv_has_rows($stmt))
	{
		// Se valida que la informacion en $stmt
		while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//Esta variable se pone cuando uno va a desplegar una tabla con informacion de detalle, como por ejemplo una factura.
			//$productos[$i]['cve'] =trim($lrow['cve']);
			$VALESDEDESPENSA[$i]['cvepd'] = trim($lrow['cvepd']);
			$VALESDEDESPENSA[$i]['strtotal'] =number_format( trim($lrow['total']),2);
			$VALESDEDESPENSA[$i]['total'] = (float)$lrow['total'];
			$VALESDEDESPENSA[$i]['nombre'] = trim($lrow['nombre']);					
			$VALESDEDESPENSA[$i]['prov'] = trim($lrow['prov']);
			$VALESDEDESPENSA[$i]['descrip'] = trim($lrow['descrip']);	
			$i++;
		}
	}
	sqlsrv_free_stmt( $stmt);


		$command= "select a.cvepd,sum(a.importe) as total , b.nombre, c.prov, c.descrip
	from nomemp.dbo.nominadperded a
	LEFT JOIN fomeadmin.dbo.nominamconceptos_presup b ON b.conc=a.cvepd
	LEFT JOIN nomemp.dbo.nominamsolche c ON c.cvepd=114
	where a.periodo='". $_REQUEST['anno']."".$_REQUEST['periodo']."BS' and a.cvepd<>'0210' GROUP BY a.CVEPD, b.nombre, c.prov, c.descrip";	
	$stmt = sqlsrv_query( $conexion_srv, $command);
	if( $stmt === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	if(sqlsrv_has_rows($stmt))
	{
		// Se valida que la informacion en $stmt
		while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//Esta variable se pone cuando uno va a desplegar una tabla con informacion de detalle, como por ejemplo una factura.
			//$productos[$i]['cve'] =trim($lrow['cve']);
			$VALESDEDESPENSA[$i]['cvepd'] = trim($lrow['cvepd']);
			$VALESDEDESPENSA[$i]['strtotal'] =number_format( trim($lrow['total']),2);
			$VALESDEDESPENSA[$i]['total'] = (float)$lrow['total'];
			$VALESDEDESPENSA[$i]['nombre'] = trim($lrow['nombre']);					
			$VALESDEDESPENSA[$i]['prov'] = trim($lrow['prov']);
			$VALESDEDESPENSA[$i]['descrip'] = trim($lrow['descrip']);	
		}
	}
	
	$TotalVales=0.00;
	echo (float)$VALESDEDESPENSA[0]['total']." - ". (float)$VALESDEDESPENSA[1]['total'];
	
	$Vtotal= (float)$VALESDEDESPENSA[0]['total'] - (float)$VALESDEDESPENSA[1]['total'];
	$Bonific= $Vtotal * 0.0073;
	echo ", TOTAL CONF:". ($Vtotal - $Bonific)."+". ((float)$VALESDEDESPENSA[2]['total']*0.0073);
	$TotalVales=($Vtotal-$Bonific) + ($VALESDEDESPENSA[2]['total']-((float)$VALESDEDESPENSA[2]['total']*0.0073));
	sqlsrv_free_stmt( $stmt);

	//////////////DEL Archivo de omar FIRMAS AUTORIZADAS
	$datosx=array();
	$consulta = "select * from nominamdepto where ";
	switch($privsolche)
	{
		case 0:
			 $consulta .= " depto=$x_depto and ";
			break;
		case 20:
			$wdepto=substr($depto,0,2);
			$consulta .= "left(depto,2)=$wdepto and ";
			break;
		case 80:
			$consulta .= "";
			break;
	}
	$consulta .= "estatus < '90'";
	$R = sqlsrv_query( $conexion_srv,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datosx[$i]['depto']= trim($row['depto']);
		$datosx[$i]['nomdepto']= trim($row['nomdepto']);
		$i++;
	}
	sqlsrv_free_stmt( $R );
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

     
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/estilos.css"/>
<script type="text/javascript" src="solicitudDChequeAutiomatica.js"></script>
<script type="text/javascript" src="../../jquery-ui-1.9.2.custom/js/jquery-1.8.3.js"></script>
<title>Solicitud de Cheque Autom&aacute;tica por Conceptos de N&oacute;mina</title>
</head>

<body>
<span class="TituloDForma">Solicitud de Cheque Autom&aacute;tica por Conceptos de N&oacute;mina</span>
<hr class="hrTitForma">
<table>
    <tr>
        <td>
        <form action="solicitudDChequeAutomatica.php">
        <table>
                <tr>
                    <td style="width:30px;">Año:</td><td style="width:60px;">
                        <input type="text" id="anno" name="anno" class="numeric" maxlength="4" width="60px" value="<?php if(isset($_REQUEST['anno'])) echo $_REQUEST['anno'];?>" /></td>
                    <td style="width:30px;">Periodo:</td><td style="width:60px;"><input type="text"  id="periodo" name="periodo" class="numeric" maxlength="2" width="60px" value="<?php if(isset($_REQUEST['periodo'])) echo $_REQUEST['periodo'];?>" /></td>
                    <td style="width:30px;">?:</td><td style="width:60px;"><input type="text" id="bis" name="bis" class="numeric" maxlength="2" width="60px" <?php if(isset($_REQUEST['bis'])) echo $_REQUEST['bis'];?> /></td>
                </tr>
                <tr>
                    <td colspan="6" align="center"><input type="submit" value="Mostrar" /></td>
                </tr>
              
       </table>
       </form>  
       </td>
    </tr>
    <tr>
    	<td>
        <input type="hidden" id="usuario" name="usuario"  class="texto8"size="10" value="<?php echo $usuario;?>">
        <input type="hidden" id="depto"   name="depto" class="texto8"  size="10" value="<?php echo $depto;?>">	
        <input type="text" class="texto8" tabindex="1" name="cvedepto" id="cvedepto" style="width:340px;" onKeyUp="searchdepto(this);" 
        			autocomplete="off" value="<?php echo $depto;?>">		    
	    
        <input type="hidden" id="nomemp"  name="nomemp"  size="50"value="<?php echo $nomemp;?>">
        <input type="hidden" id="uresp"   name="uresp"  size="50"value="<?php echo $uresp;?>">
        <input class="texto8" type="hidden" id="nomdepto2" width="41" name="nomdepto2" size="10" value="">          
        <input type="hidden" id="nomcoord" name="nomcoord"  class="texto8"size="10" value="">
     	<input class="texto8" type="hidden" id="nomdir" name="nomdir" size="10" value="">
        </td>
    </tr>
    <tr>
    
    	<td>
		<?php 
            if(isset($_REQUEST['anno']) && isset($_REQUEST['periodo']) )
            {
        ?>
        	<table>
            	<tr>
                	<td>
                    <table width="100%">
                    	<tr>
                            <td  class="texto12" align="left" ><input type="text" class="caja_toprint" style="font-size:16px; font-weight:bold;" id="concepto_nomdispersion" name="concepto_nomdispersion" value="Dispersi&oacute;n N&oacute;mina" readonly="readonly"/>:</td>
                            <td class="texto12" align="right"><input type="text" id="nomdispersion" name="nomdispersion" class="caja_toprint" style="font-size:16px;  text-align:right;"  value="$<?php echo $nomdispersion;?>" readonly="readonly" />
                            <input type="hidden" id="nomProvDispersion" name="nomProvDispersion" value="<?php echo $nomProvDispersion;?>" />
                            <input type="hidden" id="numProvDispersion" name="numProvDispersion" value="<?php echo $numProvDispersion;?>" />
                            </td>
                        </tr>
                        <tr>
                			<td  class="texto12"  align="left"><input type="text" class="caja_toprint" style="font-size:16px; font-weight:bold;" id="concepto_asimiladosdispersion" name="concepto_asimiladosdispersion" value="Dispersi&oacute;n Asimilados" readonly="readonly" />:</td>
                            <td class="texto12"  align="right"><input type="text" id="hondispersion" name="hondispersion" class="caja_toprint" style="font-size:16px;  text-align:right;" value="$<?php echo $hondispersion;?>" readonly="readonly" />
                            <input type="hidden" id="nomProvDispersionHon" name="nomProvDispersionHon" value="<?php echo $nomProvDispersionHon;?>" />
                            <input type="hidden" id="numProvDispersionHon" name="numProvDispersionHon" value="<?php echo $numProvDispersionHon;?>" /></td>
                </tr>
                     </table>
                    </td>
                </tr>
               
                <tr>
                	<td align="left"> 
                    	<table align="center">
                        	<caption class="texto12" align="center" ><b>Pago Fuera de SERVINOMINA:</b></caption>
                            <thead>
                                <tr>
                               		<th align="center" class="texto10" style="width:100px;">Num. Empleado</th>
                                    <th align="center" class="texto10" style="width:350px;">Empleado</th>
                                    <th align="center" class="texto10" style="width:200px;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
									for( $i=0 ; $i<count($fueraservinomina) ; $i++)
									{
								?>
                           	 	<tr>
                                	<td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:center;" id="numempfueraservnomina" name="numempfueraservnomina" value="<?php echo $fueraservinomina[$i]['numemp'];?>" readonly="readonly" /></td>
                                    <td class="texto10"><input type="text" class="caja_toprint" style="width:100%;font-size:16px;" id="nomempfueraservnomina" name="nomempfueraservnomina" value="<?php echo $fueraservinomina[$i]['nomemp'];?>" readonly="readonly" /></td>
                                    <td align="right" class="texto10"><input type="text" class="caja_toprint" style="width:90%;font-size:16px; text-align:right;" id="montofueraservnomina" name="montofueraservnomina" value="$<?php echo $fueraservinomina[$i]['monto'];?>" readonly="readonly" /></td>
                            	</tr>
                                <?php 
									}
								?>
                            </tbody>
						</table>
                   </td>
               </tr>
            </table>
        <?php
        
            }
        ?>
        	
      	<td> 
    </tr>
    <tr>
    	<td>
            <table align="center">
                        <caption class="texto12" align="center" ><b>Deducciones :</b></caption>
                        <thead>
                            <tr>
                                <th align="center" class="texto10" style="width:100px;">Num. Proveedor</th>
                                <th align="center" class="texto10" style="width:350px;">Concepto</th>
                                <th align="center" class="texto10" style="width:200px;">Monto</th>
                            </tr>
                        </thead>
            	<?php
				for($i=0;$i<count($Persepciones);$i++)
				{
					?>
                	<tr>
                    	<td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:center;" id="numempfueraservnomina" name="numempfueraservnomina" value="<?php echo $Persepciones[$i]['prov'];?>" readonly="readonly" /></td>
                        <td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:left;" id="numempfueraservnomina" name="numempfueraservnomina" value="<?php echo $Persepciones[$i]['nomded'];?>" readonly="readonly" /></td>
                        <td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:right;" id="numempfueraservnomina" name="numempfueraservnomina" value="$<?php echo $Persepciones[$i]['monto'];?>" readonly="readonly" /></td>
                    </tr>
                <?php	
				}
				?>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
            <table align="center">
                        <caption class="texto12" align="center" >
                        <b>Vales de Despensa :</b>
                        </caption>
                        <thead>
                            <tr>
                                <th align="center" class="texto10" style="width:100px;">Num. Proveedor</th>
                                <th align="center" class="texto10" style="width:350px;">Concepto</th>
                                <th align="center" class="texto10" style="width:200px;">Monto</th>
                            </tr>
                        </thead>
            	<?php
				for($i=0;$i<count($VALESDEDESPENSA);$i++)
				{
					?>
                	<tr>
                    	<td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:center;" id="numempfueraservnomina" name="numempfueraservnomina" value="<?php echo $VALESDEDESPENSA[$i]['prov'];?>" readonly="readonly" /></td>
                        <td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:left;" id="numempfueraservnomina" name="numempfueraservnomina" value="<?php echo $VALESDEDESPENSA[$i]['nombre'];?>" readonly="readonly" /></td>
                        <td align="center" class="texto10"><input type="text" class="caja_toprint" style=" width:100%;font-size:16px; text-align:right;" id="numempfueraservnomina" name="numempfueraservnomina" value="$<?php echo $VALESDEDESPENSA[$i]['strtotal'];?>" readonly="readonly" /></td>
                    </tr>
                <?php	
				}
				?>
                <tfood>
                	<tr>
                    	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><?php echo number_format($TotalVales,2);?></td>
                    </tr>
                </tfood>
            </table>
        </td>
    </tr>
    <tr>
    	<td align="center">
        	<input type="button" value="Generar Solicitudes de Cheque" onclick="guardar_datos(this)" style="visibility:visible"/>
        </td>
    </tr>
</table>

</body>
</html>