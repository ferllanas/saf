<?php

header("Content-type: text/html; charset=UTF8");
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

//$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

?>

<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
		<meta charset="UTF-8">
        <link rel="stylesheet" href="css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />      
        <title>SAF- Correo Electronico</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="javascript/divhide.js"></script>
        <script src="javascript/jquery.tmpl.js"></script>
		
		<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						//var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						var data = 'query=' + $(this).val();
						//alert(data);
						$.post('ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			

			function guardar(numemp,btnGuardar)
			{
				//alert(numemp);
				var tabla = document.getElementById('proveedor');
				for (var i = 0; i < tabla.rows.length; i++) 
				{ 
					var bandera=false;
					for (var j = 0; j <tabla.rows[i].cells.length;j++)
					{
						var cell = tabla.rows[i].cells[j];
						for (var k = 0; k < cell.childNodes.length; k++) 
						{
							var mynode = cell.childNodes[k];
							if (mynode.name=="xnumemp")
							{
								if(mynode.value==numemp)
								{
									bandera=true;
								}
							}
							
							if(mynode.name=="correo" && bandera==true)
							{
								xcorreo=mynode.value;
								//alert(xcorreo);
							}
							if(mynode.name=="pass" && bandera==true)
							{
								xpass=mynode.value;
								//alert(xpass);
							}
							if(mynode.name=="tipo" && bandera==true)
							{	
								xtipo=mynode.value;
								//alert(xtipo);
							}
							
						}
					}
				}

				//var pregunta = confirm("Esta seguro que desea Actualizar")
				//if (pregunta)
				//{
				//alert("php_ajax/actualiza_correo.php?xnumemp="+numemp+"&xcorreo="+xcorreo+"&xpass="+xpass+"&xtipo="+xtipo);
					location.href="php_ajax/actualiza_correo.php?xnumemp="+numemp+"&xcorreo="+xcorreo+"&xpass="+xpass+"&xtipo="+xtipo;
				//}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if numemp}}
					<td align="center">${numemp}<input name="xnumemp" id="xnumemp" type="hidden" value="${numemp}" ></td>
			
					<td align="center">${nomemp}</td>
					<td align="center"><input class="correo" name="correo" id="correo" type ="text" value="${emailfome}" size="30" style="background-color:transparent; "></td>
					<td align="center"><input class="pass" name="pass" id="pass" type ="text" value="${pasw}" size="15" style="background-color:transparent; "></td>
					<td align="center">${estatus}</td>
					<td align="center">${tipo}<input class="tipo" name="tipo" id="tipo" type="hidden" value="${tipo}"</td>
					<td align="center"><input type="button" value="Guardar" onClick="guardar('${numemp}',this)"></td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   

<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 14px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Relación de Correo Electronico</span>    

    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%"><select name="opc" id="opc" style="visibility:hidden ">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >No de Empleado</option>
              <option value="2" >Nombre</option>
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:visible; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>

<input type="hidden" name="ren" id="ren">
<input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario; ?>">

</td>
</tr>
</table>
<table>
		<thead>
		 	<th>No. Emp </th>
			<th>Nombre</th>
			<th>Correo</th>
			<th>Password</th>
			<th>Estatus</th>
			<th>Tipo</th>
			<th>Acción</th>
		</thead>
			<tbody id="proveedor" name='proveedor'>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">Encontrar Resultados</td>
				</tr>
			</tbody>
</table>
    </div>
    </body>
</html>
