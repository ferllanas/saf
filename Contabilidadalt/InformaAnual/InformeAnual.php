<?php

include_once '../../cheques/lib/ez_sql_core.php'; 
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if(!$conexion)
	echo "Error en conexion";
	
$anno=0;
if(isset($_POST['anno']))
	$anno=$_POST['anno'];
else
	$anno=date("Y");
$mes=00;
if(isset($_POST['mes']))
	$mes=$_POST['mes'];
else
	$mes=date("m");
$consulta="EXECUTE sp_presup_informe_anual_cta_publica_1a_parte $anno, $mes";

	$saprobado=0.00;
	$samp_redu=0.00;
	$svigente=0.00;
	$scom=0.00;
	$sdxacom=0.00;
	$sdev=0.00;
	$scom_no_dev=0.00;
	$ssin_dev=0.00;
	$seje=0.00;
	$spag=0.00;
	$sctaxpag=0.00;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css">         
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />       
<title>Documento sin título</title>
<style>
	#report{ font-size:13px
		
	}
	#report thead{
		background-color:#CCC;
		font-weight:800;
	}
	
</style>
</head>

<body>
<table width="100%">
	<tr>
    	<td><span class="TituloDForma">Informe Mensual de Cuenta P&uacute;blica</span>
    <hr class="hrTitForma"></td>
    </tr>
    <tr>
    	<td>
        <form action="InformeAnual.php" method="post">
        <table><tr>
            <td>A&ntilde;o</td><td><input id='anno' name='anno' type="text" value="<?php echo $anno;?>" maxlength="4" style="width:70px"  /></td><td>Mes:</td>
        <td><select id="mes" name='mes'><option value='01' <?php if($mes=='01') echo "selected";?>>Enero</option>
        					<option value='02' <?php if($mes=='02') echo "selected";?>>Febrero</option>
                            <option value='03' <?php if($mes=='03') echo "selected";?>>Marzo</option>
                            <option value='04' <?php if($mes=='04') echo "selected";?>>Abril</option>
                            <option value='05' <?php if($mes=='05') echo "selected";?>>Mayo</option>
                            <option value='06' <?php if($mes=='06') echo "selected";?>>Junio</option>
                            <option value='07' <?php if($mes=='07') echo "selected";?>>Julio</option>
                            <option value='08' <?php if($mes=='08') echo "selected";?>>Agosto</option>
                            <option value='09' <?php if($mes=='09') echo "selected";?>>Septiembre</option>
                            <option value='10' <?php if($mes=='10') echo "selected";?>>Octubre</option>
                            <option value='11' <?php if($mes=='11') echo "selected";?>>Noviembre</option>
                            <option value='12' <?php if($mes=='12') echo "selected";?>>Diciembre</option>
        	</select></td><td><input type="submit" value="buscar"/></td></tr>
        
            </table>
       </form>
        </td>
    </tr>
    <tr>
    	<td>
        	<table id="report" border="1">
            	<thead>
                	<tr>
              			<td align="center">Ejercicio del Presupuesto Capitulo del Gasto</td>
              			<td align="center">Ejercicio de Egresos Aprobado</td>
              			<td align="center">Ampliaciones/(Reducciones)</td>
              			<td align="center">Presupuesto Vigente</td>
              			<td align="center">Comprometido</td>
              			<td align="center">Presupuesto Disponible para Comprometer</td>
              			<td align="center">Devengado</td>
              			<td align="center">Comprometido no Devengado</td>
              			<td align="center">Presupuesto sin Devengar</td>
              			<td align="center">Ejercido</td>
              			<td align="center">Pagado</td>
              			<td align="center">Cuenta por Pagar (Deuda)</td>
					</tr>
                    <tr>
              			<td align="center">Nombre</td>
              			<td align="center">1</td>
              			<td align="center">2</td>
              			<td align="center">3</td>
              			<td align="center">4</td>
              			<td align="center">5=(3-4)</td>
              			<td align="center">6</td>
              			<td align="center">7=(4-6)</td>
              			<td align="center">8=(3-6)</td>
              			<td align="center">9</td>
              			<td align="center">10</td>
              			<td align="center">11=(6-10)</td>
					</tr>
                </thead>
                <tbody id="tresultados">
                	<?php
						//echo $consulta;
						if($anno>0)
						{
							$stmt = sqlsrv_query($conexion, $consulta);
							if( $stmt === false)
							{
								 die( print_r( sqlsrv_errors(), true) );
							}			
							else
							{
								//echo "Si entro";
								 $i=0;
							 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
							 {
							?>
							<tr>	
								<td align="left">  <?php echo trim($row['rubro']).".-".utf8_encode(trim($row['nomrubro'])); ?></td>
								<td align="right"><a href="detalle.php?rubro=<?php echo trim($row['rubro']);?>&tiempo=1&anno=<?php echo $anno;?>&mes=<?php echo $mes;?>"><?php echo number_format(trim($row['aprobado']),2); 	$saprobado+=(float)$row['aprobado'];?></a></td>
								<td align="right"> <?php echo number_format(trim($row['amp_redu']),2);	$samp_redu+=(float)$row['amp_redu'];?></td>
								<td align="right"> <?php echo number_format(trim($row['vigente']),2);  	$svigente+=(float)$row['vigente'];?></td>
								<td align="right"> <?php echo number_format(trim($row['com']),2); 		$scom+=(float)$row['com'];?></td>
								<td align="right"> <?php echo number_format(trim($row['dxacom']),2); 	$sdxacom+=(float)$row['dxacom'];?></td>
								<td align="right"> <?php echo number_format(trim($row['dev']),2); 		$sdev+=(float)$row['dev'];?></td>
								<td align="right"> <?php echo number_format(trim($row['com_no_dev']),2); $scom_no_dev+=(float)$row['com_no_dev'];?></td>
								<td align="right"> <?php echo number_format(trim($row['sin_dev']),2);  	$ssin_dev+=(float)$row['sin_dev'];?></td>
								<td align="right"> <?php echo number_format(trim($row['eje']),2);  		$seje+=(float)$row['eje'];?></td>
								<td align="right "><?php echo number_format(trim($row['pag']),2); 		$spag+=(float)$row['pag'];?></td>
								<td align="right"> <?php echo number_format(trim($row['ctaxpag']),2); 	$sctaxpag+=(float)$row['ctaxpag'];?></td>
							</tr>			
							<?php	
							}
						}
					}
					?>
                </tbody>
                <tfoot>
                	<tr>
              			<td	align="right">Total</td>
              			<td	align="right"><?php echo number_format($saprobado,2);?></td>
              			<td	align="right"><?php echo number_format($samp_redu,2);?></td>
              			<td	align="right"><?php echo number_format($svigente,2);?></td>
              			<td	align="right"><?php echo number_format($scom,2);?></td>
              			<td	align="right"><?php echo number_format($sdxacom,2);?></td>
              			<td	align="right"><?php echo number_format($sdev,2);?></td>
              			<td	align="right"><?php echo number_format($scom_no_dev,2);?>r</td>
              			<td	align="right"><?php echo number_format($ssin_dev,2);?></td>
              			<td	align="right"><?php echo number_format($seje,2);?></td>
              			<td	align="right"><?php echo number_format($spag,2);?>)</td>
                        <td	align="right"><?php echo number_format($sctaxpag,2);?>)</td>
                    </tr>
                </tfoot>
            </table>
        </td>
    </tr>
</table>
</body>
</html>