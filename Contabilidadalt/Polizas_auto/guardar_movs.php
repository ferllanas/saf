<?php
require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
$datas=array();
$usuario = $_COOKIE['ID_my_site'];	
$movs=array();
if(isset($_REQUEST["movs"]))
	$movs=$_REQUEST["movs"];
	
$fecini="";
if(isset($_REQUEST["fecini"]))
	$fecini=$_REQUEST["fecini"];
	
$tipo_poliza="";
if(isset($_REQUEST["tipo_poliza"]))
	$tipo_poliza=$_REQUEST["tipo_poliza"];
	
$datas['error']=0;
$datas['msg']="";
$datas['idmpoliza']="";

list( $folio, $idmpoliza, $poliza, $error)=guardarmPolizas($tipo_poliza, $fecini, $usuario);
if($error!=="Error")
{
	if(strlen($idmpoliza)>0 && strlen($folio)>0){
		for($i=0;$i<count($movs);$i++)
		{
			$msg=guardardPolizas($idmpoliza, $tipo_poliza , $poliza, $folio, $movs[$i], $usuario);
			if($msg=="Error")
			{
				$datas['error']=1;
				$datas['msg'].="Ocurrio un Error al intentar guardar en dpoliza con el movimiento: ".$movs[$i]['idm']."\n.";
			}
		}
	}
	$datas['idmpoliza']=$idmpoliza;
	echo json_encode($datas);
}
else
{
	$datas['error']=1;
	$datas['msg']="A ocurrido un error al crear la poliza, comuniquese a sistemas.";
	$datas['idmpoliza']=$idmpoliza;
	echo json_encode($datas);
}



function guardarmPolizas($tipo_poliza, $fecini, $usuario)
{
	global $username_db, $password_db, $odbc_name, $server;
	
	$folio = "";
	$idmpoliza = "";
	$poliza="";
	$error="";	
	$fails=false;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_contab_A_mpoliza_new(?,?,?,?)}";//Arma el procedimeinto almacenado
	$poliza=0;
	list($dia,$mes,$anio)=explode("/",$fecini);
	$fecha =$anio."/".$mes."/".$dia."'";	

	$params = array(&$poliza,&$tipo_poliza,&$usuario,&$fecha);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_contab_A_mpoliza".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$folio = $row['folio'];
			$idmpoliza = $row['idmpoliza'];
			$poliza = $row['poliza'];
			$error = $row['msg'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($folio,$idmpoliza,$poliza,$error);
	
}

function guardardPolizas($idmpoliza, $tipo_poliza , $poliza, $folio, $mov, $usuario)
{
	global $username_db, $password_db, $odbc_name, $server;
	
	$msg  = "";
			
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_contab_A_dpoliza_new(?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$poliza0=0;
	$cargo=floatval($mov['cargo']);
	$credito=floatval($mov['credito']);
	$params = array(&$poliza0,&$idmpoliza, &$tipo_poliza , &$poliza, &$folio,&$mov['cuenta'],&$mov['nombre'],&$mov['referencia'], &$cargo,&$credito,&$usuario, &$mov['origen'], &$mov['idm']);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		return array($folio,$idmpoliza);
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$msg = $row['msg'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $msg ;
	
}
?>