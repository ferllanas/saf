<?php
require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
$datas=array();
$usuario = $_COOKIE['ID_my_site'];	
$movs=array();
if(isset($_REQUEST["movs"]))
	$movs=$_REQUEST["movs"];
	
$fecini="";
if(isset($_REQUEST["fecini"]))
	$fecini=$_REQUEST["fecini"];
	
$tipo_poliza="";
if(isset($_REQUEST["tipo_poliza"]))
	$tipo_poliza=$_REQUEST["tipo_poliza"];
	
$quin="";
if(isset($_REQUEST["quin"]))
	$quin=$_REQUEST["quin"];
	

$nomtippoliza="";
if(isset($_REQUEST["nomtippoliza"]))
	$nomtippoliza=$_REQUEST["nomtippoliza"];
	
$datas['error']=0;
$datas['msg']="";
$datas['idmpoliza']="";
$polizasAImprimir=array();

$movAgrupadosByreferenciacia=array();

//Agrupado a falso
for($i=0;$i<count($movs);$i++)
	$movs[$i]['agrupado']=0;

$counAgrupados=0;
for($i=0;$i<count($movs);$i++)
{

	if($movs[$i]['agrupado']==0)
	{
		
		$movAgrupadosByreferenciacia[$counAgrupados]['nomtippoliza']=$nomtippoliza;
		$movAgrupadosByreferenciacia[$counAgrupados]['tipo_poliza']=$tipo_poliza;
		$movAgrupadosByreferenciacia[$counAgrupados]['usuario']=$usuario;
		
		if($tipo_poliza=="PDN")
		{
			$movAgrupadosByreferenciacia[$counAgrupados]['fecha']=$quin;
		}
		else{
			$movAgrupadosByreferenciacia[$counAgrupados]['fecha']=$fecini;
		}
		$movAgrupadosByreferenciacia[$counAgrupados]['referencia']=$movs[$i]['referencia'];

		$movs[$i]['agrupado']=1;
		$movAgrupadosByreferenciacia[$counAgrupados]['dpoliza'][]= $movs[$i];
		
		$result = findPosOnVector($movs,"referencia", $movAgrupadosByreferenciacia[$counAgrupados]['referencia']);
		while(($result = findPosOnVector($movs,"referencia", $movAgrupadosByreferenciacia[$counAgrupados]['referencia']))>=0)
		{
			$movs[$result]['agrupado']=1;
			$movAgrupadosByreferenciacia[$counAgrupados]['dpoliza'][]= $movs[$result];
		}		
		$counAgrupados++;
	}
}

$polizasAImprimir=array();



for($i=0;$i<count($movAgrupadosByreferenciacia);$i++)
{
	list( $folio, $idmpoliza, $poliza, $error)=guardarmPolizas( $movAgrupadosByreferenciacia[$i]['tipo_poliza'], 
																$movAgrupadosByreferenciacia[$i]['fecha'], 
																$movAgrupadosByreferenciacia[$i]['usuario']);
	$movAgrupadosByreferenciacia[$i]['poliza']=$poliza;
	$movAgrupadosByreferenciacia[$i]['folio']=$folio;
	//echo $error;
	if($error!=="Error")
	{
		//echo strlen($idmpoliza).">0 && ".strlen($folio).">0";
		if(strlen($idmpoliza)>0 && strlen($folio)>0){
			
			//echo "Contador=".count($movAgrupadosByreferenciacia[$i]['dpoliza']);
			for($j=0;$j<count($movAgrupadosByreferenciacia[$i]['dpoliza']);$j++)
			{
				//print_r($movAgrupadosByreferenciacia[$i]['dpoliza']); 
				$msg=guardardPolizas($idmpoliza, $tipo_poliza , $poliza, $folio, $movAgrupadosByreferenciacia[$i]['dpoliza'][$j], $usuario);
				if($msg=="Error")
				{
					$datas['error']=1;
					$datas['msg'].="Ocurrio un Error al intentar guardar en dpoliza con el movimiento: ".$movAgrupadosByreferenciacia[$i]['dpoliza'][$j]."\n.";
					//print_r($datas);
				}
			}
			
			array_push($polizasAImprimir, $idmpoliza);
		}
		
	}
	else
	{
		$datas['error']=1;
		$datas['msg']="A ocurrido un error al crear la poliza, comuniquese a sistemas.";
		$datas['idmpoliza']=$idmpoliza;
	}

}

echo json_encode($movAgrupadosByreferenciacia);

function guardarmPolizas($tipo_poliza, $fecini, $usuario)
{
	global $username_db, $password_db, $odbc_name, $server;
	
	$folio = "";
	$idmpoliza = "";
	$poliza="";
	$error="";	
	$fails=false;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$espaciosFinal="";
	$tsql_callSP ="{call sp_contab_A_mpoliza_alt(?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$poliza=0;
	
	if(strpos("/",$fecini)===FALSE)
	{
		$fecha = $fecini;	
	}
	else
		{
			list($dia,$mes,$anio)=explode("/",$fecini);
			$fecha =$anio."/".$mes."/".$dia;	
		}
		
	$params = array(&$poliza,&$tipo_poliza,&$usuario,&$fecha, &$espaciosFinal);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_contab_A_mpoliza_alt".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$folio = $row['folio'];// consecutivo
			$idmpoliza = $row['idmpoliza'];//para d poliza
			$poliza = $row['poliza'];//
			$error = $row['msg'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($folio,$idmpoliza,$poliza,$error);
	
}

function guardardPolizas($idmpoliza, $tipo_poliza , $poliza, $folio, $mov, $usuario)
{
	global $username_db, $password_db, $odbc_name, $server;
	
	$msg  = "";
	$fails=false;	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_contab_A_dpoliza_alt(?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$poliza0=0;
	$cargo=floatval(str_replace(",","",$mov['cargo']));
	$credito=floatval(str_replace(",","",$mov['credito']));

	$params = array(&$poliza0,&$idmpoliza, &$tipo_poliza , &$poliza, &$folio,&$mov['cuenta'],&$mov['nombre'],&$mov['referencia'], &$cargo,&$credito,&$usuario, &$mov['origen'], &$mov['idm']);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $msg= "Error in statement execution.\n";
		 $fails=true;
		echo "Error in statement execution.\n". $tsql_callSP ;
		print_r($params);
		return ;
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$msg = $row['msg'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $msg ;
	
}

/////////////////////////////////////////////////////////////
function findPosOnVector($vector, $nameItem, $valueItem)
{
	//echo $nameItem.", ".$valueItem;
	for($i=0;$i<count($vector);$i++)
	{
		//echo "<br>".$vector[$i][$nameItem]."==".$valueItem." && ".$vector[$i]['agrupado']."==0";
		if($vector[$i][$nameItem]==$valueItem && $vector[$i]['agrupado']==0)
			return $i;
	}
	return -1;
}
////////////////////////////////////////////////////////////
?>