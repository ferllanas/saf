<?php

require_once("../../dompdf-master/dompdf_config.inc.php");

$polizas = $_POST['polizas'];
if (mb_detect_encoding($str ) == 'UTF-8') {
   $str = mb_convert_encoding($str , "HTML-ENTITIES", "UTF-8");
}




/*$DDD="<html>
<head>
	<title>Estado de Situacion Financiera</title>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
</head>
<html style='margin:  0.01px 0.01px 0.01px 0.01px;' id='page'>
<body style='margin:  0.01px 0.01px 0.01px 0.01px;'>".$str."</body></html>";

$str=str_replace("font-size:12","font-size:8px", $DDD);
$str=str_replace("font-size:14","font-size:8px", $str);
$str=str_replace("font-size:16","font-size:10px", $str);


$dompdf = new DOMPDF();
$dompdf->load_html($str);
$dompdf->set_paper('legal', 'landscape');
$dompdf->render();
$dompdf->stream("Estado de Situacion Financiera.pdf");	
//$pdf = $dompdf->output();

echo $str; */
?>

<html>
<head>
<style>
	hr {
  page-break-after: always;
  border: 0;
  margin: 0;
  padding: 0;
}
</style>
</head>
<body>
<?php
for($i=0;$i<count($polizas);$i++)
{
	
?>
	<table width="100%" cellspacing="0px">
    	<tr>
        	<td style="text-align:left; width:20%; vertical-align:baseline;"><table width="100%" cellspacing="0px"><tr><td width="100%"><img src="../../<?php echo $imagenPDFPrincipal;?>" width="120px"></td></tr></table></td>
            <td style="text-align:center; width:40%;"></td>
            <td style="text-align:center; width:13.3%;"></td>
            <td style="text-align:right; width:26.6.3%; vertical-align:baseline;" >
            	<table  width="100%" cellspacing="0px" border="1px">
                	<thead>
                        <tr>
                            <th style=" font-family:Arial; font-size:12; text-align:center; background-color:#4BCB69; color:#FFFFFF;">Fecha</th>
                            <th style=" font-family:Arial; font-size:12; text-align:center; background-color:#4BCB69; color:#FFFFFF;">N&uacute;mero</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style=" font-family:Arial; font-size:12; text-align:center;"><?php echo $polizas[$i]['fecha'];?></td>
                            <td  style=" font-family:Arial; font-size:12; text-align:center;"><?php echo $polizas[$i]['tipo_poliza']."-".
																									$polizas[$i]['poliza']."-".
																									$polizas[$i]['folio'];?></td>
                        </tr>
                    </tbody>
                </table>
            </td>
       	</tr>
        <tr>
        	<td colspan="4" style=" font-family:Arial; font-size:14; font-weight:bold;">POLIZA</td>
        </tr>
        <tr>
        	<td colspan="4" style=" font-family:Arial; font-size:14; font-weight:bold;"><?php echo $polizas[$i]['nomtippoliza'];?></td>
        </tr>
    </table>
	<table width="100%" cellspacing="0px">
    	<thead>
        	<tr>
            	<th style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px; border-right:#000000 solid 1px;  border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:20%; background-color:#4BCB69; color:#FFFFFF;">Cuenta</th>
                <th style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:40%; background-color:#4BCB69; color:#FFFFFF;">Nombre</th>
                <th style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:13.3%; background-color:#4BCB69; color:#FFFFFF;">Referencia</th>
                <th style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:13.3%; background-color:#4BCB69; color:#FFFFFF;">Debe</th>
                <th style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:13.3%; background-color:#4BCB69; color:#FFFFFF;">Haber</th>
            </tr>
        </thead>
        <tbody>
        <?php 
		$sumCargo=0.00;
		$sumCredito=0.00;
		for($j=0; $j<count($polizas[$i]['dpoliza']);$j++)
		{
			$cargo = (float)str_replace(",","",trim($polizas[$i]['dpoliza'][$j]['cargo']));
			$credito = (float)str_replace(",","",trim($polizas[$i]['dpoliza'][$j]['credito']) );
			$sumCargo=	$sumCargo+$cargo;
			$sumCredito=$sumCredito+$credito;
		?>
        	<tr>
        		<td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px; border-right:#000000 solid 1px; text-align:center;"><?php echo$polizas[$i]['dpoliza'][$j]['cuenta'];?></td>
        		<td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; "><?php echo $polizas[$i]['dpoliza'][$j]['nombre'];?></td>
        		<td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; text-align:center;"><?php echo $polizas[$i]['dpoliza'][$j]['referencia'];?></td>
        		<td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; text-align:right;"><?php echo $polizas[$i]['dpoliza'][$j]['cargo'];?></td>
        		<td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; text-align:right;"><?php echo $polizas[$i]['dpoliza'][$j]['credito'];?></td>
            </tr>
        <?php
			//echo floatval($polizas[$i]['dpoliza'][$j]['cargo']);
			
		}
		
		
		?>
            <tr>
            	<td style=" font-family:Arial; font-size:12; border-top:#000000 solid 1px;">&nbsp;</td>
        		<td style=" font-family:Arial; font-size:12; border-top:#000000 solid 1px;">&nbsp;</td>
        		<td style=" font-family:Arial; font-size:12; border-top:#000000 solid 1px; border-right:#000000 solid 1px;">&nbsp;</td>
        		<td style=" font-family:Arial; font-size:12; border-top:#000000 solid 1px;border-right:#000000 solid 1px; border-bottom:#000000 solid 1px; text-align:right;"><?php echo number_format($sumCargo,2,".",",");?></td>
        		<td style=" font-family:Arial; font-size:12; border-top:#000000 solid 1px; border-right:#000000 solid 1px; border-bottom:#000000 solid 1px; text-align:right;"><?php echo number_format($sumCredito,2,".",",");?></td>
            </tr>
        </tbody>
        <tr>
            	<td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
        </tr>
        <tfoot>
            <tr>
            	<td  style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px; border-right:#000000 solid 1px;  border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:20%;">&nbsp;</td>
                <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:40%;">&nbsp;</td>
                <td  colspan="2" style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:13.3%;">&nbsp;</td>
                <td   style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:13.3%;">&nbsp;</td>
            </tr>
        	<tr>
            	<td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px; border-right:#000000 solid 1px;  border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; text-align:center;">ELABORO</td>
                <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; text-align:center;">REVISO</td>
                <td colspan="2" style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; text-align:center;">AUTORIZO</td>
                <td   style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px; border-top:#000000 solid 1px; border-bottom:#000000 solid 1px; width:13.3%;">&nbsp;</td>
            </tr>
        </tfoot>
    </table>
    <hr>
<?php
}
?>
</body>
</html>