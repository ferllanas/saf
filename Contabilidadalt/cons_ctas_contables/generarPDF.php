<?php

// Vamos a mostrar un PDF
header('Content-type: application/pdf');
// Se llamar� downloaded.pdf
header('Content-Disposition: attachment; filename="downloaded.pdf"');
header("Pragma: no-cache");
header("Expires: 0");

require_once("../../dompdf/dompdf_config.inc.php");
$table=$_POST['datos_a_enviar'];
$table=str_replace("color:#FFF","color:black", $table);
$table=str_replace("background:#006600","background:gren", $table);
$table=str_replace('class="fila"','class="filaPDF"', $table);

$HTML='<html>
<head>
<title>Auxiliar de Cuentas Contables</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
</head>
<body width="90%" align="center">
<table  width="90%">
	<tr>
	<td><p><span class="seccionNota"><strong>Auxiliar de Cuentas Contables </strong></span></p></td>
	<td align="right" style="vertical-align: top">'.date("d/m/Y").'</td></tr>
	<tr><td colspan="2"><hr class="hrTitForma"></td></tr>
	</tr>
</table>
'.$table.'
</body>
</html>';
$dompdf = new DOMPDF();
$dompdf->set_paper('legal', "landscape");
$dompdf->load_html($HTML);

$dompdf->render();
	  
$pdf = $dompdf->output(); 

$dompdf->stream("downloaded.pdf");

?>