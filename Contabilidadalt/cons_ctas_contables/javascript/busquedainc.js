//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectcuenta() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqcuenta = getXmlHttpRequestObjectcuenta(); 

function searchcuenta() {
    if (searchReqcuenta.readyState == 4 || searchReqcuenta.readyState == 0)
	{
        var str = escape(document.getElementById('cuenta').value);
        searchReqcuenta.open("GET",'php_ajax/queryctaincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqcuenta.onreadystatechange = handleSearchSuggestcuenta;
        searchReqcuenta.send(null);
    } 
} 

//Called when the AJAX response is returned.
function handleSearchSuggestcuenta() {
	
	//alert("aja 1");
    if (searchReqcuenta.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestcuenta')
        ss.innerHTML = '';
		//alert(searchReqcuenta.responseText);
        var str = searchReqcuenta.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOvercuenta(this);" ';
				suggest += 'onmouseout="javascript:suggestOutcuenta(this);" ';
				suggest += "onclick='javascript:setSearchcuenta(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[0]+'@'+tok[1]+'@'+tok[2]+'">' + tok[0] + ' - '+tok[1]+'</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOvercuenta(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutcuenta(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchcuenta(value,clave,abc) 
{
    document.getElementById('cuenta').value = value;
	var tok =clave.split("@");
	document.getElementById('cta').value = tok[0];
	//alert(tok[2]);
	
	document.getElementById('valsalini').value=tok[2];
	document.getElementById('sdoini').innerHTML = "Saldo Inicial:" + NumberFormat(tok[2], '2', '.', ',');
	
	//document.getElementById('disponible').value = tok[2];
	document.getElementById('search_suggestcuenta').innerHTML = '';
}
///////////////////////////////////////////////////////////////////////////////////////////////////////


function busca_polizas()
{
	var mesi1 = document.getElementById('mesi').value;
	var mesf1 = document.getElementById('mesf').value;
	var cta1 = document.getElementById('cta').value;
	var anio1 = document.getElementById('anio').value;
	var totcar = 0;
	var totcre = 0;

	//alert('php_ajax/trae_polizas.php?mesi='+mesi+"&mesf="+mesf+"&cta="+cta+"&anio="+anio);
/*	new Ajax.Request('php_ajax/trae_polizas.php?mesi='+mesi+"&mesf="+mesf+"&cta="+cta+"&anio="+anio,
					 
					 
					 {onSuccess : function(resp) 
					 {
*/
	var datas = {
					mesi: mesi1,
					mesf: mesf1,
					cta: cta1,
					anio: anio1
				};
	
	console.log(datas['mesi'])
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/trae_polizas.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
							console.log(resp);
							if( resp.length==0 )
							{
								alert("No existen resultados para estos criterios.");
							}
							if( resp.length>0 ) 
							{
								var tabla=  document.getElementById('datos');
								if(tabla.length<=0){
									alert("No encuentra tabla");
									return false;
								}
								var numren = tabla.rows.length;
								//alert(numren);
								for(var i = numren-1; i >=0; i--)
								{
									tabla.deleteRow(i);
								}
								//alert(resp.responseText);
								//var myArray = eval(resp.responseText);
								//alert("22 "+resp.responseText);
								//alert(myArray.length);
								if(resp.length>0)
								{
									for(var i = 0; i <resp.length; i++)
									{
										//alert(resp[i]['fecha']);
										var newrow = tabla.insertRow(i);
										newrow.className ="fila";
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.width="70";
										newcell.innerHTML =  resp[i]['fecha'];//"";//
										newcell.align ="center";
										
										var newcell2 = newrow.insertCell(1);
										newcell2.width="103";
										newcell2.innerHTML = resp[i].tipo;
										newcell2.align ="left";  
										
										var newcell3 = newrow.insertCell(2);
										newcell3.width="127";
										newcell3.innerHTML = resp[i].referencia;
										newcell3.align ="left"; 

										var newcell4 = newrow.insertCell(3);
										newcell4.width="328";
										newcell4.innerHTML = resp[i].concepto;
										newcell4.align ="left"; 
										
										var newcell5 = newrow.insertCell(4);
										newcell5.width="97";
										newcell5.innerHTML = resp[i].cargo;
										newcell5.align ="right"; 
										
										var newcell6 = newrow.insertCell(5);
										newcell6.width="107";
										newcell6.innerHTML =resp[i].credito;
										newcell6.align ="right"; 
										
										totcar=totcar + parseFloat(resp[i].cargo.replace(/,/g,''));
										totcre=totcre + parseFloat(resp[i].credito.replace(/,/g,''));
										document.getElementById('cargos').innerHTML="Cargos:"+NumberFormat(totcar, '2', '.', ',');
										document.getElementById('creditos').innerHTML="Creditos:"+NumberFormat(totcre, '2', '.', ',');
										var sdofin= parseFloat(resp[i].sdo_inicial.replace(/,/g,''));
									}
								 //var sdofin=parseInt();
								var salini = sdofin;
								//console.log(salini);
								var strsalini = NumberFormat(salini, '2', '.', ',');
								console.log(strsalini);
								document.getElementById('sdoini').innerHTML = "Saldo inicial:" +  strsalini;
								
								sdofin = sdofin + totcar - totcre;
								var Strfinal = NumberFormat(sdofin, '2', '.', ',')
								document.getElementById('sdofinal').innerHTML = "Saldo Final:"+ Strfinal;

										
								}

								else
								{
									//alert("No existen resultados para estos criterios.");
								}
						}
					  }
				});
}


function busca_polizas_niv()
{
	var mesi1 = document.getElementById('mesi').value;
	var mesf1 = document.getElementById('mesf').value;
	var cta1 = document.getElementById('cta').value;
	var anio1 = document.getElementById('anio').value;
	var totcar = 0;
	var totcre = 0;

	//alert('php_ajax/trae_polizas.php?mesi='+mesi+"&mesf="+mesf+"&cta="+cta+"&anio="+anio);
/*	new Ajax.Request('php_ajax/trae_polizas.php?mesi='+mesi+"&mesf="+mesf+"&cta="+cta+"&anio="+anio,
					 
					 
					 {onSuccess : function(resp) 
					 {
*/
	var datas = {
					mesi: mesi1,
					mesf: mesf1,
					cta: cta1,
					anio: anio1
				};
	
	//alert(datas['mesi'])
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/trae_polizas.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
							//console.log(resp);
							if( resp.length==0 )
							{
								alert("No existen resultados para estos criterios.");
							}
							if( resp.length>0 ) 
							{
								var tabla=  document.getElementById('datos');
								if(tabla.length<=0){
									alert("No encuentra tabla");
									return false;
								}
								var numren = tabla.rows.length;
								//alert(numren);
								for(var i = numren-1; i >=0; i--)
								{
									tabla.deleteRow(i);
								}
								//alert(resp.responseText);
								//var myArray = eval(resp.responseText);
								//alert("22 "+resp.responseText);
								//alert(myArray.length);
								if(resp.length>0)
								{
									for(var i = 0; i <resp.length; i++)
									{
										//alert(resp[i]['fecha']);
										var newrow = tabla.insertRow(i);
										newrow.className ="fila";
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.width="70";
										newcell.innerHTML =  resp[i]['fecha'];//"";//
										newcell.align ="center";
										
										var newcell2 = newrow.insertCell(1);
										newcell2.width="103";
										newcell2.innerHTML = resp[i].tipo;
										newcell2.align ="left"; 
										
										var newcell3 = newrow.insertCell(2);
										newcell3.width="103";
										newcell3.innerHTML = resp[i].cuenta;
										newcell3.align ="left"; 
										
										var newcell4 = newrow.insertCell(3);
										newcell4.width="127";
										newcell4.innerHTML = resp[i].referencia;
										newcell4.align ="left"; 

										var newcell5 = newrow.insertCell(4);
										newcell5.width="328";
										newcell5.innerHTML = resp[i].concepto;
										newcell5.align ="left"; 
										
										var newcell6 = newrow.insertCell(5);
										newcell6.width="97";
										newcell6.innerHTML = resp[i].cargo;
										newcell6.align ="right"; 
										
										var newcell7 = newrow.insertCell(6);
										newcell7.width="107";
										newcell7.innerHTML =resp[i].credito;
										newcell7.align ="right"; 
										
										totcar=totcar + parseFloat(resp[i].cargo.replace(/,/g,''));
										totcre=totcre + parseFloat(resp[i].credito.replace(/,/g,''));
										document.getElementById('cargos').innerHTML="Cargos:"+NumberFormat(totcar, '2', '.', ',');
										document.getElementById('creditos').innerHTML="Creditos:"+NumberFormat(totcre, '2', '.', ',');
										var sdofin= parseFloat(resp[i].sdo_inicial.replace(/,/g,''));
									}
								 //var sdofin=parseInt();
								var salini = sdofin;
								//console.log(salini);
								var strsalini = NumberFormat(salini, '2', '.', ',');
								console.log(strsalini);
								document.getElementById('sdoini').innerHTML = "Saldo inicial:" +  strsalini;
								
								sdofin = sdofin + totcar - totcre;
								var Strfinal = NumberFormat(sdofin, '2', '.', ',')
								document.getElementById('sdofinal').innerHTML = "Saldo Final:"+ Strfinal;

										
								}

								else
								{
									//alert("No existen resultados para estos criterios.");
								}
						}
					  }
				});
}
