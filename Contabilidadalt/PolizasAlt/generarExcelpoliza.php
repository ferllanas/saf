<?php
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=Reporte_Polizas.xls");
header("Pragma: no-cache");
header("Expires: 0");


	require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	//require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	//$ffecha=date("Y-m-d");
	$fecha_poliza="";
	$tipo_poliza="";
	$nombreusuario="";
	$tfacturas="";
	$fecha="";
	$conceptos="";
	$numero_poliza="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$partidas="";//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$descripcion="";
	$id_facts="";
	$fecpoliza="";
	$folio="";
	
	
	//print_r($partidas);
	//if(isset($_REQUEST['fecha_poliza']))
	//	$fecha_poliza=$_POST['fecha_poliza'];
		
	//$fechapolizanormal=$fecha_poliza;
	
	/*if(isset($_REQUEST['tipo_poliza']))
		$tipo_poliza=$_POST['tipo_poliza'];

	if(isset($_REQUEST['numero_poliza']))
		$numero_poliza=$_POST['numero_poliza'];
		*/
	if(isset($_REQUEST['tipo_poliza']))
		$tipo_poliza=$_REQUEST['tipo_poliza'];

	if(isset($_REQUEST['numero_poliza']))
		$idmpoliza=$_REQUEST['numero_poliza'];
	
	$command="SELECT id,CONVERT(varchar(10),fecha,103) as fecha,descrip, poliza, folio FROM contabmpoliza_alt WHERE id =$idmpoliza and tpoliza='$tipo_poliza' and estatus<'9000'";
	
	//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				//echo "MAMA";
				$fecha_poliza=$row['fecha'];
				$descripcion=utf8_decode($row['descrip']);
				$numero_poliza=$row['poliza'];
				$folio=$row['folio'];
			}
		}
	//$idmpoliza = convertirFechaEuropeoAAmericano($idmpoliza);

	//echo $fecha_poliza;
	$fecha_polizax=convertirFechaEuropeoAAmericano($fecha_poliza);
	//echo "<br>".$fecha_polizax;
	$command="SELECT * FROM contabdpoliza_alt WHERE  id_mpoliza =$idmpoliza AND tpoliza='$tipo_poliza' and estatus<'9000'";
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
		
				$tfacturas.="<tr><td  style='width: 15%;' align='center'>".$fecha_poliza.
				"</td><td  style='width: 15%;' align='center'>".$row['cuenta'].
				"</td><td style='width: 35%;'>".utf8_encode($row['concepto']).
				"</td><td style='width: 10%;' align='center' style=\"mso-number-format:'\@'\">".$row['referencia'].
				"</td><td  style='width: 15%;' align='center'>".number_format($row['cargo'],2).
				"</td><td style='width: 15%;' align='center'>".number_format($row['credito'],2)."</td></tr>";//<td class='texto11'>".trim($fecha_poliza)."</td>
				
				$totalCredito+= $row['credito'];
				$totalCargos+= $row['cargo'];	
			}
			$totcredito=number_format($totalCredito,2);
			$totcargos=number_format($totalCargos,2);
			list($days,$month,$years)= explode('/', $fecha_polizax);
			$diadesem=date("w", strtotime("$years-$month-$days")); //date("w");
			$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";							
/*	$dompdf = new DOMPDF();
		$pageheadfoot='<script type="text/php"> 
		if ( isset($pdf) ) 
		{ 	
			$pdf->page_text(30, 760, "Pagina {PAGE_NUM}", "", 10, array(0,0,0)); 
		} 
</script> ';  
	
	$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=latin1'>
<style>
body 
{
	font-family:'Arial';
	font-size:8;
}

table {
  
	vertical-align:top;
	border-collapse:collapse; 
	border: none;
}
td {padding: 0;}

#twmaarco
{
	border: 1pt solid black;
}

.textobold{
	font-weight:bold;
}
.texto14{ 
	font-size: 15pt;
	font-weight:bold;
}

.texto12{ 
	font-size: 12pt;
}
.texto12bold
{
	font-size: 12pt;
	font-weight:bold;
}
.texto11{
	font-size: 11pt;
}
</style>

</head>

<body>
*/
$htmlstr="<table style='width: 90%;margin-top: 2em; margin-left: 2em; margin-right: 3em;' width='100%'>
	<tr>
		<td style='width: 100%;' width='100%'>
			<table style='width: 100%;' align='center'>
				
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 50%; vertical-align:top;' align='left' class='texto12'>CAPTURA DE POLIZA No. $tipo_poliza-$numero_poliza-$folio</td>
								<td align='right' class='texto12'>".utf8_decode("Fecha de P&oacute;liza").":    ".utf8_decode($ffecha)." ".$hora."</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td  align='left'>Concepto: $descripcion</td>
				</tr>
				<tr>
				<td colspan='2'>&nbsp;</td>
				<tr>
                	<td style='width: 100%;' colspan='2'>
						<table style='width: 100%;' >
							<thead>
								<tr>
									<th style='width: 15%;' >Fecha</th>
									<th style='width: 15%;' >Cuenta</th>
									<th style='width: 35%;' >Concepto</th>
									<th style='width: 10%;' >Referencia</th>
									<th style='width: 15%;' >Cargos</th>
									<th style='width: 15%;' >Creditos</th>
								</tr>
								<tr>
									<td colspan='5'><hr></td>
								</tr>
							</thead>
							<tbody>$tfacturas</tbody>
						   <tfoot>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td colspan='3'><hr></td>
								</tr>
								<tr>
									<td style='width: 10%;' >&nbsp;</td>
									<td style='width: 40%;' >&nbsp;</td>
									<td style='width: 10%;' align='right'><b>Total</b></td>
									<td style='width: 15%;' align='center'><b>$".$totcargos."</b></td>
									<td style='width: 15%;' align='center'><b>$".$totcredito."</b></td>
								</tr>
							</tfoot>
					   </table>
                   	</td>
                </tr>
				<tr>
					<td colspan='2'>
						<table style='width: 70%;'>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'>$nombreusuario</td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'><HR></td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'><HR></td>
                            </tr>
                            <tr>
                                <td style='width: 40%;' align='center' >CAPTURO</td><td style='width: 20%;'>&nbsp;</td><td style='width: 40%;' align='center'  >Vo. Bo.</td>
                            </tr>
						</table>
					</td>
				</tr>
			   </table>
			   
		</td>
	</tr>

</table>";

echo utf8_decode($htmlstr);
/*".$pageheadfoot."
</body> </html>";
	//echo $htmlstr;
	//$htmlstr=utf8_encode($htmlstr);
	//$dompdf->load_html($htmlstr);
	$dompdf->load_html($htmlstr);
	$dompdf->render();
	$pdf = $dompdf->output();
	
	$tmpfile = tempnam(".", "dompdf_.pdf");
	file_put_contents($tmpfile, $pdf ); // Replace $smarty->fetch()
	rename($tmpfile,$tmpfile.".pdf");
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment; filename="downloaded.pdf"');
	header("Location: ".basename($tmpfile.".pdf"));
	
	$files = glob('*.tmp.pdf'); // get all file names
	foreach($files as $file){ // iterate files
	  if(is_file($file) && basename($tmpfile.".pdf")!= basename($file))
		unlink($file); // delete file
	}*/
}

?>