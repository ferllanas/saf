<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
////
require_once("../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
$BorrarPriv=revisaPrivilegiosBorrar($privilegios);
$VerPDFPriv = revisaPrivilegiosPDF($privilegios);
echo $VerPDFPriv;
//$EditarPriv=revisaPrivilegiosEditar($privilegios);
////
?><html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />    
         <link href="../../css/esperaicon.css" rel="stylesheet" type="text/css" />    
         
        <title>SAF- Solicitud de Cheques</title> <script src="../../cheques/javascript/divhide.js"></script>  
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				
				 var tableNew = $("#Exportar_a_Excel").eq(0).clone();
				 $(tableNew).attr( "id" , "MXS" );
				
				$(tableNew).find('tr').each(function(){
					$(this).find("td:eq(5)").remove();
				});
				
				//$(tableNew).find('td:eq(5)').remove();
				$("#datos_a_enviar").val( $("<div>").append( tableNew ).html());
				$("#FormularioExportacion").submit();
			});
			
		});
		
            $(function()
			{
				$('#div_carga')
					.hide()
					.ajaxStart(function() {
						$(this).show();
					})
					.ajaxStop(function() {
						$(this).hide();
					})
				;
				
               /*$('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						console.log('poliza_busqueda_ajax.php'+data);
						$.post('poliza_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						console.log(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });*/
            });
			
			function buscarPolizas()
			{
				var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $('#query').val();     // Toma el valor de los datos, que viene del input						
						console.log('poliza_busqueda_ajax.php'+data);
						$.post('poliza_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						console.log(resp);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
			}
			function busca_cheque()
			{
				// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
				var opcion = document.getElementById('opc').value;
				var fecini = document.getElementById('fecini').value;
				var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
				var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
				console.log(data);

				$.post('poliza_busqueda_ajax.php',data, function(resp)
				{ //Llamamos el arch ajax para que nos pase los datos
						
					console.log(resp.response);
                     $('#proveedor').empty();
                       $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                }, 'json');  // Json es una muy buena opcion               
            }
			function cancela_poliza(folio, tipo)
			{
				var pregunta = confirm("¿Esta seguro que desea eliminar la Poliza?")
				if (pregunta)
				{
					location.href="poliza_cancela.php?poliza="+folio+"&tipo="+tipo;
				}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if folio}}
					<!--<td>${folio} </td> <a href="polizas_editar.php?id=${id}">${folio}</a></td>-->
					<td align="center" >${tipo}-${poliza}-${folio}</td>
					<td align="center">${tipo}</td>
					<td align="center">${fecha}</td>
				<!--	<td>${importe}</td>-->
					<td style="mso-number-format:'\@'">${concepto}</td> 
					{{if estatus==0}}
						<td align="center">Capturada</td>
						<td >
							{{if existe=="si"}}
								<?php if($VerPDFPriv){?>
								<img src="../../imagenes/consultar.jpg" onClick="window.open('pdf_files/poliza_'+${id}+'.pdf') "><?php } ?>
							{{else}}
								<img src="../../imagenes/consultar.jpg" onClick="window.open('generarPDFpoliza.php?numero_poliza='+${id}+'&tipo_poliza='+${tipo}) ">
							{{/if}}
						</td>
						<td><?php if($BorrarPriv){?> <img src="../../imagenes/eliminar.jpg" onClick="cancela_poliza(${id},'${tipo}')"><?php } ?></td>
					{{/if}}
					{{if estatus==10}}
						<td align="center">Afectada</td>
						<td>
							{{if existe=="si"}}
								<?php if($VerPDFPriv){?>
								<img src="../../imagenes/consultar.jpg" onClick="window.open('pdf_files/poliza_'+${id}+'.pdf') ">
								<?php } ?>
							{{else}}
								<img src="../../imagenes/consultar.jpg" onClick="window.open('generarPDFpoliza.php?numero_poliza=${id}&tipo_poliza=${tipo}') ">
							{{/if}}
						</td>
						<td>
							<img src="../../imagenes/export_to_excel.gif" class="botonExcel" onclick="window.open('generarExcelpoliza.php?numero_poliza=${id}&tipo_poliza=${tipo}') " />
						</td>
						<!--<td><img src="../../imagenes/eliminar.jpg" onClick="cancela_poliza(${folio},'${tipo}')"></td>-->
					{{/if}}
					{{if estatus==20}}
						<td align="center">Con Orden pago</td>
					{{/if}}
					{{if estatus==9000}}
						<td align="center">Cancelada</td>
						<td><img src="../../imagenes/borrar.jpg" title="El vale fue cancelado."></td>
					{{/if}}

                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
			.num {
			  mso-number-format:General;
			}
			.text{
			  mso-number-format:"\@";/*force text*/
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Búsqueda de Pólizas</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >N&uacute;mero de P&oacute;liza</option>
              <option value="2" >Tipo de P&oacute;liza</option>
              <option value="4" >Descripci&oacute;n</option>              
              <option value="5" >Por rango de fechas</option>
             
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"><input type="button" onClick="buscarPolizas()" value="Buscar" align="center">
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
  
  
  
   

  
  
  
  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
<form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th><p>N&uacute;mero de P&oacute;liza</p></th>                                       
                <!--   <th>Proveedor</th>-->
                <th>Tipo</th> 
				<th>Fecha de P&oacute;liza</th>
                <th>Concepto</th>
                <th>Estado</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
     <div id="div_carga">
   		<img src="../../imagenes/ajax-loader.gif" width="64" id="cargador">
    </div>
    </body>
</html>
