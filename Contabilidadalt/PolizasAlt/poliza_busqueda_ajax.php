<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
$command="";

	//echo $opcion;
	if($opcion==1)
	{
				$command= "select a.id, a.tpoliza, a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha, a.descrip, a.estatus, a.folio from contabmpoliza_alt a  WHERE poliza LIKE '" . substr($_REQUEST['query'],1) . "%' order by poliza";
	}
	if($opcion==2)
	{
				$command= "select a.id, a.tpoliza,a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha, a.descrip, a.estatus, a.folio from contabmpoliza_alt a  WHERE tipo LIKE '%" . substr($_REQUEST['query'],1) . "%' order by poliza";
	}
	
	//echo $command;
	if($opcion==3)
	{
				$command= "select a.id, a.tpoliza, a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha,  a.descrip, a.estatus, a.folio from contabmpoliza_alt a 
								INNER JOIN compramprovs c ON  a.prov=c.prov 
								INNER JOIN nominamdepto b ON a.depto=b.depto 
								WHERE nomdepto LIKE '%" . substr($_REQUEST['query'],1) . "%' order by poliza";
	}		

	if($opcion==4)
 	{
		$descripcion=substr($_REQUEST['query'],1);
		//echo $descripcion;
		$descs=explode(" ",$descripcion);
		//echo count($descs);
		if(count($descs)==1 || count($descs)==0){
				$command= "select a.id, a.tpoliza, a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha , a.descrip, a.estatus, a.folio from contabmpoliza_alt a 
							WHERE descrip LIKE '%" . substr($_REQUEST['query'],1) . "%' order	by poliza";
		}
		else
		{
			if(count($descs)==2)
			{
				$command= "select a.id, a.tpoliza, a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha , a.descrip, a.estatus, a.folio from contabmpoliza_alt a 
								WHERE descrip >='".$descs[0] ."' AND descrip<='".$descs[1]."' order	by poliza"; 
			}
		}

     }

	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select a.id, a.tpoliza, a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha , a.descrip,  a.estatus , a.folio
							from contabmpoliza_alt a  where a.fecha >= '" . $fini . "' and a.fecha <='" . $ffin . "'";
	}	
	
	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco	
					$datos[$i]['id']=trim($row['id']);
					$datos[$i]['folio']=trim($row['folio']);
					$datos[$i]['poliza']=trim($row['poliza']);
					$datos[$i]['fecha']=trim($row['fecha']);					
					//$datos[$i]['nomprov']=trim($row['nomprov']);
					$datos[$i]['tipo']=trim($row['tpoliza']);
					$datos[$i]['concepto']=utf8_decode($row['descrip']);
					$datos[$i]['estatus']=trim($row['estatus']);
					
					$path= 'pdf_files/poliza_'.$datos[$i]['id'].'.pdf';
					if(file_exists($path))
						$datos[$i]['existe']="si";
					else
						$datos[$i]['existe']="no";
						
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>