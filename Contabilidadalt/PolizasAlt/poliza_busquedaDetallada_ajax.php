<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
$command="";

	//echo $opcion;
	if($opcion==1)
	{
				//a.id, a.tpoliza, a.poliza, CONVERT(varchar(12),a.fecha, 103) as fecha, a.descrip, a.estatus, a.folio
				$command= "select b.*, CONVERT(varchar(12),a.fecha, 103)  as fechampoliza, a.poliza, a.folio, a.descrip, a.tpoliza 
									from contabmpoliza_alt a 
									LEFT JOIN contabdpoliza_alt b ON a.id= b.id_mpoliza AND a.tpoliza= b.tpoliza
								 WHERE poliza LIKE '" . substr($_REQUEST['query'],1) . "%' order by a.poliza";
	}
	if($opcion==2)
	{
				$command= "select b.*, CONVERT(varchar(12),a.fecha, 103)  as fechampoliza, a.poliza, a.folio, a.descrip, a.tpoliza  
									from contabmpoliza_alt a 
									LEFT JOIN contabdpoliza_alt b ON a.id= b.id_mpoliza AND a.tpoliza= b.tpoliza
									WHERE tipo LIKE '%" . substr($_REQUEST['query'],1) . "%' order by a.poliza";
	}
	
	//echo $command;
	if($opcion==3)
	{
				$command= "select b.*, CONVERT(varchar(12),a.fecha, 103)  as fechampoliza, a.poliza, a.folio, a.descrip, a.tpoliza  
								from contabmpoliza_alt a 
									LEFT JOIN contabdpoliza_alt b ON a.id= b.id_mpoliza AND a.tpoliza= b.tpoliza
								INNER JOIN compramprovs c ON  a.prov=c.prov 
								INNER JOIN nominamdepto b ON a.depto=b.depto 
								WHERE nomdepto LIKE '%" . substr($_REQUEST['query'],1) . "%' order by a.poliza";
	}		

	if($opcion==4)
 	{
		$descrip=substr($_REQUEST['query'],1);
		//echo $descrip;
		$descs=explode(" ",$descrip);
		//echo count($descs);
		if(count($descs)==1 || count($descs)==0){
				$command= "select b.*, CONVERT(varchar(12),a.fecha, 103)  as fechampoliza, a.poliza, a.folio, a.descrip, a.tpoliza  
							from contabmpoliza_alt a 
							LEFT JOIN contabdpoliza_alt b ON a.id= b.id_mpoliza AND a.tpoliza= b.tpoliza
							WHERE descrip LIKE '%" . substr($_REQUEST['query'],1) . "%' order	by a.poliza";
		}
		else
		{
			if(count($descs)==2)
			{
				$command= "select b.*, CONVERT(varchar(12),a.fecha, 103)  as fechampoliza, a.poliza, a.folio, a.descrip, a.tpoliza  
									from contabmpoliza_alt a 
									LEFT JOIN contabdpoliza_alt b ON a.id= b.id_mpoliza AND a.tpoliza= b.tpoliza
								WHERE descrip >='".$descs[0] ."' AND descrip<='".$descs[1]."' order	by a.poliza"; 
			}
		}

     }

	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select b.*, CONVERT(varchar(12),a.fecha, 103)  as fechampoliza, a.poliza, a.folio, a.descrip, a.tpoliza  
									from contabmpoliza_alt a 
									LEFT JOIN contabdpoliza_alt b ON a.id= b.id_mpoliza AND a.tpoliza= b.tpoliza
									where a.fecha >= '" . $fini . "' and a.fecha <='" . $ffin . "'";
	}	
	
	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					$datos[$i]['poliza'] = $row['poliza'];
					$datos[$i]['tipo'] = $row['tpoliza'];
					$datos[$i]['folio'] = $row['folio'];
					/*$datos[$i]['concepto'] = $row['descrip'];*/
					$datos[$i]['fecha'] = $row['fechampoliza'];
					$datos[$i]['cuenta'] = $row['cuenta'];
					$datos[$i]['concepto'] = utf8_encode($row['concepto']);
					$datos[$i]['referencia'] = $row['referencia'];
					$datos[$i]['cargo'] = number_format($row['cargo'],2);
					$datos[$i]['credito'] = number_format($row['credito'],2);//<td class='texto11'>".trim($fecha_poliza)."</td>
					
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>