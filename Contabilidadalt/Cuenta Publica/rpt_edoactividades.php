<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <title>SAF- Solicitud de Cheques</title>
        <script src="javascript/navegacion.js"></script>
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
				$("#FormularioExportacion").submit();
		});
		});
            $(function()
			{
                /*$('#query').live('keydown', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('poliza_reporte_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							console.log(resp);
							$('#EYE_A1').html(resp['111']['carfin']);
						//alert(resp.response);
                        //$('#proveedor').empty();
                        //$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });*/
            });
			
			function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}
	function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
	function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						//var opcion = document.getElementById('opc').value;
						var anio = document.getElementById('anio').value;
						var mes = document.getElementById('mes').value;
						var mes_2 = document.getElementById('mes_2').value;
						//var cta = document.getElementById('numcuentacompleta').value;	  	
						if(mes.length==1)
						{
							mes="0" + mes;
						}	
						if(mes_2.length==1)
						{
							mes_2="0" + mes_2;
						}	
						var data = 'query=' + anio + mes + mes_2;     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$('#diaini').html(anio +'/'+ mes + " al " + anio +'/'+ mes_2);
						$('#per1').html(anio +'/'+ mes + " - "+ mes_2);
						$('#per1_1').html(anio +'/'+ mes + " - "+ mes_2);
						$.post('balanzaGeneral_act_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							console.log(resp);
							var C52410 = objectFindByKey(resp, 'cuenta',  '52410' );
							$("#ayudasSociales_01").html(C52410.carfin);
							var ayudasSociales_01= parseFloat(C52410.carfin.replace(/,/g, ""));
							$("#REA_O36_01").html(numberWithCommas(ayudasSociales_01.toFixed(2)));
							
							var v422_1 = objectFindByKey(resp, 'cuenta',  '422' );
							$('#422_1').html(v422_1.crefin);
							$('#PAT_1').html(v422_1.crefin);
							
							var v417_1 = objectFindByKey(resp, 'cuenta',  '417' );
							$('#417_1').html(v417_1.crefin);		
							$('#IG_1').html(v417_1.crefin);
							
							
							var C43121_1 = objectFindByKey(resp, 'cuenta',  '43121.1' );
							var C43121_2 = objectFindByKey(resp, 'cuenta',  '43121.2' );
							var C43121_3 = objectFindByKey(resp, 'cuenta',  '43121.3' );
							var C43121_4 = objectFindByKey(resp, 'cuenta',  '43121.4' );
							var IF= parseFloat(C43121_1.crefin.replace(/,/g, "")) + parseFloat(C43121_2.crefin.replace(/,/g, "")) +
										parseFloat(C43121_3.crefin.replace(/,/g, "")) + parseFloat(C43121_4.crefin.replace(/,/g, "")) ;
							$('#IF_1').html(numberWithCommas(IF.toFixed(2)));
							
							var C43590_1 = objectFindByKey(resp, 'cuenta',  '43590.1' );
							var C43590_2 = objectFindByKey(resp, 'cuenta',  '43590.2' );
							var C43590_3 = objectFindByKey(resp, 'cuenta',  '43590.3' );
							var C43600_4 = objectFindByKey(resp, 'cuenta',  '43600.4' );
							var OIF= parseFloat(C43590_1.crefin.replace(/,/g, "")) + parseFloat(C43590_2.crefin.replace(/,/g, "")) +
										parseFloat(C43590_3.crefin.replace(/,/g, "")) + parseFloat(C43600_4.crefin.replace(/,/g, "")) ;
							$('#OIB_1').html(numberWithCommas(OIF.toFixed(2)));
							
							var OI= parseFloat(IF) + parseFloat(OIF);
							$('#OI_1').html(numberWithCommas(OI.toFixed(2)));
							
							var TIB= parseFloat(v422_1.crefin.replace(/,/g, "")) + parseFloat(v417_1.crefin.replace(/,/g, ""))+ parseFloat(IF) + parseFloat(OIF);
							$('#TIB_1').html(numberWithCommas(TIB.toFixed(2)));
							
							
							var C5111 = objectFindByKey(resp, 'cuenta',  '5111' );
							var C5112 = objectFindByKey(resp, 'cuenta',  '5112' );
							var C5113 = objectFindByKey(resp, 'cuenta',  '5113' );
							var C5114 = objectFindByKey(resp, 'cuenta',  '5114' );
							var C5115 = objectFindByKey(resp, 'cuenta',  '5115' );
							var C5117 = objectFindByKey(resp, 'cuenta',  '5117' );
							var SP= parseFloat(C5111.carfin.replace(/,/g, "")) + parseFloat(C5112.carfin.replace(/,/g, "")) +
										parseFloat(C5113.carfin.replace(/,/g, "")) + parseFloat(C5114.carfin.replace(/,/g, "")) +
										parseFloat(C5115.carfin.replace(/,/g, "")) + parseFloat(C5117.carfin.replace(/,/g, ""));
							$('#SP_1').html(numberWithCommas(SP.toFixed(2)));
							
							
							var C5121 = objectFindByKey(resp, 'cuenta',  '5121' );
							var C5122 = objectFindByKey(resp, 'cuenta',  '5122' );
							var C5124 = objectFindByKey(resp, 'cuenta',  '5124' );
							var C5125 = objectFindByKey(resp, 'cuenta',  '5125' );
							var C5126 = objectFindByKey(resp, 'cuenta',  '5126' );
							var C5127 = objectFindByKey(resp, 'cuenta',  '5127' );
							var C5129 = objectFindByKey(resp, 'cuenta',  '5129' );
							var MS= parseFloat(C5121.carfin.replace(/,/g, "")) + parseFloat(C5122.carfin.replace(/,/g, "")) +
										parseFloat(C5124.carfin.replace(/,/g, "")) + parseFloat(C5125.carfin.replace(/,/g, "")) +
										parseFloat(C5126.carfin.replace(/,/g, "")) + parseFloat(C5127.carfin.replace(/,/g, "")) +
										parseFloat(C5129.carfin.replace(/,/g, ""));
							$('#MS_1').html(numberWithCommas(MS.toFixed(2)));
							
							var C5131 = objectFindByKey(resp, 'cuenta',  '5131' );
							var C5132 = objectFindByKey(resp, 'cuenta',  '5132' );
							var C5133 = objectFindByKey(resp, 'cuenta',  '5133' );
							var C5134 = objectFindByKey(resp, 'cuenta',  '5134' );
							var C5135 = objectFindByKey(resp, 'cuenta',  '5135' );
							var C5136 = objectFindByKey(resp, 'cuenta',  '5136' );
							var C5137 = objectFindByKey(resp, 'cuenta',  '5137' );
							var C5138 = objectFindByKey(resp, 'cuenta',  '5138' );
							var C5139 = objectFindByKey(resp, 'cuenta',  '5139' );
							var C51398_1 = 0.00;//objectFindByKey(resp, 'cuenta',  '51398.1' );
							var SG= parseFloat(C5131.carfin.replace(/,/g, "")) + parseFloat(C5132.carfin.replace(/,/g, "")) +
										parseFloat(C5133.carfin.replace(/,/g, "")) + parseFloat(C5134.carfin.replace(/,/g, "")) +
										parseFloat(C5135.carfin.replace(/,/g, "")) + parseFloat(C5136.carfin.replace(/,/g, "")) +
										parseFloat(C5137.carfin.replace(/,/g, "")) + parseFloat(C5138.carfin.replace(/,/g, "")) +
										parseFloat(C5139.carfin.replace(/,/g, "")) ;//+ parseFloat(C51398_1.carfin.replace(/,/g, ""))
							$('#SG_1').html(numberWithCommas(SG.toFixed(2)));
							var GF= parseFloat(SP) + parseFloat(MS) + parseFloat(SG);
							//alert(numberWithCommas(GF.toFixed(2)));
							$('#GF_1').html(numberWithCommas(GF.toFixed(2)));
									 
							var IDP = objectFindByKey(resp, 'cuenta',  '54210.1' );
							$('#IDP_1').html(IDP.carfin);
							$('#ICOG_1').html(IDP.carfin);
							
							
							var C551 = objectFindByKey(resp, 'cuenta',  '551' );
							$('#EDD_1').html(C551.carfin);
							var C5531 = objectFindByKey(resp, 'cuenta',  '5531' );
							$('#DI_1').html(C5531.carfin);
							var C55690_1 = objectFindByKey(resp, 'cuenta',  '55690.1' );
							$('#OG_1').html(C55690_1.carfin);
							var OGP= parseFloat(C551.carfin.replace(/,/g, "")) + parseFloat(C5531.carfin.replace(/,/g, "")) +
										parseFloat(C55690_1.carfin.replace(/,/g, ""));
							$('#OGP_1').html(numberWithCommas(OGP.toFixed(2)));
							
							var TGP = parseFloat(OGP) + parseFloat(GF) + parseFloat(IDP.carfin.replace(/,/g, "")) +ayudasSociales_01;
							$('#TGP_1').html(numberWithCommas(TGP.toFixed(2)));
							
							var RE = parseFloat(TIB) - parseFloat(TGP) ;
							$('#RE_1').html(numberWithCommas(RE.toFixed(2)));
							
							
							//ayudasSociales_01= C52410.carfin
							//SEGUNDO LLAMADO
							var anio = document.getElementById('anio2').value;
							var mes = document.getElementById('mes2').value;
							var mes_2 = document.getElementById('mes2_2').value;
							//var cta = document.getElementById('numcuentacompleta').value;	  	
							if(mes.length==1)
							{
								mes="0" + mes;
							}	
							if(mes_2.length==1)
							{
								mes_2="0" + mes_2;
							}	
							var data = 'query=' + anio + mes + mes_2; 
							$('#mesl').html(anio +'/'+ mes + " al " + anio +'/'+ mes_2);
							$('#per2').html(anio +'/'+ mes + " - " +  mes_2);
							$('#per2_1').html(anio +'/'+ mes + " - " + mes_2);
							$.post('balanzaGeneral_act_ajax.php',data, function(resp)
							{ //Llamamos el arch ajax para que nos pase los datos
						
									console.log(resp);
									
									var C52410 = objectFindByKey(resp, 'cuenta',  '52410' );
									$("#ayudasSociales_02").html(C52410.carfin);
									var ayudasSociales_02= parseFloat(C52410.carfin.replace(/,/g, ""));
									$("#REA_O36_02").html(numberWithCommas(ayudasSociales_02.toFixed(2)));
									
									var v422_1 = objectFindByKey(resp, 'cuenta',  '422' );
									$('#422_2').html(v422_1.crefin);
									$('#PAT_2').html(v422_1.crefin);
									
									var v417_1 = objectFindByKey(resp, 'cuenta',  '417' );
									$('#417_2').html(v417_1.crefin);		
									$('#IG_2').html(v417_1.crefin);
									
									
									var C43121_1 = objectFindByKey(resp, 'cuenta',  '43121.1' );
									var C43121_2 = objectFindByKey(resp, 'cuenta',  '43121.2' );
									var C43121_3 = objectFindByKey(resp, 'cuenta',  '43121.3' );
									var C43121_4 = objectFindByKey(resp, 'cuenta',  '43121.4' );
									var IF= parseFloat(C43121_1.crefin.replace(/,/g, "")) + parseFloat(C43121_2.crefin.replace(/,/g, "")) +
												parseFloat(C43121_3.crefin.replace(/,/g, "")) + parseFloat(C43121_4.crefin.replace(/,/g, "")) ;
									$('#IF_2').html(numberWithCommas(IF.toFixed(2)));
									
									var C43590_1 = objectFindByKey(resp, 'cuenta',  '43590.1' );
									var C43590_2 = objectFindByKey(resp, 'cuenta',  '43590.2' );
									var C43590_3 = objectFindByKey(resp, 'cuenta',  '43590.3' );
									var C43600_4 = objectFindByKey(resp, 'cuenta',  '43600.4' );
									var OIF= parseFloat(C43590_1.crefin.replace(/,/g, "")) + parseFloat(C43590_2.crefin.replace(/,/g, "")) +
												parseFloat(C43590_3.crefin.replace(/,/g, "")) + parseFloat(C43600_4.crefin.replace(/,/g, "")) ;
									$('#OIB_2').html(numberWithCommas(OIF.toFixed(2)));
									
									var OI= parseFloat(IF) + parseFloat(OIF);
									$('#OI_2').html(numberWithCommas(OI.toFixed(2)));
									
									var TIB= parseFloat(v422_1.crefin.replace(/,/g, "")) + parseFloat(v417_1.crefin.replace(/,/g, ""))+ parseFloat(IF) + parseFloat(OIF);
									$('#TIB_2').html(numberWithCommas(TIB.toFixed(2)));
									
									
									var C5111 = objectFindByKey(resp, 'cuenta',  '5111' );
									var C5112 = objectFindByKey(resp, 'cuenta',  '5112' );
									var C5113 = objectFindByKey(resp, 'cuenta',  '5113' );
									var C5114 = objectFindByKey(resp, 'cuenta',  '5114' );
									var C5115 = objectFindByKey(resp, 'cuenta',  '5115' );
									var C5117 = objectFindByKey(resp, 'cuenta',  '5117' );
									var SP= parseFloat(C5111.carfin.replace(/,/g, "")) + parseFloat(C5112.carfin.replace(/,/g, "")) +
												parseFloat(C5113.carfin.replace(/,/g, "")) + parseFloat(C5114.carfin.replace(/,/g, "")) +
												parseFloat(C5115.carfin.replace(/,/g, "")) + parseFloat(C5117.carfin.replace(/,/g, ""));
									$('#SP_2').html(numberWithCommas(SP.toFixed(2)));
									
									
									var C5121 = objectFindByKey(resp, 'cuenta',  '5121' );
									var C5122 = objectFindByKey(resp, 'cuenta',  '5122' );
									var C5124 = objectFindByKey(resp, 'cuenta',  '5124' );
									var C5125 = objectFindByKey(resp, 'cuenta',  '5125' );
									var C5126 = objectFindByKey(resp, 'cuenta',  '5126' );
									var C5127 = objectFindByKey(resp, 'cuenta',  '5127' );
									var C5129 = objectFindByKey(resp, 'cuenta',  '5129' );
									var MS= parseFloat(C5121.carfin.replace(/,/g, "")) + parseFloat(C5122.carfin.replace(/,/g, "")) +
												parseFloat(C5124.carfin.replace(/,/g, "")) + parseFloat(C5125.carfin.replace(/,/g, "")) +
												parseFloat(C5126.carfin.replace(/,/g, "")) + parseFloat(C5127.carfin.replace(/,/g, "")) +
												parseFloat(C5129.carfin.replace(/,/g, ""));
									$('#MS_2').html(numberWithCommas(MS.toFixed(2)));
									
									var C5131 = objectFindByKey(resp, 'cuenta',  '5131' );
									var C5132 = objectFindByKey(resp, 'cuenta',  '5132' );
									var C5133 = objectFindByKey(resp, 'cuenta',  '5133' );
									var C5134 = objectFindByKey(resp, 'cuenta',  '5134' );
									var C5135 = objectFindByKey(resp, 'cuenta',  '5135' );
									var C5136 = objectFindByKey(resp, 'cuenta',  '5136' );
									var C5137 = objectFindByKey(resp, 'cuenta',  '5137' );
									var C5138 = objectFindByKey(resp, 'cuenta',  '5138' );
									var C5139 = objectFindByKey(resp, 'cuenta',  '5139' );
									var C51398_1 = 0.00;//objectFindByKey(resp, 'cuenta',  '51398.1' );
									var SG= parseFloat(C5131.carfin.replace(/,/g, "")) + parseFloat(C5132.carfin.replace(/,/g, "")) +
												parseFloat(C5133.carfin.replace(/,/g, "")) + parseFloat(C5134.carfin.replace(/,/g, "")) +
												parseFloat(C5135.carfin.replace(/,/g, "")) + parseFloat(C5136.carfin.replace(/,/g, "")) +
												parseFloat(C5137.carfin.replace(/,/g, "")) + parseFloat(C5138.carfin.replace(/,/g, "")) +
												parseFloat(C5139.carfin.replace(/,/g, "")) ;//+ parseFloat(C51398_1.carfin.replace(/,/g, ""));
									$('#SG_2').html(numberWithCommas(SG.toFixed(2)));
									var GF= parseFloat(SP) + parseFloat(MS) + parseFloat(SG);
									//alert(numberWithCommas(GF.toFixed(2)));
									$('#GF_2').html(numberWithCommas(GF.toFixed(2)));
											 
									var IDP = objectFindByKey(resp, 'cuenta',  '54210.1' );
									$('#IDP_2').html(IDP.carfin);
									$('#ICOG_2').html(IDP.carfin);
									
									
									var C551 = objectFindByKey(resp, 'cuenta',  '551' );
									$('#EDD_2').html(C551.carfin);
									var C5531 = objectFindByKey(resp, 'cuenta',  '5531' );
									$('#DI_2').html(C5531.carfin);
									
									var C55690_1 = objectFindByKey(resp, 'cuenta',  '55690.1' );
									$('#OG_2').html(C55690_1.carfin);
									var OGP= parseFloat(C551.carfin.replace(/,/g, "")) + parseFloat(C5531.carfin.replace(/,/g, "")) +
												parseFloat(C55690_1.carfin.replace(/,/g, ""));
									$('#OGP_2').html(numberWithCommas(OGP.toFixed(2)));
									
									var TGP = parseFloat(OGP) + parseFloat(GF) + parseFloat(IDP.carfin.replace(/,/g, ""))+ ayudasSociales_02;
									$('#TGP_2').html(numberWithCommas(TGP.toFixed(2)));
									
									var RE = parseFloat(TIB) - parseFloat(TGP) ;
									$('#RE_2').html(numberWithCommas(RE.toFixed(2)));
									
									
									
							     // los va colocando en la forma de la tabla
                    }, 'json'); 
							
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion      
						
            }
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left" class="text" style='mso-number-format:"\@";'>${cuenta}</td>
					<td class="text" style='mso-number-format:"\@";'>${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
      
</head>
    <body>
    <span class="TituloDForma">Estado de Actividades</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
</div>
<tr>


<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<table  align="center">
<tr>
<td><!-- Asignamos variables de fechas -->
	<table align="center">
    	<tr>
        		<td>Año inicial:<input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="1"> </td>
                <td>Mes inicial:<input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="2"></td>
                 <td>Mes Final:<input type="text" name="mes_2" id="mes_2" size="2" maxlength="2" tabindex="3"></td>
        </tr>
        <tr>
        		<td>Año inicial:<input type="text"  name="anio2" id="anio2" size="4" maxlength="4" tabindex="4"> </td>
                <td>Mes inicial:<input type="text" name="mes2" id="mes2" size="2" maxlength="2" tabindex="5"></td>
                 <td>Mes Final:<input type="text" name="mes2_2" id="mes2_2" size="2" maxlength="2" tabindex="6"></td>
        </tr>
        <!--<tr>
        		<td>Año final:<input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="1"> </td>
                <td>Mes final:<input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="2"></td>
        </tr>-->
    </table>
 </td>
 <td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()" tabindex="7">
</td>
<td>
<input type="hidden" name="ren" id="ren">
    <form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
<form action="poliza_pdf.php" method="post" target="_blank" id="FormularioExportacionPDF">
    <p>Exportar a PDF  <img src="../../imagenes/pdf.png" class="botonPDF" height="21" /></p>
    <input type="hidden" id="datos_a_enviar_PDF" name="datos_a_enviar_PDF" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel" style="width:100%;" align="center">  
<tr>
<td>
<table  align="center">

<table align="center">
	<tr><td align="center"  style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Cuenta Publica <span id="anio"></span></td></tr>
    <tr><td align="center"  style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Estado de Actividades</td></tr>
    <tr><td align="center"  style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">del <span id="diaini"></span> - <span id="mesl"></span></td></tr>
    <tr><td align="center"  style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">(Pesos)</td></tr>
    <tr><td align="center"  style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Ente Público: FOMENTO METROPOLITANO DE MONTERREY </td></tr>
    <tr>
    	<td>
          <table cellspacing="0" cellpadding="0" id="tableContent" style="border:#000000 solid 1px;width:100%;" align="center">
            <col width="92">
            <col width="320">
            <col width="96" span="2">
            <col width="40">
            <col width="92">
            <col width="320">
            <col width="96" span="2">
            <col width="21">
            <tr>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; " colspan="2" rowspan="2" width="412">CONCEPTO</td>
              <td  style="background-color:#2FDC48; font-family:Arial; font-size:14; "colspan="2" width="192" align="center">Periodo</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14;" width="40">&nbsp;</td>
              <td  style="background-color:#2FDC48; font-family:Arial; font-size:14; "colspan="2" rowspan="2" width="412">CONCEPTO</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; "colspan="2" width="192" align="center">Periodo</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; " width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; " width="96" id="per1"></td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; " width="96" id="per2"></td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; " width="40">&nbsp;</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14;" width="96" id="per1_1"></td>
              <td  style="background-color:#2FDC48; font-family:Arial; font-size:14;" width="96" id="per2_1"></td>
              <td  style="background-color:#2FDC48; font-family:Arial; font-size:14;" width="21">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; font-weight:600;" colspan="2" width="412">INGRESOS Y OTROS BENEFICIOS</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:600;" colspan="2" width="412">GASTOS Y OTRAS PÉRDIDAS</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="5" width="625">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2" width="412"><b>Ingresos de la Gestión:</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; " align="right" width="96" id="IG_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="IG_2"></td>
              <td width="40">&nbsp;</td>
              <td  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; " colspan="2"><b>Gastos de Funcionamiento</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; " align="right" width="96" id="GF_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="GF_2"> </td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Impuestos</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Servicios Personales</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="SP_1"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="SP_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96" id="C1134"></td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96" id="C213"></td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Cuotas y Aportaciones de Seguridad Social</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Materiales y Suministros</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="MS_1">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="MS_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Contribuciones de Mejoras </td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Servicios Generales</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="SG_1"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="SG_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
           
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96" id="C2199">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Derechos</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td colspan="4" width="533">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Productos de Tipo Corriente1</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-style:italic;" colspan="2"><b>Transferencia, Asignaciones, Subsidios y Otras Ayudas</b></td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="REA_O36_01"><b>0.00</b></td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="REA_O36_02"><b>0.00</b></td>
              <td width="21">&nbsp;</td>
            </tr>
             <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Aprovechamientos de Tipo Corriente</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Transferencias Internas y Asignaciones al Sector Público</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="C216">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
             <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Ingresos por Venta de Bienes y Servicios</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="417_1"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="417_2"></td>
              <td width="40">&nbsp;</td>
               <td style=" font-family:Arial; font-size:12;" colspan="2">Transferencias al Resto del Sector Público</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="C216">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
             <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2" width="412">Ingresos no Comprendidos en las Fracciones de la Ley de Ingresos Causados en Ejercicios Fiscales Anteriores Pendientes de Liquidación o Pago</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Subsidios y Subvenciones</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96" id="C216">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96" id="TAC">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
               <td colspan="2">&nbsp;</td>
              <td align="right" width="96" id="C216">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96" id="TAC">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
               <td colspan="2">&nbsp;</td>
              <td align="right" width="96" id="C216">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="5" width="625">&nbsp;</td>
            </tr>
           
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td  style=" font-family:Arial; font-size:12;"  colspan="2">Ayudas Sociales</td>
              <td  style=" font-family:Arial; font-size:12;"  align="right" width="96" id="ayudasSociales_01">0.00</td>
              <td  style=" font-family:Arial; font-size:12;"  align="right" width="96" id="ayudasSociales_02">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2" width="412"><b>Participaciones, Aportaciones, Transferencias, Asignaciones, Subsidios y Otras Ayudas</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="PAT_1">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="PAT_2"></td>
              <td width="40">&nbsp;</td>
              <td  style=" font-family:Arial; font-size:12;" colspan="2">Pensiones y Jubilaciones</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96" id="DxPLP">0.00</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96" ></td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96" >&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td  style=" font-family:Arial; font-size:12;" colspan="2" width="412">Participaciones y Aportaciones</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td  style=" font-family:Arial; font-size:12;" colspan="2">Transferencias a Fideicomisos, Mandatos y Contratos Análogos</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td  style=" font-family:Arial; font-size:12;" colspan="2" width="412">Transferencia, Asignaciones, Subsidios y Otras Ayudas</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96" id="422_1"></td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96" id="422_2"></td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;"  colspan="2">Transferencias a la Seguridad Social</td>
              <td style=" font-family:Arial; font-size:12;"  align="right" width="96" id="PALP">0.00</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td  style=" font-family:Arial; font-size:12;" colspan="2">Donativos</td>
              <td style=" font-family:Arial; font-size:12;"  align="right" width="96" >0.00</td>
              <td  style=" font-family:Arial; font-size:12;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
             <tr>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2" width="412"><b>Otros Ingresos y Beneficios</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="OI_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="OI_2"></td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Transferencias al Exterior</td>
              <td  style=" font-family:Arial; font-size:12" align="right" width="96" id="PALP">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
           
            <tr>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Ingresos Financieros</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="IF_1"></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="IF_2"></td>
              <td width="40">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td align="right" width="96" ></td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td align="right" width="96" ></td>
              <td align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="5" width="625">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Incremento por Variación de Inventarios</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2"><b>Participaciones y Aportaciones</b></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="PALP"><b>0.00</b></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96"><b>0.00</b></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Disminución del Exceso de Estimaciones por Pérdida o</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Participaciones</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="PALP">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Deterioro u Obsolescencia</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Disminución del Exceso de Provisiones</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="40">&nbsp;</td>
               <td style=" font-family:Arial; font-size:12" colspan="2">Aportaciones</td>
               <td style=" font-family:Arial; font-size:12" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12"align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            
            <tr>
              <td style=" font-family:Arial; font-size:12" colspan="2">Otros Ingresos y Beneficios Varios</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="OIB_1"></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="OIB_2"></td>
              <td width="40">&nbsp;</td>
               <td style=" font-family:Arial; font-size:12" colspan="2">Convenios</td>
                <td style=" font-family:Arial; font-size:12" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96" ></td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2" width="412"><b>Total de Ingresos y Otros Beneficios</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="TIB_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="TIB_2"></td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2"><b>Intereses, Comisiones y Otros Gastos de la Deuda Pública</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="ICOG_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="ICOG_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            
            <tr>
              <td colspan="2" width="412">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Intereses de la Deuda Pública</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="IDP_1">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="IDP_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Comisiones de la Deuda Pública</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2" width="412">Gastos de la Deuda Pública</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Costo por Coberturas</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Apoyos Financieros</td>
              <td style=" font-family:Arial; font-size:12 "align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
             <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td>&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2"><b>Otros Gastos y Pérdidas Extraordinarias</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="OGP_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="OGP_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Estimaciones, Depreciaciones, Deterioros, Obsolescencia y Amortizaciones</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="EDD_1"></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="EDD_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Provisiones</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Disminución de Inventarios</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="DI_1"></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="DI_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Aumento por Insuficiencia de Estimaciones por Pérdida o Deterioro y Obsolescencia</td>
             <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Aumento por Insuficiencia de Provisiones</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td width="320">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="96">&nbsp;</td>
              <td width="40">&nbsp;</td>
               <td style=" font-family:Arial; font-size:12" colspan="2">Otros Gastos</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="OG_1"></td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96" id="OG_2"></td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td align="right" width="96">&nbsp;</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
               <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2"><b>Inversión Pública</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" >0.00</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>            
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12" colspan="2">Inversión Pública no Capitalizable</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td style=" font-family:Arial; font-size:12" align="right" width="96">0.00</td>
              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td colspan="5" width="625">&nbsp;</td>
            </tr>
            <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2"><b>Total de Gastos y Otras Pérdidas</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="TGP_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="TGP_2"></td>

              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td colspan="4" width="533">&nbsp;</td>
            </tr>
              <tr>
              <td width="92">&nbsp;</td>
              <td colspan="3" width="512">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" colspan="2"><b>Resultados del Ejercicio (Ahorro/Desahorro)</b></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="RE_1"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" width="96" id="RE_2"></td>

              <td width="21">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" width="604">&nbsp;</td>
              <td width="40">&nbsp;</td>
              <td width="92">&nbsp;</td>
              <td colspan="4" width="533">&nbsp;</td>
            </tr>
            </table></td>
    </tr>
    <tr><td><table>
            <tr>
              <td colspan="8">Bajo    protesta de decir verdad declaramos que los Estados Finzncieros y sus Notas    son razonalmente correctos y responsabilidad del emisor.</td>
             
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td colspan="2" align="center">C. Sergio Alejandro Alanis Marroquín</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="2" align="center">Lic. Eloisa Sanchez Méndez</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td colspan="2" align="center">Director Ejecutivo</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="2" align="center">Encargada del Despacho de los Asuntos de la    Direccion de</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td colspan="2">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="2" align="center">Administracion y Finanzas</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
        </table></td>
    </tr>
    </table>
    </td>
   </tr>

    </body>
</html>
