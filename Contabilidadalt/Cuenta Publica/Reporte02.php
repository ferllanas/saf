<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
             
        <title>SAF- Estado de Situaci&oacute;n Financiera</title>
        <link href="../../cheques/css/style.css" 		rel="stylesheet" >         
		<!--<link href="../../css/estilos.css" 				rel="stylesheet" type="text/css" />  --> 
        <link href="../../css/esperaicon.css" 			rel="stylesheet" type="text/css" />     
        
		<!--<script src="../../javascript/navegacion.js"></script> -->
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
$(document).ready(function() {
	
	$(".botonPDF").click(function(event) {
		
		$("#datos_a_enviar_PDF").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacionPDF").submit();
	});
	
	$(".botonExcel").click(function(event) {
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
	});
	
	
	
	//
	$('#div_carga')
	.hide()
	.ajaxStart(function() {
		$(this).show();
	})
	.ajaxStop(function() {
		$(this).hide();
	});
});
           
			
function objectFindByKey(array, key, value) {
	for (var i = 0; i < array.length; i++) {
		if (array[i][key] === value) {
			return array[i];
		}
	}
	return null;
}
			
function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function busca_cheque()
{
	
		// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
			//var opcion = document.getElementById('opc').value;
			var mesVect=("0","Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio" , "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
			var anio = document.getElementById('anio').value;
			var mes = document.getElementById('mes').value;
			var mes_X = document.getElementById('mes_X').value;
			
			var anio2 = document.getElementById('anio2').value;
			var mes2 = document.getElementById('mes2').value;
			var mes2_X = document.getElementById('mes2_X').value;
			
				
			/*$("#anioX").html(anio);
			$("#diainiX").html("31");
			$("#diainiX").html(mes);
			$("#mesX").html(mesVect[mes2]);
			*/
			
			//var cta = document.getElementById('numcuentacompleta').value;	  	
			if(mes.length==1)
			{
				mes="0" + mes;
			}					
			var data2 = 'query=' + anio2 + mes2 +mes2_X;	
			
			$("#diainiX").html(anio2+ "/" + mes2 + " al "  + mes2_X + " - " +  anio + "/" + mes +" al "+ mes_X);
			
			$("#periodo1").html(anio2+"/"+mes2+" - "+mes2_X);
			$("#periodo2").html(anio+"/"+mes+" - "+mes_X);
			
			$("#periodo1_X").html(anio2+"/"+mes2+" - "+mes2_X);
			$("#periodo2_X").html(anio+"/"+mes+" - "+mes_X);
			
			console.log(data2);
			//return false;
			$.post('balanzaGeneral_act_ajax.php',data2, function(resp)
			{ //Llamamos el arch ajax para que nos pase los datos
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////		ESTADO DE ACTIVIDADES															/////////////////
			
				var C52410 = objectFindByKey(resp, 'cuenta',  '52410' );
				var ayudasSociales= parseFloat(C52410.carfin.replace(/,/g, ""));
				
				var v422_1 = objectFindByKey(resp, 'cuenta',  '422' );
				
				
				var v417_1 = objectFindByKey(resp, 'cuenta',  '417' );
			
				
				var C43121_1 = objectFindByKey(resp, 'cuenta',  '43121.1' );
				var C43121_2 = objectFindByKey(resp, 'cuenta',  '43121.2' );
				var C43121_3 = objectFindByKey(resp, 'cuenta',  '43121.3' );
				var C43121_4 = objectFindByKey(resp, 'cuenta',  '43121.4' );
				var IF= parseFloat(C43121_1.crefin.replace(/,/g, "")) + parseFloat(C43121_2.crefin.replace(/,/g, "")) +
							parseFloat(C43121_3.crefin.replace(/,/g, "")) + parseFloat(C43121_4.crefin.replace(/,/g, "")) ;
				$('#IF_1').html(numberWithCommas(IF.toFixed(2)));
				
				var C43590_1 = objectFindByKey(resp, 'cuenta',  '43590.1' );
				var C43590_2 = objectFindByKey(resp, 'cuenta',  '43590.2' );
				var C43590_3 = objectFindByKey(resp, 'cuenta',  '43590.3' );
				var C43600_4 = objectFindByKey(resp, 'cuenta',  '43600.4' );
				var OIF= parseFloat(C43590_1.crefin.replace(/,/g, "")) + parseFloat(C43590_2.crefin.replace(/,/g, "")) +
							parseFloat(C43590_3.crefin.replace(/,/g, "")) + parseFloat(C43600_4.crefin.replace(/,/g, "")) ;
				
				var OI= parseFloat(IF) + parseFloat(OIF);
				
				var TIB= parseFloat(v422_1.crefin.replace(/,/g, "")) + parseFloat(v417_1.crefin.replace(/,/g, ""))+ parseFloat(IF) + parseFloat(OIF);
				
				
				var C5111 = objectFindByKey(resp, 'cuenta',  '5111' );
				var C5112 = objectFindByKey(resp, 'cuenta',  '5112' );
				var C5113 = objectFindByKey(resp, 'cuenta',  '5113' );
				var C5114 = objectFindByKey(resp, 'cuenta',  '5114' );
				var C5115 = objectFindByKey(resp, 'cuenta',  '5115' );
				var C5117 = objectFindByKey(resp, 'cuenta',  '5117' );
				var SP= parseFloat(C5111.carfin.replace(/,/g, "")) + parseFloat(C5112.carfin.replace(/,/g, "")) +
							parseFloat(C5113.carfin.replace(/,/g, "")) + parseFloat(C5114.carfin.replace(/,/g, "")) +
							parseFloat(C5115.carfin.replace(/,/g, "")) + parseFloat(C5117.carfin.replace(/,/g, ""));
				
				
				var C5121 = objectFindByKey(resp, 'cuenta',  '5121' );
				var C5122 = objectFindByKey(resp, 'cuenta',  '5122' );
				var C5124 = objectFindByKey(resp, 'cuenta',  '5124' );
				var C5125 = objectFindByKey(resp, 'cuenta',  '5125' );
				var C5126 = objectFindByKey(resp, 'cuenta',  '5126' );
				var C5127 = objectFindByKey(resp, 'cuenta',  '5127' );
				var C5129 = objectFindByKey(resp, 'cuenta',  '5129' );
				var MS= parseFloat(C5121.carfin.replace(/,/g, "")) + parseFloat(C5122.carfin.replace(/,/g, "")) +
							parseFloat(C5124.carfin.replace(/,/g, "")) + parseFloat(C5125.carfin.replace(/,/g, "")) +
							parseFloat(C5126.carfin.replace(/,/g, "")) + parseFloat(C5127.carfin.replace(/,/g, "")) +
							parseFloat(C5129.carfin.replace(/,/g, ""));
				
				var C5131 = objectFindByKey(resp, 'cuenta',  '5131' );
				var C5132 = objectFindByKey(resp, 'cuenta',  '5132' );
				var C5133 = objectFindByKey(resp, 'cuenta',  '5133' );
				var C5134 = objectFindByKey(resp, 'cuenta',  '5134' );
				var C5135 = objectFindByKey(resp, 'cuenta',  '5135' );
				var C5136 = objectFindByKey(resp, 'cuenta',  '5136' );
				var C5137 = objectFindByKey(resp, 'cuenta',  '5137' );
				var C5138 = objectFindByKey(resp, 'cuenta',  '5138' );
				var C5139 = objectFindByKey(resp, 'cuenta',  '5139' );
				//var C51398_1 = objectFindByKey(resp, 'cuenta',  '51398.1' );
				var SG= parseFloat(C5131.carfin.replace(/,/g, "")) + parseFloat(C5132.carfin.replace(/,/g, "")) +
							parseFloat(C5133.carfin.replace(/,/g, "")) + parseFloat(C5134.carfin.replace(/,/g, "")) +
							parseFloat(C5135.carfin.replace(/,/g, "")) + parseFloat(C5136.carfin.replace(/,/g, "")) +
							parseFloat(C5137.carfin.replace(/,/g, "")) + parseFloat(C5138.carfin.replace(/,/g, "")) +
							parseFloat(C5139.carfin.replace(/,/g, ""));// + parseFloat(C51398_1.carfin.replace(/,/g, ""));
				var GF= parseFloat(SP) + parseFloat(MS) + parseFloat(SG);
				//alert(numberWithCommas(GF.toFixed(2)));
						 
				var IDP = objectFindByKey(resp, 'cuenta',  '54210.1' );
				
				
				var C551 = objectFindByKey(resp, 'cuenta',  '551' );
				var C5531 = objectFindByKey(resp, 'cuenta',  '5531' );
				var C55690_1 = objectFindByKey(resp, 'cuenta',  '55690.1' );
				var OGP= parseFloat(C551.carfin.replace(/,/g, "")) + parseFloat(C5531.carfin.replace(/,/g, "")) +
							parseFloat(C55690_1.carfin.replace(/,/g, ""));
				var TGP = parseFloat(OGP) + parseFloat(GF) + parseFloat(IDP.carfin.replace(/,/g, ""))+ayudasSociales;
				
				var RE = parseFloat(TIB) - parseFloat(TGP) ;
				var ResultadosDelEjercicio = RE;
				//console.log("ResultadosDelEjercicio A:"+ResultadosDelEjercicio);
				//$('#ResultadosDelEjercicio').html("-"+numberWithCommas(ResultadosDelEjercicio.toFixed(2)));
				//////	FIN	ESTADO DE ACTIVIDADES															/////////////////
				
				var C111 = objectFindByKey(resp, 'cuenta',  '111' );
				var  ESF_H_13=C111.carfin.replace(/,/g, "");
				//$('#C111').html(C111.carfin);
				
				var C112 = objectFindByKey(resp, 'cuenta',  '112' );
				var ESF_H_18=C112.carfin.replace(/,/g, "");
				//$('#C112').html(C112.carfin);
				
				var C1134 = objectFindByKey(resp, 'cuenta',  '1134' );
				var ESF_H_19=C1134.carfin.replace(/,/g, "");
				//$('#C1134').html(C1134.carfin);
				
				var C114 = objectFindByKey(resp, 'cuenta',  '114' );
				//$('#C114').html(C114.carfin);
				var ESF_H_20=C114.carfin.replace(/,/g, "");
				
				
				var C116 = objectFindByKey(resp, 'cuenta',  '116' );
				var ESF_H_22=C116.crefin.replace(/,/g, "");
				//$('#C116').html(numberWithCommas(parseFloat(C116.crefin.replace(/,/g, ""))*(-1)));
				
				var C119 = objectFindByKey(resp, 'cuenta',  '119' );
				var ESF_H_23=parseFloat(C119.carfin.replace(/,/g, ""));
				//$('#C119').html(C119.carfin);
				
				var TAC= parseFloat(C111.carfin.replace(/,/g, "")) + parseFloat(C112.carfin.replace(/,/g, "")) +
							parseFloat(C1134.carfin.replace(/,/g, "")) + parseFloat(C114.carfin.replace(/,/g, "")) +
							(parseFloat(C116.crefin.replace(/,/g, ""))*(-1)) + parseFloat(C119.carfin.replace(/,/g, "")) ;
				TAC=TAC.toFixed(2);
				
				//$('#TAC').html(numberWithCommas(TAC));
				
				
				var C122 = objectFindByKey(resp, 'cuenta',  '122' );
				var G31= C122.carfin;
				var ESF_H_31= parseFloat(C122.carfin.replace(/,/g, ""));
				//$('#C122').html(C122.carfin);
				
				var C123 = objectFindByKey(resp, 'cuenta',  '123' );
				var ESF_H_33= parseFloat(C123.carfin.replace(/,/g, ""));
				//$('#C123').html(C123.carfin);
				
				var C1241 = objectFindByKey(resp, 'cuenta',  '1241' );
										
				var C1244 = objectFindByKey(resp, 'cuenta',  '1244' );
				
				var C12469 = objectFindByKey(resp, 'cuenta',  '12469' );
				
				var G34=parseFloat(C1241.carfin.replace(/,/g, ""))+parseFloat(C1244.carfin.replace(/,/g, ""))+
						parseFloat(C12469.carfin.replace(/,/g, ""));
				var bienesMuebles= G34.toFixed(2);
				var ESF_H_34=bienesMuebles;
				
				
				//$('#bienesMuebles').html(numberWithCommas(bienesMuebles));
				
				var C125110_1 = objectFindByKey(resp, 'cuenta',  '12510.1' );
				var ESF_H_37= parseFloat(C125110_1.carfin.replace(/,/g, ""));
				//$('#C125110_1').html(numberWithCommas(C125110_1.carfin));
				
				var C126 = objectFindByKey(resp, 'cuenta',  '126' );
				var ESF_H_38= parseFloat(C126.crefin.replace(/,/g, ""));
				//$('#C126').html(numberWithCommas(parseFloat(C126.crefin.replace(/,/g, "")) *(-1)));
				
				
				var OANC=	parseFloat(parseFloat(C122.carfin.replace(/,/g, ""))) + parseFloat(parseFloat(C123.carfin.replace(/,/g, ""))) +
							parseFloat(bienesMuebles) + parseFloat(parseFloat(C125110_1.carfin.replace(/,/g, ""))) +
							parseFloat(parseFloat(C126.crefin.replace(/,/g, ""))*(-1))  ;
				//$('#OANC').html(numberWithCommas(OANC.toFixed(2)));
				
				
				var TA= parseFloat(OANC.toFixed(2))+parseFloat(TAC);
				//$('#TA').html(numberWithCommas(TA));
				
				
				
				var C21212_6 = objectFindByKey(resp, 'cuenta',  '21212.6' );
				var ESF_M_18=parseFloat(C21212_6.crefin.replace(/,/g, ""))
				//$('#C21212_6').html(numberWithCommas());
				
				var C213 = objectFindByKey(resp, 'cuenta',  '213' );
				var ESF_M_19=parseFloat(C213.crefin.replace(/,/g, ""));
				//$('#C213').html(numberWithCommas(parseFloat(C213.crefin.replace(/,/g, ""))));
				
				var C216 = objectFindByKey(resp, 'cuenta',  '216' );
				var ESF_M_22=parseFloat(C216.crefin.replace(/,/g, ""));
				//$('#C216').html(numberWithCommas(parseFloat(C216.crefin.replace(/,/g, ""))));
				
				var C2199 = objectFindByKey(resp, 'cuenta',  '2199' );
				var ESF_M_23=parseFloat(C2199.crefin.replace(/,/g, ""));
				//$('#C2199').html(numberWithCommas(parseFloat(C2199.crefin.replace(/,/g, ""))));
				
				
				var CxCP= parseFloat(C21212_6.crefin.replace(/,/g, ""))+ parseFloat(C213.crefin.replace(/,/g, ""))+
							parseFloat(C216.crefin.replace(/,/g, "")) + parseFloat(C2199.crefin.replace(/,/g, ""));
							
				//$('#CxCP').html(numberWithCommas(CxCP));
				
				
				var C2111 = objectFindByKey(resp, 'cuenta',  '2111' );
				var C2112 = objectFindByKey(resp, 'cuenta',  '2112' );
				var C2113 = objectFindByKey(resp, 'cuenta',  '2113' );
				var C2117 = objectFindByKey(resp, 'cuenta',  '2117' );
				var C21199 = objectFindByKey(resp, 'cuenta',  '21199' );
				var CxPaCP= parseFloat(C2111.crefin.replace(/,/g, "")) + parseFloat(C2112.crefin.replace(/,/g, ""))+
							parseFloat(C2113.crefin.replace(/,/g, "")) + parseFloat(C2117.crefin.replace(/,/g, ""))+
							parseFloat(C21199.crefin.replace(/,/g, ""));
				
				var ESF_M_13=CxPaCP;
				//$('#CxPaCP').html(numberWithCommas(CxPaCP));
				
				var TdPC=CxPaCP + parseFloat(C21212_6.crefin.replace(/,/g, "")) + parseFloat(C213.crefin.replace(/,/g, ""))+
									parseFloat(C216.crefin.replace(/,/g, "")) + parseFloat(C2199.crefin.replace(/,/g, ""));
				//$('#TdPC').html(numberWithCommas(TdPC.toFixed(2)));
				
				///////////////////////
				var C212 = objectFindByKey(resp, 'cuenta',  '212' );
				var S18  = parseFloat(C21212_6.crefin.replace(/,/g, ""));
				var S31  = parseFloat(C212.crefin.replace(/,/g, "")) - S18;
				
				var C222 = objectFindByKey(resp, 'cuenta',  '222' );
				var S32 = parseFloat(C222.crefin.replace(/,/g, ""));
				
				var DxPLP= S31 + S32;
				var ESF_M_31=DxPLP;
				//$('#DxPLP').html(numberWithCommas(DxPLP.toFixed(2)));
				
				///////////////////////
				
				var C224 = objectFindByKey(resp, 'cuenta',  '224' );
				var PDALP= parseFloat(C224.crefin.replace(/,/g, ""));
				var ESF_M_34=PDALP;
				//$('#PDALP').html(numberWithCommas(PDALP.toFixed(2)));
				
				
				var C2263 = objectFindByKey(resp, 'cuenta',  '2263' );
				var C2269 = objectFindByKey(resp, 'cuenta',  '2269' );
				var PALP= parseFloat(C2263.crefin.replace(/,/g, "")) + parseFloat(C2269.crefin.replace(/,/g, ""));
				var ESF_M_38=PALP;
				//$('#PALP').html(numberWithCommas(PALP.toFixed(2)));
				
				var TdPNC = DxPLP + PDALP + PALP;
				//$('#TdPNC').html(numberWithCommas(TdPNC.toFixed(2)));
				
				//////////
				var TDPasivo=TdPNC+TdPC;
				//$('#TDPasivo').html(numberWithCommas(TDPasivo.toFixed(2)));
				
				////
				var C31100= objectFindByKey(resp, 'cuenta',  '31100' );
				var aportaciones=parseFloat(C31100.crefin.replace(/,/g, ""));
				var ESF_M_49=aportaciones;
				//$('#aportaciones').html(numberWithCommas(aportaciones.toFixed(2)));
				
				var C31200= objectFindByKey(resp, 'cuenta',  '31200' );
				var DonDCapital=parseFloat(C31200.crefin.replace(/,/g, ""));
				var ESF_M_50=DonDCapital;
				//$('#DonDCapital').html(numberWithCommas(DonDCapital.toFixed(2)));
				
				var C31300_2= objectFindByKey(resp, 'cuenta',  '31300.2' );
				var AporAniosAnte=parseFloat(C31300_2.crefin.replace(/,/g, ""));
				var ESF_M_51=AporAniosAnte;
				//$('#AporAniosAnte').html(numberWithCommas(AporAniosAnte.toFixed(2)));
				
				var HaciendaPublicaPatContri=AporAniosAnte+DonDCapital+aportaciones;
				//$('#HaciendaPublicaPatContri').html(numberWithCommas(HaciendaPublicaPatContri.toFixed(2)));
				
				var C32200 = objectFindByKey(resp, 'cuenta',  '32200' );
				
				var C32100 = objectFindByKey(resp, 'cuenta',  '32100' );
				var C31300_6 = objectFindByKey(resp, 'cuenta',  '31300.6' );
				
				var rdea=(parseFloat(C32200.carfin.replace(/,/g, ""))*(-1));
				
				var C33100_1995 = objectFindByKey(resp, 'cuenta',  '33100.1995' );
				var C33100_1996 = objectFindByKey(resp, 'cuenta',  '33100.1996' );
				var C33100_1997 = objectFindByKey(resp, 'cuenta',  '33100.1997' );
				var C33100_1998 = objectFindByKey(resp, 'cuenta',  '33100.1998' );
				
				var C33100_1999 = objectFindByKey(resp, 'cuenta',  '33100.1999' );
				var C33100_2000 = objectFindByKey(resp, 'cuenta',  '33100.2000' );
				var C33100_2001 = objectFindByKey(resp, 'cuenta',  '33100.2001' );
				var C33100_2002 = objectFindByKey(resp, 'cuenta',  '33100.2002' );
				var C33100_2003 = objectFindByKey(resp, 'cuenta',  '33100.2003' );
				var C33100_2004 = objectFindByKey(resp, 'cuenta',  '33100.2004' );
				var C33100_2005 = objectFindByKey(resp, 'cuenta',  '33100.2005' );
				var C33100_2006 = objectFindByKey(resp, 'cuenta',  '33100.2006' );
				var C33100_2007 = objectFindByKey(resp, 'cuenta',  '33100.2007' );
				
				var I2553=(parseFloat(C33100_1995.crefin.replace(/,/g, "")))+(parseFloat(C33100_1996.crefin.replace(/,/g, "")))+
						  (parseFloat(C33100_1997.crefin.replace(/,/g, "")))+(parseFloat(C33100_1998.crefin.replace(/,/g, "")))-
						  (parseFloat(C33100_1999.carfin.replace(/,/g, "")))-(parseFloat(C33100_2000.carfin.replace(/,/g, "")))-
						  (parseFloat(C33100_2001.carfin.replace(/,/g, "")))-(parseFloat(C33100_2002.carfin.replace(/,/g, "")))-
						  (parseFloat(C33100_2003.carfin.replace(/,/g, "")))-(parseFloat(C33100_2004.carfin.replace(/,/g, "")))-
						  (parseFloat(C33100_2005.carfin.replace(/,/g, "")))-(parseFloat(C33100_2006.carfin.replace(/,/g, "")))-
						  (parseFloat(C33100_2007.carfin.replace(/,/g, "")));
							
				var rxpm=I2553;
				
				var C32100 = objectFindByKey(resp, 'cuenta',  '32100' );
				var rdejercicio= (parseFloat(C32100.carfin.replace(/,/g, "")))*(-1);
				
				var C31300_6 = objectFindByKey(resp, 'cuenta',  '31300.6' );
				var S56= (parseFloat(C32200.carfin.replace(/,/g, "")))*(-1);
				var S57= I2553.toFixed(2);
				var S58= rdejercicio;
				var S59= (parseFloat(C31300_6.carfin.replace(/,/g, "")))*(-1);
				
				var RdEA= parseFloat(S56)+parseFloat(S57)+parseFloat(S58)+parseFloat(S59);
				var ESF_M_56 = RdEA;
				//$('#RdEA').html(numberWithCommas(RdEA.toFixed(2)));
				
				//////
				var ESF_M_55 = ResultadosDelEjercicio;
				//$('#ResultadosDelEjercicio').html(numberWithCommas(ResultadosDelEjercicio.toFixed(2)));
				
				var haciendapublicapatrimoniogenerado=  RdEA+ResultadosDelEjercicio;
				//$('#haciendapublicapatrimoniogenerado').html(numberWithCommas(haciendapublicapatrimoniogenerado.toFixed(2)));
				
				var C33100_9 = objectFindByKey(resp, 'cuenta',  '33100.9000' );
				var S66=(parseFloat(C33100_9.carfin.replace(/,/g, "")))*(-1);
				var ResultadoporPosicionMonetaria= S66;
				//$('#ResultadoporPosicionMonetaria').html(numberWithCommas(ResultadoporPosicionMonetaria.toFixed(2)));
				var L67=0.00;
				var ResultadoporTenenciadeActivosnoMonetarios=L67;
				//$('#ResultadoporTenenciadeActivosnoMonetarios').html(numberWithCommas(ResultadoporTenenciadeActivosnoMonetarios.toFixed(2)));
				
				var ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio=S66+L67;
				//$('#ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio').html(numberWithCommas(ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio.toFixed(2)));
				//var C33200_1 = objectFindByKey(resp, 'cuenta',  '32200' );
				//var ExcesoinsuficienciaACTNC= (parseFloat(C33200_1.carfin.replace(/,/g, "")))*(-1);
				//$('#ExcesoinsuficienciaACTNC').html(ExcesoinsuficienciaACTNC.toFixed(2));
				
				
				
				
				
				
				var TotalHaciendaPublicaYPatrimonio = HaciendaPublicaPatContri+haciendapublicapatrimoniogenerado+ ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio;
				//$("#TotalHaciendaPublicaYPatrimonio").html(numberWithCommas(TotalHaciendaPublicaYPatrimonio.toFixed(2)));
				
				
				var TotaldePasivoyHaciendaPublicaYPatrimonio=TDPasivo+TotalHaciendaPublicaYPatrimonio;
				//$("#TotaldePasivoyHaciendaPublicaYPatrimonio").html(numberWithCommas(TotaldePasivoyHaciendaPublicaYPatrimonio.toFixed(2)));
				
/*******************************************************************************/
/*******************************************************************************/
				
				//var cta = document.getElementById('numcuentacompleta').value;	  	
				if(mes2.length==1)
				{
					mes2="0" + mes2;
				}						
				
				var data = 'query=' + anio + mes + mes_X;
				
				
				
				
				//Segundo Query
				$.post('balanzaGeneral_act_ajax.php',data, function(resp2)
				{ //Llamamos el arch ajax para que nos pase los datos
				
								/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////		ESTADO DE ACTIVIDADES															/////////////////
				var C52410 = objectFindByKey(resp2, 'cuenta',  '52410' );
				var ayudasSociales= parseFloat(C52410.carfin.replace(/,/g, ""));
				
				var v422_1 = objectFindByKey(resp2, 'cuenta',  '422' );
				
				
				var v417_1 = objectFindByKey(resp2, 'cuenta',  '417' );
			
				
				var C43121_1 = objectFindByKey(resp2, 'cuenta',  '43121.1' );
				var C43121_2 = objectFindByKey(resp2, 'cuenta',  '43121.2' );
				var C43121_3 = objectFindByKey(resp2, 'cuenta',  '43121.3' );
				var C43121_4 = objectFindByKey(resp2, 'cuenta',  '43121.4' );
				var IF= parseFloat(C43121_1.crefin.replace(/,/g, "")) + parseFloat(C43121_2.crefin.replace(/,/g, "")) +
							parseFloat(C43121_3.crefin.replace(/,/g, "")) + parseFloat(C43121_4.crefin.replace(/,/g, "")) ;
				$('#IF_1').html(numberWithCommas(IF.toFixed(2)));
				
				var C43590_1 = objectFindByKey(resp2, 'cuenta',  '43590.1' );
				var C43590_2 = objectFindByKey(resp2, 'cuenta',  '43590.2' );
				var C43590_3 = objectFindByKey(resp2, 'cuenta',  '43590.3' );
				var C43600_4 = objectFindByKey(resp2, 'cuenta',  '43600.4' );
				var OIF= parseFloat(C43590_1.crefin.replace(/,/g, "")) + parseFloat(C43590_2.crefin.replace(/,/g, "")) +
							parseFloat(C43590_3.crefin.replace(/,/g, "")) + parseFloat(C43600_4.crefin.replace(/,/g, "")) ;
				
				var OI= parseFloat(IF) + parseFloat(OIF);
				
				var TIB= parseFloat(v422_1.crefin.replace(/,/g, "")) + parseFloat(v417_1.crefin.replace(/,/g, ""))+ parseFloat(IF) + parseFloat(OIF);
				
				
				var C5111 = objectFindByKey(resp2, 'cuenta',  '5111' );
				var C5112 = objectFindByKey(resp2, 'cuenta',  '5112' );
				var C5113 = objectFindByKey(resp2, 'cuenta',  '5113' );
				var C5114 = objectFindByKey(resp2, 'cuenta',  '5114' );
				var C5115 = objectFindByKey(resp2, 'cuenta',  '5115' );
				var C5117 = objectFindByKey(resp2, 'cuenta',  '5117' );
				var SP= parseFloat(C5111.carfin.replace(/,/g, "")) + parseFloat(C5112.carfin.replace(/,/g, "")) +
							parseFloat(C5113.carfin.replace(/,/g, "")) + parseFloat(C5114.carfin.replace(/,/g, "")) +
							parseFloat(C5115.carfin.replace(/,/g, "")) + parseFloat(C5117.carfin.replace(/,/g, ""));
				
				
				var C5121 = objectFindByKey(resp2, 'cuenta',  '5121' );
				var C5122 = objectFindByKey(resp2, 'cuenta',  '5122' );
				var C5124 = objectFindByKey(resp2, 'cuenta',  '5124' );
				var C5125 = objectFindByKey(resp2, 'cuenta',  '5125' );
				var C5126 = objectFindByKey(resp2, 'cuenta',  '5126' );
				var C5127 = objectFindByKey(resp2, 'cuenta',  '5127' );
				var C5129 = objectFindByKey(resp2, 'cuenta',  '5129' );
				var MS= parseFloat(C5121.carfin.replace(/,/g, "")) + parseFloat(C5122.carfin.replace(/,/g, "")) +
							parseFloat(C5124.carfin.replace(/,/g, "")) + parseFloat(C5125.carfin.replace(/,/g, "")) +
							parseFloat(C5126.carfin.replace(/,/g, "")) + parseFloat(C5127.carfin.replace(/,/g, "")) +
							parseFloat(C5129.carfin.replace(/,/g, ""));
				
				var C5131 = objectFindByKey(resp2, 'cuenta',  '5131' );
				var C5132 = objectFindByKey(resp2, 'cuenta',  '5132' );
				var C5133 = objectFindByKey(resp2, 'cuenta',  '5133' );
				var C5134 = objectFindByKey(resp2, 'cuenta',  '5134' );
				var C5135 = objectFindByKey(resp2, 'cuenta',  '5135' );
				var C5136 = objectFindByKey(resp2, 'cuenta',  '5136' );
				var C5137 = objectFindByKey(resp2, 'cuenta',  '5137' );
				var C5138 = objectFindByKey(resp2, 'cuenta',  '5138' );
				var C5139 = objectFindByKey(resp2, 'cuenta',  '5139' );
				//var C51398_1 = objectFindByKey(resp2, 'cuenta',  '51398.1' );
				var SG= parseFloat(C5131.carfin.replace(/,/g, "")) + parseFloat(C5132.carfin.replace(/,/g, "")) +
							parseFloat(C5133.carfin.replace(/,/g, "")) + parseFloat(C5134.carfin.replace(/,/g, "")) +
							parseFloat(C5135.carfin.replace(/,/g, "")) + parseFloat(C5136.carfin.replace(/,/g, "")) +
							parseFloat(C5137.carfin.replace(/,/g, "")) + parseFloat(C5138.carfin.replace(/,/g, "")) +
							parseFloat(C5139.carfin.replace(/,/g, ""));// + parseFloat(C51398_1.carfin.replace(/,/g, ""));
				var GF= parseFloat(SP) + parseFloat(MS) + parseFloat(SG);
				//alert(numberWithCommas(GF.toFixed(2)));
						 
				var IDP = objectFindByKey(resp2, 'cuenta',  '54210.1' );
				
				
				var C551 = objectFindByKey(resp2, 'cuenta',  '551' );
				var C5531 = objectFindByKey(resp2, 'cuenta',  '5531' );
				var C55690_1 = objectFindByKey(resp2, 'cuenta',  '55690.1' );
				var OGP= parseFloat(C551.carfin.replace(/,/g, "")) + parseFloat(C5531.carfin.replace(/,/g, "")) +
							parseFloat(C55690_1.carfin.replace(/,/g, ""));
				var TGP = parseFloat(OGP) + parseFloat(GF) + parseFloat(IDP.carfin.replace(/,/g, ""))+ayudasSociales;
				
				var RE = parseFloat(TIB) - parseFloat(TGP) ;
				var ResultadosDelEjercicio = RE;
				//console.log("ResultadosDelEjercicio A:"+ResultadosDelEjercicio);
				//$('#ResultadosDelEjercicio').html("-"+numberWithCommas(ResultadosDelEjercicio.toFixed(2)));
				//////	FIN	ESTADO DE ACTIVIDADES															/////////////////
				

					
					var C111 = objectFindByKey(resp2, 'cuenta',  '111' );
					var ESF_G_13=C111.carfin.replace(/,/g, "");
					//$('#C111_X').html(numberWithCommas(C111.carfin));
					
					var C112 = objectFindByKey(resp2, 'cuenta',  '112' );
					var ESF_G_18=C112.carfin.replace(/,/g, "");
					//$('#C112_X').html(numberWithCommas(C112.carfin));
					
					var C1134 = objectFindByKey(resp2, 'cuenta',  '1134' );
					var ESF_G_19=C1134.carfin.replace(/,/g, "");
					//$('#C1134_X').html(numberWithCommas(C1134.carfin));
					
					var C114 = objectFindByKey(resp2, 'cuenta',  '114' );
					//$('#C114_X').html(numberWithCommas(C114.carfin));
					var ESF_G_20=C114.carfin.replace(/,/g, "");
					
					var C116 = objectFindByKey(resp2, 'cuenta',  '116' );
					var ESF_G_22=C116.crefin.replace(/,/g, "");
					//$('#C116_X').html(numberWithCommas(parseFloat(C116.crefin.replace(/,/g, ""))*(-1)));
					
					var C119 = objectFindByKey(resp2, 'cuenta',  '119' );
					var ESF_G_23=parseFloat(C119.carfin.replace(/,/g, ""));
					//$('#C119_X').html(numberWithCommas(C119.carfin));
					
					var TAC= parseFloat(C111.carfin.replace(/,/g, "")) + parseFloat(C112.carfin.replace(/,/g, "")) +
								parseFloat(C1134.carfin.replace(/,/g, "")) + parseFloat(C114.carfin.replace(/,/g, "")) +
								(parseFloat(C116.crefin.replace(/,/g, ""))*(-1)) + parseFloat(C119.carfin.replace(/,/g, "")) ;
					TAC=TAC.toFixed(2);
					
					//$('#TAC_X').html(numberWithCommas(TAC));
					
					
					var C122 = objectFindByKey(resp2, 'cuenta',  '122' );
					var H31= C122.carfin;
					var ESF_G_31= parseFloat(C122.carfin.replace(/,/g, ""));
					//$('#C122_X').html(numberWithCommas(C122.carfin));
					
					var C123 = objectFindByKey(resp2, 'cuenta',  '123' );
					var ESF_G_33= parseFloat(C123.carfin.replace(/,/g, ""));
					//$('#C123_X').html(numberWithCommas(C123.carfin));
					
					var C1241 = objectFindByKey(resp2, 'cuenta',  '1241' );
											
					var C1244 = objectFindByKey(resp2, 'cuenta',  '1244' );
					
					var C12469 = objectFindByKey(resp2, 'cuenta',  '12469' );
					
					var G34=parseFloat(C1241.carfin.replace(/,/g, ""))+parseFloat(C1244.carfin.replace(/,/g, ""))+
							parseFloat(C12469.carfin.replace(/,/g, ""));
					var bienesMuebles= G34.toFixed(2);
					var ESF_G_34=bienesMuebles;
					
					//$('#bienesMuebles_X').html(numberWithCommas(bienesMuebles));
					
					var C125110_1 = objectFindByKey(resp2, 'cuenta',  '12510.1' );
					var ESF_G_37= parseFloat(C125110_1.carfin.replace(/,/g, ""));
					//$('#C125110_1_X').html(numberWithCommas(C125110_1.carfin));
					
					var C126 = objectFindByKey(resp2, 'cuenta',  '126' );
					var ESF_G_38= parseFloat(C126.crefin.replace(/,/g, ""));
					//$('#C126_X').html(numberWithCommas(parseFloat(C126.crefin.replace(/,/g, "")) *(-1)));
					
					var OANC= parseFloat(parseFloat(C122.carfin.replace(/,/g, ""))) + parseFloat(parseFloat(C123.carfin.replace(/,/g, ""))) +
								parseFloat(bienesMuebles) + parseFloat(parseFloat(C125110_1.carfin.replace(/,/g, ""))) +
								parseFloat((parseFloat(C126.crefin.replace(/,/g, ""))*(-1)))  ;
					//$('#OANC_X').html(numberWithCommas(OANC.toFixed(2)));
					
					
					var TA= parseFloat(OANC.toFixed(2))+parseFloat(TAC);
					//$('#TA_X').html(numberWithCommas(TA));
					
					
					
					var C21212_6 = objectFindByKey(resp2, 'cuenta',  '21212.6' );
					var ESF_L_18=parseFloat(C21212_6.crefin.replace(/,/g, ""))
					//$('#C21212_6_X').html(numberWithCommas(parseFloat(C21212_6.crefin.replace(/,/g, ""))));
					
					var C213 = objectFindByKey(resp2, 'cuenta',  '213' );
					var ESF_L_19=parseFloat(C213.crefin.replace(/,/g, ""))
					//$('#C213_X').html(numberWithCommas(parseFloat(C213.crefin.replace(/,/g, ""))));
					
					var C216 = objectFindByKey(resp2, 'cuenta',  '216' );
					var ESF_L_22=parseFloat(C216.crefin.replace(/,/g, ""));
					//$('#C216_X').html(numberWithCommas(parseFloat(C216.crefin.replace(/,/g, ""))));
					
					var C2199 = objectFindByKey(resp2, 'cuenta',  '2199' );
					var ESF_L_23=parseFloat(C2199.crefin.replace(/,/g, ""));
					//$('#C2199_X').html(numberWithCommas(parseFloat(C2199.crefin.replace(/,/g, ""))));
					
					
					var CxCP= parseFloat(C21212_6.crefin.replace(/,/g, ""))+ parseFloat(C213.crefin.replace(/,/g, ""))+
								parseFloat(C216.crefin.replace(/,/g, "")) + parseFloat(C2199.crefin.replace(/,/g, ""));
								
					//$('#CxCP_X').html(numberWithCommas(CxCP));
					
					
					var C2111 = objectFindByKey(resp2, 'cuenta',  '2111' );
					var C2112 = objectFindByKey(resp2, 'cuenta',  '2112' );
					var C2113 = objectFindByKey(resp2, 'cuenta',  '2113' );
					var C2117 = objectFindByKey(resp2, 'cuenta',  '2117' );
					var C21199 = objectFindByKey(resp2, 'cuenta',  '21199' );
					var CxPaCP= parseFloat(C2111.crefin.replace(/,/g, "")) + parseFloat(C2112.crefin.replace(/,/g, ""))+
								parseFloat(C2113.crefin.replace(/,/g, "")) + parseFloat(C2117.crefin.replace(/,/g, ""))+
								parseFloat(C21199.crefin.replace(/,/g, ""));
					var ESF_L_13=CxPaCP;
					//$('#CxPaCP_X').html(numberWithCommas(CxPaCP));
					
					var TdPC=CxPaCP + parseFloat(C21212_6.crefin.replace(/,/g, "")) + parseFloat(C213.crefin.replace(/,/g, ""))+
										parseFloat(C216.crefin.replace(/,/g, "")) + parseFloat(C2199.crefin.replace(/,/g, ""));
					//$('#TdPC_X').html(numberWithCommas(TdPC.toFixed(2)));
					
					///////////////////////
					var C212 = objectFindByKey(resp2, 'cuenta',  '212' );
					var S18  = parseFloat(C21212_6.crefin.replace(/,/g, ""));
					var S31  = parseFloat(C212.crefin.replace(/,/g, "")) - S18;
					
					var C222 = objectFindByKey(resp2, 'cuenta',  '222' );
					var S32 = parseFloat(C222.crefin.replace(/,/g, ""));
					
					var DxPLP= S31 + S32;
					var ESF_L_31=DxPLP;
					//$('#DxPLP_X').html(numberWithCommas(DxPLP.toFixed(2)));
					
					///////////////////////
					
					var C224 = objectFindByKey(resp2, 'cuenta',  '224' );
					var PDALP= parseFloat(C224.crefin.replace(/,/g, ""));
					var ESF_L_34=PDALP;
					//$('#PDALP_X').html(numberWithCommas(PDALP.toFixed(2)));
					
					
					var C2263 = objectFindByKey(resp2, 'cuenta',  '2263' );
					var C2269 = objectFindByKey(resp2, 'cuenta',  '2269' );
					var PALP= parseFloat(C2263.crefin.replace(/,/g, "")) + parseFloat(C2269.crefin.replace(/,/g, ""));
					var ESF_L_38=PALP;
					//$('#PALP_X').html(numberWithCommas(PALP.toFixed(2)));
					
					var TdPNC = DxPLP + PDALP + PALP;
					//$('#TdPNC_X').html(numberWithCommas(TdPNC.toFixed(2)));
					
					//////////
					var TDPasivo=TdPNC+TdPC;
					//$('#TDPasivo_X').html(numberWithCommas(TDPasivo.toFixed(2)));
					
					////
					var C31100= objectFindByKey(resp2, 'cuenta',  '31100' );
					var aportaciones=parseFloat(C31100.crefin.replace(/,/g, ""));
					var ESF_L_49=aportaciones;
					//$('#aportaciones_X').html(numberWithCommas(aportaciones.toFixed(2)));
					
					var C31200= objectFindByKey(resp2, 'cuenta',  '31200' );
					var DonDCapital=parseFloat(C31200.crefin.replace(/,/g, ""));
					var ESF_L_50=DonDCapital;
					//$('#DonDCapital_X').html(numberWithCommas(DonDCapital.toFixed(2)));
					
					var C31300_2= objectFindByKey(resp2, 'cuenta',  '31300.2' );
					var AporAniosAnte=parseFloat(C31300_2.crefin.replace(/,/g, ""));
					var ESF_L_51=AporAniosAnte;
					//$('#AporAniosAnte_X').html(numberWithCommas(AporAniosAnte.toFixed(2)));
					
					var HaciendaPublicaPatContri=AporAniosAnte+DonDCapital+aportaciones;
					//$('#HaciendaPublicaPatContri_X').html(numberWithCommas(HaciendaPublicaPatContri.toFixed(2)));
					
					var C32200 = objectFindByKey(resp2, 'cuenta',  '32200' );
					
					var C32100 = objectFindByKey(resp2, 'cuenta',  '32100' );
					var C31300_6 = objectFindByKey(resp2, 'cuenta',  '31300.6' );
					
					var rdea=(parseFloat(C32200.carfin.replace(/,/g, ""))*(-1));
					
					var C33100_1995 = objectFindByKey(resp2, 'cuenta',  '33100.1995' );
					var C33100_1996 = objectFindByKey(resp2, 'cuenta',  '33100.1996' );
					var C33100_1997 = objectFindByKey(resp2, 'cuenta',  '33100.1997' );
					var C33100_1998 = objectFindByKey(resp2, 'cuenta',  '33100.1998' );
					
					var C33100_1999 = objectFindByKey(resp2, 'cuenta',  '33100.1999' );
					var C33100_2000 = objectFindByKey(resp2, 'cuenta',  '33100.2000' );
					var C33100_2001 = objectFindByKey(resp2, 'cuenta',  '33100.2001' );
					var C33100_2002 = objectFindByKey(resp2, 'cuenta',  '33100.2002' );
					var C33100_2003 = objectFindByKey(resp2, 'cuenta',  '33100.2003' );
					var C33100_2004 = objectFindByKey(resp2, 'cuenta',  '33100.2004' );
					var C33100_2005 = objectFindByKey(resp2, 'cuenta',  '33100.2005' );
					var C33100_2006 = objectFindByKey(resp2, 'cuenta',  '33100.2006' );
					var C33100_2007 = objectFindByKey(resp2, 'cuenta',  '33100.2007' );
					
					var I2553=(parseFloat(C33100_1995.crefin.replace(/,/g, "")))+(parseFloat(C33100_1996.crefin.replace(/,/g, "")))+
							  (parseFloat(C33100_1997.crefin.replace(/,/g, "")))+(parseFloat(C33100_1998.crefin.replace(/,/g, "")))-
							  (parseFloat(C33100_1999.carfin.replace(/,/g, "")))-(parseFloat(C33100_2000.carfin.replace(/,/g, "")))-
							  (parseFloat(C33100_2001.carfin.replace(/,/g, "")))-(parseFloat(C33100_2002.carfin.replace(/,/g, "")))-
							  (parseFloat(C33100_2003.carfin.replace(/,/g, "")))-(parseFloat(C33100_2004.carfin.replace(/,/g, "")))-
							  (parseFloat(C33100_2005.carfin.replace(/,/g, "")))-(parseFloat(C33100_2006.carfin.replace(/,/g, "")))-
							  (parseFloat(C33100_2007.carfin.replace(/,/g, "")));
								
					var rxpm=I2553;
					
					var C32100 = objectFindByKey(resp2, 'cuenta',  '32100' );
					var rdejercicio= (parseFloat(C32100.carfin.replace(/,/g, "")))*(-1);
					
					var C31300_6 = objectFindByKey(resp2, 'cuenta',  '31300.6' );
					var S56= (parseFloat(C32200.carfin.replace(/,/g, "")))*(-1);
					var S57= I2553.toFixed(2);
					var S58= rdejercicio;
					var S59= (parseFloat(C31300_6.carfin.replace(/,/g, "")))*(-1);
					
					var RdEA= parseFloat(S56)+parseFloat(S57)+parseFloat(S58)+parseFloat(S59);
					var ESF_L_56 = RdEA;
					//$('#RdEA_X').html(numberWithCommas(RdEA.toFixed(2)));
					
					//////
					var ESF_L_55 = ResultadosDelEjercicio;
					//$('#ResultadosDelEjercicio_X').html(numberWithCommas(ResultadosDelEjercicio.toFixed(2)));
					
					var haciendapublicapatrimoniogenerado=  RdEA+ResultadosDelEjercicio;
					//$('#haciendapublicapatrimoniogenerado_X').html(numberWithCommas(haciendapublicapatrimoniogenerado.toFixed(2)));
					
					var C33100_9 = objectFindByKey(resp2, 'cuenta',  '33100.9000' );
					var S66=(parseFloat(C33100_9.carfin.replace(/,/g, "")))*(-1);
					var ResultadoporPosicionMonetaria= S66;
					//$('#ResultadoporPosicionMonetaria_X').html(numberWithCommas(ResultadoporPosicionMonetaria.toFixed(2)));
					var L67=0.00;
					var ResultadoporTenenciadeActivosnoMonetarios=L67;
					//$('#ResultadoporTenenciadeActivosnoMonetarios_X').html(numberWithCommas(ResultadoporTenenciadeActivosnoMonetarios.toFixed(2)));
					
					var ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio=S66+L67;
					//$('#ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio_X').html(numberWithCommas(ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio.toFixed(2)));
					//var C33200_1 = objectFindByKey(resp2, 'cuenta',  '32200' );
					//var ExcesoinsuficienciaACTNC= (parseFloat(C33200_1.carfin.replace(/,/g, "")))*(-1);
					//$('#ExcesoinsuficienciaACTNC').html(ExcesoinsuficienciaACTNC.toFixed(2));
					
					
					
					
					
					
					var TotalHaciendaPublicaYPatrimonio = HaciendaPublicaPatContri+haciendapublicapatrimoniogenerado+ ExcesooInsuficienciaenlaActualizaciondelaHaciendaPublicaYPatrimonio;
					//$("#TotalHaciendaPublicaYPatrimonio_X").html(numberWithCommas(TotalHaciendaPublicaYPatrimonio.toFixed(2)));
					
					
					var TotaldePasivoyHaciendaPublicaYPatrimonio=TDPasivo+TotalHaciendaPublicaYPatrimonio;
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Efectivo y Equivalentes - Origen
					var ECSF_E13=0.00;
					if( ESF_G_13<ESF_H_13 )
						ECSF_E13=parseFloat( ESF_G_13)-parseFloat( ESF_H_13);
					$('#ECSF_E13').html(numberWithCommas(ECSF_E13.toFixed(2)));
					
					//Efectivo y Equivalentes - Aplicacion
					var ECSF_F13=0.00;
					if( ESF_G_13>ESF_H_13 )
						ECSF_F13=parseFloat( ESF_G_13)-parseFloat( ESF_H_13);
					$('#ECSF_F13').html(numberWithCommas(ECSF_F13.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Derechos a Recibir Efectivo o Equivalentes - Origen
					var ECSF_E14=0.00;
					if(ESF_H_18  > ESF_G_18)
						ECSF_E14=parseFloat(ESF_H_18) -parseFloat(ESF_G_18);
					$('#ECSF_E14').html(numberWithCommas(ECSF_E14.toFixed(2)));
					
					//Derechos a Recibir Efectivo o Equivalentes - Aplicacion
					var ECSF_F14=0.00;
					if(ESF_G_18 > ESF_H_18)
						ECSF_F14=parseFloat(ESF_G_18)-parseFloat(ESF_H_18);
					$('#ECSF_F14').html(numberWithCommas(ECSF_F14.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Derechos a Recibir Bienes o Servicios - Origen
					var ECSF_E15=0.00;
					if(ESF_H_19  > ESF_G_19)
						ECSF_E15=parseFloat(ESF_H_19) -parseFloat(ESF_G_19);
					$('#ECSF_E15').html(numberWithCommas(ECSF_E15.toFixed(2)));
					
					//Derechos a Recibir Bienes o Servicios - Aplicacion
					var ECSF_F15=0.00;
					if(ESF_G_19 > ESF_H_19)
						ECSF_F15=parseFloat(ESF_G_19)-parseFloat(ESF_H_19);
					$('#ECSF_F15').html(numberWithCommas(ECSF_F15.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Derechos a Recibir Bienes o Servicios - Origen
					var ECSF_E16=0.00;
					if(ESF_H_20  > ESF_G_20)
						ECSF_E16=parseFloat(ESF_H_20) -parseFloat(ESF_G_20);
					$('#ECSF_E16').html(numberWithCommas(ECSF_E16.toFixed(2)));
					
					//Derechos a Recibir Bienes o Servicios - Aplicacion
					var ECSF_F16=0.00;
					if(ESF_G_20 > ESF_H_20)
						ECSF_F16=parseFloat(ESF_G_20)-parseFloat(ESF_H_20);
					$('#ECSF_F16').html(numberWithCommas(ECSF_F16.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Almacenes - Origen
					var ECSF_E17=0.00;
					$('#ECSF_E17').html(numberWithCommas(ECSF_E17.toFixed(2)));
					
					//Almacenes - Aplicacion
					var ECSF_F17=0.00;
					$('#ECSF_F17').html(numberWithCommas(ECSF_F17.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Estimación por Pérdida o Deterioro de Activos Circulantes - Origen
					var ECSF_E18=0.00;
					if(ESF_H_22  > ESF_G_22)
						ECSF_E18=parseFloat(ESF_H_22) -parseFloat(ESF_G_22);
					$('#ECSF_E18').html(numberWithCommas(ECSF_E18.toFixed(2)));
					
					//Estimación por Pérdida o Deterioro de Activos Circulantes - Aplicacion
					var ECSF_F18=0.00;
					if(ESF_G_22 > ESF_H_22)
						ECSF_F18=parseFloat(ESF_G_22)-parseFloat(ESF_H_22);
					$('#ECSF_F18').html(numberWithCommas(ECSF_F18.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Otros Activos Circulantes - Origen
					var ECSF_E20=0.00;
					if(ESF_H_23  > ESF_G_23)
						ECSF_E20=parseFloat(ESF_H_23) -parseFloat(ESF_G_23);
					$('#ECSF_E20').html(numberWithCommas(ECSF_E20.toFixed(2)));
					
					//Otros Activos Circulantes - Aplicacion
					var ECSF_F20=0.00;
					if(ESF_G_23 > ESF_H_23)
						ECSF_F20=parseFloat(ESF_G_23)-parseFloat(ESF_H_23);
					$('#ECSF_F20').html(numberWithCommas(ECSF_F20.toFixed(2)));
					
					///////////////////////////////////////////////////////////////
					//Activo Circulante-  origen
					var ECSF_E11=0.00;
					ECSF_E11=ECSF_E13+ECSF_E14+ECSF_E15+ECSF_E16+ECSF_E17+ECSF_E18+ECSF_E20;
					$('#ECSF_E11').html(numberWithCommas(ECSF_E11.toFixed(2)));
					//Activo Circulante - Aplicacion
					var ECSF_F11=0.00;
					ECSF_F11=ECSF_F13+ECSF_F14+ECSF_F15+ECSF_F16+ECSF_F17+ECSF_F18+ECSF_F20;
					$('#ECSF_F11').html(numberWithCommas(ECSF_F11.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Inversiones Financieras a Largo Plazo - Origen
					var ECSF_E25=0.00;
					if(ESF_H_31  > ESF_G_31)
						ECSF_E25=parseFloat(ESF_H_31) -parseFloat(ESF_G_31);
					$('#ECSF_E25').html(numberWithCommas(ECSF_E25.toFixed(2)));
					
					//Inversiones Financieras a Largo Plazo - Aplicacion
					var ECSF_F25=0.00;
					if(ESF_G_31 > ESF_H_31)
						ECSF_F25=parseFloat(ESF_G_31)-parseFloat(ESF_H_31);
					$('#ECSF_F25').html(numberWithCommas(ECSF_F25.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Derechos a Recibir Efectivo o Equivalentes a Largo Plazo - Origen
					var ECSF_E26=0.00;
					$('#ECSF_E26').html(numberWithCommas(ECSF_E26.toFixed(2)));
					
					//Derechos a Recibir Efectivo o Equivalentes a Largo Plazo - Aplicacion
					var ECSF_F26=0.00;
					$('#ECSF_F26').html(numberWithCommas(ECSF_F26.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Bienes Inmuebles, Infraestructura y Construcciones en Proceso - Origen
					var ECSF_E27=0.00;
					if(ESF_H_33  > ESF_G_33)
						ECSF_E27=parseFloat(ESF_H_33) -parseFloat(ESF_G_33);
					$('#ECSF_E27').html(numberWithCommas(ECSF_E27.toFixed(2)));
					
					
					//Bienes Inmuebles, Infraestructura y Construcciones en Proceso - Aplicacion
					var ECSF_F27=0.00;
					if(ESF_G_33 > ESF_H_33)
						ECSF_F27=parseFloat(ESF_G_33)-parseFloat(ESF_H_33);
					$('#ECSF_F27').html(numberWithCommas(ECSF_F27.toFixed(2)));
					
					////////////////////////////////////////////////////////////////////////////////////
					//Bienes Inmuebles- Origen
					var ECSF_E28=0.00;
					if(ESF_H_34  > ESF_G_34)
						ECSF_E28=parseFloat(ESF_H_34) -parseFloat(ESF_G_34);
					$('#ECSF_E28').html(numberWithCommas(ECSF_E28.toFixed(2)));
					
					
					//Bienes Inmuebles - Aplicacion
					var ECSF_F28=0.00;
					if(ESF_G_34 > ESF_H_34)
						ECSF_F28=parseFloat(ESF_G_34)-parseFloat(ESF_H_34);
					$('#ECSF_F28').html(numberWithCommas(ECSF_F28.toFixed(2)));
					
					////////////////////////////////////////////////////////////////////////////////////
					//Activos Intangibles- Origen
					var ECSF_E29=0.00;
					if(ESF_H_37  > ESF_G_37)
						ECSF_E29=parseFloat(ESF_H_37) -parseFloat(ESF_G_37);
					$('#ECSF_E29').html(numberWithCommas(ECSF_E29.toFixed(2)));
					
					
					//Activos Intangibles - Aplicacion
					var ECSF_F29=0.00;
					if(ESF_G_37 > ESF_H_37)
						ECSF_F29=parseFloat(ESF_G_37)-parseFloat(ESF_H_37);
					$('#ECSF_F29').html(numberWithCommas(ECSF_F29.toFixed(2)));
					
					
					////////////////////////////////////////////////////////////////////////////////////
					//Depreciación, Deterioro y Amortización Acumulada de Bienes- Origen
					console.log("ESF_H_38  > ESF_G_38");
					console.log(ESF_H_38 +" > "+ESF_G_38);
					var ECSF_F30=0.00;
					if(ESF_H_38  > ESF_G_38)
					
						ECSF_F30=parseFloat(ESF_H_38)-parseFloat(ESF_G_38);
					$('#ECSF_F30').html(numberWithCommas(ECSF_F30.toFixed(2)));
					
					//Depreciación, Deterioro y Amortización Acumulada de Bienes - Aplicacion
					
					
					var ECSF_E30=0.00;
					if(ESF_G_38 > ESF_H_38)
						ECSF_E30=parseFloat(ESF_G_38)-parseFloat(ESF_H_38) ;
					$('#ECSF_E30').html(numberWithCommas(ECSF_E30.toFixed(2)));
					
					////////////////////////////////////////////////////////////////////////////////////
					//Activos Diferidos- Origen
					var ECSF_E32=0.00;
					$('#ECSF_E32').html(numberWithCommas(ECSF_E32.toFixed(2)));
					
					//Activos Diferidos- Aplicacion
					var ECSF_F32=0.00;
					$('#ECSF_F32').html(numberWithCommas(ECSF_F32.toFixed(2)));
					////////////////////////////////////////////////////////////////////////////////////
					//Estimación por Pérdida o Deterioro de Activos no Circulantes- Origen
					var ECSF_E33=0.00;
					$('#ECSF_E33').html(numberWithCommas(ECSF_E33.toFixed(2)));
					
					//Estimación por Pérdida o Deterioro de Activos no Circulantes- Aplicacion
					var ECSF_F33=0.00;
					$('#ECSF_F33').html(numberWithCommas(ECSF_F33.toFixed(2)));
					
					////////////////////////////////////////////////////////////////////////////////////
					//Otros Activos no Circulantes- Origen
					var ECSF_E34=0.00;
					$('#ECSF_E34').html(numberWithCommas(ECSF_E34.toFixed(2)));
					
					//Otros Activos no Circulantes- Aplicacion
					var ECSF_F34=0.00;
					$('#ECSF_F34').html(numberWithCommas(ECSF_F34.toFixed(2)));
					////////////////////////////////////////////////////////////////////////////////////
					//Activo No Circulante
					var ECSF_E23=ECSF_E25+ECSF_E26+ECSF_E27+ECSF_E28+ECSF_E29+ECSF_E30+ECSF_E32+ECSF_E33+ECSF_E34;
					$('#ECSF_E23').html(numberWithCommas(ECSF_E23.toFixed(2)));
					//Activo No Circulante- Aplicacion
					var ECSF_F23=ECSF_F25+ECSF_F26+ECSF_F27+ECSF_F28+ECSF_F29+ECSF_F30+ECSF_F32+ECSF_F33+ECSF_F34;
					$('#ECSF_F23').html(numberWithCommas(ECSF_F23.toFixed(2)));
					
					////////////////////////////////////////////////////////////////////////////////////
					//Activo Origen
					var ECSF_E9=ECSF_E11+ECSF_E23;
					$('#ECSF_E9').html(numberWithCommas(ECSF_E9.toFixed(2)));
					//Activo- Aplicacion
					var ECSF_F9=ECSF_F11+ECSF_F23;
					$('#ECSF_F9').html(numberWithCommas(ECSF_F9.toFixed(2)));
					
/////////////////////////////////////////////////////////////////////////////////////////////////////////
					var ECSF_J13=0.00;
					if(ESF_L_13>ESF_M_13)
						ECSF_J13=ESF_L_13-ESF_M_13;
					$('#ECSF_J13').html(numberWithCommas(ECSF_J13.toFixed(2)));
					
					var ECSF_K13=0.00;
					if(ESF_M_13>ESF_L_13)
						ECSF_K13=ESF_M_13-ESF_L_13;
					$('#ECSF_K13').html(numberWithCommas(ECSF_K13.toFixed(2)));
					
					///
					var ECSF_J14=0.00;
					if(ESF_L_18>ESF_M_18)
						ECSF_J14=ESF_L_18-ESF_M_18;
					$('#ECSF_J14').html(numberWithCommas(ECSF_J14.toFixed(2)));
					
					var ECSF_K14=0.00;
					if(ESF_M_18>ESF_L_18)
						ECSF_K14=ESF_M_18-ESF_L_18;
					$('#ECSF_K14').html(numberWithCommas(ECSF_K14.toFixed(2)));
					
					///
					var ECSF_J15=0.00;
					if(ESF_L_19>ESF_M_19)
						ECSF_J15=ESF_L_19-ESF_M_19;
					$('#ECSF_J15').html(numberWithCommas(ECSF_J15.toFixed(2)));
					
					var ECSF_K15=0.00;
					if(ESF_M_19>ESF_L_19)
						ECSF_K15=ESF_M_19-ESF_L_19;
					$('#ECSF_K15').html(numberWithCommas(ECSF_K15.toFixed(2)));
					
					///
					var ECSF_J16=0.00;
					$('#ECSF_J16').html(numberWithCommas(ECSF_J16.toFixed(2)));
					
					var ECSF_K16=0.00;
					$('#ECSF_K16').html(numberWithCommas(ECSF_K16.toFixed(2)));
					
					///
					var ECSF_J17=0.00;
					$('#ECSF_J17').html(numberWithCommas(ECSF_J17.toFixed(2)));
					
					var ECSF_K17=0.00;
					$('#ECSF_K17').html(numberWithCommas(ECSF_K17.toFixed(2)));
					
					///
					var ECSF_J18=0.00;
					if(ESF_L_22>ESF_M_22)
						ECSF_J18=ESF_L_22-ESF_M_22;
					$('#ECSF_J18').html(numberWithCommas(ECSF_J18.toFixed(2)));
					
					var ECSF_K18=0.00;
					if(ESF_M_22>ESF_L_22)
						ECSF_K18=ESF_M_22-ESF_L_22;
					$('#ECSF_K18').html(numberWithCommas(ECSF_K18.toFixed(2)));
					
					
					///
					var ECSF_J20=0.00;
					$('#ECSF_J20').html(numberWithCommas(ECSF_J20.toFixed(2)));
					
					var ECSF_K20=0.00;
					$('#ECSF_K20').html(numberWithCommas(ECSF_K20.toFixed(2)));
					
					//
					var ECSF_J21=0.00;
					if(ESF_L_23>ESF_M_23)
						ECSF_J21=ESF_L_23-ESF_M_23;
					$('#ECSF_J21').html(numberWithCommas(ECSF_J21.toFixed(2)));
					
					var ECSF_K21=0.00;
					if(ESF_M_23>ESF_L_23)
						ECSF_K21=ESF_M_23-ESF_L_23;
					$('#ECSF_K21').html(numberWithCommas(ECSF_K21.toFixed(2)));
					
					var ECSF_J11=0.00;
					ECSF_J11 = ECSF_J13+ECSF_J14+ECSF_J15+ECSF_J16+ECSF_J17+ECSF_J18+ECSF_J20+ECSF_J21;
					$('#ECSF_J11').html(numberWithCommas(ECSF_J11.toFixed(2)));
					var ECSF_K11=0.00;
					ECSF_K11 = ECSF_K13+ECSF_K14+ECSF_K15+ECSF_K16+ECSF_K17+ECSF_K18+ECSF_K20+ECSF_K21;
					$('#ECSF_K11').html(numberWithCommas(ECSF_K11.toFixed(2)));
					
					///////////////////////////////////////////////////////////////////////////////////
					var ECSF_J25=0.00;
					$('#ECSF_J25').html(numberWithCommas(ECSF_J25.toFixed(2)));
					var ECSF_K25=0.00;
					$('#ECSF_K25').html(numberWithCommas(ECSF_K25.toFixed(2)));
					//
					var ECSF_J26=0.00;if(ESF_L_31>ESF_M_31)
						ECSF_J26=ESF_L_31-ESF_M_31;
					$('#ECSF_J26').html(numberWithCommas(ECSF_J26.toFixed(2)));
					var ECSF_K26=0.00;
					if(ESF_M_31>ESF_L_31)
						ECSF_K26=ESF_M_31-ESF_L_31;
					$('#ECSF_K26').html(numberWithCommas(ECSF_K26.toFixed(2)));
					//
					var ECSF_J27=0.00;
					$('#ECSF_J27').html(numberWithCommas(ECSF_J27.toFixed(2)));
					var ECSF_K27=0.00;
					$('#ECSF_K27').html(numberWithCommas(ECSF_K27.toFixed(2)));
					///
					var ECSF_J28=0.00;
					if(ESF_L_34>ESF_M_34)
						ECSF_J28=ESF_L_34-ESF_M_34;
					$('#ECSF_J28').html(numberWithCommas(ECSF_J28.toFixed(2)));
					var ECSF_K28=0.00;
					if(ESF_M_34>ESF_L_34)
						ECSF_K28=ESF_M_34-ESF_L_34;
					$('#ECSF_K28').html(numberWithCommas(ECSF_K28.toFixed(2)));
					//
					var ECSF_J29=0.00;
					$('#ECSF_J29').html(numberWithCommas(ECSF_J29.toFixed(2)));
					var ECSF_K29=0.00;
					$('#ECSF_K29').html(numberWithCommas(ECSF_K29.toFixed(2)));
					///
					var ECSF_J31=0.00;
					if(ESF_L_38>ESF_M_38)
						ECSF_J31=ESF_L_38-ESF_M_38;
					$('#ECSF_J31').html(numberWithCommas(ECSF_J31.toFixed(2)));
					var ECSF_K31=0.00;
					if(ESF_M_38>ESF_L_38)
						ECSF_K31=ESF_M_38-ESF_L_38;
					$('#ECSF_K31').html(numberWithCommas(ECSF_K31.toFixed(2)));
					
					
					var ECSF_J23=0.00;
					ECSF_J23= ECSF_J25+ECSF_J26+ECSF_J27+ECSF_J28+ECSF_J29+ECSF_J31;
					$('#ECSF_J23').html(numberWithCommas(ECSF_J23.toFixed(2)));
					var ECSF_K23=0.00;
					ECSF_K23= ECSF_K25+ECSF_K26+ECSF_K27+ECSF_K28+ECSF_K29+ECSF_K31;
					$('#ECSF_K23').html(numberWithCommas(ECSF_K23.toFixed(2)));
					//////////////////////////////////////////////////////////////////////////
					
					var ECSF_J39=0.00;
					if(ESF_L_49>ESF_M_49)
						ECSF_J31=ESF_L_49-ESF_M_49;
					$('#ECSF_J39').html(numberWithCommas(ECSF_J39.toFixed(2)));
					
					var ECSF_K39=0.00;
					if(ESF_M_49>ESF_L_49)
						ECSF_K31=ESF_M_49-ESF_L_49;
					$('#ECSF_K39').html(numberWithCommas(ECSF_K39.toFixed(2)));
					
					///
					
					var ECSF_J40=0.00;
					if(ESF_L_50>ESF_M_50)
						ECSF_J40=ESF_L_50-ESF_M_50;
					$('#ECSF_J40').html(numberWithCommas(ECSF_J40.toFixed(2)));
					
					var ECSF_K40=0.00;
					if(ESF_M_50>ESF_L_50)
						ECSF_K40=ESF_M_50-ESF_L_50;
					$('#ECSF_K40').html(numberWithCommas(ECSF_K40.toFixed(2)));
					
					////
					
					var ECSF_J41=0.00;
					if(ESF_L_51>ESF_M_51)
						ECSF_J41=ESF_L_51-ESF_M_51;
					$('#ECSF_J41').html(numberWithCommas(ECSF_J41.toFixed(2)));
					
					var ECSF_K41=0.00;
					if(ESF_M_51>ESF_L_51)
						ECSF_K41=ESF_M_51-ESF_L_51;
					$('#ECSF_K41').html(numberWithCommas(ECSF_K41.toFixed(2)));
					
					///
					var ECSF_J37=0.00;
					ECSF_J37=ECSF_J39+ECSF_J40+ECSF_J41;
					$('#ECSF_J37').html(numberWithCommas(ECSF_J37.toFixed(2)));
					
					var ECSF_K37=0.00;
					ECSF_K37=ECSF_K39+ECSF_K40+ECSF_K41;
					$('#ECSF_K37').html(numberWithCommas(ECSF_K37.toFixed(2)));
					
////////////////////////////////////////////////////////////////////////////////////////////
					
					var ECSF_J45=0.00;
					if(ESF_L_55>ESF_M_55)
						ECSF_J45=ESF_L_55-ESF_M_55;
					$('#ECSF_J45').html(numberWithCommas(ECSF_J45.toFixed(2)));
					
					var ECSF_K45=0.00;
					if(ESF_M_55>ESF_L_55)
						ECSF_K45=ESF_M_55-ESF_L_55;
					$('#ECSF_K45').html(numberWithCommas(ECSF_K45.toFixed(2)));
					//
					var ECSF_J46=0.00;
					if(ESF_L_56>ESF_M_56)
						ECSF_J46=ESF_L_56-ESF_M_56;
					$('#ECSF_J46').html(numberWithCommas(ECSF_J46.toFixed(2)));
					
					var ECSF_K46=0.00;
					if(ESF_M_56>ESF_L_56)
						ECSF_K46=ESF_M_56-ESF_L_56;
					$('#ECSF_K46').html(numberWithCommas(ECSF_K46.toFixed(2)));
					//
					var ECSF_J47=0.00;
					$('#ECSF_J47').html(numberWithCommas(ECSF_J47.toFixed(2)));
					var ECSF_K47=0.00;
					$('#ECSF_K47').html(numberWithCommas(ECSF_K47.toFixed(2)));
					
					var ECSF_J48=0.00;
					$('#ECSF_J48').html(numberWithCommas(ECSF_J48.toFixed(2)));
					var ECSF_K48=0.00;
					$('#ECSF_K48').html(numberWithCommas(ECSF_K48.toFixed(2)));
					
					var ECSF_J49=0.00;
					$('#ECSF_J49').html(numberWithCommas(ECSF_J49.toFixed(2)));
					var ECSF_K49=0.00;
					$('#ECSF_K49').html(numberWithCommas(ECSF_K49.toFixed(2)));
					
					var ECSF_J43=0.00;
					ECSF_J43=ECSF_J45+ECSF_J46+ECSF_J47+ECSF_J48+ECSF_J49;
					$('#ECSF_J43').html(numberWithCommas(ECSF_J43.toFixed(2)));
					
					var ECSF_K43=0.00;
					ECSF_K43=ECSF_K45+ECSF_K46+ECSF_K47+ECSF_K48+ECSF_K49;
					$('#ECSF_K43').html(numberWithCommas(ECSF_K43.toFixed(2)));
					
////////////////////////////////////////////////////////////////////////////////////////////

					var ECSF_J53=0.00;
					$('#ECSF_J53').html(numberWithCommas(ECSF_J53.toFixed(2)));
					var ECSF_K53=0.00;
					$('#ECSF_K53').html(numberWithCommas(ECSF_K53.toFixed(2)));
					
					var ECSF_J54=0.00;
					$('#ECSF_J54').html(numberWithCommas(ECSF_J54.toFixed(2)));
					var ECSF_K54=0.00;
					$('#ECSF_K54').html(numberWithCommas(ECSF_K54.toFixed(2)));

					var ECSF_J51=0.00;
					ECSF_J51= ECSF_J53+ECSF_J54;
					$('#ECSF_J51').html(numberWithCommas(ECSF_J51.toFixed(2)));
					
					var ECSF_K51=0.00;
					ECSF_K51= ECSF_K53+ECSF_K54;
					$('#ECSF_K51').html(numberWithCommas(ECSF_K51.toFixed(2)));
					
////////////////////////////////////////////////////////////////////////////////////////////

					
					var ECSF_J35=0.00;
					ECSF_J35=ECSF_J37+ECSF_J43+ECSF_J51;
					$('#ECSF_J35').html(numberWithCommas(ECSF_J35.toFixed(2)));
					
					var ECSF_K35=0.00;
					ECSF_K35=ECSF_K37+ECSF_K43+ECSF_K51;
					$('#ECSF_K35').html(numberWithCommas(ECSF_K35.toFixed(2)));
				
////////////////////////////////////////////////////////////////////////////////////////////
					
					
					var ECSF_J9=0.00;
					ECSF_J9=ECSF_J11+ECSF_J23+ECSF_J35;
					$('#ECSF_J9').html(numberWithCommas(ECSF_J9.toFixed(2)));
					
					var ECSF_K9=0.00;
					ECSF_K9=ECSF_K11+ECSF_K23+ECSF_K35;
					$('#ECSF_K9').html(numberWithCommas(ECSF_K9.toFixed(2)));
			}, 'json');  // Json es una muy buena opcion  
		}, 'json');  // Json es una muy buena opcion               
}
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left" class="text" style='mso-number-format:"\@";'>${cuenta}</td>
					<td class="text" style='mso-number-format:"\@";'>${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
	   body {
			color: black;
		}
		
		.titulos{
			font-family:Arial;
			font-size:11px;
		}
		
		#tableContent td.verde{
			background-color:#2FDC48;
		}

			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
			
			.num {
			  mso-number-format:General;
			}
			.text{
			  mso-number-format:"\@";/*force text*/
			}
			
			
        </style>
</head>
    <body>
    <span class="TituloDForma">Estado de Cambios en la Situación Financiera</span>
    <hr class="hrTitForma">
<div align="center" ><!--id="main"-->
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>
<td><!-- Asignamos variables de fechas -->
	<table>
    	<tr>
        		<td>A&ntilde;o inicial:</td><td><input type="text"  name="anio2" id="anio2" size="4" maxlength="4" tabindex="1"> </td>
                <td>Mes inicial:</td><td><input type="text" name="mes2" id="mes2" size="2" maxlength="2" tabindex="2"></td>
                <td>Mes final:</td><td><input type="text" name="mes2_X" id="mes2_X" size="2" maxlength="2" tabindex="2"></td>
        </tr>
        <tr>
        		<td>A&ntilde;o final:</td><td><input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="3"> </td>
                <td>Mes inicial:</td><td><input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="4"></td>
                <td>Mes final:</td><td><input type="text" name="mes_X" id="mes_X" size="2" maxlength="2" tabindex="4"></td>
        </tr>
    </table>
 </td>
 <td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()" tabindex="5">
</td>
<td>
<input type="hidden" name="ren" id="ren">
    <form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
  <form action="poliza_pdf.php" method="post" target="_blank" id="FormularioExportacionPDF">
    <p>Exportar a PDF  <img src="../../imagenes/pdf.png" class="botonPDF" /></p>
    <input type="hidden" id="datos_a_enviar_PDF" name="datos_a_enviar_PDF" />
</form>

</td>
</tr>
</table>
<table id="Exportar_a_Excel" style="width:100%;" align="center">
	<tr><td  align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Cuenta P&uacute;blica</td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Estado de Cambios en la Situaci&oacute;n Financiera</td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">del <span id="diainiX"></span> </td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">(Pesos)</td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Ente P&uacute;blico: FOMENTO METROPOLITANO DE MONTERREY </td></tr>
    <tr>
    	<td style="width:100%;">
          <table cellspacing="0" cellpadding="0" id="tableContent" style="border-color:#000000;width:90%;" align="center">
            <thead>
            	<tr>
                	<th width="15%">&nbsp;</th>
                	<th width="15%">&nbsp;</th>
                    <th width="9%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                   	<th width="9%">&nbsp;</th>
                    <th width="2%">&nbsp;</th>
                    <th width="15%">&nbsp;</th>
                    <th width="15%">&nbsp;</th>
                    <th width="9%">&nbsp;</th>
                    <th width="1%">&nbsp;</th>
                    <th width="9%">&nbsp;</th>
                </tr>
            </thead>
            <tr>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; border-left:#000000 solid 1px;" colspan="2" rowspan="2" >CONCEPTO</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; " colspan="3" align="center">Periodo</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; ">&nbsp;</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; " colspan="2" rowspan="2">CONCEPTO</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; border-right:#000000 solid 1px;" colspan="3" align="center"><span style="background-color:#2FDC48; font-family:Arial; font-size:14; ">Periodo</span></td>
            </tr>
            <tr >
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center;" align="center" >Origen</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center;"  align="center" >&nbsp;</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center;"  align="center" >Aplicaci&oacute;n</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; " >&nbsp;</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center;" align="center" >Origen</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center; "align="center" ></td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center;  border-right:#000000 solid 1px;"align="center" >Aplicaci&oacute;n</td>
            </tr>
            <tr>
              <td colspan="2" style=" font-family:Arial; font-size:12; font-weight:600;  border-left:#000000 solid 1px;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:600;" align="center">+</td>
              <td >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:600;" align="center">-</td>
              <td >&nbsp;</td>
              <td colspan="2"  style=" font-family:Arial; font-size:12; font-weight:600;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:600;" align="center">+</td>
              <td  style=" ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:600; border-right:#000000 solid 1px;" align="center">-</td>
            </tr>
            <tr>
              <td colspan="2" style=" font-family:Arial; font-size:12; font-weight:600;  border-left:#000000 solid 1px;">ACTIVO</td>
              <td id="ECSF_E9" align="right">&nbsp;</td>
              <td >&nbsp;</td>
              <td id="ECSF_F9" align="right">&nbsp;</td>
              <td >&nbsp;</td>
              <td colspan="2"  style=" font-family:Arial; font-size:12; font-weight:600;">PASIVO</td>
              <td id="ECSF_J9" align="right">&nbsp;</td>
              <td style=" ">&nbsp;</td>
              <td style=" border-right:#000000 solid 1px;" id="ECSF_K9" align="right">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2"  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; border-left:#000000 solid 1px;">Activo    Circulante</td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; " align="right" id="ECSF_E11"></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; " ></td>
              <td style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; " align="right" id="ECSF_F11"></td>
              <td></td>
              <td colspan="2"  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic;">Pasivo Circulante</td>
              <td style="font-family:Arial; font-size:12;font-weight:600; font-style:italic;" align="right" id="ECSF_J11"></td>
              <td>&nbsp;</td>
              <td style="font-family:Arial; font-size:12;font-weight:600; font-style:italic; border-right:#000000 solid 1px;" align="right" id="ECSF_K11"></td>
              
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Efectivo    y Equivalentes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E13"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F13">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Cuentas por Pagar a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J13" ></td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K13">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2">Derechos    a Recibir Efectivo o Equivalentes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E14">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F14">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Documentos por Pagar a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J14"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K14">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2">Derechos    a Recibir Bienes o Servicios</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E15"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F15">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Porci&oacute;n a Corto Plazo de la Deuda P&uacute;blica a Largo    Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J15"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K15">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2">Inventarios</td>
              <td style=" font-family:Arial; font-size:12; text-align:right;" align="right" id="ECSF_E16"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F16">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">T&iacute;tulos y Valores a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J16">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K16">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Almacenes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E17"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" ></td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F17"></td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Pasivos Diferidos a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J17">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K17">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Estimaci&oacute;n    por P&eacute;rdida o Deterioro de Activos Circulantes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E18" class="number">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F18">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Fondos y Bienes de Terceros en Garant&iacute;a y/o    Administraci&oacute;n a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J18"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K18">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Otros    Activos Circulantes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E20"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F20">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Provisiones a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J20">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K20">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Otros Pasivos a Corto Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J21" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K21" >&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Total de Activos Circulantes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TAC">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TAC_X">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TAC_X">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" colspan="4" >&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2" ></td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TdPC"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="TdPC_X">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" colspan="5" >&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Activo    No Circulante</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E23"></td>
              <td style=" font-family:Arial; font-size:12;"  >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right"  id="ECSF_F23">&nbsp;</td>
              <td></td>
              <td style=" font-family:Arial; font-size:12;" colspan="2" >Pasivo No Circulante</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J23"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K23"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
           <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Inversiones    Financieras a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E25">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F25">0.00</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Cuentas por Pagar a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J25">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K25">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Derechos    a Recibir Efectivo o Equivalentes a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E26">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F26">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Documentos por Pagar a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J26"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K26">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Bienes    Inmuebles, Infraestructura y Construcciones en Proceso</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E27"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F27">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Deuda P&uacute;blica a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J27">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K27">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Bienes    Muebles</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E28">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F28">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Pasivos Diferidos a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J28"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K28">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Activos    Intangibles</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E29">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F29">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Fondos y Bienes de    Terceros en Garant&iacute;a y/o en Administraci&oacute;n a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J29">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K29">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Depreciaci&oacute;n,    Deterioro y Amortizaci&oacute;n Acumulada de Bienes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E30">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F30">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Provisiones a Largo Plazo</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J31">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K31">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Activos    Diferidos</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E32">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F32">0.00</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Estimaci&oacute;n    por P&eacute;rdida o Deterioro de Activos no Circulantes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E33">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F33">0.00</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J23">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K23">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >Otros    Activos no Circulantes</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_E34">0.00</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_F34">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:bold;" align="center" id="TDPasivo">+</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; font-weight:bold; border-right:#000000 solid 1px;" align="center" id="TDPasivo_X">-</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="OANC"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="OANC_X">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="OANC_X">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " colspan="2" >Hacienda P&uacute;blica/Patrimonio</td>
              <td style=" font-family:Arial; font-size:12;" id="ECSF_J35" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="ECSF_K35" align="right">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TA"></td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TA_X">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="TA_X">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Hacienda P&uacute;blica/Patrimonio Contribuido</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J37"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K37">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="5" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="2" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2" >Aportaciones</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J39">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " align="right"  >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right"  id="ECSF_K39">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2" >Donaciones de Capital</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J40"></td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K40">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2" >Actualizaci&oacute;n de la    Hacienda P&uacute;blica/Patrimonio</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J41"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K41">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
             <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Hacienda    P&uacute;blica/Patrimonio Generado </td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J43"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K43">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Resultados del Ejercicio    (Ahorro/ Desahorro)</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J45">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K45">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Resultados de Ejercicios    Anteriores</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J46"></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K46">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">Reval&uacute;os</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J47">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K47">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">Reservas</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J48">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K47">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Rectificaciones de    Resultados de Ejercicios Anteriores</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J49">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K49">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="5" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Exceso o Insuficiencia en    la Actualizaci&oacute;n de la Hacienda P&uacute;blica/Patrimonio</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J51">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K51">&nbsp;</td>
            </tr>
             <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="5" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Resultado por Posici&oacute;n    Monetaria</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J53" ></td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K53">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="5" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">Resultado por Tenencia de    Activos no Monetarios</td>
              <td style=" font-family:Arial; font-size:12;" align="right" id="ECSF_J54">0.00</td>
              <td style=" font-family:Arial; font-size:12; " align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" align="right" id="ECSF_K54">0.00</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="4" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" colspan="4" >&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px; border-left:#000000 solid 1px;" colspan="5" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px; border-right:#000000 solid 1px;" colspan="4" >&nbsp;</td>
            </tr>
           <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="5">Bajo    protesta de decir verdad declaramos que los Estados Financieros y sus Notas    son razonalmente correctos y responsabilidad del emisor.</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
            </tr>
            
            <tr>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center; border-top:#000000 solid 1px;" colspan="2">C. Sergio Alejandro Alanis Marroqu&iacute;n</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center; border-top:#000000 solid 1px;" colspan="2">Lic. Eloisa Sanchez M&eacute;ndez</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center;" colspan="2">Director Ejecutivo</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center;" colspan="2">Encargada del Despacho de los Asuntos de la    Direccion de</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" colspan="2">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center;" colspan="2">Administracion y Finanzas</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; ">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
              <td style=" font-family:Arial; font-size:12;"></td>
            </tr><!---->
        </table></td>
    </tr>
    </table>

    </div>
    <div id="div_carga">
   		<img src="../../imagenes/ajax-loader.gif" width="64" id="cargador">
    </div>
    </body>
</html>
