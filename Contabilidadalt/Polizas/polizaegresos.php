<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Polizas de Egresos Automaticas</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script src="javascript/polizas_funciones.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('poliza_Aafectar_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			
			function busca_movegresos()
			{
        		// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
				var opcion = document.getElementById('opc').value;
				if(opcion>0)
				{
					var fecini = document.getElementById('fecini').value;
					var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  						
					var fecinisplit= fecini.split("/");
					var fecfinsplit= fecfin.split("/");
					//alert(fecfinsplit[1]+"=="+fecinisplit[1]);
					if(fecfinsplit[1]==fecinisplit[1] && fecfinsplit[2]==fecinisplit[2] )
					{
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);
	
						$.post('poliza_egresos_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
						$('#proveedor').empty();
						$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
								 // los va colocando en la forma de la tabla
						}, 'json');  // Json es una muy buena opcion      
						
					}
					else
					{
						alert("Debe seleccionar polizas que sea del mismo mes y año.");
					}
					
				}
            }
			function calculatotal()
			{
				var numpoliza=0;
				var totcargo=0.00;
				var totcredito=0.00;
				var polizas = document.getElementsByName('afectacion');
				for(i=0;i<polizas.length;i++)
				{	
					if(	polizas[i].checked)
					{	
						var tok=polizas[i].value.split("¬");
						totcargo=parseFloat(totcargo)+parseFloat(tok[4]);
						totcredito=parseFloat(totcredito)+parseFloat(tok[5]);
						numpoliza++;			
					}		
				}	
				if(numpoliza<=0)
				{
					alert("No existen polizas seleccionadas");
					return false;
				}
				else
				{
					//alert('si entra');
					document.getElementById('totcargo').value=totcargo;
					document.getElementById('totcredito').value=totcredito;
				}
			}
			
			function cancela_poliza(vale, tipo)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					location.href="php_ajax/cancela_poliza.php?poliza="+vale+"&tipo="+tipo;
				}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>          									
					<td>${origen}</td>
					<td>${referencia}</td>
					<td>${cuenta}</td>				
					<td>${nombre}</td>
					<td>${concepto}</td> 
					<td>${cargo}</td>
					<td>${credito}</td>
					<td><input type="checkbox" id="afectacion[]" name="afectacion" value="${origen}¬${referencia}¬${cuenta}¬${concepto}¬${cargo}¬${credito}¬${id_registro}" onClick="calculatotal()"></td>				
            </tr>
        </script>   
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Generacion de  Pólizas de Egresos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table> 
<tr>
	<td>Tipo de poliza: E</td>
    <td>Numero de poliza:<input type="text" name="npoliza" id="npoliza" style="width:150; text-align:left; z-index:1"  /></td>
</tr>  
<tr>
<td width="23%"><select name="opc" id="opc" style="z-index:2" >
  <option value="0" >-- Origen de la Poliza --</option>
  <option value="1" >Cheque</option>
  <option value="2" >Transferencia</option>
</select></td>
<td>
<div align="left" id="rangofecha" style="z-index:3; position:relative;  width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
  
  
  
   

  
  
  
  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_movegresos()">
</div>
</td>
</tr>
</table>
<form >
<table>
<tr>
	<td>
		<table>
        	<thead>         		   
              	<th><p>Origen</p></th>                                       
                <th><p>Referencia</p></th> 
        		<th><p>Cuenta</p></th>
                <th><p>Nombre de la Cuenta</p></th>
                <th><p>Concepto</p></th> 
                <th><p>Cargo</p></th>
                <th><p>Credito</p></th>
                <th><p>Afectar</p></th> 
			</thead>
            <tbody id="proveedor" name='proveedor'>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Encontrar Resultados</td>
                </tr>
            </tbody>
          </table>    
      </td>
</tr>
<tr>
	<td align="center">
      Total de Cargos: $<input type="text" value="" name="totcargo" id="totcargo" style="text-align:right"/>
    
      Total de Creditos: $<input type="text" value="" name="totcredito" id="totcredito" style="text-align:right"/>
    </td>
</tr>
<tr>
	<td align="center">
      <input type="button" value="Generar poliza" onClick="gen_polizapago_aut()">
    </td>
</tr>
</table>
</form>

</table>
    </div>
    </body>
</html>
