<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Reporte de Polizas Contables</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="javascript/divhide.js"></script>
        <script src="javascript/jquery.tmpl.js"></script>
		<script src="javascript/funciones_cheque.js"></script>
		<script src="../../javascript_globalfunc/funcionesGlobales.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('busca_poliza.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_poliza()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);

						$.post('busca_poliza.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						//alert(resp.length);

							if (resp.length<1)
							{
								document.getElementById('opc').value=1;
								document.getElementById('query').value='';
								document.getElementById('fecini').value='';
								document.getElementById('fecfin').value='';
								ShowHidden();
							}
						
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }

			
			function cambia_estatus(folio,x)
			{
//				var pregunta = confirm("Esta seguro que desea afectar Cheque")
//				if (pregunta)
//				{

				if (x==1)
				{
					var pregunta1 = confirm("Esta seguro que desea Afectar esta Solicitud  -- "+folio+"")
						if (pregunta1)
						{
							var datas = {
								 pfolio: folio,
								 px: x,
								 };
							
							jQuery.ajax({
								type: 'post',
								cache:	false,
								url:'php_ajax/solcheque_recibido.php',
								dataType: "json",
								data: datas,
								error: function(request,error) 
								{
								 console.log(arguments);
								 alert ( " Can't do because: " + error );
								},
								success: function(resp)
								{ console.log(arguments);
									var myObj = resp;
									
									///NOT how to print the result and decode in html or php///
									console.log(myObj);
			
									if(resp.length>0)
									{
										alert("Folio de Recepción No. "+resp);
										location.href="recepcion_cheques.php";
									}
									else
									{
										alert("Actualizacion Rechazada.");
									}
								}
								
								});
						}	
				}
				if (x==2)
				{
					var pregunta = confirm("Esta seguro que desea cancelar esta Solicitud  -- "+folio+"")
					if (pregunta)				
						location.href="php_ajax/cancela_sol_cheque.php?folio="+folio;
				}
				if (x==3)
				{
					window.open('concepto.php?folio='+folio+'',"Editar","width=900, height=400, top=180, left=200");
					//new Ajax.Request('php_ajax/busca_factura.php?factura='+factura+"&prov="+prov,
				}
				
				
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl"> 
            <tr>
               {{if folio}}
					<td>${folio}</td>
					<td>${fecha}</td>
					<td>${npoliza}</td>
					<td>${descrip}</td> 
					//{{if estatus==0}}
					//	<td id="estatus"><img src="../../imagenes/actualiza3.png" width="30" height="25" onClick="cambia_estatus(${folio},1)"></td>
					//	<td><img src="../../imagenes/eliminar.jpg" width="30" height="25" onClick="cambia_estatus(${folio},2)"></td>
					//	<td><img src="../../imagenes/edit-icon.png" width="30" height="25" onClick="cambia_estatus(${folio},3)"></td>
					//{{/if}}
					//{{if estatus==10}}
						<td colspan="2" align="center"><img src="../../imagenes/consultar.jpg" width="30" height="25"  onClick="window.open('pdf_files/poliza_'+${folio}+'.pdf')"></td>
					//{{/if}}
					
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   

<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Reporte de Polizas Contables </span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%"><select name="opc" id="opc" onChange="ShowHidden()">
             
              <option value="1" >Poliza</option>
              <!--<option value="2" >Cuenta</option>             
              <option value="3" >Concepto</option>              
              <option value="4" >Por rango de fechas</option>-->
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecini",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger1",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger2",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_poliza()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
</td>
</tr>
</table>
<table>
                <thead>
    				<th>Folio</th>                                       
                    <th>Fecha</th>
                    <th>Poliza</th>
                    <th>Descripcion</th>
                    <th>Estatus</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
