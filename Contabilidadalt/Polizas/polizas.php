<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<!DOCTYPE html PUBLIC "-//W3C//Dtd class="texto8" XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd class="texto8"/xhtml1-transitional.dtd class="texto8"">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin1" />
<title>Alta de poliza</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript/polizas_funciones.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0">
  	<tr>
    	<td class="TituloDForma">Alta de P&oacute;liza
   	      <hr class="hrTitForma"></td class="texto8">
    </tr>
    <tr>
      <td class="texto8"><table width="100%" border="0">
        <tr>
          <td class="texto8"><table width="100%" border="0">
            <tr>
              <td class="texto8" width="6%"><label>Fecha:</label></td class="texto8">
              <td><input tabindex="1" class="texto8" name="fecpoliza" type="text" id="fecpoliza" maxlength="10" style="width:70px" />
                <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer" title=""%y/%m/%d"" align="absmiddle" />
                <!--{literal}-->
                <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecpoliza",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script></td>
              <td class="texto8" width="8%">Tipo poliza:
                <input tabindex="2"  class="texto8" name="tpoliza" type="text" id="tpoliza" size="10" maxlength="1" style="width:15px" onblur=" this.value=this.value.toUpperCase();
          if(this.value!='I' && this.value!='D' && this.value!='E'){ alert('El tipo de poliza solo debe ser I , D � E. '); this.value=''; return false;}" /></td class="texto8">
              <td class="texto8" width="9%"><label>N&uacute;mero de Poliza:</label></td>
              <td><input tabindex="3"  class="texto8" type="text" name="numpoliza" id="numpoliza" title="N�mero de Poliza" maxlength="14" onkeypress="return aceptarSoloNumeros(this,event);"/></td class="texto8">
            </tr>
            <tr>
              <td class="texto8"><label>Descripci&oacute;n:</label></td class="texto8">
              <td class="texto8" colspan="4"><textarea tabindex="4"  name="descrip" id="descrip" style="width:100%"></textarea></td class="texto8">
            </tr>
            <tr>
              <td class="texto8" colspan="3"><div align="left" style="z-index:0; position:relative; width:600px">Nombre de la cuenta:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input tabindex="6"  class="texto8" name="nomsubcuenta" type="text" id="nomsubcuenta" size="50" style="width:300px;"  onkeyup="searchSuggest();" autocomplete="off"/>
                <input type="text" class="texto8" name="numcuentacompleta" id="numcuentacompleta" size="50" style="width:100px;" readonly="readonly" />
                <div id="search_suggest" style="z-index:2; position:absolute;" > </div>
              </div></td>
              <td class="texto8" colspan="2"><label>Cargo: $</label>
                <input tabindex="7"  class="texto8" type="text" name="Cargo" id="Cargo" style="width:120px" onkeypress="return aceptarSoloNumeros(this,event);" onkeydown="asignaFormatoSiesNumerico(this, event)" /></td>
              <td class="texto8" colspan="2"><label>Credito: $</label>
                <input tabindex="8"  class="texto8" type="text" name="Credito" id="Credito"  style="width:120px"  onkeypress="return aceptarSoloNumeros(this,event);" onkeydown="asignaFormatoSiesNumerico(this, event)" /></td>
            </tr>
            <tr>
              <td class="texto8"><label>Referencia:</label></td>
              <td class="texto8"><input tabindex="9"  class="texto8" type="text" name="Referencia" id="Referencia" /></td>
              <td class="texto8"><label>Origen:</label></td>
              <td><select tabindex="10" name="origen" id="origen" class="texto8" size="1">
                <option id=0 selected="selected"></option>
                <?php
                 		$command= "select id,dcorta,descrip from egresostipodocto where poliza='true'";
						//echo "<br>$command</br>";
						$getProducts = sqlsrv_query($conexion,$command);
						while($row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
						{	
							echo "<option value=".$row['id'].">".$row['descrip']."</option>";
						}
                  ?>
              </select></td>
            </tr>
            <tr>
              <td class="texto8"><label>Concepto:</label></td>
              <td class="texto8" colspan="4"><textarea tabindex="11" name="Concepto" id="Concepto" style="width:100%"></textarea></td >
              <td class="texto8" width="49%" valign="bottom" align="center"><input tabindex="12" class="texto8" type="button" value="Agregar" onclick="agregarCargoCredito()" /></td class="texto8">
            </tr>
          </table></td class="texto8">
        </tr>
        <tr>
          <td class="texto8" width="100%">
          <table class="tableone" id="sortable" border="0" style="width:800px">
          <thead>
            <tr align="center">
              <th  class="th1" style="width:60px">Cuenta</th>
              <th  class="th2" style="width:200px">Nombre de Cuenta</th>
              <th  class="th1" style="width:70px">Cargo</th>
              <th  class="th1" style="width:70px">Credito</th>
              <th  class="th2" style="width:150px">Concepto</th>
              <!--<th style="width:60px">Origen</th>-->
              <th style="width:70px">&nbsp;</th>
            </tr>
          </thead>
           <tbody >
                    <tr>
                        <td colspan="6">
                            <div class="innerb" style="width:800px;height:20em;">
                                <table class="tabletwo"  style="width:800px;">
                                    <tbody  name="tfacturas" id="tfacturas" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
          </table></td class="texto8">
        </tr>
      </table></td class="texto8">
    </tr>
    <tr>	
    	<td class="texto8" valign="middle" align="center"><strong><label>Total de Cargos: $</label><input name="totcargo" id="totcargo"  style="width:100px" readonly="readonly" value='0'/><label>Total de Creditos: $</label><input name="totcredito" id="totcredito"  style="width:100px" readonly="readonly" value='0' /></strong></td>
    </tr>
    <tr>
    	<td align="center">
        	<input type="button" onclick="guardarPoliza();"  value="Guardar P�liza" class="texto8"/>
        </td>
    </tr>
  </table>
</form>
</body>
</html>