<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");
	$fecha_poliza="";
	$tipo_poliza="";
	$nombreusuario="";
	$tfacturas="";
	
	$fecha="";
	$conceptos="";
	//$concepto="";
	$numero_poliza="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$partidas="";//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$descripcion="";
	$id_facts="";
	//if(isset($_POST['partidas']))
	//		$partidas=$_POST['partidas'];
	
	//print_r($partidas);
	$idpoliza="0";
	if(isset($_POST['idpoliza']))
		$idpoliza=$_POST['idpoliza'];
		
	if(isset($_POST['fecpoliza']))
		$fecha_poliza=$_POST['fecpoliza'];

	if(isset($_POST['tpoliza']))
		$tipo_poliza=trim($_POST['tpoliza']);

	if(isset($_POST['numpoliza']))
		$numero_poliza=$_POST['numpoliza'];

	if(isset($_POST['descrip']))
			$descripcion=$_POST['descrip'];
	
	
	
	if(isset($_POST['dcuenta']))
		$partidas['numcuentacompleta']=$_POST['dcuenta'];
		
	if(isset($_POST['dcargo']))
		$partidas['Cargo']=$_POST['dcargo'];
	
	//print_r($partidas['Cargo']);
	
	if(isset($_POST['dcredito']))
		$partidas['Credito']=$_POST['dcredito'];
	
	if(isset($_POST['dreferencia']))
		$partidas['Referencia']=$_POST['dreferencia'];
		
	if(isset($_POST['dconcepto']))
		$partidas['Concepto']=$_POST['dconcepto'];
		
	if(isset($_POST['idpartida']))
		$partidas['id']=$_POST['idpartida'];
		
	$fecha_polizaNormal=$fecha_poliza;
	$fecha_poliza=convertirFechaEuropeoAAmericano($fecha_poliza);
	
	if(isset($usuario))
	{
		//echo "tipopoliza:".$tipo_poliza;
		if($tipo_poliza=='I' || $tipo_poliza=='D' || $tipo_poliza=='E')
		{
			//Obtencion de nombre de usuario
			$command="SELECT nombre, appat, apmat FROM nominadempleados WHERE numemp='$usuario'";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$nombreusuario=$row['nombre']." ".$row['appat']." ".$row['apmat'];
					
				}
			}
			
			$existe=false;
			$command="SELECT id FROM contabmpoliza WHERE tipo='$tipo_poliza' AND poliza =$numero_poliza";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					//echo "MAMA";
					if(trim($idpoliza) != trim($row['id']))
						$existe=true;	
					
				}
			}
			
			if(!$existe)
			{
		
				if ( is_array( $partidas ) ) // Pregunta se es una array la variable facturas
				{		
					list($id, $fecha, $hora)= fun_contab_A_mpoliza($idpoliza,$tipo_poliza,
									$numero_poliza,
									$descripcion,
									$usuario,
									$fecha_poliza); 
					if(strlen($id)>0)//Para ver si en realidad se genero un vale
					{
						for( $i=0 ; $i  < count($partidas['Cargo']) ; $i++ )
						{
								$partidas['Cargo'][$i]=str_replace(",","",$partidas['Cargo'][$i]);
								$partidas['Credito'][$i]=str_replace(",","",$partidas['Credito'][$i]);
								
								$fails = func_contab_A_dpoliza($partidas['id'][$i],
										$tipo_poliza,
										$numero_poliza, 
										$i,
										$partidas['numcuentacompleta'][$i], 
										$partidas['Concepto'][$i],
										$partidas['Referencia'][$i],
										$partidas['Cargo'][$i],
										$partidas['Credito'][$i],
										'0000',
										$usuario);
										
										$totalCredito+= $partidas['Credito'][$i];
										$totalCargos+= $partidas['Cargo'][$i];
								$tfacturas.="<tr><td  style='width: 15%;' align='center'>".$partidas['numcuentacompleta'][$i]."</td><td style='width: 35%;'>".
								htmlentities($partidas['Concepto'][$i])."</td><td style='width: 10%;' align='center'>".
								$partidas['Referencia'][$i]."</td><td  style='width: 15%;' align='center'>".
								number_format($partidas['Cargo'][$i],2)."</td><td style='width: 15%;' align='center'>".
								number_format($partidas['Credito'][$i],2)."</td></tr>";
								
						}
						
						list($days,$month,$years)= explode('/', $fecha);
						$diadesem=date("w", strtotime("$years-$month-$days")); //date("w");
						$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
							
						$datos[0]['path'] = crearPolizaPDF($id,$ffecha,$tipo_poliza,$numero_poliza, htmlentities($descripcion),$nombreusuario, $tfacturas,number_format($totalCredito,2),number_format($totalCargos,2), $hora, $fecha_polizaNormal);
					}
					else
						{	$fails=true; $msgerror="No pudo generar ningun vale";}
					
				}
				else
					{	$fails=true; $msgerror=" No es un array la variable";}
			}
			else
			{
				$fails=true; $msgerror="Ya existe una poliza con el tipo y poliza especificados.";
			}
		}
		else
		{
			$fails=true; $msgerror="No es un tipo de poliza valido.";
		}
			
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	

	if($fecha_poliza=="true" )
		$datos[0]['id']=trim($id);// regresa el id de la
	else
		$datos[0]['id']=$id_facts;
		
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	if($fails)
	{	echo "<script type='text/javascript'>alert('".$datos[0]['msgerror']."');history.back();</script>'";
		//$back = $_SERVER['HTTP_REFERER'];
		//header("Location: $back");
	}
	else
		header("Location: ../".$datos[0]['path']);
	////Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_contab_A_mpoliza($id,$tipo_poliza,
								$numero_poliza,
								$descripcion,
								$usuario,
								$fecha_poliza)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_contab_A_mpoliza(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$tipo_poliza,
								&$numero_poliza,
								&$descripcion,
								&$usuario,
								$fecha_poliza);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("sp_contab_A_mpoliza".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

//Funcion para registrar el p´roductoa  la requisicion
function func_contab_A_dpoliza($id,
								$tipo,
								$poliza, 
								$mov,
								$cuenta, 
								$concepto,
								$referencia,
								$cargo,
								$credito,
								$estatus,
								$usuario)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//$tsql_callSP ="{call sp_egresos_A_dcheque(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$partidas[$i]['producto'], $partidas[$i]['cantidad'], $partidas[$i]['descripcion'],'0',$partidas[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = $tsql_callSP ="{call sp_contab_A_dpoliza(?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,
								&$tipo,
								&$poliza, 
								&$mov,
								&$cuenta, 
								&$concepto,
								&$referencia,
								&$cargo,
								&$credito,
								&$estatus,
								&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_contab_A_dpoliza". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}

function crearPolizaPDF($id, $fecha,$tipo_poliza,$numero_poliza, $descripcion,$nombreusuario, $tfacturas,$totalCredito,$totalCargos, $hora, $fecha_poliza)
{
	global $imagenPDFPrincipal;
	$dompdf = new DOMPDF();
		$pageheadfoot='<script type="text/php"> 
		if ( isset($pdf) ) 
		{ 	
			$pdf->page_text(30, 760, "Pagina {PAGE_NUM}", "", 10, array(0,0,0)); 
		} 
</script> ';  
	
	$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=latin1'>
<style>
body 
{
	font-family:'Arial';
	font-size:8;
}

table {
  
	vertical-align:top;
	border-collapse:collapse; 
	border: none;
}
td {padding: 0;}

#twmaarco
{
	border: 1pt solid black;
}

.textobold{
	font-weight:bold;
}
.texto14{ 
	font-size: 15pt;
	font-weight:bold;
}

.texto12{ 
	font-size: 12pt;
}
.texto12bold
{
	font-size: 12pt;
	font-weight:bold;
}
.texto11{
	font-size: 11pt;
}
</style>

</head>

<body>
<table style='width: 90%;margin-top: 2em; margin-left: 2em; margin-right: 3em;' width='100%'>
	<tr>
		<td style='width: 100%;' width='100%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 30%; height: 22px;' colspan='2'>
                    	<table style='width: 100%;'>
                        	<tr>
                            	<td style='width: 20%;'><img src='../../../$imagenPDFPrincipal' style='width: 100px; height: 38px;'></td>
                                <td style='width: 80%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
                            </tr>
                        </table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 50%; vertical-align:top;' align='left' class='texto12'>CAPTURA DE POLIZA No. $tipo_poliza-$numero_poliza</td>
								<td style='width: 50%; vertical-align:top;' align='right'>Fecha:  $fecha $hora</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td  align='left'>Concepto: $descripcion</td><td align='right'>".htmlentities("Fecha de P�liza").":    $fecha_poliza</td>
				</tr>
				<tr>
				<td colspan='2'>&nbsp;</td>
				<tr>
                	<td style='width: 100%;' colspan='2'>
						<table style='width: 100%;' >
							<thead>
								<tr>
									<th style='width: 15%;' >Cuenta</th>
									<th style='width: 35%;' >Concepto</th>
									<th style='width: 10%;' >Referencia</th>
									<th style='width: 15%;' >Cargos</th>
									<th style='width: 15%;' >Creditos</th>
								</tr>
								<tr>
									<td colspan='5'><hr></td>
								</tr>
							</thead>
							<tbody>$tfacturas</tbody>
						   <tfoot>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td colspan='3'><hr></td>
								</tr>
								<tr>
									<td style='width: 10%;' >&nbsp;</td>
									<td style='width: 40%;' >&nbsp;</td>
									<td style='width: 10%;' align='right'><b>Total</b></td>
									<td style='width: 15%;' align='center'><b>$totalCargos</b></td>
									<td style='width: 15%;' align='center'><b>$totalCredito</b></td>
								</tr>
							</tfoot>
					   </table>
                   	</td>
                </tr>
				<tr>
					<td colspan='2'>
						<table style='width: 70%;'>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'>$nombreusuario</td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'><HR></td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'><HR></td>
                            </tr>
                            <tr>
                                <td style='width: 40%;' align='center' >CAPTURO</td><td style='width: 20%;'>&nbsp;</td><td style='width: 40%;' align='center'  >Vo. Bo.</td>
                            </tr>
						</table>
					</td>
				</tr>
			   </table>
		</td>
	</tr>

</table>
".$pageheadfoot."
</body> </html>";

	//$htmlstr=utf8_encode($htmlstr);
	$dompdf->load_html($htmlstr);
	//$dompdf->set_paper('letter', 'landscape');
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/poliza_".$id.".pdf", $pdf);
	
	return("pdf_files/poliza_".$id.".pdf");
}
?>