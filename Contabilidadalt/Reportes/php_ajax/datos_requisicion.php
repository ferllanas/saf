<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$usuario = $_COOKIE['ID_my_site'];//"001981";//
$requi= $_REQUEST['requi'];

$cuadros=array();//prepara el aray que regresara

$command= "SELECT  c.cuadroc  FROM compradrequisi a 
	INNER JOIN compradcuadroc b ON a.id=b.iddrequisi 
	INNER JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
WHERE requisi='$requi' GROUP BY c.cuadroc";


$stmt2 = sqlsrv_query( $conexion , $command );
$i=0;
if ( $stmt2 === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
}
else
{
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$cuadros[$i]['cuadro']=trim($row['cuadroc']);
		$cuadros[$i]['requi'] = $requi;
		$i++;
	}
}

if($i>=1)
{
	$command= "SELECT  d.orden  FROM compradrequisi a 
	INNER JOIN compradcuadroc b ON a.id=b.iddrequisi 
	INNER JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
	INNER JOIN compramordenes d ON d.cuadroc=c.cuadroc
	WHERE requisi='$requi' GROUP BY d.orden";
	$i=0;
	$stmt3 = sqlsrv_query( $conexion , $command );
	if ( $stmt3 === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		while( $row = sqlsrv_fetch_array($stmt3, SQLSRV_FETCH_ASSOC))
		{
			// Comienza a realizar el arreglo, trim elimina espacios en blanco		
			$orden[$i]['orden']=trim($row['orden']);
			$orden[$i]['cuadroc']=trim($row['cuadroc']);
			$i++;
		}
	}
	
	
	if($i>=1)
	{
		$command= "SELECT  e.surtido, e.orden FROM compradrequisi a 
		INNER JOIN compradcuadroc b ON a.id=b.iddrequisi 
		INNER JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
		INNER JOIN compramordenes d ON d.cuadroc=c.cuadroc
		INNER JOIN compramsurtido e ON e.orden=d.orden
		WHERE requisi='$requi' GROUP BY e.surtido, e.orden";
		$i=0;
		$stmt3 = sqlsrv_query( $conexion , $command );
		if ( $stmt3 === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			while( $row = sqlsrv_fetch_array($stmt3, SQLSRV_FETCH_ASSOC))
			{
				// Comienza a realizar el arreglo, trim elimina espacios en blanco		
				$surtidos[$i]['surtido']=trim($row['surtido']);
				$surtidos[$i]['orden']=trim($row['orden']);
				$i++;
			}
		}
		
		if($i>=1)
		{
			$command= "SELECT  e.surtido, e.orden FROM compradrequisi a 
			INNER JOIN compradcuadroc b ON a.id=b.iddrequisi 
			INNER JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
			INNER JOIN compramordenes d ON d.cuadroc=c.cuadroc
			INNER JOIN compramsurtido e ON e.orden=d.orden
			WHERE requisi='$requi' GROUP BY e.surtido, e.orden";
			$i=0;
			$stmt3 = sqlsrv_query( $conexion , $command );
			if ( $stmt3 === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				while( $row = sqlsrv_fetch_array($stmt3, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$orden[$i]['surtido']=trim($row['surtido']);
					$orden[$i]['orden']=trim($row['orden']);
					$i++;
				}
			}
		}
	}
}
?>