﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	//$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	
	$msgerror="";
	$fails=false;
	$datos=array();
	$anio="";
	$mes="";
	$cta="";
	if(isset($_REQUEST['anio']))
		$anio=$_REQUEST['anio'];

	if(isset($_REQUEST['mes']))
		$mes=$_REQUEST['mes'];
		
	//$numero_poliza = convertirFechaEuropeoAAmericano($numero_poliza);

	if(isset($_REQUEST['cta']))
			$cta=$_REQUEST['cta'];

		
//Funcion para registrar el p´roductoa  la requisicion

	global $server,$odbc_name,$username_db ,$password_db;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$params = $tsql_callSP ="{call sp_contab_c_tot_x_ctas_univel(?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$anio,&$mes,&$cta);//Arma parametros de entrada
	//echo $tsql_callSP;
	//print_r($params);
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);	
	if( $stmt )
	{
		 $i=0;
		 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		 {
			$datos[$i]['cuenta']=$row['cuenta']; 
			$datos[$i]['nombre']=$row['nombre'];
			$datos[$i]['cargo']=$row['cargos'];
			$datos[$i]['credito']=$row['creditos'];
			$i++;
		 }
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_contab_c_tot_x_ctas_univel". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	echo json_encode($datos);
	//return $fails;


?>