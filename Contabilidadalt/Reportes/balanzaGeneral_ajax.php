<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$anio="";
$mes="";

$datos=array();
$anio= substr($_REQUEST['query'],0,4);
$mes= substr($_REQUEST['query'],4,2);

//$cta= substr($_REQUEST['query'],6);
//if(!isset($cta))
//	$cta="";
$command="";
if(isset($_REQUEST['cuenta']) && strlen($_REQUEST['cuenta'])>0){
	$command="SELECT a.id as idmpoliza, a.descrip as nombre, a.tipo, a.poliza, CONVERT(varchar(10),a.fecha,103) as fecha,
b.cuenta, b.credito as creditos, b.cargo as cargos, c.ultimonivel
FROM contabmpoliza a 
LEFT JOIN contabdpoliza b ON a.poliza=b.poliza 
LEFT JOIN contabmcuentas c ON c.cuenta=b.cuenta 
WHERE YEAR(a.fecha)='$anio' AND MONTH(a.fecha)='$mes' AND b.cuenta='".$_REQUEST['cuenta']."'";
}
else{
	$command="sp_contab_balanza_gral_alt $anio,$mes";
}
//echo $command;
$stmt = sqlsrv_query($conexion, $command);
if( $stmt )
{
	 $i=0;
	 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	 {

		$datos[$i]['cuenta']= trim($row['cuenta']); 
		$datos[$i]['nombre']= utf8_encode($row['nombre']);
		$datos[$i]['cargo']=	number_format($row['cargos'],2);
		$datos[$i]['credito']= 	number_format($row['creditos'],2);
		$datos[$i]['ultimonivel']= 	$row['ultimonivel'];
		
		if(isset($_REQUEST['cuenta']) && strlen($_REQUEST['cuenta'])>0)
		{
			$datos[$i]['fecha']= 	$row['fecha'];
		}
		else
		{
			$datos[$i]['carini']= 	number_format($row['carini'],2);
			$datos[$i]['creini']= 	number_format($row['creini'],2);
			$datos[$i]['carfin']= 	number_format($row['carfin'],2);
			$datos[$i]['crefin']= 	number_format($row['crefin'],2);
			
		}
		$i++;
	 }
	 sqlsrv_free_stmt($stmt);
}
sqlsrv_close($conexion);
//$datos['anio']=$anio;
//$datos['mes']=$mes;
echo json_encode($datos);   // Los codifica con el jason
?>