<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />  
        <link href="../../css/esperaicon.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        <script src="javascript/navegacion.js"></script>
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			
				
			
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
				$("#FormularioExportacion").submit();
			});
			
		});
            $(function()
			{
			$("#detallediv").hide();
			
			
				$('#div_carga')
					.hide()
					.ajaxStart(function() {
						$(this).show();
					})
					.ajaxStop(function() {
						$(this).hide();
					})
				;
		
                $('#query').live('keydown', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('poliza_reporte_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						//var opcion = document.getElementById('opc').value;
						var anio = document.getElementById('anio').value;
						var mes = document.getElementById('mes').value;
						//var cta = document.getElementById('numcuentacompleta').value;	  	
						if(mes.length==1)
						{
							mes="0" + mes;
						}						
						var data = 'query=' + anio + mes ;     // Toma el valor de los datos, que viene del input						
						//alert(data);
						console.log('balanzaGeneral_ajax.php'+data);
						$.post('balanzaGeneral_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							console.log(resp),
							//alert(resp.response);
                      	  	$('#proveedor').empty();
							if(resp.length<=0)
							{
								alert("No existen resultados para estos criterios.");
								document.getElementById('anio').value="";
								document.getElementById('mes').value="";
								//document.getElementById('numcuentacompleta').value="";
								//document.getElementById('nomsubcuenta').value="";
							}
                        	$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
							
							
							
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			
			function viewDetalle(objeto){
				var anio = document.getElementById('anio').value;
				var mes = document.getElementById('mes').value;
				var cuenta = $(objeto).attr('cuenta');
				//var cta = document.getElementById('numcuentacompleta').value;	  	
				if(mes.length==1)
				{
					mes="0" + mes;
				}						
				var data = 'query=' + anio + mes +"&cuenta="+cuenta;     // Toma el valor de los datos, que viene del input		
				
				$("#detallediv").show();				
				//alert(data);
				//$("#detallediv").css('visibility','visible');
				console.log('balanzaGeneral_ajax.php'+data);
				$.post('balanzaGeneral_ajax.php',data, function(resp)
				{ //Llamamos el arch ajax para que nos pase los datos
				
					console.log(resp);
					//alert(resp.response);
					$('#detallebody').empty();
					if(resp.length<=0)
					{
						alert("No existen resultados para estos criterios.");
						//document.getElementById('anio').value="";
						//document.getElementById('mes').value="";
						//document.getElementById('numcuentacompleta').value="";
						//document.getElementById('nomsubcuenta').value="";
					}
					$('#tmpl_detalle').tmpl(resp).appendTo('#detallebody'); 
							
							     // los va colocando en la forma de la tabla
                 }, 'json');  // Json es una muy buena opcion         
				
			}
			
        </script>
         <script id="tmpl_detalle" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left" style="width:20%" class="text" style='mso-number-format:"\@";'><a href="#" onclick="viewDetalle(this)" cuenta="${cuenta}">${cuenta}</a></td>
					<td class="text" style='width:50%;mso-number-format:"\@";'><a href="#" onclick="viewDetalle(this)" cuenta="${cuenta}">${nombre}</a></td>
					<td align="right" style='width:10%;'>${cargo}</td>
					<td align="right" style='width:10%;'>${credito}</td>	
					<td align="right" style='width:10%;'>${fecha}</td>-->
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left" class="text" style='mso-number-format:"\@";'>{{if ultimonivel==1}}<a href="#" onclick="viewDetalle(this)" cuenta="${cuenta}">${cuenta}</a>
																		{{else}} ${nombre}  
																		{{/if}} 
																		</td>
					<td class="text" style='mso-number-format:"\@";'> {{if ultimonivel==1}}<a href="#" onclick="viewDetalle(this)" cuenta="${cuenta}">${nombre}</a>
																		{{else}} ${nombre}  
																		{{/if}} 
																		</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
			
			.num {
			  mso-number-format:General;
			}
			.text{
			  mso-number-format:"\@";/*force text*/
			}
			
        </style>
</head>
    <body>
    <span class="TituloDForma">Balanza General</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>


<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->

<td><!-- Asignamos variables de fechas -->
  Año:<input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="1">    
  Mes:<input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="2"></td>
 <td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()" tabindex="5">
</td>
<td>
<input type="hidden" name="ren" id="ren">
    <form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th width="60px" class="text" >Cuenta</th>  
                <th width="250px" class="text">Nombre de la Cuenta</th> 
                <th width="40px" class="num">Cargo inicial</th>
                <th width="40px" class="num">Credito inicial</th>                                        
                <th width="40px" class="num">Cargos</th>
                <th width="40px"  class="num">Creditos</th>   
                <th width="40px"  class="num">Cargo Final</th>
                <th width="40px"  class="num">Credito Final</th>    
                <!--<th width="40px">Saldo Inicial</th>-->
                <!-- <th width="40px">Saldo final</th>            -->
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
  

 </div>
 <div id="detallediv" style=" height:100%; width:100%;position:absolute; left:0px; top:0px; display:none; background-color:rgba(0,0,0,0.55)" >
 
 	<img src="../../imagenes/delete.png" style="position:relative;left:80%; top:110px; z-index:4;" onClick="$('#detallediv').hide();">
    <table id="detalletable" border="solid black 1px"  style=" border-collapse:collapse; background-color:rgba(255,255,255,1.00); width:800px; position:relative; left:20%; top: 100px; border:solid black 4px; display: block;height: 500px;overflow-y: scroll;">
    <caption align="center">Detalle</caption>
            <thead bgcolor="#32AE4F" style="display:block; width:800px;" >
                <th style="width:20%;" class="text" >Cuenta</th>  
                <th style="width:50%;"  class="text">Decripcion</th> 
                <th style="width:10%;"  class="num">Cargo</th>
                <th style="width:10%;"  class="num">Credito</th>                                        
                <th style="width:10%;"  class="num">fecha</th>
            </thead>
            <tbody id="detallebody" name='detallebody' style="display:block; overflow-y: scroll; height:500px; width:800px;">
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">Encontrar Resultados</td>
                </tr>
            </tbody>
    </table>
    
</div>
<div id="div_carga">
   		<img src="../../imagenes/ajax-loader.gif" width="64" id="cargador">
    </div>
    
    </body>
</html>
