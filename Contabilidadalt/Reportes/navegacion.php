<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
require_once("../../connections/dbconexion.php");
$query="";
if(isset($_REQUEST['valor']))
	{
		
		switch($_REQUEST['tipodoc'])
		{
			case 1:
				$query="SELECT  a.requisi, c.cuadroc, d.orden, e.surtido, f.vale as vale, g.folio as cheque,
						h.nomprov
						FROM compradrequisi a 
							LEFT JOIN compradcuadroc b ON a.id=b.iddrequisi 
							LEFT JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
							LEFT JOIN compramordenes d ON d.cuadroc=c.cuadroc
							LEFT JOIN compramsurtido e ON e.orden=d.orden
							LEFT JOIN egresosdvale f ON e.surtido=f.numdocto AND f.tipodocto=1
							LEFT JOIN egresosdcheque g ON g.numdocto= f.id AND g.tipodocto=5
							LEFT JOIN compramprovs h ON h.prov=d.prov
						WHERE requisi='".$_REQUEST['valor']."'";
			break;
			
			case 2:
				$query="SELECT  a.requisi, c.cuadroc, d.orden, e.surtido, f.vale as vale, g.folio as cheque,
						h.nomprov
						FROM compradrequisi a 
							LEFT JOIN compradcuadroc b ON a.id=b.iddrequisi 
							LEFT JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
							LEFT JOIN compramordenes d ON d.cuadroc=c.cuadroc
							LEFT JOIN compramsurtido e ON e.orden=d.orden
							LEFT JOIN egresosdvale f ON e.surtido=f.numdocto AND f.tipodocto=1
							LEFT JOIN egresosdcheque g ON g.numdocto= f.id AND g.tipodocto=5
							LEFT JOIN compramprovs h ON h.prov=d.prov
						WHERE c.cuadroc='".$_REQUEST['valor']."'";
			break;
			
			case 3:
				$query="SELECT  a.requisi, c.cuadroc, d.orden, e.surtido, f.vale as vale, g.folio as cheque,
						h.nomprov
					FROM compradrequisi a 
						LEFT JOIN compradcuadroc b ON a.id=b.iddrequisi 
						LEFT JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
						LEFT JOIN compramordenes d ON d.cuadroc=c.cuadroc
						LEFT JOIN compramsurtido e ON e.orden=d.orden
						LEFT JOIN egresosdvale f ON e.surtido=f.numdocto AND f.tipodocto=1
						LEFT JOIN egresosdcheque g ON g.numdocto= f.id AND g.tipodocto=5
						LEFT JOIN compramprovs h ON h.prov=d.prov
					WHERE d.orden='".$_REQUEST['valor']."'";
			break;
					
			case 4:
				$query="SELECT  a.requisi, c.cuadroc, d.orden, e.surtido, f.vale as vale, g.folio as cheque,
						h.nomprov
					FROM compradrequisi a 
						LEFT JOIN compradcuadroc b ON a.id=b.iddrequisi 
						LEFT JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
						LEFT JOIN compramordenes d ON d.cuadroc=c.cuadroc
						LEFT JOIN compramsurtido e ON e.orden=d.orden
						LEFT JOIN egresosdvale f ON e.surtido=f.numdocto AND f.tipodocto=1
						LEFT JOIN egresosdcheque g ON g.numdocto= f.id AND g.tipodocto=5
						LEFT JOIN compramprovs h ON h.prov=d.prov
					WHERE e.surtido='".$_REQUEST['valor']."'";
			break;
					
			case 5:
				$query="SELECT  a.requisi, c.cuadroc, d.orden, e.surtido, f.vale as vale, g.folio as cheque,
						h.nomprov
					FROM compradrequisi a 
						LEFT JOIN compradcuadroc b ON a.id=b.iddrequisi 
						LEFT JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
						LEFT JOIN compramordenes d ON d.cuadroc=c.cuadroc
						LEFT JOIN compramsurtido e ON e.orden=d.orden
						LEFT JOIN egresosdvale f ON e.surtido=f.numdocto AND f.tipodocto=1
						LEFT JOIN egresosdcheque g ON g.numdocto= f.id AND g.tipodocto=5
						LEFT JOIN compramprovs h ON h.prov=d.prov
					WHERE f.vale='".$_REQUEST['valor']."'";
			break;
			case 7:
				$query="SELECT  a.requisi, c.cuadroc, d.orden, e.surtido, f.vale as vale, g.folio as cheque,
							h.nomprov
						FROM compradrequisi a 
							LEFT JOIN compradcuadroc b ON a.id=b.iddrequisi 
							LEFT JOIN compramcuadroc c ON b.cuadroc=c.cuadroc
							LEFT JOIN compramordenes d ON d.cuadroc=c.cuadroc
							LEFT JOIN compramsurtido e ON e.orden=d.orden
							LEFT JOIN egresosdvale f ON e.surtido=f.numdocto AND f.tipodocto=1
							LEFT JOIN egresosdcheque g ON g.numdocto= f.id AND g.tipodocto=5
							LEFT JOIN compramprovs h ON h.prov=d.prov
						WHERE g.folio='".$_REQUEST['valor']."'";
			break;
		}
	}
function getDatos($query)
{
	global  $username_db,$password_db, $odbc_name, $server;
	$datos = array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$stmt2 = sqlsrv_query( $conexion,$query);
	$i=0;
	$increm_req=0;
	$increm_cuadro=0;
	$increm_orden=0;
	$increm_surtido=0;
	$increm_vale=0;
	$increm_cheque=0;
	$increm_solcheque=0;
	$increm_prov=0;
	if($stmt2)
	{
		while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
		{
			if($i==0)
			{
				$datos[$increm_req]['requisi']=trim($row['requisi']);
				$datos[$increm_cuadro]['cuadroc']=trim($row['cuadroc']);
				$datos[$increm_orden]['orden']=trim($row['orden']);
				$datos[$increm_surtido]['surtido']=trim($row['surtido']);
				$datos[$increm_vale]['vale']=trim($row['vale']);
				$datos[$increm_cheque]['cheque']=trim($row['cheque']);
				if(isset($row['solcheque']))
					$datos[$increm_solcheque]['solcheque']=trim($row['solcheque']);
				else
					$datos[$increm_solcheque]['solcheque']="";
					
				if(isset($row['nomprov']))
					$datos[$increm_solcheque]['nomprov']=trim($row['nomprov']);
				else
					$datos[$increm_solcheque]['nomprov']="";
				
				$increm_req++;
				$increm_cuadro++;
				$increm_orden++;
				$increm_surtido++;
				$increm_vale++;
				$increm_cheque++;
				$increm_solcheque++;
				$increm_prov++;
			}
			else
			{
				if(agregaRequisi($datos,trim($row['requisi']),$increm_req))
					$increm_req++;
				else
				{
					$datos[$increm_req]['requisi']="";
					$increm_req++;
				}
					
				if(agregaCuadro($datos,trim($row['cuadroc']),$increm_cuadro))
					$increm_cuadro++;
				else
				{
					$datos[$increm_cuadro]['cuadroc']="";
					$increm_cuadro++;
				}
					
				if(agregaoOrden($datos,trim($row['orden']),$increm_orden))
					$increm_orden++;
				else
				{
					$datos[$increm_orden]['orden']="";
					$increm_orden++;
				}

				if(agregaoSurtido($datos,trim($row['surtido']),$increm_surtido))
					$increm_surtido++;
				else
				{
					$datos[$increm_surtido]['surtido']="";
					$increm_surtido++;
				}
					
				if(agregaoVale($datos,trim($row['vale']),$increm_vale))
					$increm_vale++;
				else
				{	$datos[$increm_vale]['vale']="";
					$increm_vale++;
				}
					
				if(agregaoCheque($datos,trim($row['cheque']),$increm_cheque))
					$increm_cheque++;
				else
				{
					$datos[$increm_cheque]['cheque']="";
					$increm_cheque++;
				}
					
				if(isset($row['solcheque']))
					if(agregaoSolCheque($datos,trim($row['solcheque']),$increm_solcheque))
						$increm_solcheque++;
					else
					{
						$datos[$increm_solcheque]['solcheque']="";
						$increm_solcheque++;
					}
				else
				{	$datos[$increm_solcheque]['solcheque']="";
					$increm_solcheque++;
				}
				
				$datos[$increm_prov]['nomprov']=trim($row['nomprov']);
				$increm_prov++;
				/*if(agregaoProv($datos,trim($row['nomprov']),$increm_prov))
					$increm_prov++;
				else
				{
					$datos[$increm_prov]['nomprov']="";
					
				}*/
			}
			$i++;
		}
	}
	else
	{
		echo "Error";
	}
	
	return $datos;
}
function agregaoProv(&$datos,$requisi,$i)
{
	$encontro=false;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['nomprov']==$requisi)
			$encontro= true;
	}
	if(!$encontro)
		$datos[$i]['nomprov']=$requisi;
		
	
	return !$encontro;
}

function agregaRequisi(&$datos,$requisi,$i)
{
	$encontro=false;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['requisi']==$requisi)
			$encontro= true;
	}
	if(!$encontro)
		$datos[$i]['requisi']=$requisi;
		
	
	return !$encontro;
}

function agregaCuadro(&$datos,$cuadroc,$i)
{
	$encontro=false;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['cuadroc']==$cuadroc)
			$encontro= true;
	}
	if(!$encontro)
		$datos[$i]['cuadroc']=$cuadroc;
		
	
	return !$encontro;
}

function agregaoOrden(&$datos,$orden,$i)
{
	$encontro=false;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['orden']==$orden)
			$encontro= true;
	}
	if(!$encontro)
		$datos[$i]['orden']=$orden;
		
	
	return !$encontro;
}

function agregaoSurtido(&$datos,$surtido,$i)
{
	$encontro=false;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['surtido']==$surtido)
			$encontro= true;
	}
	if(!$encontro)
		$datos[$i]['surtido']=$surtido;
		
	
	return !$encontro;
}

function agregaoVale(&$datos,$vale,$i)
{
	$encontro=false;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['vale']==$vale)
			$encontro= true;
	}
	if(!$encontro)
		$datos[$i]['vale']=$vale;
		
	
	return !$encontro;
}

function agregaoCheque(&$datos,$cheque,$i)
{
	$encontro=false;
	//echo $cheque;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['cheque']==$cheque)
			$encontro= true;
	}
	
	if(!$encontro)
	{
		$datos[$i]['cheque']= $cheque;
	}
	
		
	
	return !$encontro;
}

function agregaoSolCheque(&$datos,$cheque,$i)
{
	$encontro=false;
	echo $cheque;
	for($j=0; $j<$i && $encontro==false ;$j++)
	{
		if($datos[$j]['solcheque']==$cheque)
			$encontro= true;
	}
	
	if(!$encontro)
	{
		echo "no se encontro ".$i;
		$datos[$i]['solcheque']= $cheque;
	}
	else
		echo "si se encontro";
		
	
	return !$encontro;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin1" />
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="javascript/navegacion.js"></script>
<title>Navegaci&oacute;n</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="navegacion.php">
<table width="100%" border="0" align="center">

    <tr>
    	<td class="TituloDForma" colspan="5">Navegaci&oacute;n</td class="texto8">
    </tr>	
    <tr>
    	<td width="5%">Documento:</td>
    	<td width="13%"><select name="tipodoc" id="tipodoc">
    	  <option value="1" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==1) echo "SELECTED"; ?>>Requisici&oacute;n</option>
    	  <option value="2" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==2) echo "SELECTED"; ?>>Cuadro Comparativo</option>
    	  <option value="3" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==3) echo "SELECTED"; ?>>Orden de Compra</option>
    	  <option value="4" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==4) echo "SELECTED"; ?>>Surtido</option>
    	  <option value="5" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==5) echo "SELECTED"; ?>>Vales</option>
    	  <option value="6" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==6) echo "SELECTED"; ?>>Solitud de Cheque</option>
    	  <option value="7" <?php if(isset($_REQUEST['tipodoc']))if($_REQUEST['tipodoc']==7) echo "SELECTED"; ?>>Cheque</option>
    </select></td>
        <td width="13%"><input type="text" id="valor" name="valor" value="<?php if(isset($_REQUEST['valor']))echo $_REQUEST['valor'];?>"  /></td>
         <td width="69%"><input type="submit" value="buscar" /></td>
    </tr>
</table>
<table border="1" align="center">
<thead>
	<tr>
	<th width="100/8%" class="subtituloverde8">Requisici&oacute;n</th>
	<th width="100/8%" class="subtituloverde8">Cuadro Comparativo</th>
	<th width="100/8%" class="subtituloverde8">Orden de Compra</th>
	<th width="100/8%" class="subtituloverde8">Surtido</th>
	<th width="100/8%" class="subtituloverde8">Vale</th>
   	<th width="100/8%" class="subtituloverde8">Solicitud de Cheque</th>
    <th width="100/8%" class="subtituloverde8">Cheque</th>
    <th width="200px" class="subtituloverde8">Proveedor</th>
    </tr>
</thead>

<?php 
			//-- GROUP BY g.folio, f.vale

			//echo $query;
			$datos = getDatos($query);
			for($i=0; $i<count($datos); $i++)
			{
				if( strlen($datos[$i]['requisi'])>0 || strlen($datos[$i]['cuadroc'])>0 || strlen($datos[$i]['orden'])>0 || 
					strlen($datos[$i]['surtido'])>0 || strlen($datos[$i]['vale'])>0 || strlen($datos[$i]['solcheque'])>0 ||
					strlen($datos[$i]['cheque'])>0 )
				{
?>        	
            <tr>
                <td align="center" class="texto8"><a href="../../Compras/comrequisicion/pdf_files/<?php echo $datos[$i]['requisi'];?>.pdf"><?php echo $datos[$i]['requisi'];?><a></td>
                <td align="center" class="texto8"><a href="../../Compras/comordendcompra/pdf_files/cuadroc_<?php echo $datos[$i]['cuadroc'];?>.pdf"><?php echo $datos[$i]['cuadroc'];?></a></td>
                <td align="center" class="texto8"><a href="../../Compras/comordendcompra/pdf_files/OrdCompra_<?php echo $datos[$i]['orden'];?>.pdf"><?php echo $datos[$i]['orden'];?></a></td>
                <td align="center" class="texto8"><a href="../../Compras/surtido/pdf_files/acusedsurtido_<?php echo $datos[$i]['surtido'];?>.pdf"><?php echo $datos[$i]['surtido'];?></a></td>
                <td align="center" class="texto8"><a href="../../egresos/vales/pdf_files/vale_<?php echo $datos[$i]['vale'];?>.pdf"><?php echo $datos[$i]['vale'];?></a></td>
                <td align="center" class="texto8"><?php echo $datos[$i]['solcheque'];?></td>
                <td align="center" class="texto8"><a href="../../egresos/impreCheques/pdf_files/cheque_<?php echo $datos[$i]['cheque'];?>.pdf"><?php echo $datos[$i]['cheque'];?></a></td>
                 <td align="center" class="texto8"><?php echo $datos[$i]['nomprov'];?></td>
            </tr>     
<?php	
				}
			}

?>
</table>
 </form>
</body>
</html>