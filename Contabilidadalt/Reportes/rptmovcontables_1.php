<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Reporte de Polizas Contables</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="javascript/divhide.js"></script>
        <script src="javascript/navegacion.js"></script>
        <script src="javascript/jquery.tmpl.js"></script>
		<script src="javascript/funciones_cheque.js"></script>
		<script src="javascript_globalfunc/funcionesGlobales.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
        <link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
		$(".botonExcel").click(function(event) {		
			$("#datos_a_enviar").val( $("<div>").append( $("#sortable").eq(0).clone()).html());
			$("#FormularioExportacion").submit();		
});
});
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('busca_cheque3.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);

						$.post('busca_cheque3.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						//alert(resp.length);

							if (resp.length<1)
							{
								document.getElementById('opc').value=1;
								document.getElementById('query').value='';
								document.getElementById('fecini').value='';
								document.getElementById('fecfin').value='';
								ShowHidden();
							}
						
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }

			
			function cambia_estatus(folio,x)
			{
//				var pregunta = confirm("Esta seguro que desea afectar Cheque")
//				if (pregunta)
//				{

				if (x==1)
				{
					var pregunta1 = confirm("Esta seguro que desea Afectar esta Solicitud  -- "+folio+"")
						if (pregunta1)
						{
							var datas = {
								 pfolio: folio,
								 px: x,
								 };
							
							jQuery.ajax({
								type: 'post',
								cache:	false,
								url:'php_ajax/solcheque_recibido.php',
								dataType: "json",
								data: datas,
								error: function(request,error) 
								{
								 console.log(arguments);
								 alert ( " Can't do because: " + error );
								},
								success: function(resp)
								{ console.log(arguments);
									var myObj = resp;
									
									///NOT how to print the result and decode in html or php///
									console.log(myObj);
			
									if(resp.length>0)
									{
										alert("Folio de Recepción No. "+resp);
										location.href="recepcion_cheques.php";
									}
									else
									{
										alert("Actualizacion Rechazada.");
									}
								}
								
								});
						}	
				}
				if (x==2)
				{
					var pregunta = confirm("Esta seguro que desea cancelar esta Solicitud  -- "+folio+"")
					if (pregunta)				
						location.href="php_ajax/cancela_sol_cheque.php?folio="+folio;
				}
				if (x==3)
				{
					window.open('concepto.php?folio='+folio+'',"Editar","width=900, height=400, top=180, left=200");
					//new Ajax.Request('php_ajax/busca_factura.php?factura='+factura+"&prov="+prov,
				}
				
				
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl"> 
            <tr>
                {{if nomprov}}
					<td>${folio}</td>
					<td>${nomprov}</td>
					<td>${fecha}</td>
					<td align="right">${importe}</td>
					<td>${concepto}</td> 
					{{if estatus==0}}
						<td id="estatus"><img src="../imagenes/actualiza3.png" width="30" height="25" onClick="cambia_estatus(${folio},1)"></td>
						<td><img src="../imagenes/eliminar.jpg" width="30" height="25" onClick="cambia_estatus(${folio},2)"></td>
						<td><img src="../imagenes/edit-icon.png" width="30" height="25" onClick="cambia_estatus(${folio},3)"></td>
					{{/if}}
					{{if estatus==5}}
						<td colspan="2" align="center"><img src="../imagenes/consultar.jpg" width="30" height="25"  onClick="window.open('pdf_files/'+${folio}+'.pdf')"></td>
					{{/if}}
					
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   

<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Balanza General</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>
<td class="texto8" width="3%">A&ntilde;o:</td>
<td class="texto8" width="4%"><input name="anio" id="anio" maxlength="4" style="height: 20px;width: 40px;"  /></td>
<td class="texto8" width="3%">Mes:</td>
<td width="11%"><input name="mes" id="mes" maxlength="2" style="height: 20px;width: 20px;" onBlur="Validames();" /><input name="desmes" id="desmes" style="height: 20px;width: 80px;" readonly  /></td>
        <td class="texto8" colspan="3"><div align="left" style="z-index:0; position:relative; width:450px">Nombre de la cuenta:
                <input tabindex="6"  class="texto8" name="nomsubcuenta" type="text" id="nomsubcuenta" size="50" style="width:250px;"  onkeyup="searchSuggest();" autocomplete="off"/>
                <input type="text" class="texto8" name="numcuentacompleta" id="numcuentacompleta" size="50" style="width:70px;" readonly />
                <div id="search_suggest" style="z-index:2; position:absolute;" > </div>
              </div></td>
              <td class="texto8" width="10%" valign="bottom" align="left"><input tabindex="12" class="texto8" type="button" value="Buscar" onClick="BuscaMovimientos()" /><input tabindex="12" class="texto8" type="button" value="Limpiar" onClick="Limpiacampos()" /></td>
      <td class="texto8" width="20%"> <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
<p><img src="../../imagenes/a_excel.jpg" class="botonExcel" style="width:30px" /></p>
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form></td>
</tr>
<tr>
      <td colspan="8" class="texto8">
      <table class="tableone" id="sortable" border="0" style="width:750px">
      <thead>
        <tr>
          <th style="width:100px">Cuenta</th>
          <th style="width:480px">Nombre de Cuenta</th>
          <th style="width:100px">Cargo</th>
          <th style="width:120px">Credito</th>         
        </tr>
      </thead>
       <tbody >
                <tr>
                    <td colspan="6">
                        <div class="innerb" style="width:800px;height:50em;">
                            <table class="tabletwo"  style="width:800px;">
                                <tbody  name="tfacturas" id="tfacturas" >
                                   
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
      </table></td class="texto8">
    </tr>
</table>

    </div>
    </body>
</html>
