﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");
	$fecha_poliza="";
	$tipo_poliza="";
	$nombreusuario="";
	$tfacturas="";
	
	$fecha="";
	$conceptos="";
	//$concepto="";
	$numero_poliza="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$partidas="";//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$descripcion="";
	$id_facts="";
	$fecpoliza="";
	if(isset($_REQUEST['partidas']))
			$partidas=$_POST['partidas'];
	
	//print_r($partidas);
	if(isset($_REQUEST['fecha_poliza']))
		$fecha_poliza=$_POST['fecha_poliza'];
		
	$fechapolizanormal=$fecha_poliza;
	
	
	if(isset($_REQUEST['tipo_poliza']))
		$tipo_poliza=$_POST['tipo_poliza'];

	if(isset($_REQUEST['numero_poliza']))
		$numero_poliza=$_POST['numero_poliza'];
		
	//$numero_poliza = convertirFechaEuropeoAAmericano($numero_poliza);

	if(isset($_REQUEST['descripcion']))
			$descripcion=$_POST['descripcion'];

	//if(isset($_REQUEST['fecpoliza']))
	//	$fecpoliza = $_POST['fecpoliza'];
		
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	
	$conexion_srv = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	if(!$conexion_srv)
		echo "Error de conexion";
	
	$fecha_poliza=convertirFechaEuropeoAAmericano($fecha_poliza);
	//echo "XXXXXXXAAA=".$fecha_poliza;
	//echo "AAAAAAAA";
	if(isset($usuario))
	{
		
		//if($tipo_poliza=='I' || $tipo_poliza=='D' || $tipo_poliza=='E')
		//{
			//Obtencion de nombre de usuario
			$command="SELECT nombre, appat, apmat FROM [nomemp].[dbo]. nominadempleados WHERE numemp='$usuario'";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= $command;
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$nombreusuario=$row['nombre']." ".$row['appat']." ".$row['apmat'];
					
				}
			}
			
			$existe=false;
			$command="SELECT id FROM contabmpoliza_alt WHERE tipo='$tipo_poliza' AND poliza =$numero_poliza and estatus<'9000'";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= $descriptioncode= $command;
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					//echo "MAMA";
					$existe=true;	
					
				}
			}
			
			if(!$existe)
			{
		
				if ( is_array( $partidas ) ) // Pregunta se es una array la variable facturas
				{		
					list($id, $fecha, $hora)= fun_contab_A_mpoliza("0",$tipo_poliza,
									$numero_poliza,
									$descripcion,
									$usuario,
									$fecha_poliza); 
					if(strlen($id)>0)//Para ver si en realidad se genero un vale
					{
						$j=0;
						for( $i=0 ; $i  < count($partidas) ; $i++ )
						{
							
								$j=$i+1;
								$partidas[$i]['Cargo']=str_replace(",","",$partidas[$i]['Cargo']);
								$partidas[$i]['Credito']=str_replace(",","",$partidas[$i]['Credito']);
								
								$fails = func_contab_A_dpoliza("0",$id,
										$tipo_poliza,
										$numero_poliza, 
										$j,
										$partidas[$i]['numcuentacompleta'], 
										$partidas[$i]['Concepto'],
										$partidas[$i]['Referencia'],
										$partidas[$i]['Cargo'],
										$partidas[$i]['Credito'],
										'0000',
										$usuario,
										$partidas[$i]['Origen']);
										
										$totalCredito+= $partidas[$i]['Credito'];
										$totalCargos+= $partidas[$i]['Cargo'];
										
								$tfacturas.="<tr><td  style='width: 15%;' align='center'>".$fechapolizanormal.
								"</td><td  style='width: 15%;' align='center'>".$partidas[$i]['numcuentacompleta'].
								"</td><td style='width: 35%;'>".utf8_decode($partidas[$i]['Concepto']).
								"</td><td style='width: 10%;' align='center'>".$partidas[$i]['Referencia'].
								"</td><td  style='width: 15%;' align='center'>".number_format($partidas[$i]['Cargo'],2).
								"</td><td style='width: 15%;' align='center'>".number_format($partidas[$i]['Credito'],2)."</td></tr>";//<td class='texto11'>".trim($fecha_poliza)."</td>
								
						}
						
						list($days,$month,$years)= explode('/', $fecha);
						$diadesem=date("w", strtotime("$years-$month-$days")); //date("w");
						$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
							
						$datos[0]['path'] = crearPolizaPDF($id,$ffecha,$tipo_poliza,$numero_poliza, utf8_decode($descripcion),$nombreusuario, $tfacturas,number_format($totalCredito,2),number_format($totalCargos,2), $hora, $fechapolizanormal);
					}
					else
						{	$fails=true; $msgerror="No pudo generar ningun vale";}
					
				}
				else
					{	$fails=true; $msgerror=" No es un Existen Partidas";}
			}
			else
			{
				$fails=true; $msgerror="Ya existe una poliza con el tipo y poliza especificados.";
			}
		}
		else
		{
			$fails=true; $msgerror="No es un tipo de poliza valido.";
		}
			
	//}
	//else
	//{	$fails=true; $msgerror="No existe usuario Logeado.";}

	

	if($fecha_poliza=="true" )
		$datos[0]['id']=trim($id);// regresa el id de la
	else
		$datos[0]['id']=$id_facts;
		
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_contab_A_mpoliza($id,$tipo_poliza,
								$numero_poliza,
								$descripcion,
								$usuario,
								$fecpoliza)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$espaciosFinal="";
	//ECHO 
	$tsql_callSP ="{call sp_contab_A_mpoliza_alt(?,?,?,?)}";//Arma el procedimeinto almacenado,?,?
	$params = array(&$id,&$tipo_poliza,
								//&$numero_poliza,
								//&$descripcion,
								&$usuario,
								&$fecpoliza,
								&$espaciosFinal);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("AACCC sp_contab_A_mpoliza".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

//Funcion para registrar el p´roductoa  la requisicion
function func_contab_A_dpoliza($id,$idmpoliza,
								$tipo,
								$poliza, 
								$mov,
								$cuenta, 
								$concepto,
								$referencia,
								$cargo,
								$credito,
								$estatus,
								$usuario,
								$origen)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
		$params = $tsql_callSP ="{call sp_contab_A_dpoliza_alt(?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$idmpoliza,
								&$tipo,
								&$poliza, 
								&$mov,
								&$cuenta, 
								&$concepto,
								&$referencia,
								&$cargo,
								&$credito,
								&$estatus,
								&$usuario,
								&$origen);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	$time_start = microtime_float();
	echo $time_start;
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_contab_A_dpoliza". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}

function crearPolizaPDF($id, $fecha,$tipo_poliza,$numero_poliza, $descripcion,$nombreusuario, $tfacturas,$totalCredito,$totalCargos, $hora, $fecha_poliza)
{
	global $imagenPDFPrincipal;
	$dompdf = new DOMPDF();
		$pageheadfoot='<script type="text/php"> 
		if ( isset($pdf) ) 
		{ 	
			$pdf->page_text(30, 760, "Pagina {PAGE_NUM}", "", 10, array(0,0,0)); 
		} 
</script> ';  
	
	$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=latin1'>
<style>
body 
{
	font-family:'Arial';
	font-size:8;
}

table {
  
	vertical-align:top;
	border-collapse:collapse; 
	border: none;
}
td {padding: 0;}

#twmaarco
{
	border: 1pt solid black;
}

.textobold{
	font-weight:bold;
}
.texto14{ 
	font-size: 15pt;
	font-weight:bold;
}

.texto12{ 
	font-size: 12pt;
}
.texto12bold
{
	font-size: 12pt;
	font-weight:bold;
}
.texto11{
	font-size: 11pt;
}
</style>

</head>

<body>
<table style='width: 90%;margin-top: 2em; margin-left: 2em; margin-right: 3em;' width='100%'>
	<tr>
		<td style='width: 100%;' width='100%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 30%; height: 22px;' colspan='2'>
                    	<table style='width: 100%;'>
                        	<tr>
                            	<td style='width: 20%;'><img src='../../../$imagenPDFPrincipal' style='width: 100px; height: 38px;'></td>
                                <td style='width: 80%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
                            </tr>
                        </table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 50%; vertical-align:top;' align='left' class='texto12'>CAPTURA DE POLIZA No. $tipo_poliza-$numero_poliza</td>
								<td align='right' class='texto12'>".utf8_decode("Fecha de Póliza").":    ".utf8_decode($fecha)." ".$hora."</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td  align='left'>Concepto: $descripcion</td>
				</tr>
				<tr>
				<td colspan='2'>&nbsp;</td>
				<tr>
                	<td style='width: 100%;' colspan='2'>
						<table style='width: 100%;' >
							<thead>
								<tr>
									<th style='width: 15%;' >Fecha</th>
									<th style='width: 15%;' >Cuenta</th>
									<th style='width: 35%;' >Concepto</th>
									<th style='width: 10%;' >Referencia</th>
									<th style='width: 15%;' >Cargos</th>
									<th style='width: 15%;' >Creditos</th>
								</tr>
								<tr>
									<td colspan='5'><hr></td>
								</tr>
							</thead>
							<tbody>$tfacturas</tbody>
						   <tfoot>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td colspan='3'><hr></td>
								</tr>
								<tr>
									<td style='width: 10%;' >&nbsp;</td>
									<td style='width: 40%;' >&nbsp;</td>
									<td style='width: 10%;' align='right'><b>Total</b></td>
									<td style='width: 15%;' align='center'><b>$totalCargos</b></td>
									<td style='width: 15%;' align='center'><b>$totalCredito</b></td>
								</tr>
							</tfoot>
					   </table>
                   	</td>
                </tr>
				<tr>
					<td colspan='2'>
						<table style='width: 70%;'>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'>$nombreusuario</td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'><HR></td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'><HR></td>
                            </tr>
                            <tr>
                                <td style='width: 40%;' align='center' >CAPTURO</td><td style='width: 20%;'>&nbsp;</td><td style='width: 40%;' align='center'  >Vo. Bo.</td>
                            </tr>
						</table>
					</td>
				</tr>
			   </table>
		</td>
	</tr>

</table>
".$pageheadfoot."
</body> </html>";

	//$htmlstr=utf8_encode($htmlstr);
	$dompdf->load_html($htmlstr);
	//$dompdf->set_paper('letter', 'landscape');
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/poliza_$tipo_poliza-$numero_poliza.pdf", $pdf);
	
	return("pdf_files/poliza_".$id.".pdf");
}
?>