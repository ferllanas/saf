<?php
header("Content-type: text/html; charset=UTF8");
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
	$consulta= " ";
	$datos= array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$sw=0;
	$fi="";
	$ff="";
	$fecha1="";
	$fecha2="";
	$excel="";
	
	if(isset($_REQUEST['sw']))
		$sw = $_REQUEST['sw'];	

	if(isset($_REQUEST['fi']))
	{	$fi = $_REQUEST['fi'];
		
		list($dia,$mes , $anio) = explode("/", $fi);
		$fecha1=$anio.$mes.$dia;
	}
	if(isset($_REQUEST['ff']))
	{	$ff = $_REQUEST['ff'];			

		list($dia2,$mes2 , $anio2) = explode("/", $ff);
		$fecha2=$anio2.$mes2.$dia2;
	}
	if($sw > 0)
	{
		if ($conexion)
		{
		
			$consulta="SELECT a.fml,a.folioese,a.folioasig,a.feccan,a.fecsit,a.cvesit2 as motivo,b.solnombre,
b.solappat,b.solapmat,c.descripcion from carterdcarcan a 
left join estsocdfolese b on a.folioese=b.folioese left join catsmscat c on a.cvesit2=c.scat 
WHERE FECCAN>='20150401' and feccan<'20150720' and ofican<>'DP' and a.cvesit2<'22009900' and 
(SUBSTRING(a.cvesit2,5,3)='901' or SUBSTRING(a.cvesit2,5,3)='905' or SUBSTRING(a.cvesit2,5,3)='911' 
or SUBSTRING(a.cvesit2,5,3)='912') AND a.feccan>'$fecha1' AND a.feccan<'$fecha2' group by 
a.fml,a.folioese,a.folioasig,a.feccan,a.fecsit,a.cvesit2,
b.solnombre,b.solappat,b.solapmat,c.descripcion";

//echo $consulta;
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['fml']= trim(substr($row['fml'],0,4)."-".substr($row['fml'],4,4)."-".substr($row['fml'],8,4));
				$datos[$i]['folioese']= trim($row['folioese']);
				$datos[$i]['folioasig']= trim($row['folioasig']);
				$datos[$i]['feccan']= trim(substr($row['feccan'],6,2)."/".substr($row['feccan'],4,2)."/".substr($row['feccan'],0,4));
				$datos[$i]['fecsit']= trim($row['fecsit']);
				$datos[$i]['motivo']= trim($row['motivo']);
				$datos[$i]['solncompleto']= utf8_encode(trim($row['solnombre']))." ".utf8_encode(trim($row['solappat'])) ." ".utf8_encode(trim($row['solapmat']));
				$datos[$i]['descripcion']= trim($row['descripcion']);
				
				//$datos[$i]['conyncompleto']= utf8_encode(trim($row['conyncompleto']));
				
				/*
				$datos[$i]['tipolote']= trim($row['tipolote']);
				$datos[$i]['aream2']= trim($row['aream2']);
				$datos[$i]['origen']= trim($row['origen']);*/
				$i++;
			}
		}
	}

?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />

<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>

<script language="javascript" src="../javascript/jQuery.js"></script>
<script language="javascript" src="../javascript/jquery-1.3.2.min.js"></script>

<script language="javascript" src="../Administracion/javascript/funciones_globales.js"></script>
<script language="javascript" src="javascript/funcion_asig.js"></script>

<script language="javascript">
	$(document).ready(function() {
		 $(".botonExcel").click(function(event) {
		 $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		 $("#FormularioExportacion").submit();
	});
	});
	
function cargar_cancelaciones()
{
	var fi=document.getElementById("fi").value;
	var ff=document.getElementById("ff").value;
	var sw=1;
	location.href="rep_cancelaciones.php?sw="+sw+"&fi="+fi+"&ff="+ff;
}
</script>


<style type="text/css">
<!--
.Estilo2 {font-size: 12px}
-->
</style>
</head>

<body>
<p class="seccionNota Estilo1">Reporte de Cancelaciones</p>
<hr class="hrTitForma">

<table width="837" border="0">
  <tr>
    <td width="305">
	
		Del:</span>
      <input name="fi" type="text" size="12" id="fi" readonly value="<?php echo $fi; ?>"  class="required" maxlength="10"  style="width:70">
      <img src="../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle">
      <!--{literal}-->
     			 <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fi",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger1",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script>
      <span class="cajaPrecio">Al:</span>
      <input name="ff" type="text" size="12" id="ff" readonly value="<?php echo $ff; ?>"  class="required" maxlength="10"  style="width:70">
      <img src="../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer; z-index:5" title="Date selector" align="absmiddle">
	<script type="text/javascript">
					Calendar.setup({
						inputField     :    "ff",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script>	</td>
    <td width="74"><img src="../imagenes/consultar.jpg" width="27" height="26" onClick="cargar_cancelaciones()">
	</td>
    <td width="380">
		<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
			<p>Exportar a Excel  <img src="../imagenes/export_to_excel.png" width="37" height="34" class="botonExcel" /></p>
			<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
		</form>
	</td>
    <td width="23">&nbsp;</td>
    <td width="12">&nbsp;</td>
    <td width="17"></td>
  </tr>
</table>

<table width="100%" border="1" id="Exportar_a_Excel">
  <tr class="subtituloverde">
    <td width="90"><div align="center" class="Estilo2">F M L </div></td>
    <td width="197"><span class="Estilo2">Solicitante</span></td>
 <!--   <td width="90"><div align="center" class="Estilo2">Folio ESE</div></td>
    <td width="86"><div align="center" class="Estilo2">Folio ASIGNA</div></td>-->
    <td width="83"><div align="center" class="Estilo2">Fecha Cancelaci&oacute;n</div></td>
   <!-- <td width="75"><div align="center" class="Estilo2">Fecha Situacion</div></td>-->
    <td width="168"><div align="center" class="Estilo2">Motivo</div></td>
    <!--<td width="89"><span class="Estilo2">Descripci&oacute;n</span></td>-->
   <!-- <td width="68"><span class="Estilo2">Tipo de Lote </span></td>
    <td width="71"><span class="Estilo2">Superficie</span></td>
    <td width="51"><span class="Estilo2">Origen</span></td>-->
  </tr>
  <tr>
    <?php 
	
		for($i=0;$i<count($datos);$i++)
		{
	?>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['fml'];?></td>
    <td align="left" style="font-size:12px "><?php echo $datos[$i]['solncompleto'];?></td>
    <!--<td align="center" style="font-size:12px "><?php echo $datos[$i]['folioese'];?> </td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['folioasig'];?></td>-->
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['feccan'];?></td>
   <!-- <td style="font-size:12px "><?php echo $datos[$i]['fecsit'];?></td>-->
    <td style="font-size:12px "><?php echo $datos[$i]['descripcion'];?></td>
   <!-- <td align="left" style="font-size:12px "><?php echo $datos[$i]['descripcion'];?></td>-->
  </tr>
  <?php 
		}
	?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
