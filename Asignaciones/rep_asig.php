<?php
header("Content-type: text/html; charset=UTF8");
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
	$consulta= " ";
	$datos= array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$sw=0;
	$fi="";
	$ff="";
	$excel="";
	
	if(isset($_REQUEST['sw']))
		$sw = $_REQUEST['sw'];	

	if(isset($_REQUEST['fi']))
		$fi = $_REQUEST['fi'];
		
		list($dia,$mes , $anio) = explode("/", $fi);
		$fecha1=$anio.$mes.$dia;

	if(isset($_REQUEST['ff']))
		$ff = $_REQUEST['ff'];			

		list($dia2,$mes2 , $anio2) = explode("/", $ff);
		$fecha2=$anio2.$mes2.$dia2;

	if($sw > 0)
	{
		if ($conexion)
		{
			$consulta = "select LEFT(a.fml,4)+'-'+SUBSTRING(a.fml,5,4)+'-'+RIGHT(a.fml,4) as fml,
						LEFT(a.folioasig,4)+'-'+RIGHT(a.folioasig,6) as folioasig,
						LEFT(f.nts,4)+'-'+SUBSTRING(f.nts,5,6)+'-'+RIGHT(f.nts,4) as nts,
						d.descripcion,c.solncompleto,c.conyncompleto,
						RIGHT(a.fecasig,2)+'/'+SUBSTRING(a.fecasig,5,2)+'/'+LEFT(a.fecasig,4) as fecasig ,
						b.tipolote,b.aream2,e.origen from asignadasigna a left join tecnicdlotes b on a.fml=b.fml  
						left join estsocdfolese c on c.folioese=a.folioese left join catsmscat d on a.cvesit=d.scat 
						left join tecnicmfracc e on LEFT(a.fml,4)=e.fracc left join estsocdfolnts f on f.nts=b.nts 
						where a.fecasig>=$fecha1 and a.fecasig<=$fecha2 and f.cvesit<'20029000' and b.cvesit<'21079000' 
						and f.nts is not null and b.fracc<'3000' group by a.fml,a.folioasig,f.nts,d.descripcion,c.solncompleto,
						c.conyncompleto,a.fecasig,b.tipolote,b.aream2,e.origen order by a.fml";

			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['fml']= trim($row['fml']);
				$datos[$i]['folioasig']= trim($row['folioasig']);
				$datos[$i]['nts']= trim($row['nts']);
				$datos[$i]['descripcion']= trim($row['descripcion']);
				$datos[$i]['solncompleto']= utf8_encode(trim($row['solncompleto']));
				$datos[$i]['conyncompleto']= utf8_encode(trim($row['conyncompleto']));
				$datos[$i]['fecasig']= trim($row['fecasig']);
				$datos[$i]['tipolote']= trim($row['tipolote']);
				$datos[$i]['aream2']= trim($row['aream2']);
				$datos[$i]['origen']= trim($row['origen']);
				$i++;
			}
		}
	}

?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />

<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>

<script language="javascript" src="../javascript/jQuery.js"></script>
<script language="javascript" src="../javascript/jquery-1.3.2.min.js"></script>

<script language="javascript" src="../Administracion/javascript/funciones_globales.js"></script>
<script language="javascript" src="javascript/funcion_asig.js"></script>

<script language="javascript">
	$(document).ready(function() {
		 $(".botonExcel").click(function(event) {
		 $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		 $("#FormularioExportacion").submit();
	});
	});
</script>


<style type="text/css">
<!--
.Estilo2 {font-size: 12px}
-->
</style>
</head>

<body>
<p class="seccionNota Estilo1">Reporte de Asignaciones</p>
<hr class="hrTitForma">

<table width="837" border="0">
  <tr>
    <td width="305">
	
		Del:</span>
      <input name="fi" type="text" size="12" id="fi" readonly value="<?php echo $fi; ?>"  class="required" maxlength="10"  style="width:70">
      <img src="../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle">
      <!--{literal}-->
     			 <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fi",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger1",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script>
      <span class="cajaPrecio">Al:</span>
      <input name="ff" type="text" size="12" id="ff" readonly value="<?php echo $ff; ?>"  class="required" maxlength="10"  style="width:70">
      <img src="../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer; z-index:5" title="Date selector" align="absmiddle">
	<script type="text/javascript">
					Calendar.setup({
						inputField     :    "ff",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script>	</td>
    <td width="74"><img src="../imagenes/consultar.jpg" width="27" height="26" onClick="cargar()">
	</td>
    <td width="380">
		<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
			<p>Exportar a Excel  <img src="../imagenes/export_to_excel.png" width="37" height="34" class="botonExcel" /></p>
			<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
		</form>
	</td>
    <td width="23">&nbsp;</td>
    <td width="12">&nbsp;</td>
    <td width="17"></td>
  </tr>
</table>

<table width="120%" border="1" id="Exportar_a_Excel">
  <tr class="subtituloverde">
    <td width="86"><div align="center" class="Estilo2">Folio de asignaci&oacute;n</div></td>
    <td width="90"><div align="center" class="Estilo2">F M L </div></td>
    <td width="83"><div align="center" class="Estilo2">solic</div></td>
    <td width="75"><div align="center" class="Estilo2">Estatus</div></td>
    <td width="168"><div align="center" class="Estilo2">Asignatario</div></td>
    <td width="197"><span class="Estilo2">Conyuge</span></td>
    <td width="89"><span class="Estilo2">Fecha de Asignaci&oacute;n</span></td>
    <td width="68"><span class="Estilo2">Tipo de Lote </span></td>
    <td width="71"><span class="Estilo2">Superficie</span></td>
    <td width="51"><span class="Estilo2">Tipo</span></td>
  </tr>
  <tr>
    <?php 
		for($i=0;$i<count($datos);$i++)
		{
	?>
    <td align="center" style="font-size:12px "> <?php echo $datos[$i]['folioasig'];?> </td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['fml'];?></td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['nts'];?></td>
    <td style="font-size:12px "><?php echo $datos[$i]['descripcion'];?></td>
    <td style="font-size:12px "><?php echo $datos[$i]['solncompleto'];?></td>
    <td style="font-size:12px "><?php echo $datos[$i]['conyncompleto'];?></td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['fecasig'];?></td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['tipolote'];?></td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['aream2'];?></td>
    <td align="center" style="font-size:12px "><?php echo $datos[$i]['origen'];?></td>
  </tr>
  <?php 
		}
	?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
