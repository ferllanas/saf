<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
?>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd /xhtml1-transitional.dtd ">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin1" />
<title>Ajuste de Presupuesto</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript/presupuesto_funciones.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<script type="text/javascript" src="ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="ajaximage/scripts/jquery.form.js"></script>


<script> 
function validar(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) alert('Tecla pulsada'); 
} 
function igualacant()
{
	//alert("jala");
	var montoc= document.getElementById('montoc').value;
	document.getElementById('montod').value=montoc;
	document.getElementById('montoe').value=montoc;
	document.getElementById('montop').value=montoc;
	document.getElementById('montod').setfocus;
	
}
</script> 

<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
</head>

<body>
 <table width="100%" border="0">
  	<tr>
    	<td class="TituloDForma">Ajuste de Presupuesto
   	      <hr class="hrTitForma"></td>
    </tr>
    <tr>
      <td class="texto8"><table width="100%" border="0">
        <tr>
          <td class="texto8">
              <table width="100%" border="0">
               <tr>
          <td class="texto8" style="width:80px" ><label>A&ntilde;o:</label></td><td width="67" class="texto8" style="width:50px" ><input class="texto8" tabindex="1" name="anio" type="text" id="anio" maxlength="4" onkeyup="return validar(event)"  style="width:40px" /></td>          
          <td class="texto8" >Mes<input class="texto8" tabindex="2" name="mes" type="text" id="mes" maxlength="2" onkeyup="return validar(event)"  style="width:30px" /></td>          
        </tr>
         <tr>
          	<td class="texto8" ><label>Documento:</label></td>
            <td class="texto8" colspan="2"  >
            <select class="texto8" tabindex="4" name="documento" id="documento" style="width:150px" >
            <?php
					$command="select id,descrip from egresostipodocto";						
					//echo $command;
					$stmt2 = sqlsrv_query( $conexion,$command);					
					while($row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
					{
						echo "<option value='".$row['id']."'>".$row['descrip']."</option>";
					}
				?>
            </select></td>
            </tr>
            <tr>
        	<td class="texto8" >N&uacute;mero de Docum&eacute;nto:</td>
        	<td><input class="texto8" tabindex="5" name="ndoc" type="text" id="ndoc"   style="width:50px" /></td>
        </tr>
       <tr>
       	<td class="texto8">Concepto:</td>
        <td colspan="4"  class="texto8"><textarea tabindex="6" name="concepto" id="concepto"></textarea></td>
       </tr>
        <tr>
          	<td class="texto8" colspan="4" style="width:70px" >
           <div align="left" style="z-index:0; position:relative; width:600px">Nombre de la cuenta:
                <input tabindex="7"  class="texto8" name="nomsubcuenta" type="text" id="nomsubcuenta" size="50" style="width:300px;"  onkeyup="searchSuggest();" autocomplete="off"/>
                <input type="text" class="texto8" name="numcuentacompleta" id="numcuentacompleta" size="50" style="width:100px;" readonly="readonly" />
                <div id="search_suggest" style="z-index:7; position:absolute;" > </div>
              </div></td>
        	<td width="395" class="texto8" >&nbsp;</td>
        </tr>
    <tr>
       <td class="texto8" style="width:50px" ><label>Monto Comprometido:</label></td><td width="67" class="texto8" style="width:50px" ><input class="texto8" value="0" name="montoc" tabindex="8" type="text" id="montoc" style="width:80px; text-align:right;" onblur="igualacant();"  /></td>  
    </tr>
    <tr>
       <td class="texto8" style="width:50px" ><label>Monto Devengado:</label></td><td width="67" class="texto8" style="width:50px" ><input class="texto8" value="0" name="montod" tabindex="9" type="text" id="montod" style="width:80px; text-align:right;" /></td>  
    </tr>
    <tr>
       <td class="texto8" style="width:50px" ><label>Monto Ejercido:</label></td><td width="67" class="texto8" style="width:50px" ><input class="texto8" value="0" name="montoe" tabindex="10" type="text" id="montoe" style="width:80px; text-align:right;" /></td>  
    </tr>
    <tr>
       <td class="texto8" style="width:50px" ><label>Monto Pagado:</label></td><td width="67" class="texto8" style="width:50px" ><input class="texto8" value="0" name="montop" tabindex="11" type="text" id="montop" style="width:80px; text-align:right;" /></td>  
    </tr>
    <tr>
    	<td align="center" colspan="4">
        	<input type="button" tabindex="12" onclick="ajustedpresup_esp();" id="gralguarda" name="gralguarda" value="Ajustar" class="texto8"/>
        </td>
    </tr>
  </table>
<!--</form>-->
</body>
</html>