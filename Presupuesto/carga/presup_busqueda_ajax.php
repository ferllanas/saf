<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$nom1='';
$nom2='';
	//echo $opcion;
	if(strlen($_REQUEST['query'])>0)
	{
				$command= "SELECT a.id,a.cuenta,b.nomcta,c.a�o as anio,a.total,a.ene,a.feb,a.mar,a.abr,a.may,a.jun,a.jul,a.ago,a.sep,a.oct,a.nov,a.dic FROM presupdautorizado a left join presupmcuentas b on a.cuenta=b.cuenta left join presupmautorizado c on a.folio=c.folio WHERE a.folio LIKE '%" . $_REQUEST['query'] . "%' order by a.cuenta";
	
				//echo $command;
				$stmt2 = sqlsrv_query($conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco	
					$datos[$i]['id']=trim($row['id']);
					$datos[$i]['cuenta']=trim($row['cuenta']);
					$datos[$i]['nomcta']=utf8_encode($row['nomcta']);					
					$datos[$i]['total']=number_format($row['total']);
					$datos[$i]['anio']=$row['anio'];
					$datos[$i]['ene']=number_format($row['ene']);
					$datos[$i]['feb']=number_format($row['feb']);
					$datos[$i]['mar']=number_format($row['mar']);
					$datos[$i]['abr']=number_format($row['abr']);
					$datos[$i]['may']=number_format($row['may']);
					$datos[$i]['jun']=number_format($row['jun']);
					$datos[$i]['jul']=number_format($row['jul']);
					$datos[$i]['ago']=number_format($row['ago']);
					$datos[$i]['sep']=number_format($row['sep']);
					$datos[$i]['oct']=number_format($row['oct']);
					$datos[$i]['nov']=number_format($row['nov']);
					$datos[$i]['dic']=number_format($row['dic']);					
					$i++;
				}
	}
echo json_encode($datos);   // Los codifica con el jason
?>