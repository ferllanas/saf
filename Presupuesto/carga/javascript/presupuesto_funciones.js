
var partidasPresup=new Array(); 
var countpartidasPoliza =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

function objfacturaDProv(Concepto, Credito ,	numcuentacompleta ,  nomsubcuenta , Cargo , Concepto,Referencia){  
	this.Concepto = Concepto;
	this.Credito = Credito;
	this.numcuentacompleta = numcuentacompleta;
    this.nomsubcuenta = nomsubcuenta;
	this.Cargo = Cargo;
	this.Referencia = Referencia;
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }

function cargarpresup()
{
	
	var np=0.00;
	var sp=0.00;
	var ms=0.00;
	var sg=0.00;
	var as=0.00;
	var af=0.00;
	var op=0.00;
	var dp=0.00;
	var error=0;
	var errorcalculo=false;
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}
	var documento= document.getElementById('pathtexto').value;
	//alert(documento);
	var datas = { ppath: documento	};
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'php_ajax/cargapresup.php',
						dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(request);
						 alert ( " Can't do because: " + error );
						},
						success: function(resp)
						{
							var myObj = resp;
							for(i=0;i<resp.length;i++)
							{								
								
								if(resp[i].rubro==0000)
								{
									//alert(sp);
									np=parseInt(np)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==1000)
								{
									//alert(sp);
									sp=parseInt(sp)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==2000)
								{
									ms=parseInt(ms)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==3000)
								{
									sg=parseInt(sg)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==4000)
								{
									as=parseInt(as)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==5000)
								{
									af=parseInt(af)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==6000)
								{
									op=parseInt(op)+parseInt(resp[i].anual);
								}
								if(resp[i].rubro==9000)
								{
									dp=parseInt(dp)+parseInt(resp[i].anual);
								}
								if(resp[i].nomcuenta == null)
								{
									resp[i].nomcuenta='****** Error en Cuenta ******';									
									error=1;
								}
								if(resp[i].nomcuenta.match(/Error.*/) )
								{	
									error=1;
								}
								if(resp[i].anual!=resp[i].canual)								
								{	
									errorcalculo=true;
								}
								pasa=validarepetidos(resp[i].cuenta);
								if(errorcalculo==false)
								{
									if(pasa==false)
									{
										agregarpolizaMTablaFacturas(resp[i].cuenta,resp[i].nomcuenta,resp[i].anual,resp[i].ene,resp[i].feb,resp[i].mar,resp[i].abr,resp[i].may,resp[i].jun,resp[i].jul,resp[i].ago,resp[i].sep,resp[i].oct,resp[i].nov,resp[i].dic,resp[i].canual,resp[i].rubro);
										partidasPresup[countpartidasPoliza]= new objpolizaDctas(resp[i].cuenta,resp[i].anual,resp[i].ene,resp[i].feb,resp[i].mar,resp[i].abr,resp[i].may,resp[i].jun,resp[i].jul,resp[i].ago,resp[i].sep,resp[i].oct,resp[i].nov,resp[i].dic);
										//sumaporrubro(resp[i].anual,resp[i].rubro)
									}
									else
									{
										
										alert("Cuenta "+resp[i].cuenta +" Duplicadas revise el Documento");
										document.getElementById('gralguarda').disabled=true;
										return(false);
									}
								}
								else
								{
									alert("Cuenta "+resp[i].cuenta +" No coinciden los Total Anual: $"+resp[i].anual +" Total Calculado: $"+resp[i].canual );
									document.getElementById('gralguarda').disabled=true;
									return(false);
								}
								countpartidasPoliza++;								
							}
		                    console.log(myObj);
							if(resp.error==1)
							{
								alert("No se Pudo Cragar el Archivo");								
							}
							if(error==1)
							{
								alert("Se encontaron errores en la cuenta "+resp[i].cuenta +" favor de verificarlas");								
								document.getElementById('gralguarda').disabled=true;
							}
							else
							{
								//validarepetidos();
								document.getElementById('np').value=addCommas(np);
								document.getElementById('sp').value=addCommas(sp);
								document.getElementById('ms').value=addCommas(ms);
								document.getElementById('sg').value=addCommas(sg);
								document.getElementById('as').value=addCommas(as);
								document.getElementById('af').value=addCommas(af);
								document.getElementById('op').value=addCommas(op);
								document.getElementById('dp').value=addCommas(dp);		
								document.getElementById('gralguarda').disabled=false;
							}
						}
					
					});
	
}
function validarepetidos(cuenta)
{
	var Table = document.getElementById('tfacturas');
	//alert("pasa");
	for (var i = 0; i < Table.rows.length; i++) 
	{ 		
		for (var j = 0; j <Table.rows[i].cells.length;j++)
		{
			var cell = Table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];			
				//alert(mynode.name);
				if(mynode.name=="cuenta")
				{
					
					if(cuenta==mynode.value)
					{						
						return(true);
					}					
				}				
			}
		}
	}
	return(false);
}

function agregarpolizaMTablaFacturas(cuenta,nomcuenta,anual,ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic,canual,rubro)
{
	var Table = document.getElementById('tfacturas');
	var contRow = Table.rows.length;
	
	var newrow = Table.insertRow(contRow);
	var newcell1 = newrow.insertCell(0);						
	newcell1.innerHTML = "<input type='hidden' id='cuenta' name='cuenta' readonly  value='"+cuenta+"' >"+cuenta;
	//newcell1.className ="td1";
	//newcell1.style.width = "30px";

	var newcell2 = newrow.insertCell(1);
	newcell2.innerHTML = nomcuenta;
	//newcell2.className ="td2";
	newcell2.style.width = "250px";
	
	var newcell3 = newrow.insertCell(2);
	newcell3.innerHTML = anual;
	//newcell3.className ="td8";
	//newcell3.style.width = "50px";
	
	var newcell4 = newrow.insertCell(3);
	newcell4.innerHTML = ene;
	//newcell4.className ="td8";
	//newcell4.style.width = "50px";

	var newcell5 = newrow.insertCell(4);
	newcell5.innerHTML = feb;
	//newcell5.className ="td8";
	//newcell5.style.width = "50px";

	var newcell6 = newrow.insertCell(5);
	newcell6.innerHTML = mar;
	//newcell6.className ="td8";
	//newcell6.style.width = "50px";
	
	var newcell7 = newrow.insertCell(6);
	newcell7.innerHTML = abr;
	//newcell7.className ="td8";
	//newcell7.style.width = "50px";
	
	var newcell8 = newrow.insertCell(7);
	newcell8.innerHTML = may;
	//newcell8.className ="td8";
	//newcell8.style.width = "50px";
	
	var newcell9 = newrow.insertCell(8);
	newcell9.innerHTML = jun;
	//newcell9.className ="td8";
	//newcell9.style.width = "50px";
	
	var newcell10 = newrow.insertCell(9);
	newcell10.innerHTML = jul;
	//newcell10.className ="td8";
	//newcell10.style.width = "50px";

	var newcell11 = newrow.insertCell(10);
	newcell11.innerHTML = ago;
	//newcell11.className ="td8";
	//newcell11.style.width = "50px";

	var newcell12 = newrow.insertCell(11);
	newcell12.innerHTML = sep;
	//newcell12.className ="td8";
	//newcell12.style.width = "50px";

	var newcell13 = newrow.insertCell(12);
	newcell13.innerHTML = oct;
	//newcell13.className ="td8";
	//newcell13.style.width = "50px";

	var newcell14 = newrow.insertCell(13);
	newcell14.innerHTML = nov;
	//newcell14.className ="td8";
	//newcell14.style.width = "50px";

	var newcell15 = newrow.insertCell(14);
	newcell15.innerHTML = dic;
	//newcell15.className ="td8";
	//newcell15.style.width = "50px";

	var newcell16 = newrow.insertCell(15);
	newcell16.innerHTML = rubro;

}

function refreshTablaFacturas()
{
	//partidasPoliza
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}

	for (var i = 0; i < partidasPoliza.length; i++) 
	{ 
		agregarFacturaATablaFacturas(partidasPoliza[i]['Concepto'] ,
									partidasPoliza[i]['Credito'],
									partidasPoliza[i]['numcuentacompleta'],
									partidasPoliza[i]['nomsubcuenta'],
									partidasPoliza[i]['Cargo']);
	}
	
}

function guardardpresup()
{
	var anio= document.getElementById('anio').value;
	if(anio=="")
	{
		alert("No se Tecleo el Año a Afectar");
		return;
	}
	var datas = {
					partidas	: partidasPresup,
					anio   : anio
    			};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/guardaDpresup.php',
				dataType: "json",
				data:     datas,
				error: function(request,error) 
				{
				 console.log(request);
				 alert ( " Can't do because: " + error );
				},
				success: function(resp) 
				{
					//alert(resp)
					console.log(resp);
					if( resp.length ) 
					{						
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								document.getElementById('anio').value="";
								var Table = document.getElementById('tfacturas');
								var numren=Table.rows.length;
								for(var i = numren-1; i >=0; i--)
								{
									Table.deleteRow(i);
								}
								alert("Carga Satisfactoria de Presupuesto folio:"+resp[0].folio);
							}
							else
							{
								alert( "Error: "+resp[0].msgerror);
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
					}
				}
			});
}
var partidasPresup=new Array(); 
var countpartidasPoliza =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

 function objpolizaDctas(cuenta,anual,ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic )
 {  
	this.numcuentacompleta = cuenta;
	this.anual=anual;
	this.ene=ene;
	this.feb=feb;
	this.mar=mar;
	this.abr=abr;
	this.may=may;
	this.jun=jun;
	this.jul=jul;
	this.ago=ago;
	this.sep=sep;
	this.oct=oct;
	this.nov=nov;
	this.dic=dic;	
	this.partidas = new Array();
	this.numpartidas = 0;
	
 }
function afectar_polizas()
{
	var polizasAAfectar=new Array();
	var numpoliza=0;
	var polizas = document.getElementsByName("afectacion");
	var tipops = document.getElementsByName("tipop");
	
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizasAAfectar[numpoliza]=polizas[i].value;
			numpoliza++;
			//alert(polizas[i].value);
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea afectar a la(s) polizas seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		
		var datas = {
					polizas		: 		polizasAAfectar
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/afecta_poliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									 busca_cheque();						
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}

function desafectar_polizas()
{
	var polizasAAfectar=new Array();
	var numpoliza=0;
	var polizas=document.getElementsByName("afectacion");
	//alert("AAAA");
	
	for(i=0;i<polizas.length;i++)
		if(	polizas[i].checked)
		{	
			polizasAAfectar[numpoliza]=polizas[i].value;
			numpoliza++;
			//alert(polizas[i].value);
		}
		
	if(numpoliza<=0)
	{
		alert("No existen polizas seleccionadas");
		return false;
	}
	
	var pregunta = confirm("¿Esta seguro que desea desafectar a la(s) polizas seleccionadas? ");//+idpartida);//
	if (pregunta)
	{
		//alert("BBB");
		var datas = {
					polizas		: 		polizasAAfectar
				};
	
		jQuery.ajax({
					type:     'post',
					cache:    false,
					url:      'php_ajax/desafecta_poliza.php',
					data:     datas,
					success: function(resp) 
					{
						//alert(resp)
						if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								if(myArray[0].fallo==false)
								{
									//alert("WS");
									 busca_cheque();
									//alert("ZA");
								}
								else
								{
									alert( "No existe partida de poliza con este numero.");
								}
							}
							else
								alert( "Error no se pudo obtener la partida de la poliza ."+myArray[0].msgerror);
						}
					}
				});
		
	}
}

// busuqeda incrementl *****
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
	//alert("error");
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('nomsubcuenta').value);
		searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
       if(searchReq.responseText.length<1);
		{
			document.getElementById('numcuentacompleta').value = '';	
			//document.getElementById('nomsubcuenta').value = '';
		}
	   ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+'; '+tok[0]+' ">' + tok[1] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    //document.getElementById('nomsubcuenta').value = idprod;
	var tok =idprod.split(";");	
	document.getElementById('numcuentacompleta').value = tok[0];	
	document.getElementById('nomsubcuenta').value = tok[1];
	//$('#Concepto').text(tok[1]);
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
	document.getElementById('montoc').focus();
}

function addslashes (str)
{
   
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  

}
function aceptarSoloNumeros(evt)
{
//asignamos el valor de la tecla a keynum
	if(window.event)
	{// IE
		keynum = evt.keyCode;
	}
	else
	{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if(keynum>47 && keynum<58)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function ajustedpresup_esp()
{
	var anio= document.getElementById('anio').value;
	//alert(anio);
	var mes= document.getElementById('mes').value;
	var documento= document.getElementById('documento').value;
	var ndoc= document.getElementById('ndoc').value;
	var concepto= document.getElementById('concepto').value;
	var numcuentacompleta= document.getElementById('numcuentacompleta').value;
	var montoc= document.getElementById('montoc').value;
	var montod= document.getElementById('montod').value;
	var montoe= document.getElementById('montoe').value;
	var montop= document.getElementById('montop').value;
	
	
	if(anio=="")
	{
		alert("No se Tecleo el Año a Afectar");
		return;
	}
	/// VALIDACION DE AJUSTES DE PRESUPUESTO///
	var mensaje="";
	var datas = {
					anio:anio,
					mes:mes,
					numcuentacompleta:numcuentacompleta,
					montoc:montoc				
				};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/valajustapresup_2.php',
				dataType: "json",
				data:     datas,
				error: function(request,error) 
				{
				 //console.log(request);
				 alert ( " Can't do because: " + error );
				},
				success: function(resp) 
				{
					//alert(resp.length)
					console.log(resp.length);
					console.log('cierre:'+resp[0].cierre+' disponible:'+resp[0].disponible);
					if( resp.length>0 ) 
					{						
						if(resp.length>0)
						{												
							if(resp[0].cierre==0)
							{
								if(resp[0].disponible==0)
								{									
									var datas = {
													anio:anio,
													mes:mes,													
													documento:documento,
													ndoc:ndoc,
													concepto:concepto,
													numcuentacompleta:numcuentacompleta,
													montoc:montoc,
													montod:montod,
													montoe:montoe,
													montop:montop
												};
												
									jQuery.ajax({
												type:     'post',
												cache:    false,
												url:      'php_ajax/ajustapresup_new.php',
												dataType: "json",
												data:     datas,
												error: function(request,error) 
												{
												 console.log(request);
												 alert ( " Can't do because: " + error );
												},
												success: function(resp) 
												{
													//alert(resp)
													console.log(resp);
													if( resp.length ) 
													{						
														if(resp.length>0)
														{
															if(resp[0].fails==false)
															{
																document.getElementById('anio').value="";	
																document.getElementById('mes').value="";
																document.getElementById('documento').value="";
																document.getElementById('ndoc').value="";
																document.getElementById('concepto').value="";
																document.getElementById('numcuentacompleta').value="";
																document.getElementById('nomsubcuenta').value="";
																document.getElementById('montoc').value="";
																document.getElementById('montod').value="";
																document.getElementById('montoe').value="";
																document.getElementById('montop').value="";
																alert("Ajuste Satisfactoria de Presupuesto");
															}
															else
															{
																alert( "Error: "+resp[0].msgerror);
															}
														}
														else
															alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
													}
												}
											});
										////termina
								}// if disponible==0
								else
								{
									if(resp[0].disponible==1)
									{
										alert("Presupuesto Insuficiente");
									}
									else
									{
										alert("La cuenta no esta registrada favor de avisar a sistemas");										
									}
								}
							}// if cierre==0
							else
							{
								alert("No Procede el Presupuesto del Mes esta cerrado");
							}
						}
						else
						{
							alert("Error no se pudo la Validacion");
							//return false;
						}						
						//alert(mensaje);
						//return mensaje;
					}
				}
			});
	
}

function ajustedpresup()
{
	var anio= document.getElementById('anio').value;
	//alert(anio);
	var mes= document.getElementById('mes').value;
	var tipo= document.getElementById('tipo').value;
	var documento= document.getElementById('documento').value;
	var ndoc= document.getElementById('ndoc').value;
	var concepto= document.getElementById('concepto').value;
	var numcuentacompleta= document.getElementById('numcuentacompleta').value;
	var monto= document.getElementById('monto').value;
	
	if(anio=="")
	{
		alert("No se Tecleo el Año a Afectar");
		return;
	}
	/// VALIDACION DE AJUSTES DE PRESUPUESTO///
	var mensaje="";
	var datas = {
					anio:anio,
					mes:mes,
					numcuentacompleta:numcuentacompleta,
					monto:monto
				};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/valajustapresup.php',
				dataType: "json",
				data:     datas,
				error: function(request,error) 
				{
				 //console.log(request);
				 alert ( " Can't do because: " + error );
				},
				success: function(resp) 
				{
					//alert(resp.length)
					console.log(resp.length);
					console.log('cierre:'+resp[0].cierre+' disponible:'+resp[0].disponible);
					if( resp.length>0 ) 
					{						
						if(resp.length>0)
						{												
							if(resp[0].cierre==0)
							{
								if(resp[0].disponible==0)
								{
									
									//mensaje= "Ok";
									///PARA VALIDAR EL PROGRAMA //
									var datas = {
													anio:anio,
													mes:mes,
													tipo:tipo,
													documento:documento,
													ndoc:ndoc,
													concepto:concepto,
													numcuentacompleta:numcuentacompleta,
													monto:monto
												};
												
									jQuery.ajax({
												type:     'post',
												cache:    false,
												url:      'php_ajax/ajustapresup.php',
												dataType: "json",
												data:     datas,
												error: function(request,error) 
												{
												 console.log(request);
												 alert ( " Can't do because: " + error );
												},
												success: function(resp) 
												{
													//alert(resp)
													console.log(resp);
													if( resp.length ) 
													{						
														if(resp.length>0)
														{
															if(resp[0].fails==false)
															{
																document.getElementById('anio').value="";	
																document.getElementById('mes').value="";
																document.getElementById('tipo').value="";
																document.getElementById('documento').value="";
																document.getElementById('ndoc').value="";
																document.getElementById('concepto').value="";
																document.getElementById('numcuentacompleta').value="";
																document.getElementById('nomsubcuenta').value="";
																document.getElementById('monto').value="";
																alert("Ajuste Satisfactoria de Presupuesto");
															}
															else
															{
																alert( "Error: "+resp[0].msgerror);
															}
														}
														else
															alert( "Error no se pudo obtener la cuenta."+resp[0].msgerror);
													}
												}
											});
										////termina
								}// if disponible==0
								else
								{
									if(resp[0].disponible==1)
									{
										alert("Presupuesto Insuficiente");
									}
									else
									{
										alert("La cuenta no esta registrada favor de avisar a sistemas");										
									}
								}
							}// if cierre==0
							else
							{
								alert("No Procede el Presupuesto del Mes esta cerrado");
							}
						}
						else
						{
							alert("Error no se pudo la Validacion");
							//return false;
						}						
						//alert(mensaje);
						//return mensaje;
					}
				}
			});
	
	
	
/*	var pasa="";
	pasa=validapresupajuste(anio,mes,numcuentacompleta,monto);
	alert(pasa);
	if(pasa=="Ok")
	{
		
	}
	else
	{
		alert("Error "+pasa);
	}*/
}

function validapresupajuste(anio,mes,numcuentacompleta,monto)
{
	var mensaje="";
	var datas = {
					anio:anio,
					mes:mes,
					numcuentacompleta:numcuentacompleta,
					monto:monto
				};
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/valajustapresup.php',
				dataType: "json",
				data:     datas,
				error: function(request,error) 
				{
				 //console.log(request);
				 alert ( " Can't do because: " + error );
				},
				success: function(resp) 
				{
					//alert(resp)
					//console.log(resp);
					if( resp.length ) 
					{						
						if(resp.length>0)
						{
							if(resp[0].cierre==0)
							{
								if(resp[0].disponible==0)
								{
									
									mensaje= "Ok";
									///PARA VALIDAR EL PROGRAMA //
									
								}
								else
								{
									alert("Presupuesto Insuficiente");
								}
							}
							else
							{
								alert("No Procede el Presupuesto del Mes esta cerrado");
							}
						}
						else
						{
							alert("Error no se pudo la Validacion");
							return false;
						}						
						alert(mensaje);
						return mensaje;
					}
				}
			});

}
