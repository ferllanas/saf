<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
		
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$mesi =  $_REQUEST['mesi'];
$mesf =  $_REQUEST['mesf'];
$anio =  $_REQUEST['anio'];
$op =  $_REQUEST['op'];

$mesi=(int)$mesi;
$mesf=(int)$mesf;
$anio=(int)$anio;

$datos=array();
if ($conexion)
{
	
/*	$consulta = "select a.rubro,a.cuenta,a.nomcta,";
	$consulta .= "(case when b.mes=1 then b.monto else 0 end) as ene,";
	$consulta .= "(case when b.mes=2 then b.monto else 0 end) as feb,";
	$consulta .= "(case when b.mes=3 then b.monto else 0 end) as mar,";
	$consulta .= "(case when b.mes=4 then b.monto else 0 end) as abr,";
	$consulta .= "(case when b.mes=5 then b.monto else 0 end) as may,";
	$consulta .= "(case when b.mes=6 then b.monto else 0 end) as jun,";
	$consulta .= "(case when b.mes=7 then b.monto else 0 end) as jul,";
	$consulta .= "(case when b.mes=8 then b.monto else 0 end) as ago,";
	$consulta .= "(case when b.mes=9 then b.monto else 0 end) as sep,";
	$consulta .= "(case when b.mes=10 then b.monto else 0 end) as oct,";
	$consulta .= "(case when b.mes=11 then b.monto else 0 end) as nov,";
	$consulta .= "(case when b.mes=12 then b.monto else 0 end) as dic from ";
	$consulta .= "v_presing_mcuentas a left join (select ctapresup,MONTH(falta) as mes,SUM(monto) as monto from ";
	$consulta .= "presingdgpo_cpi where MONTH(falta)>=$mesi and MONTH(falta)<=$mesf and YEAR(falta)=YEAR(GETDATE()) group by ";
	$consulta .= "ctapresup,falta) b on a.cuenta=b.ctapresup where ";
	if($op==0)
	{
		$consulta .= "cuenta<>111 and cuenta<>119 ";
	}	
	   else
	{ 
		$consulta .= "cuenta=111 or cuenta=119 ";
	}	

		$consulta .= "order by a.rubro,a.cuenta,a.nomcta";
	*/
	
		$consulta = "select rubro,cuenta, nomcta, sum(ene) as ene,";
		$consulta .= "sum(feb) as feb,";
		$consulta .= "sum(mar) as mar,";
		$consulta .= "sum(abr) as abr,";
		$consulta .= "sum(may) as may,";
		$consulta .= "sum(jun) as jun,";
		$consulta .= "sum(jul) as jul,";
		$consulta .= "sum(ago) as ago,";
		$consulta .= "sum(sep) as sep,";
		$consulta .= "sum(oct) as oct,";
		$consulta .= "sum(nov) as nov,";
		$consulta .= "sum(dic) as dic from (";
	
		$consulta .= "select a.rubro,a.cuenta,a.nomcta,";
		$consulta .= "(case when b.mes=1 then b.monto else 0 end) as ene,";
		$consulta .= "(case when b.mes=2 then b.monto else 0 end) as feb,";
		$consulta .= "(case when b.mes=3 then b.monto else 0 end) as mar,";
		$consulta .= "(case when b.mes=4 then b.monto else 0 end) as abr,";
		$consulta .= "(case when b.mes=5 then b.monto else 0 end) as may,";
		$consulta .= "(case when b.mes=6 then b.monto else 0 end) as jun,";
		$consulta .= "(case when b.mes=7 then b.monto else 0 end) as jul,";
		$consulta .= "(case when b.mes=8 then b.monto else 0 end) as ago,";
		$consulta .= "(case when b.mes=9 then b.monto else 0 end) as sep,";
		$consulta .= "(case when b.mes=10 then b.monto else 0 end) as oct,";
		$consulta .= "(case when b.mes=11 then b.monto else 0 end) as nov,";
		$consulta .= "(case when b.mes=12 then b.monto else 0 end) as dic from v_presing_mcuentas a ";
		$consulta .= "left join (select ctapresup,MONTH(falta) as mes,SUM(monto) as monto from ";
		$consulta .= "presingdgpo_cpi where MONTH(falta)>=$mesi and MONTH(falta)<=$mesf and YEAR(falta)=$anio ";
		$consulta .= "group by ctapresup,falta) b on a.cuenta=b.ctapresup where ";
		

		if($op==0)
		{
			$consulta .= "cuenta<>111 and cuenta<>119 ";
			$consulta .= "union  select a.rubro,a.cuenta,a.nomcta,";
			$consulta .= "(case when b.mes=1 then b.monto else 0 end) as ene,";
			$consulta .= "(case when b.mes=2 then b.monto else 0 end) as feb,";
			$consulta .= "(case when b.mes=3 then b.monto else 0 end) as mar,";
			$consulta .= "(case when b.mes=4 then b.monto else 0 end) as abr,";
			$consulta .= "(case when b.mes=5 then b.monto else 0 end) as may,";
			$consulta .= "(case when b.mes=6 then b.monto else 0 end) as jun,";
			$consulta .= "(case when b.mes=7 then b.monto else 0 end) as jul,";
			$consulta .= "(case when b.mes=8 then b.monto else 0 end) as ago,";
			$consulta .= "(case when b.mes=9 then b.monto else 0 end) as sep,";
			$consulta .= "(case when b.mes=10 then b.monto else 0 end) as oct,";
			$consulta .= "(case when b.mes=11 then b.monto else 0 end) as nov,";
			$consulta .= "(case when b.mes=12 then b.monto else 0 end) as dic from ";
			$consulta .= "v_presing_mcuentas a left join (select ctapresup,MONTH(fecha) as mes,SUM(monto) as monto from ";
			$consulta .= "v_presing_ap_estatal where MONTH(fecha)>=$mesi and MONTH(fecha)<=$mesf and YEAR(fecha)=$anio ";
			$consulta .= "group by ctapresup,fecha) b on a.cuenta=b.ctapresup where ";
			$consulta .= "cuenta<>111 and cuenta<>119 ";
		}	
		   else
		{ 
			$consulta .= "cuenta=111 or cuenta=119 ";		
			$consulta .= "union  select a.rubro,a.cuenta,a.nomcta,";
			$consulta .= "(case when b.mes=1 then b.monto else 0 end) as ene,";
			$consulta .= "(case when b.mes=2 then b.monto else 0 end) as feb,";
			$consulta .= "(case when b.mes=3 then b.monto else 0 end) as mar,";
			$consulta .= "(case when b.mes=4 then b.monto else 0 end) as abr,";
			$consulta .= "(case when b.mes=5 then b.monto else 0 end) as may,";
			$consulta .= "(case when b.mes=6 then b.monto else 0 end) as jun,";
			$consulta .= "(case when b.mes=7 then b.monto else 0 end) as jul,";
			$consulta .= "(case when b.mes=8 then b.monto else 0 end) as ago,";
			$consulta .= "(case when b.mes=9 then b.monto else 0 end) as sep,";
			$consulta .= "(case when b.mes=10 then b.monto else 0 end) as oct,";
			$consulta .= "(case when b.mes=11 then b.monto else 0 end) as nov,";
			$consulta .= "(case when b.mes=12 then b.monto else 0 end) as dic from ";
			$consulta .= "v_presing_mcuentas a left join (select ctapresup,MONTH(fecha) as mes,SUM(monto) as monto from ";
			$consulta .= "v_presing_ap_estatal where MONTH(fecha)>=$mesi and MONTH(fecha)<=$mesf and YEAR(fecha)=$anio ";
			$consulta .= "group by ctapresup,fecha) b on a.cuenta=b.ctapresup where ";
		
			$consulta .= "cuenta=111 or cuenta=119 ";
		}	
	

			$consulta .= ") ee group by ee.rubro,ee.cuenta, ee.nomcta	order by rubro, cuenta,nomcta";
			//$consulta .= "and b.monto>0 order by a.rubro,a.cuenta,a.nomcta";		
	
	
	
	//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['rubro']= trim($row['rubro']);
			$datos[$i]['cuenta']= trim($row['cuenta']);
			$datos[$i]['nomcta']= trim($row['nomcta']);
			$datos[$i]['ene']= number_format(trim($row['ene']),2);
			$datos[$i]['feb']= number_format(trim($row['feb']),2);
			$datos[$i]['mar']= number_format(trim($row['mar']),2);
			$datos[$i]['abr']= number_format(trim($row['abr']),2);
			$datos[$i]['may']= number_format(trim($row['may']),2);
			$datos[$i]['jun']= number_format(trim($row['jun']),2);
			$datos[$i]['jul']= number_format(trim($row['jul']),2);
			$datos[$i]['ago']= number_format(trim($row['ago']),2);
			$datos[$i]['sep']= number_format(trim($row['sep']),2);
			$datos[$i]['oct']= number_format(trim($row['oct']),2);
			$datos[$i]['nov']= number_format(trim($row['nov']),2);
			$datos[$i]['dic']= number_format(trim($row['dic']),2);
			$datos[$i]['recaudado']= number_format($row['ene']+$row['feb']+$row['mar']+$row['abr']+$row['may']+$row['jun']+$row['jul']+$row['ago']+$row['sep']+$row['oct']+$row['nov']+$row['dic'],2);
			$i++;
		}
	}
}
echo json_encode($datos);
?>