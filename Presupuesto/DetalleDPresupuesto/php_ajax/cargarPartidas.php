<?php
header("Content-type: text/html; charset=ISO-8859-1");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();

	$consulta = "select partida, nompartida from presupmpartidas where estatus<9000";
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']= trim($row['partida']);
			$datos[$i]['nombre']=trim($row['partida']).".-".utf8_encode(trim($row['nompartida']));
           $i++;
		}
		
		$datos[$i]['id']= "";
		$datos[$i]['nombre']="TODAS";
	}

echo json_encode($datos);
?>