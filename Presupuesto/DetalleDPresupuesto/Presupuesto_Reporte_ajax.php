<?php
header("Content-type: text/html; charset=UTF-8");
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$annio="";
$mes1="";
$mes2="";
$tipo="";
$partidaOCuenta="";
$id="";
$nom="";
if(isset($_REQUEST['annio']))
	$annio=$_REQUEST['annio'];
if(isset($_REQUEST['mes1']))
	$mes1=$_REQUEST['mes1'];
if(isset($_REQUEST['mes2']))
	$mes2=$_REQUEST['mes2'];
if(isset($_REQUEST['tipo']))
	$tipo=$_REQUEST['tipo'];
	
if(isset($_REQUEST['partidaOCuenta']))
	$partidaOCuenta=$_REQUEST['partidaOCuenta'];
	
if(isset($_REQUEST['cuentapart']))
	list($id, $nom)=explode(".-",$_REQUEST['cuentapart']);

	
	$command= "EXECUTE sp_presupuesto_reporte  $mes1, $mes2, $annio, $tipo, $partidaOCuenta, '$id', '$id'";
	//$command= "EXECUTE sp_presupuesto_reporte  8, 8, 2013, 1, 1, '', ''";
	
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$datos[$i]['docto']=trim($row['numdocto']);
		$datos[$i]['tipodocto']=trim(utf8_encode($row['tipodocto']));
		$datos[$i]['falta']=trim($row['falta']);				
		if(trim(utf8_encode($row['nomprov']))=="")
			$datos[$i]['nomprov']="**** Sin Proveedor  ******";
		else
			$datos[$i]['nomprov']=trim(utf8_encode($row['nomprov']));
		$datos[$i]['importe']=trim(number_format($row['total'],2));
		$datos[$i]['concepto']=trim(utf8_encode($row['concepto']));
		$datos[$i]['xx']=trim(utf8_encode($row['xx']));
		$datos[$i]['cuenta']=trim(utf8_encode($row['cuenta']));
		$datos[$i]['partida']=trim(utf8_encode($row['partida']));
		//$datos[$i]['estatus']=trim($row['estatus']);
		$i++;
	}
					//$proveedor = $db->get_results);
				// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>