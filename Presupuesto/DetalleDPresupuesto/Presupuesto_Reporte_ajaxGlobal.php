<?php
header("Content-type: text/html; charset=UTF-8");
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$tipo = 0;
if(isset($_REQUEST['tipo']))
	$tipo= $_REQUEST['tipo'];
	
$annio = 0;
if(isset($_REQUEST['annio']))
	$annio = $_REQUEST['annio'];
	
$mes1 = 0;
if(isset($_REQUEST['mes1']))
	$mes1 = $_REQUEST['mes1'];
	
$mes2 = 0;
if(isset($_REQUEST['mes2']))
	$mes2 = $_REQUEST['mes2'];
	
$rubros = "";
if(isset($_REQUEST['rubros']))
	$rubros = $_REQUEST['rubros'];
	
$seleccion = 0;
if(isset($_REQUEST['seleccion']))
	$seleccion = $_REQUEST['seleccion'];
	
	$command= "EXECUTE sp_presupuesto_reporte_grupal  $mes1, $mes2, $annio, $tipo, $seleccion, '$rubros'";
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['partida']= $row['partida'];
		$datos[$i]['nompartida']= trim(utf8_encode($row['nompartida']));
		$datos[$i]['totgral']=  number_format($row['totgral'],2);
		
		$datos[$i]['id']=  $row['id'];
		$datos[$i]['numdocto']=  $row['numdocto'];
		$datos[$i]['factura']=  $row['factura'];
		$datos[$i]['cnumdocto']=  $row['cnumdocto'];
		$datos[$i]['xx']=  $row['xx'];
		$datos[$i]['tipodocto']=  $row['tipodocto'];
		$datos[$i]['nomprov']=  trim(utf8_encode($row['nomprov']));
		$datos[$i]['falta']=  $row['falta'];
		$datos[$i]['total']= number_format( $row['total'],2);
		$datos[$i]['concepto']=  trim(utf8_encode($row['concepto']));
		$datos[$i]['cuenta']=  trim($row['cuenta']);
		$datos[$i]['rubro']=  trim($row['rubro']);
		$i++;	
	}
				
echo json_encode($datos);   // Los codifica con el jason
?>