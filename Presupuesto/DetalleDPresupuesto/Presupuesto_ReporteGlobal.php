<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Resumen Global de Presupuesto</title>
        <script src="../../javascript_globalfunc/funcionesGlobales.js"></script>
		<script src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
		<script src="../../cheques/javascript/funciones_cheque.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		var alldata= new Array();
		function validaMes2()
			{
				var mes1= $('#mes1').val();
				var mes2= $('#mes2').val();
				if(mes1>mes2)
				{
					alert("El mes de corte no puede ser anterior al mes inicial.");
					return false;
				}
				return true;
			}
          
			function buscar()
			{
                
					var tipo = $("#opc option:selected").val();
					var annio = document.getElementById('annio').value;
					var mes1 = document.getElementById('mes1').value;
					var mes2 = document.getElementById('mes2').value;
					var rubros = $('#rubros option:selected').val();
					
					var seleccion=0;
					if(rubros!="")
					{
						seleccion=1;
					}
					
					var data = 'tipo=' + tipo +"&annio="+annio+"&mes1="+mes1+"&mes2="+mes2+"&rubros="+rubros+"&seleccion="+seleccion;     
					//alert(data);
					$.post('Presupuesto_Reporte_ajaxGlobal.php',data, function(resp)
					{ //Llamamos el arch ajax para que nos pase los datos
						
						console.log(resp);
						alldata = resp;
						
						var partidas=new Array();
						var j=0;
						for(var i=0;i<alldata.length;i++)
						{
							if(i==0)
							{
								var partida= new Array();
								partida['partida']= alldata[i]['partida'];
								partida['nompartida']= alldata[i]['nompartida'];
								partida['totgral']= alldata[i]['totgral'];
								
								partidas[j]=partida;
								j++;
							}
							else
							{
								if(partidas[j-1]['partida']!= alldata[i]['partida'] && 
									partidas[j-1]['nompartida']!= alldata[i]['nompartida'] &&
									partidas[j-1]['totgral']!= alldata[i]['totgral'])
								{
									var partida= new Array();
									partida['partida']= alldata[i]['partida'];
									partida['nompartida']= alldata[i]['nompartida'];
									partida['totgral']= alldata[i]['totgral'];
									
									partidas[j]=partida;
									j++;
								}
							}
						}
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(partidas).appendTo('#proveedor'); 
						// Checa el plugin de templating para Java, tomando los productos del script de abajo y 
						// los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion    
            }
			
			function verDetalle(partida, nompartida, totgral)
			{
				
				var cuentas= new Array;
				var j=0;
				$('#ctasAsignadas').empty();
				var total=0.00;
				for(var i=0; i< alldata.length; i++)
				{
					if(partida== alldata[i]['partida'] && 
					   nompartida== alldata[i]['nompartida'] && 
					   totgral== alldata[i]['totgral'] )
					{
						var cuenta=new Array;
						cuenta['cuenta']= alldata[i]['cuenta'];
						cuenta['id']= alldata[i]['id'];
						cuenta['tipodocto']= alldata[i]['tipodocto'];
						cuenta['nomprov']= alldata[i]['nomprov'];
						cuenta['falta']= alldata[i]['falta'];
						cuenta['total']= alldata[i]['total'];
						cuenta['concepto']= alldata[i]['concepto'];
						
						cuentas[j] = cuenta;
						total += parseFloat(cuenta['total'].replace(/,/g,""));
						j++;
					}
				}
				var totalstr=addCommas(total);
				
				$('#totalClasificacion').html(totalstr);
				$('#NPartida').html(partida+".-"+nompartida);
				$('#tmpl_presingmcuentas').tmpl(cuentas).appendTo('#ctasAsignadas'); 
				loadPopupBox();
				//NPartida = ;
			}
			
			//Exportar a EXCEL
			jQuery(document).ready(function() {
						
					jQuery(".botonExcel").click(function(event) {
							jQuery("#datos_a_enviar").val( jQuery("<div>").append( jQuery("#Exportar_a_Excel").eq(0).clone()).html());
							jQuery("#FormularioExportacion").submit();
					});
				
					//Exportar a PDF
					jQuery(".botonPDF").click(function(event) 
					{
						
							jQuery("#datos_a_enviarPDF").val( jQuery("<div>").append( jQuery("#Exportar_a_Excel").eq(0).clone()).html());
							jQuery("#opc2").val(jQuery("#opc option:selected").text());
							jQuery("#annio2").val(jQuery("#annio").val())
							jQuery("#mes12").val( jQuery("#mes1 option:selected").text())
							jQuery("#mes22").val( jQuery("#mes2 option:selected").text());
							jQuery("#rubro2").val(jQuery("#opc option:selected").text());
							jQuery("#FormularioExportacionPDF").submit();
					});
					
					jQuery(".botonDetallePDF").click(function(event) 
					{
						
							jQuery("#datos_a_enviarDetallePDF").val( jQuery("<div>").append( jQuery("#detalleExportar_a_Excel").eq(0).clone()).html());
							jQuery("#opc3").val(jQuery("#opcdet option:selected").text());
							jQuery("#annio3").val(jQuery("#annio").val())
							jQuery("#mes13").val( jQuery("#mes1 option:selected").text())
							jQuery("#mes23").val( jQuery("#mes2 option:selected").text());
							jQuery("#rubro3").val(jQuery("#opc option:selected").text());
							jQuery("#FormularioExportacionDetallePDF").submit();
					});
			
				});
			
			
			
			 $(document).ready( function() {
   
										// When site loaded, load the Popupbox First
										//loadPopupBox();
								   
										$('#popupBoxClose').click( function() {           
											unloadPopupBox();
										});
									   
										$('#container').click( function() {
											unloadPopupBox();
										});
							 });
 function unloadPopupBox() {    // TO Unload the Popupbox
            $('#popup_box').fadeOut("slow");
            $("#container").css({ // this is just for style       
                "opacity": "1" 
            });
			$('#ctasAsignadas').empty();
			$('#cuenta').val("");
			$('#monto').val("");
			
			//Elimina las cuentas del grupo
			//cuentasDGrupo.splice(0,cuentasDGrupo.length);
			
			$('#grupo').val("");
			$('#totalClasificacion').val("");
        }   
       
        function loadPopupBox() {    // To Load the Popupbox
            $('#popup_box').fadeIn("slow");
            $("#container").css({ // this is just for style
                "opacity": "0.3" 
            });        
        }     
			</script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if partida}}
					<td align='center'>${partida}</td>
					<td align='left'>${nompartida}</td>
					<td align='right'>$${totgral}</td>
					<td><img src="../../imagenes/consultar.jpg" width="27" height="26" onclick="verDetalle(${partida}, '${nompartida}', '${totgral}')"></td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
        
        <script id="tmpl_presingmcuentas" type="text/x-jquery-tmpl">   
            <tr>
                    	<td align="center" >${cuenta}</td>
                        <td align="center">${id}</td>
						<td align="left" >${tipodocto}</td>
						<td align="left" >${nomprov}</td>
						<td align="center" >${falta}</td>
                    	<td align="right">$${total}</td>
						<td align="left" >${concepto}</td>
            </tr>
		</script>
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Resumen Global de Presupuesto</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" onChange="busca_cheque()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Comprometido</option>
              <option value="2" >Devengado</option>
              <option value="3" >Ejercido</option>
              <option value="4" >Pagado</option>
           </select></td>
<td>Año:<input type="text" value="<?php echo date("Y")?>" style="width:45px; text-align:right;" id="annio" name="annio"></td>
<td>Mes:<select id="mes1" name="mes1">
			<option value="01">ENE</option>
            <option value="02">FEB</option>
            <option value="03">MAR</option>
            <option value="04">ABR</option>
            <option value="05">MAY</option>
            <option value="06">JUN</option>
            <option value="07">JUL</option>
            <option value="08">AGO</option>
            <option value="09">SEP</option>
            <option value="10">OCT</option>
            <option value="11">NOV</option>
            <option value="12">DIC</option>
		</select></td>
<td>Mes Corte:<select id="mes2" name="mes2" onChange="validaMes2()">
			<option value="01">ENE</option>
            <option value="02">FEB</option>
            <option value="03">MAR</option>
            <option value="04">ABR</option>
            <option value="05">MAY</option>
            <option value="06">JUN</option>
            <option value="07">JUL</option>
            <option value="08">AGO</option>
            <option value="09">SEP</option>
            <option value="10">OCT</option>
            <option value="11">NOV</option>
            <option value="12">DIC</option>
		</select></td>
</tr>
<tr>
	<td><select name="rubros" id="rubros">
    		<?php
					$consulta = "select rubro, nomrubro from presupmrubros where estatus<9000";
					$R = sqlsrv_query( $conexion,$consulta);
					if ( $R === false)
					{ 
						$resoponsecode="02";
						die($consulta."". print_r( sqlsrv_errors(), true));
					}
					else
					{
						while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
						{
							?>
							<option value="<?php echo trim($row['rubro']);?>" ><?php echo trim($row['rubro']).".-".utf8_encode(trim($row['nomrubro']));?></option>
							<?php
						}
						
					}
				?>
    <option value="">TODOS</option>
</select></td>
    <td align="right" colspan=3>
    	<table width="100%">
        <tr>
        	<td style="width:1/3%;"><input type="button" value="Buscar" onClick="buscar()"></td>
    		<td style="width:1/3%;">
            					<form style="width:100%;" action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion" 
                                				title="Los datos que se muestran en la tabla de abajo son lo que seran enviados a excel.">Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /><input type="hidden" id="datos_a_enviar" name="datos_a_enviar" /></form>
            </td>
 			<td style="width:1/3%;">
                                <form style="width:100%;" action="php_ajax/crearResumenglobalPresupuestosPDF.php" method="post" target="_blank" 
                                				id="FormularioExportacionPDF" 
                                                title="Los datos que se muestran en la tabla de abajo son lo que seran enviados a excel.">Exportar a PDF <img src="../../imagenes/pdf icon.gif" width="30px" class="botonPDF">
                                                <input type="hidden" id="datos_a_enviarPDF" name="datos_a_enviarPDF" />
                                                <input type="hidden" id="opc2" name="opc2">
                                                <input type="hidden" id="annio2" name="annio2">
                                                <input type="hidden" id="mes12" name="mes12">
                                                <input type="hidden" id="mes22" name="mes22">
                                                <input type="hidden" id="rubro2" name="rubro2">
                                </form>
            </td>
        </tr>
    </table>
    </td>
   
</tr>
</table>
<table  name="Exportar_a_Excel" id="Exportar_a_Excel" style="width:100%;">
                <thead>
                    <th>Partida</th> 
                    <th>Nom. Partida</th>                                      
                    <th>Total</th>
                    <th>Ver</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
 </div>
 <div id="popup_box" >    
    <a id="popupBoxClose">X</a> 
    <form style="width:100%;" action="php_ajax/crearResumenglobalPresupuestosDetallePDF.php" method="post" target="_blank" 
                                				id="FormularioExportacionDetallePDF" 
                                                title="Generar PDF.">Exportar a PDF <img src="../../imagenes/pdf icon.gif" width="30px" class="botonDetallePDF">
                                                <input type="hidden" id="datos_a_enviarDetallePDF" name="datos_a_enviarDetallePDF" />
                                                <input type="hidden" id="opc3" name="opc3">
                                                <input type="hidden" id="annio3" name="annio3">
                                                <input type="hidden" id="mes13" name="mes13">
                                                <input type="hidden" id="mes23" name="mes23">
                                                <input type="hidden" id="rubro3" name="rubro3">
                                </form>
    <table width="100%" name="detalleExportar_a_Excel" id="detalleExportar_a_Excel">
    	<tr>
        	<td>
            	<span class="TituloDForma">Detalle por Partida: <span id="NPartida"></span></span>
    			<hr class="hrTitForma">
            </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
        <tr>
        	<td width="100%">
                 <table width="100%" class="tablesorter">
                    <thead>
                        <tr>
                            <th align="center">Cuenta</tH>
                            <th align="center">Folio</th>
                            <th align="center">Tipo Docto.</th>
                            <th align="center">Proveedor</th>
                            <th align="center">Fecha</th>
                            <th align="center">Total</th>
                            <th align="center">Concepto</th>
         				</tr>
                    </thead>
                    <tbody id="ctasAsignadas" name="ctasAsignadas">
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td colspan="6" align="right">Restan: $<span id="totalClasificacion">0.00</span></td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
        <tr>
        	<td align="center"><input type="button" value="Cerrar" onclick="unloadPopupBox()" /></td>
        </tr>
    </table>  
   
</div>
    </body>
</html>
