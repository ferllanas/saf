<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
		
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

	$datos=array();
	if ($conexion)
	{
		$consulta = "select * from configmmeses order by mes";
		
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			//echo $consulta;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['mes']= trim($row['mes']);
				$datos[$i]['nombre']= trim($row['nombre']);
				$datos[$i]['corto']= trim($row['corto']);
				$i++;
			}
		}
	}
echo json_encode($datos);
?>