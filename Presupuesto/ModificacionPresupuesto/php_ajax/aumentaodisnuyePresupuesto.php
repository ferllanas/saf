<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$usuario ="";
	if(isset($_COOKIE['ID_my_site']))
		$usuario = $_COOKIE['ID_my_site'];
		
$ENE="";
if(isset($_REQUEST['ENE']))
	$ENE=$_REQUEST['ENE'];

$FEB="";
if(isset($_REQUEST['FEB']))
	$FEB=$_REQUEST['FEB'];

$MAR="";
if(isset($_REQUEST['MAR']))
	$MAR=$_REQUEST['MAR'];
	
$ABR="";
if(isset($_REQUEST['ABR']))
	$ABR=$_REQUEST['ABR'];

$MAY="";
if(isset($_REQUEST['MAY']))
	$MAY=$_REQUEST['MAY'];

$JUN="";
if(isset($_REQUEST['JUN']))
	$JUN=$_REQUEST['JUN'];

$JUL="";
if(isset($_REQUEST['JUL']))
	$JUL=$_REQUEST['JUL'];
	
$AGO="";
if(isset($_REQUEST['AGO']))
	$AGO=$_REQUEST['AGO'];
	
$SEP="";
if(isset($_REQUEST['SEP']))
	$SEP=$_REQUEST['SEP'];
	
$OCT="";
if(isset($_REQUEST['OCT']))
	$OCT=$_REQUEST['OCT'];
	
$NOV="";
if(isset($_REQUEST['NOV']))
	$NOV=$_REQUEST['NOV'];
	
$DIC="";
if(isset($_REQUEST['DIC']))
	$DIC=$_REQUEST['DIC'];
	
$cuenta="";
if(isset($_REQUEST['cuenta']))
	$cuenta=$_REQUEST['cuenta'];
	
$anio="";
if(isset($_REQUEST['annio']))
	$anio=$_REQUEST['annio'];
	
$motivo="";
if(isset($_REQUEST['motivo']))
	$motivo=$_REQUEST['motivo'];

	
	
$tsql_callSP ="{call sp_presup_A_dmodif(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	$params = array(&$anio,
								&$ENE,
								&$FEB, 
								&$MAR,
								&$ABR, 
								&$MAY,
								&$JUN,
								&$JUL,
								&$AGO,
								&$SEP,
								&$OCT,
								&$NOV,
								&$DIC,
								&$cuenta,
								&$usuario,
								&$motivo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		
		$datos[0]['error']=print_r($params)."".sqlsrv_errors();
		$datos[0]['fallo']=true;
		// die("sp_egresos_A_mcheque".   print_r( , true));
	}
	else
	{
		$datos[0]['fallo']=false;
	}
	/*if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	sqlsrv_close( $conexion);*/
	
	echo json_encode($datos);
	
?>