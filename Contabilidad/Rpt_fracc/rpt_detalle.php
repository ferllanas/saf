<?php
require_once("../../connections/dbconexionFome.php");
require_once("../../Administracion/globalfuncions.php");
require_once("../../dompdf/dompdf_config.inc.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$mun="";
if($_REQUEST['mpo'])
	$mun=$_REQUEST['mpo'];
$fracc="";
if($_REQUEST['fracc'])
	$fracc=$_REQUEST['fracc'];
$descmun="";
if($_REQUEST['desc'])
	$descmun=$_REQUEST['desc'];
$nomcol="";
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($conexion)
{
	$consulta="select nomcol from tecnicmfracc where fracc='$fracc' ";
	$getProducts = sqlsrv_query($conexion,$consulta);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		//echo $resoponsecode;
		$i=0;
		while( $row2 = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
				$nomcol=$row2['nomcol'];
		}
	}
}
?><html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        <script src="javascript/navegacion.js"></script>
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone() ).html());
				$("#FormularioExportacion").submit();
		});
		});			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left">${cuenta}</td>
					<td align="left">${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Reporte de Lotes No Asignados del Municipio de <?php echo $descmun; ?> Fraccionamiento <?php echo utf8_encode($nomcol); ?> </span>
    
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table > 
<tr><td><input type="button" value="Regresar" onClick="location.href='rpt_mpo.php?mpo=<?php echo $mun;?>&desc=<?php echo $descmun;?>'"/>  
<tr>



<input type="hidden" name="ren" id="ren">
    <form action="ficheroExcelDet.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th width="80px">F-M-L</th>
                <th width="80px">Tipo Lote</th>
                <th width="120px">Descripción</th>
                <th width="100px">Dirección</th>
                <th width="80px">Vendible</th> 
                <th width="80px">Area M2</th>   
                <th width="80px">Origen</th>
                                                                           
                </thead>
                <tbody id="proveedor" name='proveedor'>
                <?php 
				$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
				$conexion = sqlsrv_connect($server,$infoconexion);
				if($conexion)
				{
				 	$consulta="select a.fml,a.tipolote,a.vendible,a.adm,a.aream2,a.calle,a.exterior,b.descripcion,c.origen from tecnicdlotes a 
								left join catsmscat b on a.tipolote=b.scat left join tecnicmfracc c on a.fracc=c.fracc where a.fracc='$fracc' and a.cvesit='21070000'  order by a.fml";
					//echo $consulta;
					$getProducts = sqlsrv_query($conexion,$consulta);
					if ( $getProducts === false)
					{ 
						$resoponsecode="02";
						$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
						//echo $descriptioncode;
					}
					else
					{
					
						$resoponsecode="Cantidad rows=".count($getProducts);
						//echo $resoponsecode;
						$i=0;
						while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
						{			
							?>
                            <tr>
                             <td align="center"><b><?php echo substr($row['fml'],0,4)."-".substr($row['fml'],4,4)."-".substr($row['fml'],8,4);?></b></td> 
                              <td align="center"><b><?php echo utf8_decode(trim($row['tipolote']));?></b></td>
                              <td align="center"><?php echo $row['descripcion'];?></td>
                              <td align="center"><?php echo $row['calle']." ".$row['exterior'];?></td>
                              <td align="center"><?php echo $row['vendible'];?></td>                              
                              <td align="center"><?php echo number_format($row['aream2'],2,'.',',');?></td>
                              <td align="center"><?php echo $row['origen'];?></td>
                                                    
                            	
                            </tr>
                            <?php
							$i++;
						}
					}
				?>
                    
               	<?php
				}
				?>
                </tbody>
  </table>
    </div>
    </body>
</html>
