<?php
require_once("../../connections/dbconexionFome.php");
require_once("../../Administracion/globalfuncions.php");
require_once("../../dompdf/dompdf_config.inc.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');


?><html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        <script src="javascript/navegacion.js"></script>
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone() ).html());
				$("#FormularioExportacion").submit();
		});
		});			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left">${cuenta}</td>
					<td align="left">${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Reporte General de Lotes </span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>



<input type="hidden" name="ren" id="ren">
    <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th width="80px">Municipio</th>  
                <th width="80px">Total de Lotes</th>
                <th width="80px">No Asignados</th>   
                <th width="80px">En Proceso de Asignación</th>
                <th width="80px">En Cartera</th>
                 <th width="80px">En Proceso de Escrituración</th>
                                                            
                </thead>
                <tbody id="proveedor" name='proveedor'>
                <?php 
				$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
				$conexion = sqlsrv_connect($server,$infoconexion);
				if($conexion)
				{
				 	$consulta="select b.mun,c.descripcion,
								sum(CASE WHEN a.cvesit = '21070000' THEN 1 END) as libres,
								sum(CASE WHEN a.cvesit = '21070003' THEN 1 END) as ProcAsig,
								sum(CASE WHEN a.cvesit = '21070004' THEN 1 END) as Cartera,
								sum(CASE WHEN a.cvesit >= '21070005' and a.cvesit < '21079000' THEN 1 END) as ProcEsc,
								COUNT(a.fml) as lotes from tecnicdlotes a 
								left join tecnicmfracc b on a.fracc=b.fracc
								left join catsmscat c on b.mun=c.scat 
								where a.cvesit<'21079000' group by b.mun,c.descripcion order by b.mun";
					$getProducts = sqlsrv_query($conexion,$consulta);
					if ( $getProducts === false)
					{ 
						$resoponsecode="02";
						$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
						//echo $descriptioncode;
					}
					else
					{
					
						$resoponsecode="Cantidad rows=".count($getProducts);
						//echo $resoponsecode;
						$i=0;
						while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
						{			
							?>
                            <tr>
                              
                              <td><b><?php echo utf8_decode(trim($row['descripcion']));?></b></td>
                              <td align="center"><a href="rpt_mpo.php?mpo=<?php echo $row['mun'];?>&desc=<?php echo $row['descripcion'];?>"><?php echo number_format($row['lotes'],0,'.',',');?></a></td>
                              <td align="center"><?php echo number_format($row['libres'],0,'.',',');?></td>
                              <td align="center"><?php echo number_format($row['ProcAsig'],0,'.',',');?></td>
                              <td align="center"><?php echo number_format($row['Cartera'],0,'.',',');?></td>
                              <td align="center"><?php echo number_format($row['ProcEsc'],0,'.',',');?></td>
                                                        
                            	
        </tr>
                            <?php
							$i++;
						}
					}
				?>
                    
               	<?php
				}
				?>
                </tbody>
  </table>
    </div>
    </body>
</html>
