<?php
header("Content-type: text/html; charset=UTF-8");//ISO-8859-1
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$command="";
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
				$command= "select  a.id, a.tipo, a.poliza, CONVERT(varchar(12),a.fecha, 103) as falta, a.descrip, a.estatus,
							b.tipo, b.mov,  b.cargo, b.credito, ltrim(rtrim(b.cuenta))+'.' as cuenta, b.referencia, b.estatus, c.descrip as docorigen, d.nombre from contabmpoliza a LEFT JOIN contabdpoliza b on a.poliza=b.poliza  LEFT JOIN egresostipodocto c ON c.id=b.docorigen LEFT JOIN menumusuarios d ON d.usuario=b.usuario WHERE a.poliza LIKE '%" . substr($_REQUEST['query'],1) . "%' order by a.poliza, b.mov";
	}
	if($opcion==2)
	{
				$command= "select  a.id, a.tipo, a.poliza, CONVERT(varchar(12),a.fecha, 103) as falta, a.descrip, a.estatus,
							b.tipo, b.mov,  b.cargo, b.credito, ltrim(rtrim(b.cuenta))+'.' as cuenta, b.referencia, b.estatus, c.descrip as docorigen, d.nombre from contabmpoliza a LEFT JOIN contabdpoliza b on a.poliza=b.poliza  LEFT JOIN egresostipodocto c ON c.id=b.docorigen LEFT JOIN menumusuarios d ON d.usuario=b.usuario  WHERE a.tipo LIKE '%" . substr($_REQUEST['query'],1) . "%' order by a.poliza";
	}
	
	
	//	echo $opcion;
	if($opcion==4)
 	{
				$command= "select  a.id, a.tipo, a.poliza, CONVERT(varchar(12),a.fecha, 103) as falta, a.descrip, a.estatus,
							b.tipo, b.mov,  b.cargo, b.credito,  ltrim(rtrim(b.cuenta))+'.' as cuenta, b.referencia, b.estatus, c.descrip as docorigen, d.nombre from contabmpoliza a 
LEFT JOIN contabdpoliza b on a.poliza=b.poliza 
LEFT JOIN egresostipodocto c ON c.id=b.docorigen 
LEFT JOIN menumusuarios d ON d.usuario=b.usuario 
WHERE a.descrip LIKE '%" . substr($_REQUEST['query'],1) . "%' order	by a.poliza";

     }
	//echo "Antes de pasar al 5";
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select  a.id, a.tipo, a.poliza, CONVERT(varchar(12),a.fecha, 112) as falta, a.descrip, a.estatus,b.tipo, b.mov,
b.cargo, b.credito, b.docorigen, ltrim(rtrim(b.cuenta))+'.' as cuenta, b.referencia, b.estatus, c.descrip as docorigen, d.nombre from contabmpoliza a LEFT JOIN contabdpoliza b on a.poliza=b.poliza LEFT JOIN egresostipodocto c ON c.id=b.docorigen LEFT JOIN menumusuarios d ON d.usuario=b.usuario WHERE
CONVERT(varchar(12),a.fecha, 112) >='" . $fini . "' and CONVERT(varchar(12),a.fecha, 112) <='" . $ffin . "' order by a.fecha";//"select a.id, a.tipo, a.poliza, CONVERT(varchar(12),a.fecha, 103) as falta, a.descrip, a.estatus, b.tipo, b.mov,  b.cargo, b.credito, b.docorigen, ltrim(rtrim(b.cuenta))+'.' as cuenta, b.referencia, b.estatus, c.descrip as docorigen, d.nombre from contabmpoliza a LEFT JOIN contabdpoliza b on a.poliza=b.poliza LEFT JOIN egresostipodocto c ON c.id=b.docorigen LEFT JOIN menumusuarios d ON d.usuario=b.usuario WHERE a.falta >= '" . $fini . "' and a.falta <='" . $ffin . "'";
				 
	}	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco	
					$datos[$i]['id']=trim($row['id']);
					$datos[$i]['folio']=trim($row['poliza']);
					$datos[$i]['docorigen']=trim($row['docorigen']);
					$datos[$i]['mov']=trim($row['mov']);
					$datos[$i]['falta']=trim($row['falta']);
					$datos[$i]['cuenta']=trim($row['cuenta']);
					$datos[$i]['referencia']=trim($row['referencia']);
					$datos[$i]['tipo']=trim($row['tipo']);
					$datos[$i]['concepto']=trim(utf8_decode($row['descrip']));
					$datos[$i]['estatus']=trim($row['estatus']);
					$cargo = (float)$row['cargo'];
					$datos[$i]['cargo']= number_format($cargo,2,".",",");
					$credito=(float)$row['credito'];
					$datos[$i]['credito']= number_format($credito,2,".",",");
					$datos[$i]['usuario']= trim(utf8_decode($row['nombre']));
					$datos[$i]['estatus']= $row['estatus'];
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>