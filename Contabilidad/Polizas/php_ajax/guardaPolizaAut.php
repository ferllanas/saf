﻿<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");
	$fecha_poliza="";
	$tipo_poliza="";
	$nombreusuario="";
	$tfacturas="";
	
	$fecha="";
	$conceptos="";
	//$concepto="";
	$numero_poliza="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$partidas=array();//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$descripcion="";
	$id_facts="";
	$fecpoliza="";
	if(isset($_POST['polizas']))
			$partidas=$_POST['polizas'];
			
	//print_r($partidas);
			
	$fechapolizanormal=date("Ymd"); 
	if(isset($_POST['tipo']))
		$tipo_poliza=$_POST['tipo'];

	if(isset($_POST['numpoliza']))
		$numero_poliza=$_POST['numpoliza'];
		
	$fecini=$_POST['fecini'];
	$fecfin=$_POST['fecfin'];
	$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
	$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
	//$numero_poliza = convertirFechaEuropeoAAmericano($numero_poliza);
	$descripcion="Poliza de Gasto de Solicitud de Cheque del ".$fini." al ".$ffin;
	if(isset($usuario))
	{
		if($tipo_poliza=='I' || $tipo_poliza=='D' || $tipo_poliza=='E')
		{
			//Obtencion de nombre de usuario
			$command="SELECT nombre, appat, apmat FROM nominadempleados WHERE numemp='$usuario'";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$nombreusuario=$row['nombre']." ".$row['appat']." ".$row['apmat'];
					
				}
			}
			
			$existe=false;
			$command="SELECT id FROM contabmpoliza WHERE tipo='$tipo_poliza' AND poliza =$numero_poliza";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$existe=true;	
					
				}
			}
			
			if(!$existe)
			{
						
				if ( is_array( $partidas ) ) // Pregunta se es una array la variable facturas
				{		
					list($id, $fecha, $hora)= fun_contab_A_mpoliza("0",$tipo_poliza,
									$numero_poliza,
									$descripcion,
									$usuario,
									$fechapolizanormal); 
					if(strlen($id)>0)//Para ver si en realidad se genero una poliza
					{
						for( $i=0 ; $i  < count($partidas) ; $i++ )
						{
							
								$partidas[$i]['cargo']=str_replace(",","",$partidas[$i]['cargo']);
								if(strlen($partidas[$i]['cargo'])<1)
									$partidas[$i]['cargo']=0;
								$partidas[$i]['credito']=str_replace(",","",$partidas[$i]['credito']);
								if(strlen($partidas[$i]['credito'])<1)
									$partidas[$i]['credito']=0;
								$fails = func_contab_A_dpoliza('0',
										$tipo_poliza,
										$numero_poliza, 
										$i,
										$partidas[$i]['cuenta'], 
										$partidas[$i]['concepto'],
										$partidas[$i]['referencia'],
										$partidas[$i]['cargo'],
										$partidas[$i]['credito'],
										'0000',
										$usuario,
										$partidas[$i]['origen'],
										$partidas[$i]['id_registro']);										
										$totalCredito+= $partidas[$i]['credito'];
										$totalCargos+= $partidas[$i]['cargo'];
								/*$tfacturas.="<tr><td  style='width: 15%;' align='center'>".$partidas[$i]['cuenta']."</td><td style='width: 35%;'>".utf8_decode($partidas[$i]['concepto'])."</td><td style='width: 10%;' align='center'>".$partidas[$i]['referencia']."</td><td  style='width: 15%;' align='center'>".number_format($partidas[$i]['cargo'],2)."</td><td style='width: 15%;' align='center'>".number_format($partidas[$i]['credito'],2)."</td></tr>";//<td class='texto11'>".trim($fecha_poliza)."</td>*/
								
						}
						
						/*list($days,$month,$years)= explode('/', $fecha);
						$diadesem=date("w", strtotime("$years-$month-$days")); //date("w");
						$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
							
						$datos[0]['path'] = crearPolizaPDF($id,$ffecha,$tipo_poliza,$numero_poliza, utf8_decode($descripcion),$nombreusuario, $tfacturas,number_format($totalCredito,2),number_format($totalCargos,2), $hora, $fechapolizanormal);*/
					}
					else
						{	$fails=true; $msgerror="No pudo generar ningun vale";}
					
				}
				else
				{
					$fails=true; $msgerror=" No es un array la variable";
				}
			}
			else
			{
				$fails=true; $msgerror="Ya existe una poliza con el tipo y poliza especificados.";
			}
		}
		else
		{
			$fails=true; $msgerror="No es un tipo de poliza valido.";
		}
			
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	

	if($fecha_poliza=="true" )
		$datos[0]['id']=trim($id);// regresa el id de la
	else
		$datos[0]['id']=$id_facts;
		
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_contab_A_mpoliza($id,$tipo_poliza,
								$numero_poliza,
								$descripcion,
								$usuario,
								$fecpoliza)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_contab_A_mpoliza(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$tipo_poliza,
								&$numero_poliza,
								&$descripcion,
								&$usuario,
								&$fecpoliza);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("sp_contab_A_mpoliza".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

//Funcion para registrar el p´roductoa  la requisicion
function func_contab_A_dpoliza($id,
								$tipo,
								$poliza, 
								$mov,
								$cuenta, 
								$concepto,
								$referencia,
								$cargo,
								$credito,
								$estatus,
								$usuario,
								$origen,
								$id_registro)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//$tsql_callSP ="{call sp_egresos_A_dcheque(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$partidas[$i]['producto'], $partidas[$i]['cantidad'], $partidas[$i]['descripcion'],'0',$partidas[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = $tsql_callSP ="{call sp_contab_poliza_paso2(?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$id,&$tipo,&$poliza,&$mov,&$cuenta,								
								&$concepto,								
								&$referencia,
								&$cargo,
								&$credito,
								&$estatus,
								&$usuario,
								&$origen,
								&$id_registro);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( "sp_contab_A_dpoliza". print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}

function crearPolizaPDF($id, $fecha,$tipo_poliza,$numero_poliza, $descripcion,$nombreusuario, $tfacturas,$totalCredito,$totalCargos, $hora, $fecha_poliza)
{
	global $imagenPDFPrincipal;
	$dompdf = new DOMPDF();
		$pageheadfoot='<script type="text/php"> 
		if ( isset($pdf) ) 
		{ 	
			$pdf->page_text(30, 760, "Pagina {PAGE_NUM}", "", 10, array(0,0,0)); 
		} 
</script> ';  
	
	$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<style>
body 
{
	font-family:'Arial';
	font-size:8;
}

table {
  
	vertical-align:top;
	border-collapse:collapse; 
	border: none;
}
td {padding: 0;}

#twmaarco
{
	border: 1pt solid black;
}

.textobold{
	font-weight:bold;
}
.texto14{ 
	font-size: 15pt;
	font-weight:bold;
}

.texto12{ 
	font-size: 12pt;
}
.texto12bold
{
	font-size: 12pt;
	font-weight:bold;
}
.texto11{
	font-size: 11pt;
}
</style>

</head>

<body>
<table style='width: 90%;margin-top: 2em; margin-left: 2em; margin-right: 3em;' width='100%'>
	<tr>
		<td style='width: 100%;' width='100%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 30%; height: 22px;' colspan='2'>
                    	<table style='width: 100%;'>
                        	<tr>
                            	<td style='width: 20%;'><img src='../../../$imagenPDFPrincipal' style='width: 100px; height: 38px;'></td>
                                <td style='width: 80%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
                            </tr>
                        </table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 50%; vertical-align:top;' align='left' class='texto12'>CAPTURA DE POLIZA No. $tipo_poliza-$numero_poliza</td>
								<td style='width: 50%; vertical-align:top;' align='right'>Fecha:  $fecha $hora</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td  align='left'>Concepto: $descripcion</td><td align='right'>".utf8_decode("Fecha de Póliza").":    $fecha_poliza</td>
				</tr>
				<tr>
				<td colspan='2'>&nbsp;</td>
				<tr>
                	<td style='width: 100%;' colspan='2'>
						<table style='width: 100%;' >
							<thead>
								<tr>
									<th style='width: 15%;' >Cuenta</th>
									<th style='width: 35%;' >Concepto</th>
									<th style='width: 10%;' >Referencia</th>
									<th style='width: 15%;' >Cargos</th>
									<th style='width: 15%;' >Creditos</th>
								</tr>
								<tr>
									<td colspan='5'><hr></td>
								</tr>
							</thead>
							<tbody>$tfacturas</tbody>
						   <tfoot>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td colspan='5'>&nbsp;</td>
								</tr>
						   		<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td colspan='3'><hr></td>
								</tr>
								<tr>
									<td style='width: 10%;' >&nbsp;</td>
									<td style='width: 40%;' >&nbsp;</td>
									<td style='width: 10%;' align='right'><b>Total</b></td>
									<td style='width: 15%;' align='center'><b>$totalCargos</b></td>
									<td style='width: 15%;' align='center'><b>$totalCredito</b></td>
								</tr>
							</tfoot>
					   </table>
                   	</td>
                </tr>
				<tr>
					<td colspan='2'>
						<table style='width: 70%;'>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                                <td style='width: 20%; height: 20px;'>&nbsp;</td>
                                <td style='width: 40%; height: 20px;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'>$nombreusuario</td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'>&nbsp;</td>
                            </tr>
							<tr>
                                <td style='width: 40%;'><HR></td>
                                <td style='width: 20%;'>&nbsp;</td>
                                <td style='width: 40%;'><HR></td>
                            </tr>
                            <tr>
                                <td style='width: 40%;' align='center' >CAPTURO</td><td style='width: 20%;'>&nbsp;</td><td style='width: 40%;' align='center'  >Vo. Bo.</td>
                            </tr>
						</table>
					</td>
				</tr>
			   </table>
		</td>
	</tr>

</table>
".$pageheadfoot."
</body> </html>";

	//$htmlstr=utf8_encode($htmlstr);
	$dompdf->load_html($htmlstr);
	//$dompdf->set_paper('letter', 'landscape');
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/poliza_".$id.".pdf", $pdf);
	
	return("pdf_files/poliza_".$id.".pdf");
}
?>