<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$datos=array();
$datos[0]['fallo']=true;

$cuenta="";
if(isset($_REQUEST['cuenta']))
	$cuenta= $_REQUEST['cuenta'];
$strcuenta="";
$isnum=false;
for($i=0;$i<strlen($cuenta);$i++)
{
	if($cuenta[$i]=="." || is_numeric($cuenta[$i]))
	{
		if(is_numeric($cuenta[$i]))
		{
			if($i>0)
				if($isnum)
				{
					$strcuenta.=".";
				}

			$strcuenta .= $cuenta[$i];
			$isnum=true;
		}
	}
	else
	{
		$datos[$i]['msgerror']="El número de cuenta contiene un caracter no valido.";
		die( json_encode($datos)); 
	}
}

$strcuenta.=".";
$subcuenta="";
if(isset($_REQUEST['subcuenta']))
	$subcuenta= $_REQUEST['subcuenta'];
	
for($i=0;$i<strlen($subcuenta);$i++)
{
	if($subcuenta[$i]!="." && !is_numeric($subcuenta[$i]))
	{
		$datos[$i]['msgerror']="Número de subcuenta contiene un caracter no valido.";
		die( json_encode($datos)); 
	}
}
	
$strcuenta.=$subcuenta;
$command= "SELECT nombre FROM contabmcuentas WHERE cuenta='".$strcuenta."'";

	
	//echo $command;
		
	
	
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$datos[$i]['nombre']=trim($row['nombre']);
		$datos[$i]['cuenta']=$strcuenta;
		$datos[$i]['fallo']=false;
		$i++;
	}

	echo json_encode($datos);   // Los codifica con el jason
?>