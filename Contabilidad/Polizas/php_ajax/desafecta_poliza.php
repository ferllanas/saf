<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];//"001981";//
	
	$datos=array();//prepara el aray que regresara
	$ffecha=date("Y-m-d");
	$nombreusuario="";
	$tfacturas="";
	$descriptioncode="";
	
	$fecha="";
	$conceptos="";
	//$concepto="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	
	$subtotal=0.00;
	$iva=0.00;
	$totalCredito=0.00;
	$totalCargos=0.00;
	$id_facts="";
	
	
	$polizas=array();//variable que almacena la cadena de los facturas
	if(isset($_REQUEST['polizas']))
	{
		$polizas=$_REQUEST['polizas'];
	}

	if(isset($usuario))
	{
		$polizassql="";
		for($i=0;$i<count($polizas);$i++)
		{
			if($i+1== count($polizas))
				$polizassql .= $polizas;
			else
				$polizassql .= $polizas.',';
		}
		//Obtencion de nombre de usuario
		$command="SELECT SUM(cargo) as cargos, SUM(credito) as credito, a.poliza FROM contabdpoliza a WHERE poliza IN($polizassql) GROUP BY a.poliza";
		$descriptioncode.=$command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				if($row['cargos']==$row['credito'])
				{
					$fails=true;
					$descriptioncode .= "La poliza ".$row['poliza']." no contiene un cargo y credito válido.\n";
				}
			}
		}
		
		if(!$fails)
		{
			for($i=0;$i<count($polizas);$i++)
			{
					//echo $polizas[$i]."\n";
					list($id, $fecha, $hora) = fun_contab_afecta_poliza($polizas[$i]);
			}
		}
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	

		
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_contab_afecta_poliza($poliza)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//ECHO 
	$tsql_callSP ="{call sp_contab_desafecta_poliza(?)}";//Arma el procedimeinto almacenado
	$params = array(&$poliza);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("sp_contab_desafecta_poliza".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['poliza'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

?>