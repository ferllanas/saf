<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$path="";
if(isset($_POST['ppath']))
{
	$path=$_POST['ppath'];
}
if(strlen($path)>0)
{
	$archivo = file("../".$path);
	if (!$archivo) 
	{
		 $datos['error']=1;
		 echo "<p>No se pudo abrir el archivo.</p>";
	}
	$filas=count($archivo);
	for($i=0;$i<$filas;$i++)
	{
		$campo=explode("\t",trim($archivo[$i]));
		if(isset($campo[0]))
		{
		 	$datos[$i]['cuenta']=$campo[0]; 
			$datos[$i]['nomcuenta']=validacuenta($campo[0]);
		}
		else
			$datos[$i]['cuenta']=""; 
			
		if(isset($campo[1]))
		 	$datos[$i]['concepto']=utf8_encode($campo[1]); 
		else
			$datos[$i]['concepto']="";
		if(isset($campo[2]))	
		 	$datos[$i]['cargo']=trim($campo[2]); 
		else
			$datos[$i]['cargo']= "0.00";
			
		if(isset($campo[3]))
		 	$datos[$i]['credito']= trim($campo[3]);
		else
			$datos[$i]['credito']="0.00";
			
		if(isset($campo[4]))
		 	$datos[$i]['referencia']=$campo[4]; 
		else
		 	$datos[$i]['referencia']=""; 
		if(isset($campo[5]))
		 	$datos[$i]['origen']=$campo[5]; 
		else
		 	$datos[$i]['origen']="0";
		
	}
}
//print_r($datos);
echo json_encode($datos);
function validacuenta($cuenta)
{
	global $username_db, $password_db,$odbc_name,$server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$consulta="select nombre from contabmcuentas where cuenta='$cuenta' and ultimonivel=1";
	$stmt2 = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$nomcuenta=utf8_encode($row['nombre']);				
	}
	if(isset($nomcuenta))
		return($nomcuenta);
	else
		return('****** Error en Cuenta ******'); 
}


function validaPoliza($cuenta)
{
	global $username_db, $password_db,$odbc_name,$server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$consulta="select nombre from contabmcuentas where cuenta='$cuenta' and ultimonivel=1";
	$stmt2 = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$nomcuenta=utf8_encode($row['nombre']);				
	}
	if(isset($nomcuenta))
		return($nomcuenta);
	else
		return('****** Error en Cuenta ******'); 
}

	
?>
