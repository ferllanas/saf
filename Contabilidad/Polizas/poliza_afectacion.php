<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Afectaci&oacute;n de Poliza</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script src="javascript/polizas_funciones.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//	alert(data);
						$.post('poliza_Aafectar_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			
			function busca_cheque()
			{
        		// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
				var opcion = document.getElementById('opc').value;
				if(opcion>0)
				{
					if(opcion==1)
					{
						var data = 'query=' + opcion + document.getElementById('query').value;     // Toma el valor de los datos, que viene del input	
						
						$.post('poliza_Aafectar_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
						$('#proveedor').empty();
						$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
								 // los va colocando en la forma de la tabla
						}, 'json');  // Json es una muy buena opcion      
					}
					else
					{
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  						
						var fecinisplit= fecini.split("/");
						var fecfinsplit= fecfin.split("/");
						//alert(fecfinsplit[1]+"=="+fecinisplit[1]);
						if(fecfinsplit[1]==fecinisplit[1] && fecfinsplit[2]==fecinisplit[2] )
						{
							var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
							//alert(data);
		
							$.post('poliza_Aafectar_busqueda_ajax.php',data, function(resp)
							{ //Llamamos el arch ajax para que nos pase los datos
							
							//alert(resp.response);
							$('#proveedor').empty();
							$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
									 // los va colocando en la forma de la tabla
							}, 'json');  // Json es una muy buena opcion      
							
						}
						else
						{
							alert("Debe seleccionar polizas que sea del mismo mes y año.");
						}
					}
				}
            }
			
			function cancela_poliza(vale, tipo)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					location.href="php_ajax/cancela_poliza.php?poliza="+vale+"&tipo="+tipo;
				}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if folio}}
					<td><input type="checkbox" id="afectacion[]" name="afectacion" value="${id}">
					</td>
					<td>${folio}</td>
					<td>${tipo}</td>
					<td>${falta}</td>
				<!--	<td>${importe}</td>-->
					<td>${concepto}</td> 
					{{if estatus==0}}
						<td><img src="../../imagenes/consultar.jpg" onClick="window.open('pdf_files/poliza_'+${folio}+'.pdf') "></td>
						<!--<td><img src="../../imagenes/eliminar.jpg" onClick="cancela_poliza(${folio},'${tipo}')"></td>-->
					{{/if}}
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Afectación de Pólizas</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Número de Póliza</option>
            <!--  <option value="2" >Tipo de Póliza</option>
              <option value="4" >Descripción</option>              -->
              <option value="5" >Por rango de fechas</option>
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
  
  
  
   

  
  
  
  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
</td>
</tr>
</table>
<form >
<table>
<!--<tr>
	<td  align="center">
	<input type="button" value="Afectar" onclick="afectar_polizas()">
    </td>
</tr>-->
<tr>
	<td>
        <table>
                        <thead>
                        <th><p>Afectar</p></th>    
              <th><p>Número de Póliza</p></th>                                       
                            <!--   <th>Proveedor</th>-->
                         <th>Tipo</th> 
        <th>Fecha de póliza</th>
                            <th>Descripción</th>
                        </thead>
                        <tbody id="proveedor" name='proveedor'>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2">Encontrar Resultados</td>
                            </tr>
                        </tbody>
          </table>    
      </td>
</tr>
<tr>
	<td align="center">
      <input type="button" value="Afectar" onClick="afectar_polizas()">
    </td>
</tr>
</table>
</form>

</table>
    </div>
    </body>
</html>
