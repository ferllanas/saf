<!DOCTYPE html PUBLIC "-//W3C//Dtd class="texto8" XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd class="texto8"/xhtml1-transitional.dtd class="texto8"">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<title>Alta de poliza</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript/polizas_funciones.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<script type="text/javascript" src="ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="ajaximage/scripts/jquery.form.js"></script>

<script type="text/javascript" >
$(document).ready(function() { 
						   
			$('#gralguarda').prop('disabled',true);
		    $('#photoimg1').live('change', function()
			{ 
				$("#imageform1").ajaxForm({
					target: '#preview1'
					}).submit();
				});
			});
</script>

<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
</head>

<body>
<!--<form id="form1" name="form1" method="post" action="">-->
  <table width="100%" border="0">
  	<tr>
    	<td class="TituloDForma">Alta de P&oacute;liza
   	      <hr class="hrTitForma"></td>
    </tr>
    <tr>
      <td class="texto8"><table width="100%" border="0">
        <tr>
          <td class="texto8">
              <table width="100%" border="0">
               <tr>
          <td class="texto8" width="7%"><label>Fecha:</label></td>
          <td width="22%">
            <input class="texto8" name="fecpoliza" type="text" id="fecpoliza" maxlength="10" style="width:70px" />
             <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecpoliza",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
           </td>
          <td class="texto8" width="22%">Tipo poliza:
          <input class="texto8" name="tpoliza" type="text" id="tpoliza" size="10" maxlength="1" style="width:15px" onblur=" this.value=this.value.toUpperCase();
          if(this.value!='I' && this.value!='D' && this.value!='E'){ alert('El tipo de poliza solo debe ser I , D � E. '); this.value=''; return false;}" /></td>
          <td class="texto8" width="49%" colspan="2"><label>N&uacute;mero de Poliza:</label>
            <input class="texto8" type="text" name="numpoliza" id="numpoliza" title="N�mero de Poliza" maxlength="14" onkeypress="return aceptarSoloNumeros(this,event);" /></td>
        </tr>
         <tr>
                  <td class="texto8"><label>Concepto:</label></td >
                  <td class="texto8" colspan="3"><textarea name="descrip" id="descrip" style="width:80%"></textarea></td ></tr>
                  <tr>
                  <td class="texto8"><label>Archivo(*.txt):</label></td >
                  <td class="texto8" valign="bottom" align="left" colspan="3">
                  		<form id="imageform1" method="post" enctype="multipart/form-data" action='ajaximage/ajaximage.php'>
                               <input type="file" name="photoimg1" id="photoimg1" />
                       </form><input class="texto8" type="button" value="agregar" onclick="cargarpoliza()" />
                       <div id='preview1'>
						</div>
                        </td>
                </tr>
              </table>
			</td class="texto8">
        </tr>
        <tr>
          <td class="texto8" width="100%">
          <table class="tableone" id="sortable" border="0" style="width:800px">
          <thead>
            <tr>
              	<th class="th1" style="width:70px">Cuenta</th>
              	<th class="th2" style="width:150px">Descripcion</th>
              	<th class="th2" style="width:70px">Cargo</th>
              	<th class="th3" style="width:70px">Credito</th>
             	<th class="th4" style="width:80px">Referencia</th>
				<th class="th4" style="width:100px">Concepto</th>
                <th class="th4" style="width:50px">Origen</th> 				                                            
            </tr>
          </thead>
           <tbody >
                    <tr>
                        <td colspan="7">
                            <div class="innerb" style="width:800px;height:20em;">
                                <table class="tabletwo"  style="width:800px;">
                                    <tbody  name="tfacturas" id="tfacturas" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
          </table></td class="texto8">
        </tr>
      </table></td class="texto8">
    </tr> 
 <tr>	
    	<td class="texto8" valign="middle" align="center"><strong><label>Total de Cargos: $</label><input name="totcargo" id="totcargo"  style="width:100px" readonly="readonly" value='0'/><label>Total de Creditos: $</label><input name="totcredito" id="totcredito"  style="width:100px" readonly="readonly" value='0' /></strong></td>
    </tr>
    <tr>
    	<td align="center">
        	<input type="button" onclick="guardarMPoliza();" id="gralguarda" name="gralguarda" value="Guardar P�liza" class="texto8"/>
        </td>
    </tr>
  </table>
<!--</form>-->
</body>
</html>