<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();

$nom1='';
$nom2='';

$opcion= substr($_REQUEST['query'],0,1);
$fecini=substr($_REQUEST['query'],1,11);
$fecfin=substr($_REQUEST['query'],11,11);
$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
$tsql_callSP ="{call sp_contab_poliza_diario_paso1(?,?,?)}";//Arma el procedimeinto almacenado
//$tsql_callSP ="{call sp_pruebas2(?,?,?)}";//Arma el procedimeinto almacenado
$params = array(&$opcion,&$fini,&$ffin);//Arma parametros de entrada
$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
if($stmt===FALSE)
{
	  die( print_r( sqlsrv_errors(), true));
}
else
{
	//
	//echo $tsql_callSP;
	//print_r($params);
	//print_r($stmt);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
	{
		//print_r($row);
		// Comienza a realizar el arreglo, trim elimina espacios en blanco	
		//$datos[$i]['id']=trim($row['id']);
		$datos[$i]['origen']=trim($row['origen']);
		$datos[$i]['referencia']=trim($row['referencia']);
		$datos[$i]['cuenta']=trim($row['cuenta']);					
		$datos[$i]['nombre']=trim($row['nombre']);
		$datos[$i]['concepto']=utf8_decode($row['concepto']);
		$datos[$i]['cargo']=trim($row['cargo']);
		$datos[$i]['credito']=trim($row['credito']);
		$datos[$i]['id_registro']=$row['id'];
		
		$i++;
	}
}
//print_r($row);
echo json_encode($datos);   // Los codifica con el jason
?>