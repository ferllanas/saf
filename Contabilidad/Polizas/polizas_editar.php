<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$datos= array();
$id="";
$fecpoliza="";
$tpoliza="";
$numpoliza="";
$descrip="";

if(isset($_REQUEST['id']))
	$id=$_REQUEST['id'];
	
$command="SELECT convert(varchar(10),fecha,103) as fecpoliza, tipo, poliza, descrip  FROM contabmpoliza  WHERE id=$id ";

$stmt2 = sqlsrv_query( $conexion,$command);
if($stmt2)
{
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco	
		$fecpoliza=trim($row['fecpoliza']);
		$tpoliza=trim($row['tipo']);
		$numpoliza=trim($row['poliza']);
		$descrip=trim($row['descrip']);
	}
}

$i=0;
$command="  SELECT a.id, a.tipo, a.poliza, a.cuenta, a.concepto, a.referencia, a.cargo, a.referencia, a.cargo, a.credito, c.nombre as nomcuenta 
  FROM contabdpoliza a 
  INNER JOIN contabmpoliza b ON a.poliza=b.poliza 
  INNER JOIN contabmcuentas c ON a.cuenta=c.cuenta 
  WHERE b.id=$id and a.estatus<9000 ORDER BY a.mov  ";
//echo $command;
$stmt2 = sqlsrv_query( $conexion,$command);
if($stmt2)
{
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco	
		$datos[$i]['id']=trim($row['id']);
		$datos[$i]['tipo']=trim($row['tipo']);
		$datos[$i]['poliza']=trim($row['poliza']);					
		$datos[$i]['cuenta']=trim($row['cuenta']);
		$datos[$i]['concepto']=trim($row['concepto']);
		$datos[$i]['referencia']=utf8_decode($row['referencia']);
		$datos[$i]['cargo']=trim($row['cargo']);
		$datos[$i]['credito']=trim($row['credito']);
		$datos[$i]['nomcuenta']=trim($row['nomcuenta']);
		
		$i++;
	}
}

$datos
?>
<!DOCTYPE html PUBLIC "-//W3C//Dtd class="texto8" XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd class="texto8"/xhtml1-transitional.dtd class="texto8"">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin1" />
<title>Alta de poliza</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../javascript_globalfunc/jQuery.js"></script>
<script type="text/javascript" src="javascript/polizas_funciones.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
</head>

<body>
<form id="form1" name="form1" method="post" action="php_ajax/edita_guardaPoliza.php">
  <table width="100%" border="0">
  	<tr>
    	<td class="TituloDForma">Edici&oacute;n de P&oacute;liza
    	  <hr class="hrTitForma"><input type="hidden" name="idpoliza" id="idpoliza" value="<?php echo $id;?>" /></td class="texto8">
    </tr>
    <tr>
      <td class="texto8"><table width="100%" border="0">
        <tr>
          <td class="texto8">
              <table width="100%" border="0">
               <tr>
          <td class="texto8" width="6%"><label>Fecha:</label></td class="texto8">
          <td>
            <input class="texto8" name="fecpoliza" type="text" id="fecpoliza" maxlength="10" style="width:70px" value="<?php echo $fecpoliza;?>"/>
             <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecpoliza",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
           </td>
          <td class="texto8" width="8%">Tipo p&oacute;liza:</td>
          <td>  <input class="texto8" name="tpoliza" type="text" id="tpoliza" size="10" maxlength="1" style="width:15px" onblur=" this.value=this.value.toUpperCase();
          if(this.value!='I' && this.value!='D' && this.value!='E') alert('El tipo de poliza solo debe ser I , D � E.');" value="<?php echo $tpoliza;?> " readonly="readonly" /></td class="texto8">
          <td class="texto8" width="9%"><label>N&uacute;mero de Poliza:</label></td>
          <td>  <input class="texto8" type="text" name="numpoliza" id="numpoliza" title="N�mero de Poliza" maxlength="14" value="<?php echo $numpoliza;?> " readonly="readonly" /></td class="texto8">
        </tr>
        <tr>
              <td class="texto8"><label>Descripci&oacute;n:</label></td class="texto8">
              <td class="texto8" colspan="4"><textarea name="descrip" id="descrip" style="width:100%" value="" class="texto8"><?php echo $descrip;?></textarea></td class="texto8">
        </tr>
                <tr>
                  <td width="6%" height="25" class="texto8"><label>No. Cuenta:</label></td class="texto8">
                  <td class="texto8" width="14%"><input class="texto8" name="numcuenta" type="text" id="numcuenta" style="width:50px" maxlength="9"  onKeyPress="return aceptarSoloNumeros(this,event);" /></td class="texto8">
                  <td class="texto8" width="8%"><label> No. Subcuenta:</label></td class="texto8">
                  <td class="texto8" width="14%"><input class="texto8" type="text" name="numsubcuenta" id="numsubcuenta" style="width:50px" maxlength="9" onblur="getNomCuenta()" onKeyPress="return aceptarSoloNumeros(this,event);"  /><input type="hidden" name="numcuentacompleta" id="numcuentacompleta" value="" /></td class="texto8">
                  <td class="texto8" width="9%"><label>Nombre cuenta:</label></td class="texto8">
                  <td class="texto8" width="49%">  <input class="texto8" type="text" name="nomsubcuenta" id="nomsubcuenta" width="250px" maxlength="14" size="14" style="width:250px" /></td class="texto8">
                  <td class="texto8" width="0%">&nbsp;</td class="texto8">
                </tr>
                <tr>
                  <td class="texto8"><label>Cargo:</label></td class="texto8">
                  <td class="texto8"><input class="texto8" type="text" name="Cargo" id="Cargo" style="width:120px" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" /></td class="texto8">
                  <td class="texto8"><label>Credito:</label></td class="texto8">
                  <td class="texto8"><input class="texto8" type="text" name="Credito" id="Credito"  style="width:120px" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/></td class="texto8">
                  <td class="texto8"><label>Referencia:</label></td class="texto8">
                  <td class="texto8"><input class="texto8" type="text" name="Referencia" id="Referencia" /></td class="texto8">
                </tr>
                <tr>
                  <td class="texto8"><label>Concepto:</label></td class="texto8">
                  <td class="texto8" colspan="4"><textarea name="Concepto" id="Concepto" style="width:100%"></textarea></td class="texto8">
                  <td class="texto8" width="49%" valign="bottom" align="center"><input class="texto8" type="button" value="agregar" onclick="agregarCargoCredito_edicion()" /></td class="texto8">
                </tr>
              </table>
			</td class="texto8">
        </tr>
        <tr>
          <td class="texto8" width="100%">
          <table class="tableone" id="sortable" border="0" style="width:850px">
          <thead>
            <tr>
              <th class="th1" style="width:100px">Cuenta</th>
              <th class="th2" style="width:200px">Nombre de Cuenta</th>
              <th class="th3" style="width:100px">Cargo</th>
              <th class="th4" style="width:100px">Credito</th>
              <th class="th5" style="width:200px">Concepto</th>
               <th class="th5" style="width:100px">Referencia</th>
              <th class="th6" style="width:50px">&nbsp;</th>
            </tr>
          </thead>
           <tbody >
                    <tr>
                        <td colspan="6">
                            <div class="innerb" style="width:850px;height:20em;">
                                <table class="tabletwo"  style="width:850px;">
                                    <tbody  name="tfacturas" id="tfacturas" >
                                       <?php
									   		for($i=0;$i<count($datos);$i++)
											{
		
												?>
                                                <tr>
												 <td class="td1" style="width:100px">
                                                 		<input type="hidden" name="idpartida[]" id="idpartida[]" value="<?php echo $datos[$i]['id'];?>"/>
                                                 		<input type="text" id="dcuenta[]" name="dcuenta[]" class="texto8" value="<?php echo $datos[$i]['cuenta'];?>"   style="width:100%;" onKeyPress="return aceptarSoloNumeros(this,event);" onblur="getNomCuenta2(this, <?php echo $i;?>);"/>
                                                  </td>
                                                  <td class="td2" style="width:200px">
                                                  		<input type="text" id="dnomcuenta[]" name="dnomcuenta[]" class="texto8" value="<?php echo $datos[$i]['nomcuenta'];?>"  style="width:100%;"/>
                                                  </td>
                                                  <td class="td3" style="width:100px">
                                                  		<input type="text" id="dcargo[]" name="dcargo[]" class="texto8" value="<?php echo number_format($datos[$i]['cargo'],2);?>"  style="width:100%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
                                                  </td>
                                                  <td class="td4" style="width:100px">
                                                  		<input type="text" id="dcredito[]" name="dcredito[]" class="texto8" value="<?php echo number_format($datos[$i]['credito'],2);?>"  style="width:100%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
                                                  </td>
                                                  <td class="td5" style="width:200px">
                                                  		<textarea style="width:100%;" id="dconcepto[]" name="dconcepto[]" class="texto8" ><?php echo $datos[$i]['concepto'];?></textarea>
                                                  </td>
                                                   <td class="td5" style="width:100px">
                                                  		<input type="text" id="dreferencia[]" name="dreferencia[]" class="texto8" value="<?php echo $datos[$i]['referencia'];?>"  style="width:98%;" align="right" onKeyPress="return aceptarSoloNumeros(this,event);" />
                                                  </td>
                                                  <td class="td6" style="width:50px">
                                                  	<img src="../../imagenes/eliminar.jpg" onClick="cancela_partidapoliza(<?php echo $datos[$i]['id'];?>,<?php echo $i;?>)" title="Borrar partida.">
                                                    </td>
                                                </tr>
                                            	<?php
											}
									   ?>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
          </table></td class="texto8">
        </tr>
      </table></td class="texto8">
    </tr>
    <tr>
    	<td align="center">
        	<input type="submit" value="Guardar P&oacute;liza" class="texto8"/><!--type="button" onclick="guardarPoliza();"-->
        </td>
    </tr>
  </table>
</form>
</body>
</html>