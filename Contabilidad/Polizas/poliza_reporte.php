<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
			<link type="text/css" href="../../jquery-ui-1.9.2.custom/css/ui-lightness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" />   
			<script type="text/javascript" src="../../jquery-ui-1.9.2.custom/js/jquery-1.8.3.js"></script>
			<script type="text/javascript" src="../../jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<link rel="stylesheet" href="../../jquery-ui-1.9.2.custom/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../jquery-ui-1.9.2.custom/development-bundle/jquery-1.8.3.js"></script>
	<script src="../../jquery-ui-1.9.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../jquery-ui-1.9.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../jquery-ui-1.9.2.custom/development-bundle/ui/jquery.ui.progressbar.js"></script>
	<link rel="stylesheet" href="../../jquery-ui-1.9.2.custom/development-bundle/demos/demos.css">
	<style>
	.ui-progressbar .ui-progressbar-value { background-image: url(images/pbar-ani.gif); }
	</style>
	<script>
	$(function() {
		$( "#progressbar" ).progressbar({
			value: 59
		});
	});
	</script>

   <!--     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		
		var cpt = 0;
		var contador=0;
		$(document).ready(function() {   // Setup the ajax indicator  
								   $('body').append('<div id="ajaxBusy"><p><img src="../../imagenes/loading.gif"></p></div>'); 
								   $('#ajaxBusy').css({    display:"none", 
													  margin:"0px",    
													  paddingLeft:"0px",    
													  paddingRight:"0px",    
													  paddingTop:"0px",    
													  paddingBottom:"0px",   
													  position:"absolute",    
													  //right:"3px",   
													  left: "50%",
													  top:"50%",//"3px",    
													  width:"auto"  });
								   
					
						
			$("#progressbar").progressbar({	value: cpt });		   
		});
		
		
		
		
		
		// Ajax activity indicator bound to ajax start/stop document events
		$(document).ajaxStart(function(){ $('#ajaxBusy').show(); 
										 cpt=0;
										 $("#progressbar").progressbar({	value: cpt });	
										 $("#progressbar").show()
											})
					.ajaxStop(function(){ $('#ajaxBusy').hide();
										  $("#progressbar").hide('slow')
										});




		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
				$("#FormularioExportacion").submit();
		});
			
		});
         
		$('#query').live('keydown', function(e)
		{ // cuando se realiza el tecleo, recomendado metodo live
				if(e.which=='13')
				{
					var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
					var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
					//alert(data);
					$.post('poliza_reporte_ajax.php',data, function(resp)
					{ //Llamamos el arch ajax para que nos pase los datos
					
						
						//alert(resp.response);
						$('#proveedor').empty();
						contador=resp.length;
						console.log(contador);
						//alert(contador);
						//return false;
						//
						$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
									 // los va colocando en la forma de la tabla
					}, 'json');  // Json es una muy buena opcion
				}
		});
           
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);

						$.post('poliza_reporte_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			function cancela_poliza(vale, tipo)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					location.href="php_ajax/cancela_poliza.php?poliza="+vale+"&tipo="+tipo;
				}
			}
			
			
			var formatHelpers = { 
				embolden: function(i) 
				{
					cpt += 1;
					var value = cpt * 100 / contador;
					$("#progressbar").progressbar('value', value);
					return i;
				}
			};
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
				
            <tr>
			
                {{if folio}}
					<td align="center">${formatHelpers.embolden(folio)}</td>
					<td align="center">${mov}</td>
					<td align="left">${concepto}</td>
					<td align="center">${tipo}</td>
					<td align="center">${docorigen}</td>
					<td align="left">${referencia}</td>
					<td align="left">${usuario}</td>
					<td align="center">${falta}</td>
					<td align="left">${cuenta}</td>
					<td align="right">$ ${cargo}</td> 
					<td align="right">$ ${credito}</td> 
					{{if estatus<9000}}
						<td align="right">Activo</td> 
					  {{else}}	
					  	<td align="right">Inactivo</td> 
				    {{/if}}
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Reporte de Pólizas</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
  <div class="trigger"></div>
<div class="result"></div>
<div class="log"></div>
<div id="progressbar"></div>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Número de Póliza</option>
              <option value="2" >Tipo de Póliza</option>
              <option value="4" >Descripción</option>              
              <option value="5" >Por rango de fechas</option>
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
    <form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th width="60px">Número de Póliza</th>  
                <th width="10px">Movimiento</th>                                       
                <th width="250px">Concepto</th>
                <th width="20px">Tipo</th> 
                <th width="20px">Doc. Origen</th> 
                <th width="20px">Referencia</th>  
                <th width="100px">Usuario</th>
				<th width="60px">Fecha de Póliza</th>
                <th width="60px">Cuenta</th>
                <th width="70px">Cargo</th>
                <th width="70px">Credito</th>
                <th width="70px">Estado</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
