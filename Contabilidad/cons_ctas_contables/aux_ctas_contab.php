<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);


 $datos=array();	
 $anio = date("Y");
 $mes_act = (int)date("m");

 
 	if ($conexion)
	{
		$consulta = "select * from configmmeses order by mes";
		
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			//echo $consulta;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['mes']= trim($row['mes']);
				$datos[$i]['nombre']= trim($row['nombre']);
				$datos[$i]['corto']= trim($row['corto']);
				$i++;
			}
		}
	}

// echo $mes_act;
 //echo $datos[$i]['mes'];

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="javascript/busquedainc.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script>
		$(document).ready(function() 
		 {
			$("#botonPDF").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#master").eq(0).clone()).html());
				$("#FormularioExportacion").submit();
			});
			
			$("#botonExcel").click(function(event) {
				//alert("jijij");
				$("#datos_a_enviar_excell").val( $("<div>").append( $("#master").eq(0).clone()).html());
				$("#FormularioExportacion_excell").submit();
			});
		});
</script>
</head>

<body>
<p><span class="seccionNota"><strong>Auxiliar de Cuentas Contables </strong></span></p>
<hr class="hrTitForma">
<table width="98%" border="0" align="center">
  <tr>
    <td width="80" class="texto10">Cuenta</td>
    <td width="259">
		<div align="left" style="z-index:2; position:relative; width:600px; height: 24px;">
        	<input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" onkeyup="searchcuenta(this);" style="width:580px; height:20px; " autocomplete="off">
        	<div class="texto8" id="search_suggestcuenta" style="z-index:1;" > </div>
		</div>	
	
	</td>
    <td width="70" class="texto10" align="right">A&ntilde;o:</td>
    <td width="70" class="texto10"><input type="text" name="anio" id="anio" size="4px" value="<?php echo $anio;?>" ></td>
    <td width="70" class="texto10" align="right">Mes Inicial:</td>
    <td width="70" class="texto10">
		<select name="mesi" id="mesi">
			<?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
				  	echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
				}
			?>
		</select>
	</td>
    <td width="70" class="texto10" align="right">Mes Final:</td>
    <td width="73" class="texto10">
		<select name="mesf" id="mesf" onChange="busca_polizas()">
			<?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
					echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
				}
			?>
		</select>
	</td>
  </tr>
  <tr>
    <td class="texto10"><input type="hidden" name="cta" id="cta"></td>
    <td class="texto10"><input type="button" onClick="busca_polizas()" value="Buscar"></td>
    <td class="texto10"><input type="hidden" id="valsalini" value="0"></td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10"><input type="hidden" name="ren_excell" id="ren_excell">
    <form action="generarExcell.php" method="post" target="_blank" id="FormularioExportacion_excell">
    <img src="../../imagenes/export_to_excel.gif" id="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar_excell" name="datos_a_enviar_excell" />
</form></td>
    <td class="texto10">  <form action="generarPDF.php" method="post" target="_blank" id="FormularioExportacion"><img src="../../imagenes/Publish_PDF_32.png" id="botonPDF" width="20" /><input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form></td>
  </tr>
</table>
<table name="master" id="master" width="98%" height="10%" border=0 align="center" style="overflow:auto; " >
  <thead>
	<tr class="texto10">
    	<td align="right" colspan="2" id="sdoini">Saldo Inicial:</td>
   		<td align="right" id="cargos">Cargos:</td>
    	<td align="right" id="creditos">Creditos:</td>
    	<td align="right" id="sdofinal" colspan="2">Saldo Final:</td>
  </tr>
</thead>
  <thead style="font-family:Arial, Helvetica, sans-serif; color:#FFF; font-size:8pt; font-weight:bold; background:#006600; padding:1px 1px 1px 1px; text-align:center;">
  	<th width="70">FECHA</th>
    <th width="103">POLIZA</th>
    <th width="127">REFERENCIA</th>
    <th width="328" align="left">DESCRIPCION</th>
    <th width="97">CARGOS</th>
    <th width="107">CREDITOS</th>
  </thead>
  <tbody id="datos" name="datos">
  </tbody>

</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
