<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
		
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$mesi = $_POST['mesi'];
$mesf = $_POST['mesf'];
$cta = $_POST['cta'];
$anio = $_POST['anio'];

$datos=array();
 //echo "Se cayo";
if ($conexion)
{
	$consulta = "select c.sdo_inicial,CONVERT(varchar(12),a.fecha, 103) as fecha,a.tipo,a.poliza,b.cuenta,b.referencia,b.concepto,b.cargo,b.credito from contabmpoliza a  inner join contabdpoliza b on a.id=b.id_mpoliza  left join contabdsaldos_iniciales c on b.cuenta=c.cuenta and c.a�o=$anio where a.estatus=10 and b.estatus<9000 ";
	$consulta .= " and YEAR(a.fecha)=$anio and MONTH(a.fecha)>=$mesi and MONTH(a.fecha)<=$mesf and b.cuenta LIKE '%" . $cta . "%' AND a.estatus=10 ORDER BY a.fecha ASC
";
	
	//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die(print_r( $_POST, true).",".$consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['sdo_inicial']= trim($row['sdo_inicial']);
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['tipo']= trim($row['tipo'])."-".trim($row['poliza']);
			$datos[$i]['cuenta']= trim($row['cuenta']);
			$datos[$i]['referencia']= trim($row['referencia']);
			$datos[$i]['concepto']= utf8_decode(trim($row['concepto']));
			$datos[$i]['cargo']=  number_format(trim($row['cargo']),2);
			$datos[$i]['credito']=  number_format(trim($row['credito']),2);
			$i++;
		}
	}
}
//$datos['consulta']=$consulta;
echo json_encode($datos);
?>