<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
             
        <title>SAF- Estado de Situaci&oacute;n Financiera</title>
        <link href="../../cheques/css/style.css" 		rel="stylesheet" >         
		<!--<link href="../../css/estilos.css" 				rel="stylesheet" type="text/css" />  --> 
        <link href="../../css/esperaicon.css" 			rel="stylesheet" type="text/css" />     
        
		<!--<script src="../../javascript/navegacion.js"></script> -->
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
$(document).ready(function() {
	
	$(".botonPDF").click(function(event) {
		
		//console.log($("#Exportar_a_Excel").eq(0).clone());
		$("#datos_a_enviar_PDF").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacionPDF").submit();
	});
	
	$(".botonExcel").click(function(event) {
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
	});
	
	
	
	//
	$('#div_carga')
	.hide()
	.ajaxStart(function() {
		$(this).show();
	})
	.ajaxStop(function() {
		$(this).hide();
	});
});
           
			
function objectFindByKey(array, key, value) {
	for (var i = 0; i < array.length; i++) {
		if (array[i][key] === value) {
			return array[i];
		}
	}
	return null;
}
			
function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function busca_cheque()
{
	
		// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
			//var opcion = document.getElementById('opc').value;
			var mesVect=("0","Enero","Febrero", "Marzo", "Abril", "Mayo", "Junio" , "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
			/*var anio = document.getElementById('anio').value;
			var mes = document.getElementById('mes').value;
			var mes_X = document.getElementById('mes_X').value;
			*/
			var anio2 = document.getElementById('anio2').value;
			var mes2 = document.getElementById('mes2').value;
			var mes2_X = document.getElementById('mes2_X').value;
			
				
			/*$("#anioX").html(anio);
			$("#diainiX").html("31");
			$("#diainiX").html(mes);
			$("#mesX").html(mesVect[mes2]);
			*/
			
			//var cta = document.getElementById('numcuentacompleta').value;	  	
							
			var data2 = 'query=' + anio2 + mes2 +mes2_X;	
			
			/*$("#diainiX").html(anio2+ "/" + mes2 + " al "  + mes2_X + " - " +  anio + "/" + mes +" al "+ mes_X);
			
			$("#periodo1").html(anio2+"/"+mes2+" - "+mes2_X);
			$("#periodo2").html(anio+"/"+mes+" - "+mes_X);
			
			$("#periodo1_X").html(anio2+"/"+mes2+" - "+mes2_X);
			$("#periodo2_X").html(anio+"/"+mes+" - "+mes_X);*/
			
			$.post('balanzaGeneral_act_ajax.php',data2, function(resp)
			{ //Llamamos el arch ajax para que nos pase los datos
			
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				var objetoZ = objectFindByKey(resp, 'cuenta',  '111' );
				$('#EADA_F14').html(objetoZ.sdo_inicial);
				$('#EADA_G14').html(objetoZ.cargo);
				$('#EADA_H14').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I14').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J14').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '112' );
				$('#EADA_F15').html(objetoZ.sdo_inicial);
				$('#EADA_G15').html(objetoZ.cargo);
				$('#EADA_H15').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I15').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J15').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '1134' );
				$('#EADA_F16').html(objetoZ.sdo_inicial);
				$('#EADA_G16').html(objetoZ.cargo);
				$('#EADA_H16').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I16').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J16').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '114' );
				$('#EADA_F17').html(objetoZ.sdo_inicial);
				$('#EADA_G17').html(objetoZ.cargo);
				$('#EADA_H17').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I17').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J17').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				$('#EADA_F18').html( "0.00" );
				$('#EADA_G18').html( "0.00" );
				$('#EADA_H18').html( "0.00" );
				$('#EADA_I18').html( "0.00" );
				$('#EADA_J18').html( "0.00" );
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '116' );
				$('#EADA_F19').html(objetoZ.sdo_inicial);
				$('#EADA_G19').html(objetoZ.cargo);
				$('#EADA_H19').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I19').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J19').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '119' );
				$('#EADA_F20').html(objetoZ.sdo_inicial);
				$('#EADA_G20').html(objetoZ.cargo);
				$('#EADA_H20').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I20').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J20').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				$('#EADA_F23').html( "0.00" );
				$('#EADA_G23').html( "0.00" );
				$('#EADA_H23').html( "0.00" );
				$('#EADA_I23').html( "0.00" );
				$('#EADA_J23').html( "0.00" );
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '122' );
				$('#EADA_F24').html(objetoZ.sdo_inicial);
				$('#EADA_G24').html(objetoZ.cargo);
				$('#EADA_H24').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I24').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J24').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '123' );
				$('#EADA_F25').html(objetoZ.sdo_inicial);
				$('#EADA_G25').html(objetoZ.cargo);
				$('#EADA_H25').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I25').html( numberWithCommas( SdoFinal.toFixed(2) ) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J25').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '1241' );
				$('#EADA_F26').html(objetoZ.sdo_inicial);
				$('#EADA_G26').html(objetoZ.cargo);
				$('#EADA_H26').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I26').html( numberWithCommas( SdoFinal.toFixed(2)) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J26').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '1244' );
				$('#EADA_F29').html(objetoZ.sdo_inicial);
				$('#EADA_G29').html(objetoZ.cargo);
				$('#EADA_H29').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I29').html( numberWithCommas( SdoFinal.toFixed(2)) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J29').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '12469' );
				$('#EADA_F30').html(objetoZ.sdo_inicial);
				$('#EADA_G30').html(objetoZ.cargo);
				$('#EADA_H30').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I30').html( numberWithCommas( SdoFinal.toFixed(2)) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J30').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				
				/*objetoZ = objectFindByKey(resp, 'cuenta',  '12510' );
				$('#EADA_F31').html(objetoZ.sdo_inicial);
				$('#EADA_G31').html(objetoZ.cargo);
				$('#EADA_H31').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I31').html( numberWithCommas( SdoFinal) );
				$('#EADA_J31').html( numberWithCommas(parseFloat(SdoFinal) - parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ));
				*/
				
				$('#EADA_F31').html("0.00");
				$('#EADA_G31').html("0.00");
				$('#EADA_H31').html("0.00");
				$('#EADA_I31').html( "0.00" );
				$('#EADA_J31').html( "0.00");
				
				$('#EADA_F32').html("0.00");
				$('#EADA_G32').html("0.00");
				$('#EADA_H32').html("0.00");
				$('#EADA_I32').html( "0.00" );
				$('#EADA_J32').html( "0.00");
				
				
				objetoZ = objectFindByKey(resp, 'cuenta',  '126' );
				$('#EADA_F33').html(objetoZ.sdo_inicial);
				$('#EADA_G33').html(objetoZ.cargo);
				$('#EADA_H33').html(objetoZ.credito);
				var SdoFinal=parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) + parseFloat(objetoZ.cargo.replace(/,/g, ""));
				SdoFinal= SdoFinal-parseFloat(objetoZ.credito.replace(/,/g, ""))
				$('#EADA_I33').html( numberWithCommas( SdoFinal.toFixed(2)) );
				SdoFinal=SdoFinal-parseFloat(objetoZ.sdo_inicial.replace(/,/g, "")) ;
				$('#EADA_J33').html( numberWithCommas(SdoFinal.toFixed(2)));
				
				var EADA_F22= 	parseFloat($('#EADA_F33').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F32').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F31').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F30').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F29').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F26').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F25').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F24').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F23').html().replace(/,/g, ""));
				$('#EADA_F22').html( numberWithCommas(EADA_F22.toFixed(2)));
				
				var EADA_G22= 	parseFloat($('#EADA_G33').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G32').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G31').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G30').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G29').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G26').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G25').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G24').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G23').html().replace(/,/g, ""));
				$('#EADA_G22').html( numberWithCommas(EADA_G22.toFixed(2)));
				
				var EADA_H22= 	parseFloat($('#EADA_H33').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H32').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H31').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H30').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H29').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H26').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H25').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H24').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H23').html().replace(/,/g, ""));
				$('#EADA_H22').html( numberWithCommas(EADA_H22.toFixed(2)));
				
				var EADA_I22= 	parseFloat($('#EADA_I33').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I32').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I31').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I30').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I29').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I26').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I25').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I24').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I23').html().replace(/,/g, ""));
				$('#EADA_I22').html( numberWithCommas(EADA_I22.toFixed(2)));
				
				var EADA_J22= 	parseFloat($('#EADA_J33').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J32').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J31').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J30').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J29').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J26').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J25').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J24').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J23').html().replace(/,/g, ""));
				$('#EADA_J22').html( numberWithCommas(EADA_J22.toFixed(2)));
				/////////////////////////////////////////////////////////////////////
				var EADA_F13= 	parseFloat($('#EADA_F14').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F15').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F16').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F17').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F18').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F19').html().replace(/,/g, ""))+
								parseFloat($('#EADA_F20').html().replace(/,/g, ""));
				$('#EADA_F13').html( numberWithCommas(EADA_F13.toFixed(2)));
				
				var EADA_G13= 	parseFloat($('#EADA_G14').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G15').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G16').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G17').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G18').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G19').html().replace(/,/g, ""))+
								parseFloat($('#EADA_G20').html().replace(/,/g, ""));
				$('#EADA_G13').html( numberWithCommas(EADA_G13.toFixed(2)));
				
				var EADA_H13= 	parseFloat($('#EADA_H14').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H15').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H16').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H17').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H18').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H19').html().replace(/,/g, ""))+
								parseFloat($('#EADA_H20').html().replace(/,/g, ""));
				$('#EADA_H13').html( numberWithCommas(EADA_H13.toFixed(2)));
				
				var EADA_I13= 	parseFloat($('#EADA_I14').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I15').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I16').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I17').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I18').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I19').html().replace(/,/g, ""))+
								parseFloat($('#EADA_I20').html().replace(/,/g, ""));
				$('#EADA_I13').html( numberWithCommas(EADA_I13.toFixed(2)));
				
				var EADA_J13= 	parseFloat($('#EADA_J14').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J15').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J16').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J17').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J18').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J19').html().replace(/,/g, ""))+
								parseFloat($('#EADA_J20').html().replace(/,/g, ""));
				$('#EADA_J13').html( numberWithCommas(EADA_J13.toFixed(2)));
				//////////////////////////////////////////////////////////////////////////////
				
				
				var EADA_F11= EADA_F13	+EADA_F22;
				$('#EADA_F11').html( numberWithCommas(EADA_F11.toFixed(2)));
				
				var EADA_G11= EADA_G13	+EADA_G22;
				$('#EADA_G11').html( numberWithCommas(EADA_G11.toFixed(2)));
				
				var EADA_H11= EADA_H13	+EADA_H22;	
				$('#EADA_H11').html( numberWithCommas(EADA_H11.toFixed(2)));
				
				var EADA_I11= 	EADA_I13	+EADA_I22;
				$('#EADA_I11').html( numberWithCommas(EADA_I11.toFixed(2)));
				
				var EADA_J11= 	EADA_J13	+EADA_J22;
				$('#EADA_J11').html( numberWithCommas(EADA_J11.toFixed(2)));
			
				
		}, 'json');  // Json es una muy buena opcion               
}
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left" class="text" style='mso-number-format:"\@";'>${cuenta}</td>
					<td class="text" style='mso-number-format:"\@";'>${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
	   body {
			color: black;
		}
		
		.titulos{
			font-family:Arial;
			font-size:11px;
		}
		
		#tableContent td.verde{
			background-color:#2FDC48;
		}

			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
			
			.num {
			  mso-number-format:General;
			}
			.text{
			  mso-number-format:"\@";/*force text*/
			}
			
			
        </style>
</head>
    <body>
    <span class="TituloDForma">Estado Anal&iacute;tico del Activo</span>
    <hr class="hrTitForma">
<div align="center" ><!--id="main"-->
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>
<td><!-- Asignamos variables de fechas -->
	<table>
    	<tr>
        		<td>A&ntilde;o inicial:</td><td><input type="text"  name="anio2" id="anio2" size="4" maxlength="4" tabindex="1"> </td>
                <td>Mes inicial:</td><td><input type="text" name="mes2" id="mes2" size="2" maxlength="2" tabindex="2"></td>
                <td>Mes final:</td><td><input type="text" name="mes2_X" id="mes2_X" size="2" maxlength="2" tabindex="2"></td>
        </tr>
       <!-- <tr>
        		<td>A&ntilde;o final:</td><td><input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="3"> </td>
                <td>Mes inicial:</td><td><input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="4"></td>
                <td>Mes final:</td><td><input type="text" name="mes_X" id="mes_X" size="2" maxlength="2" tabindex="4"></td>
        </tr>-->
    </table>
 </td>
 <td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()" tabindex="5">
</td>
<td>
<input type="hidden" name="ren" id="ren">
    <form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
  <form action="poliza_pdf.php" method="post" target="_blank" id="FormularioExportacionPDF">
    <p>Exportar a PDF  <img src="../../imagenes/pdf.png" class="botonPDF" height="21" /></p>
    <input type="hidden" id="datos_a_enviar_PDF" name="datos_a_enviar_PDF" />
</form>

</td>
</tr>
</table>
<table id="Exportar_a_Excel" style="width:100%;" align="center">
	<tr><td  align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Cuenta P&uacute;blica</td></tr>
    <tr>
      <td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Estado Anal&iacute;tico del Activo</td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">del <span id="diainiX"></span> </td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">(Pesos)</td></tr>
    <tr><td align="center" style="width:100%; font-family:Arial; font-size:16; font-weight:bold;">Ente P&uacute;blico: FOMENTO METROPOLITANO DE MONTERREY </td></tr>
    <tr>
    	<td style="width:100%;">
          <table cellspacing="0" cellpadding="0" id="tableContent" style="border-color:#000000;width:90%;" align="center">
            <thead>
            	<tr>
                	<th width="35%">&nbsp;</th>
                    <th width="15%">&nbsp;</th>
                   	<th width="12.5%">&nbsp;</th>
                    <th width="12.5%">&nbsp;</th>
                    <th width="12.5%">&nbsp;</th>
                    <th width="12.5%">&nbsp;</th>
                </tr>
            </thead>
            <tr>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; border-left:#000000 solid 1px;" >CONCEPTO</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; " align="center">Saldo Inicial<br>1</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; " align="center">Cargos del Periodo<br>2</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; " align="center">Abonos del Periodo del Periodo<br>3</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; " align="center">Saldo final<br>
              4=(1+2-3)</td>
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; border-top:#000000 solid 1px; border-right:#000000 solid 1px;" align="center">Variacion del Periodo<br>(4-1)</td>
            </tr>
            <tr >
              <td style="background-color:#2FDC48; font-family:Arial; font-size:14; text-align:center;  border-right:#000000 solid 1px;"align="center"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; font-weight:600;  border-left:#000000 solid 1px;">ACTIVO</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F11" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G11"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H11"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I11"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;;" id="EADA_J11"></td>
            </tr>
            <tr>
              <td  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; border-left:#000000 solid 1px;">&nbsp;</td>
              <td></td>
              <td></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td  style=" font-family:Arial; font-size:12;font-weight:600; font-style:italic; border-left:#000000 solid 1px; ">Activo    Circulante</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F13" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G13"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H13"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I13"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;;" id="EADA_J13"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Efectivo    y Equivalentes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F14" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G14"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H14"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I14"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;;" id="EADA_J14"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;">Derechos    a Recibir Efectivo o Equivalentes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F15" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G15"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H15"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I15"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J15"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;">Derechos    a Recibir Bienes o Servicios</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F16" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G16"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H16"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I16"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J16"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;">Inventarios</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F17" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G17"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H17"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I17"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J17"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Almacenes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F18" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G18"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H18"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I18"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J18"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Estimaci&oacute;n    por P&eacute;rdida o Deterioro de Activos Circulantes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F19" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G19"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H19"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I19"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J19"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Otros    Activos Circulantes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F20" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G20"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H20"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I20"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J20"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" colspan="3" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px; font-style:italic; font-weight:bold;">Activo    No Circulante</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F22" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G22"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H22"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I22"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J22"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Inversiones    Financieras a Largo Plazo</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F23" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G23"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H23"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I23"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J23"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Derechos    a Recibir Efectivo o Equivalentes a Largo Plazo</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F24" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G24"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H24"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I24"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J24"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Bienes    Inmuebles, Infraestructura y Construcciones en Proceso</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F25" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G25"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H25"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I25"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J25"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Bienes    Muebles</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F26" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G26"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H26"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I26"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J26"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Activos    Intangibles</td>
             <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F29" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G29"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H29"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I29"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J29"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Depreciaci&oacute;n,    Deterioro y Amortizaci&oacute;n Acumulada de Bienes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F30" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G30"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H30"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I30"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J30"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Activos    Diferidos</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F31" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G31"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H31"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I31"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J31"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Estimaci&oacute;n    por P&eacute;rdida o Deterioro de Activos no Circulantes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F32" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G32"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H32"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I32"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J32"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >Otros    Activos no Circulantes</td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_F33" ></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_G33"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_H33"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; font-weight:600;" id="EADA_I33"></td>
              <td style="text-align:right; font-family:Arial; font-size:12; border-right:#000000 solid 1px;" id="EADA_J33"></td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" >&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-left:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-right:#000000 solid 1px;" colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px; border-left:#000000 solid 1px;" colspan="3" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px;">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px;" >&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; border-bottom:#000000 solid 1px; border-right:#000000 solid 1px;" >&nbsp;</td>
            </tr>
           <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="3">Bajo    protesta de decir verdad declaramos que los Estados Financieros y sus Notas    son razonalmente correctos y responsabilidad del emisor.</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
            </tr>
            
            <tr>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; text-align:center; border-top:#000000 solid 1px;" >C. Sergio Alejandro Alanis Marroqu&iacute;n</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center; border-top:#000000 solid 1px;" colspan="2">Lic. Eloisa Sanchez M&eacute;ndez</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12; text-align:center;">Director Ejecutivo</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center;" colspan="2">Encargada del Despacho de los Asuntos de la    Direccion de</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" colspan="2">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12;" align="right">&nbsp;</td>
              <td style=" font-family:Arial; font-size:12; text-align:center;" colspan="2">Administracion y Finanzas</td>
            </tr>
            <tr>
              <td style=" font-family:Arial; font-size:12;" align="right"></td>
              <td style=" font-family:Arial; font-size:12;" align="right"></td>
              <td style=" font-family:Arial; font-size:12;" align="right"></td>
              <td style=" font-family:Arial; font-size:12;" align="right"></td>
              <td style=" font-family:Arial; font-size:12;" align="right"></td>
              <td style=" font-family:Arial; font-size:12;" align="right"></td>
            </tr><!---->
        </table></td>
    </tr>
    </table>

    </div>
    <div id="div_carga">
   		<img src="../../imagenes/ajax-loader.gif" width="64" id="cargador">
    </div>
    </body>
</html>
