<?php
// Archivo de consultas de Solicitud de Cheques


$cuentasA = array("52410", "422", "417", "43121.1", "43121.2", "43121.3", "43121.4" , "43590.1", "43590.2", "43590.3", "43600.4",
				  "5111", "5112", "5113", "5114", "5115", "5117", "5121", "5122", "5124","5125", "5126", "5127","5129",
				  "5131","5132","5133", "5134", "5135", "5136", "5137", "5138", "5139",
				  "54", "551", "5531", "55690.1",  "111",  "112",  "1134",  "114",  "116",  "119", "122",  "123",  
				   "1241",  "1244",  "12469", "12510.1", "126", "21212.6", "213", "216",  "2199",  "2111",  "2112",
				    "2113",  "2117",  "21199", "212",  "222", "224",  "2263",  "2269",  "31100",  "31200",  "31300.2",
					 "32200",  "32100", "31300.6", "33100.1995" , "33100.1996", "33100.1997" , "33100.1998" , "33100.1999",
					 "33100.2000", "33100.2001", "33100.2002", "33100.2003", "33100.2004", "33100.2005", "33100.2006", "33100.2007",
					 "33100.9000", "541", "542", "556", "56", 
					 );


include_once '../../cheques/lib/ez_sql_core.php'; 
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$anio="";
$mes="";

$datos=array();
$anio= substr($_REQUEST['query'],0,4);
$mes= substr($_REQUEST['query'],4,2);
$mes_2= substr($_REQUEST['query'],6,2);
//$cta= substr($_REQUEST['query'],6);
//if(!isset($cta))
//	$cta="";
//$command="sp_contab_cuenta_publica '$anio','$mes'";
$command="sp_contab_ctapub_edoact_sitfin '$anio','$mes','$mes_2'";
$stmt = sqlsrv_query($conexion, $command);
$i=0;
if( $stmt )
{
	
	 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	 {
		if (in_array(trim($row['cuentaL']), $cuentasA)) 
		{
		
				$datos[$i]['cuenta'] = trim($row['cuentaL']); 
	    		$datos[$i]['nombre'] = utf8_encode(trim($row['nombre']));
				$datos[$i]['cargo']  =	number_format($row['cargos'],2);
				$datos[$i]['credito']= 	number_format($row['creditos'],2);
				$datos[$i]['sdo_inicial']= 	number_format($row['sdo_inicial'],2);
				$datos[$i]['sdo_final']= 	number_format($row['sdo_final'],2);
				$datos[$i]['carfin'] = 	number_format($row['carfin'],2);
				$datos[$i]['crefin'] = 	number_format($row['crefin'],2);			
			if($datos[$i]['cuenta']=="56"){
				$i++;
				$datos[$i]['cuenta']= "CuentaExtrana";
				$datos[$i]['nombre'] = "Pues que rollo";
				$datos[$i]['cargo']  =	"20.00";
				$datos[$i]['credito']= 	"35.00";
				$datos[$i]['sdo_inicial']= 	"69.00";
				$datos[$i]['sdo_final']= 	"96.00";
				$datos[$i]['carfin'] = 	"98.00";
				$datos[$i]['crefin'] = 	"78.00";
			}
			$i++;
		}	
	 }
}

$datos[$i]['cuenta']=""; 
$datos[$i]['nombre']= "";
$datos[$i]['cargo']="";
$datos[$i]['credito']="";
$datos[$i]['sdo_inicial']= "";
$datos[$i]['sdo_final']= 	"";
$datos[$i]['carfin']= 	"";
$datos[$i]['crefin']= 	"";
//$datos['anio']=$anio;
//$datos['mes']=$mes;
echo json_encode($datos);   // Los codifica con el jason
?>