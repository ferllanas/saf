<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
require_once("../../dompdf/dompdf_config.inc.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');


?><html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        <script src="javascript/navegacion.js"></script>
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone() ).html());
				$("#FormularioExportacion").submit();
		});
		});			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left">${cuenta}</td>
					<td align="left">${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Catalogo de Cuentas</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>


<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<!-- Asignamos variables de fechas -->
<!--<td>
  Año:<input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="1">    
  Mes:<input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="2"></td>
  <td>
  <div align="left" style="z-index:0; position:relative; width:450px">Nombre de la cuenta:
    <input tabindex="3"  class="texto8" name="nomsubcuenta" type="text" id="nomsubcuenta" size="50" style="width:250px;"  onkeyup="searchSuggest();" autocomplete="off"/>
    <input type="text" class="texto8" name="numcuentacompleta" id="numcuentacompleta" size="50" style="width:70px;" readonly="readonly" tabindex="4" />
    <div id="search_suggest" style="z-index:2; position:absolute;" > </div>
  </div><td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()" tabindex="5">
</td>--><td>
<input type="hidden" name="ren" id="ren">
    <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th width="60px">Cuenta</th>  
                <th width="250px">Nombre de la Cuenta</th> 
                <th width="40px">Padre</th>
                <th width="40px">Ultimo Nivel</th>                                        
                </thead>
                <tbody id="proveedor" name='proveedor'>
                <?php 
				$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
				$conexion = sqlsrv_connect($server,$infoconexion);
				if($conexion)
				{
				 	$consulta="SELECT * FROM v_contab_cat_ctas";
					$getProducts = sqlsrv_query($conexion,$consulta);
					if ( $getProducts === false)
					{ 
						$resoponsecode="02";
						$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
						//echo $descriptioncode;
					}
					else
					{
					
						$resoponsecode="Cantidad rows=".count($getProducts);
						//echo $resoponsecode;
						$i=0;
						while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
						{			
							?>
                            <tr>
                              <td><?php echo $row['cuenta'];?></td>
                              <td><?php echo utf8_decode(trim($row['NOMBRE']));?></td>
                              <td><?php echo trim($row['padre']);?></td>
                              <td><?php if((int)trim($row['ultimonivel'])==1) {?><!--<img class='ximage' src="../../imagenes/actualiza1.png" width="16px">-->SI<?php }?></td>
                            </tr>
                            <?php
							$i++;
						}
					}
				?>
                    
               	<?php
				}
				?>
                </tbody>
  </table>
    </div>
    </body>
</html>
