<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$anio="";
$mes="";

$datos=array();
$anio= substr($_REQUEST['query'],0,4);
$mes= substr($_REQUEST['query'],4,2);
$cta= substr($_REQUEST['query'],6);
if(!isset($cta))
	$cta="";

$command="sp_contab_c_tot_x_ctas_univel '$anio','$mes','$cta'";
$stmt = sqlsrv_query($conexion, $command);
if( $stmt )
{
	 $i=0;
	 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	 {
		// print_r($row);
		$datos[$i]['sinicia']= (float)$row['sdo_inicial'] + (float)$row['monto'];
        $datos[$i]['sfinal']=$datos[$i]['sinicia'] + (float)$row['cargo'] - (float)$row['credito'];
		$datos[$i]['cuenta']=$row['cuenta']; 
		$datos[$i]['nombre']=$row['nombre'];
		$datos[$i]['cargo']=$row['cargo'];
		$datos[$i]['credito']=$row['credito'];
		
		$datos[$i]['saldoinicia']= number_format( $datos[$i]['sinicia'] , 2);
		$datos[$i]['saldofinal']=  number_format( $datos[$i]['sfinal']  , 2);
		$i++;
	 }
}
echo json_encode($datos);   // Los codifica con el jason
?>