<?php

// Vamos a mostrar un PDF
header('Content-type: application/pdf');
// Se llamar� downloaded.pdf
header('Content-Disposition: attachment; filename="downloaded.pdf"');
header("Pragma: no-cache");
header("Expires: 0");

require_once("../../dompdf-master/dompdf_config.inc.php");
require_once("../../connections/dbconexion.php");
$table=$_POST['datos_a_enviar'];
$cuenta=$_POST['cuentapdf'];
$fecini=$_POST['fecfinpdf'];
$fecini=$_POST['fecinipdf'];

$fechaExploded=explode("/",$fecini);

$table=str_replace("color:#FFF","color:black", $table);
$table=str_replace("background:#006600","background:gren", $table);
$table=str_replace('class="fila"','class="filaPDF"', $table);

$datetime = date('d/m/Y h:i:s a', time());

$dateExp= explode(" " ,$datetime);
$date= $dateExp[0];
$time= $dateExp[1];

$anio=$fechaExploded[2];
$dompdf = new DOMPDF();



$HTML='<html>
<head>
<title>Libro Mayor</title>
<link href="../../css/estilosPDF.css" rel="stylesheet" type="text/css">
</head>
<body width="90%">

<table width="90%" align="center" class="cabezales">
	<tr>
		<td align="center"><img src="../../'.$imagenPDFPrincipal.'" width="80px">
		</td>
		<td align="center">
			FOMENTO METROPOLITANO DE MONTERREY<br>
			LIBRO MAYOR<br>
			'.$cuenta.'<br>
			EJERCICIO DEL '.$anio.'<br>
			CIFRAS EN PES0S Y CENTAVOS)
		</td>
		<td align="center">
			HORA	'.$time.'<br>
			FECHA	'.$date.'<br>
		</td>
	</tr>
</table>
'.$table.'
<script type="text/php">
if ( isset($pdf) ) { 
    $font = Font_Metrics::get_font("Arial", "normal");
    $size = 9;
    $y = $pdf->get_height() - 24;
    $x = $pdf->get_width() - 15 - Font_Metrics::get_text_width("1/1", $font, $size);
    $pdf->page_text($x, $y, "{PAGE_NUM}/{PAGE_COUNT}", $font, $size);
}   
</script> 
</body>
</html>';

//$dompdf->set_paper('legal', "landscape");
$dompdf->load_html($HTML);

$dompdf->render();
	  
$pdf = $dompdf->output(); 

$dompdf->stream("downloaded.pdf");

?>