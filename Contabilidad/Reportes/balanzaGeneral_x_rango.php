<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        <script src="javascript/navegacion.js"></script>
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
		$(document).ready(function() {
			$(".botonExcel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
				$("#FormularioExportacion").submit();
		});
		});
            $(function()
			{
                $('#query').live('keydown', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var anio = document.getElementById('anio').value;
						var mes = document.getElementById('mes').value;
						var fin = document.getElementById('mesfin').value;
						//var cta = document.getElementById('numcuentacompleta').value;	  	
						if(mes.length==1)
						{
							mes="0" + mes;
						}						
						var data = 'anio=' + anio +'&mes='+ mes +"&fin="+ fin ;    // Toma el valor de los datos, que viene del input						
						alert(data);
						$.post('poliza_reporte_ajax_x_rango.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						//var opcion = document.getElementById('opc').value;
						var anio = document.getElementById('anio').value;
						var mes = document.getElementById('mes').value;
						var fin = document.getElementById('mesfin').value;
						//var cta = document.getElementById('numcuentacompleta').value;	  	
						if(mes.length==1)
						{
							mes="0" + mes;
						}						
						var data = 'anio=' + anio +'&mes='+ mes +"&fin="+ fin ;     // Toma el valor de los datos, que viene del input						
						//alert(data);
						
						$.post('balanzaGeneral_ajax_x_rango.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							console.log(resp),
							//alert(resp.response);
                      	  	$('#proveedor').empty();
							if(resp.length<=0)
							{
								alert("No existen resultados para estos criterios.");
								document.getElementById('anio').value="";
								document.getElementById('mes').value="";
								//document.getElementById('numcuentacompleta').value="";
								//document.getElementById('nomsubcuenta').value="";
							}
                        	$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
							
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if cuenta}}
					<td align="left">${cuenta}</td>
					<td >${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Balanza General Por Rango</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table >   
<tr>


<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->

<td><!-- Asignamos variables de fechas -->
  Año:<input type="text"  name="anio" id="anio" size="4" maxlength="4" tabindex="1">    
  Mes Inicio:<input type="text" name="mes" id="mes" size="2" maxlength="2" tabindex="2">
  Mes Final:<input type="text" name="mesfin" id="mesfin" size="2" maxlength="2" tabindex="2"></td>
 <td>
<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()" tabindex="5">
</td>
<td>
<input type="hidden" name="ren" id="ren">
    <form action="poliza_excel.php" method="post" target="_blank" id="FormularioExportacion">
    <p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>
</td>
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
      			<th width="60px">Cuenta</th>  
                <th width="250px">Nombre de la Cuenta</th> 
                <th width="40px">Cargo inicial</th>
                <th width="40px">Credito inicial</th>                                        
               
                <th width="40px">Cargos</th>
                <th width="40px">Creditos</th>   
                <th width="40px">Cargo Final</th>
                <th width="40px">Credito Final</th>    
                <!--<th width="40px">Saldo Inicial</th>-->
                <!-- <th width="40px">Saldo final</th>            -->
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
