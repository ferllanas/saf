<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);


 $datos=array();	
 $anio = date("Y");
 $mes_act = (int)date("m");

 
 	if ($conexion)
	{
		$consulta = "select * from configmmeses order by mes";
		
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			//echo $consulta;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['mes']= trim($row['mes']);
				$datos[$i]['nombre']= trim($row['nombre']);
				$datos[$i]['corto']= trim($row['corto']);
				$i++;
			}
		}
	}

// echo $mes_act;
 //echo $datos[$i]['mes'];

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../cheques/javascript/jquery.tmpl.js"></script>
<script language="javascript" src="javascript/busquedainc_LibroMayor.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<script>
		$(document).ready(function() 
		 {
			$("#botonPDF").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#master").eq(0).clone()).html());
				
				$("#cuentapdf").val( $("#cuenta").val()   	);
				$("#fecinipdf").val( $("#mesi").val()		);
				$("#fecfinpdf").val( $("#mesf").val()		);
				$("#FormularioExportacion").submit();
			});
			
			$("#botonExcel").click(function(event) {
				//alert("jijij");
				$("#datos_a_enviar_excell").val( $("<div>").append( $("#master").eq(0).clone()).html());
				$("#FormularioExportacion_excell").submit();
			});
		});
		
		function muestra_movimientosCuentas()
		{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						//var opcion = document.getElementById('opc').value;
						//var anio = document.getElementById('anio').value;
						var cta = document.getElementById('cuenta').value;
						if(cta.length<=0){
							alert("Favor de seleccionar una cuenta");
							return false;
						}
						var mesi = document.getElementById('mesi').value;
						if(mesi.length<=0){
							alert("Favor de seleccionar una fecha de inicio");
							return false;
						}
						
						var mesf = document.getElementById('mesf').value;
						if(mesf.length<=0){
							alert("Favor de seleccionar una fecha de fin");
							return false;
						}
						
						mesisplit = mesi.split("/");
						mesfsplit = mesf.split("/");
						
						if(mesisplit[2]!=mesfsplit[2])
						{
							alert("Favor de seleccionar fechas en el mismo a�o");
							return false;
						}
						//alert(mesi);
						var data = 'mesi=' + mesi +"&mesf=" +mesf + "&cta=" + cta;     // Toma el valor de los datos, que viene del input						
						console.log(data)
						
						$.post('php_ajax/buscar_LibroMayor.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							console.log(resp);
                      	  	$('#proveedor').empty();
							if(resp.length<=0)
							{
								alert("No existen resultados para estos criterios.");
								document.getElementById('anio').value="";
								document.getElementById('mes').value="";
								document.getElementById('numcuentacompleta').value="";
								document.getElementById('nomsubcuenta').value="";
							}
							console.log("antes de");
                        	$('#tmpl_proveedor').tmpl(resp.datos).appendTo('#proveedor'); 
							
							$('#tmpl_proveedorsub').tmpl(resp.subtotales).appendTo('#proveedor'); 
							
							$('#tmpl_proveedortotales').tmpl(resp.totales).appendTo('#proveedor'); 
							console.log("Despues de");
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
</script>

        
<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
	<tr>
		{{if cuenta}}
			<td class="celBody" align="center">${falta}</td>
			<td class="celBody" align="center">${evento}</td>
			<td class="celBody" align="left">${concepto}</td>
			<td class="celBody" align="center">
				<table width="100%" class="tableincell">
					<tr>
						<td align="right" width="50%" height="30px" class="2CellsLeftCell">${cargo}</td>
						<td align="right" width="50%" height="30px" class="2CellsRightCell">${credito}</td>
					</tr>
				</table>
			</td>
			<td class="celBody" align="right">${saldo}</td>	
		{{else}}
			<td class="celBody" colspan="2">No existen resultados</td>
		{{/if}}
	</tr>
</script>   

<script id="tmpl_proveedorsub" type="text/x-jquery-tmpl">   
	<tr>
		{{if cuenta}}
			<td class="celBody" align="right" colspan="3">SUBTOTAL</td>
			<td class="celBody" align="center">
				<table width="100%" class="tableincell">
					<tr>
						<td align="right" width="50%" class="2CellsLeftCell">${debe}</td>
						<td align="right" width="50%" class="2CellsRightCell">${haber}</td>
					</tr>
				</table>
			</td>
			<td class="celBody" align="center">&nbsp;</td>	
		{{else}}
			<td class="celBody" colspan="2">No existen resultados</td>
		{{/if}}
	</tr>
</script>

<script id="tmpl_proveedortotales" type="text/x-jquery-tmpl">   
	<tr>
		{{if cuenta}}
			<td class="celBody" align="right" colspan="3">TOTAL</td>
			<td class="celBody" align="center">
				<table width="100%" class="tableincell">
					<tr>
						<td align="right" width="50%" class="2CellsLeftCell">${debe}</td>
						<td align="right" width="50%" class="2CellsRightCell">&nbsp;</td>
					</tr>
				</table>
			</td>
			<td class="celBody" align="center"></td>	
		{{else}}
			<td class="celBody" colspan="2">No existen resultados</td>
		{{/if}}
	</tr>
</script>  
   
</head>

<body>
<p><span class="seccionNota"><strong>LIBRO MAYOR</strong></span></p>
<hr class="hrTitForma">
<table width="90%" border="0" align="center">
  <tr>
    <td width="80" class="texto10">Cuenta</td>
    <td width="259">
		<div align="left" style="z-index:2; position:relative; width:600px; height: 24px;">
        	<input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" onkeyup="searchcuenta(this);" style="width:580px; height:20px; " autocomplete="off">
        	<div class="texto8" id="search_suggestcuenta" style="z-index:1;" > </div>
		</div>	
	
	</td>
    <td width="70" class="texto10" align="right"><!--A&ntilde;o:--></td>
    <td width="70" class="texto10"><!--<input type="text" name="anio" id="anio" size="4px" value="<?php echo $anio;?>" >--></td>
    <td width="70" class="texto10" align="right">Fecha inicial:</td>
    <td width="70" class="texto10">  <input type="text"  name="mesi" id="mesi" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "mesi",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
		<!--<select name="mesi" id="mesi">
			<?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
				  	echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
				}
			?>
		</select>-->
	</td>
    <td width="70" class="texto10" align="right">Fecha Final:</td>
    <td width="73" class="texto10"><input type="text" name="mesf" id="mesf" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "mesf",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
		<!--<select name="mesf" id="mesf" onChange="busca_polizas_niv()">
			<?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
					echo "<option value=".$datos[$i]["mes"].">".$datos[$i]["nombre"]."</option>\n";
				}
			?>
		</select>-->
	</td>
  </tr>
  <tr>
    <td class="texto10"><input type="hidden" name="cta" id="cta"></td>
    <td class="texto10"><input type="button" onClick="muestra_movimientosCuentas()" value="Buscar"></td>
    <td class="texto10"><input type="hidden" id="valsalini" value="0"></td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10">&nbsp;</td>
    <td class="texto10"><input type="hidden" name="ren_excell" id="ren_excell">
    <form action="generarExcell.php" method="post" target="_blank" id="FormularioExportacion_excell">
    <img src="../../imagenes/export_to_excel.gif" id="botonExcel" /></p>
    <input type="hidden" id="datos_a_enviar_excell" name="datos_a_enviar_excell" />
    
</form></td>
    <td class="texto10"><form action="generarPDF.php" method="post" target="_blank" id="FormularioExportacion"><img src="../../imagenes/Publish_PDF_32.png" id="botonPDF" width="20" />
   <input type="hidden" id="cuentapdf" name="cuentapdf" value="">
    <input type="hidden" id="fecfinpdf" name="fecfinpdf" value="" >
    <input type="hidden" id="fecinipdf" name="fecinipdf" value="">
    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form></td>
  </tr>
</table>
<table name="master" id="master" width="90%" height="10%" border=0 align="center" style="overflow:auto;" class="tableCuerpo" >
	<thead style="font-family:Arial, Helvetica, sans-serif; color:#FFF; font-size:8pt; font-weight:bold; background:#006600; padding:1px 1px 1px 1px; text-align:center;">
  		<tr>
        	<th>FECHA</th>
    		<th>No. DE EVENTO</th>
    		<th>DESCRIPCION</th>
    		<th>
            	<table width="100%">
                	<tr>
                    	<td colspan="2" align="center">MONTO</td>
                    </tr>
                    <tr>
                    	<td align="center" width="50%">DEBE</td>
                        <td align="center" width="50%">HABER</td>
                    </tr>
                </table>
            </th>
    		<th align="center">SALDO</th>
        </tr>
	</thead>
	<tbody id="proveedor" name="proveedor">
  	</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
