<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
		
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$seprat=  explode("/",$_POST['mesi']);
$mesi= $seprat[1]."/".$seprat[0]."/".$seprat[2];

$seprat=  explode("/",$_POST['mesf']);
$mesf= $seprat[1]."/".$seprat[0]."/".$seprat[2];

$seprat=  explode(" ",$_POST['cta']);
$cta = $seprat[0];

//
$sumdebe=0.00;
$sumhaber=0.00;
$datos=array();
if ($conexion)
{
	$consulta = "SELECT convert(varchar(10),falta,103) as falta1,* FROM v_contab_libromayor a WHERE a.falta>='".$mesi."' and a.falta<='".$mesf."' and a.cuenta LIKE '%".$cta."%' ORDER BY a.id ASC";
	//echo json_encode($consulta);
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die(print_r( $_POST, true).",".$consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['falta']= trim($row['falta1']);
			$datos[$i]['evento']= trim($row['evento']);
			$datos[$i]['concepto']= trim($row['concepto']);
			$datos[$i]['cargo']= number_format(trim($row['cargo']),2);
			$datos[$i]['credito']= number_format(trim($row['credito']),2);
			
			$sumdebe += $row['cargo'];
			$sumhaber += $row['credito'];
			
			$datos[$i]['saldo']= number_format($row['cargo']-$row['credito'],2);
			$datos[$i]['cuenta']=  trim($row['cuenta']);
			$datos[$i]['id']=  trim($row['id']);
			$i++;
		}
	}
}

$results=array();
$subtotales=array();

$subtotales['debe']= number_format($sumdebe,2);
$subtotales['haber']=number_format($sumhaber,2);

$totales['debe']= number_format(($sumdebe-$sumhaber),2);

$results['datos']=$datos;
$results['subtotales']=$subtotales;
$results['totales']=$totales;
//$datos['consulta']=$consulta;
echo json_encode($results);
?>