function buscar_DRequi()
{
	var requisicion=document.getElementById('requi').value;
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					requi		: 		requisicion
				};
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/datos_requisicion.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								document.getElementById('nomsubcuenta').value=myArray[0].nombre;
							}
							else
							{
								alert( "No existe cuenta con este numero.");
							}
						}
						else
							alert( "Error no se pudo obtener la cuenta."+myArray[0].msgerror);
					}
				}
			});
}


// busuqeda incrementl *****
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
	//alert("error");
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('nomsubcuenta').value);
		searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
       if(searchReq.responseText.length<1);
		{
			document.getElementById('numcuentacompleta').value = '';	
			//document.getElementById('nomsubcuenta').value = '';
		}
	   ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+'; '+tok[0]+' ">' + tok[1] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    //document.getElementById('nomsubcuenta').value = idprod;
	var tok =idprod.split(";");	
	document.getElementById('numcuentacompleta').value = tok[0];	
	document.getElementById('nomsubcuenta').value = tok[1];
	//$('#Concepto').text(tok[1]);
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
	document.getElementById('Cargo').focus();
}

function addslashes (str)
{
   
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  

}
function aceptarSoloNumeros(evt)
{
//asignamos el valor de la tecla a keynum
	if(window.event)
	{// IE
		
		keynum = evt.keyCode;
	}
	else
	{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	alert(keynum);
	if(keynum>47 && keynum<58)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function Validames()
{
	
	var mes=document.getElementById('mes').value;
	switch(mes) 
	{ 
   	case '1': 
      	 document.getElementById('desmes').value="Enero"; 
		 document.getElementById('nomsubcuenta').focus();
      	 break 
   	case '2': 
      	 document.getElementById('desmes').value="Febrero";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
   	case '3': 
      	 document.getElementById('desmes').value="Marzo";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '4': 
      	 document.getElementById('desmes').value="Abril";
		 document.getElementById('nomsubcuenta').focus();
      	 break 		 
	case '5': 
      	 document.getElementById('desmes').value="Mayo";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '6': 
      	 document.getElementById('desmes').value="Junio";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '7': 
      	 document.getElementById('desmes').value="Julio";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '8': 
      	 document.getElementById('desmes').value="Agosto"; 
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '9': 
      	 document.getElementById('desmes').value="Septiembre";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '10': 
      	 document.getElementById('desmes').value="Octubre";
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '11': 
      	 document.getElementById('desmes').value="Noviembre";  
		 document.getElementById('nomsubcuenta').focus();
      	 break 
	case '12': 
      	 document.getElementById('desmes').value="Diciembre";
		 document.getElementById('nomsubcuenta').focus();
      	 break 		 
   	default: 
      	 alert("Mes Invalido");
		 document.getElementById('mes').value="";
		 document.getElementById('desmes').focus();
	} 	
}

function Limpiacampos()
{
	document.getElementById('nomsubcuenta').value="";	
	document.getElementById('numcuentacompleta').value="";
	document.getElementById('anio').value="";
	document.getElementById('mes').value="";
	document.getElementById('desmes').value="";
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
	for(var i = numren-1; i >=0; i--)
	{
		Table.deleteRow(i);
	}
	document.getElementById('imgexell').disabled=true;
	document.getElementById('anio').focus();
}
function BuscaMovimientos()
{
	
	var cta= document.getElementById('numcuentacompleta').value;
	var anio= document.getElementById('anio').value;
	var mes= document.getElementById('mes').value;
	if(anio.length <= 0 )
	{
		alert("Favor de ingresar el A�o.");
		return(false);
	}	
	
	if(mes.length <= 0)
	{
		alert("Favor de ingresar el Mes.");
		return(false);
	}	
	
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}
	
	var datas = { 	anio: anio,
					mes:mes,
					cta: cta};
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'php_ajax/buscamovs.php',
						dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(request);
						 alert ( " Can't do because: " + error );
						},
						success: function(resp)
						{
							//alert(resp);
							var myObj = resp;
							for(i=0;i<resp.length;i++)
							{							
								agregarpolizaMTablaFacturas(resp[i].cuenta,resp[i].nombre,resp[i].cargo,resp[i].credito);
								
							}
							document.getElementById('imgexell').disabled=false;
        		            console.log(myObj);
							if(resp.error==1)
							{
								alert("Error en la Busqueda");
								document.getElementById('imgexell').disabled=true;
							}							
						}
					
					});
	

}
function agregarpolizaMTablaFacturas(cuenta,nomcuenta,cargo,credito)
{
	var Table = document.getElementById('tfacturas');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	newcell1.innerHTML = cuenta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell1.className ="td1";
	newcell1.style.width = "70px";

	var newcell2 = newrow.insertCell(1);
	newcell2.innerHTML = nomcuenta;//"<input type='hidden' id='vnumcuentacompleta' name='vnumcuentacompleta' value='"++"' readonly tabindex='-1' >"+numcuentacompleta;
	newcell2.className ="td8";
	newcell2.style.width = "450px";
	
	var newcell3 = newrow.insertCell(2);
	newcell3.innerHTML  ="<input type='hidden' id='vCargo' name='vCargo' readonly  value='"+cargo+"' >"+cargo;
	//"+addCommas(nomsubcuenta);
	newcell3.className ="td3";
	newcell3.style.width = "80px";

	var newcell4 = newrow.insertCell(3);
	newcell4.innerHTML ="<input type='hidden' id='vCredito' name='vCredito' readonly  value='"+credito+"' >"+credito;
	newcell4.className ="td4";
	newcell4.style.width = "80px";

}