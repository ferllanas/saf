<?php

include_once '../../cheques/lib/ez_sql_core.php'; 
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if(!$conexion)
	echo "Error en conexion";

//print_r($_REQUEST);
$anno=0;
$rubro="";
if(isset($_REQUEST['rubro']))
{
	$rubro=$_REQUEST['rubro'];
}

$tiempo=0;
if(isset($_REQUEST['tiempo']))
{
	$tiempo=$_REQUEST['tiempo'];
}

if(isset($_REQUEST['anno']))
	$anno=$_REQUEST['anno'];
else
	$anno=date("Y");
$mes=00;
if(isset($_REQUEST['mes']))
	$mes=$_REQUEST['mes'];
else
	$mes=date("m");
$consulta="EXECUTE sp_presup_informe_anual_cta_publica_1a_parte_det_rubro $anno, $mes, '$rubro', $tiempo";

	$saprobado=0.00;
	$samp_redu=0.00;
	$svigente=0.00;
	$scom=0.00;
	$sdxacom=0.00;
	$sdev=0.00;
	$scom_no_dev=0.00;
	$ssin_dev=0.00;
	$seje=0.00;
	$spag=0.00;
	$sctaxpag=0.00;

//echo $consulta;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css">         
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />       
<title>Documento sin título</title>
<style>
	#report{ font-size:13px
		
	}
	#report thead{
		background-color:#CCC;
		font-weight:800;
	}
	
</style>
</head>

<body>
<table width="100%">
	<tr>
    	<td><span class="TituloDForma">Resume Mensual por Rubro</span>
    <hr class="hrTitForma"></td>
    </tr>
    <tr>
    	<td>
        	<table id="report" border="1">
            	<thead>
                	<tr>
                    	<td align="center">Rubro</td>
              			<td align="center">Cuenta</td>
              			<td align="center">Total</td>
					</tr>
                </thead>
                <tbody id="tresultados">
                	<?php
						//echo $consulta;
						if($anno>0)
						{
							$stmt = sqlsrv_query($conexion, $consulta);
							if( $stmt === false)
							{
								echo $consulta;
								 die( print_r( sqlsrv_errors(), true) );
							}			
							else
							{
								//echo "Si entro";
								 $i=0;
							 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
							 {
							?>
							<tr>	
								<td align="left">  <?php echo trim($row['rubro']).".-".utf8_encode(trim($row['nomrubro'])); ?></td>
								<td align="left"><?php echo trim($row['cuenta']).".-".utf8_encode(trim($row['NOMCTA'])); ?></td>
								<td align="right"> <?php echo number_format(trim($row['suma']),2);	$samp_redu+=(float)$row['suma'];?></td>
							</tr>			
							<?php	
							}
						}
					}
					?>
                </tbody>
                <tfoot>
                	<tr>
              			<td	align="right">Total</td>
              			<td	align="right"></td>
              			<td	align="right"><?php echo number_format($samp_redu,2);?></td>
              		
                    </tr>
                </tfoot>
            </table>
        </td>
    </tr>
</table>
</body>
</html>