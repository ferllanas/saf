<?php

require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');



$datos=array();												  
$tpoliza="";
if(isset($_REQUEST["tipo_poliza"]))
	$tpoliza=$_REQUEST["tipo_poliza"];
	
$tipos_mov="";
if(isset($_REQUEST["tipos_mov"]))
	$tipos_mov=$_REQUEST["tipos_mov"];
	
$vista="";
if(isset($_REQUEST["vista"]))
	$vista=$_REQUEST["vista"];
	
$fecini="";
if(isset($_REQUEST["fecini"]))
	$fecini=$_REQUEST["fecini"];

$periodo="";
if(isset($_REQUEST["periodo"]))
	$periodo=$_REQUEST["periodo"];

$orden="";
if(isset($_REQUEST["orden"]))
	$orden=$_REQUEST["orden"];
	
$quin="";
if(isset($_REQUEST["quin"]))
	$quin=$_REQUEST["quin"];
	

$mesA="";
if(isset($_REQUEST["mesA"]))
	$mesA=$_REQUEST["mesA"];
	
$tipo_poliza="";
if(isset($_REQUEST["tipopoliza"]))
	$tipo_poliza=$_REQUEST["tipopoliza"];
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
$conexion = sqlsrv_connect($server,$infoconexion);

$tsql_callSP="";
if( (strlen($vista)>0 && strlen($vista)>0)  )
{

	
 	
	$conWhere=false;
	$tsql_callSP ="SELECT id, cuenta, convert(varchar(10),falta,103) as falta, nombre, dcorta,numdocto, cargo, credito, origen FROM $vista";
	
	
	if(strlen($tipos_mov)>0)
	{
		$tsql_callSP .="  WHERE tipomov IN($tipos_mov)";
		 $conWhere=true;
	}
	
	if($periodo=="fecha")
	{
		if(isset($_REQUEST['fecini'])  && strlen($_REQUEST['fecini'])>0)
			if($conWhere)
			{
				list($dia,$mes,$anio)=explode("/",$_REQUEST['fecini']);
				$tsql_callSP .=" AND falta='".$anio."/".$mes."/".$dia."'";	
			}
			else
			{
				$conWhere=true;
				list($dia,$mes,$anio)=explode("/",$_REQUEST['fecini']);
				$tsql_callSP .=" WHERE falta='".$anio."/".$mes."/".$dia."'";	
			}
		else
		{
			if($conWhere)
			{
				
				$tsql_callSP .=" AND falta='".date("Y/m/01")."'";	
				
			}
			else
			{
				$conWhere=true;
				$tsql_callSP .=" WHERE falta='".date("Y/m/01")."'";	
			}
		}
		$tsql_callSP.= " ORDER BY ".$orden;//" ORDER BY dcorta, numdocto";//id, cuenta, tipomov DESC
	}
	else
	{
		if($periodo=="quin")
		{
			if(!$conWhere)
				$tsql_callSP .=" WHERE referencia='".$quin."'";	
			else
				$tsql_callSP .=" AND referencia='".$quin."'";
				
				$tsql_callSP.=" ORDER BY ".$orden;//" ORDER BY dcorta, numdocto";//id, cuenta DESC";
		}
		else
		{
			//if($periodo=="mes")
			if(!$conWhere)
				$tsql_callSP .=" WHERE mes=".$mesA."";	
			else
				$tsql_callSP .=" AND mes=".$mesA."";
				
			$tsql_callSP.=" ORDER BY ".$orden;//" ORDER BY dcorta, numdocto";//id, cuenta, tipomov DESC";
		}
	}
	
	$i=0;
	$stmt = sqlsrv_query($conexion, $tsql_callSP);	
	if( $stmt )
	{
		 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		 {
			$datos[$i]['id']=$row["id"];
			$datos[$i]['cuenta']=trim($row["cuenta"]);
			$datos[$i]['falta']=trim($row["falta"]);
			$datos[$i]['nombre']=utf8_encode(trim($row["nombre"]));
			$datos[$i]['referencia']=  trim($row["dcorta"])."_".trim($row["numdocto"]);
			$datos[$i]['cargo']=number_format($row["cargo"],2,".",",");
			$datos[$i]['origen']=trim($row["origen"]);
			$datos[$i]['credito']=number_format($row["credito"],2,".",",");
			$i++;
		 }
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die(print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	
}
	//$datos[0]['consulta']=$tsql_callSP;
	echo json_encode($datos);

?>