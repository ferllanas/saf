<?php
	require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");
	require_once("../../dompdf/dompdf_config.inc.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');



														  
$tpoliza="";
if(isset($_REQUEST["tipo_poliza"]))
	$tpoliza=$_REQUEST["tipo_poliza"];
	
$tipos_mov="";
if(isset($_REQUEST["tipos_mov"]))
	$tipos_mov=$_REQUEST["tipos_mov"];
	
$vista="";
if(isset($_REQUEST["vista"]))
	$vista=$_REQUEST["vista"];
	
$fecini="";
if(isset($_REQUEST["fecini"]))
	$fecini=$_REQUEST["fecini"];

$periodo="";
if(isset($_REQUEST["periodo"]))
	$periodo=$_REQUEST["periodo"];
	
$quin="";
if(isset($_REQUEST["quin"]))
	$quin=$_REQUEST["quin"];
	

$mesA="";
if(isset($_REQUEST["mesA"]))
	$mesA=$_REQUEST["mesA"];
	
$tipo_poliza="";
if(isset($_REQUEST["tipopoliza"]))
	$tipo_poliza=$_REQUEST["tipopoliza"];
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />  
        <link href="../../css/esperaicon.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        
		<script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../../cheques/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
        
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
        <script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>		
		
$(document).ready(function() {
								   
	$(".mes").css("visibility", "hidden");
	$(".fecha").css("visibility", "hidden");
	$(".quin").css("visibility", "hidden");
	var $periodoSel =$( "#tipo_poliza option:selected" ).attr('periodo');
	$("."+$periodoSel).css("visibility", "visible");
		

	$( "select" ).change(function () {
		var $periodoSel 				=$( "#tipo_poliza option:selected" ).attr('periodo');

		$(".mes").css("visibility", "hidden");
		$(".fecha").css("visibility", "hidden");
		$(".quin").css("visibility", "hidden");
		$("."+$periodoSel).css("visibility", "visible");
	});
			
	$('#buscheq').click(function(){
			cargaDeNuevo();
	});
			
	$("#SelectALL").change(function() {
		if(this.checked) {
			$(".checkID").attr("checked","checked")
	 }
	 else{
		 $(".checkID").attr("checked","")
	 }
	});
	
	$(".botonExcel").click(function(event) {
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
	});
	
	$('#div_carga')
			.hide()
			.ajaxStart(function() {
				$(this).show();
			})
			.ajaxStop(function() {
				$(this).hide();
			})
		;
});
           
		   
function cargaDeNuevo()
{
	var $tipo_movdescripcion= $( "#tipo_poliza option:selected" ).text();
	var $tipo_poliza		= $( "#tipo_poliza option:selected" ).val();
	var $tipos_mov			= $( "#tipo_poliza option:selected" ).attr('tipos_mov');
	var $vista 				=$( "#tipo_poliza option:selected" ).attr('observa');
	var $periodo 				=$( "#tipo_poliza option:selected" ).attr('periodo');
	var $orden 				=$( "#tipo_poliza option:selected" ).attr('orden');
	
	var $mesA		= $( "#mesA option:selected" ).val();
	var $quin		= $( "#quin" ).val();
	
	var $fecini 			=$( "#fecini" ).val();
			
	var datas={
			tipo_poliza: $tipo_poliza,
			tipo_movdescripcion: $tipo_movdescripcion,
			tipos_mov: $tipos_mov,
			vista: $vista ,
			periodo: $periodo,
			mesA: $mesA,
			quin:$quin,
			fecini:$fecini,
			orden: $orden
	};
	
	console.log(datas);
	jQuery.ajax({
		type:     'post',
		cache:    false,
		url:      'buscarPolizas.php',
		data:     datas,
		dataType: 'json',
		error: function(  jqXHR,  textStatus,  errorThrown )
		{
			$('#proveedor').empty();
			$("#btnguardar").attr("disabled",false);
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			
		},
		success: function(resp) 
		{
			console.log(resp);
			if(resp.lenght<=0)
				alert("No existen resultado para estos terminos, favor de verificar su consulta.");
			else{
				$('#proveedor').empty();
				$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
			}
		}
	});
}
		
function guardarSeleccion()
{
	$("#btnguardar").attr("disabled",true);
	
	var carts = [];
	$("#proveedor tr").each(function(){
		var objetoo=$(this).find(".checkID");
		if(objetoo.is(":checked")==true)
		{
			var element = {};
			$(this).find("td").each(function(){
				$(this).find("span").each(function(){
						//alert($(this).attr("id"));	
						element[$(this).attr("id")]=$(this).html();
				});
			});
			carts.push(element);
		}				
	});
	
	if(carts.length<=0){
		$("#btnguardar").attr("disabled",false);
		alert("Debe seleccionar almenos un movimiento.");
		return 0;
	}
	
	var fecha=$('#fecini').val();
	if(fecha.length<=0 || fecha.length<10){
		$("#btnguardar").attr("disabled",false);
		alert("Debe seleccionar una fecha, el formato debe ser dd/mm/aaaa!.");
		return 0;
	}
	
	var datas={
			movs:carts,
			fecini: $('#fecini').val(),
			mesA: $( "#mesA option:selected" ).val(),
			quin: $( "#quin option:selected" ).val(),
			periodo: $( "#tipo_poliza option:selected" ).attr('periodo'),
			tipo_poliza: $( "#tipo_poliza option:selected" ).val()
	};
	
	//console.log(datas);
	jQuery.ajax({
		type:     'post',
		cache:    false,
		url:      'guardar_movs.php',
		data:     datas,
		dataType: 'json',
		error: function(  jqXHR,  textStatus,  errorThrown )
		{
			$("#btnguardar").attr("disabled",false);
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
		},
		success: function(resp) 
		{
			console.log(resp);
			if(resp.error==1)
			{
				alert(resp.msg);
			}
			else
			{
				alert("La poliza "+resp.idmpoliza+" ha sido generada satizfactoriamente.");
				cargaDeNuevo();//setInterval('cargaDeNuevo()',5000);
				
			}
		}
	});
}
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl"> 
		<tr>
              <td align="center"><input type="checkbox" class="checkID" value="${id}" name="idcontabdmovs"></td>
              <td align="center"><span id="cuenta">${cuenta}</span></td>
              <td align="center"><span id="falta">${falta}</span></td>
              <td align="left"><span id="nombre">${nombre}</span></td>
              <td align="center"><span id="referencia">${referencia}</span></td>
              <td align="right"><span id="cargo">${cargo}</span></td>
              <td align="right"><span id="origen" style="visibility:hidden;">${origen}</span>
                                <span id="idm" style="visibility:hidden;">${id}</span>
                                <span id="credito">${credito} </span>
              					</td>

            </tr>
           <!-- <tr>
                {{if cuenta}}
					<td align="left">${cuenta}</td>
					<td >${nombre}</td>
					<td align="right">${carini}</td>
					<td align="right">${creini}</td>
					
					<td align="right">${cargo}</td>
					<td align="right">${credito}</td>	
					<td align="right">${carfin}</td>-->
					<td align="right">${crefin}</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
            </tr>-->
			
        </script>   
   
<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Consulta de Asientos</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
 <form action="polizas_auto.php" method="post">
<table >   
<tr>


<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->

<td><!-- Asignamos variables de fechas -->
  <!--Poliza:<input type="text"  name="poliza" id="poliza" maxlength="10" tabindex="1">    -->
  Tipo:<select name="tipo_poliza" id="tipo_poliza"><?php 
  $tsql_callSP ="SELECT * FROM contabmtipo_poliza where estatus<9000";
  
  $stmt = sqlsrv_query($conexion, $tsql_callSP);	
	if( $stmt )
	{
		 while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		 {
  ?>
  
  <option value="<?php echo trim($row['tpoliza']);?>" 
  			periodo="<?php echo trim($row['periodo']);?>" 
            tipos_mov="<?php echo trim($row['tipos_mov']);?>" 
            observa="<?php echo trim($row['observa']);?>" <?php if($tpoliza==trim($row['tpoliza']) ) echo "SELECTED";?>
  			orden="<?php echo trim($row['orden']);?>"><?php echo trim($row['tpoliza']).".- ".utf8_encode(trim($row['descrip']));?></option>
  <?php
		 }
	}
  ?></select><br><div style="position:relative;"><div class="fecha" align="left" style="text-align:left; position:relative; left:5px; top:5px;">
  Fecha:<input type="text"  name="fecini" id="fecini" size="10" value="<?php if(isset($_REQUEST['fecini'])) echo $_REQUEST['fecini']; else echo date("d/m/Y");?>">
  <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecini",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger1",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>
   </div>
   <div class="mes" align="left" style="text-align:left;position:relative; left:5px; top:5px;">
   	Mes:<select id="mesA">
    		<option value="1" <?php if($mesA==1) echo "SELECTED";?>>Enero</option>
            <option value="2" <?php if($mesA==2) echo "SELECTED";?>>Febrero</option>
            <option value="3" <?php if($mesA==3) echo "SELECTED";?>>Marzo</option>
            <option value="4" <?php if($mesA==4) echo "SELECTED";?>>Abril</option>
            <option value="5" <?php if($mesA==5) echo "SELECTED";?>>Mayo</option>
            <option value="6" <?php if($mesA==6) echo "SELECTED";?>>Junio</option>
            <option value="7" <?php if($mesA==7) echo "SELECTED";?>>Julio</option>
            <option value="8" <?php if($mesA==8) echo "SELECTED";?>>Agosto</option>
            <option value="9" <?php if($mesA==9) echo "SELECTED";?>>Septiembre</option>
            <option value="10" <?php if($mesA==10) echo "SELECTED";?>>Octubre</option>
            <option value="11" <?php if($mesA==11) echo "SELECTED";?>>Noviembre</option>
            <option value="12" <?php if($mesA==12) echo "SELECTED";?>>Diciembre</option></select>
   </div>
   <div class="quin" align="left" style="text-align:left; position:relative; left:5px; top:5px;">
   Periodo:<input id="quin" type="text" value="<?php echo $quin;?>" width="60px" maxlength="6"> aaaaqq
   </div>
   </div>
 
  </td>
 <td>
<input type="button" name="buscheq" id="buscheq" value="Buscar"  tabindex="5">
</td>
</tr>
</table>
</form>
<table id="Exportar_a_Excel">
    <thead>
        <th width="40px"><input type="checkbox" name="SelectALL" id="SelectALL">Tipo</th>
        <th width="40px">Cuenta</th>
        <th width="30px">Fecha</th> 
        <th width="40px">Nombre</th> 
        <th width="40px">Referencia</th> 
        <th width="40px">Cargo</th>   
        <th width="40px">Credito</th> 
    </thead>
    <tbody id="proveedor" name='proveedor'>        
	</tbody>
</table>
  
  <table>
  	<tr>
    	<td><input type="button" value="Cancelar"></td><td><input type="button" id="btnguardar" value="Guardar" onClick="guardarSeleccion();"></td>
   </tr>
  </table>
    </div>
    <div id="div_carga">
   		<img src="../../imagenes/ajax-loader.gif" width="64" id="cargador">
    </div>
    </body>
</html>
