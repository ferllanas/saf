
//Agrega una nueva menu en la lista despencientes
function agrega_Menu()
{	
	var usuario=$('usuario').value;
	
	//alert('php_ajax/agregar_menu.php?usuario='+usuario);
	new Ajax.Request('php_ajax/agregar_menu.php?usuario='+usuario,
						{onSuccess : function(resp) 
							{
								if( resp.responseText ) 
								{
									if(resp.responseText=='null')
										alert("No existen resultados para estos criterios.");
										
									var myArray = eval(resp.responseText);
									if(myArray.length>0)
									{
										for(var i = 0; i <myArray.length; i++)
										{
											if(myArray[i].error!="1")
											{
												var tabla = $('menus');
												var plantilla = document.getElementById('filaOculta');
												var nuevaFila = plantilla.cloneNode(true);
												nuevaFila.style.visibility = 'visible';  //hacemos visible la fila
												tabla.appendChild(nuevaFila);
										
												for (var j = 0; j <= nuevaFila.cells.length;j++)
												{
													var cell = nuevaFila.cells[j];
													if(typeof(cell) != "undefined")
													{
														for (var k = 0; k < cell.childNodes.length; k++) 
														{
															var mynode = cell.childNodes[k];
																							
																							
															if(mynode.name=="nombre")
																mynode.value="";
																								
															if(mynode.name=="descripcion")
																mynode.value="";
																								
															if(mynode.name=="liga")
																mynode.value="";
																							
															if(mynode.name=="tipo")
																mynode.value="";
																
															if(mynode.name=="posicion")
																mynode.value="";
																
															if(mynode.name=="status")
																mynode.value="";
																
															if(mynode.name=="id_menu")
																mynode.value=myArray[i].idef;
					
														}
													}
												}
											}
										}
									}
									else
									{
										alert("No existen resultados para estos criterios.");
									}
									
								}
							}
						});
}

//Elimina una menu
function eliminar_Menu(e,s)
{
	var table=$('tmenus');
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="id_menu")
				{
					//alert(mynode.value);
					new Ajax.Request('php_ajax/eliminar_menu.php?id='+mynode.value,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("menus eliminada!.");
																location.reload(true);
															}
															else
															{
																alert("Error al eliminar la menu!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
				}
			}
		}
	}
												
	///
}

function GuardarMenus()
{
	var usuario=$('usuario').value;
	
	var table=$('tmenus');
									
	var cadena_menus="(";
	for (var i = 1; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_menus+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="nombre")
					cadena_menus+='"nombre"=>"'+mynode.value+'",';
													
				if(mynode.name=="descripcion")
					cadena_menus+='"descripcion"=>"'+mynode.value+'",';
													
				if(mynode.name=="liga")
					cadena_menus+='"liga"=>"'+mynode.value+'",';
												
				if(mynode.name=="tipo")
					cadena_menus+='"tipo"=>"'+mynode.value+'",';
					
				if(mynode.name=="posicion")
					cadena_menus+='"posicion"=>"'+mynode.value+'",';
					
				if(mynode.name=="status")
					cadena_menus+='"status"=>"'+mynode.value+'",';
					
				if(mynode.name=="id_menu")
					cadena_menus+='"id"=>"'+mynode.value+'",';
					
			}
		}
		cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
		cadena_menus+='),';
	}
	cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
								cadena_menus+=')';
								//$('id_estudioPAE').value=myArray[0].idef;
								//alert('php_ajax/actualizar_menus.php?menus='+cadena_menus+'&usuario='+usuario);
								new Ajax.Request('php_ajax/actualizar_menus.php?menus='+cadena_menus+'&usuario='+usuario,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														//alert(resp.responseText);
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																alert("menus actualizadas!.");
																location.reload(true);
															}
															else
															{
																alert("Error en proceso de actualizacion!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
}