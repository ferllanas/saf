
//Agrega una nueva menu en la lista despencientes
function agrega_Usuario()
{	
	var usuario=$('usuario').value;
	
	//alert('php_ajax/agregar_menu.php?usuario='+usuario);
	new Ajax.Request('php_ajax/agregar_usuario.php?',
						{onSuccess : function(resp) 
							{
								if( resp.responseText ) 
								{
									if(resp.responseText=='null')
										alert("No existen resultados para estos criterios.");
										
									var myArray = eval(resp.responseText);
									if(myArray.length>0)
									{
										for(var i = 0; i <myArray.length; i++)
										{
											if(myArray[i].error!="1")
											{
												var tabla = $('menus');
												var plantilla = document.getElementById('filaOculta');
												var nuevaFila = plantilla.cloneNode(true);
												nuevaFila.style.visibility = 'visible';  //hacemos visible la fila
												tabla.appendChild(nuevaFila);
										
												for (var j = 0; j <= nuevaFila.cells.length;j++)
												{
													var cell = nuevaFila.cells[j];
													if(typeof(cell) != "undefined")
													{
														for (var k = 0; k < cell.childNodes.length; k++) 
														{
															var mynode = cell.childNodes[k];
																							
																							
															if(mynode.name=="usuario")
																mynode.value="";
																								
															if(mynode.name=="nombre")
																mynode.value="";
																
															if(mynode.name=="id_usuario")
																mynode.value=myArray[i].idef;
					
														}
													}
												}
											}
										}
									}
									else
									{
										alert("No existen resultados para estos criterios.");
									}
									
								}
							}
						});
}

//Elimina una menu
function eliminar_Usuario(e,s)
{
	var table=$('tmenus');
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="id_usuario")
				{
					//alert(mynode.value);
					new Ajax.Request('php_ajax/eliminar_menu.php?id='+mynode.value,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Usuario eliminado!.");
																location.reload(true);
															}
															else
															{
																alert("Error al eliminar la menu!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
				}
			}
		}
	}
												
	///
}

function GuardarUsuarios()
{
	var usuario=$('usuario').value;
	
	var table=$('tmenus');
									
	var cadena_menus="(";
	for (var i = 1; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_menus+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="usuario")
					cadena_menus+='"usuario"=>"'+mynode.value+'",';
													
				if(mynode.name=="nombre")
					cadena_menus+='"nombre"=>"'+mynode.value+'",';
													
				if(mynode.name=="area")
					cadena_menus+='"area"=>"'+mynode.value+'",';
					
				if(mynode.name=="id_usuario")
					cadena_menus+='"id_usuario"=>"'+mynode.value+'",';
												
			}
		}
		cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
		cadena_menus+='),';
	}
	cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
								cadena_menus+=')';
								//$('id_estudioPAE').value=myArray[0].idef;
								//alert('php_ajax/actualizar_usuarios.php?usuarios='+cadena_menus);
								new Ajax.Request('php_ajax/actualizar_usuarios.php?usuarios='+cadena_menus,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														//alert(resp.responseText);
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																alert("Usuarios modificados!.");
																location.reload(true);
															}
															else
															{
																alert("Error en proceso de actualizacion!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
}

function GuardarDetUsuarios()
{
	var usuario= $('usuario').value;
	var pswd=$('pswd').value;
	var nombre=$('nombre').value;
	var estatus=$('estatus').value;
	var prom=$('prom').value;
	var cajero=$('cajero').value;
	var area=$('area').value;
	var numemp=$('numemp').value;
	var usuant=$('usuant').value;
	var fun=$('fun').value;
	var id_usuario=$('id_usuario').value;
	
	var table=$('tmenudusuario');
									
	var cadena_menus="(";
	for (var i = 1; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_menus+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="modulo")
					cadena_menus+='"modulo"=>"'+mynode.value+'",';
													
				if(mynode.name=="id_modulo")
				{
					cadena_menus+='"id_modulo"=>"'+mynode.value+'",';
				}
				
			}
		}
		cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
		cadena_menus+='),';
	}
	cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
								cadena_menus+=')';
								//$('id_estudioPAE').value=myArray[0].idef;
								/*alert('php_ajax/actualizar_detusuarios.php?menudusuarios='+cadena_menus+'&usuario='+usuario+
												 '&pswd='+pswd+ '&nombre='+nombre+ '&estatus='+estatus+	 '&prom='+prom+
												 '&cajero='+cajero+ '&area='+area+ '&numemp='+numemp+ '&usuant='+usuant+
													'&fun='+fun+'&id_usuario='+id_usuario);
								*/
								new Ajax.Request('php_ajax/actualizar_detusuarios.php?menudusuarios='+cadena_menus+'&usuario='+usuario+
												 '&pswd='+pswd+ '&nombre='+nombre+ '&estatus='+estatus+	 '&prom='+prom+
												 '&cajero='+cajero+ '&area='+area+ '&numemp='+numemp+ '&usuant='+usuant+
													'&fun='+fun+'&id_usuario='+id_usuario,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														//alert(resp.responseText);
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																alert("Actualizacion de usuario completada!.");
																location.reload(true);
															}
															else
															{
																alert("Error en proceso de actualizacion!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
}

function eliminar_MenuDUsuario(e,s)
{
	
	//var table=$('tmenudusuario');
	var nombre_modulo="";
	var nombre=$('nombre').value;
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				
				if(mynode.name=="modulo")
				{
					nombre_modulo=mynode.text;
				}
				if(mynode.name=="id_modulo")
				{
					//alert(mynode.value);
					new Ajax.Request('php_ajax/eliminar_menudeusuario.php?id='+mynode.value,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Fue eliminado el acceso del usuario "+nombre+" al modulo!");
																location.reload(true);
															}
															else
															{
																alert("Error al eliminar la menu!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
				}
			}
		}
	}
}

function agrega_MenuDUsuario()
{
	var usuario=$('usuario').value;
	
	
	new Ajax.Request('php_ajax/agregar_menuausuario.php?usuario='+usuario,
						{onSuccess : function(resp) 
							{
								if( resp.responseText ) 
								{
									if(resp.responseText=='null')
										alert("No existen resultados para estos criterios.");
										
									var myArray = eval(resp.responseText);
									if(myArray.length>0)
									{
										for(var i = 0; i <myArray.length; i++)
										{
											if(myArray[i].error!="1")
											{
												var tabla = $('menus');
												var plantilla = document.getElementById('filaOculta');
												var nuevaFila = plantilla.cloneNode(true);
												nuevaFila.style.visibility = 'visible';  //hacemos visible la fila
												tabla.appendChild(nuevaFila);
										
												for (var j = 0; j <= nuevaFila.cells.length;j++)
												{
													var cell = nuevaFila.cells[j];
													if(typeof(cell) != "undefined")
													{
														for (var k = 0; k < cell.childNodes.length; k++) 
														{
															var mynode = cell.childNodes[k];
																							
																							
															if(mynode.name=="modulo")
																mynode.value="";
																								
															if(mynode.name=="id_modulo")
																mynode.value=myArray[i].idef;
																
															if(mynode.name=="posicion")
																mynode.value="";
					
														}
													}
												}
											}
										}
									}
									else
									{
										alert("No existen resultados para estos criterios.");
									}
									
								}
							}
						});
}


function usuario_quitar_Para_aut_Requisi(e,s)
{
	//var table=$('tmenus');
	var rowindexA = s.parentNode.parentNode.rowIndex-1;
	var id_suario=s.getAttribute("valor");
	new Ajax.Request('php_ajax/quitar_Usuario_Para_Firmas_Requisi.php?id='+id_suario,
											{onSuccess : function(resp) 
												{
													console.log(resp);
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Usuario eliminado!.");
																location.reload(true);
															}
															else
															{
																alert("Error tratar de editar usuario!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
/*	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="id_usuario")
				{
					new Ajax.Request('php_ajax/quitar_Usuario_Para_Firmas_Requisi.php?id='+mynode.value,
											{onSuccess : function(resp) 
												{
													console.log(resp);
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Usuario eliminado!.");
																location.reload(true);
															}
															else
															{
																alert("Error tratar de editar usuario!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
					
				}
			}
		}
	}
		*/										
	///
}

function usuario_add_aut_requisi(){
	var nomemp = $('nomemp').value ;
	var numemp = $('numemp').value ;
	var dir= 	 $('dir').value ;
	if(nomemp.length<=0)
	{
		alert("Favor de Seleccionar un usuario con la busqueda incremental");
		return false;
	}
	
	new Ajax.Request('php_ajax/usuario_add_requisiciones.php?numemp='+numemp+'&dir='+dir,
	{onSuccess : function(resp) 
		{
			var myArray = eval(resp.responseText);
			if(myArray.length>0)
			{
				if(myArray[0].error==0)
				{
					alert ("El usuario ha sido registrado");
					location.reload(true);
				}
				else
				{
					alert ("Ha ocurrido un error al intentar registrar al usuario.");
					alert(myArray[0].string);
				}
			}
			/*
			console.log(resp.responseText);
			console.log(resp.responseText.error);
			console.log(resp.responseText[0].error);
			if( resp.responseText[0].error==0 ) 
			{
				alert ("El usuario ha sido registrado");
				location.reload(true);
			}
			else
				alert ("Ha ocurrido un error al intentar registrar al usuario.");
			*/
		}
	});
}



function usuario_quitar_Para_aut_solche(e,s)
{
	
	var id_suario=s.getAttribute("valor");
	//var table=$('tmenus');
	var tabla_firmas=$('tabla_firmas').value;
	
	new Ajax.Request('php_ajax/quitar_Usuario_Para_Firmas_solche.php?id='+id_suario+'&tabla='+tabla_firmas,
											{onSuccess : function(resp) 
												{
													console.log(resp);
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Usuario eliminado!.");
																location.reload(true);
															}
															else
															{
																alert("Error tratar de editar usuario!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});

/*var rowindexA=s.parentNode.parentNode.rowIndex-1;
	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="id_usuario")
				{
					//alert(mynode.value);
					//return false;
					
					new Ajax.Request('php_ajax/quitar_Usuario_Para_Firmas_solche.php?id='+mynode.value+'&tabla='+tabla_firmas,
											{onSuccess : function(resp) 
												{
													console.log(resp);
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Usuario eliminado!.");
																location.reload(true);
															}
															else
															{
																alert("Error tratar de editar usuario!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
					
				}
			}
		}
	}
				*/								
	///
}

function usuario_add_aut_solche(){
	var tabla_firmas=$('tabla_firmas').value;
	var nomemp = $('nomemp').value ;
	var numemp = $('numemp').value ;
	var depto= 	 $('depto').value ;
	if(nomemp.length<=0)
	{
		alert("Favor de Seleccionar un usuario con la busqueda incremental");
		return false;
	}
	
	new Ajax.Request('php_ajax/usuario_add_aut_solche.php?numemp='+numemp+'&depto='+depto+'&tabla='+tabla_firmas,
	{onSuccess : function(resp) 
		{
			var myArray = eval(resp.responseText);
			if(myArray.length>0)
			{
				if(myArray[0].error==0)
				{
					alert ("El usuario ha sido registrado");
					location.reload(true);
				}
				else
				{
					alert ("Ha ocurrido un error al intentar registrar al usuario.");
					alert(myArray[0].string);
				}
			}
			/*
			console.log(resp.responseText);
			console.log(resp.responseText.error);
			console.log(resp.responseText[0].error);
			if( resp.responseText[0].error==0 ) 
			{
				alert ("El usuario ha sido registrado");
				location.reload(true);
			}
			else
				alert ("Ha ocurrido un error al intentar registrar al usuario.");
			*/
		}
	});
}