
//Agrega una nueva menu en la lista despencientes
function agrega_Multifuncional()
{	
	var usuario=$('usuario').value;
	
	//alert('php_ajax/agregar_menu.php?usuario='+usuario);
	new Ajax.Request('php_ajax/agregar_multifuncional.php?',
						{onSuccess : function(resp) 
							{
								if( resp.responseText ) 
								{
									if(resp.responseText=='null')
										alert("No existen resultados para estos criterios.");
										
									var myArray = eval(resp.responseText);
									if(myArray.length>0)
									{
										for(var i = 0; i <myArray.length; i++)
										{
											if(myArray[i].error!="1")
											{
												var tabla = $('menus');
												var plantilla = document.getElementById('filaOculta');
												var nuevaFila = plantilla.cloneNode(true);
												nuevaFila.style.visibility = 'visible';  //hacemos visible la fila
												tabla.appendChild(nuevaFila);
										
												for (var j = 0; j <= nuevaFila.cells.length;j++)
												{
													var cell = nuevaFila.cells[j];
													if(typeof(cell) != "undefined")
													{
														for (var k = 0; k < cell.childNodes.length; k++) 
														{
															var mynode = cell.childNodes[k];
																							
																							
															if(mynode.name=="nombre")
																mynode.value="";
																								
															if(mynode.name=="descripcion")
																mynode.value="";
																
															if(mynode.name=="ip")
																mynode.value="";
																
															if(mynode.name=="id_multifuncional")
																mynode.value=myArray[i].idef;
					
														}
													}
												}
											}
										}
									}
									else
									{
										alert("No existen resultados para estos criterios.");
									}
									
								}
							}
						});
}

//Elimina una menu
function eliminar_Multifuncional(e,s)
{
	var table=$('tmenus');
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="id_multifuncional")
				{
					//alert(mynode.value);
					new Ajax.Request('php_ajax/eliminar_multifuncional.php?id='+mynode.value,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Multifuncional eliminado!.");
																location.reload(true);
															}
															else
															{
																alert("Error al eliminar la menu!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
				}
			}
		}
	}
												
	///
}

function Guardar_Multifuncionales()
{
	var usuario=$('usuario').value;
	
	var table=$('tmenus');
									
	var cadena_menus="(";
	for (var i = 1; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_menus+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="nombre")
					cadena_menus+='"nombre"=>"'+mynode.value+'",';
													
				if(mynode.name=="descripcion")
					cadena_menus+='"descripcion"=>"'+mynode.value+'",';
													
				if(mynode.name=="ip")
					cadena_menus+='"ip"=>"'+mynode.value+'",';
					
				if(mynode.name=="area")
					cadena_menus+='"area"=>"'+mynode.value+'",';
				
				
				if(mynode.name=="documentpath")
					cadena_menus+='"documentpath"=>"'+mynode.value+'",';
					
				if(mynode.name=="id_multifuncional")
					cadena_menus+='"id_multifuncional"=>"'+mynode.value+'",';
												
			}
		}
		cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
		cadena_menus+='),';
	}
	cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
								cadena_menus+=')';
								//$('id_estudioPAE').value=myArray[0].idef;
								//alert('php_ajax/actualizar_multifuncional.php?usuarios='+cadena_menus);
								new Ajax.Request('php_ajax/actualizar_multifuncional.php?usuarios='+cadena_menus,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														//alert(resp.responseText);
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																alert("Multifuncionales modificados!.");
																location.reload(true);
															}
															else
															{
																alert("Error en proceso de actualizacion!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
}

function GuardarDetUsuarios()
{
	var usuario= $('usuario').value;
	var pswd=$('pswd').value;
	var nombre=$('nombre').value;
	var estatus=$('estatus').value;
	var prom=$('prom').value;
	var cajero=$('cajero').value;
	var area=$('area').value;
	var numemp=$('numemp').value;
	var usuant=$('usuant').value;
	var fun=$('fun').value;
	var id_multifuncional=$('id_multifuncional').value;
	
	var table=$('tmenudusuario');
									
	var cadena_menus="(";
	for (var i = 1; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_menus+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="modulo")
					cadena_menus+='"modulo"=>"'+mynode.value+'",';
													
				if(mynode.name=="id_modulo")
				{
					cadena_menus+='"id_modulo"=>"'+mynode.value+'",';
				}
				
			}
		}
		cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
		cadena_menus+='),';
	}
	cadena_menus=cadena_menus.substring(0,cadena_menus.length-1);
								cadena_menus+=')';
								//$('id_estudioPAE').value=myArray[0].idef;
								/*alert('php_ajax/actualizar_detusuarios.php?menudusuarios='+cadena_menus+'&usuario='+usuario+
												 '&pswd='+pswd+ '&nombre='+nombre+ '&estatus='+estatus+	 '&prom='+prom+
												 '&cajero='+cajero+ '&area='+area+ '&numemp='+numemp+ '&usuant='+usuant+
													'&fun='+fun+'&id_multifuncional='+id_multifuncional);
								*/
								new Ajax.Request('php_ajax/actualizar_detusuarios.php?menudusuarios='+cadena_menus+'&usuario='+usuario+
												 '&pswd='+pswd+ '&nombre='+nombre+ '&estatus='+estatus+	 '&prom='+prom+
												 '&cajero='+cajero+ '&area='+area+ '&numemp='+numemp+ '&usuant='+usuant+
													'&fun='+fun+'&id_multifuncional='+id_multifuncional,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														//alert(resp.responseText);
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																alert("Actualizacion de usuario completada!.");
																location.reload(true);
															}
															else
															{
																alert("Error en proceso de actualizacion!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
}

function eliminar_MenuDUsuario(e,s)
{
	
	//var table=$('tmenudusuario');
	var nombre_modulo="";
	var nombre=$('nombre').value;
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	for (var j = 0; j <= $('menus').rows[rowindexA].cells.length;j++)
	{
		var cell = $('menus').rows[rowindexA].cells[j];
		if(typeof(cell) != "undefined")
		{
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				
				if(mynode.name=="modulo")
				{
					nombre_modulo=mynode.text;
				}
				if(mynode.name=="id_modulo")
				{
					//alert(mynode.value);
					new Ajax.Request('php_ajax/eliminar_menudeusuario.php?id='+mynode.value,
											{onSuccess : function(resp) 
												{
													if( resp.responseText ) 
													{
														var myArray = eval(resp.responseText);
														if(myArray.length>0)
														{
															if(myArray[0].error==0)
															{
																//$('menus').deleteRow(s.parentNode.parentNode.rowIndex-1);
																alert("Fue eliminado el acceso del usuario "+nombre+" al modulo!");
																location.reload(true);
															}
															else
															{
																alert("Error al eliminar la menu!.");
																alert(myArray[0].string);
															}
														}
													}
												}
											});
				}
			}
		}
	}
}

function agrega_MenuDUsuario()
{
	var usuario=$('usuario').value;
	
	
	new Ajax.Request('php_ajax/agregar_menuausuario.php?usuario='+usuario,
						{onSuccess : function(resp) 
							{
								if( resp.responseText ) 
								{
									if(resp.responseText=='null')
										alert("No existen resultados para estos criterios.");
										
									var myArray = eval(resp.responseText);
									if(myArray.length>0)
									{
										for(var i = 0; i <myArray.length; i++)
										{
											if(myArray[i].error!="1")
											{
												var tabla = $('menus');
												var plantilla = document.getElementById('filaOculta');
												var nuevaFila = plantilla.cloneNode(true);
												nuevaFila.style.visibility = 'visible';  //hacemos visible la fila
												tabla.appendChild(nuevaFila);
										
												for (var j = 0; j <= nuevaFila.cells.length;j++)
												{
													var cell = nuevaFila.cells[j];
													if(typeof(cell) != "undefined")
													{
														for (var k = 0; k < cell.childNodes.length; k++) 
														{
															var mynode = cell.childNodes[k];
																							
																							
															if(mynode.name=="modulo")
																mynode.value="";
																								
															if(mynode.name=="id_modulo")
																mynode.value=myArray[i].idef;
																
															if(mynode.name=="posicion")
																mynode.value="";
					
														}
													}
												}
											}
										}
									}
									else
									{
										alert("No existen resultados para estos criterios.");
									}
									
								}
							}
						});
}