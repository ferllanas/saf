//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectSolche() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqSolche = getXmlHttpRequestObjectSolche(); 

function searchProveedorSolche()
{
	//$('nombre').value = "";
	//limpia_tabla();
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";
	var tabla_firmas=$('tabla_firmas').value;
    if (searchReqSolche.readyState == 4 || searchReqSolche.readyState == 0)
	{
        var str = escape(document.getElementById('provname1').value);
        searchReqSolche.open("GET",'php_ajax/queryusuarioincremSolche.php?q=' + str+'&tabla='+tabla_firmas ,true)//'searchSuggest.php?search=' + str, true);
        searchReqSolche.onreadystatechange = handleSearchSuggestSolche;
        searchReqSolche.send(null);
    }        
}

//Called when the AJAX response is returned.
function handleSearchSuggestSolche() {
	
	//alert("aja 1");
    if (searchReqSolche.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv')
        ss.innerHTML = '';
        var str = searchReqSolche.responseText.split("\n");
	   for(i=0; i < str.length - 1 && i<50; i++) 
	   {
			//alert(str);
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var tok = str[i].split(";");
			var suggest = '<div onmouseover="javascript:suggestOverSolche(this);" ';
			suggest += 'onmouseout="javascript:suggestOutSolche(this);" ';
			suggest += "onclick='javascript:setSearchSolche(this.innerHTML,this.title);' ";
			suggest += 'class="suggest_link" title="'+tok[0]+';'+tok[1]+';'+tok[2]+';'+tok[3]+';'+tok[4]+';'+tok[5]+'">' + tok[1] +'.-' + tok[0] + '</div>';
			ss.innerHTML += suggest;
		}//
    }
}

//Mouse over function
function suggestOverSolche(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutSolche(div_value) {
    div_value.className = 'suggest_link';
}

function setSearchSolche(value,idprod) 
{
    document.getElementById('provname1').value = value;
	var tok =idprod.split(";");
	$('nomemp').value = tok[0];
	$('numemp').value = tok[1];
	$('dir').value = tok[4];
	$('depto').value = tok[3];
	
	document.getElementById('search_suggestProv').innerHTML = '';
}
//////////////////////////////////////////////////////////////////////////

function getXmlHttpRequestObjectProv1() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv1 = getXmlHttpRequestObjectProv1();

function searchProveedor1() 
{
	
	//$('nombre').value = "";
	//limpia_tabla();
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqProv1.readyState == 4 || searchReqProv1.readyState == 0)
	{
        var str = escape(document.getElementById('provname1').value);
        searchReqProv1.open("GET",'php_ajax/queryusuarioincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv1.onreadystatechange = handleSearchSuggestProv1;
        searchReqProv1.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv1() {
	
	//alert("aja 1");
    if (searchReqProv1.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv')
        ss.innerHTML = '';
        var str = searchReqProv1.responseText.split("\n");
	   for(i=0; i < str.length - 1 && i<50; i++) 
	   {
			//alert(str);
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var tok = str[i].split(";");
			var suggest = '<div onmouseover="javascript:suggestOverProv1(this);" ';
			suggest += 'onmouseout="javascript:suggestOutProv1(this);" ';
			suggest += "onclick='javascript:setSearchProv1(this.innerHTML,this.title);' ";
			suggest += 'class="suggest_link" title="'+tok[0]+';'+tok[1]+';'+tok[2]+';'+tok[3]+';'+tok[4]+';'+tok[5]+'">' + tok[1] +'.-' + tok[0] + '</div>';
			ss.innerHTML += suggest;
		}//
    }
}
//Mouse over function
function suggestOverProv1(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv1(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv1(value,idprod) 
{
    document.getElementById('provname1').value = value;
	var tok =idprod.split(";");
	$('nomemp').value = tok[0];
	$('numemp').value = tok[1];
	$('dir').value = tok[4];
	document.getElementById('search_suggestProv').innerHTML = '';
}


function limpia_tabla()
{
	var tabla= $('provsT');//Asigna a la variable tabla el objeto provsT(cuerpo de la tabla) que se encuentra declarado en buscaprov.php

	var numren=tabla.rows.length;
	for(var i = numren-1; i >=0; i--)
	{
		tabla.deleteRow(i);
	}
}