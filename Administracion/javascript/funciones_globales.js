function llenarCeros(object)
{
	if(object.value.length>0)
		object.value = ('0000'+object.value).substring(('0000'+object.value).length-4,('0000'+object.value).length)
}

function addCeros(object)
{
	if(object.value.length>0)
	{
		var stri = '0000'+object.value;
		stri=stri.substr( stri.length - 4, 4 );
		object.value=stri;
	}
}

//Funcion permite no aceptar el focus
function clearDefault(arg) {	//alert("Test");	
arg.value="";
}

function html_entity_decode(str) {
  var ta=document.createElement("textarea");
  ta.innerHTML=str.replace(/</g,"&lt;").replace(/>/g,"&gt;");
  return ta.value;
}

function encode_utf8( s ){  return unescape( encodeURIComponent( s ) );}

function decode_utf8( s ){  return decodeURIComponent( escape( s ) );}


function validarDobleComillas(str)
{
	if(str.search(/"/i)>-1)
	{
		return false;
	}
	else
		return true;
	
}

function aceptarSoloNumerosEnteros(objecto, e)
{
	tecla=(document.all) ? e.keyCode : e.which;
	if (tecla ==9  //Tab
		|| (tecla >=48 && tecla<=57) //Numeros
		//|| (e.keyCode>=96 && e.keyCode<=105) //numero teclad numerico
		//|| tecla==46 	// punto
		//|| tecla==44 	// coma
		//|| tecla==188 	// comma
		||tecla==220  	// back slash
		||tecla==110 
		||tecla==39 ||tecla==37) //rigth arrow , left arrow
		
	{
		return true;
	}
	else
	{
		return false;
	}
}

function aceptarSoloNumericos(objecto, e)
{
	tecla=(document.all) ? e.keyCode : e.which;
	if (tecla ==9  //Tab
		|| (tecla >=48 && tecla<=57) //Numeros
		//|| (e.keyCode>=96 && e.keyCode<=105) //numero teclad numerico
		|| tecla==46 	// punto
		|| tecla==44 	// coma
		|| tecla==188 	// comma
		||tecla==220  	// back slash
		||tecla==110 
		||tecla==39 ||tecla==37) //rigth arrow , left arrow
		
	{
		return true;
	}
	else
	{
		return false;
	}
}


//////////////////////////////////////////////////////////////

function aceptarSoloNumeros(objecto, e)
{
	//6,934???????? !"#$%&
	tecla=(document.all) ? e.keyCode : e.which;
	//alert(tecla);
	if (tecla ==9  //Tab
		|| (tecla >=48 && tecla<=57) //Numeros
		//|| (e.keyCode>=96 && e.keyCode<=105) //numero teclad numerico
		|| tecla==46 	// punto
		|| tecla==44 	// coma
		|| tecla==188 	// comma
		||tecla==220  	// back slash
		||tecla==8  	// back slash
		||tecla==0  	// back slash
		||tecla==110 
		||tecla==39 ||tecla==37) //rigth arrow , left arrow
	{
		return true;
	}
	else
	{
		return false;
	}
}

function asignaFormatoSiesNumerico(objeto,e)
{
	tecla=(document.all) ? e.keyCode : e.which;
	if (tecla ==9  //Tab
		|| (tecla >=48 && tecla<=57) //Numeros
		|| tecla==46 	// punto
		|| tecla==44 	// coma
		)
	{
		asignarFormato(objeto);
	}
	
}

function asignarFormato(objeto)
{
	var numero = new oNumero(objeto.value.replace(/,/g,""));
	if(objeto.value.indexOf(".",0)>0)
	{
		
		var numDec = (objeto.value.length-1) - objeto.value.indexOf(".",0);
		if(numDec<=2)
			objeto.value= numero.formato(numDec,true);
		else
			objeto.value= numero.formato(2,true);
	}
	else
	{
			objeto.value= numero.formato(-1,true);
	}
}


//Objeto oNumero
function oNumero(numero)
{
	//Propiedades
	this.valor = numero || 0
	this.dec = -1;
	//M�todos
	this.formato = numFormat;
	this.ponValor = ponValor;
	//Definici�n de los m�todos
	function ponValor(cad)
	{
		if (cad =='-' || cad=='+') return
			if (cad.length ==0) return
			if (cad.indexOf('.') >=0)
				this.valor = parseFloat(cad);
			else
				this.valor = parseInt(cad);
	}
	function numFormat(dec, miles)
	{
		var num = this.valor, signo=3, expr;
		var cad = ""+this.valor;
		var ceros = "", pos, pdec, i;
		for (i=0; i < dec; i++)
			ceros += '0';

		pos = cad.indexOf('.')
		if (pos < 0)
		{
				cad = cad+"."+ceros;
		}
		else
		{
			pdec = cad.length - pos -1;
			if (pdec <= dec)
			{
				for (i=0; i< (dec-pdec); i++)
					cad += '0';
			}
			else
			{
				num = num*Math.pow(10, dec);
				num = Math.round(num);
				num = num/Math.pow(10, dec);
				cad = new String(num);
			}
		}
		//alert("Valorcito:"+cad)
		pos = cad.indexOf('.');

		if (pos < 0 || pos == undefined) 
			pos = cad.length;
		
		if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
			   signo = 4;
		
		if (miles && pos > signo)
			do
			{
					expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
					cad.match(expr)
					cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
			}while (cad.indexOf(',') > signo)
			
		if (dec<0) 
			cad = cad.replace(/\./,'')
		return cad;
	}
}//Fin del objeto oNumero:


/////////////////////////////////////////////////////////

function NumberFormat(num, numDec, decSep, thousandSep)
{
    var arg;
    var Dec;
    Dec = Math.pow(10, numDec); 
    if (typeof(num) == 'undefined') return; 
    if (typeof(decSep) == 'undefined') decSep = ',';
    if (typeof(thousandSep) == 'undefined') thousandSep = '.';
    if (thousandSep == '.')
     arg=/./g;
    else
     if (thousandSep == ',') arg=/,/g;
    if (typeof(arg) != 'undefined') num = num.toString().replace(arg,'');
    num = num.toString().replace(/,/g, '.'); 
    if (isNaN(num)) num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * Dec + 0.50000000001);
    cents = num % Dec;
    num = Math.floor(num/Dec).toString(); 
    if (cents < (Dec / 10)) cents = "0" + cents; 
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
     num = num.substring(0, num.length - (4 * i + 3)) + thousandSep + num.substring(num.length - (4 * i + 3));
    if (Dec == 1)
     return (((sign)? '': '-') + num);
    else
     return (((sign)? '': '-') + num + decSep + cents);
} 

function EvaluateText(cadena, obj)
{
    opc = false; 
    if (cadena == "%d")
     if (event.keyCode > 47 && event.keyCode < 58)
      opc = true;
    if (cadena == "%f")
	{ 
     if (event.keyCode > 47 && event.keyCode < 58)
      opc = true;
     if (obj.value.search("[.*]") == -1 && obj.value.length != 0)
      if (event.keyCode == 46)
       opc = true;
    }
    if(opc == false)
     event.returnValue = false; 
}


/////////////////////////////////////////////////////////////





function validaFormaXid(id) 
{
	var error = false;
	
	 $("input").each(function(index, domEle){
			
				domEle.style["background-color"] = "#FFF";//domEle.setAttribute("background-color", "#F00");

	 }); 
	 
	 //valida quer no lleve en los campos comillas o comillas simples
	 $("input").each(function(index, domEle){
			
			if (domEle.value.search(/"/i) != -1 || domEle.value.search(/'/i) != -1) 
			{
				domEle.style["background-color"] = "#F00";//domEle.setAttribute("background-color", "#F00");
				error = true;
			}
	 }); 
	
	if(error)
	{
		alert("Favor de revisar y evitar colocar doble comillas o comillas simples");
		return 	error;
	}
	
	$('.required').each(function(index, domEle){
							if (domEle.value == "") 
							{
								domEle.style["background-color"] = "#F00";
								error = true;
							}
	});
	
	if(error)
	{
		alert("Los datos marcado en rojo son requeridos.");
		return 	error;
	}
	
	
	
	if(error==false)
	{
		//alert(id);
		var email_array= $('.email');
		///alert(email_array.length);
		if(email_array.length>0)
		{
			//alert("asassdsdf");
			$('.email').each(function(index, domEle)
			{
					            				if ((domEle.value.indexOf(".") > 2) && (domEle.value.indexOf("@") > 0)) 
												{
													error = true;
                									domEle.style["background-color"] = "#F00";
            									}
        										});
		}
		
		if(error)
		{
			alert("No es un email valido.");
			return 	error;
		}
		else
		{
			var numerics_array=  $('.numeric')//('input.numeric');
			if(numerics_array.length>0)
			{
       			 $('.numeric').each(function(index, domEle){
						if (domEle.value == "") 
						{
							error = true;
							domEle.style["background-color"] = "#F00";
						}
						if(error==false)
						{
          	 		 		var strChars = "0123456789.-";
							for (i = 0; i < domEle.value.length; i++) 
							{
							
               	 				strChar = domEle.value.charAt(i);
               	 				if (strChars.indexOf(strChar) == -1 ) 
				 				{
                	    			error = true;
                 	   				domEle.style["background-color"] = "#F00";
              	  				}
          	  				}
						}
      		 		});
			}
			
			
			if(error)
			{
				alert("No es valor numerico valido");
				return 	error;
			}
			else
			{
					var alpha_array=  $('.alpha')//('input.alpha');
					if(alpha_array.length>0)
					{
						 $('.alpha').each(function(index, domEle){
								if (domEle.value == "") 
								{
									error = true;
									domEle.style["background-color"]="#F00";//background = "#F00";
								}
								
								if(error)
								{
									alert("A caray Favor de llenar los datos sombreados.");
									return 	error;
								}
								
								if(!error)
								{
									var strChars = "qwertyuiopasdfghjkl�zxcvbnmQWERTYUIOP�LKJHGFDSAZXCVBNM.-,1234567890 ";
									for (i = 0; i < domEle.value.length; i++) 
									{
									
										strChar = domEle.value.charAt(i);
										if (strChars.indexOf(strChar) == -1 ) 
										{
											error = true;
											domEle.style.style["background-color"]="#F00";//background = "#F00";
										}
									}
								}
							});
					}
					
					if(error)
					{
						alert("Favor de llenar los datos sombreados.");
						return 	error;
					}
			}
		}
	}
	
		
	return false;

}


    $(document).ready(function() {  
         $(".botonExcel").click(function(event) {  
         $("#datos_a_enviar").val( $("<div>").append( $("#bodyF").eq(0).clone()).html());  
         $("#FormularioExportacion").submit();  
    });  
    });  
