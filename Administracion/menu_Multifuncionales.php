<?php
	require_once("../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$multifuncionales= null;
	$area=null;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		$command= "SELECT * FROM multifuncionales";	
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$multifuncionales[$i]['id'] =$lrow['id'];
				$multifuncionales[$i]['nombre'] =$lrow['nombre'];
				$multifuncionales[$i]['ip'] =$lrow['ip'];
				$multifuncionales[$i]['descripcion'] =$lrow['descripcion'];
				$multifuncionales[$i]['area'] =$lrow['area'];
				$multifuncionales[$i]['documentpath'] =$lrow['documentpath'];
				$i++;
			}
		}
		
		//Obtiene los catalagos de area
		 $command= "SELECT idarea, nomarea FROM CatArea ";//WHERE cat ='1190'
		 $catareas = sqlsrv_query( $conexion, $command);
		if( $catareas === false)
		{
			echo "Error in executing statement 3.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		if(sqlsrv_has_rows($catareas))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $catareas, SQLSRV_FETCH_ASSOC))
			{
				$area[$i]['scat'] =$lrow['idarea'];
				$area[$i]['descripcion'] =$lrow['nomarea'];
				$i++;
			}
		}
	}
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Notificaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="body.css">-->
<link href="../../include/estilos.css" rel="stylesheet" type="text/css">		
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>

<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../javascript/prototype.js"></script>
<script language="javascript" src="../javascript/funciones.js"></script>
<script language="javascript" src="javascript/funciones_Multifuncionales.js"></script>
</head>

<body >
<form id="form_notificaciones"  name ="form_notificaciones" style="width:100%"><!-- method='POST' action="php_ajax/upload_imagendeestudiofisico.php" enctype='multipart/form-data'>-->
<table width="100%">
	<tr> <td colspan="4" align="center" >Administración de Multifuncionales <b><?php echo $usuario;?></b> <input type="hidden" value="<?php echo $usuario;?>" id="usuario" name="usuario"></td>
	</tr>
	<tr> <td colspan="4" align="center" class="texto8">.</td></tr>
  	<tr>
		<td colspan="4" align="center">
			<table border="1" id="tmenus" name="tmenus" width="100%">
				<tr>
					<th class="subtituloverde" width="14%">Nombre</th>
					<th class="subtituloverde" width="14%">Descripcion</th>
					<th class="subtituloverde" width="14%">IP</th>
					<th class="subtituloverde" width="14%">Path</th>
					<th class="subtituloverde" width="14%">Area</th>
					<th class="subtituloverde" width="10%"></th>
					
				</tr>
				<tbody id="menus">
					<?php
						/*
							$multifuncionales[$i]['id'] =$lrow['id'];
							$multifuncionales[$i]['nombre'] =$lrow['nombre'];
							$multifuncionales[$i]['ip'] =$lrow['ip'];
							$multifuncionales[$i]['area'] =$lrow['area'];
						*/  
					if( $multifuncionales!=null) for($j=0;$j<count($multifuncionales);$j++){ ?>
						<tr id="filaOculta" >
						<td width="14%"><input type="text" name="nombre"  id="nombre" align="middle" maxlength="50" class="caja_toprint" style="width:100%" value="<?php echo ltrim(rtrim($multifuncionales[$j]['nombre'])); ?>"></td>
						<td width="14%"><input type="text" name="descripcion"  id="descripcion" align="middle" maxlength="500" class="caja_toprint" style="width:100%" value="<?php echo ltrim(rtrim($multifuncionales[$j]['descripcion'])); ?>"></td>
						<td width="14%"><input type="text" name="ip"  id="ip" align="middle" maxlength="15" class="caja_toprint" style="width:100%" value="<?php echo ltrim(rtrim($multifuncionales[$j]['ip'])); ?>"></td>
						<td width="14%"><input type="text" name="documentpath"  id="documentpath" align="middle" maxlength="2000" class="caja_toprint" style="width:100%" value="<?php echo ltrim(rtrim($multifuncionales[$j]['documentpath'])); ?>"></td>
						<td width="14%"><select name="area"  id="area" class="caja_entrada" style="width:100%">
						<?php
								for($k=0;$k<count($area);$k++)
								{
									if(trim($area[$k]['scat'])==trim($multifuncionales[$j]['area']))
										echo "<option value='".$area[$k]['scat']."'  selected>".$area[$k]['scat']."- ".ltrim(rtrim($area[$k]['descripcion']))."</option>";
									else
										echo "<option value='".$area[$k]['scat']."' >".$area[$k]['scat']."- ".ltrim(rtrim($area[$k]['descripcion']))."</option>";
								}
						?>
							</select>
						</td>
						<td width="10%">
							<table width="100%">
								<tr><td align="center" width="100%"><a href="http://<?php if(strlen($multifuncionales[$j]['ip'])>0) echo $multifuncionales[$j]['ip'];?>" class="texto8" target="_blank">Estado</a>
								</td></tr>
								<tr><td align="center" width="100%"><input type="button" value="+" id="ad_colin" name="ad_colin" onClick="agrega_Multifuncional();" >
										<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_Multifuncional(event,this);" >
										<input type="hidden" id="id_multifuncional" name="id_multifuncional" value="<?php echo $multifuncionales[$j]['id'];?>">
								</td></tr>
							</table>
						</td>
					</tr>
					<?php }else{ ?>
						<tr id="filaOculta" >
						<td width="14%"><input type="text" name="nombre"  id="nombre" align="middle" maxlength="50" class="caja_toprint" style="width:100%" value=""></td>
						<td width="14%"><input type="text" name="descripcion"  id="descripcion" align="middle" maxlength="500" class="caja_toprint" style="width:100%" value=""></td>
						<td width="14%"><input type="text" name="ip"  id="ip" align="middle" maxlength="15" class="caja_toprint" style="width:100%" value=""></td>
						<td width="14%"><input type="text" name="path"  id="path" align="middle" maxlength="200" class="caja_toprint" style="width:100%" value=""></td>
						<td width="14%"><select name="area"  id="area" class="caja_entrada" style="width:100%">
						<?php
								for($k=0;$k<count($area);$k++)
								{
									echo "<option value='".$area[$k]['scat']."' >".$area[$k]['scat']."- ".ltrim(rtrim($area[$k]['descripcion']))."</option>";
								}
						?>
							</select>
						</td>
						<td width="10%">
							<table width="100%">
								<tr><td align="center" width="100%"><a href="" class="texto8" target="_blank">Estado</a>
								</td></tr>
								<tr><td align="center" width="100%"><input type="button" value="+" id="ad_colin" name="ad_colin" onClick="agrega_Multifuncional();" >
										<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_Multifuncional(event,this);" >
										<input type="hidden" id="id_multifuncional" name="id_multifuncional" value="">
								</td></tr>
							</table>
										
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr><td align="center" width="100%"><input type="button"  value="Guardar" onClick="Guardar_Multifuncionales();" class="caja_entrada"></td></tr>
</table>
</form>

</body>
</html>
