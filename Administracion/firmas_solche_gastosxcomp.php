<?php
	require_once("../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$usuarios= null;
	$area=null;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		$command= " SELECT a.*, b.nomdir as nomdir, LTRIM(RTRIM(c.nombre))+' '+LTRIM(RTRIM(c.appat)) +' '+LTRIM(RTRIM(c.apmat)) as nombre,
  c.nomdepto
  FROM [fomeadmin].[dbo].[configdfirmas_solchegtosxcomp] a 
  LEFT JOIN nomemp.dbo.nominamdepto b ON a.depto collate DATABASE_DEFAULT=b.depto  collate DATABASE_DEFAULT
  LEFT JOIN nomemp.dbo.v_todos_activos c ON a.numemp collate DATABASE_DEFAULT =c.numemp  collate DATABASE_DEFAULT
   WHERE a.estatus<9000 ORDER BY a.nombre ASC";
   
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				if(strlen($lrow['nombre'])>0)
				{
					$usuarios[$i]['id'] =$lrow['id'];
					$usuarios[$i]['numemp'] =$lrow['numemp'];
					$usuarios[$i]['nombre'] =$lrow['nombre'];
					$usuarios[$i]['nomdir'] =$lrow['nomdir'];
					$usuarios[$i]['nomdepto'] =$lrow['nomdepto'];
				
					$i++;
				}
			}
		}
	}
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Administrador de Firmas Para Autorización de Solicitudes de Cheque de Gastos Por Comprobar</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<link type="text/css" href="../css/admin.css" rel="stylesheet">
<script language="javascript" src="../prototype/prototype.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>
<script language="javascript" src="javascript/funciones_Adminusuarios.js"></script>

</head>

<body >


<form id="form_notificaciones"  name ="form_notificaciones" style="width:100%">
<input type="hidden" name="tabla_firmas" id="tabla_firmas" value="configdfirmas_solchegtosxcomp">
<table width="100%">
<tr>
    <td colspan="8" align="center" class="TituloDForma">Administrador de Firmas Para Autorización de Solicitudes de Cheque de Gastos Por Comprobar<hr class="hrTitForma">	<input type="hidden" value="<?php echo $usuario;?>" id="usuario" name="usuario"></td>
</tr>
	 </td>
	</tr>
	<tr>
    <td width="250px" height="39" class="texto8">Nombre/N&uacute;mero de Empleado: </td>
    <td width="350px"> 
      <div align="left" style="z-index:1; position:absolute; width:350px; top: 55px; left:200px;">     
        <input name="provname1" type="text" class="texto8" id="provname1" style="width:250px;" tabindex="4"  onKeyUp="searchProveedorSolche(this);" value="<?php echo $nombre;?>" size="60" autocomplete="off">        
        <div id="search_suggestProv" style="z-index:2;" > </div>
		</div>
		</div>  
        <input  type="hidden" id="nomemp" name="nomemp">
        <input  type="hidden" id="numemp" name="numemp">
        <input  type="hidden" id="dir" name="dir">
        <input  type="hidden" id="depto" name="depto">
        </td>
 <!--     <td width="128"><a href="usuarios.php" class="texto9"></a></td>-->
      <td width="102px"><a href="#" class="button" onClick="usuario_add_aut_solche()">Agregar Usuario</a></td>
      <td></td>
<!--      <td width="58"> <input name="button" type="button" class="texto8" onClick="busqueda();" value="Buscar"></td>-->
  </tr>
  	<tr>
		<td colspan="4" align="center">
			<table border="1" id="thetable" name="thetable">
				<tr>
					<th class="subtituloverde" width="40px">Num. Empleado</th>
					<th class="subtituloverde" width="130px">Nombre</th>
					<th class="subtituloverde" width="130px">Departamento</th>
                    <th class="subtituloverde" width="130px">Direcci&oacute;n</th>
					<th class="subtituloverde" width="100px"></th>
				</tr>
				<tbody name="bodyRes" id="bodyRes">
					<?php  if( $usuarios!=null) for($j=0;$j<count($usuarios);$j++){ ?>
						<tr id="filaOculta" >
						<td align="center"><?php echo ltrim(rtrim($usuarios[$j]['numemp'])); ?></td>
						<td ><?php echo utf8_decode(ltrim(rtrim($usuarios[$j]['nombre']))); ?></td>
						<td >
							<?php echo ltrim(rtrim($usuarios[$j]['nomdepto']));?>
						</td>
                        <td>
                        	<?php echo ltrim(rtrim($usuarios[$j]['nomdir']));?>
                        </td>
						<td align="center" >
                        	<img src="../imagenes/delete.png" id="rem_colin" name="rem_colin" title="Eliminar" onClick="usuario_quitar_Para_aut_solche(event,this);" valor="<?php echo $usuarios[$j]['id'];?>">
							<input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $usuarios[$j]['id'];?>">
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
</table>
</form>

</body>
</html>
