<?php

$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

function redondear_dos_decimal($valor) { 
	$intvalor = $valor;
	$float_redondeado=0.00;
	if(( $valor - (int)$intvalor)>0 && ($valor - (int)$intvalor)<1 )
   		$float_redondeado = round($valor * 100.00) / 100.00; 
	else
		$float_redondeado=$valor;
   return $float_redondeado; 
} 

function FormatErrors( $errors )
	{
    	/* Display errors. */
	    $error= "Error information: <br/>";

	    foreach ( $errors as $error )
    	{
        	 $error.=   "SQLSTATE: ".$error['SQLSTATE']."<br/>";
	         $error.=   "Code: ".$error['code']."<br/>";
	         $error.=   "Message: ".$error['message']."<br/>";
	    }
		return  $error;
	}

function pegarGeoeferencia($target_path)
{
	//echo "$target_path:<br />\n";
	$exif = exif_read_data($target_path, 'IFD0');
	if(!$exif)
		return "No se encontr� informaci�n de cabecera Para generar la marca de agua de Georeferenciaci�n.<br />\n";
	//echo $exif===false ? exit("No se encontr� informaci�n de cabecera.<br />\n") : "La imagen contiene cabeceras<br />\n";
	
	$exif = exif_read_data($target_path, 0, true);
	//echo "prueba2.jpg:<br />\n";
	foreach ($exif as $clave => $secci�n)
	 {
		foreach ($secci�n as $nombre => $valor)
		 {
		   //echo "$clave.$nombre: $valor<br />\n";
			if($nombre=='Height')
			{
				$height=$valor;
				//echo $height;
			}
			if($nombre=='Width')
			{			
				$Width=$valor;
				//echo $Width;
			}
			if($nombre=='DateTimeOriginal')
				$time=$valor;
			if($nombre=='GPSLongitude')
			{
				//print_r($valor);
				//echo "<br>Latongitud";
				list($divisor, $dividendo) =  split("/", $valor[0], 2);
				$myhrs=(float)$divisor/$dividendo;
				//echo "<br>horas: ".$myhrs;
				list($divisor, $dividendo) =  split("/", $valor[1], 2);
				$mymin=(float)$divisor/$dividendo;
				//echo "<br>min: ".$mymin;
				list($divisor, $dividendo) =  split("/", $valor[2], 2);
				$myseg=(float)$divisor/$dividendo;
				//echo "<br>seg: ".$myseg;			
				$long=($myhrs+($mymin+$myseg/60)/60)*-1;
				//echo "<br>Long Decimal: ".$long;
	
			}
			if($nombre=='GPSLatitude')
			{
				//print_r($valor);
				//echo "<br>Latitud";
				list($divisor, $dividendo) =  split("/", $valor[0], 2);
				$myhrs=(float)$divisor/$dividendo;
				//echo "<br>horas: ".$myhrs;
				list($divisor, $dividendo) =  split("/", $valor[1], 2);
				$mymin=(float)$divisor/$dividendo;
				//echo "<br>min: ".$mymin;
				list($divisor, $dividendo) =  split("/", $valor[2], 2);
				$myseg=(float)$divisor/$dividendo;
				//echo "<br>seg: ".$myseg;			
				$lat=$myhrs+($mymin+$myseg/60)/60;
				//echo "<br>Lat Decimal: ".$lat;
			}
		}
	}
	
	$archivo=$target_path;//"C:/pruebas/prueba.jpg";
	$image1 = imagecreatefromjpeg($archivo);
	$image = imagecreate($Width,$height);
	$black=imagecolorallocate($image, 0, 0, 0);	
	$blue=imagecolorallocate($image, 0, 0, 255);
	$white=imagecolorallocate($image, 255, 255, 255);
	$orange = imagecolorallocate($image, 255, 128, 0);
	$red = imagecolorallocate($image, 255, 0, 0);
	$lightblue=imagecolorallocate($image, 156, 227, 254);
	
	$cen = ($Width )/2;
	if ($cen < 1)
	{ 
		$cen = 1;
	}
	imagestring($image, 100, 10, 0, "latitud:".$lat."   Longitud:".$long , $white);
	imagestring($image, 100, 10, 20, "Fecha:".$time , $white);
	//ImageFilledRectangle($image,10,10,50,50,$blue);
	imagejpeg($image,$target_path); 
	imagedestroy($image);
	$image=imagecreatefromjpeg($target_path);
	imagecopymerge($image1, $image,0, 0, 0, 0,600,200, 50);	
	imagejpeg($image1,$archivo); 
	//$image = imagecreatefrompng("C:/pruebas/pba.png");	
	//imagepng($image1,$archivo);
	imagedestroy($image);
	imagedestroy($image1);
}

function num2letras($num, $fem = false, $dec = true) { 
   $matuni[2]  = "dos"; 
   $matuni[3]  = "tres"; 
   $matuni[4]  = "cuatro"; 
   $matuni[5]  = "cinco"; 
   $matuni[6]  = "seis"; 
   $matuni[7]  = "siete"; 
   $matuni[8]  = "ocho"; 
   $matuni[9]  = "nueve"; 
   $matuni[10] = "diez"; 
   $matuni[11] = "once"; 
   $matuni[12] = "doce"; 
   $matuni[13] = "trece"; 
   $matuni[14] = "catorce"; 
   $matuni[15] = "quince"; 
   $matuni[16] = "dieciseis"; 
   $matuni[17] = "diecisiete"; 
   $matuni[18] = "dieciocho"; 
   $matuni[19] = "diecinueve"; 
   $matuni[20] = "veinte"; 
   $matunisub[2] = "dos"; 
   $matunisub[3] = "tres"; 
   $matunisub[4] = "cuatro"; 
   $matunisub[5] = "quin"; 
   $matunisub[6] = "seis"; 
   $matunisub[7] = "sete"; 
   $matunisub[8] = "ocho"; 
   $matunisub[9] = "nove"; 

   $matdec[2] = "veint"; 
   $matdec[3] = "treinta"; 
   $matdec[4] = "cuarenta"; 
   $matdec[5] = "cincuenta"; 
   $matdec[6] = "sesenta"; 
   $matdec[7] = "setenta"; 
   $matdec[8] = "ochenta"; 
   $matdec[9] = "noventa"; 
   $matsub[3]  = 'mill'; 
   $matsub[5]  = 'bill'; 
   $matsub[7]  = 'mill'; 
   $matsub[9]  = 'trill'; 
   $matsub[11] = 'mill'; 
   $matsub[13] = 'bill'; 
   $matsub[15] = 'mill'; 
   $matmil[4]  = 'millones'; 
   $matmil[6]  = 'billones'; 
   $matmil[7]  = 'de billones'; 
   $matmil[8]  = 'millones de billones'; 
   $matmil[10] = 'trillones'; 
   $matmil[11] = 'de trillones'; 
   $matmil[12] = 'millones de trillones'; 
   $matmil[13] = 'de trillones'; 
   $matmil[14] = 'billones de trillones'; 
   $matmil[15] = 'de billones de trillones'; 
   $matmil[16] = 'millones de billones de trillones'; 
   
   //Zi hack
   $float=explode('.',$num);
   $num=$float[0];

   $num = trim((string)@$num); 
   if ($num[0] == '-') { 
      $neg = 'menos '; 
      $num = substr($num, 1); 
   }else 
      $neg = ''; 
   while ($num[0] == '0') $num = substr($num, 1); 
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
   $zeros = true; 
   $punt = false; 
   $ent = ''; 
   $fra = ''; 
   for ($c = 0; $c < strlen($num); $c++) { 
      $n = $num[$c]; 
      if (! (strpos(".,'''", $n) === false)) { 
         if ($punt) break; 
         else{ 
            $punt = true; 
            continue; 
         } 

      }elseif (! (strpos('0123456789', $n) === false)) { 
         if ($punt) { 
            if ($n != '0') $zeros = false; 
            $fra .= $n; 
         }else 

            $ent .= $n; 
      }else 

         break; 

   } 
   $ent = '     ' . $ent; 
   if ($dec and $fra and ! $zeros) { 
      $fin = ' coma'; 
      for ($n = 0; $n < strlen($fra); $n++) { 
         if (($s = $fra[$n]) == '0') 
            $fin .= ' cero'; 
         elseif ($s == '1') 
            $fin .= $fem ? ' una' : ' un'; 
         else 
            $fin .= ' ' . $matuni[$s]; 
      } 
   }else 
      $fin = ''; 
   if ((int)$ent === 0) return 'Cero ' . $fin; 
   $tex = ''; 
   $sub = 0; 
   $mils = 0; 
   $neutro = false; 
   while ( ($num = substr($ent, -3)) != '   ') { 
      $ent = substr($ent, 0, -3); 
      if (++$sub < 3 and $fem) { 
         $matuni[1] = 'una'; 
         $subcent = 'as'; 
      }else{ 
         $matuni[1] = $neutro ? 'un' : 'uno'; 
         $subcent = 'os'; 
      } 
      $t = ''; 
      $n2 = substr($num, 1); 
      if ($n2 == '00') { 
      }elseif ($n2 < 21) 
         $t = ' ' . $matuni[(int)$n2]; 
      elseif ($n2 < 30) { 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      }else{ 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      } 
      $n = $num[0]; 
      if ($n == 1) { 
        if(strlen($t)==0)
		 	$t = ' cien';
		else
        	$t = ' ciento' . $t; 
      }elseif ($n == 5){ 
         $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
      }elseif ($n != 0){ 
         $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
      } 
      if ($sub == 1) { 
      }elseif (! isset($matsub[$sub])) { 
         if ($num == 1) { 
            $t = ' mil'; 
         }elseif ($num > 1){ 
            $t .= ' mil'; 
         } 
      }elseif ($num == 1) { 
         $t .= ' ' . $matsub[$sub] . '?n'; 
      }elseif ($num > 1){ 
         $t .= ' ' . $matsub[$sub] . 'ones'; 
      }   
      if ($num == '000') $mils ++; 
      elseif ($mils != 0) { 
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
         $mils = 0; 
      } 
      $neutro = true; 
      $tex = $t . $tex; 
   } 
   $tex = $neg . substr($tex, 1) . $fin; 
   //Zi hack --> return ucfirst($tex);
   $end_num = ucfirst($tex).' pesos '.$float[1].'/100 M.N.';
   return $end_num; 
} 


function ascii_to_entities($str) 
    { 
       $count    = 1; 
       $out    = ''; 
       $temp    = array(); 
    
       for ($i = 0, $s = strlen($str); $i < $s; $i++) 
       { 
           $ordinal = ord($str[$i]); 
    
           if ($ordinal < 128) 
           { 
                if (count($temp) == 1) 
                { 
                    $out  .= '&#'.array_shift($temp).';'; 
                    $count = 1; 
                } 
            
                $out .= $str[$i]; 
           } 
           else 
           { 
               if (count($temp) == 0) 
               { 
                   $count = ($ordinal < 224) ? 2 : 3; 
               } 
        
               $temp[] = $ordinal; 
        
               if (count($temp) == $count) 
               { 
                   $number = ($count == 3) ? (($temp['0'] % 16) * 4096) + 
(($temp['1'] % 64) * 64) + 
($temp['2'] % 64) : (($temp['0'] % 32) * 64) + 
($temp['1'] % 64); 

                   $out .= '&#'.$number.';'; 
                   $count = 1; 
                   $temp = array(); 
               } 
           } 
       } 

       return $out; 
    } 


function convertirFechaEuropeoAAmericano($fecha)
{
	
	list($dd, $mm, $aaaa) = explode("/", $fecha);
	return $aaaa."/".$mm."/".$dd;
}

function get_Privilegios($usuario, $nivel)
{
	global $username_db, $password_db, $odbc_name, $server;

	$idPantalla=$nivel;
	$privilegios = array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$SAF_Privilegios=get_SAF_Privilegios();
	if($conexion )
	{
		$consulta="SELECT a.saf_privilegios_idsafprivilegios, a.estatus FROM saf_menudnivel_x_saf_privilegios a 
					LEFT JOIN menudnivel b on b.ID=a.menudnivel_idmenudnivel	
					LEFT JOIN menummenu c ON c.nivel=b.nivel
					WHERE a.usuario='$usuario'  AND c.nivel=$idPantalla";
		//echo $consulta;
		$stmt = sqlsrv_query( $conexion, $consulta);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n". $consulta;
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			//$i=0;
			if(sqlsrv_has_rows($stmt))
			{
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
				{
					for($j=0;$j<count($SAF_Privilegios);$j++)
					{
						//echo $SAF_Privilegios[$j]['privilegio']."==".$row['saf_privilegios_idsafprivilegios']."<br>";
						if($SAF_Privilegios[$j]['privilegio']==$row['saf_privilegios_idsafprivilegios'])
						{
							$SAF_Privilegios[$j]['estatus'] = $row['estatus'];
							//echo 
						}
					}
					//$i++;
				}
			}
		}
	}	
	
	return $SAF_Privilegios;
}


function get_SAF_Privilegios()
{
	global $username_db, $password_db, $odbc_name, $server;

	//$idPantalla=$nivel;
	$privilegios = array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	if($conexion )
	{
		$consulta="SELECT sp2.id, sp2.nombre, 0 as estatus, descripcion FROM saf_privilegios  sp2 where estatus<90 ORDER BY id ASC";
					
		///$commando="SELECT req FROM menumusuarios WHERE usuario='$usuario'";
		//echo $commando."<br>";
		$stmt = sqlsrv_query( $conexion, $consulta);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n". $consulta;
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			$i=0;
			if(sqlsrv_has_rows($stmt))
			{
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
				{
					$privilegios[$i]['privilegio'] = $row['id'];
					$privilegios[$i]['estatus'] = $row['estatus'];
					$privilegios[$i]['nombre'] = $row['nombre'];
					$privilegios[$i]['descripcion'] = $row['descripcion'];
					$i++;
				}
			}
		}
	}
	return $privilegios;
}

function revisaPrivilegiosBorrar($privilegios)
{
	$borrar=false;
	for($i=0 ; $i<count($privilegios) && $borrar==false ; $i++)
	{
		if($privilegios[$i]['privilegio']==1 && $privilegios[$i]['estatus']==0)
		{
			$borrar=true;
		}
	}
	return $borrar;
}

function revisaPrivilegiosPDF($privilegios)
{
	$borrar=false;
	for($i=0 ; $i<count($privilegios) && $borrar==false ; $i++)
	{
		if($privilegios[$i]['privilegio']==2 && $privilegios[$i]['estatus']==0)
		{
			$borrar=true;
		}
	}
	return $borrar;
}

function revisaPrivilegioseditar($privilegios)
{
	$borrar=false;
	for($i=0 ; $i<count($privilegios) && $borrar==false ; $i++)
	{
		if($privilegios[$i]['privilegio']==3 && $privilegios[$i]['estatus']==0)
		{
			$borrar=true;
		}
	}
	return $borrar;
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function get_FieldsOfSimpleQuery($table, $datas, $order, $where)
{
	global $username_db, $password_db, $odbc_name, $server;

	$privilegios = array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	if($conexion )
	{
		$consulta="SELECT  $datas  FROM $table ";
		
		if(strlen($order)>0)
		{
			$consulta.=" ".$order;
		}
		
		if(strlen($where)>0)
		{
			$consulta.=" ".$where;
		}
		//echo $consulta;
		$stmt = sqlsrv_query( $conexion, $consulta);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n". $consulta;
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			//$i=0;
			if(sqlsrv_has_rows($stmt))
			{
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
				{
					array_push($privilegios,$row );
				}
			}
		}
	}	
	
	return $privilegios;
}

?>