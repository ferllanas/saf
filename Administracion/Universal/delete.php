<?php
include_once('../../conexion/validaSession.php');

$FYidioma=$_COOKIE['FYidioma'];
$idioma=$_COOKIE['idioma'];
require("../../idiomas/$idioma.php");
require("../../config/conf.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$datos=array();
$datos['error']=0;
$datos['msg']="";
$updateStr="";
$insertItemsStr="";
$insertValuesStr="";
			
$id="";
if(isset($_POST['id']))
 $id=$_POST['id'];
 
$Dev_table_name="";
if(isset($_POST['Dev_table_name']))
 $Dev_table_name=$_POST['Dev_table_name'];
 
$PRI="";
if(isset($_POST['PRI']))
 $PRI=$_POST['PRI'];

$conn = new mysqli($mysql_host, $mysql_user, $mysql_pass, $FuerzaYa_DB );
if ($conn->connect_errno) {
	$datos['error']=1;
	$datos['msg']= "Falló la conexión a MySQLi";//"Falló la conexión a MySQLi: (" . $conn->connect_errno . ") " . $conn->connect_error;
	throw new Exception($conn->connect_error);
}

$query="";
try{
	$conn->autocommit(FALSE); // i.e., start transaction
	
	
	$group_id = $conn->insert_id; // last auto_inc id from *this* connection
	$updateStr=substr($updateStr, 0, -1);
	$query = "UPDATE ".$_POST['Dev_table_name']." SET estatus=90 WHERE ".$_POST['PRI']."=".$id;
	$result = $conn->query($query);
	if ( !$result ) {
		//$result->free();
		throw new Exception($conn->error);
	}
    // our SQL queries have been successful. commit them
    // and go back to non-transaction mode.
	$datos['id']= $PRIValue;
    $conn->commit();
    $conn->autocommit(TRUE); // i.e., end transaction
}
catch ( Exception $e ) {

    // before rolling back the transaction, you'd want
    // to make sure that the exception was db-related
	//print_r($e);
	$datos['error']= 2;
	$datos['msg']= $e->getMessage()."  ".$query;
	$datos['id']= $PRIValue;
    $conn->rollback(); 
    $conn->autocommit(TRUE); // i.e., end transaction   
}


echo json_encode($datos);

?>