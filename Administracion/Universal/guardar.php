<?php
include_once('../../conexion/validaSession.php');

$FYidioma=$_COOKIE['FYidioma'];
$idioma=$_COOKIE['idioma'];
require("../../idiomas/$idioma.php");
require("../../config/conf.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$datos=array();
$datos['error']=0;
$datos['msg']="";
$updateStr="";
$insertItemsStr="";
$insertValuesStr="";
			
if(isset($_POST)){
	$PRIValue=$_POST[$_POST['PRI']];
	foreach ($_POST as $clave => $valor) {
		
		if( $_POST['PRI']!==$clave  && 'Dev_table_name'!==$clave &&  'PRI'!==$clave){
			$updateStr.=" $clave='$valor' ,";
			$insertItemsStr.=" $clave,";
			$insertValuesStr.=" '$valor',";
		}
	}
	
}



$conn = new mysqli($mysql_host, $mysql_user, $mysql_pass, $FuerzaYa_DB );
if ($conn->connect_errno) {
	$datos['error']=1;
	$datos['msg']= "Falló la conexión a MySQLi";//"Falló la conexión a MySQLi: (" . $conn->connect_errno . ") " . $conn->connect_error;
	throw new Exception($conn->connect_error);
}

try{
	$conn->autocommit(FALSE); // i.e., start transaction
	
	if($PRIValue<=0)
	{
		$insertValuesStr=substr($insertValuesStr, 0, -1);
		$insertItemsStr=substr($insertItemsStr, 0, -1);
		//rtrim($insertValuesStr, ",");
		//rtrim($insertItemsStr, ",");
		// assume that the TABLE groups has an auto_increment id field
		
		$query = "INSERT INTO ".$_POST['Dev_table_name']."(".$insertItemsStr.") ";
		$query .= "VALUES (".$insertValuesStr.")";
		$result = $conn->query($query);
		if ( !$result ) {
			//$result->free();
			throw new Exception($conn->error);
		}
		
		$PRIValue = $conn->insert_id; // last auto_inc id from *this* connection
		$datos['id']= $PRIValue;
	}
	else
	{
		$group_id = $conn->insert_id; // last auto_inc id from *this* connection
		$updateStr=substr($updateStr, 0, -1);
		$query = "UPDATE ".$_POST['Dev_table_name']." SET ".$updateStr." WHERE ".$_POST['PRI']."=".$PRIValue;
		$result = $conn->query($query);
		if ( !$result ) {
			//$result->free();
			throw new Exception($conn->error);
		}
	}
    // our SQL queries have been successful. commit them
    // and go back to non-transaction mode.
	$datos['id']= $PRIValue;
    $conn->commit();
    $conn->autocommit(TRUE); // i.e., end transaction
}
catch ( Exception $e ) {

    // before rolling back the transaction, you'd want
    // to make sure that the exception was db-related
	//print_r($e);
	$datos['error']= 2;
	$datos['msg']= $e->getMessage();
	$datos['id']= $PRIValue;
    $conn->rollback(); 
    $conn->autocommit(TRUE); // i.e., end transaction   
}


echo json_encode($datos);

?>