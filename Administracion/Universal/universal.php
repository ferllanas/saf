<?php
include_once('../../conexion/validaSession.php');
include('funtionsUniversal.php');

$Dev_table_name="canvas";
if(isset($_REQUEST['Dev_table_name']))
	$Dev_table_name=$_REQUEST['Dev_table_name'];
//else
//	echo "Error no obtuvo nombre de la tabla.";

$Dev_action_form="list";
if(isset($_REQUEST['Dev_action_form']))
	$Dev_action_form="Dev_action_form";
//else
//	echo "Error el tipo de la forma.";
	
$FYidioma=$_COOKIE['FYidioma'];
$idioma=$_COOKIE['idioma'];
require("../../idiomas/$idioma.php");
require("../../config/conf.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
			
$COLUMNS = getColumns($Dev_table_name);	
$PRIOfTable=getPRIMARYKEYfromArrayColumnsTable($COLUMNS);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en" class="no-js">
	<head>
		<meta content="text/html; charset=UTF-8"/> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        
        
		<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>ResponsiveMultiLevelMenu/css/default.css" />
        
		<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>ResponsiveMultiLevelMenu/css/component.css" />
		<script 								src="<?php echo getDirAtras(getcwd());?>ResponsiveMultiLevelMenu/js/modernizr.custom.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>javascript/jqueryGlobalFunctions.js"></script>
        
        
		<script src="<?php echo getDirAtras(getcwd());?>ResponsiveMultiLevelMenu/js/jquery.dlmenu.js"></script>
		<script type="text/javascript" src="../../../jquery-cookie-master/jquery.cookie.js"></script>
		<script>
			$(function() {
			  
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
				
				$(".delete_Item").click(function(){
					
					var strValue=$(this).attr('value');
					var res=strValue.split(",");
					
					var datas = {
						 id: res[1],
						 Dev_table_name: res[0],
						 PRI: res[2]
					};
					
					 $.ajax({
					type: "POST",
					url: "delete.php",
					data: datas,
					dataType: "json",
					success: function(data) {
						console.log(data);
						if(data.error>0){
							alert(data.msg);							
						}
						else
						{
							//alert(data[0]['id']);
							//alert(data['id']);
							//alert(data.id);
							window.location.href = 'universal.php?Dev_table_name=<?php echo $Dev_table_name;?>';
						}
					},
					error: function(){
						  alert('error handing here');
					}
				});
				});
			});
		</script>
<link rel="stylesheet" type="text/css" href="../../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<link rel="stylesheet" href="<?php echo getDirAtras(getcwd());?>css/fuerzaya.css" />
<title><?php echo $idioma_AdministradorCanvas; ?></title>
</head>
<?php include_once(getDirAtras(getcwd())."indexMenu.php");?>
<body>

<table width="100%">
	<tr>
    	<td width="100%"><?php echo  $idioma_AdministradorCanvas;?><hr /></td>
    </tr>
</table>
<table>
	<tr>
    	<td><input type="button" value="<?php echo $idioma_crearNuevTipo;?>"  onclick="javascript: location.href='editar.php?Dev_table_name=<?php echo $Dev_table_name;?>&Dev_action_form=editar&id=0'"/></td>
    </tr>
</table>
<table id="thetable" cellspacing="0" align="center">
	<thead>
    	<tr>
        	<?php foreach( $COLUMNS as $Flieds)
			{
				//print_r($Flieds);
				$XtrasArray=array();
				if(strlen($Flieds['Comment'])>0 )
					$XtrasArray= eval("return ".$Flieds['Comment'].";");
				
				if(isset($XtrasArray['edicion']) && $XtrasArray['edicion']=="true")
				{
				?>
                <th align="center"
                 <?php 
				 	
					if(strpos($Flieds['Type'],"bigint")!==FALSE)
						echo "width='40px'";
					
					if(strpos($Flieds['Type'], "varchar")!==FALSE)
					{
						$cantChar=(float)getValueFromParentesis($Flieds['Type']);
						if($cantChar<100 && $cantChar>80)
							echo "width='180px'";
						else
							if($cantChar>50 && $cantChar<80)
								echo "width='90px'";
							else
								if($cantChar<50  && $cantChar>0)
									echo "width='90px'";
								else
									if($cantChar>100)
										echo "width='200px'";
										
					}
					
					if(strpos($Flieds['Type'], "text")!==FALSE)
					{
						echo "width='200px'";
					}
					
					if(strpos($Flieds['Type'], "integer")!==FALSE)
					{
						echo "width='50px'";
					}
					
					if(strpos($Flieds['Type'], "int")!==FALSE)
					{
						echo "width='50px'";
					}
					
					if(strpos($Flieds['Type'], "decimal")!==FALSE)
					{
						echo "width='100px'";
					}
					
					?>
                 ><?php echo eval("return ". $XtrasArray['variable'].";");?></th>
                <?php
				}
			}?>
             <th align="center">&nbsp;</th> 
            <th align="center">&nbsp;</th > 
        </tr>
    </thead>
    <tbody>
    <?php
		
		$conexion = mysql_connect($mysql_host, $mysql_user, $mysql_pass);
		
		if(!$conexion)
			die("No pudo conectarse a la base de datos.");
			
   		if(!mysql_select_db($FuerzaYa_DB, $conexion))
			die("La base de datos $FuerzaYa_DB no existe.");
			
	
		$consulta = "SELECT * FROM $Dev_table_name WHERE id_idiomas=$FYidioma AND estatus<90";
		$rs3 = mysql_query($consulta, $conexion) or die(mysql_error());
		$i=0;
		while( $row = mysql_fetch_assoc( $rs3))
		{
			?>
			<tr <?php if($i==0) echo 'class="first"';?>>
			<?php
			foreach( $COLUMNS as $Flieds)
			{
				$XtrasArray=array();
				if(strlen($Flieds['Comment'])>0 )
						$XtrasArray= eval("return ".$Flieds['Comment'].";");
				
				//echo $Flieds['Field'].",".strlen($XtrasArray['related']);
				if(strpos($Flieds['Field'], "_")>0 && strlen($XtrasArray['related'])>0) 
				{
					if(strlen($XtrasArray['related'])>0)
					{
						list($TRelated, $FRelated)=explode("_", $Flieds['Field']);
						
						//Obtiene renglones de la tabla relacionada
						$ValuesToRelated=getRowsEspecificColumns($TRelated, $FRelated);
						//print_r($ValuesToRelated);
						if(count($ValuesToRelated)>0)
						{
							//Obtiene renglones el valos de las columnas que deben ser mostradas en la seleccion
							$fielsToShow=array();
							if(strlen($XtrasArray['related'])>0)
								if(strpos($XtrasArray['related'], ",")!==false)
									$fielsToShow=explode(",",$XtrasArray['related']);
								else
									array_push($fielsToShow,$XtrasArray['related']);

								foreach( $ValuesToRelated as $rowRelated )
								{
									if($rowRelated[$FRelated]===$row[$Flieds['Field']])		
									{	
										if(count($fielsToShow)>0)
										{
												foreach( $fielsToShow as $fieldS ){
												?>	
												<td align="center">	<?php echo $rowRelated[$fieldS];?>	</td>
                                                <?php
												}
										}
									}
								}
						}
					}
				}
				else
				{
					$XtrasArray=array();
					if(strlen($Flieds['Comment'])>0 )
						$XtrasArray= eval("return ".$Flieds['Comment'].";");
					
					if(isset($XtrasArray['edicion']) && $XtrasArray['edicion']=="true")
					{
					?>
					<td align="center"><?php echo trim($row[$Flieds['Field']]);?></td>
					<?php
					}
				}
			}
			?>
			   <td><a href="editar.php?Dev_table_name=<?php echo $Dev_table_name;?>&amp;Dev_action_form=ver&amp;id=<?php echo trim($row[$PRIOfTable]);?>"><?php echo $idioma_vereditar;?></a></td>
            <td><a href="#" class="delete_Item" value="<?php echo trim($Dev_table_name);?>,<?php echo trim($row[$PRIOfTable]);?>,<?php echo $PRIOfTable;?>">Eliminar</a></td>
			</tr>
			<?php
			$i++;
		}
	?>
    </tbody>
</table>

<!--<script type="text/javascript" src="../../googleapis/ajax/libs/ajax/jquery/1.8.1/jquery.min.js"></script>-->
<script type="text/javascript" src="../../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>

<script>
/*<![CDATA[*/

jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:500});

	//$('#thetable2').tableScroll();
});

/*]]>*/
</script>
</body>
</html>