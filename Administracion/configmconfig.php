<?php

	
	require_once("../connections/dbconexion.php");
    require_once("funcionesGenerales.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$usuarios= null;
	$area=null;
	
	$configmconfig=array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		$command= " SELECT * FROM configmconfig where estatus = 0";
   
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$configmconfig=$lrow;
			}
		}
	}
	
	$empleados = get_GeneralesDeEmpleados();
	//print_r($empleados);
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Administrador de Firmas Para Autorización de Solicitudes de Cheque</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<link type="text/css" href="../css/admin.css" rel="stylesheet">
<script language="javascript" src="../prototype/prototype.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>
<script language="javascript" src="javascript/funciones_Adminusuarios.js"></script>

</head>

<body >


<form id="form_notificaciones"  name ="form_notificaciones" action="configmconfig_guardar.php" method="post">
<input type="hidden" name="tabla" id="tabla" value="configmconfig">

<table width="100%">
    <tr>
            <td align="center" class="TituloDForma">Configuraci&oacute;n de Sistema<hr class="hrTitForma">	<input type="hidden" value="<?php echo $usuario;?>" id="usuario" name="usuario">
            </td>
    </tr>
    <tr>
    	<td>
        	<table>
            	<tr><td class="texto12">Quien Autoriza Requisici&oacute;n?:</td><td><input type="text" id="autoriza_requisi" name="autoriza_requisi" style="width:300px;" value="<?php echo $configmconfig['autoriza_requisi'];?>"><input type="hidden" id="configmconfig_id" name="configmconfig_id" value="<?php echo $configmconfig['id'];?>"></td></tr>
                <tr><td class="texto12">Visto Bueno Requisici&oacute;n?:</td><td><input type="text" id="vobo_requisi" style="width:300px;" name="vobo_requisi" value="<?php echo $configmconfig['vobo_requisi'];?>"></td></tr>
                <tr><td class="texto12">Responsable Solicitud de Cheque:</td><td>
                <select name="resp_solche" id="resp_solche" style="width:300px;"><?php for($i=0;$i<count($empleados);$i++){?>
                			<option value="<?php echo $empleados[$i]['numemp'];?>" <?php if($empleados[$i]['numemp']==$configmconfig['resp_solche']) echo "SELECTED";?>><?php echo $empleados[$i]['numemp'].".-".$empleados[$i]['nomemp'];?></option>
                <?php }
				?></select>
                </td></tr>
                <tr><td class="texto12">Prcentaje de iva:</td><td><input type="text" id="pctiva" style="width:300px;" name="pctiva" value="<?php echo $configmconfig['pctiva'];?>"></td></tr>
                <tr><td class="texto12">Responsable de Patrimonio</td><td>
                <select name="resp_patrimonio" id="resp_patrimonio" style="width:300px;"><?php for($i=0;$i<count($empleados);$i++){?>
                			<option value="<?php echo $empleados[$i]['numemp'];?>" <?php if($empleados[$i]['numemp']==$configmconfig['resp_patrimonio']) echo "SELECTED";?>><?php echo $empleados[$i]['numemp'].".-".$empleados[$i]['nomemp'];?></option>
                <?php }
				?></select>
               </td></tr>
                <tr><td class="texto12">cta_c_bco</td><td><input type="text" id="cta_c_bco" style="width:300px;" name="cta_c_bco" value="<?php echo $configmconfig['cta_c_bco'];?>"></td></tr>
                <tr><td class="texto12">Aprueba requisici&oacute;n:</td><td><input type="text" id="aprueba_requisi" style="width:300px;" name="aprueba_requisi" value="<?php echo $configmconfig['aprueba_requisi'];?>"></td></tr>
                <tr><td class="texto12">Autorizacion de Requisici&oacute;n en area:</td><td><input type="text" id="area_aut_req" style="width:300px;" name="area_aut_req" value="<?php echo $configmconfig['area_aut_req'];?>"></td></tr>
                <tr><td class="texto12">area_vobo_req</td><td><input type="text" id="area_vobo_req" style="width:300px;" name="area_vobo_req" value="<?php echo $configmconfig['area_vobo_req'];?>"></td></tr>
                <tr><td class="texto12">ajuste_vale</td><td><input type="text" id="ajuste_vale" style="width:300px;" name="ajuste_vale" value="<?php echo $configmconfig['ajuste_vale'];?>"></td></tr>
                <tr><td class="texto12">retencion</td><td><input type="text" id="retencion" style="width:300px;" name="retencion" value="<?php echo $configmconfig['retencion'];?>"></td></tr>
                <tr><td class="texto12">cta_deudores_diversos</td><td><input type="text" id="cta_deudores_diversos" style="width:300px;" name="cta_deudores_diversos" value="<?php echo $configmconfig['cta_deudores_diversos'];?>"></td></tr>
                <tr><td class="texto12">Solicitud de Transferencias:</td><td><input type="text" id="sol_transf" style="width:300px;" name="sol_transf" value="<?php echo $configmconfig['sol_transf'];?>"></td></tr>
                <tr><td class="texto12">Autorizaci&oacute;n de Transferencias:</td><td><input type="text" id="aut_transf" style="width:300px;" name="aut_transf" value="<?php echo $configmconfig['aut_transf'];?>"></td></tr>
                <tr><td class="texto12">Coordinador de Ingresos:</td><td><input type="text" id="coord_ingresos" style="width:300px;" name="coord_ingresos" value="<?php echo $configmconfig['coord_ingresos'];?>"></td></tr>
                <tr><td class="texto12">Director de Finanzas:</td><td><input type="text" id="dir_finanzas" style="width:300px;" name="dir_finanzas" value="<?php echo $configmconfig['dir_finanzas'];?>"></td></tr>
                <tr><td class="texto12">Coordinador de Egresos:</td><td><input type="text" id="coord_egresos" style="width:300px;" name="coord_egresos" value="<?php echo $configmconfig['coord_egresos'];?>"></td></tr>
                <tr><td class="texto12">Coordinador de Recursos Humanos:</td><td><input type="text" id="coord_rh" style="width:300px;" name="coord_rh" value="<?php echo $configmconfig['coord_rh'];?>"></td></tr>
                <tr><td class="texto12">Revici&oacute;n de Cuado Comparativo:</td><td><select name="resp_patrimonio" id="resp_patrimonio" style="width:300px;"><?php for($i=0;$i<count($empleados);$i++){?>
                			<option value="<?php echo $empleados[$i]['numemp'];?>" <?php if($empleados[$i]['numemp']==$configmconfig['revisa_cc']) echo "SELECTED";?>><?php echo $empleados[$i]['numemp'].".-".$empleados[$i]['nomemp'];?></option>
                <?php }
				?></select>
           		</td></tr>
                <tr><td class="texto12">Autorizaci&oacute;n de Cuadro Comparativo:</td><td><select name="resp_patrimonio" id="resp_patrimonio" style="width:300px;"><?php for($i=0;$i<count($empleados);$i++){?>
                			<option value="<?php echo $empleados[$i]['numemp'];?>" <?php if($empleados[$i]['numemp']==$configmconfig['autoriza_cc']) echo "SELECTED";?>><?php echo $empleados[$i]['numemp'].".-".$empleados[$i]['nomemp'];?></option>
                <?php }
				?></select></td></tr>
                <tr><td class="texto12">Usuario Autorizacion de Requisicion:</td><td><select name="resp_patrimonio" id="resp_patrimonio" style="width:300px;"><?php for($i=0;$i<count($empleados);$i++){?>
                			<option value="<?php echo $empleados[$i]['numemp'];?>" <?php if($empleados[$i]['numemp']==$configmconfig['usu_aut_req']) echo "SELECTED";?>><?php echo $empleados[$i]['numemp'].".-".$empleados[$i]['nomemp'];?></option>
                <?php }
				?></select></td></tr>
            </table>
        <td>
	</tr>
    
<tr>
	<td align="center"><input type="submit" value="Guardar" align="middle"></td>
</tr>
</table>
</form>

</body>
</html>
