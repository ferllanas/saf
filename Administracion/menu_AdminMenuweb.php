<?php
	require_once("../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$menus= null;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		$command= "SELECT * FROM cat_menuweb ORDER BY posicion ASC";	
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$menus[$i]['id'] =$lrow['id'];
				$menus[$i]['nombre'] =$lrow['nombre_menu'];
				//echo $menus[$i]['orientacion'];
				$menus[$i]['liga'] =$lrow['liga'];
				$menus[$i]['posicion'] =$lrow['posicion'];
				$menus[$i]['tipo'] =$lrow['tipo'];
				$menus[$i]['status'] =$lrow['status'];
				//echo $menus[$i]['status'];
				$menus[$i]['descripcion'] =$lrow['descripcion'];
				$i++;
			}
					
		}
	}
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Notificaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="body.css">-->
<link href="../../include/estilos.css" rel="stylesheet" type="text/css">		
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>

<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../javascript/prototype.js"></script>
<script language="javascript" src="../javascript/funciones.js"></script>
<script language="javascript" src="javascript/funciones_Adminmenu.js"></script>
</head>

<body >
<form id="form_notificaciones"  name ="form_notificaciones" style="width:100%"><!-- method='POST' action="php_ajax/upload_imagendeestudiofisico.php" enctype='multipart/form-data'>-->
<table width="100%">
	<tr> <td colspan="4" align="center" > Administración de Menus <b><?php echo $usuario;?></b> <input type="hidden" value="<?php echo $usuario;?>" id="usuario" name="usuario"></td>
	</tr>
	<tr> <td colspan="4" align="center" class="texto8"> Esta pantalla permite agregar nuevos modulos al sistema web.</td></tr>
  	<tr>
		<td colspan="4" align="center">
			<table border="1" id="tmenus" name="tmenus" width="100%">
				<tr>
					<th class="subtituloverde" width="14%">Nombre de Menu </th>
					<th class="subtituloverde" width="14%">Descripcion del menu</th>
					<th class="subtituloverde" width="14%">Liga de menu</th>
					<th class="subtituloverde" width="14%">Tipo de menu</th>
					<th class="subtituloverde" width="14%">Posicion del menu</th>
					<th class="subtituloverde" width="14%">Estatus del menu</th>
				</tr>
				<tbody id="menus">
					<?php  if( $menus!=null) for($j=0;$j<count($menus);$j++){ ?>
						<tr id="filaOculta" >
						<td width="14%"><input type="text" name="nombre"  id="nombre" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value="<?php echo ltrim(rtrim($menus[$j]['nombre'])); ?>"></td>
						<td width="14%"><input type="text" name="descripcion"  id="descripcion" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value="<?php echo ltrim(rtrim($menus[$j]['descripcion'])); ?>"></td>
						<td width="14%"><input type="text" name="liga"  id="liga" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value="<?php echo ltrim(rtrim($menus[$j]['liga'])); ?>"></td>
						<td width="14%"><select name="tipo"  id="tipo" style="width:100%" class="caja_entrada">
							<?php
								if(ltrim(rtrim($menus[$j]['tipo']))=="0") 
								{
									echo "<option value='0'  selected>Busqueda</option>"; 
									echo "<option value='1'>Directo</option>";
								}
								else{
									echo "<option value='0'  >Busqueda</option>";
									echo "<option value='1' selected>Directo</option>"; 
								}
							?>
							</select>	
						</td>
						<td width="14%"><input type="text" name="posicion"  id="posicion" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value="<?php echo ltrim(rtrim($menus[$j]['posicion'])); ?>" ></td>
						<td width="14%"><select name="status"  id="status" style="width:100%" class="caja_entrada">
							<?php
								if(ltrim(rtrim($menus[$j]['status']))=="1") 
								{
									echo "<option value='1'  selected>Habilitado</option>"; 
									echo "<option value='0'>Deshabilitado</option>";
								}
								else
								{
									echo "<option value='1'  selected>Habilitado</option>";
									echo "<option value='0'>Deshabilitado</option>";
								}
 
							?>
									</select>						
						</td>
						<td width="14%"><input type="button" value="+" id="ad_colin" name="ad_colin" onClick="agrega_Menu();" >
										<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_Menu(event,this);" >
										<input type="hidden" id="id_menu" name="id_menu" value="<?php echo $menus[$j]['id'];?>">
						</td>
					</tr>
					<?php }else{ ?>
					<tr id="filaOculta" >
						<td width="14%"><input type="text" name="nombre"  id="nombre" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value=""></td>
						<td width="14%"><input type="text" name="descripcion"  id="descripcion" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value=""></td>
						<td width="14%"><input type="text" name="liga"  id="liga" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value=""></td>
						<td width="14%"><select name="tipo"  id="tipo" style="width:100%" class="caja_entrada">
											<option value='0'  selected>Busqueda</option>
											<option value='1'>Directo</option>
										</select>
						</td>
						<td width="14%"><input type="text" name="posicion"  id="posicion" align="middle" maxlength="100" class="caja_entrada" style="width:100%" value=""></td>
						<td width="14%"><select name="status"  id="status" style="width:100%" class="caja_entrada">
											<option value='1'  selected>Habilitado</option>
											<option value='0'>Deshabilitado</option>
										</select>
						</td>
						<td width="14%"><input type="button" value="+" id="ad_colin" name="ad_colin" onClick="agrega_Menu();" >
										<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_Menu(event,this);" >
										<input type="hidden" id="id_menu" name="id_menu" value="">
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr><td align="center" width="100%"><input type="button"  value="Guardar" onClick="GuardarMenus();" class="caja_entrada"></td></tr>
</table>
</form>

</body>
</html>
