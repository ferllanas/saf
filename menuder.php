<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml2/DTD/xhtml1-strict.dtd">
<?php
	require_once("connections/dbconexion.php");
?>
<html>
    <head>
        <title>Men&uacute; Izquierdo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta name="title" content="Fomerrey">
        <meta http-equiv="title" content="Fomerrey">
        <meta name="description" content="Fomerrey">
        <meta name="keywords" content="Fomerrey">
        <meta http-equiv="description" content="Fomerrey">
        <meta http-equiv="keywords" content="Fomerrey">
        
        <!--<link type="text/css" href="css/estilos.css" rel="stylesheet"> -->
        <script type="text/javascript" src="menuaccordion/jquery.min.js"></script>
        <script type="text/javascript" src="menuaccordion/ddaccordion.js">
            /***********************************************
            * Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
            * Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
            * This notice must stay intact for legal use
            ***********************************************/
        </script>
    
        <!-- <script type="text/javascript">
        
            ddaccordion.init({
                headerclass: "headerbar", //Shared CSS class name of headers group
                contentclass: "submenu", //Shared CSS class name of contents group
                revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc] [] denotes no content
                onemustopen: true, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: false, //Should contents open by default be animated into view?
                persiststate: true, //persist state of opened contents within browser session?
                toggleclass: ["", "selected"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "normal", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            })
            
        </script> -->
    
        <style type="text/css">    
            .urbangreymenu {
                width: 150px; /*width of menu*/
            }
            
            .urbangreymenu .headerbar{
                font: bold 11px Arial, Verdana, Helvetica, sans-serif;
                color: white;
                background: #005200 /*url(IMAGEN/botones/arrowstop.gif)*/ no-repeat 2px 5px; /*last 2 values are the x and y coordinates of bullet image*/
                margin-bottom: 1px; /*bottom spacing between header and rest of content*/
                text-transform: none;
                padding: 5px 5px 5px 5px; /*31px is left indentation of header text*/
				border-bottom: 1px solid #FFFFFF; 
            }
            
            .urbangreymenu .headerbar a{
                text-decoration: none;
                color: white;
                display: block;
            }
            
            .urbangreymenu ul{
                list-style-type: none;
                margin: 0;
                padding: 0;
                margin-bottom: 0; /*bottom spacing between each UL and rest of content*/
            }
            
            .urbangreymenu ul li{
                padding-bottom: 1px; /*bottom spacing between menu items*/
            }
            
            .urbangreymenu ul li a{
                font: normal 11px Arial, Verdana, Helvetica, sans-serif;
                color: black;
                background: #8BB381;
                display: block;
                padding: 5px 0;
                line-height: 15px;
                padding-left: 10px; /*link text is indented 8px*/
				padding-right: 5px;
                text-decoration: none;
            }
            
            .urbangreymenu ul li a:visited{
                color: black;
            }
            
            .urbangreymenu ul li a:hover{ /*hover state CSS*/
                color: white;
                font: bold;
                /*background: black;*/
                background:#8BB381;
            }
    
        </style>
    </head>
    <body>
	<?php 
		$nivel = $_GET['idnivel'];		
		$llave = $_GET['idllave'];
		if ($nivel == 0)
		{
			$consulta = "sp_geomun";			
			$dbRes = odbc_exec($conexion_db, $consulta);
			echo "<div class='urbangreymenu'>";
			echo "<div class='headerbar'><a href='#' >Municipios</a></div>";	
			echo "<ul class='submenu'>";				
			while($row = odbc_fetch_object($dbRes))
			{
				$descrip = $row->descripcion;
				$nivel = $row->id;
				$llave = $row->llave;
				$mun= $row->llave;				
				
				echo "<li><a href='geo.php?idnivel=$nivel&idllave=$llave&mun=$mun' target='centro'>$descrip</a></li>";						
			}
			echo "</ul>";
		}
		else
		{
			if ($nivel == 1)
			{
				$consulta = "sp_geofracc '".$llave."'";
				$dbRes = odbc_exec($conexion_db, $consulta);
				echo "<div class='urbangreymenu'>";
				echo "<div class='headerbar'><a href='#' >Fraccionamientos</a></div>";	
				echo "<ul class='submenu'>";				
				while($row = odbc_fetch_object($dbRes))
				{
					$descrip = $row->descripcion;
					$nivel = $row->id;
					$llave = $row->llave;
					$fracc = $row->llave;
					$mun = $row->mun;					
					echo "<li><a href='geo.php?idnivel=$nivel&idllave=$llave&mun=$mun&fracc=$fracc' target='centro'>$descrip</a></li>";						
				}
				echo "</ul>";
			}
			else
			{
				if ($nivel == 2)
				{
					$consulta = "sp_geomza '".$llave."'";
					$dbRes = odbc_exec($conexion_db, $consulta);
					echo "<div class='urbangreymenu'>";
					echo "<div class='headerbar'><a href='#' >Manzanas</a></div>";	
					echo "<ul class='submenu'>";				
					while($row = odbc_fetch_object($dbRes))
					{
						$descrip = $row->descripcion;
						$nivel = $row->id;
						$llave = $row->llave;
						$mzna = $row->llave;
						$fracc = $row->fracc;
						$mun = $row->mun;
						echo "<li><a href='geo.php?idnivel=$nivel&idllave=$llave&mzna=$mzna&fracc=$fracc&mun=$mun' target='centro'>$descrip</a></li>";						
					}
					echo "</ul>";
				}
				else
				{
					if ($nivel == 3)
					{
						$consulta = "select 4 as id,fml as llave,substring(fml,9,4) as descripcion,fracc,mun,fm from tecnicdlotes where fm= '".$llave."' order by fml";
						$dbRes = odbc_exec($conexion_db, $consulta);
						//echo $llave;
						echo "<div class='urbangreymenu'>";
						echo "<div class='headerbar'><a href='#' >lotes</a></div>";	
						echo "<ul class='submenu'>";				
						while($row = odbc_fetch_object($dbRes))
						{
							$descrip = $row->descripcion;
							$nivel = $row->id;
							$llave = $row->llave;
							$mzna = $row->fm;
							$fracc = $row->fracc;
							$mun = $row->mun;
							$lote= $row->llave;
							echo "<li><a href='geo.php?idnivel=$nivel&idllave=$llave&mzna=$mzna&fracc=$fracc&mun=$mun&lote=$lote' target='centro'>$descrip</a></li>";						
						}
						echo "</ul>";
					}
				}
			}
		}		
	?>	
   </body>
</html>