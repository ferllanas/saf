<?php
header("Content-type: text/html; charset=UTF8");
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
$command="";
	//echo $opcion;
	if($opcion==1)
	{
		$command= "SELECT a.folio, b.nombre as cajero, c.nomemp as cajerogral , a.total, alias as banco, CONVERT(varchar(10), a.fecha, 103) as fecha, a.estatus
			FROM ingresosmcobranza_cajero a 
			LEFT JOIN v_cajeros b ON a.cajero=b.num
			LEFT JOIN v_cajero_gral c ON a.cajero_gral=c.numemp
			LEFT JOIN egresosmbancos d ON a.banco=d.banco
		   WHERE a.estatus=0";
  	
		$termino=substr($_REQUEST['query'],1);
  		if(strlen($termino)>0)
			if(is_numeric($termino))
  				$command.=" AND a.cajero LIKE '%" . substr($_REQUEST['query'],1) . "%'";
			else
				$command.=" AND b.nombre LIKE '%" . substr($_REQUEST['query'],1) . "%'";
			
		$command.="	order by a.folio";
	}
	
	if($opcion==2)
	{
		$termino=substr($_REQUEST['query'],1);
		
		

		$command= "SELECT a.folio, b.nombre as cajero, c.nomemp as cajerogral , a.total, alias as banco, CONVERT(varchar(10), a.fecha, 103) as fecha
				FROM ingresosmcobranza_cajero a 
				LEFT JOIN v_cajeros b ON a.cajero=b.num
				LEFT JOIN v_cajero_gral c ON a.cajero_gral=c.numemp
				LEFT JOIN egresosmbancos d ON a.banco=d.banco
				WHERE a.estatus=0";
		if(strlen($termino)>0)
			if(is_numeric($termino))
				$command.=" AND a.banco LIKE '%" . substr($_REQUEST['query'],1) . "%'";
			else
				$command.=" AND d.alias LIKE '%" . substr($_REQUEST['query'],1) . "%'";
		$command.="	order by a.folio";
	}
	
	//echo $command;
	if($opcion==3)
	{
		$termino=substr($_REQUEST['query'],1);
		
		

		$command= "SELECT a.folio, b.nombre as cajero, c.nomemp as cajerogral , a.total, alias as banco, CONVERT(varchar(10), a.fecha, 103) as fecha
						FROM ingresosmcobranza_cajero a 
						LEFT JOIN v_cajeros b ON a.cajero=b.num
						LEFT JOIN v_cajero_gral c ON a.cajero_gral=c.numemp
						LEFT JOIN egresosmbancos d ON a.banco=d.banco
						WHERE a.estatus=0";
		if(strlen($termino)>0)
			if(is_numeric($termino))
				$command.=" AND a.cajero_gral LIKE '%" . substr($_REQUEST['query'],1) . "%'";
			else
				$command.=" AND c.nomemp LIKE '%" . substr($_REQUEST['query'],1) . "%'";
		$command.="	order by a.folio";
	}		
	
	
		if($opcion==6)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "SELECT a.folio, b.nombre as cajero, c.nomemp as cajerogral , a.total, alias as banco, CONVERT(varchar(10), a.fecha, 103) as fecha
						FROM ingresosmcobranza_cajero a 
						LEFT JOIN v_cajeros b ON a.cajero=b.num
						LEFT JOIN v_cajero_gral c ON a.cajero_gral=c.numemp
						LEFT JOIN egresosmbancos d ON a.banco=d.banco
						WHERE a.estatus=0 AND a.fecha >= '" . $fini . "' and a.fecha <='" . $ffin . "' ";
				
											
				$command .="  order by a.folio";
				 
	}	
	
			//	echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$datos[$i]['folio']=trim($row['folio']);
					$datos[$i]['cajero']=trim(utf8_encode($row['cajero']));
					$datos[$i]['cajerogral']=trim(utf8_encode($row['cajerogral']));
					$datos[$i]['banco']=trim(utf8_encode($row['banco']));					
					$datos[$i]['fecha']=trim($row['fecha']);
					$datos[$i]['total']="$".number_format($row['total'],2);
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>