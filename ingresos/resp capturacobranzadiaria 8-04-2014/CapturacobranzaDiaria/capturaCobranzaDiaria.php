<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$command = "select id, descrip, monto from ingresosmtipodenominacion a WHERE estatus<9000  ORDER BY orden";	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../../cheques/css/style.css">         
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script src="../../jquery-ui-1.9.2.custom/js/jquery-1.8.3.js"></script>
<script src="../../cheques/javascript/jquery.tmpl.js"></script>
<script src="javascript/busquedaIncrementalBanco.js"></script>
<script src="javascript/busquedaIncrementaCajeros.js"></script>
<script src="javascript/busquedaIncrementaCajeroGral.js"></script>
<script src="javascript/busquedaIncrementaOrigen.js"></script>
<script src="javascript/capturaCobranzaDiaria.js"></script>
<script src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<title>Reporte de Cobranza</title>
<script>
	function desglosarMonedas()
	{
		var cajero= document.getElementById('cajero').value;
		if(cajero.length<=0)
		{
			return false;
		}
		
		var banco= document.getElementById('banco').value;
		/*if(banco.length<=0)
		{
			return false;
		}*/
		
		var data = 'cajero='+cajero+'&banco='+banco; 
		
		$.post('php_ajax/buscarDesgloseDMonedas.php',data, function(resp)
		{ //Llamamos el arch ajax para que nos pase los datos
		
			//alert(resp.response);
			$('#proveedor').empty();
			// Checa el plugin de templating para Java, tomando los productos del script de abajo y 
			$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
			// los va colocando en la forma de la tabla
		}, 'json');  // Json es una muy buena opcion
	}
</script>

 <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
<tr class="fila">
	{{if nomprov}}
		<td align="center" style="width:100px; text-align:center;">
					<input type="hidden" id="id_ingresosmtipodenominacion" name="id_ingresosmtipodenominacion" value="{id}">
					<input type="text" name="valor" id="valor" value="0.00" style="width:100px; text-align:center;">
		</td>
		<td align="left">${descrip}</td>
		<td align="right">${monto}</td>
	{{else}}
			<td colspan="7">No existen resultados</td>
	{{/if}}
</tr>
</script>   


</head>

<body>
 <span class="TituloDForma">Captura de Cobranza</span>
    <hr class="hrTitForma">
<table>
	<tr>
    	<td>
  <table>
  	<tr>
    	<td><div align="left" style="z-index:1; position:absolute; width:295px; left: 1px; top: 33px; height: 25px;">     
		Cajero:<input class="texto8" type="text" id="cajero" name="cajero" tabindex="1" style="width:250px; top:3px; position:relative;"  onKeyUp="searchCajero(this);" autocomplete="off">
		<div id="search_suggestCajero" style="z-index:2;" > </div>
	  </div></td>
        <td><div align="left" style="z-index:3; position:absolute; width:295px; left: 303px; top: 33px; height: 25px;">     
		Banco:<input class="texto8" type="text" id="banco" name="banco" tabindex="2" style="width:250px; top:3px; position:relative;"  onKeyUp="searchBancos(this);" autocomplete="off">
		<div id="search_suggestProv" style="z-index:4;" > </div>
			</div>
      </td>
       <td><div align="left" style="z-index:5; position:absolute; width:323px; left: 601px; top: 33px; height: 25px;">     
		Cajero General:
		    <input class="texto8" type="text" id="CajeroGral" name="CajeroGral" tabindex="3" style="width:240px; top:3px; position:relative;"  onKeyUp="searchCajeroGral(this);" autocomplete="off">
		<div id="search_suggestCajeroGral" style="z-index:6;" > </div>
			</div>
      </td>
	  <td><div align="left" style="z-index:7; position:absolute; width:220px; left: 950px; top: 33px; height: 25px;">     
		Origen:
		    <input class="texto8" type="text" id="Origen" name="Origen" tabindex="4" style="width:180px; top:3px; position:relative;"  onKeyUp="searchOrigen(this);" autocomplete="off">
		<div id="search_suggestOrigen" style="z-index:8;" > </div>
			</div>
      </td>
    </tr>
</table>
</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td width="100%" align="center">
 <table id="Exportar_a_Excel" class="tablesorter" align="center">
 <caption style="background:#CCC; color:#FFF">DESGLOSE DE MONEDAS</caption>
                <thead>
   <th width="100px" style="text-align:center">Cantidad</th>   
                    <th width="150px" style="text-align:center">CONCEPTO</th> 
					<th width="100px" style="text-align:center">TOTAL</th>                                       
                </thead>
                <tbody id="proveedor" name='proveedor'>
                <?php
					//echo $command;
				$getProducts = sqlsrv_query( $conexion_srv,$command);
					if ( $getProducts === false)
					{ 
						$resoponsecode="02";
						die($command."". print_r( sqlsrv_errors(), true));
					}
					else
					{
						$resoponsecode="Cantidad rows=".count($getProducts);
						$i=0;
						while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
						{
							?>
                            <tr>
                            	<td style="text-align:center;">
                    <input type="text" id="valor" name="valor_<?php echo trim($row['id']);?>" value="0" style="text-align:right" onKeypress="javascript:return aceptarSoloNumeros(this, event);" onKeyUp="javascript:asignaFormatoSiesNumerico(this,event);calculaTotal(this);" onblur="javascript: calculaTotal(this);"/>
					<input type="hidden" id="monto_<?php echo trim($row['id']);?>" name="monto_<?php echo trim($row['id']);?>" value="<?php echo number_format(trim($row['monto']),2);?>" />
                    <input type="hidden" id="descr_<?php echo trim($row['id']);?>" name="descr_<?php echo trim($row['id']);?>" value="<?php echo trim($row['descrip']);?>" /></td>
								<td style="text-align:center;"><?php echo utf8_decode(trim($row['descrip']));?> DE <?php echo number_format($row['monto'],2);?></td>
								<td style="text-align:right;"><input tabindex="-1" id="total_<?php echo trim($row['id']);?>" name="totales" type="text" value="0.00" style="text-align:right;" /></td>
                            </tr>
							<?php
						}
					}
				?>
					
                </tbody>
                <tfoot>
                	<tr>
                    	<td colspan="3"><hr /></td>
                    </tr>
                	<tr>
                	<td></td>
                    <td style="text-align:right;">TOTAL:$</td>
                    <td><input tabindex="-1" type="text" readonly="readonly" id="total" name="total" value="0.00" style="text-align:right;" /></td>
                    </tr>
                </tfoot>
</table>
	</td>
    </tr>
    <tr>
    	<td align="center">
        	<input type="button" onclick="Guardar()" value="Guardar" />
        </td>
    </tr>
    
</table>
</body>
</html>