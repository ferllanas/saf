// JavaScript Document
function calculaTotal(objeto)
{
	 var idonTable =objeto.name.split("_")[1];
	 var cantidad=objeto.value;
	 var monto=$("#monto_"+idonTable).val();
	 
	var total= parseFloat(cantidad.replace(/,/g,"")) * parseFloat(monto.replace(/,/g,""));
	var formatiado=NumberFormat(total,2,".",",");
	 $("#total_"+idonTable).val(  formatiado );
	 
	sumatotales(); 
	
	
}

function sumatotales()
{
	var mytotal=0.00;
	$("input[type='text']").each(function(){
							var objetooo= {};
							
							if(this.name=="totales")
							{
								//alert(this.value);
								var total = this.value;
								mytotal += parseFloat(total.replace(/,/g,""));
							}
					 });
	
	var formatiado=NumberFormat(mytotal,2,".",",");
	$("#total").val(formatiado);
	
}

function Guardar()
{
	if(document.getElementById('cajero').value.length<=0)
	{
		alert("Favor de seleccionar el cajero.");
		return false;
	}
	
	if(document.getElementById('banco').value.length<=0)
	{
		alert("Favor de seleccionar el banco.");
		return false;
	}
	
	if(document.getElementById('CajeroGral').value.length<=0)
	{
		alert("Favor de seleccionar el cajero general.");
		return false;
	}
	
	if(document.getElementById('total').value<=0)
	{
		alert("No hay datos Capturados.");
		return false;
	}	
		
	var cajeroA= document.getElementById('cajero').value.split(".-")[0]; 
	var nomcajeroA= document.getElementById('cajero').value.split(".-")[1]; 
	var bancoA = document.getElementById('banco').value.split(".-")[0]; 
	var bancoNombreA = document.getElementById('banco').value.split(".-")[1]; 
	var CajeroGralA = document.getElementById('CajeroGral').value.split(".-")[0];
	var nomCajeroGralA = document.getElementById('CajeroGral').value.split(".-")[1];
	var totalA = document.getElementById('total').value.replace(/,/g,"");
	var origenA= document.getElementById('Origen').value.split(".-")[0]; 
	var nomorigenA = document.getElementById('Origen').value.split(".-")[1];
	
	
	var monedasJS=new Array();
	$("input[type='text']").each(function(){
							var objetooo= {};
							
							if(this.id=="valor")
							{
								 //alert(this.value);
								 var idonTable =this.name.split("_")[1];
								 objetooo['denominacion'] = $("#monto_"+idonTable).val();
							     objetooo['denominacion']=  objetooo['denominacion'].replace(/,/g,"");
								 
								 objetooo['descrip'] = $("#descr_"+idonTable).val();
								 objetooo['cantidad'] = this.value.replace(/,/g,"");
								 objetooo['monto'] = $("#total_"+idonTable).val();
								 objetooo['monto']= objetooo['monto'].replace(/,/g,"");
								 monedasJS.push( objetooo );
							}
							
							 //
							// objetooo['id']=this.value;
	                        // objetooo['id']=this.value;
							// 
					 });
	
	
	var datas = {
					cajero : cajeroA,
					banco: bancoA,
					total: totalA,
					CajeroGral: CajeroGralA,
					monedas: monedasJS,
					nomcajero: nomcajeroA,
					nomCajeroGral:nomCajeroGralA,
					bancoNombre: bancoNombreA,
					origen : origenA,
					nomorigen : nomorigenA
    			};
	//alert(datas.cajero+", "+datas.banco+", "+datas.CajeroGral+", "+datas.total);
			
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/capturaCobranzaDiaria.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
					console.log(resp);
					
					if(resp.fallo==false)
					{
						$("input[type='text']").each(function(){
						  		this.value=""
						  		if(this.id=="valor")
								{
									this.value="0";
									var idonTable =this.name.split("_")[1];
									$("#total_"+idonTable).val( 0.00 );
								}
						  });
						window.open(resp.path,'REPORTE DE COBRANZA DIARIO','width=800,height=600');
						location.href="capturaCobranzaDiaria.php";
						
					}
					else
					{
						alert(resp.msg);
					}
					
				}
			});
}


function sp_ingresos_B_mcobranza_cajero(imagen , nfolio)
{
	
	if(!confirm("�Esta seguro que desea eliminar esta informacion?"))
		return false;
		
	var datas = {
					xfolio : nfolio,
    			};
				
				
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/sp_ingresos_B_mcobranza_cajero.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
					console.log(resp);
					
					if(resp.fallo==false)
					{
						if(resp.mensaje=="OK")
						{
							alert("Se ha eliminado la captura.");
							$(imagen).parent().parent().remove();
						}
					}
					else
					{
						alert(resp.mensaje);
					}
				}
			});
}