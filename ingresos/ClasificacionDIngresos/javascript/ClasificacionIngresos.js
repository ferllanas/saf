// JavaScript Document
var allMovimientos= new Array();
var gruposMov= new Array();
var botonGloba= new Object;
function cargarMovimientosIngresos()
{
	//allMovimientos.splice(0, allMovimientos.length);
	gruposMov.splice(0, gruposMov.length);
	var banco = document.getElementById('banco').value;
	var fecini = document.getElementById('fecini').value;
	var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
	var data = 'banco=' + banco +'&ini=' + fecini +'&fin='+ fecfin;     // Toma el valor de los datos, que viene del input						
	
	//alert(data);
	$.post('php_ajax/getMovimientos.php',data, function(resp)
	{
		//Listar Datos
		console.log(resp);
		
		
		if(resp.length>0)
		{
			TomarDatosPrincipalesYDesplegar(resp);
		
			for(var i=0;i<gruposMov.length;i++)
			{
				gruposMov[i]['totalstr']=" $"+addCommas(gruposMov[i]['total']);
			}
			
			$('#proveedor').empty();
			$('#tmpl_proveedor').tmpl(gruposMov).appendTo('#proveedor'); 
		}
		else
		{
			$('#proveedor').empty();
			alert("No obtuvo resultados para estos criterios.");
		}
	}, 'json'   );  // Json es una muy buena opcion            
}


function TomarDatosPrincipalesYDesplegar(datos)
{
	
	if(datos.length>0)
	{
		var grupo={};
		grupo['grupo']=datos[0].grupo;
		grupo['total']=datos[0].total;
		grupo['descrip']=datos[0].descrip;
		grupo['tipomov']=datos[0].tipomov;
		grupo['fecha']=datos[0].fecha;
		gruposMov.push(grupo);
		
		for(var i=0; i<datos.length ; i++)
		{
			//Agrega solo movimientos
			var movs={};
			movs['total']=datos[i].total;
			movs['tipomov']=datos[i].tipomov;
			movs['fecha']=datos[i].fecha;
			movs['id']=datos[i].id;
			movs['grupo']=datos[i].grupo;
			allMovimientos.push(movs);
			
			if(! existeGrupoEngruposMov(datos[i].grupo, datos[i].total) )
			{
				var grupo2={};
				grupo2['grupo'] = datos[i].grupo;
				grupo2['total'] = datos[i].total;
				grupo2['descrip'] = datos[i].descrip;	
				grupo2['tipomov'] = datos[i].tipomov;	
				grupo2['fecha'] = datos[i].fecha;	
				
				gruposMov.push(grupo2);
			}
		}
		
		//alert(gruposMov.length);
	}
	else
		alert("No obtuvo resultados.");
}

function existeGrupoEngruposMov(grupo, total)
{
	var existe=false;
	for(var j=0;j<gruposMov.length && existe==false;j++)
	{
		if( grupo == gruposMov[j].grupo && total == gruposMov[j].total )
			existe=true;
		else
			existe=false;
			
	}
	return existe;
}

function openVentanaClasificacion(grupo, objeto)
{
	loadPopupBox();
	$('#grupo').val(grupo);
	botonGloba=objeto;
	setTotalDeLaClasificacion(grupo)
}

var cuentasDGrupo = new Array();
function agregarCuenta()
{
	
	var vcuentas= {};
	var datcuenta= $('#cuenta').val();
	var tok = datcuenta.split(".-");
	vcuentas['grupo']=  $('#grupo').val();
	vcuentas['monto']=  parseFloat( ($('#monto').val()).replace(/,/g,""));
	if(vcuentas['monto']<=0)
	{
		alert("No ha ingresado el monto para esta cuenta.");
		return false;
	}
	
	var totalClas = parseFloat(getTotalDeLaClasificacion(vcuentas['grupo']));
	//alert(totalClas);
	//alert(parseFloat(vcuentas['monto']));
	
	totalClas = totalClas - vcuentas['monto'] ;
	//alert(totalClas);
	
	if(totalClas < 0)
	{
		alert("Excede al monto de la codificación.!!");
		return(false);
	}
	
	vcuentas['nomcta']=  tok[1];
	vcuentas['cuenta']= tok[0];
	alert(vcuentas['cuenta']);
	vcuentas['montostr']= "$"+addCommas(vcuentas['monto']);
	
	cuentasDGrupo.push(vcuentas);
	
	
	$('#ctasAsignadas > tr').remove();
	$('#tmpl_presingmcuentas').tmpl(cuentasDGrupo).appendTo('#ctasAsignadas'); 
	
	setTotalDeLaClasificacion( $('#grupo').val() );
	
	$('#cuenta').val("");
	$('#monto').val("");
}

function eliminatDeTabla(button, cuenta,nomcta,monto)
{
	var grupo=  $('#grupo').val();
	for(var i=0;i<cuentasDGrupo.length;i++)
	{
		if(cuentasDGrupo[i].grupo== grupo && cuenta==cuentasDGrupo[i].cuenta && cuentasDGrupo[i].monto==monto )
		{
			cuentasDGrupo.splice(i,1);
			var row=button.parentNode.parentNode;
			row.parentNode.removeChild( row );
		}
	}	

	setTotalDeLaClasificacion(grupo);
}

function getTotalDeLaClasificacion(grupo)
{
	var TotalEnGrupo=0.00;
	for(var i=0;i<gruposMov.length;i++)
	{
		if(gruposMov[i].grupo==grupo)
			TotalEnGrupo += parseFloat(gruposMov[i].total);
	}
	
	
	for(var j=0;j< cuentasDGrupo.length ;j++)
	{
		TotalEnGrupo -= parseFloat(cuentasDGrupo[j].monto);
	}
	
	TotalEnGrupo=TotalEnGrupo.toFixed(2);
	return(TotalEnGrupo);
}

function setTotalDeLaClasificacion(grupo)
{
	var TotalEnGrupo = getTotalDeLaClasificacion(grupo)
	$('#totalClasificacion').val(addCommas(TotalEnGrupo));
}

function guardarClasificacion()
{
	var grupo=  $('#grupo').val();
	if(getTotalDeLaClasificacion(grupo)!=0)
	{
		alert("Favor de revisar monto de cada cuenta al parecer aun falta para completar el total de la codificación.");
		return false;
	}
	
	var banco=  $('#banco').val();
	
	
	var datas = {
					pgrupo : grupo,
					pgruposmov: gruposMov,
					pallMovimientos: allMovimientos,
					pcuentasDGrupo: cuentasDGrupo,
					pbanco: banco
    			};
	
	//try
	//{
		jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/guardarYGenerarClasificacionDIngresos.php',
				data:     datas,
				dataType: "json",
				//async:    true,
				/*type:     'post',
				cache:    false,
				url:      'php_ajax/guardarYGenerarClasificacionDIngresos.php',
				data:     datas,
				dataType: "json",*/
				success: function(source)
				{
					console.log(source);
					if(source.success==true)
					{
						alert(source.msg);
						//Borra cuentasDGrupo
						
						cuentasDGrupo.splice(i,cuentasDGrupo.length);
						botonGloba.parentNode.removeChild(botonGloba); 
    					unloadPopupBox();
						$('#ctasAsignadas > tr').remove();
						$('#cuenta').val("");
						$('#monto').val("");
						$('#grupo').val("");
						$('#totalClasificacion').val("");
					}
					else
					{
						alert(source.msg);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) 
				{
					console.log(xhr);
					alert(xhr.status);
					alert(thrownError);
				}


			});
	/*}
	catch(ex)  
    {  
        alert(ex.description);  
    } */ 
}

function cerrarDetalle()
{
	unloadPopupBox();
}
function cargarMovimientosClasificados()
{
	//allMovimientos.splice(0, allMovimientos.length);
	gruposMov.splice(0, gruposMov.length);
	var banco = document.getElementById('banco').value;
	var fecini = document.getElementById('fecini').value;
	var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
	var data = 'banco=' + banco +'&ini=' + fecini +'&fin='+ fecfin;     // Toma el valor de los datos, que viene del input						
	
	$.post('php_ajax/getMovimientosClasificados.php',data, function(resp)
	{
		//Listar Datos
		console.log(resp);
		$('#proveedor').empty();
		if(resp.length>0)
		{
			$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); 
		}
	}, 'json');  // Json es una muy buena opcion               
}

function openVerDetalleMovClasificado(grupo, objetoBtn)
{
	var data = 'gpo=' + grupo;
	$.post('php_ajax/getCuentasDGruposClasificados.php',data, function(resp)
	{
		//Listar Datos
		console.log(resp);
		$('#ctasAsignadas').empty();
		if(resp.length>0)
		{
			var total=0.00;
			for(var i=0;i<resp.length;i++)
			{
				total += parseFloat(resp[i].monto);
			}
		
			var totalstr=addCommas(total);
			$('#totalClasificacion').val(totalstr);
			
			$('#tmpl_presingmcuentas').tmpl(resp).appendTo('#ctasAsignadas'); 
			loadPopupBox();
			//$('#totalClasificacion').val("0.00");
		}
	}, 'json');  // Json es una muy buena opcion     
	
}

 $(document).ready( function() {
   
        // When site loaded, load the Popupbox First
        //loadPopupBox();
   
        $('#popupBoxClose').click( function() {           
            unloadPopupBox();
        });
       
        $('#container').click( function() {
            unloadPopupBox();
        });
							 });
 function unloadPopupBox() {    // TO Unload the Popupbox
            $('#popup_box').fadeOut("slow");
            $("#container").css({ // this is just for style       
                "opacity": "1" 
            });
			$('#ctasAsignadas').empty();
			$('#cuenta').val("");
			$('#monto').val("");
			
			//Elimina las cuentas del grupo
			cuentasDGrupo.splice(0,cuentasDGrupo.length);
			
			$('#grupo').val("");
			$('#totalClasificacion').val("");
        }   
       
        function loadPopupBox() {    // To Load the Popupbox
            $('#popup_box').fadeIn("slow");
            $("#container").css({ // this is just for style
                "opacity": "0.3" 
            });        
        }        
		
		