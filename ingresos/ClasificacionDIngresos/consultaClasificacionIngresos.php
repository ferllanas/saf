<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="../../cheques/css/style.css">         
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/estilos.css"/>

<script src="../../jquery-ui-1.9.2.custom/js/jquery-1.8.3.js"></script>
<script src="../../cheques/javascript/jquery.tmpl.js"></script>

<script src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<script src="javascript/busquedaIncrementalCuentaPresup.js"></script>
<script src="javascript/ClasificacionIngresos.js"></script>
<script src="javascript/busquedaIncrementalBanco.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                    	<td align="center">${gpoGrupo}</td>
                        <td align="left">${nombanco}  </td>
                    	<td align="center">${tipomov}</td>
						<td align="center">${fecha}</td>
						<td align="right">$${montostr}</td>
                        <td align="center"><input type="button" value="ver"  onClick="openVerDetalleMovClasificado( ${gpoGrupo} ,this);" /></td>
            </tr>
</script>

<script id="tmpl_presingmcuentas" type="text/x-jquery-tmpl">   
            <tr>
                    	<td align="center" >${partida}</td>
                        <td align="left">${nomcta}</td>
                    	<td align="right">${montostr}</td>
                       
            </tr>
</script>
<!-- <td align="center"><input type="button" value="-"  onclick="eliminatDeTabla(this, ${cuenta},'${nomcta}',${monto})"/></td>-->
<title>Consulta de Codificaci&oacute;n de Ingresos</title>
</head>

<body>
    <span class="TituloDForma">Consulta de Codificaci&oacute;n de Ingresos</span>
    <hr class="hrTitForma">
<table style="width:100%">
	<tr>
    	<td style="width:100%">
        	<table style="width:700px">
            	<tr> 
                <td style="width:250px"><div align="left" style="z-index:4; position:absolute; width:295px; left: 18px; top: 28px; height: 25px;">     
		Banco:<input class="texto8" type="text" id="banco" name="banco" style="width:250px; top:3px; position:relative;"  onKeyUp="searchBancos(this);" autocomplete="off">
		<div id="search_suggestProv" style="z-index:5;" > </div>
</div>
					</td>
                    <td style="width:150px" class="texto8" scope="row" >Fecha Inicial:
                    <input name="fecini" type="text" size="7" id="fecini" value="<?php echo date('d/m/Y');?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:$('optfecha').checked=true">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
					</td>
					<td style="width:150px" class="texto8" scope="row" >Fecha Final:
                    <input name="fecfin" type="text" size="7" id="fecfin" value="<?php echo  date('d/m/Y');?>" class="texto8" maxlength="10"  style="width:70"  onFocus="javascript:$('optfecha').checked=true">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Calendario" align="absmiddle">
                    <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
					</td>
                    
			  </tr>
              <tr>
              	<td colspan="3" align="center"><input type="button" value="Buscar" onclick="cargarMovimientosClasificados()" />
                </td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
 	<tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td style="width:100%" align="center">
        	<table style="width:620px" border="1">
            	<thead>
                	<tr>
                    	<th align="center">Grupo</th>
                        <th align="center">Banco</th>
                    	<th align="center">Movimiento</th>
						<th align="center">Fecha</th>
						<th align="center">Monto</th>
                        <th align="center">&nbsp;</th>
                    </tr>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<div id="popup_box" >    
    <a id="popupBoxClose">X</a> 
    <table width="100%">
    	<tr>
        	<td>
            	<span class="TituloDForma">Cuentas Codificadas</span>
    			<hr class="hrTitForma">
            </td>
        </tr>
    	<!--<tr>
        	<td width="100%">
            	<table width="100%">
                	<tr>
                    	<td width="260px">
                        	<input type="hidden" value="grupo" id="grupo" name="" />
                        	<div align="left" style="z-index:4; position:absolute; width:295px; left: 5px; top: 57px; height: 25px; vertical-align:bottom;">     
		Cuenta:<input class="texto8" type="text" id="cuenta" name="cuenta" style="width:200px; top:3px; position:relative;"  onKeyUp="searchCuentaPresup(this);" autocomplete="off">
		<div id="search_suggestCuentaPresup" style="z-index:5; " > </div>
</div>
                        </td>
                        <td>
                        	Monto :$
                        </td>
                        <td>
                        	<input type="text" id="monto" name="monto" value='0.00' width="80px" style="text-align:right;"
                            		onKeypress="javascript:return aceptarSoloNumeros(this, event);" 
                                    onKeyUp="javascript:asignaFormatoSiesNumerico(this,event);" 
                                   />
                        </td>
                        <td>
                        	<input type="button" onclick="agregarCuenta();" value="Agregar" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>-->
        <tr>
        	<td>&nbsp;</td>
        </tr>
        <tr>
        	<td width="100%">
                 <table width="100%" class="tablesorter">
                    <thead>
                        <tr>
                            <th align="center">Cuenta</tH>
                            <th align="center">Nombre</th>
                            <th align="center">Monto</th>
                           <!-- <th>&nbsp;</th>-->
         				</tr>
                    </thead>
                    <tbody id="ctasAsignadas" name="ctasAsignadas">
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td colspan="3" align="right">Restan $<input type="text" id="totalClasificacion" name="totalClasificacion" value="" style="text-align:right"  readonly="readonly"/></td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
        <tr>
        	<td align="center"><input type="button" value="Cerrar" onclick="cerrarDetalle()" /></td>
        </tr>
    </table>  
   
</div>
</body>
</html>