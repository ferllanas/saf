<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	


$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$banco="";
$nombanco="";
if(isset($_REQUEST['gpo']))
{
	$gpo= $_REQUEST['gpo'];
}
	

if( $conexion)
{
	
	
	
	$command= "  SELECT   a.gpo, a.ctapresup, a.monto, b.nomcta
	FROM presingdgpo_cpi a 
	LEFT JOIN presingmcuentas b ON a.ctapresup=b.cuenta";	
	
	$entro=false;
	if(strlen($gpo)>0)
	{
		$command.=" WHERE gpo = $gpo";
		$entro=true;
	}
	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['gpoGrupo']= trim($row['gpo']);
			$datos[$i]['partida']= trim($row['ctapresup']);
			$datos[$i]['montostr']= number_format(trim($row['monto']),2);
			$datos[$i]['monto']= trim($row['monto']);
			$datos[$i]['nomcta']= utf8_encode(trim($row['nomcta']));
			$i++;
		}
	}
	
}
echo json_encode($datos);
//echo $sct;
?>