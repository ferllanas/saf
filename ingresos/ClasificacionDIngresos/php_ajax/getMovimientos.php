<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$tipo=0;

$banco="";
$ini="";
$fin="";
$nombanco="";

if(isset($_REQUEST['banco']))
{
	list($banco, $nombanco)= explode("-",	$_REQUEST['banco']);
	//echo $banco;
}
	
if(isset($_REQUEST['ini']))
{
	list($dia,$mes,$anno)= explode("/",	$_REQUEST['ini']);
	$ini= $mes."/".$dia."/".$anno;
}

if(isset($_REQUEST['fin']))
{
	list($dia,$mes,$anno)= explode("/",	$_REQUEST['fin']);
	$fin= $mes."/".$dia."/".$anno;
}

$datos = get_Movimientos($banco, $ini, $fin);

echo json_encode($datos);

function get_Movimientos($banco, $ini, $fin)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_ingresos_C_movs_pdtes_clasifica(?,?,?)}";//Arma el procedimeinto almacenado
	//echo $tsql_callSP;
	$params = array(&$banco, &$ini, &$fin);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$datos= array();
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );
	//print_r($params);
	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			//echo "<br>";
			$datos[$i]['id']= trim($row['id']);
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['descrip']= trim($row['descrip']);
			$datos[$i]['total']= trim($row['total']);
			$datos[$i]['tipomov']= trim($row['tipomov']);
			$datos[$i]['grupo']= trim($row['gpo']);
			$i++;
		}
	}
	
	return $datos;
}
?>