<?php
//header('Content-Type: application/json');
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$usuario = "";
$datos=array();
$datos['status']="success";

$usuario = $_COOKIE['ID_my_site'];
if(strlen($usuario)<=0)
{
	echo json_encode(array('success'=>false, 'msg'=>"No se encuentra logeado el usuario, Favor de iniciar en el sistema de nuevo!"));//array('status'=>'error', 'data'=>$datos)
	exit;
}

$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$retval="OK";
$grupo=0;

$pgrupo="";
//print_r($_POST); 
if(isset($_POST['pgrupo']))
	$pgrupo=$_POST['pgrupo'];
	
$pbanco=$_POST['pbanco'];
$nombanco = "";
if(isset($_POST['pbanco']))
	list($pbanco, $nombanco)= explode(".-",	$_POST['pbanco']);
	//$_POST['pbanco'];

$pgruposMov=array();
if(isset($_POST['pgruposmov']))
	$pgruposMov=$_POST['pgruposmov'];
else
{	die(json_encode(array('success'=>false, 'msg'=>"No recibio datos del grupo.")));
}

$pallMovimientos=array();
if(isset($_POST['pallMovimientos']))
	$pallMovimientos=$_POST['pallMovimientos'];
	
$pcuentasDGrupo=array();
if(isset($_POST['pcuentasDGrupo']))
{
	$pcuentasDGrupo=$_POST['pcuentasDGrupo'];
	//print_r($pcuentasDGrupo);
}
else
{
	die(json_encode(array('success'=>false, 'msg'=>"No recibio datos del cuentas de Grupo.")));
}
	
	if(count($pgruposMov)<=0)
	{
		die( json_encode(array('success'=>false, 'msg'=>"No recibio registros")));
	}
	
	for($i=0;$i<count($pgruposMov);$i++)
	{
		if($pgrupo==$pgruposMov[$i]['grupo'])
		{
			//echo "Entro";
			list($dia,$mes, $anio)=explode("/",$pgruposMov[$i]['fecha']);//$pgruposMov[$i]['fecha']
			$fecha=$mes."/".$dia."/".$anio;
  			$grupo = sp_presing_A_mgpo_cpi($fecha, 
										   $pgruposMov[$i]['tipomov'], 
										   $pgruposMov[$i]['total'], 
										   $usuario, 
										   $pbanco );
			if($grupo>0)
			{
				for($j=0; $j<count($pcuentasDGrupo) && $retval=="OK";$j++)
				{
					$retval = sp_presing_A_dgpo_cpi($grupo, $pcuentasDGrupo[$j]['monto'], $pcuentasDGrupo[$j]['cuenta'], $usuario);
				}
			}
			else
			{
				echo json_encode(array('success'=>false, 'msg'=>"Ha ocurrido un error al intentar crear el grupo de clasificacion."));
			}
		}
		
	}

if(trim($retval)=="OK" && $grupo>0)
{
	for($i=0;$i<count($pallMovimientos) && $retval=="OK" ;$i++)
	{
		if( $pallMovimientos[$i]['grupo'] == $pgrupo )
			$retval = sp_presing_M_gpo_bancosdmovs($grupo, $pallMovimientos[$i]['id'], $usuario);
	}
	
	if($retval!="OK")
 		echo json_encode(array('success'=>false, 'msg'=>"Ha ocurrido algun error al intentar marcar los movimientos como clasificados."));
	else
		echo json_encode(array('success'=>true, 'msg'=>"La Clasificacion de egresos fue satisfactoria."));
}
else
{
	echo json_encode(array('success'=>false, 'msg'=>"Ocurrio algun error.".$retval."==OK"." && ". $grupo));
}
 


function sp_presing_A_mgpo_cpi($fecha, $tipomov, $monto, $usuario, $banco)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$grupo=0;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_presing_A_mgpo_cpi(?,?,?,?, ?)}";//Arma el procedimeinto almacenado
	//echo $tsql_callSP;
	$params = array(&$fecha, &$tipomov, &$monto, &$usuario, &$banco);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$datos= array();
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			//echo "<br>";
			$grupo= trim($row['grupo']);
			$i++;
		}
	}
	
	return $grupo;
}


function sp_presing_A_dgpo_cpi($grupo, $monto, $cuenta, $usuario)
{
	global  $username_db, $password_db, $odbc_name, $server;
	
	
	$retval="false";
	//echo $retval."ME LLEVA la que me trajo";
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_presing_A_dgpo_cpi(?,?,?,?)}";//Arma el procedimeinto almacenado
	//echo $tsql_callSP;
	$params = array(&$grupo, &$monto, &$cuenta, &$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$datos= array();
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$retval = trim($row['msg']);
			$i++;
		}
	}
	
	return $retval;
}

function sp_presing_M_gpo_bancosdmovs($grupo, $id, $usuario)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$retval="false";
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_presing_M_gpo_bancosdmovs(?,?,?)}";//Arma el procedimeinto almacenado
	//echo $tsql_callSP;
	$params = array(&$grupo, &$id, &$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$datos= array();
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			//echo "<br>";
			$retval = trim($row['msg']);
			$i++;
		}
	}
	
	return $retval;
}

?>