<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	


$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$banco="";
$nombanco="";
if(isset($_REQUEST['banco']))
{
	list($banco, $nombanco)= explode(".-",	$_REQUEST['banco']);
}
	
$ini="";
if(isset($_REQUEST['ini']))
{
	list($dia,$mes,$anno)= explode("/",	$_REQUEST['ini']);
	//$ini= $mes."/".$dia."/".$anno;
}

$fin="";
if(isset($_REQUEST['fin']))
{
	list($dia,$mes,$anno)= explode("/",	$_REQUEST['fin']);
	//$fin= $mes."/".$dia."/".$anno;
}

if( $conexion)
{
	
	
	
	$command= "SELECT   a.gpo, CONVERT(VARCHAR(10), fecha,103) as fecha,  b.descrip as tipomov, b.corta ,c.nombanco , a.monto
	FROM presingmgpo_cpi a 
	LEFT JOIN bancosmtipomov b ON a.tipomov=b.id 
	LEFT JOIN egresosmbancos c ON a.banco=c.banco ";	
	
	$entro=false;
	if(strlen($banco)>0)
	{
		$command.=" WHERE a.banco like '$banco'";
		$entro=true;
	}
	
	if(strlen($ini)>0)
	{
		if($entro)
			$command.=" AND a.fecha>'$ini'";
		else
			$command.=" WHERE a.fecha>'$ini'";
			
		$entro=true;
	}
	
	if(strlen($fin)>0)
	{
		if($entro)
			$command.=" AND a.fecha<'$fin'";
		else
			$command.=" WHERE a.fecha<'$fin'";
			
		$entro=true;
	}
	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['gpoGrupo']= trim($row['gpo']);
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['montostr']= number_format(trim($row['monto']),2);
			$datos[$i]['nombanco']= utf8_encode(trim($row['nombanco']));
			$datos[$i]['tipomov']= trim($row['tipomov']);
			$i++;
		}
	}
	
}
echo json_encode($datos);
//echo $sct;
?>