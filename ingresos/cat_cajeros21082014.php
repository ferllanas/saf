<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		require_once("../connections/dbconexion.php");
		$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
		$conexion = sqlsrv_connect($server,$infoconexion);
		
		$datos = array();
		$usuario=$_COOKIE['ID_my_site'];
		//$usuario='001349';
		$numemp='';
		$uvehiculo='';
		
		//$consulta = "select a.id,b.numemp,b.nomemp FROM ingresosmcajeros a left join v_nomina_empleadosactivos b on a.numemp=b.numemp";
		$consulta = "select a.cajero,b.numemp,b.nomemp FROM ingresosmcajeros a inner join v_nomina_empleadosactivos b on ";
		$consulta .="a.numemp  COLLATE DATABASE_DEFAULT = b.numemp COLLATE DATABASE_DEFAULT where a.estatus<9000 order by b.nomemp";
		//echo $consulta;
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['cajero']= trim($row['cajero']);
			$datos[$i]['numemp']= trim($row['numemp']);
			$datos[$i]['nomemp']= trim($row['nomemp']);
	
			$i++;
		}
		
		
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../css/estilos.css" rel="stylesheet" type="text/css">

<script language="javascript" src="javascript/funcion_moneda.js"></script>
<script language="javascript" src="../prototype/jQuery.js"></script>
<script language="javascript" src="../prototype/prototype.js"></script>
<script language="javascript" src="../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../javascript_globalfunc/funcionesGlobales.js"></script>

<style type="text/css">
<!--
.Estilo1 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<p>&nbsp;</p>
<table width="662" border="0" align="center">
  <tr>
    <td width="406" >
	<div align="left"   style="z-index:7; position:relative; width:400px; "><span class="texto10">Empleado</span>      
	  <input name="uvehiculo" type="text" class="texto8" id="uvehiculo" tabindex="1" onKeyUp="searchempleado(this);" value="<?php echo $uvehiculo;?>" style="width:320px " autocomplete="off">
      <span class="texto10">      </span>      
      <div id="search_suggestempleado" style="z-index:8; "></div>
    </div>
	</td>
    <td width="114"><span class="texto10" >Cajero</span>
		<input type="text" name="cajero" id="cajero" size="6" tabindex="2" maxlength="4" onKeyPress="return aceptarSoloNumeros(this, event);">
	</td>
    <td width="42"><input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
    <input type="hidden" name="numemp" id="numemp" value="<?php echo $numemp;?>"></td>
    <td width="82"><input type="button" name="Submit" tabindex="3" value="Actualizar" onClick="agregar()"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<table width="60%" height="19%" border=2 align="center" style="vertical-align:top; border-color: #006600">
  <td width="90%" height="171" style="vertical-align:top;">
      <table width="100%" border=0 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <td width="66" align="center" class="subtituloverde">Cajero</td>
          <td width="317" align="center" class="subtituloverde" >Nombre del Cajero </td>
          <td width="69" align="center" class="subtituloverde" >&nbsp;</td>
        </tr>
      </table>
      <div style="overflow:auto; height:350px; padding:0">
        <table name="Tabla" id="Tabla" width="100%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white>
          <?php 
		
			for($i=0;$i<count($datos);$i++)
			{
		?>
          <tr class="fila" >
            <td width="29" align="center"><?php echo $datos[$i]['cajero'];?>
                <input name="numemp" id="numemp" type="hidden" value="<?php echo $datos[$i]['id'];?>" >
            </td>
            <td width="146" align="left"><?php echo $datos[$i]['nomemp'];?></td>
            <td width="31" align="center"><img src="../imagenes/eliminar.jpg" width="26" height="26" onClick="borrar(this,<?php echo $datos[$i]['id'];?>)"></td>
          </tr>
          <?php 
		}
		?>
        </table>
    </div></td>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
