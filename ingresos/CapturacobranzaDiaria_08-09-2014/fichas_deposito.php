<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$consulta= "";
$datos= array();
$datos2= array();
$fecha_recauda="";
$caja="";
$cajero="";
$cob_oficina=0;
$total_general=0;
$cob_tot=0;

$observa="";
$cob_campo = 0;
$vale_min  = "";
$vale_max  = "";

	$fec_recauda=" ";
	
	if(isset($_REQUEST['fec_recauda']))
		$fec_recauda = $_REQUEST['fec_recauda'];	

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	if(strlen($fec_recauda) > 1)
	{
		list($dia,$mes , $anio) = explode("/", $fec_recauda);
	//	$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
		$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
		$fecha_recauda=$dia." DE ".substr($meses,$mes*10-10,10)." DE ".$anio ;


		list($dd2,$mm2,$aa2)= explode('/',$fec_recauda);
		$fecha=$aa2.'-'.$mm2.'-'.$dd2;
		$fecha_r=$aa2.$mm2.$dd2;
		
		$consulta0 = "select observa from ingresosmreportedepositos where fecha=CONVERT(varchar(12),'$fecha', 103)";
		$R = sqlsrv_query( $conexion,$consulta0);
		$i=0;	

		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$observa = trim($row['observa']);
		}		
		
		$consulta = "select isnull(sum(pagcap+pagif-boncap-bonif+pagmor-bonmor),0) + 
		(select isnull(sum(imppag+pagmor-bonmor-impbon),0) as pagado from fomedbe.dbo.otrserdcreditos where 
		fecpago='$fecha_r' and oficre='IC' and cvesit<'23509000') + 
		(select isnull(sum(impcap+pagmor-bonmor),0) as pagado from fomedbe.dbo.otringdingresos where 
		fecpago='$fecha_r' and oficre='IC' and cvesit<'23909000') as pagado, 
		MIN(numvale) as vale_min, MAX(numvale) as vale_max 
		from fomedbe.dbo.carterdcreditos where fecpago='$fecha_r' and oficre='IC' and cvesit<'22109000'";
	
		if ($conexion)
		{
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;	
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$cob_campo = trim($row['pagado']);
				$vale_min  = trim($row['vale_min']);
				$vale_max  = trim($row['vale_max']);
	
			}
		}
		sqlsrv_free_stmt( $R);
		/*
		$consulta2 ="select isnull(sum(pagcap+pagif+pagmor-bonmor-boncap-bonif),0.00) + 
				(select isnull(sum(imppag+pagmor-bonmor-impbon),0) from fomedbe.dbo.otrserdcreditos 
				 where fecpago='$fecha_r' and oficre='IN' and cvesit<'23509000' and right(tipocre,1)<>'4')+ 
				 (select isnull(sum(impcap+pagmor-bonmor),0) from fomedbe.dbo.otringdingresos 
				 where fecpago='$fecha_r' and cvesit<'23909000'  and right(tipocre,1)<>'4') as pagado,right(caja,4) as cajero,descripcion 
				 from fomedbe.dbo.carterdcreditos a left join fomedbe.dbo.catsmscat b on b.scat=a.caja
				 where a.fecpago='$fecha_r' and oficre='IN'  and right(tipocre,1)<>'4' and a.cvesit<'22109000' and b.cvesit<'21809000' group by a.caja,b.descripcion";
		*/
		//echo $consulta2;CONVERT(varchar(12),'$fec_recauda', 103)
		
		$consulta2 ="select right(a.caja,4) as caja ,B.DESCRIPCION,sum(a.pago) as pago from (select CAJA,impcap+pagmor-bonmor as pago from fomedbe.dbo.otringdingresos 
                         where fecpago='$fecha_r' and cvesit<'23909000' and right(tipocre,4)<>'8304' and right(tipocre,4)<>'88104' and right(tipocre,4)<>'8404' and right(tipocre,4)<>'8204' and oficre='IN'
                         UNION all
					select caja,imppag+pagmor-bonmor-impbon as pago from fomedbe.dbo.otrserdcreditos 
                         where fecpago='$fecha_r' and oficre='IN' and cvesit<'23509000' and right(tipocre,1)<>'4' 
                         UNION all
					select CAJA,pagcap+pagif+pagmor-bonmor-boncap-bonif as pago from fomedbe.dbo.carterdcreditos
                                   where fecpago='$fecha_r' and oficre='IN'  and right(tipocre,1)<>'4' and cvesit<'22109000') a left join fomedbe.dbo.catsmscat b on a.caja=b.scat  group by caja, descripcion";
		
		$R = sqlsrv_query( $conexion,$consulta2);
		$i=0;	
		/*if( $R === false) {
			die( print_r( sqlsrv_errors(), true) );
		}*/
	
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos2[$i]['cob_oficina']=trim($row['pago']);
			$datos2[$i]['caja'] = trim($row['caja']);
			$datos2[$i]['cajero']  = trim($row['caja'])."  ".trim($row['descripcion']);
			$cob_oficina = $cob_oficina + trim($row['pago']);
			$i++;
			//echo $datos2[$i]['caja'];
			//$cob_oficina = trim($row['pagado']);
			//$caja  = trim($row['cajero']);
			//$cajero  = trim($row['cajero'])."  ".trim($row['descripcion']);
		}
		//$total_general = $cob_campo + $cob_oficina;
		/////////////////
		$consulta3 = "select CONVERT(varchar(12),a.fecha,103) as fecha,a.banco,b.nomcorto,b.cuenta_ban,b.alias,a.total from ingresosmcobranza_cajero a 
		left join egresosmbancos b on a.banco=b.banco where a.estatus<9000 and b.estatus<9000 
		and a.fecha = CONVERT(varchar(12),'$fecha', 103)";
		$R = sqlsrv_query( $conexion,$consulta3);
		$i=0;
		$total_general=0.00;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['banco']= trim($row['banco']);
			$datos[$i]['nomcorto']= trim($row['nomcorto']);
			$datos[$i]['cuenta_ban']= trim($row['cuenta_ban']);
			$datos[$i]['alias']= trim($row['alias']);
			$datos[$i]['total']= trim($row['total']);
			$total_general=$total_general+$datos[$i]['total'];
			$i++;
		}
	}
	$cob_tot=$cob_campo+$cob_oficina;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../../cheques/css/style.css">         
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script src="javascript/resumen_recauda.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<title>Depositos y/o Pagos en Efectivo</title>

<style type="text/css">
<!--
.Estilo2 {font-size: 12px}
-->
</style>
<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="24%" class="texto12">Fecha: <span class="texto9">
      <input type="text" value="<?php echo $fec_recauda;?>" name="fec_recauda" size="7" id="fec_recauda"  class="required" maxlength="10"  style="width:70" />
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle" /></span>
        <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fec_recauda",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger1",	// trigger for the calendar (button ID)
						singleClick    :    true
					});
		</script>
    </td>
    <td width="32%" class="texto12">
      <div align="center"><img src="../../imagenes/consultar.jpg" width="27" height="26" onclick="javascript: ;location.href='fichas_deposito.php?editar=true&fec_recauda='+document.getElementById('fec_recauda').value" /> </div></td>
    <td width="44%" class="texto12"><img src="../../imagenes/pdf.png" name="pdf" id="pdf" width="27" height="26" onclick="Ver_depositos_PDF()" /> </td>
  </tr>
</table>
<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="74%" class="texto12"><div align="center"><strong>REPORTE DE DEPOSITOS Y/O PAGOS DEL <?PHP echo $fecha_recauda;?></strong> </div>    </tr>
</table>

<table align="center" width="80%" border="1">
  <tr bgcolor="#CCCCCC" class="subtituloverde12">
    <td width="104"><div align="center">BANCO</div></td>
    <td width="128"><div align="center">CUENTA</div></td>
    <td width="302"><div align="center">DESCRIPCION</div></td>
    <td width="122"><div align="center">IMPORTE</div></td>
  </tr>
  <?php 
		for($i=0;$i<count($datos);$i++)
		{
	?>
  <tr bgcolor="#CCCCCC" class="texto12">
    <td align="center"><?php echo $datos[$i]['nomcorto'];?></td>
    <td align="center"><?php echo $datos[$i]['cuenta_ban'];?></td>
    <td align="center"><?php echo $datos[$i]['alias'];?></td>
    <td align="right"><?php echo number_format($datos[$i]['total'],2);?></td>
  </tr>
  <?php 
		}
	?>
</table>
<table align="center" width="80%" border="0">
  <tr>
    <td><span class="Estilo2">Observaciones: </span>
    <input type="text" size="140" name="observa" id="observa" value="<?php echo $observa;?>" /></td>
  </tr>
</table>

<table class="texto12" align="center" width="80%" border="0">
  <tr >
    <td bgcolor="#99FF99" class="texto10" width="9%">Vale</td>
    <td bgcolor="#99FF99" width="14%"><?php echo $vale_min;?></td>
    <td bgcolor="#99FF99" width="19%">&nbsp;</td>
  <td bgcolor="#99FF99">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#99FF99" width="9%" class="texto10">Al</td>
    <td bgcolor="#99FF99" width="14%" ><?php echo $vale_max;?></td>
    <td bgcolor="#99FF99" align="right"><strong><?php echo number_format($cob_campo,2);?></strong></td>
  <td bgcolor="#99FF99">&nbsp;</td>
  </tr>
</table>
<table class="texto12" align="center" width="80%" border="0">
    <?php 
		for($i=0;$i<count($datos2);$i++)
		{
	?>
	    <tr>
			<td bgcolor="#99FF99" width="9%" class="texto10">Cajero</td>
			<td bgcolor="#99FF99" width="14%"><?php echo $datos2[$i]['caja'];?></td>
			<td bgcolor="#99FF99" align="right" width="19%"><strong><?php echo number_format($datos2[$i]['cob_oficina'],2);?></strong></td>
			<td bgcolor="#99FF99">&nbsp;</td>
		</tr>
	<?php 
		}
	?>
</table>
<table class="texto12" align="center" width="80%" border="0">
  <tr bgcolor="#CCFF99">
    <td width="9%" class="texto10">&nbsp;</td>
    <td width="14%"><div align="right"></div></td>
    <td align="right" width="19%"><strong>$ <?php echo number_format($cob_tot,2);?></strong></td>
    <td align="right"><div align="right"></div>      <strong> $ <?php echo number_format($total_general,2);?></strong></td>
  </tr>
</table>
<table align="center" width="80%" border="0">
  <tr>
    <td>
      <input type="submit" name="Guardar" value="Guardar" onclick="guarda_datos()" />   </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="33%"><div align="center">___________________________________</div></td>
    <td width="46%"><div align="center">___________________________________</div></td>
    <td width="21%">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center"><strong>Lic. Laura Patricia Guajardo Martinez</strong></div></td>
    <td><div align="center"><strong>C. Jose Guadalupe Davila Rodriguez</strong></div></td>
    <td><div align="right">MO00703-2</div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
