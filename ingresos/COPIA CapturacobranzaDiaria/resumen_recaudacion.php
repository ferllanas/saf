<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
	$consulta= " ";
	$datos= array();
	$tot=0;
	$totgral=0;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	$fec_recauda=" ";
	
	if(isset($_REQUEST['fec_recauda']))
		$fec_recauda = $_REQUEST['fec_recauda'];	
	

	if(strlen($fec_recauda) > 1)
	{
		list($dia,$mes , $anio) = explode("/", $fec_recauda);
	//	$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
		$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
		$fecha_recauda=$dia." DE ".substr($meses,$mes*10-10,10)." DE ".$anio ;
					
		if ($conexion)
		{
			list($dd2,$mm2,$aa2)= explode('/',$fec_recauda);
			$fecha=$aa2.'-'.$mm2.'-'.$dd2;
			$b_consulta = "select a.banco,a.alias,a.nomcorto,a.cuenta_ban,a.estatus,
			( SELECT  sum(b.total) FROM ingresosmcobranza_cajero b 
			WHERE  a.banco=b.banco  and b.fecha = CONVERT(varchar(12),'$fecha', 103) 
			group by b.banco) as tot from egresosmbancos a where a.estatus<9000 order by a.banco,a.nomcorto";			
								
			$R = sqlsrv_query( $conexion,$b_consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$cta_ban[$i]['banco']= trim($row['banco']);
				$cta_ban[$i]['nomcorto']= trim($row['nomcorto']);
				$cta_ban[$i]['cuenta_ban']= trim($row['cuenta_ban']);
				$cta_ban[$i]['alias']= trim($row['alias']);
				if($row['tot']==null)
					$cta_ban[$i]['tot']=0.00;
				   else
					$cta_ban[$i]['tot']= number_format(trim($row['tot']),2);
				$i++;
				$totgral=$totgral + $row['tot'];
			}
				
			
			$consulta = "select a.fecha,a.banco,b.nomcorto,b.cuenta_ban,b.alias,a.total from ingresosmcobranza_cajero a ";
			$consulta .= "left join egresosmbancos b on a.banco=b.banco where a.estatus<9000 and b.estatus<9000 ";
			$consulta .= "and a.fecha = CONVERT(varchar(12),'$fecha', 103)";
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['fecha']= trim($row['fecha']);
				$datos[$i]['banco']= trim($row['banco']);
				$datos[$i]['nomcorto']= trim($row['nomcorto']);
				$datos[$i]['cuenta_ban']= trim($row['cuenta_ban']);
				$datos[$i]['alias']= trim($row['alias']);
				$datos[$i]['total']= number_format(trim($row['total']),2);
				$i++;
			}
		}
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<script src="javascript/resumen_recauda.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>

<table align="center" width="80%" border="0">
  <tr>
    <td width="24%" class="texto12">Fecha:
      <span class="texto9">
      <input type="text" value="<?php echo $fec_recauda;?>" name="fec_recauda" size="7" id="fec_recauda"  class="required" maxlength="10"  style="width:70">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle"></span> 
        
     
      <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fec_recauda",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger1",	// trigger for the calendar (button ID)
						singleClick    :    true
					});
		</script>
    <td width="32%" class="texto12">        <div align="center"><img src="../../imagenes/consultar.jpg" width="27" height="26" onClick="javascript: ;location.href='resumen_recaudacion.php?editar=true&fec_recauda='+document.getElementById('fec_recauda').value"> 
    </div>
    <td width="44%" class="texto12"><img src="../../imagenes/enviar_pdf.jpg" name="pdf" id="pdf" width="27" height="26" onClick="VerPDF()">  
  </tr>
</table>


<table align="center" width="80%" border="0">
  <tr>
    <td width="74%" class="texto12"><div align="center"><strong>RESUMEN DE RECAUDACION DEL <?PHP echo $fecha_recauda;?></strong> </div>    </tr>
</table>

<table width="80%" border="1" align="center">

  <tr bgcolor="#CCCCCC" class="subtituloverde12">
    <td width="104"><div align="center">BANCO</div></td>
    <td width="128"><div align="center">CUENTA</div></td>
    <td width="302"><div align="center">DESCRIPCION</div></td>
    <td width="122"><div align="center">IMPORTE</div></td>
  </tr>
  <?php 
		for($i=0;$i<count($cta_ban);$i++)
		{
	?>
  <tr class="texto12">
    <td align="center"><?php echo $cta_ban[$i]['nomcorto'];?></td>
    <td align="center"><?php echo $cta_ban[$i]['cuenta_ban'];?></td>
    <td align="center"><?php echo $cta_ban[$i]['alias'];?></td>
    <td align="right"><?php echo $cta_ban[$i]['tot'];?></td>
  </tr>
  <?php 
		}
	?>
</table>

<table width="80%" border="0" align="center">

  <tr bgcolor="#CCCCCC" class="texto12">
    <td width="520" align="center"><strong>Total General </strong></td>
    <td width="118" align="right">
	<div align="right"><strong>
		<?php echo number_format($totgral,2);?>
	</strong></div>	</td>
  </tr>
</table>

<table align="center" width="80%" border="1">
  <tr bgcolor="#CCCCCC" class="subtituloverde12">
    <td width="104"><div align="center">BANCO</div></td>
    <td width="128"><div align="center">CUENTA</div></td>
    <td width="302"><div align="center">DESCRIPCION</div></td>
    <td width="122"><div align="center">IMPORTE</div></td>
  </tr>
	<?php 
		for($i=0;$i<count($datos);$i++)
		{
	?>  
  <tr class="texto12">
	<td align="center"><?php echo $datos[$i]['nomcorto'];?></td>
		<td align="center"><?php echo $datos[$i]['cuenta_ban'];?></td>
		<td align="center"><?php echo $datos[$i]['alias'];?></td>
		<td align="right"><?php echo $datos[$i]['total'];?></td>
  </tr>

	<?php 
		}
	?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
