<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$consulta= "";
$datos= array();

$cob_campo = 0;
$vale_min  = "";
$vale_max  = "";

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	$consulta = "select isnull(sum(imppag+pagmor-bonmor-impbon),0) + 
	(select isnull(sum(imppag+pagmor-bonmor-impbon),0) as pagado from fomedbe.dbo.otrserdcreditos where 
	fecpago='20140408' and oficre='IC' and cvesit<'23509000') + 
	(select isnull(sum(impcap+pagmor-bonmor),0) as pagado from fomedbe.dbo.otringdingresos where 
	fecpago='20140408' and oficre='IC' and cvesit<'23909000') as pagado, 
	MIN(numvale) as vale_min, MAX(numvale) as vale_max 
	from fomedbe.dbo.carterdcreditos where fecpago='20140408' and oficre='IC' and cvesit<'22109000'";

	if ($conexion)
	{
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;	
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$cob_campo = trim($row['pagado']);
			$vale_min  = trim($row['vale_min']);
			$vale_max  = trim($row['vale_max']);

		}
	}
sqlsrv_free_stmt( $R);
// cobranza en Oficinas del dia 8 de abril
		$consulta2 ="select isnull(sum(imppag+pagmor-bonmor-impbon),0) + 
		(select isnull(sum(imppag+pagmor-bonmor-impbon),0) from fomedbe.dbo.otrserdcreditos 
		where fecpago='20140408' and oficre='IN' and cvesit<'23509000')+ 
		(select isnull(sum(impcap+pagmor-bonmor),0) from fomedbe.dbo.otringdingresos 
		where fecpago='20140408' and cvesit<'23909000') as pagado,right(caja,4) as cajero,descripcion 
		from fomedbe.dbo.carterdcreditos a left join fomedbe.dbo.catsmscat b on b.scat=a.caja
		where a.fecpago='20140408' and oficre='IN' and a.cvesit<'22109000' group by a.caja,b.descripcion";
		//echo $consulta2;CONVERT(varchar(12),'$fec_recauda', 103)
		$R = sqlsrv_query( $conexion,$consulta2);
		$i=0;	
		//if( $R === false) {
   		 //die( print_r( sqlsrv_errors(), true) );
		//}

		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$cob_oficina = trim($row['pagado']);
			$cajero  = trim($row['cajero'])."  ".trim($row['descripcion']);

		}
		$total_general = $cob_campo + $cob_oficina;
		/////////////////
		$consulta3 = "select a.fecha,a.banco,b.nomcorto,b.cuenta_ban,b.alias,a.total from ingresosmcobranza_cajero a 
		left join egresosmbancos b on a.banco=b.banco where a.estatus<9000 and b.estatus<9000 
		and a.fecha = '2013-03-20'";
		$R = sqlsrv_query( $conexion,$consulta3);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['banco']= trim($row['banco']);
			$datos[$i]['nomcorto']= trim($row['nomcorto']);
			$datos[$i]['cuenta_ban']= trim($row['cuenta_ban']);
			$datos[$i]['alias']= trim($row['alias']);
			$datos[$i]['total']= number_format(trim($row['total']),2);
			$i++;
		}
		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../../cheques/css/style.css">         
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<title>Depositos y/o Pagos en Efectivo</title>

<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
</style>
<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="24%" class="texto12">Fecha: <span class="texto9">
      <input type="text" value="<?php echo $fec_recauda;?>" name="fec_recauda" size="7" id="fec_recauda"  class="required" maxlength="10"  style="width:70" />
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle" /></span>
        <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fec_recauda",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger1",	// trigger for the calendar (button ID)
						singleClick    :    true
					});
		</script>
    </td>
    <td width="32%" class="texto12">
      <div align="center"><img src="../../imagenes/consultar.jpg" width="27" height="26" onclick="javascript: ;location.href='fichas_deposito.php?editar=true&fec_recauda='+document.getElementById('fec_recauda').value" /> </div></td>
    <td width="44%" class="texto12"><img src="../../imagenes/enviar_pdf.jpg" name="pdf" id="pdf" width="27" height="26" onclick="VerPDF()" /> </td>
  </tr>
</table>
<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="74%" class="texto12"><div align="center"><strong>FICHAS DE DEPOSITO </strong></div></td>
  </tr>
</table>
<p>&nbsp;</p>
<table align="center" width="80%" border="1">
  <tr bgcolor="#CCCCCC" class="subtituloverde12">
    <td width="104"><div align="center">BANCO</div></td>
    <td width="128"><div align="center">CUENTA</div></td>
    <td width="302"><div align="center">DESCRIPCION</div></td>
    <td width="122"><div align="center">IMPORTE</div></td>
  </tr>
  <?php 
		for($i=0;$i<count($datos);$i++)
		{
	?>
  <tr bgcolor="#CCCCCC" class="texto12">
    <td align="center"><?php echo $datos[$i]['nomcorto'];?></td>
    <td align="center"><?php echo $datos[$i]['cuenta_ban'];?></td>
    <td align="center"><?php echo $datos[$i]['alias'];?></td>
    <td align="right"><?php echo $datos[$i]['total'];?></td>
  </tr>
  <?php 
		}
	?>
</table>
<p>&nbsp;</p>

<table width="80%" border="0" align="center">
  <tr>
    <td width="47%"><div align="center" class="subtituloverde12">Cobranza Oficina</div></td>
    <td width="15%">&nbsp;</td>
    <td width="38%"><div align="center" class="subtituloverde12">Cobranza Campo</div></td>
  </tr>
  <tr>
    <td><table width="100%" height="32" border="0">
      <tr bgcolor="#99FF99" class="texto10">
        <td width="42" class="texto10">Cajero:</td>
        <td width="261"><?php echo $cajero;?></td>
      </tr>
    </table></td>
    <td>&nbsp;</td>
    <td><table width="100%" border="0" height="32">
        <tr bgcolor="#99FF99" class="texto10">
          <td width="34">Del Vale</td>
          <td width="75" align="center"><?php echo $vale_min;?></td>
          <td width="46">Al Vale </td>
          <td width="80" align="center"><?php echo $vale_max;?></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" height="32">
      <tr bgcolor="#99FF99" class="texto12">
        <td width="118">Total Recaudado:</td>
        <td width="185"><strong>$ <?php echo number_format($cob_oficina,2);?></strong></td>
      </tr>
    </table>      <span class="texto12"></span></td>
    <td>&nbsp;</td>
    <td><div align="center" class="texto12">
        <table width="100%" border="0" height="32">
          <tr bgcolor="#99FF99">
            <td width="46%">Total Recaudado:</td>
            <td width="54%"><strong>$ <?php echo number_format($cob_campo,2);?></strong></td>
          </tr>
        </table>
    </div></td>
  </tr>
  <tr bgcolor="#CCFF99">
    <td colspan="3" class="caja_grande"><div align="center">Total General: $ <?php echo number_format($total_general,2);?></div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
