<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
require_once("../../../dompdf/dompdf_config.inc.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$datos=array();
$datos['mensaje']="";
$datos['fallo']=false;
$datos['path']="";

$usuario = "";
if(isset($_COOKIE['ID_my_site']))
	$usuario = $_COOKIE['ID_my_site'];
	
$cajero="";
if(isset($_POST['cajero']))
	$cajero=$_POST['cajero'];
	
$banco="";
if(isset($_POST['banco']))
	$banco=$_POST['banco'];
	
$CajeroGral="";
if(isset($_POST['CajeroGral']))
	$CajeroGral=$_POST['CajeroGral'];
	
$total="";
if(isset($_POST['total']))
	$total=$_POST['total'];
	
$monedas=array();
if(isset($_POST['monedas']))
	$monedas=$_POST['monedas'];
	
$nomcajero=array();
if(isset($_POST['nomcajero']))
	$nomcajero=$_POST['nomcajero'];

$nomCajeroGral=array();
if(isset($_POST['nomCajeroGral']))
	$nomCajeroGral=$_POST['nomCajeroGral'];

$bancoNombre=array();
if(isset($_POST['bancoNombre']))
	$bancoNombre=$_POST['bancoNombre'];

$origen="";
if(isset($_POST['origen']))
	$origen=$_POST['origen'];	

$nomorigen="";
if(isset($_POST['nomorigen']))
	$nomorigen=$_POST['nomorigen'];	
	
$fecha= date("Y/m/d");
list($years,$month,$days)= explode('/', $fecha);
$diadesem=date("w");
$ffecha = (string)$dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
$ffecha= strtoupper($ffecha);					
$folio=0;
$folio = A_mcobranza_cajero($cajero, $fecha, $banco, $total, $usuario, $CajeroGral, $origen);
//echo "folio regreso= ".$folio;
//print_r($monedas);
if($folio>0)
{
	for($i=0;$i<count($monedas);$i++)
	{
		$msg=A_dcobranza_cajero($folio, $monedas[$i]['cantidad'], $monedas[$i]['denominacion'], $monedas[$i]['monto'], $usuario, $origen);
		if( $msg == "OK" )
		{
			$datos['mensaje']="Se ha registrado la captura exitosamente.";
			$datos['fallo']=false;
		}
		else
		{
			$datos['mensaje']="Se ha registrado la captura exitosamente.";
			$datos['fallo'] = true;
		}
		
	}
}
else
{
	$datos['mensaje']="No se pudo generar el registro.";
	$datos['fallo']=true;
}

//print_r($monedas);
$datos['path']=crearPDF($cajero, $nomcajero, $CajeroGral, $nomCajeroGral,  $fecha, $banco, $bancoNombre, $total, $usuario, $monedas, $ffecha,$folio,$origen,$nomorigen);

echo json_encode($datos);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function A_mcobranza_cajero($cajero, $fecha, $banco, $total, $usuario, $CajeroGral, $origen)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$folio=0;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_ingresos_A_mcobranza_cajero(?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$cajero, &$fecha, &$banco,&$total,&$usuario, &$CajeroGral, &$origen);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$folio= trim($row['folio']);
		}
	}
	
	return $folio;
}

function A_dcobranza_cajero($myfolio, $cantidad, $denominacion, $monto, $usuario, $origen)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
		$tsql_callSP ="{call sp_ingresos_A_dcobranza_cajero(?,?,?,?,?)}";//Arma el procedimeinto almacenado
		$params = array(&$myfolio, &$cantidad, &$denominacion, &$monto, &$usuario);//Arma parametros de entrada
		//print();
		//$params = array(&$myfolio, &$cantidad, &$denominacion, &$monto, &$usuario);
		$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$g=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$folio= trim($row['msg']);
		}
	}
	
	return $folio;
}

function crearPDF($cajero, $nomcajero, $CajeroGral, $nomCajeroGral,  $fecha, $banco, $bancoNombre, $total, $usuario, $monedas, $ffecha,$folio,$origen,$nomorigen)
{
	global  $username_db, $password_db, $odbc_name, $server;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$cuenta_ban="";
	$nomcorto="";
	$alias="";;
	 list($cuenta_ban,$nomcorto,$alias) = get_Datos_Banco($banco);
	/*table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}*/
	$dompdf = new DOMPDF();
	$dompdf->set_paper('letter', "portrait");
	
	$codigo = get_CodigoCalidad();
	$pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	
	
	$pdf->page_text(530, 760, "'.$codigo.'", "", 12, array(0,0,0)); 
	
 } 

</script> ';  

			
	$html='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
		<style>
		body 
		{
			font-family:"Arial";
			font-size:9;
		}
				
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
<body >
	<table style="width:100%;">
		<tr>
			<td style="width:100%;">
				<table style="width:100%;">
					<tr>
						<td style="width:15%; text-align: center;" class="texto14"></td>
					  	<td style="width:70%; text-align: center;" class="texto14">FOMENTO METROPOLITANO DE MONTERREY</td>
						<td style="width:15%; text-align: center;" class="texto14"></td>
					</tr>
					<tr>
						<td style="width:15%; text-align: center;"></td>
					  	<td style="width:70%; text-align: center;" >REPORTE DE COBRANZA DEL '.$ffecha.'</td>
					  	<td style="width:15%; text-align: center;font-size:12px;" >Folio: '.$folio.'</td>
					  </tr>
					<tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="width:100%;">
				<table style="width:100%;">
					<tr>
					   <td style="width:100/3%;">CAJERO '.$cajero.'</td>
					   <td style="width:100/3%;">CTA '.$cuenta_ban.' '.$nomcorto.'</td>
					   <td style="width:100/3%;">'.$alias.' </td>
					</tr> 
				</table>
			</td>
		</tr>
		<tr>
			<td style="width:100%; text-align: center; background:#CCC;" class="texto12bold">DESGLOSE DE MONEDAS</td>
		</tr>
<tr>
	<td style="width:100%; text-align:center;" align="center">
			<table  border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" style="width:100%; text-align:center;" align="center">
                <thead >
                	<tr>
                        <th  style="text-align:center">Cantidad</th>   
                        <th  style="text-align:center">CONCEPTO</th> 
                        <th  style="text-align:center">TOTAL</th>                                       
                    </tr>
                </thead>
                <tbody id="proveedor" name="proveedor" >';
				
						for( $i=0;$i<count($monedas);$i++)
						{
                            
                           $html .='<tr>
                            	<td style="text-align:center;">'.number_format(trim($monedas[$i]['cantidad'])).'</td>
								<td style="text-align:center;">'.trim($monedas[$i]['descrip']).' DE '.number_format(trim($monedas[$i]['denominacion']), 2).'</td>
								<td style="text-align:right;">'.number_format(trim($monedas[$i]['monto']), 2).'</td>
                            </tr>';
						}
						
						for($i=0;$i<(20-count($monedas));$i++)
						{
                            
                           $html .='<tr>
                            	<td style="text-align:center;">&nbsp;</td>
								<td style="text-align:center;">&nbsp;</td>
								<td style="text-align:right;">&nbsp;</td>
                            </tr>';
						}

				$html .='</tbody>
                <tfoot>
                	<tr>
                        <td></td>
                        <td style="text-align:right;">TOTAL:$</td>
                        <td style="text-align:right;">'.number_format($total,2).'</td>
                    </tr>
                </tfoot>
			</table>
		</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width:100%; text-align:center;" align="center">
			<table style="width:100%; text-align:center;" align="center">
				<tr>
					<td style="width:40%;">&nbsp;</td>
					<td style="width:20%;>&nbsp;&nbsp;</td>
					<td style="width:40%;>&nbsp;</td>
				</tr>
				<tr>
					<td><hr></td>
					<td>&nbsp;&nbsp;</td>
					<td><hr></td>
				</tr>
				<tr>
					<td style="text-align:center;">CAJERO</td>
					<td>&nbsp;</td>
					<td style="text-align:center;">CAJERO GENERAL</td>
				</tr>
				<tr>
					<td style="text-align:center;">'.$nomcajero.'</td>
					<td>&nbsp;&nbsp;</td>
					<td style="text-align:center;">'.$nomCajeroGral.'</td>
				</tr>
			</table>
		<td>
	</tr>
</table>
'.$pageheadfoot.'
</body>
</html>';//$nomcajero, $CajeroGral, $nomCajeroGral
$dompdf->load_html($html);
$dompdf->render();
$pdf = $dompdf->output(); 

file_put_contents("../pdf_files/reporteCobranza_".$folio.".pdf", $pdf);

return("pdf_files/reporteCobranza_".$folio.".pdf");
}

function get_Datos_Banco($banco)
{
	global  $username_db, $password_db, $odbc_name, $server;
	
	$cuenta_ban = "";
	$nomcorto = "";
	$alias = "";
			
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="SELECT cuenta_ban, nomcorto, alias FROM egresosmbancos WHERE banco=".$banco;//Arma el procedimeinto almacenado
	
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$cuenta_ban = trim($row['cuenta_ban']);
			$nomcorto = trim($row['nomcorto']);
			$alias = trim($row['alias']);
		}
	}
	
	return array($cuenta_ban,$nomcorto,$alias);
}


function get_CodigoCalidad()
{
	global  $username_db, $password_db, $odbc_name, $server;
	
	$codigo = "";
			
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="SELECT codigo FROM configdcodigos WHERE descrip='DESGLOSE' AND estatus=0";//Arma el procedimeinto almacenado
	
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$codigo = trim($row['codigo']);
		
		}
	}
	
	return $codigo;
}
?>