<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
require_once("../../../dompdf/dompdf_config.inc.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$usuario = "";
if(isset($_COOKIE['ID_my_site']))
	$usuario = $_COOKIE['ID_my_site'];
	
$folio="";
if(isset($_POST['xfolio']))
	$folio=$_POST['xfolio'];
	
if($folio>0)
{
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$tsql_callSP ="{call sp_ingresos_B_mcobranza_cajero(?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$folio,  &$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$fails=false;
	
	sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );

	$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//

	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$g=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$datos['mensaje']= trim($row['msg']);
			$datos['fallo']=false;
		}
	}
}
else
{
	$datos['mensaje']="No ha seleccionado un folio valido de Captura diaria de Cobranza.";
	$datos['fallo']=true;
}


echo json_encode($datos);


?>