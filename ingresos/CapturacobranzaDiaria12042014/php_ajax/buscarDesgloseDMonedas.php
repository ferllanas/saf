<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$command= "";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

/*
$cajero=0;
if(isset($_REQUEST['cajero']))
	$cajero=$_REQUEST['cajero'];
$datosCajero= explode ( ".-" , $cajero);
$cajero = substr($datosCajero[0],2,6);
*/

if($conexion)
{
	$command = "select id, descrip, monto from ingresosmtipodenominacion a WHERE estatus<9000  ORDER BY orden";	
	
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']= trim($row['id']);
			$datos[$i]['descrip']= utf8_decode(trim($row['descrip']));
			$datos[$i]['monto']= "$".number_format(trim($row['monto']),2);
			$i++;
		}
	}
	
}
echo json_encode($datos);
?>