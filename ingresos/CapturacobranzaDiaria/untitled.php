<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body style='width: 601px; margin-top:30px'>
	<table style="width:100%;">
	<tr>
	  <td style="width:100%; text-align: center;">FOMENTO METROPOLITANO DE MONTERREY</td></tr>
	<tr>
	  <td style="width:100%; text-align: center;">REPORTE DE COBRANZA DEL $fecha</td></tr>
	<tr>
    	<td style="width:100%;">
		  <table style="width:100%;">
			<tr>
				<td style="width:100/3%;">'.$cajero." ".$nomcajero.'</td>
			   <td style="width:100/3%;">'.$banco." ".$bancoNombre.'</td>
               <td style="width:100/3%;">ENGANCHE </td>
			</tr> 
		</table>
</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td style="width:100%; text-align:center;" align="center">
			<table id="Exportar_a_Excel" border="1" class="tablesorter" cellpadding="2" cellspacing="0" style="width:100%; border-color:#000; border-width:medium; border-style:solid; ">
 				<caption style="background:#CCC; color:#FFF;width:450px">DESGLOSE DE MONEDAS</caption>
                <thead >
                	<tr>
                        <th width="150px" style="text-align:center">Cantidad</th>   
                        <th width="150px" style="text-align:center">CONCEPTO</th> 
                        <th width="150px" style="text-align:center">TOTAL</th>                                       
                    </tr>
                </thead>
                <tbody id="proveedor" name="proveedor" >
                	<tr>
                            	<td style="text-align:center;">23405</td>
								<td style="text-align:center;">BILLETES DE  1,000</td>
								<td style="text-align:right;">23405000.00</td>
                    </tr>
                </tbody>
                <tfoot>
                	<tr>
                        <td></td>
                        <td style="text-align:right;">TOTAL:$</td>
                        <td style="text-align:right;">23405000.00</td>
                    </tr>
                </tfoot>
			</table>
		</td>
    </tr>
</table>
</body>
</html>
