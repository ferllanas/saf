<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
require_once("../../../dompdf/dompdf_config.inc.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$consulta= "";
$datos= array();

$html = '';
$cob_campo = 0;
$vale_min  = "";
$vale_max  = "";

	$fec_recauda="29/04/2014";
	
	if(isset($_REQUEST['fec_recauda']))
		$fec_recauda = $_REQUEST['fec_recauda'];	

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	if(strlen($fec_recauda) > 1)
	{
		list($dia,$mes , $anio) = explode("/", $fec_recauda);
	//	$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
		$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
		$fecha_recauda=$dia." DE ".substr($meses,$mes*10-10,10)." DE ".$anio ;


		list($dd2,$mm2,$aa2)= explode('/',$fec_recauda);
		$fecha=$aa2.'-'.$mm2.'-'.$dd2;
		$fecha_r=$aa2.$mm2.$dd2;
		
		$consulta = "select isnull(sum(imppag+pagmor-bonmor-impbon),0) + 
		(select isnull(sum(imppag+pagmor-bonmor-impbon),0) as pagado from fomedbe.dbo.otrserdcreditos where 
		fecpago='$fecha_r' and oficre='IC' and cvesit<'23509000') + 
		(select isnull(sum(impcap+pagmor-bonmor),0) as pagado from fomedbe.dbo.otringdingresos where 
		fecpago='$fecha_r' and oficre='IC' and cvesit<'23909000') as pagado, 
		MIN(numvale) as vale_min, MAX(numvale) as vale_max 
		from fomedbe.dbo.carterdcreditos where fecpago='$fecha_r' and oficre='IC' and cvesit<'22109000'";
	
		if ($conexion)
		{
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;	
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$cob_campo = trim($row['pagado']);
				$vale_min  = trim($row['vale_min']);
				$vale_max  = trim($row['vale_max']);
	
			}
		}
		sqlsrv_free_stmt( $R);
	// cobranza en Oficinas del dia 8 de abril
		$consulta2 ="select right(a.caja,4) as caja ,B.DESCRIPCION,sum(a.pago) as pago from (select CAJA,impcap+pagmor-bonmor as pago from fomedbe.dbo.otringdingresos 
                         where fecpago>='20140908' and cvesit<'23909000' and right(tipocre,4)<>'8304' and right(tipocre,4)<>'88104' and right(tipocre,4)<>'8404' and right(tipocre,4)<>'8204' and oficre='IN'
                         UNION all
					select caja,imppag+pagmor-bonmor-impbon as pago from fomedbe.dbo.otrserdcreditos 
                         where fecpago>='20140908' and oficre='IN' and cvesit<'23509000' and right(tipocre,1)<>'4' 
                         UNION all
					select CAJA,pagcap+pagif+pagmor-bonmor-boncap-bonif as pago from fomedbe.dbo.carterdcreditos
                                   where fecpago=>='20140908' and oficre='IN'  and right(tipocre,1)<>'4' and cvesit<'22109000') a left join fomedbe.dbo.catsmscat b on a.caja=b.scat  group by caja, descripcion";
		
		$R = sqlsrv_query( $conexion,$consulta2);
		$i=0;	
	
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$cob_oficina = trim($row['pagado']);
			$cajero  = trim($row['cajero'])."  ".trim($row['descripcion']);
		}
		$total_general = $cob_campo + $cob_oficina;
		/////////////////
		$consulta3 = "select a.fecha,a.banco,b.nomcorto,b.cuenta_ban,b.alias,a.total from ingresosmcobranza_cajero a 
		left join egresosmbancos b on a.banco=b.banco where a.estatus<9000 and b.estatus<9000 
		and a.fecha = CONVERT(varchar(12),'$fecha', 103)";
		$R = sqlsrv_query( $conexion,$consulta3);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['banco']= trim($row['banco']);
			$datos[$i]['nomcorto']= trim($row['nomcorto']);
			$datos[$i]['cuenta_ban']= trim($row['cuenta_ban']);
			$datos[$i]['alias']= trim($row['alias']);
			$datos[$i]['total']= number_format(trim($row['total']),2);
			$i++;
		}
	}
	
$html .= '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css" />

<title>Depositos y/o Pagos en Efectivo</title>
<body>

<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="74%" class="texto12"><div align="center"><strong>REPORTE DE DEPOSITOS Y/O PAGOS DEL <?PHP echo $fecha_recauda;?></strong> </div></td>
  </tr>
</table>
<table align="center" width="80%" border="1">
  <tr bgcolor="#CCCCCC" class="subtituloverde12">
    <td width="104"><div align="center">BANCO</div></td>
    <td width="128"><div align="center">CUENTA</div></td>
    <td width="302"><div align="center">DESCRIPCION</div></td>
    <td width="122"><div align="center">IMPORTE</div></td>
  </tr>';
  
		for($i=0;$i < count($datos);$i++)
		{
$html .= '
  <tr bgcolor="#CCCCCC" class="texto12">
    <td align="center">'.$datos[$i]['nomcorto'].'</td>
    <td align="center">'.$datos[$i]['cuenta_ban'].'</td>
    <td align="center">'.$datos[$i]['alias'].'</td>
    <td align="right">'.$datos[$i]['total'].'</td>
  </tr>';
 		}
$html .= '
</table>
<p>&nbsp;</p>
<table width="80%" border="0" align="center">
  <tr>
    <td width="47%"><div align="center" class="subtituloverde12">Cobranza Oficina</div></td>
    <td width="15%">&nbsp;</td>
    <td width="38%"><div align="center" class="subtituloverde12">Cobranza Campo</div></td>
  </tr>
  <tr>
    <td><table width="100%" height="32" border="0">
        <tr bgcolor="#99FF99" class="texto10">
          <td width="42" class="texto10">Cajero:</td>
          <td width="261">'.$cajero.'</td>
        </tr>
    </table></td>
    <td>&nbsp;</td>
    <td><table width="100%" border="0" height="32">
        <tr bgcolor="#99FF99" class="texto10">
          <td width="34">Del Vale</td>
          <td width="75" align="center">'.$vale_min.'</td>
          <td width="46">Al Vale </td>
          <td width="80" align="center">'.$vale_max.'</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" height="32">
        <tr bgcolor="#99FF99" class="texto12">
          <td width="118">Total Recaudado:</td>
          <td width="185"><strong>$ '.number_format($cob_oficina,2).'</strong></td>
        </tr>
      </table>
        <span class="texto12"></span></td>
    <td>&nbsp;</td>
    <td><div align="center" class="texto12">
        <table width="100%" border="0" height="32">
          <tr bgcolor="#99FF99">
            <td width="46%">Total Recaudado:</td>
            <td width="54%"><strong>$ '.number_format($cob_campo,2).'</strong></td>
          </tr>
        </table>
    </div></td>
  </tr>
  <tr bgcolor="#CCFF99">
    <td colspan="3" class="caja_grande"><div align="center">Total General: $ '.number_format($total_general,2).'</div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table align="center" width="80%" border="0">
  <tr>
    <td width="33%"><div align="center">___________________________________</div></td>
    <td width="46%"><div align="center">___________________________________</div></td>
    <td width="21%">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center"><strong>Ing. Maria del Socorro de la Garza Lozano </strong></div></td>
    <td><div align="center"><strong>C. Jose Guadalupe Davila Rodriguez</strong></div></td>
    <td><div align="right">MO00703-2</div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>';
	$dompdf = new DOMPDF();
	$dompdf->set_paper('letter', "landscape");
	$dompdf->load_html($html);
	$dompdf->render();
	$pdf = $dompdf->output(); 
	
	file_put_contents("../pdf_files/ficha_deposito.pdf", $pdf);

	echo json_encode("Ok");

?>