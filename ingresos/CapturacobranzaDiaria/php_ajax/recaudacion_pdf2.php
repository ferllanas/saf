<?php
header("Content-type: text/html; charset=UTF8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
require_once("../../../dompdf/dompdf_config.inc.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	$html = '';
	$consulta= " ";
	$datos= array();
	$cta_ban= array();
	$tot=0;
	$totgral=0;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	//$fec_recauda="19/03/2013";
	
	if(isset($_REQUEST['fec_recauda']))
		$fec_recauda = $_REQUEST['fec_recauda'];	
	

	if(strlen($fec_recauda) > 1)
	{
		list($dia,$mes , $anio) = explode("/", $fec_recauda);
	//	$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
		$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
		$fecha_recauda=$dia." DE ".substr($meses,$mes*10-10,10)." DE ".$anio ;
					
		if ($conexion)
		{
			list($dd2,$mm2,$aa2)= explode('/',$fec_recauda);
			$fecha=$aa2.'-'.$mm2.'-'.$dd2;
			$b_consulta = "select a.banco,a.alias,a.nomcorto,a.cuenta_ban,a.estatus,
			( SELECT  sum(b.total) FROM ingresosmcobranza_cajero b 
			WHERE  a.banco=b.banco  and b.fecha = CONVERT(varchar(12),'$fecha', 103)  and b.estatus<9000 
			group by b.banco) as tot from egresosmbancos a where a.estatus<9000 order by a.banco,a.nomcorto";			
								
			$R = sqlsrv_query( $conexion,$b_consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$cta_ban[$i]['banco']= trim($row['banco']);
				$cta_ban[$i]['nomcorto']= trim($row['nomcorto']);
				$cta_ban[$i]['cuenta_ban']= trim($row['cuenta_ban']);
				$cta_ban[$i]['alias']= trim($row['alias']);
				if($row['tot']==null)
					$cta_ban[$i]['tot']=0.00;
				   else
					$cta_ban[$i]['tot']= number_format(trim($row['tot']),2);
				$i++;
				$totgral=$totgral + $row['tot'];
			}
				
			
			$consulta = "select a.fecha,a.banco,b.nomcorto,b.cuenta_ban,b.alias,a.total from ingresosmcobranza_cajero a ";
			$consulta .= "left join egresosmbancos b on a.banco=b.banco where a.estatus<9000 and b.estatus<9000 ";
			$consulta .= "and a.fecha = CONVERT(varchar(12),'$fecha', 103)";
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['fecha']= trim($row['fecha']);
				$datos[$i]['banco']= trim($row['banco']);
				$datos[$i]['nomcorto']= trim($row['nomcorto']);
				$datos[$i]['cuenta_ban']= trim($row['cuenta_ban']);
				$datos[$i]['alias']= trim($row['alias']);
				$datos[$i]['total']= number_format(trim($row['total']),2);
				$i++;
			}
		}
	}


$html .= '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table align="center" width="70%" border="0">
  <tr>
    <td width="74%" class="texto12"><div align="center"><strong>RESUMEN DE RECAUDACION DEL '.$fecha_recauda.'</strong> </div>
  </tr>
</table>
<table width="50%" border="1" align="center">
  <tr bgcolor="#CCCCCC" class="subtituloverde">
    <td class="texto10" width="104"><div align="center">BANCO</div></td>
    <td class="texto10" width="128"><div align="center">CUENTA</div></td>
    <td class="texto10" width="302"><div align="center">DESCRIPCION</div></td>
    <td class="texto10" width="122"><div align="center">IMPORTE</div></td>
  </tr>';
 
		for($i=0;$i<count($cta_ban);$i++)
		{
$html .= '
		  <tr class="texto10">
			<td class="texto10" width="104" align="center">'.$cta_ban[$i]['nomcorto'].'</td>
			<td class="texto10" width="128" align="center">'.$cta_ban[$i]['cuenta_ban'].'</td>
			<td class="texto10" width="302" align="center">'.$cta_ban[$i]['alias'].'</td>
			<td class="texto10" width="122" align="right">'.$cta_ban[$i]['tot'].'</td>
		  </tr>';
		}
$html .= '
</table>
<table width="50%" border="0" align="center">
  <tr class="texto10">
    <td width="104" align="center">&nbsp;</td>
    <td width="128" align="center">&nbsp;</td>
    <td width="302" align="center"><strong>Total General </strong></td>
    <td width="122" align="right">'.number_format($totgral,2).'</td>  
  </tr>
</table>

<table align="center" width="50%" border="1">
  <tr bgcolor="#CCCCCC" class="subtituloverde">
    <td width="104"><div align="center">BANCO</div></td>
    <td width="128"><div align="center">CUENTA</div></td>
    <td width="302"><div align="center">DESCRIPCION</div></td>
    <td width="122"><div align="center">IMPORTE</div></td>
  </tr>';
		for($i=0;$i<count($datos);$i++)
		{
$html .= '
  <tr class="texto10">
    <td class="texto10" width="104" align="center">'.$datos[$i]['nomcorto'].'</td>
    <td class="texto10" width="128" align="center">'.$datos[$i]['cuenta_ban'].'</td>
    <td class="texto10" width="302" align="center">'.$datos[$i]['alias'].'</td>
    <td class="texto10" width="122" align="right">'.$datos[$i]['total'].'</td>
  </tr>';
		}
$html .= '
</table>

<table width="85%" align="center"  border="0">
  <tr>
    <td class="texto10" align="right">MO00702-3</td>
  </tr>
</table>

</body>
</html>';
	$dompdf = new DOMPDF();
	$dompdf->set_paper('letter', "landscape");
	$dompdf->load_html($html);
	$dompdf->render();
	$pdf = $dompdf->output(); 
	
	file_put_contents("../pdf_files/resumen_recaudacion.pdf", $pdf);

	echo json_encode("Ok");
?>