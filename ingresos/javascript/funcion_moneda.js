function alta()
{
	var usuario=document.getElementById('usuario').value;
	var descri=document.getElementById('descri').value;
	var orden=document.getElementById('orden').value;
	var monto=document.getElementById('monto').value;
	if(descri.length<1)
	{
		alert("Falta capturar descripcion");
		document.getElementById('descri').focus();
		return false;
	}
	if(orden.length<1)
	{
		alert("Falta capturar Orden");
		document.getElementById('orden').focus();
		return false;
	}
	if(parseFloat(document.getElementById('monto').value.replace(/,/g,''))<.01)
	{
		alert("Falta capturar Monto");
		document.getElementById('monto').focus();
		return false;
	}
	
	//alert('php_ajax/alta_tipomoneda.php?descri='+descri+"&monto="+monto+"&orden="+orden+"&usuario="+usuario);
	location.href='php_ajax/alta_tipomoneda.php?descri='+descri+"&monto="+monto+"&orden="+orden+"&usuario="+usuario;
}

function limpia()
{
	document.getElementById('descri').value="";
	document.getElementById('orden').value="";
	document.getElementById('monto').value="";
	document.getElementById('descri').focus();
}


//////////////////////////////////////////////////////////////////////////////////

function getXmlHttpRequestObjectdepto() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqempleado = getXmlHttpRequestObjectdepto(); 

function searchempleado() {
    if (searchReqempleado.readyState == 4 || searchReqempleado.readyState == 0)
	{
        var str = escape(document.getElementById('uvehiculo').value);
        searchReqempleado.open("GET",'php_ajax/queryempleadoincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqempleado.onreadystatechange = handleSearchSuggestempleado;
        searchReqempleado.send(null);
    } 
	 if(str.length<1)
	 {
	 	//document.getElementById('depa').value ="";
//		document.getElementById('search_suggestdepto').value="";
	 }
} 

//Called when the AJAX response is returned.
function handleSearchSuggestempleado() {
	
	//alert("aja 1");
    if (searchReqempleado.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestempleado')
        ss.innerHTML = '';
		//alert(searchReqdepto.responseText);
        var str = searchReqempleado.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOverempleado(this);" ';
				suggest += 'onmouseout="javascript:suggestOutempleado(this);" ';
				suggest += "onclick='javascript:setSearchempleado(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[0]+'@'+tok[1]+'@'+tok[2]+'@'+tok[3]+'">' + tok[0] + ' - '+tok[1]+'</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverempleado(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutempleado(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchempleado(value,clave,abc) 
{
	//alert("aja");
   // document.getElementById('uvehiculo').value = value;
	//alert(document.getElementById('cvedepto').value);
	var tok =clave.split("@");
//	alert(tok[0]);
	//$('uvehiculo').value = tok[0]+' - '+tok[1];
	$('uvehiculo').value =tok[0].substring(2,8)+' - '+tok[1];
	var uvehiculo = document.getElementById('uvehiculo').value;
	document.getElementById('numemp').value=tok[0].substring(0,8);
	document.getElementById('search_suggestempleado').innerHTML = '';
}


function agregar()
{
	var usuario = document.getElementById('usuario').value;
	var numemp = document.getElementById('numemp').value;
	var cajero = document.getElementById('cajero').value;
	var cajerox = document.getElementById('uvehiculo').value;
	
	if(cajerox.length<1)
	{
		alert("Falta Capturar Empleado");
		document.getElementById('uvehiculo').focus();
		return false;
	}
	if(cajero.length<1)
	{
		alert("Falta Capturar Cajero");
		document.getElementById('cajero').focus();
		return false;
	}
	
	//alert('php_ajax/busca_cajero.php?numemp='+numemp);
	new Ajax.Request('php_ajax/busca_cajero.php?numemp='+numemp,
	{onSuccess : function(resp) 
		{
			//alert(resp.responseText);
			if( resp.responseText )
			{
				var myArray = eval(resp.responseText);
				//alert("22 "+resp.responseText);
				//alert(myArray.length);
				if(myArray.length>0)
				{
					alert ("Num. de Cajero ya Existe, Intente con otro");
					document.getElementById('uvehiculo').value="";
					document.getElementById('cajero').value="";
					document.getElementById('uvehiculo').focus();
					return false
				}
				else
				{
					location.href='php_ajax/altacajero.php?usuario='+usuario+"&numemp="+numemp+"&cajero="+cajero;
				}
			}
		}
	});
	

	//location.href='php_ajax/altacajero.php?usuario='+usuario+"&numemp="+numemp;
}


function borrar(obj,id)
{
	var usuario = document.getElementById('usuario').value;
	location.href='php_ajax/cancela_cajero.php?id='+id+"&usuario="+usuario;
}	