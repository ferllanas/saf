<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
			date_default_timezone_set('America/Mexico_City');
			
	require_once("../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	$id_paso = trim($_REQUEST['id_paso']);
	//$concepto = "";
	$command= "select * from ingresosmtipodenominacion WHERE id=$id_paso and estatus<9000";
	$stmt = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
	{
		$id=$row['id'];
		$descrip=utf8_decode($row['descrip']);
		$orden=$row['orden'];					
		$monto=$row['monto'];
		$i++;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="../prototype/jQuery.js"></script>

<script language="javascript">
	function graba_cat()
	{
		var id_paso=document.getElementById('id').value;	
		var descri=document.getElementById('descri').value;
		var orden=document.getElementById('orden').value;
		var monto=document.getElementById('monto').value;
		//location.href="php_ajax/cambia_concepto.php?folio="+folio+"&connvo="+connvo;
					var datas = {
						 pid_paso: id_paso,
						 pdescri: descri,
						 porden: orden,
						 pmonto: monto,
						 };
					
					jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'php_ajax/cambia_cat.php',
						dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(request);
						 alert ( " Can't do because: " + error );
						},
						success: function(resp)
						{ console.log(resp);
							var myObj = resp;
							
							///NOT how to print the result and decode in html or php///
							console.log(myObj);
							
							if(resp.length>0)
							{
								//alert(resp);
								window.close();
							}
							else
							{
								alert("Actualizacion Rechazada.");
							}
						}
						
						});
	}

	function cerrar()
	{
		window.close();
		//window.parent.close()
	}


</script>

<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="722" height="319" border="0" align="center">
  <tr>
    <td colspan="4" class="subtituloverde12">Catalogo de Moneda </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="92"><input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
      <span class="texto10">Id</span></td>
    <td width="480"><input type="text" name="id" id="id" size="8" readonly style="text-align:right" value="<?php echo $id;?>" ></td>
    <td width="60">&nbsp;</td>
    <td width="62">&nbsp;</td>
  </tr>
  <tr>
    <td class="texto10">Descripcion</td>
    <td><input type="text" name="descri" id="descri" size="60" value="<?php echo $descrip;?>"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto10">Orden</td>
    <td><input type="text" name="orden" id="orden" size="8" style="text-align:right" value="<?php echo $orden;?>" onKeyPress="return aceptarSoloNumeros(this, event);" ></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto10">Monto</td>
    <td><input class="texto10" type="text" name="monto" id="monto" size="15px" value="<?php echo $monto;?>" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" style="text-align:right"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="texto10">
      <input type="button" name="grabar2" value="Guardar" onClick="graba_cat()">
    </span></td>
    <td><span class="texto10">
      <input type="button" name="Submit" value="Cancelar" onClick="cerrar()">
    </span></td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
