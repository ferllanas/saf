<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$usuario = $_COOKIE['ID_my_site'];
	$idPantalla=0;
	if(isset($_REQUEST['nivel']))
	{
		$idPantalla=$_REQUEST['nivel'];
		$hour = time() + 144000; 
		setcookie('lastPantalla', $idPantalla, $hour); 
	}
	else
		if(isset($_COOKIE['lastPantalla']))
			$idPantalla=$_COOKIE['lastPantalla'];
		
	$privilegios = get_Privilegios($usuario, $idPantalla);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>

<!-- CALENDARIO -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo getDirAtras(getcwd());?>calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar-setup.js"></script>
<!-- CALENDARIO -->
<title>Reporte de Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Servicios Pendientes de Asignar</h1>
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
    </tr>
</table>

<table width="50%" align="center">
    <tr>
   	<td width="50%" valign="top" align="center">
                <table  id="thetable" cellspacing="0" align="left" width="100%">
                  <thead>
                    <caption class="cabezales">Pendientes</caption>
                        <tr>
                            <th class="titles" align="center" width="50%">Usuario</th>
                            <th class="titles" align="center" width="50%">Cantidad</th>
                        </tr>
                  </thead>
                  <tbody>
                    <?php
                        $command="SELECT COUNT(a.usuario) as cantidad, c.nombre, c.appat, c.apmat
  FROM [fomeadmin].[dbo].[serviciomservicios] a 
   LEFT JOIN nomemp.dbo.v_nomina_nomiv_nomhon6_nombres_activos_depto_dir c 
                    ON a.usuario collate database_default  =c.numemp collate database_default
                    WHERE  a.estatus=0
                    GROUP BY a.usuario, c.nombre, c.appat, c.apmat";
                        //echo $command;
                        $getProducts = sqlsrv_query( $conexion_srv,$command);
                        
                        if ( $getProducts === false)
                        { 
                            $resoponsecode="02";
                            $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
                        }
                        else
                        {
                            $i=0;
                            $resoponsecode="Cantidad rows=".count($getProducts);
                            while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                            {		
                    ?>
                    
                        <tr <?php if($i==0) echo 'class="first"';?>>
                            <td align="left"><?php echo utf8_encode($row['nombre'].' '. $row['appat']);?></td>
                            <td align="center"><?php echo trim($row['cantidad']);?></td>
                        </tr>
                    <?php
                            
                                $i++;
                            }
                        }
                    ?>
                    </tbody>
                </table>
    </td>
    </tr>
</table>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>