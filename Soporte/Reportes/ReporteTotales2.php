<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$usuario = $_COOKIE['ID_my_site'];
	$idPantalla=0;
	if(isset($_REQUEST['nivel']))
	{
		$idPantalla=$_REQUEST['nivel'];
		$hour = time() + 144000; 
		setcookie('lastPantalla', $idPantalla, $hour); 
	}
	else
		if(isset($_COOKIE['lastPantalla']))
			$idPantalla=$_COOKIE['lastPantalla'];
		
	$privilegios = get_Privilegios($usuario, $idPantalla);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>

<!-- CALENDARIO -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo getDirAtras(getcwd());?>calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar-setup.js"></script>
<!-- CALENDARIO -->
<title>Reporte de Servicios</title>
</head>
<body style="background:#f8f8f8; text-align:center;" >
<!-- -------- Titulo -------- -->
<table width="50%" align="center">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Resumenes</h1>
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
    </tr>
    <tr>
    	<td>
        <form action="ReporteTotales.php" method="post">
        	<table>
            <tr>
    	<td width="21%" class="texto8" scope="row" >Fecha Inicial:
                    <input name="fecini" type="text" size="7" id="fecini" value="<?php if(isset($_REQUEST['fecini'])) echo $_REQUEST['fecini']; else echo date('Y/m/d');?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:document.getElementById('optfecha').checked=true">
					<img src="<?php echo getDirAtras(getcwd());?>calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%Y/%m/%d",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td>
				  <td width="76%" class="texto8" scope="row" >Fecha Final:
                    <input name="fecfin" type="text" size="7" id="fecfin" value="<?php if(isset($_REQUEST['fecfin'])) echo $_REQUEST['fecfin']; else echo date('Y/m/d');?>" class="texto8" maxlength="10"  style="width:70"  onFocus="javascript:document.getElementById('optfecha').checked=true">
					<img src="<?php echo getDirAtras(getcwd());?>calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Calendario" align="absmiddle">
                    <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%Y/%m/%d",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td>
                <td colspan="2">
                	<input type="submit" value="Consultar" />
                </td>
                </tr>
                </table>
                </form>
		</td>
    </tr>
</table>

<?php
if(isset($_REQUEST['fecfin']) && isset($_REQUEST['fecini']))
{
?>
<!-- -------- Contenidos -------- -->
<table width="100%" align="center">
	<tr>
    	<td width="50%" align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td width="50%" align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td width="50%" align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td width="50%" align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td width="50%" align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td width="50%" align="center">&nbsp;</td>
    </tr>
    <tr>
        <td width="50%" valign="top" align="center">
        		<table width="100%" align="center">
    					<caption><h2>------------------------------------------------ del <?php echo $_REQUEST['fecini'];?> al  <?php echo $_REQUEST['fecfin'];?>-------------------------------------------------------------</h2></caption>
                	<tr>
                    	<td width="50%" align="right" valign="top">
                        	 <table  id="thetable" cellspacing="0" align="center" >
                                <thead>
                                    <caption class="cabezales">Solicitudes</caption>
                                        <tr>
                                            <th class="titles" align="center">Usuario</th>
                                            <th class="titles" align="center">Capturo</th>
                                        </tr>
                                </thead>
                                 <tbody>
                                    <?php
                                        $command="SELECT  a.usuario, COUNT(a.usuario) as cantidad , b.nomemp
                                FROM serviciomservicios a  
                                LEFT JOIN nomemp.dbo.v_nomina_nomiv_nomhon6_nombres_activos_depto_dir b 
                                    ON a.usuario collate database_default  =b.numemp collate database_default  
                                WHERE falta>='".$_REQUEST['fecini']."' AND falta<='".$_REQUEST['fecfin']."' GROUP BY usuario, b.nomemp ORDER BY nomemp";
                                        //echo $command;
                                        $getProducts = sqlsrv_query( $conexion_srv,$command);
                                        
                                        if ( $getProducts === false)
                                        { 
                                            $resoponsecode="02";
                                            $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
                                        }
                                        else
                                        {
                                            $i=0;
                                            $resoponsecode="Cantidad rows=".count($getProducts);
                                            while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                                            {		
                                    ?>
                                        <tr <?php if($i==0) echo 'class="first"';?>>
                                            <td align="left"><?php echo utf8_encode($row['nomemp']);?></td>
                                            <td align="center"><?php echo trim($row['cantidad']);?></td>
                                        </tr>
                                    <?php
                                            
                                                $i++;
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </td>
                        <td width="50%" align="left" valign="top">
                        	 <table  id="thetable" cellspacing="0" align="center">
                                  <thead>
                                    <caption class="cabezales">Atendidas</caption>
                                        <tr>
                                            <th class="titles" align="center">Tecnico</th>
                                            <th class="titles" align="center">Atendio</th>
                                        </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                        $command="SELECT COUNT(b.usuario) as cantidad, c.nombre, c.appat, c.apmat
                  FROM [fomeadmin].[dbo].[serviciomservicios] a 
                  left join  [fomeadmin].[dbo].serviciodservicios b ON a.id=b.idservicio
                   LEFT JOIN nomemp.dbo.v_nomina_nomiv_nomhon6_nombres_activos_depto_dir c 
                                    ON b.usuario collate database_default  =c.numemp collate database_default
                                    WHERE  a.estatus=0
                                    GROUP BY b.usuario, c.nombre, c.appat, c.apmat";
                                        //echo $command;
                                        $getProducts = sqlsrv_query( $conexion_srv,$command);
                                        
                                        if ( $getProducts === false)
                                        { 
                                            $resoponsecode="02";
                                            $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
                                        }
                                        else
                                        {
                                            $i=0;
                                            $resoponsecode="Cantidad rows=".count($getProducts);
                                            while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                                            {		
                                    ?>
                                        <tr <?php if($i==0) echo 'class="first"';?>>
                                            <td align="left"><?php echo utf8_encode($row['nomemp']);?></td>
                                            <td align="center"><?php echo trim($row['cantidad']);?></td>
                                        </tr>
                                    <?php
                                            
                                                $i++;
                                            }
                                        }
                                    ?>
                                </tbody>
                                </table>
                        </td>
                    </tr>
                </table>
               
        </td>
   </tr>
   <tr>
   	<td>&nbsp;</td>
   </tr>
    <tr>
   	<td>&nbsp;</td>
   </tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>-------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr><td>&nbsp;</td></tr>
   <tr>
   		<td width="50%" valign="top" align="center">
    		<table align="center" width="100%">
            	
            	<tr>
                	<td width="50%" align="center" valign="top">
                    	<table  id="thetable" cellspacing="0" align="center">
                          <thead>
                            <caption class="cabezales">Por Asignar</caption>
                                <tr>
                                    <th class="titles" align="center">Usuario</th>
                                    <th class="titles" align="center">Capturo</th>
                                </tr>
                          </thead>
                          <tbody>
                            <?php
                                $command="SELECT a.numemp, nomemp, COUNT(a.numemp) as cantidad FROM servicioasigna a
                        LEFT JOIN nomemp.dbo.v_nomina_nomiv_nomhon6_nombres_activos_depto_dir b 
                            ON a.numemp collate database_default  =b.numemp collate database_default
                        WHERE a.estatus=0 GROUP BY a.numemp, nomemp";
                                //echo $command;
                                $getProducts = sqlsrv_query( $conexion_srv,$command);
                                
                                if ( $getProducts === false)
                                { 
                                    $resoponsecode="02";
                                    $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
                                }
                                else
                                {
                                    $i=0;
                                    $resoponsecode="Cantidad rows=".count($getProducts);
                                    while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                                    {		
                            ?>
                            
                                <tr <?php if($i==0) echo 'class="first"';?>>
                                    <td align="left"><?php echo utf8_encode($row['nomemp']);?></td>
                                    <td align="center"><?php echo trim($row['cantidad']);?></td>
                                </tr>
                            <?php
                                    
                                        $i++;
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </td>
                    <td width="50%" align="center" valign="top">
                    	 <table  id="thetable" cellspacing="0" align="center" >
                          <thead>
                            <caption class="cabezales">Pendientes</caption>
                                <tr>
                                    <th class="titles" align="center" width="50%">Tecnico</th>
                                    <th class="titles" align="center" width="50%">Total</th>
                                </tr>
                          </thead>
                          <tbody>
                            <?php
                                $command="SELECT COUNT(a.usuario) as cantidad, c.nombre, c.appat, c.apmat
          FROM [fomeadmin].[dbo].[serviciomservicios] a 
           LEFT JOIN nomemp.dbo.v_nomina_nomiv_nomhon6_nombres_activos_depto_dir c 
                            ON a.usuario collate database_default  =c.numemp collate database_default
                            WHERE  a.estatus=0
                            GROUP BY a.usuario, c.nombre, c.appat, c.apmat";
                                //echo $command;
                                $getProducts = sqlsrv_query( $conexion_srv,$command);
                                
                                if ( $getProducts === false)
                                { 
                                    $resoponsecode="02";
                                    $descriptioncode= FormatErrors( sqlsrv_errors()) ; 
                                }
                                else
                                {
                                    $i=0;
                                    $resoponsecode="Cantidad rows=".count($getProducts);
                                    while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
                                    {		
                            ?>
                            
                                <tr <?php if($i==0) echo 'class="first"';?>>
                                    <td align="left"><?php echo utf8_encode($row['nombre'].' '. $row['appat']);?></td>
                                    <td align="center"><?php echo trim($row['cantidad']);?></td>
                                </tr>
                            <?php
                                    
                                        $i++;
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
		</td>
    </tr>
</table>
<?php
}
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>