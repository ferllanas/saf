<?php
require("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

/*print_r($_REQUEST);

 if(isset($_REQUEST['mes']) && $_REQUEST['mes']==07)
 	echo "Julio";*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        <h1>Indicadores</h1>
        </td>
        </tr>
        <tr>
        <td>
        <form action="indicadoresV2.php" method="post">
        	<table>
            	<tr>
                	<td>A&ntilde;o</td>
                    <td>
                    	<input type="text" value="<?php if(isset($_REQUEST['annio'])) echo $_REQUEST['annio']; else echo date("Y");?>" id="annio" name="annio" />
                    	<!--<select id="annio" name="annio">
                    	<option value="2013" <?php if($_REQUEST['annio']==2013)  echo "SELECTED"; else if(date("Y")==2013) echo "SELECTED";?>>2013</option>
                    	<option value="2014" <?php if($_REQUEST['annio']==2014)  echo "SELECTED"; else if(date("Y")==2014) echo "SELECTED";?>>2014</option>
                        <option value="2014" <?php if($_REQUEST['annio']==2015)  echo "SELECTED"; else if(date("Y")==2015) echo "SELECTED";?>>2014</option>
                    </select>-->
                    </td>
                    <?php $selected=false;
					//echo $selected;
					?>
                     <td><select id="mes" name="mes" onchange="javascript: document.forms[0].submit();">
                    	<option value="01" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==01 ) {  echo "SELECTED"; $selected=true;} else{ if( !$selected)if(date("m")==01) echo "SELECTED";}?>>Enero</option>
                    	<option value="02" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==02 ) {  echo "SELECTED"; $selected=true;} else{if( !$selected)  if(date("m")==02) echo "SELECTED";}?>>Febrero</option>
                        <option value="03" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==03 ) {   echo "SELECTED"; $selected=true;} else{if( !$selected) if(date("m")==03) echo "SELECTED";}?>>Marzo</option>
                        <option value="04" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==04 ) {   echo "SELECTED"; $selected=true;} else{if( !$selected) if(date("m")==04) echo "SELECTED";}?>>Abril</option>
                        <option value="05" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==05 ) {   echo "SELECTED"; $selected=true;} else{if( !$selected) if(date("m")==05) echo "SELECTED";}?>>Mayo</option>
                        <option value="06" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==06 ) {  echo "SELECTED"; $selected=true;} else{  if( !$selected)if(date("m")==06) echo "SELECTED";}?>>Junio</option>
                        <option value="07" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==07 ) {   echo "SELECTED"; $selected=true;} else{ if( !$selected)if(date("m")==07) echo "SELECTED";}?>>Julio</option>
                        <option value="08" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==08 ) {   echo "SELECTED"; $selected=true;} else{ if( !$selected)if(date("m")==08) echo "SELECTED";}?>>Agosto</option>
                        <option value="09" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==09 ) {  echo "SELECTED"; $selected=true;} else{ if( !$selected) if(date("m")==09) echo "SELECTED";}?>>Septiembre</option>
                        <option value="10" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==10 ) {   echo "SELECTED"; $selected=true;} else{ if( !$selected)if(date("m")==10) echo "SELECTED";}?>>Octubre</option>
                        <option value="11" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==11 ) {   echo "SELECTED"; $selected=true;} else{ if( !$selected)if(date("m")==11) echo "SELECTED";}?>>Noviembre</option>
                        <option value="12" <?php  if(isset($_REQUEST['mes']) && $_REQUEST['mes']==12 ) {  echo "SELECTED"; $selected=true;} else{  if( !$selected) if(date("m")==12) echo "SELECTED";}?>>Diciembre</option>
                    </select>
                    </td>
                    <td><input type="submit" value="Consultar" /></td>
                </tr>
            </table>
        </form>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table id="thetable" cellspacing="0" align="left" style="text-align:left;width:10%;">
	 <caption class="cabezales">Indicadores</caption>
  <thead>
    	<tr>
        	<th class="titles" align="center">Concepto</th>
            <th class="titles" align="center">Cantidades</th>
        </tr>
  </thead>
    <?php
	if(isset($_REQUEST['annio']) && isset($_REQUEST['mes']))
	{
    	$command='select COUNT(id) as estimado, sum(case when mins<=30 then 1 else 0 end) as realizado from (select a.id, 
      (DATEDIFF("day",c.fasig,b.falta)*24*60)+DATEDIFF("MINUTE",c.hasig,b.hora) as mins 
from serviciomservicios a 
inner join servicioasigna c on a.id=c.idservicio 
inner join 
(SELECT bb.usuario,bb.falta,bb.hora,bb.idasignacion 
            FROM serviciomdiagnostico bb inner join servicioddiagnostico cc on bb.id=cc.iddiagnostico and cc.idcatdiagnostico<>22
            where YEAR(bb.falta)='.$_REQUEST['annio'].' 
            group by bb.usuario,bb.falta,bb.hora,bb.idasignacion) b 
on c.id=b.idasignacion AND YEAR(B.FALTA)='.$_REQUEST['annio'].' AND MONTH(B.FALTA)='.$_REQUEST['mes'].' 
WHERE YEAR(a.falta)='.$_REQUEST['annio'].' AND MONTH(a.falta)='.$_REQUEST['mes'].' ) pr';

		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		$datos=array();
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
					$datos[0]['concepto']="Estimado";
					$datos[0]['valor']=number_format($row['estimado'],0);
				
					$datos[1]['concepto']="Meta";
					$datos[1]['valor']=number_format(($row['estimado']*0.80),0);
				
					$datos[2]['concepto']="Realizado";
					$datos[2]['valor']=number_format($row['realizado'], 0);
				
					$datos[3]['concepto']="%";
					
					if((float)$datos[1]['valor'] > 0 )
						$datos[3]['valor']=number_format(($row['realizado']/$datos[1]['valor'])*100,0);
					
				
			}
		}
		?>
			<tbody>
        <?php
			for($i=0;$i<count($datos);$i++)
			{
		?>
            <tr <?php if($i==0) echo 'class="first"';?>>
        		<td align="center"><?php echo $datos[$i]['concepto'];?></td>
            	<td align="center"><?php if(isset($datos[$i]['valor'])) echo $datos[$i]['valor'];?></td>
            </tr>
    <?php
			}
	}
	?>
    		</tbody>
</table>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>