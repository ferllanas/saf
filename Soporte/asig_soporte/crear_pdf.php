<?php
$id=1;
$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
	{	
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Emision de Cheques</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../prototype/prototype.js'></script>
</head>

<body>
";
    if (version_compare(PHP_VERSION, "5.1.0", ">="))
 	require_once("../../dompdf/dompdf_config.inc.php");
	date_default_timezone_set("America/Mexico_City");
	require_once("../../connections/dbconexion.php");
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	include("../../Administracion/globalfuncions.php");

 $command="
select a.id,a.numemp,a.numpatrimonio,a.observa,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.hora,108) as hora,
b.numemp as tec,CONVERT(varchar(10),b.fasig,103) as fecasig,CONVERT(varchar(10),b.hasig,108) as hasig,
rtrim(ltrim(c.nombre))+' '+rtrim(ltrim(c.appat))+' '+rtrim(ltrim(c.apmat)) as nomemp,
rtrim(ltrim(d.nombre))+' '+rtrim(ltrim(d.appat))+' '+rtrim(ltrim(d.apmat)) as nomtec,
CONVERT(varchar(10),f.falta,103) as fecdiag,CONVERT(varchar(10),f.hora,108) as hdiag,f.observa as diag,
e.folio, e.descrip, e.marca, e.modelo
from serviciomservicios a 
left join servicioasigna b on a.id=b.idservicio and b.estatus<90
left join fome2011.dbo.activodmuebles e on a.numpatrimonio=e.folio
left join nomemp.dbo.nominadempleados c on a.numemp=c.numemp collate database_default
left join nomemp.dbo.nominadempleados d on b.numemp=d.numemp collate database_default
left join serviciomdiagnostico f on a.id=f.idservicio 
where a.id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$id=trim($row['id']);
			
			$nomemp = trim($row['nomemp']);
			$nomtec = trim($row['nomtec']);
			$fecha = trim($row['fecha']);
			$hora = trim($row['hora']);
			$fecasig = trim($row['fecasig']);
			$hasig = trim($row['hasig']);
			$fecdiag = trim($row['fecdiag']);
			$hdiag = trim($row['hdiag']);
			$detalle = trim($row['observa']);
			$diag = trim($row['diag']);
			$folio = trim($row['folio']);
			$descrip = trim($row['descrip']);
			$marca = trim($row['marca']);
			$modelo = trim($row['modelo']);
			$equipo=$folio." ".$descrip." ".$marca." ".$modelo;
			
			
		}
		$descrips="";
		$consulta = "select a.id,b.Descripcion from serviciodservicios a left join catservicios b on a.idcatservicio=b.id where idservicio=$id";
		$R = sqlsrv_query( $conexion_srv,$consulta);
		while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$id_s= $row['id'];
			$descrips.= $row['Descripcion']." - ";				
		}
		$descripD="";
			$consulta = "select a.id,b.descripcion from servicioddiagnostico a left join catdiagnostico b on a.idcatdiagnostico=b.id 
left join serviciomdiagnostico c on a.iddiagnostico=c.id where c.idservicio=$id and a.estatus<90";
			//echo $consulta;
			$R = sqlsrv_query( $conexion_srv,$consulta);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id_d= $row['id'];
				$descripD.= $row['descripcion']." - ";				
			}
			echo "<td class='titles'>$descrip </td>";
			
	}
$fecha1 = $fecha." ".$hora;
$fecha2 = $fecdiag." ".$hdiag;
//echo $fecha1. " ".$fecha2;

function getMinuts($fecha1, $fecha2)
{
    $fecha1 = str_replace('/', '-', $fecha1);
    $fecha2 = str_replace('/', '-', $fecha2);
    $fecha1 = strtotime($fecha1);
    $fecha2 = strtotime($fecha2);
    return round(($fecha2 - $fecha1) / 60);
}
if ($fecha==$fecdiag)
	$min= getMinuts($fecha1, $fecha2)/60;
else
	$min= (getMinuts($fecha1, $fecha2)/60)-(getMinuts($fecha1, $fecha2)/1400)*17;

	//echo $importe;
	
//    F.SP Regresa ya los datos con informacion, separando los que regresa el SP y los que ya vienen desde la captura de la Forma
	
	
	//echo .'***('.$TotalImporte.')***'.\n\n.'
$html.="

<table width='600px' border='0' align='center'>
   	    <tr>
		  <td width='100%'>
                 
	 	  </td>
  	    </tr>  
	    <tr>
			<td width='100%'>
             <table width='600px' border='0'>
              <tr>
                <td align='left' width='20'><img src='../../$imagenPDFPrincipal' width='100' height='38' /></td>
                <td class='texto12' align='center'><b>COORDINACION DE TECNOLOG&Iacute;AS DE LA INFORMACI&Oacute;N<br />                  
              	</tr>
			    <tr>
					<td width='100%' colspan=2 align='right' class='texto12' ><b>SOLICITUD : ". $id ."</b></td><br>
				</tr>				  
            </table>
			 <hr>
				<table width='600px'>				  
				   <tr>
					<td width='100%' align='left' class='texto9' ><b>SOLICITANTE :</b> ". $nomemp ."</td><br>
				   </tr>
				   <tr>
					<td width='70%' align='left' class='texto9'><b>EQUIPO: </b> ". $equipo ."</td>
					</tr>
					<tr>
					 <td width='30%' align='left' class='texto9'><b>FECHA SOLICITUD: </b>".$fecha." ".$hora."</td>
				   </tr>
				</table>			
           
            <table width='600px' border='0'>
              <tr>
                <td width='100%' align='left' class='texto9'><b>FALLA REPORTADA: </b>  ".$descrips."</td>
			</tr>
			 <tr>
                <td width='100%' align='left' class='texto9'><b>OBSERVACIONES: </b>  ".$descrip."</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
        </table>
		<hr>
		<table width='600px'>				  
		   <tr>
			<td width='100%' align='left' class='texto9' ><b>TECNICO ASIGNADO :</b> ". $nomtec ."</td><br>
		   </tr>				  
			<tr>
			 <td width='30%' align='left' class='texto9'><b>FECHA DE ASIGNACION: </b>".$fecasig." ".$hasig."</td>
		   </tr>
		   <tr>
			<td width='100%' align='left' class='texto9' ><b>ACCIONES REALIZADAS :</b> ". $descripD ."</td><br>
		   </tr>
		   <tr>
			<td width='70%' align='left' class='texto9'><b>DIAGNOSTICO: </b> ". $diag ."</td>
			</tr>
			<tr>
			 <td width='30%' align='left' class='texto9'><b>FECHA DE DIAGNOSTICO: </b>".$fecdiag." ".$hdiag."</td>
		   </tr>
		</table>			
   		<hr>
		<table>
		<tr>
			<td width='30%' colspan='5' align='left' class='texto12'><b>TIEMPO DE RESPUESTA: ".$min." HRS</b></td>
		</tr>
		  <tr>
    	<td class='texto9'>Calificaci&oacute;n del Servicio:</td>
        <td colspan='8' width='1094' class='texto9'>&nbsp;&nbsp;10&nbsp;<input type='radio' name='califica' id='califica' value='10'/>&nbsp;&nbsp;9&nbsp;<input type='radio' name='califica' id='califica' value='9'/>&nbsp;&nbsp;8&nbsp;<input type='radio' name='califica' id='califica' value='8'/>&nbsp;&nbsp;7&nbsp;<input type='radio' name='califica' id='califica' value='7'/>&nbsp;&nbsp;6&nbsp;<input type='radio' name='califica' id='califica' value='6'/> </td>
    </tr>
		</table>
        <table width='600px' border='0'>
      <tr>
                <td width='31%'>&nbsp;</td>
                <td width='4%'>&nbsp;</td>               
                <td width='40%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>TECNICO</b></td>
                <td class='texto8'>&nbsp;</td> 
				 <td align='center' class='texto8'><b>SOLICITANTE</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>               
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>               
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
			   <tr>
                <td align='center' valign='bottom' class='texto8' ></td>
                <td class='texto8'>&nbsp;</td>               
                <td align='center'  valign='bottom' class='texto8'></td>
              </tr>
              <tr>
                <td align='center' valign='bottom' class='texto8' ><hr></td>
                <td class='texto8'>&nbsp;</td>               
                 <td align='center'  valign='bottom' class='texto8'><hr></td>
              </tr>			  
              <tr>
                <td align='center' class='texto8'><B>".$nomtec."</B></td>
                <td class='texto8'>&nbsp;</td>  
                <td align='center' class='texto8'><B>".$nomemp."</B></td>
              </tr>              
            </table>			
    <table width='100%' border='0'>
              
              <tr>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'></td>
             </tr>
            </table>
	 	  </td>
  	    </tr>  
	  </table>
	</body>
</html>";	

  echo $html;
  /*
*/
// Variables de impresiÃ³n del sistema de Almacen a PDF

  $dompdf = new DOMPDF();
  $dompdf->set_paper("letter","portrait");
  $dompdf->load_html($html);
  $dompdf->render();
 
  $pdf = $dompdf->output(); 
  file_put_contents("$id.pdf", $pdf);
 // $dompdf->stream("../pdf_files/$folio.pdf");
 echo json_encode($id);
  
// Se genera la informacion a insertar en el sp_egresos_A_msolche
//function fun_creaCheque($numprov,$depto,$importe,$concepto, $usuario, $uresp, $nomresp, factura) se incluira factura en el store

//Recibo los valores a insertar en el procedimiento almacenado ....
?>