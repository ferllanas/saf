<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$usuario = $_COOKIE['ID_my_site'];
	$idPantalla=0;
	if(isset($_REQUEST['nivel']))
	{
		$idPantalla=$_REQUEST['nivel'];
		$hour = time() + 144000; 
		setcookie('lastPantalla', $idPantalla, $hour); 
	}
	else
		if(isset($_COOKIE['lastPantalla']))
			$idPantalla=$_COOKIE['lastPantalla'];
		
	$privilegios = get_Privilegios($usuario, $idPantalla);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<title>Contratos</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Catalogo de Diagnosticos &nbsp;&nbsp;</h1>
            &nbsp;<input class="btn" type="button" value="Registar Catalogo de Diagnostico"  onclick="javascript: location.href='editarcatdiagnostico.php'"/>
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table style="width:100%;" id="thetable" cellspacing="0" align="center">
  <thead>
    	<tr>
        	<td class="titles" align="left">Descripción</td>
            <td class="titles" align="left">Fecha Alta</td>
            <td class="titles" align="center">Situaci&oacute;n</td>           
            <td class="titles" align="center">&nbsp;</td>
            <td class="titles" align="center">&nbsp;</td>
        </tr>
    <?php
    	$command="SELECT a.id,a.descripcion,convert(varchar(10),a.fecalta,103) as fecha,a.estatus from catdiagnostico a  where a.estatus<100";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		//	$consulta = "SELECT a.id,a.descripcion,a.fecalta,a.usuario,b.nombre from catservicios a left join usuario b on a.usuario=b.idusuario where a.estatus<100";
			//$rs3 = mysql_query($consulta, $conexion) or die(mysql_error());
			
		$i=0;
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
			
	?>
    	<tr <?php if($i==0) echo 'class="first"';?>>
        	<td><?php echo $row['descripcion'];?></td>
            <td><?php echo trim($row['fecha']);?></td>
            <td align="center"><?php if($row['estatus']<90) echo "Activo"; else echo "Inactivo";?></td>
            <td align="center">
             <?php
			///echo revisaPrivilegiosBorrar($privilegios);
				if( revisaPrivilegioseditar($privilegios))
				{
			?><a class="edit" href="editarcatdiagnostico.php?id=<?php echo trim($row['id']);?>"><img src="../../imagenes/edit.png" title="Editar" />
            <div class="clr"></div>
            Editar</a> <?php
				}
			?></td>
            <td align="center">
             <?php
			///echo revisaPrivilegiosBorrar($privilegios);
				if( revisaPrivilegiosBorrar($privilegios))
				{
			?>
            <a class="delete" href="eliminacatdiagnostico.php?id=<?php echo trim($row['id']);?>" onclick="return confirm('Esta seguro de eliminar  <?php echo trim($row['descripcion']);?> ?')"><img src="../../imagenes/delete.png" title="Eliminar" />
            <div class="clr"></div>
            Eliminar</a>
            <?php
				}
			?></td>
        </tr>
    <?php
			
				$i++;
			}
		}
	?>
</table>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>