<?php

require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');

$id="0";
$titulo="Agregar Servicio";
if(isset($_REQUEST['id']) && $_REQUEST['id']!="")
{
	$id_s=$_REQUEST['id'];
	$titulo="Editar Servicio";
}
if(isset($_REQUEST['idasigna']) && $_REQUEST['idasigna']!="")
{
	$idasig=$_REQUEST['idasigna'];	
}
$nombre="";

$descripcion = "";
$estatus = 0;


if(isset($_REQUEST['id']))
{	
	$command="SELECT * FROM catservicios where id=$id";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$id=trim($row['id']);
			$descripcion = trim($row['Descripcion']);
			$estatus=$row['estatus'];
			
		}
	}
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../css/biopop.css">
<!--llamado al plugin y estilos del jquery ui para datepicker y timepicker--> 
<script type="text/javascript" src="../tecnico/javascript/jquery.js"></script>
<script type="text/javascript" src="../tecnico/javascript/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="../tecnico/javascript/jquery-ui-timepicker-addon.js"></script>
<!--llamado a la hoja de estilos para que la mascara tenga un ambiente agradable (sino se ve transparente)-->
<link type="text/css" href="../tecnico/javascript/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script type="text/javascript">
//ala funcion jQuery "$()" se le pasa una funcion que le asigne a el elemento con id txtFecha el metodo datepicker (que le aplica la mascara de fecha)
$(function() {
	$("#txtFecha").datepicker();
});
//lo mismo que la funcion anterior solo que en este caso aplicamos timepicker (mascara de seleccion de hora )
//IMPORTANTE LOS {} DEBEN IR DENTRO DEL PARENTESIS, solo me detendre a decir que sin ellos muestra mascara de fecha y hora en uno solo
//quitenlos para que vean la diferencia.
$(function() {
	$("#haten").timepicker({});
});
</script>

<script type="text/javascript" src="../tecnico/javascript/diagnostico.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>

<!-- Load jQuery build -->


<!-- Rutina para cargar imagen previa  -->

<style>
body
{
font-family:arial;
}
.preview
{
width:200px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
</style>

<title>Servicios</title>
</head>
<body  style="background:#f8f8f8; overflow:scroll;">
<!-- -------- Titulo -------- -->

<div id="" class="titles">Diagnostico de Servicio <?php echo $id_s." - ".$idasig;?></div><br />


<!-- -------- Sección De Proyecto -------- -->
<table width="100%">	 
   <tr>
    <td class="titles">Posible Falla:</td>
    <?php
		if ($conexion_srv)
		{
			$descrip="";
			$consulta = "select a.id,b.Descripcion from serviciodservicios a left join catservicios b on a.idcatservicio=b.id  where idservicio=$id";
			$R = sqlsrv_query( $conexion_srv,$consulta);
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id= $row['id'];
				$descrip.= $row['Descripcion']." - ";				
			}
			echo "<td class='titles'>$descrip </td>";
		}
	?>					
    </tr>   
    <tr>
            <td width="121"><input name="id_s" id="id_s" type="hidden" value="<?php echo $id_s;?>" /><input name="idasig" id="idasig" type="hidden" value="<?php echo $idasig;?>" /></td>
    </tr> 
<tr>
    	  <td width="121" height="39" class="titles">Hora de Atencion:</td>  
          <td> <input type="text"  class="texto8" name="haten" id="haten" style="width:60px;" tabindex="1" > </td>
     </tr>    
      
    </tr>  
    <tr >
    <td class="titles">Acci&oacute;n Realizada:</td>
    </tr>
    <?php
		if ($conexion_srv)
		{
			$consulta = "select * from catdiagnostico where estatus=0";
			$R = sqlsrv_query( $conexion_srv,$consulta);
			echo "<tr><td>&nbsp;</td>";
			$i=0;
			while( $row =sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$id_i= $row['id'];
				$descrip= utf8_encode($row['descripcion']);
				if($i==5)
				{
					echo "</tr>";
					echo "<tr><td>&nbsp;</td>";	
					$i=0;
				}
				$i++;
				echo "<td class='titles'>$descrip <input type='checkbox' name='$id_i' id='$id_i' value=$id_i /></td>";
				
			}
			echo "</tr>";
		}
	?>					
    </tr>    
      <tr>
    	<td class="titles">Observaciones:</td>
        <td colspan="8" width="1094"><textarea cols="100" rows="3" name="observa" id="observa"></textarea></td>
    </tr>   
    </table>
<br />
<div class="clr"></div>
<div id="" class="line"></div>
<br />
<!-- -------- Botones -------- -->
<table width="100%" align="left">
      <tr>
      	<td width="10%">&nbsp;</td>
    	<td width="22%" align="center"><input class="btn" type="button" value="Guardar" onclick="javascript: guardadiagnostico()"/></td>
        <td width="68%" align="center"><input class="btn-suprim" type="button" value="Cancelar" onclick="javascript:location.href='pendientes.php'"/></td>
    
    </table>
    </tr>
</table>
<!--</form>-->
</body>
</html>