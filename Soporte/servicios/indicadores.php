<?php
require("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">
<link rel="stylesheet" type="text/css" href="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.css"/>
<title>Servicios</title>
</head>
<body style="background:#f8f8f8;">
<!-- -------- Titulo -------- -->
<table width="100%">
	<tr>
    	<td id="titulo" width="100%">
        	<h1>Servicios &nbsp;&nbsp; </h1>
            &nbsp;<input class="btn" type="button" value=" Registar Solicitud de Servicios "  onclick="javascript: location.href='editarservicios.php'"/>
    		<div class="clr"></div>
            <div id="" class="line"></div>
        </td>
    </tr>
</table>

<!-- -------- Contenidos -------- -->
<table style="width:90%;" id="thetable" cellspacing="0" align="center">
  <thead>
    	<tr>
        	<td class="titles" align="center">Folio</td>
            <td class="titles" align="center">Solicitante</td>
            <td class="titles" align="center">Falla</td>
            <td class="titles" align="center">Fecha</td>
            <td class="titles" align="center">Hora</td>
            <td class="titles" align="center">Situaci&oacute;n</td>
            <td class="titles" align="center">Técnico</td>
            <td class="titles" align="center">&nbsp;</td>
            <td class="titles" align="center">&nbsp;</td>
        </tr>
    <?php
    	$command="select a.id,a.numemp,b.nomemp as nombre,
a.numpatrimonio,a.estatus,a.observa,CONVERT(varchar(10),a.falta,103) as fecha,CONVERT(varchar(10),a.hora,108) as hora,
 d.nombre as tecnico
from serviciomservicios a 
left join v_nomina_nomiv_nomhon6_nombres_activos_depto_dir b on a.numemp=b.numemp collate database_default  
left join servicioasigna c on a.id=c.idservicio and c.estatus<90
left join nomemp.dbo.nominadempleados d on c.numemp=d.numemp collate database_default
where a.estatus<=30  order by a.id,fecha,hora
";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
 		
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
			
	?>
    	<tr <?php if($i==0) echo 'class="first"';?>>
        	<td align="center"><?php echo $row['id'];?></td>
            <td align="center"><?php echo utf8_encode(trim($row['nombre']));?></td>
            <td align="center"><?php echo trim($row['observa']);?></td>
            <td align="center"><?php echo trim($row['fecha']);?></td>
            <td align="center"><?php echo trim($row['hora']);?></td>
            
            <td align="center"><?php if($row['estatus']==0) echo "Sin Asignar";else { if($row['estatus']>=20) echo "Diagnostico"; else echo "Asignado";}?></td>
            <?php 
				if($row['estatus']==0)
					$tecnico="";
				else
					$tecnico=$row['tecnico'];
			?>
            <?php 
				if($row['estatus']==30) 
					$ruta="../asig_soporte/verificaservicios.php?id=".$row['id']."&estatus=".$row['estatus']; 
				else
				{
					if($row['estatus']<20)
						$ruta="../asig_soporte/asignarservicios.php?id=".$row['id']."&estatus=".$row['estatus'];
					else
						$ruta="#";
				}
			?>
            <td align="center"><?php echo trim($tecnico);?></td>
            <td align="center"><a class="edit" href= "<?php echo $ruta; ?>" ><img src="../../imagenes/asigna.png" title="Editar" />
            <div class="clr"></div>
            <?php if($row['estatus']==0) echo "Asignar"; else { if($row['estatus']==10)  echo "Reasignar"; else { if($row['estatus']==20)echo "Pendiente de Imprimir"; else echo "verificar";} }?></a></td>
            <?php
			if($row['estatus']==0)
			{?>
            <td align="center"><a class="delete" href="eliminaservicios.php?id=<?php echo trim($row['id']);?>" onclick="return confirm('Esta seguro de eliminar el Servicio <?php echo $row['descripcion'];?> ?')"><img src="../../imagenes/delete.png" title="Eliminar" />
            <div class="clr"></div>
            Eliminar</a></td>
            <?php
            }
			else
			{
				echo "<td>&nbsp;</td>";
			}
			?>
            
        </tr>
    <?php
			
				
			}
		}
	?>
</table>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascript/farinspace-jquery.tableScroll-af4eca8/jquery.tablescroll.js"></script>
<script>
jQuery(document).ready(function($)
{
	$('#thetable').tableScroll({height:300});
	$('#thetable2').tableScroll();
});
</script>
</body>
</html>