<?php
    if (version_compare(PHP_VERSION, "5.1.0", ">="))
	date_default_timezone_set("America/Mexico_City");
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	include("../../../Administracion/globalfuncions.php");

//	$cuenta 	 = strrpos($_REQUEST["cuenta"],".");
	$cuenta 	 = $_REQUEST["cuenta"];
	$subcuenta   = $_REQUEST["subcuenta"];
	$sctanombre  = $_REQUEST["sctanombre"]; // Es el que traera el detalle del padre
	$univel		 = $_REQUEST["univel"];		
	$tfnivel='0';
	if($univel==1)
		$tfnivel='1';
	$usuario 	 = $_REQUEST["usuario"];

	$tipo_cta 	 = $_REQUEST["tipo_cta"];
	$anio_ini 	 = $_REQUEST["anio_ini"];
	$naturaleza_cta 	 = $_REQUEST["naturaleza_cta"];
	
	fun_creaCuentas($cuenta,$sctanombre,$subcuenta, $usuario,$tfnivel, $tipo_cta ,$anio_ini, $naturaleza_cta);
	
function fun_creaCuentas($cuenta,$sctanombre,$subcuenta, $usuario,$univel,$tipo_cta ,$anio_ini, $naturaleza_cta)
{
	global $server,$odbc_name,$username_db ,$password_db;
	$fails=false;		
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion

	// A.SP Una vez almacenados los datos en la variable del procedimiento almacenado se procede a llamarlo para indicar cuantos campos se van a ingresar
	$cheqsql_callSP ="{call sp_contab_A_mcuentas(?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado (Llamado al procedimiento)
	$params = array(&$cuenta,&$sctanombre,&$subcuenta,&$univel,&$usuario,&$tipo_cta,&$anio_ini,&$naturaleza_cta);//Arma parametros de entrada al sp_egresos_A_mcuentas	
	//echo $cheqsql_callSP;
	//print_r($params);
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 segundos		
	$stmt = sqlsrv_query($conexion, $cheqsql_callSP, $params);
    // Verifica que informacion trae $stmt
	if( $stmt === false )
	{
		 $fails="Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)." sp_contab_A_mcuentas ". print_r($params,true));
	}	
	if(!$fails)
	{
		// B.SP Arrastra la iformacion desde el SP
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
		// C.SP Antes de recibir los datos del procedimiento almacenado se validan las variables		
			$fecha = $row["fecha"];
			$hora = $row["hora"];
		// D.SP En este caso los valores que entrega el procedimiento almacenado se les puede dar un formato especial acorde a las necesidades
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
			//$cvedepto = $row["cvedepto"];			
		}
		sqlsrv_free_stmt( $stmt);
	}	
	sqlsrv_close( $conexion);
		// E.SP En esta momento regresa la informacion a almacenar en el Array
		//	return array($fecha,$hora);
  //}
}
		// F.SP Regresa ya los datos con informacion, separando los que regresa el SP y los que ya vienen desde la captura de la Forma
		
		//echo json_encode($subcuenta);  
?>