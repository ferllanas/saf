<?php
if (version_compare(PHP_VERSION, "5.1.0", ">="))
		date_default_timezone_set("America/Mexico_City");
		
require_once("../../connections/dbconexion.php");
$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
	$usuario = $_COOKIE["ID_my_site"]; //Misma variable para Almacen
// 	$depto = $_COOKIE["depto"]; //Misma variable para Almacen
// 	$privsolche = $_COOKIE["privsolche"]; //Misma variable para Almacen

//$usuario 	 = 1981;
$falta		 = date("Y-m-d");
$halta 		 = date("H:i:s");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Captura de Cuentas Contables</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/style.css">         
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script src="javascript/catcuentasinc.js" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/provfuncion.js"></script>
<script language="javascript" src="javascript/valida.js"></script>
<script language="javascript" src="javascript/catcuentasinc.js"></script>
<script language="javascript" src="javascript/catsubcuentasinc.js"></script>
<script language="javascript" src="javascript/addcatcontab.js"></script>
<style type='text/css'>
	bordes 
	{	
		border-left:none;
		border-right:none;
	}
</style>
</head>
<body>
<form id="form1" name="form1" method="post" action="">
  <blockquote>
    <p align="center" class="TituloDForma">CATALOGO DE CUENTAS <br /></p>
    <hr class="hrTitForma" />
  </blockquote>
  <input type="hidden" class="texto10" size="6" name="usuario" id="usuario" value="<?php echo "$usuario";?>" />
  <input type="hidden" class="texto10" size="6" name="primernivel" id="primernivel" value="<?php echo "$primernivel";?>" />
  <input type="hidden" class="texto10" size="6" name="ultimonivel" id="ultimonivel" value="<?php echo "$ultimonivel";?>" />
  <input type="hidden" class="texto10" size="6" name="falta" id="falta" value="<?php echo "$falta";?>" />      
  <input type="hidden" class="texto10" size="6" name="halta" id="halta" value="<?php echo "$halta";?>" />      
  <input type="hidden" class="texto10" size="4" name="ctax" id="ctax" />
  <input type="hidden" class="texto10" size="4" name="ctanombrex" id="ctanombrex" />  
  <input type="hidden" class="texto10" size="4" name="padrex" id="padrex" />    
  <table width="68%" height="128" style="border-style:groove" align="center">
    <tr>
      <td colspan="3" class="subtituloverde12" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td width="25%" class="texto10" align="right">Cuenta Superior:</td>
      <td>
        <div align="left"  style="z-index:3; position:relative; width:300px; top: 0px; left: 0px; height: 21px;">
          <input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="1" style="background:#EBEBEB" onkeyup="searchCuentas(this);" size="10%" autocomplete="off" height="12"> <!-- onblur = "alert('salio'); "-->
          <input type="hidden" class="texto10" size="4" name="sctax" id="sctax" style="background:#EBEBEB" height="12" readonly="readonly" />
  <div id="search_suggestCuentas" style="z-index:4; "></div>
      </div></td>
      <td><span class="texto10">Nombre :</span>         <input type="text" tabindex="50" class="texto8" readonly="readonly" size="60" name="ctanombre" id="ctanombre" /></td>
    </tr>
    <tr>
      <td colspan="3" class="bordes">&nbsp;</td>
    </tr>
    <tr>
      <td class="texto10" align="right">Cuenta:</td>
      <td width="13%"><label>
        <input type="text" name="subcuenta" class="texto8" size="10%" id="subcuenta" height="12" style="background:#EBEBEB" tabindex="3" />
      </label></td>
      <td width="62%" class="texto8"><span class="texto10">Descripción : </span>        <input type="text" class="texto8" size="60" tabindex="4" name="sctanombre" id="sctanombre" /></td>
    </tr>
    <tr>
    	<td colspan="3">
        	<table width="100%">
            	<tr>
                	<td>Año Inicial:<input type="text" id="anio_ini" name="anio_ini" style="width:50px" tabindex="5" /></td>
                    <td>Tipo de Cuenta:<select name="tipo_cta" id="tipo_cta" tabindex="6" >
                    							<?php 
													$command  = "select id, descrip from contabmtipo_cta " ;
													$getProducts = sqlsrv_query( $conexion_srv,$command);
													if ( $getProducts === false)
													{ 
														$resoponsecode="02";
														$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
														//echo $descriptioncode;
													}
													else
													{
													
														while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
														{
																									?>
                    										<option value="<?php echo $row['id'];?>"><?php echo $row['descrip'];?></option>
                                                <?php 
														}
													}
												?>  
                                                 </select>
                    </td>
                    <td>Naturaleza de la Cuenta:<select name="naturaleza_cta" id="naturaleza_cta" tabindex="7" >
                    							<?php 
													$command  = "select id, descrip from contabmnaturaleza_cta " ;
													$getProducts = sqlsrv_query( $conexion_srv,$command);
													if ( $getProducts === false)
													{ 
														$resoponsecode="02";
														$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
														//echo $descriptioncode;
													}
													else
													{
													
														while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
														{
																									?>
                    										<option value="<?php echo $row['id'];?>"><?php echo $row['descrip'];?></option>
                                                <?php 
														}
													}
												?>  
                                                 </select></td>
                             
                </tr>
            </table>
        </td>
    </tr>
    <tr>
      <td colspan="3" class="texto10"><p>
        <label>
          <div align="center">
            <input name="univel" type="checkbox" id="univel" tabindex="8"  value="1" checked />
            Ultimo Nivel       </label>
        <p align="center">
          <input type="button" name="validar" id="validar" tabindex="9" onclick="prev_guarda_datos()" value="Guardar"/>
      </p></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
</form>
</body>
</html>