//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectSubCtas() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqSubCtas = getXmlHttpRequestObjectSubCtas(); 

function searchSubCuentas() {
	//document.getElementById('cuenta').value = 0;
    if (searchReqSubCtas.readyState == 4 || searchReqSubCtas.readyState == 0)
	{
			var str = escape(document.getElementById('subcuenta').value);
			//alert (str);
				searchReqSubCtas.open("GET",'php_ajax/querysubctasincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
				searchReqSubCtas.onreadystatechange = handleSearchSuggestSubCtas;
				searchReqSubCtas.send(null);
    }       
} 

//Called when the AJAX response is returned.
function handleSearchSuggestSubCtas() {	
    if (searchReqSubCtas.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestSubCuentas')
        ss.innerHTML = '';
		//alert(searchReqSubCtas.responseText);
        var str = searchReqSubCtas.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				//alert(tok[0]);
				var suggest = '<div onmouseover="javascript:suggestOverSubCtas(this);" ';
				suggest += 'onmouseout="javascript:suggestOutSubCtas(this);" ';
				suggest += "onclick='javascript:setSearchSubCtas(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[0]+'@'+tok[1]+'@'+tok[2]+' ">' + tok[2] +' - '+ tok[1] + '</div>';				
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverSubCtas(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutSubCtas(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchSubCtas(value,idprod) 
{
		var tok =idprod.split("@");
		document.getElementById('subcuenta').value = tok[0];
		document.getElementById('sctanombre').value = tok[1];
		document.getElementById('ctax').value = tok[0];
		document.getElementById('sctax').value = tok[2];				
		document.getElementById('search_suggestSubCuentas').innerHTML = '';
}