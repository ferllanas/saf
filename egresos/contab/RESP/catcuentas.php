<?php
if (version_compare(PHP_VERSION, "5.1.0", ">="))
		date_default_timezone_set("America/Mexico_City");
		
require_once("../../connections/dbconexion.php");
$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//	$usuario = $_COOKIE["ID_my_site"]; //Misma variable para Almacen
// 	$depto = $_COOKIE["depto"]; //Misma variable para Almacen
// 	$privsolche = $_COOKIE["privsolche"]; //Misma variable para Almacen

$usuario 	 = 1981;
$falta		 = date("Y-m-d");
$halta 		 = date("H:i:s");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Captura de Cuentas Contables</title>
<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script src="javascript/catcuentasinc.js" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/provfuncion.js"></script>
<script language="javascript" src="javascript/catcuentasinc.js"></script>
<script language="javascript" src="javascript/catsubcuentasinc.js"></script>
<script language="javascript" src="javascript/addcatcontab.js"></script>
<style type='text/css'>
	bordes 
	{	
		border-left:none;
		border-right:none;
	}
</style>
</head>
<body>
<form id="form1" name="form1" method="post" action="">
  <p class="seccionNota" align="center"></p>
  <input type="hidden" class="texto10" size="6" name="usuario" id="usuario" value="<?php echo "$usuario";?>" />
  <input type="hidden" class="texto10" size="6" name="primernivel" id="primernivel" value="<?php echo "$primernivel";?>" />
  <input type="hidden" class="texto10" size="6" name="ultimonivel" id="ultimonivel" value="<?php echo "$ultimonivel";?>" />
  <input type="hidden" class="texto10" size="6" name="falta" id="falta" value="<?php echo "$falta";?>" />      
  <input type="hidden" class="texto10" size="6" name="halta" id="halta" value="<?php echo "$halta";?>" />      
  <input type="hidden" class="texto10" size="4" name="ctax" id="ctax" />
  <input type="hidden" class="texto10" size="4" name="sctax" id="sctax" />  
  <table width="54%" height="128" border="1" align="center">
    <tr>
      <td colspan="3" class="subtituloverde12" align="center">CATALOGO DE CUENTAS </td>
    </tr>
    <tr>
      <td width="15%" class="texto10">CTA SUPERIOR:</td>
      <td width="26%">
   <div align="left"  style="z-index:3; position:absolute; width:540px; top: 81px; left: 433px; height: 21px;">
   <input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="0" style="background:#EBEBEB" onkeyup="searchCuentas(this);" size="10%" autocomplete="off" height="12">
   <input type="hidden" class="texto10" size="4" name="sctax" id="sctax" style="background:#EBEBEB" height="12" readonly="readonly" />
<div id="search_suggestCuentas" style="z-index:4; "></div>
   </div>
   </td>
      <td width="59%"><input type="text" class="texto8" tabindex="1" size="60" name="ctanombre" id="ctanombre" /></td>
    </tr>
    <tr>
      <td colspan="3" class="bordes">&nbsp;</td>
    </tr>
    <tr>
      <td class="texto10">CUENTA</td>
      <td><label>
        <input type="text" name="subcuenta" class="texto8" size="10%" id="subcuenta" height="12" style="background:#EBEBEB" />
      </label></td>
      <td><input type="text" class="texto8" size="60" tabindex="3" name="sctanombre" id="sctanombre" /></td>
    </tr>
    <tr>
      <td colspan="3" class="texto10"><p>
        <label>
          <div align="center">
            <input name="univel" type="checkbox" id="univel" tabindex="4" onclick="value=1" value="0"  />
            ULTIMO NIVEL        </label>
        <p align="center">
          <input type="button" name="validar" id="validar" tabindex="5" onclick="prev_guarda_datos()" value="Salvar"/>
      </p></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
</form>
</body>
</html>