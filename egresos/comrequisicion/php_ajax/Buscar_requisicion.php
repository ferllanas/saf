<?php
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
	
		$fecini=$_GET['fecini'];
		$fecfin=$_GET['fecfin'];		
		$idarea=$_COOKIE['id_area_usuario'];
				
		$comando="select a.requisi,a.entregaen,a.observa,a.depto,b.nomdepto,a.coord,b.nomcoord,a.dir,b.nomdir,a.falta,a.estatus,a.path from compramrequisi a left join nominamdepto b on a.depto=b.depto ";
		if(strlen($fecini)>0 && strlen($fecini)>0)
		{
			$comando.=" WHERE a.falta>='".$fecini."'  and  a.falta<='".$fecfin."' ";			
		}
		else
		{
			if(strlen($fecini)>0)
			{
				$comando.=" WHERE a.falta>='".$fecini."'";			
			}
		}		
		$comando.=" ORDER BY a.requisi";
		$getProducts = sqlsrv_query( $conexion_srv,$comando);
		if ( $getProducts === false)
     	{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
            {
            	$datos[$i]['requisi'] = $row['requisi'];
				$datos[$i]['entregaen'] = html_entity_decode(trim($row['entregaen']));
				$datos[$i]['observa'] = html_entity_decode(trim($row['observa']));
				$datos[$i]['depto'] = $row['depto'];
				$datos[$i]['nomdepto'] = html_entity_decode(trim($row['nomdepto']));
				$datos[$i]['coord'] =$row['coord'];
				$datos[$i]['nomcoord'] = html_entity_decode(trim($row['nomcoord']));
				$datos[$i]['dir'] = $row['dir'];
				$datos[$i]['nomdir'] = html_entity_decode(trim($row['nomdir']));
				$datos[$i]['falta'] =$row['falta'];
				$datos[$i]['estatus'] = $row['estatus'];
				$datos[$i]['path'] =$row['path'];
				$i++;
            }
		}
		sqlsrv_free_stmt( $getProducts );
	}
	else
	{
			$datos[0]['error']="1";
			$datos[0]['string']="no hay conexion";
	}
	echo json_encode($datos);
?>