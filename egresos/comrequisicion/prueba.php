<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<?php
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($conexion)
{		
	$comando="select a.requisi,a.entregaen,a.observa,a.depto,b.nomdepto,a.coord,b.nomcoord,a.dir,b.nomdir,a.falta,a.estatus,a.path from compramrequisi a left join nominamdepto b on a.depto=b.depto ";	
	$comando.=" ORDER BY a.requisi";
	$getProducts = sqlsrv_query( $conexion_srv,$comando);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['requisi'] = $row['requisi'];
			$datos[$i]['entregaen'] = html_entity_decode(trim($row['entregaen']));
			$datos[$i]['observa'] = html_entity_decode(trim($row['observa']));
			$datos[$i]['depto'] = $row['depto'];
			$datos[$i]['nomdepto'] = html_entity_decode(trim($row['nomdepto']));
			$datos[$i]['coord'] =$row['coord'];
			$datos[$i]['nomcoord'] = html_entity_decode(trim($row['nomcoord']));
			$datos[$i]['dir'] = $row['dir'];
			$datos[$i]['nomdir'] = html_entity_decode(trim($row['nomdir']));
			$datos[$i]['falta'] = $row['falta'];
			$datos[$i]['estatus'] = $row['estatus'];
			$datos[$i]['path'] =$row['path'];
			$i++;
		}
	}
}
print_r($datos);
?>
</head>

<body>
</body>
</html>
