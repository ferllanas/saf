//Elimina una menu
function eliminar_ProductoARequisicion( e , s )
{
	//var table=$( 'tmenus' );
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	//alert(rowindexA+","+ $('menus').rows.length);
	$('menus').deleteRow( rowindexA );

}

//Agrega una nueva menu en la lista despencientes
function agrega_ProductoARequisicion()
{	
	var usuario=$('usuario').value;
	
	//alert("!"+$('busProd').value+"!")
	if($('busProd').value.length<=0)
	{
		alert('Selecciona un producto!');
		return false;
	}
	
	var Table = document.getElementById('menus');
	
	//Get reference to table body.
	//var TableBody = Table.firstChild;
	
	//Create the new elements
	var NewRow = document.createElement("tr");
	
	var cellNomProd = document.createElement("td");
	var celluniProd = document.createElement("td");
	var cellDescProd = document.createElement("td");
	var cellCantidadProd = document.createElement("td");
	var cellQuitarCell = document.createElement("td");
	
	
	var inputIdProd = document.createElement("input");
	inputIdProd.type="hidden";
	inputIdProd.id="idprod";
	inputIdProd.name="idprod";
	inputIdProd.value=$('busProd').value;
	
	var inputUniProd = document.createElement("input");
	inputUniProd.type="text";
	inputUniProd.id="uniprod";
	inputUniProd.name="uniprod";
	inputUniProd.className="caja_toprint";
	inputUniProd.style.width="100%";
	inputUniProd.style.align="center";
	inputUniProd.value=$('unidaddPro').value;
	
	var inputnomProd = document.createElement("input");
	inputnomProd.type="text";
	inputnomProd.id="producto";
	inputnomProd.name="producto";
	inputnomProd.className="caja_toprint";
	inputnomProd.style.width="100%";
	inputnomProd.value=$('txtSearch').value;
	
	var inputDescProd = document.createElement("input");
	inputDescProd.type="text";
	inputDescProd.id="desc";
	inputDescProd.name="desc";
	inputDescProd.className ="required";
	
	var inputCantidadProd = document.createElement("input");
	inputCantidadProd.type="text";
	inputCantidadProd.id="cantidad";
	inputCantidadProd.name="cantidad";
	inputCantidadProd.className ="required numeric";
	
	var inputbotonBorrar = document.createElement("input");
	inputbotonBorrar.type="button";
	inputbotonBorrar.setAttribute("onclick", "javascript:eliminar_ProductoARequisicion(event,this);");
//onclick = new function(){eliminar_ProductoARequisicion('event',inputbotonBorrar);}//"alert('MAMA')";//
	inputbotonBorrar.value="-";
	
	//Add textboxes to cells
	cellNomProd.appendChild(inputIdProd);
	cellNomProd.appendChild(inputnomProd);
	celluniProd.appendChild(inputUniProd);
	cellDescProd.appendChild(inputDescProd);
	cellCantidadProd.appendChild(inputCantidadProd);
	cellQuitarCell.appendChild(inputbotonBorrar);
	
	//Add elements to row.
	NewRow.appendChild(cellNomProd);
	NewRow.appendChild(celluniProd);
	NewRow.appendChild(cellDescProd);
	NewRow.appendChild(cellCantidadProd);
	NewRow.appendChild(cellQuitarCell);
	
	//Add row to table
	Table.appendChild(NewRow);

	$('txtSearch').value="";
	$('busProd').value="";
}



function cambiar_UnidadesParaProducto(productoSelected,resp)
{
	//alert("ASASASAS ");
	/*if(resp=='null')
			alert("No existen resultados para estos criterios.");
		//got an array of suggestions.
		alert(resp);
		var myArray = eval(resp.responseText);*/
}

function validaDatos()
{
	if(checkForm($('form_requisicion')))//Esta funcion se encuentra en el archivo funciones.js
	{
		return false;
	}
	else
		return true;
}

function GuardarRequisicion()
{
	if(!validaDatos())
	{
		alert("Favor de revisar la información.");
		return false;
	}
		
	var id_req=$('id_req').value;
	
	if(id_req.length>0)
	{
		alert("No puede registrarse de nuevo esta requisición, intente creando una nueva requisición, y cancele esta si es necesario!.");
		return false;
	}
		
	var usuario=$('usuario').value;
	var departamento="";//
	var entreganen=$('entreganen').value;
	var observaciones=$('observaciones').value;
	
	var table=$('menus');
	
	//Ciclo para obtener los productos de la requisicion
	var cadena_productosDRequi="(";
	for (var i = 0; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_productosDRequi+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="idprod")
					cadena_productosDRequi+='"producto"=>"'+mynode.value+'",';
					
													
				if(mynode.name=="cantidad")
					cadena_productosDRequi+='"cantidad"=>"'+mynode.value+'",';
				
				if(mynode.name=="desc")
					cadena_productosDRequi+='"descripcion"=>"'+mynode.value+'",';
				
				if(mynode.name=="uniprod")
					cadena_productosDRequi+='"uniprod"=>"'+mynode.value+'",';
				
			}
		}
		cadena_productosDRequi=cadena_productosDRequi.substring(0,cadena_productosDRequi.length-1);
		cadena_productosDRequi+='),';
	}
	
	
	cadena_productosDRequi = cadena_productosDRequi.substring(0,cadena_productosDRequi.length-1);
	cadena_productosDRequi += ')';
	
	//Verifica que halla agregado productos a la requisicion
	if(cadena_productosDRequi.length<5)
	{
		alert('Favor de agregar almenos un producto.');
		return false;
	}
		
	/*alert('php_ajax/comrequisicion_CrearRequisicion.php?usuario='+usuario+
	'&departamento='+departamento+
	'&observaciones='+observaciones+
	'&entreganen='+entreganen+
	'&id_req='+id_req+
	'&productos='+cadena_productosDRequi);				*/
	new Ajax.Request('php_ajax/comrequisicion_CrearRequisicion.php?usuario='+usuario+
				'&departamento='+departamento+
				'&observaciones='+observaciones+
				'&entreganen='+entreganen+
				'&id_req='+id_req+
				'&productos='+cadena_productosDRequi,
				{onSuccess : function(resp) 
					{
						if( resp.responseText ) 
						{
							//alert(resp.responseText);
							var myArray = eval(resp.responseText);
							if(myArray.length>0)
							{
								//alert(myArray[0].fallo);
								if(myArray[0].fallo==false)
								{
									alert("La requisicion ha sido registrada!.");
									$('id_req').value=myArray[0].id;
									$('msgnumreq').style.visibility='visible';
									$('id_req').style.visibility='visible';
									document.form_requisicion.submit();//$('form_requisicion').submit;
									//location.reload(true);
								}
								else
								{
									alert("Error en proceso de actualizacion!.");
								}
							}
						}
					}
				});
}

function busquedaIncrementalProductos(inputTermino)
{
	//new Ajax.Updater('coches', 'php_ajax/query.php?q='+this.value, {method: 'get' })
	var termino=inputTermino.value;
	var productoDom=$('producto');
	productoDom.options.length = 0;
	
	//alert('php_ajax/query.php?q='+termino);
	new Ajax.Request('php_ajax/query.php?q='+termino,
					{onSuccess : function(resp) 
						{
							if( resp.responseText ) 
							{
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									for( var ii = 0; ii < myArray.length; ii++ ) 
									{
										var popular1 = new Option( myArray[ii].valor,myArray[ii].id,"","");
										popular1.title=myArray[ii].valor;
										productoDom[ii] =popular1;
									}
									
									productoDom.size =10;
								}
							}
						}
					});
}


function tomarValorDComboyCambiarInput(combo,input)
{
	if(combo.selectedIndex >= 0)
		input.value = combo.options[combo.selectedIndex].text;
		
	combo.size=0;
}

//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('txtSearch').value);
        searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	
    if (searchReq.readyState == 4) 
	{
        var ss = document.getElementById('search_suggest')
        ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		if(str.length<31)
		   for(i=0; i < str.length - 1; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split(";");
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    document.getElementById('txtSearch').value = value;
	var tok =idprod.split(";");
	$('busProd').value = tok[0];
	$('unidaddPro').value = tok[1];
    document.getElementById('search_suggest').innerHTML = '';
}
