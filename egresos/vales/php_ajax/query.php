<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if( isset($_GET['q']) && isset($_GET['tipo']) && isset($_GET['prov']) && $conexion)
{
	$tipo=$_GET['tipo'] ;
	$prov=$_GET['prov'] ;
	$termino=$_GET['q'];
	//echo $tipo;
	if($tipo=='st')
	{
		$command= "select a.orden,a.surtido from compramsurtido a 
											INNER JOIN compramordenes b on a.orden=b.orden
								where a.surtido  LIKE '%$termino%' 
								and b.prov=$prov 
								and a.estatus=0 
								and b.estatus=0 group by a.orden,a.surtido";	
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			die($command."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['id']= trim($row['surtido']);
				$datos[$i]['surtido']= trim($row['surtido']);
				$sct.= $datos[$i]['id'].";".$datos[$i]['surtido'].";".$datos[$i]['id']. "\n";
				$i++;
			}
		}
		echo $sct;
	}
	else
	{
		if($tipo==="nc")
		{
			$command= "select a.folio,b.tipo, b.iddocto from egresosmncredito a INNER JOIN egresosdncredito b ON a.folio=b.folio where a.folio LIKE '%$termino%' and a.estatus=0 ";	
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r( sqlsrv_errors(), true));
			}
			else
			{
			
				$resoponsecode="Cantidad rows=".count($getProducts);
				$i=0;
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$datos[$i]['id']= trim($row['folio']);
					$datos[$i]['surtido']= trim($row['iddocto']);
					$sct.= $datos[$i]['id'].";".$datos[$i]['surtido'].";".$datos[$i]['id']. "\n";
					$i++;
				}
			}
			echo $sct;
		}
		else
		{
			if($tipo=='anc')
			{
				$command= "select a.folio,b.tipo, b.iddocto from egresosmncredito a INNER JOIN egresosdncredito b ON a.folio=b.folio where a.folio LIKE '%$termino%' and a.estatus=10  GROUP BY a.folio,b.tipo, b.iddocto";	
				//echo $command;
				$getProducts = sqlsrv_query( $conexion_srv,$command);
				if ( $getProducts === false)
				{ 
					$resoponsecode="02";
					die($command."". print_r( sqlsrv_errors(), true));
				}
				else
				{
				
					$resoponsecode="Cantidad rows=".count($getProducts);
					$i=0;
					while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
					{
						$datos[$i]['id']= trim($row['folio']);
						$datos[$i]['surtido']= trim($row['iddocto']);
						$sct.= $datos[$i]['id'].";".$datos[$i]['surtido'].";".$datos[$i]['id']. "\n";
						$i++;
					}
				}
				echo $sct;
			}
		}
	}
}
//echo $command;
?>