
<?php
//include('../../../pdf/class.ezpdf.php');
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
require_once("../../../dompdf/dompdf_config.inc.php");
//$usarioCreador = $_COOKIE['ID_my_site'];	

$numvale="";
if(isset($_GET['id']))
	$numvale=$_GET['id'];
	
$fecha="";
$nomprov="";
$numprov="";
$concepto="";
$totalVale="";
$fechaPago="";
$quienReviso="";
$totalValeLetra="";
$tfacturas="";
//////////////////////////////////////////////////////////////////////////////////////
$command="SELECT a.concepto, a.prov,  a.subtotal, a.iva, a.total, a.saldo, a.usuario,
			CONVERT(varchar(12),a.fpago,103)as fpago, CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta, b.nomprov, c.nombre as quienReviso
  FROM egresosmvale a INNER JOIN compramprovs b ON a.prov=b.prov INNER JOIN menumusuarios c ON c.usuario=a.usuario WHERE a.vale='$numvale'";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$concepto=trim($row['concepto']);
			$numprov= trim($row['prov']);
			$nomprov= strtoupper(trim($row['nomprov']));
			$fechaPago= trim($row['fpago']);
			$totalVale= trim($row['total']);
			$fecha= trim($row['falta']);
			$quienReviso= trim($row['quienReviso']);
		}
	}
//echo redondear_dos_decimal($totalVale);
$totalValeLetra ="***".strtoupper (num2letras(redondear_dos_decimal($totalVale)) )."***";

$totalVale=number_format($totalVale,2);
$command="SELECT factura, SUM(total) as total FROM egresosdvale WHERE vale='$numvale' AND tipodocto IN(1,3) GROUP BY factura";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$tfacturas.="<tr><td class='texto11'>".trim($row['factura'])."</td><td class='texto11'>".number_format(trim($row['total']),2)."</td></tr>";
			
		}
	}
$command="SELECT numdocto,  total FROM egresosdvale WHERE vale='$numvale' AND tipodocto=4";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$tfacturas.="<tr><td class='texto11'>".trim($row['numdocto'])."</td><td class='texto11'>-".number_format(trim($row['total']),2)."</td></tr>";
			
		}
	}
$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
<style>
body 
{
	font-family:'Arial';
	font-size:8;
}

table {
  
	vertical-align:top;
	border-collapse:collapse; 
	border: none;
}
td {padding: 0;}

#twmaarco
{
	border: 1pt solid black;
}

.textobold{
	font-weight:bold;
}
.texto14{ 
	font-size: 15pt;
	font-weight:bold;
}

.texto12{ 
	font-size: 12pt;
}
.texto12bold
{
	font-size: 12pt;
	font-weight:bold;
}
.texto11{
	font-size: 11pt;
}
</style>

</head>

<body>
<table style='width: 90%;margin-top: 2em; margin-left: 2em; margin-right: 3em;' width='100%'>
	<tr>
		<td style='width: 45%;' width='45%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 100%; height: 22px;' colspan='2'>
						<img src='../../../$imagenPDFPrincipal' style='width: 100px; height: 38px;'>
					</td>
				</tr>
				<tr>
					<td style='width: 100%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
				</tr>
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 70%; vertical-align:top;' align='left' class='texto12'>Direccion de Administración y Finanzas</td>
								<td style='width: 30%;; vertical-align:top; ' align='center'>Pagina: 1</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' align='left' class='texto12'>Coordinación de Egresos y Patrimonial</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td style='width: 50%;' align='left' class='texto12'>Jefatura de Pagos</td>
								<td style='width: 50%;' align='right' >Fecha de Recepción:  $fecha</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td align='left' style='width: 10%;' >Proveedor: </td><td class='textobold' style='width: 90%;'>$numprov $nomprov</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 50%; vertical-align: top'>
						<table style='width: 100%;'>
							<tr>
								<td  style='width: 100%;'>Concepto</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table id='twmaarco' width='100%'>
										<tr>
											<td>$concepto</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table style='width: 100%;'>
										<tr>
											<th style='width: 50%;'>Factura</th><th style='width: 50%;'>Importe</th>
										</tr>
										$tfacturas
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td style='width: 50%; vertical-align: top'>
						<table id='twmaarco' width='100%' height='100px'>
							<tr><td width='100%' align='center' class='texto12bold'>Vale a Revisión</td></tr>
							<tr><td width='100%' align='center' class='texto12bold'> $numvale </td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 65%;'>&nbsp;</td>
					<td style='width: 35%;' align='rigth'>=================================</td>
				</tr>
				<tr>
					<td align='right' style='width: 65%;'>Total Vale:</td>
					<td style='width: 35%;' align='right'>$totalVale </td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;' align='left'> $totalValeLetra</td>
				</tr>
				<tr>
					<td colspan='2' bordercolor='#000000' style='border:1px; width: 100%;'>
						<table  id='twmaarco' width='100%'>
							<tr><td style='width: 100%;' bordercolor=''>Fecha de Programación de Pago: $fechaPago</td></tr>
							<tr><td style='width: 100%;' bordercolor=''>Nota: Recepción de Facturas: Lunes de 09:00 a 14:00  y de 15:00 a 17:00 hrs.</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;'>
						<table style='width: 100%;'>
							<tr>
								<td style='width:45%' align='center'>________________________</td>
								<td style='width:10%' >&nbsp;</td>
								<td style='width:45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>$quienReviso</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>&nbsp;</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>________________________</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>Capturo</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>Revisado</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td style='width: 5%;' width='5%'>&nbsp;</td>
		<td style='width: 45%;' width='45%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 100%; height: 22px;' colspan='2'>
						<img src='../../../$imagenPDFPrincipal' style='width: 100px; height: 38px;'>
					</td>
				</tr>
				<tr>
					<td style='width: 100%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
				</tr>
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 70%; vertical-align:top;' align='left' class='texto12'>Dirección de Administración y Finanzas</td>
								<td style='width: 30%;; vertical-align:top; ' align='center'>Pagina: 1</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' align='left' class='texto12'>Coordinación de Egresos y Patrimonial</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td style='width: 50%;' align='left' class='texto12'>Jefatura de Pagos</td>
								<td style='width: 50%;' align='right' >Fecha de Recepción:  $fecha</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td align='left' style='width: 10%;' >Proveedor: </td><td class='textobold' style='width: 90%;'>$numprov $nomprov</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 50%; vertical-align: top'>
						<table style='width: 100%;'>
							<tr>
								<td  style='width: 100%;'>Concepto</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table id='twmaarco' width='100%'>
										<tr>
											<td>$concepto</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table style='width: 100%;'>
										<tr>
											<th style='width: 50%;'>Factura</th><th style='width: 50%;'>Importe</th>
										</tr>
										$tfacturas
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td style='width: 50%; vertical-align: top'>
						<table id='twmaarco' width='100%' height='100px'>
							<tr><td width='100%' align='center' class='texto12bold'>Vale a Revisión</td></tr>
							<tr><td width='100%' align='center' class='texto12bold'> $numvale </td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 65%;'>&nbsp;</td>
					<td style='width: 35%;' align='rigth'>=================================</td>
				</tr>
				<tr>
					<td align='right' style='width: 65%;'>Total Vale:</td>
					<td style='width: 35%;' align='right'>$totalVale </td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;' align='left'> $totalValeLetra</td>
				</tr>
				<tr>
					<td colspan='2' bordercolor='#000000' style='border:1px; width: 100%;'>
						<table  id='twmaarco' width='100%'>
							<tr><td style='width: 100%;' bordercolor=''>Fecha de Programación de Pago: $fechaPago</td></tr>
							<tr><td style='width: 100%;' bordercolor=''>Nota: Recepción de Facturas: Lunes de 09:00 a 14:00  y de 15:00 a 17:00 hrs.</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;'>
						<table style='width: 100%;'>
							<tr>
								<td style='width:45%' align='center'>________________________</td>
								<td style='width:10%' >&nbsp;</td>
								<td style='width:45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>$quienReviso</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>&nbsp;</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>________________________</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>Capturo</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>Revisado</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body> </html>";

//echo $htmlstr;
//if ( isset( $_POST["html"] ) ) {

 // if ( get_magic_quotes_gpc() )
//    $htmlstr = stripslashes($htmlstr);
  
	//$old_limit = ini_set("memory_limit", "16M");
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($htmlstr);
	$dompdf->set_paper('letter', 'landscape');
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/vale_".$numvale.".pdf", $pdf);
	
	//location
  	//$dompdf->stream("vale_".$numvale.".pdf");/**/

?>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
</head>
	<body>
		<embed src=" <?php echo '../pdf_files/vale_'.$numvale.'.pdf'; ?> " width="100%" height="600">
	</body> 
</html>

<!--<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
<style>
body 
{
	font-family:'Arial';
	font-size:9;
}

table {
  	margin-top: 0em;
	margin-left: 0em;
	vertical-align:top;
	border-collapse:collapse; 
	border: none;
}
td {padding: 0;}

#twmaarco
{
	border: 1pt solid black;
}

.textobold{
	font-weight:bold;
}
.texto14{ 
	font-size: 15pt;
	font-weight:bold;
}

.texto12bold
{
	font-size: 12pt;
	font-weight:bold;
}
.texto12{ 
	font-size: 12pt;
}
.texto11{
	font-size: 11pt;
}
</style>

</head>

<body>
<table style='width: 90%; top: 0px' width='100%'>
	<tr>
		<td style='width: 45%;' width='50%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 100%; height: 22px;' colspan='2'>
						<img src='../../../imagenes/fomerrey.png' style='width: 100px; height: 38px;'>
					</td>
				</tr>
				<tr>
					<td style='width: 100%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
				</tr>
				<tr>
					<td colspan='2' style='border: 0pt;'>
						<table style='width: 100%; vertical-align: top'>
							<tr>
								<td style='width: 70%; vertical-align:top;' align='left' class='texto12'>Dirección de Administración y Finanzas</td>
								<td style='width: 30%;; vertical-align:top; ' align='center'>Pagina: 1</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' align='left' class='texto12'>Coordinación de Egresos y Patrimonial</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td style='width: 50%;' align='left' class='texto12'>Jefatura de Pagos</td>
								<td style='width: 50%;' align='right' >Fecha de Recepción:  $fecha</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td align='left' style='width: 10%;' >Proveedor: </td><td class='textobold' style='width: 90%;'>$numprov $nomprov</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 50%; vertical-align: top'>
						<table style='width: 100%;'>
							<tr>
								<td  style='width: 100%;'>Concepto</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table id='twmaarco' width='100%'>
										<tr>
											<td>$concepto</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table style='width: 100%;'>
										<tr>
											<th style='width: 50%;'>Factura</th><th style='width: 50%;'>Importe</th>
										</tr>
										$tfacturas
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td style='width: 50%; vertical-align: top'>
						<table id='twmaarco' width='100%' height='100px'>
							<tr><td width='100%' align='center' class='texto12bold'>Vale a Revisión</td></tr>
							<tr><td width='100%' align='center' class='texto12bold'> $numvale </td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 65%;'>&nbsp;</td>
					<td style='width: 35%;' align='rigth'>=====================</td>
				</tr>
				<tr>
					<td align='right' style='width: 65%;'>Total Vale:</td>
					<td style='width: 35%;' align='right'>$totalVale </td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;' align='left'> $totalValeLetra</td>
				</tr>
				<tr>
					<td colspan='2' bordercolor='#000000' style='border:1px; width: 100%;'>
						<table  id='twmaarco' width='100%'>
							<tr><td style='width: 100%;' bordercolor=''>Fecha de Programación de Pago: $fechaPago</td></tr>
							<tr><td style='width: 100%;' bordercolor=''>Nota: Recepción de Facturas: Lunes de 09:00 a 14:00  y de 15:00 a 17:00 hrs.</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;'>
						<table style='width: 100%;'>
							<tr>
								<td style='width:45%' align='center'>________________________</td>
								<td style='width:10%' >&nbsp;</td>
								<td style='width:45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>$quienReviso</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>&nbsp;</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>________________________</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>Capturo</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>Revisado</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td style='width: 10%;' width='1%'>&nbsp;</td>
		<td style='width: 45%;' width='50%'>
			<table style='width: 100%;' align='center'>
				<tr>
					<td style='width: 100%; height: 22px;' colspan='2'>
						<img src='../../../imagenes/fomerrey.png' style='width: 100px; height: 38px;'>
					</td>
				</tr>
				<tr>
					<td style='width: 100%;' colspan='2' align='left' class='texto14'>Fomento Metropolitano de Monterrey</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td style='width: 70%;' align='left' class='texto12'>Dirección de Administración y Finanzas</td>
								<td style='width: 30%;' align='center'>Pagina: 2 </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' align='left' class='texto12'>Coordinación de Egresos y Patrimonial</td>
				</tr>
				<tr>
					<td colspan='2'>
						<table style='width: 100%;'>
							<tr>
								<td style='width: 50%;' align='left' class='texto12'>Jefatura de Pagos</td>
								<td style='width: 50%;' align='rigth'>Fecha de Recepción:  $fecha</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan='2' align='left' class='textobold'>Proveedor: $numprov $nomprov</td></tr>
				<tr>
					<td style='width: 50%; vertical-align: top'>
						<table style='width: 100%;'>
							<tr>
								<td  style='width: 100%;'>Concepto</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table id='twmaarco' width='100%'>
										<tr>
											<td>$concepto</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align='center' style='width: 100%;'>
									<table style='width: 100%;'>
										<tr>
											<th style='width: 50%;'>Factura</th><th style='width: 50%;'>Importe</th>
										</tr>
										$tfacturas
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td style='width: 50%; vertical-align: top'>
						<table id='twmaarco' width='100%' height='100px'>
							<tr><td width='100%' align='center' class='texto11'>Vale a Revisión</td></tr>
							<tr><td width='100%' align='center' class='texto12'> $numvale </td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style='width: 65%;'>&nbsp;</td>
					<td style='width: 35%;' align='rigth'>=====================</td>
				</tr>
				<tr>
					<td align='right' style='width: 65%;'>Total Vale:</td>
					<td style='width: 35%;' align='right'>$totalVale </td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;' align='left'> $totalValeLetra</td>
				</tr>
				<tr>
					<td colspan='2' bordercolor='#000000' style='border:1px; width: 100%;'>
						<table  id='twmaarco' width='100%'>
							<tr><td style='width: 100%;' bordercolor=''>Fecha de Programación de Pago: $fechaPago</td></tr>
							<tr><td style='width: 100%;' bordercolor=''>Nota: Recepción de Facturas: Lunes de 09:00 a 14:00  y de 15:00 a 17:00 hrs.</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='2' style='width: 100%;'>
						<table style='width: 100%;'>
							<tr>
								<td style='width:45%' align='center'>________________________</td>
								<td style='width:10%' >&nbsp;</td>

								<td style='width:45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>$quienReviso</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>&nbsp;</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>________________________</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>________________________</td>
							</tr>
							<tr>
								<td style='width: 45%' align='center'>Capturo</td>
								<td style='width: 10%' >&nbsp;</td>
								<td style='width: 45%' align='center'>Revisado</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body> </html>-->