<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
	
	$command= "select a.vale, a.factura, a.total, a.estatus, a.id, a.anexo, a.ruta_anexo, c.nomprov from egresosdvale a LEFT JOIN egresosmvale b on a.vale=b.vale LEFT JOIN compramprovs c ON c.prov=b.prov WHERE a.vale =" . $_REQUEST['query'] . " AND a.estatus<9000 order by a.vale";

	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$datos[$i]['folio']=trim($row['vale']);
		$datos[$i]['factura']=trim($row['factura']);					
		$datos[$i]['importe']=number_format(trim($row['total']),2);
		$datos[$i]['estatus']=$row['estatus'];
		$datos[$i]['id']=trim($row['id']);
		
		if($row['anexo']!=null)
			$datos[$i]['anexo']= $row['anexo'];
		else
			$datos[$i]['anexo']="N";
	
		if(trim($row['ruta_anexo'])!=null)
			$datos[$i]['ranexo']= $row['ruta_anexo'];
		else
			$datos[$i]['ranexo']= "N";
		$datos[$i]['nomprov']=utf8_encode(trim($row['nomprov']));	
			
		$i++;
	}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>