<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        
        <script type="text/javascript" src="../../googleapis/ajax/libs/ajax/jquery/1.8.1/jquery.min.js"></script>
        <link rel="stylesheet" href="../../googleapis/ajax/libs/jqueryui/jquery-ui-1.8.23/development-bundle/themes/base/jquery.ui.all.css">
        <script type="text/javascript" src="javascript/jquery-uploader/jquery.uploadify.v2.1.0.min.js"></script>
        <script type="text/javascript" src="javascript/jquery-uploader/swfobject.js"></script>

       
        <script src="../../cheques/javascript/divhide.js"></script>
		<script src="../../cheques/javascript/funciones_cheque.js"></script>
		
		<script src="../../cheques/javascript/jquery.tmpl.js"></script>

		
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
			function cargarFacturas(object)
			{
				var data = 'query=' + $(object).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('valeAgregarAnexosXSurtido_bus_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							console.log(resp);
							$('#proveedor').empty();
							$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 						
							for(var i=0;i<resp.length;i++)
							{
								activarUpload(resp[i].id);
							}
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
			}
			
            $(function()
			{
                $('#query').live('keyup', function()
				{ 
					cargarFacturas(this);
						
                });
            });
			
			
			function activarUpload(id)
			{
				$('#anexo_'+id).uploadify({
					'uploader'  		: 'javascript/jquery-uploader/uploadify.swf',
					'script'  : 'javascript/jquery-uploader/uploadAnexoAFactura.php',
					'cancelImg' : 'imagenes/cancel.png',
					'auto'      : true,
					'folder'    : 'imagenes',
					'height'    :  21,
					'width'     :  21,
					'buttonImg' : 'imagenes/image_add.png',
					'buttonText': 'Click para cambiar la imagen del fondo del diseño.',
					'scriptData' : {'texto':  $("#path_"+id).val() }, //  },// $("#path"+this.id ).val()
					'onComplete': function(event, queueID, fileObj, response, data) 
					{
						if(response!="error")
						{
							//alert(response);
							alert("Anexo agregado exitosamente.");
							$("#path"+$(this).attr("id")).val(response);
							cargarFacturas($("#query"))
						}
						else
							alert("A ocurrido un error al intentar adjuntar el archivo anexo.");

					}
				});
			}
			/*jQuery(document).ready(function()
			{ 
				activarUpload();
				
			});*/
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if id}}
					<td align="center">${folio}</td>
					<td align="center">${factura}</td>
					<td align="right">$${importe}</td>
					<td>
						{{if ranexo!="N"}}
							<a href="../${ranexo}" target="_blank" >Ver Anexo</a> Modificar: 
						 {{else}}
							Agregar: 
						{{/if}}
							<input type="file"   id="anexo_${id}">
							  <input type="hidden" id="path_${id}"  value="${id}" size="25"  />
					</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
    
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Agregar Anexos Por Surtido</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%"> Surtido: </td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
  
  
  
   

  
  
  
  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
</td>
</tr>
</table>
<table>
                <thead>
					<th>Folio</th>
                    <th>Factura</th>
                    <th>Importe</th>
                    <th>Anexo</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
