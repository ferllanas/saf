<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$ffecha=date("Y-m-d");
	$fecha="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$facturas="";//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$total=0.00;
	$montofactura=0.00;
	$pajuste_vale=0.00;
	if(isset($usuario))
	{
		if(isset($_POST['prods']))
			$facturas=$_POST['prods'];
			
		if(isset($_POST['pajuste_vale']))
			$pajuste_vale=$_POST['pajuste_vale'];
		
		$incongruencia=false;
		if ( is_array( $facturas ) ) // Pregunta se es una array la variable facturas
		{
			//suma los totales de cada una de las partidas
			for( $i=0 ; $i  < count($facturas) ; $i++ )
			{
				$totalXFactura=0.00;
				for( $j=0 ; $j  < count($facturas[$i]['partidas']) ; $j++ )
				{
					if($facturas[$i]['partidas'][$j]['tipoDoc']=='st' || $facturas[$i]['partidas'][$j]['tipoDoc']=='nc')
					{
						$subtotal+= (float)$facturas[$i]['partidas'][$j]['sumSubTotal'];
						$iva+= (float)$facturas[$i]['partidas'][$j]['sumIva'];
						$total+= (float)$facturas[$i]['partidas'][$j]['sumTotal'];
						$totalXFactura+= (float)$facturas[$i]['partidas'][$j]['sumTotal'];
					}
					else
					{
						$subtotal = $subtotal - (float)$facturas[$i]['partidas'][$j]['sumSubTotal'];
						$iva = $iva - (float)$facturas[$i]['partidas'][$j]['sumIva'];
						$total= $total - (float)$facturas[$i]['partidas'][$j]['sumTotal'];
						$totalXFactura= $totalXFactura- (float)$facturas[$i]['partidas'][$j]['sumTotal'];
					}	
				}
				
				$t1MAS2= (float)$totalXFactura -(float)str_replace(",","",$facturas[$i]['montoFactura']);
				
				if($t1MAS2 > $pajuste_vale || $t1MAS2 < - $pajuste_vale)
				{
					$incongruencia=true;
					$msgerror=" El total de la Factura '".$facturas[$i]['numfact']."' no concuerda con el total de los documentos agregados a ella.\n";//. $t1MAS2." > ". $pajuste_vale ."||". $t1MAS2 ."< -". $pajuste_vale."\n". (float)$totalXFactura." - ".(float)$facturas[$i]['montoFactura'];//
				}
			}
			
			if($incongruencia)
			{
				$fails=true; 
			}
			else
			{
				//Crea el vale
				list($id, $fecha, $hora)= fun_creaVale($facturas[0]['idprov'],
									convertirFechaEuropeoAAmericano($facturas[0]['fechaDPago']),
									$facturas[0]['concepto'],
									$usuario,
									$subtotal, 
									$iva, 
									$total ); 
				if(strlen($id)>0)//Para ver si en realidad se genero un vale
				{
					for( $i=0 ; $i  < count($facturas) ; $i++ )
					{
						for( $j=0 ; $j  < count($facturas[$i]['partidas']) ; $j++ )
						{
							if($facturas[$i]['partidas'][$j]['tipoDoc']=='st')
								$tipodoc="1";
					
							if($facturas[$i]['partidas'][$j]['tipoDoc']=='nc')
								$tipodoc="3";
				
							if($facturas[$i]['partidas'][$j]['tipoDoc']=='anc')
								$tipodoc="4";
							/*$msgerror=$id						.",". 
												$facturas[$i]['numfact']					.",".  
												$facturas[$i]['partidas'][$j]['sumSubTotal'].",".  
												$facturas[$i]['partidas'][$j]['sumIva']		.",". 
												$facturas[$i]['partidas'][$j]['sumTotal'] 	.",". 
												$tipodoc									.",". 
												$facturas[$i]['partidas'][$j]['numDoc'];*/
							$montofactura=str_replace(",","",$facturas[$i]['montoFactura']);
							$fails=func_creaDetalleVale($id						, 
												$facturas[$i]['numfact']					, 
												$facturas[$i]['partidas'][$j]['sumSubTotal'], 
												$facturas[$i]['partidas'][$j]['sumIva']		,
												$facturas[$i]['partidas'][$j]['sumTotal'] 	,
												$tipodoc									,
												$facturas[$i]['partidas'][$j]['numDoc'],
												$montofactura, $usuario, $facturas[$i]['anexo']);
						}
					}
				}
				else
					{	$fails=true; $msgerror="No pudo generar ningun vale";}
			}
		}
		else
			{	$fails=true; $msgerror=" No es un array la variable";}
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	$datos=array();//prepara el aray que regresara
	$datos[0]['id']=trim($id);// regresa el id de la
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_creaVale($prov,
								$fecha_pago,
								$concepto,
								$usuario,
								$subtotal, 
								$iva, 
								$total)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_egresos_A_mvale(?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$prov,
								&$fecha_pago,
								&$concepto, 
								&$usuario,
								&$subtotal, 
								&$iva,
								&$total);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die(  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

//Funcion para registrar el p�roductoa  la requisicion
function func_creaDetalleVale($vale, 
										$factura, 
										$subtotal, 
										$iva,
										$total ,
										$tipodoc,
										$numdocto,
										$montofactura
										,$usuario, $anexo)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$id="";//Store proc regresa id de la nueva requisicion
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_egresos_A_dvale(?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$facturas[$i]['producto'], $facturas[$i]['cantidad'], $facturas[$i]['descripcion'],'0',$facturas[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = array(&$vale, 
					&$factura, 
					&$subtotal, 
					&$iva,
					&$total ,
					&$tipodoc,
					&$numdocto,
					&$montofactura,&$usuario,
					&$anexo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die(  print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>