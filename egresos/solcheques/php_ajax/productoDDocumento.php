<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');	

$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo "SSSAAAA";
if(isset($_POST['tipo']) && isset($_POST['numdoc']) && isset($_POST['prov']) && $conexion)
{
	$tipo = $_POST['tipo'];
	$numdoc = $_POST['numdoc'];
	$prov = $_POST['prov'];
	$command="";
	if($tipo=='st')
	{
		$command= "SELECT a.surtido, a.orden, a.estatus,
						b.cantidad, b.ctacargo, b.ctacredito, b.iddcuadroc, 
						c.prov ,c.ordenprov,
						d.id, d.precio1, d.precio2, d.precio3, d.precio4, d.ivapct,
						e.prod
					FROM compramsurtido a 
					INNER JOIN compradsurtido b ON a.surtido=b.surtido 
					INNER JOIN compramordenes c ON a.orden=c.orden
					INNER JOin compradcuadroc d on b.iddcuadroc=d.id 
					INNER JOIN compradrequisi e ON e.id=d.iddrequisi where c.prov=$prov AND a.surtido=$numdoc";	
	}

	if($tipo=='nc')
	{
		$command= "SELECT  folio, folioprov, prov, fecha,  subtotal, iva, total, observa, usuario, estatus, falta, halta, fbaja
					FROM         egresosmncredito where folio=$numdoc AND estatus=0";	
	}

	if($tipo=='anc')
	{
		$command= "SELECT  folio, folioprov, prov, fecha,  subtotal, iva, total, observa, usuario, estatus, falta, halta, fbaja
					FROM         egresosmncredito where folio=$numdoc AND estatus=10";/*"SELECT a.surtido, a.orden, a.estatus,
						b.cantidad, b.ctacargo, b.ctacredito, b.iddcuadroc, 
						c.prov ,c.ordenprov,
						d.id, d.precio1, d.precio2, d.precio3, d.precio4, d.ivapct,
						e.prod
					FROM compramsurtido a 
					INNER JOIN compradsurtido b ON a.surtido=b.surtido 
					INNER JOIN compramordenes c ON a.orden=c.orden
					INNER JOin compradcuadroc d on b.iddcuadroc=d.id 
					INNER JOIN compradrequisi e ON e.id=d.iddrequisi where c.prov=$prov AND a.surtido=$numdoc";	*/
	}
	
	//echo $command;
	$ordenprov=0;
	$sumSubTotal=0.0;
	$sumIva=0.0;
	$sumTotal = 0.0;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			if($tipo=='st')
			{
				$ordenprov = $row['ordenprov'];
				
				$sumSubTotal += (float)$row['cantidad'] * (float)$row['precio'.$ordenprov] ;
				$sumIva += (float)$row['cantidad'] * ( (float)$row['precio'.$ordenprov] * ( $row['ivapct']/100   ) );
			}
	
			if($tipo=='nc')
			{
				$sumSubTotal+= $row['subtotal'];
				$sumIva += $row['iva'];
			}

			if($tipo=='anc')
			{
				$sumSubTotal+= $row['subtotal'];
				$sumIva += $row['iva'];
			}

			$i++;
		}
	}
	$sumTotal +=   $sumSubTotal + $sumIva ;
	
	$datos[0]['tipo']=$tipo;
	$datos[0]['numdoc']=$numdoc;
	$datos[0]['sumSubTotal']=$sumSubTotal;
	$datos[0]['sumIva']=$sumIva;
	$datos[0]['sumTotal']=$sumTotal;
	echo json_encode($datos);

}
?>