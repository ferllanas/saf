<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
	
	$command= "	select a.folio, a.depto,b.nomdepto, a.subtotal, a.iva, a.importe, a.ruta_anexo, a.anexo,CONVERT(VARCHAR(11),a.frecibe,103) as fecha, 				a.prov,c.nomprov,a.estatus
	from egresosmsolche a 
	left join nominamdepto b on a.depto=b.depto
	LEFT JOIN compramprovs c ON a.prov=c.prov WHERE a.folio like '%".$_REQUEST['query']. "%' and a.estatus<9000 order by a.folio ";
//echo $command;
	$stmt2 = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
		// Comienza a realizar el arreglo, trim elimina espacios en blanco		
		$datos[$i]['folio']=trim($row['folio']);
		$datos[$i]['depto']=trim($row['depto']);
		$datos[$i]['nomdepto']=trim($row['nomdepto']);
		$datos[$i]['subtotal']=number_format(trim($row['subtotal']),2);
		$datos[$i]['iva']=number_format(trim($row['iva']),2);
		$datos[$i]['importe']=number_format(trim($row['importe']),2);
		$datos[$i]['estatus']=$row['estatus'];
		$datos[$i]['prov']=trim($row['prov']);
		$datos[$i]['fecha']=trim($row['fecha']);
		$datos[$i]['anexo']=trim($row['anexo']);
		if(trim($row['ruta_anexo'])!=null)
			$datos[$i]['ranexo']= $row['ruta_anexo'];
		else
			$datos[$i]['ranexo']= "N";
		$datos[$i]['nomprov']=utf8_encode(trim($row['nomprov']));	
			
		$i++;
	}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>