<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        
        <script type="text/javascript" src="../../googleapis/ajax/libs/ajax/jquery/1.8.1/jquery.min.js"></script>
		
		<script src="../../cheques/javascript/jquery.tmpl.js"></script>

        <script>
			function cambiaAnexo(objeto)
			{
				var data= "datos="+$(objeto).val();
				//alert(data);
				$.post('php_ajax/cambiarAnexo.php',data, function(resp)
				{ //Llamamos el arch ajax para que nos pase los datos
						
						console.log(resp);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 						
						for(var i=0;i<resp.length;i++)
						{
							activarUpload(resp[i].id);
						}
							     // los va colocando en la forma de la tabla
                }, 'json');
			}
			function cargarFacturas(object)
			{
				var data = 'query=' + $(object).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('solcheAgregarAnexos_bus_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						console.log(resp);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 						
						for(var i=0;i<resp.length;i++)
						{
							activarUpload(resp[i].id);
						}
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
			}
			
            $(function()
			{
                $('#query').live('keyup', function()
				{ 
					cargarFacturas(this);
						
                });
            });
			
			
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                
					<td align="center">${folio}</td>
					<td align="center">${nomdepto}</td>
					<td align="right">$${importe}</td>
					
						{{if anexo=="N"}}
							<td><input type="radio" name="anex_${folio}" value="0;${folio}" checked onclick="cambiaAnexo(this)">S/Anexo</td>
							<td><input type="radio" name="anex_${folio}" value="1;${folio}" onclick="cambiaAnexo(this)">C/Anexo</td>
						 {{else}}
							{{if anexo==0}}
								<td><input type="radio" name="anex_${folio}" value="0;${folio}" checked onclick="cambiaAnexo(this)">S/Anexo</td>
								<td><input type="radio" name="anex_${folio}" value="1;${folio}" onclick="cambiaAnexo(this)">C/Anexo</td>
							 {{else}}
							 	<td><input type="radio" name="anex_${folio}" value="0;${folio}" onclick="cambiaAnexo(this)">S/Anexo</td>
								<td><input type="radio" name="anex_${folio}" value="1${folio}" checked onclick="cambiaAnexo(this)">C/Anexo</td
							 {{/if}}
						{{/if}}
					
                
            </tr>
        </script>   
    
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Solicitud de Cheque-Anexo</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%"> Solicitud de Cheque: </td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
  
  
  
   

  
  
  
  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
</td>
</tr>
</table>
<table>
                <thead>
					<th>Folio</th>
                    <th>Departamento</th>
                    <th>Importe</th>
                    <th>S/Anexo</th>
                    <th>C/Anexo</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
