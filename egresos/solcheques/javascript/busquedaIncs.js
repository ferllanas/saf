//Funciones para busqueda avanzada , autocompletar de requisiciones
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest()
{
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('txtSearch').value);
		var tipo;
		if($('#st').is(":checked"))
			tipo = "st";
			
		if($('#nc').is(":checked"))	
			tipo = "nc";
			
		if($('#anc').is(":checked"))	
			tipo = "anc";

		var prov = $('#prov1').val();

		if(prov.length<=0)
			alert("Selecciona un proveedor");
		//alert('php_ajax/query.php?q=' + str +'&tipo='+ tipo + '&prov='+prov);
		searchReq.open("GET",'php_ajax/query.php?q=' + str +'&tipo='+ tipo + '&prov='+prov,true)//'searchSuggest.php?search=' + str, true);
        //alert('php_ajax/query.php?q=' + str);
		searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }        
}
//Called when the AJAX response is returned.
function handleSearchSuggest() {
	
    if (searchReq.readyState == 4) 
	{
        var ss = document.getElementById('search_suggest')
        ss.innerHTML = '';
		console.log(searchReq.responseText);
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<0)
		   for(i=0; i < str.length - 1; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split(";");
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) 
{
    document.getElementById('txtSearch').value = value;
	var tok =idprod.split(";");
	$('surtido').value = tok[0];
	//alert(tok[0]);
	//$('unidaddPro').value = tok[1];
    document.getElementById('search_suggest').innerHTML = '';
}