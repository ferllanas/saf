/*//Elimina una menu
*/
var facturasDProv=new Array(); 
var countFacturasdProv =0;
var ObjSelsumDocdFact;

//Agrega una partida
function objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	this.tipoDoc = tipoDoc;
	this.numDoc = numDoc;
	this.sumSubTotal = sumSubTotal;
    this.sumIva = sumIva;
	this.sumTotal = sumTotal;
}

function objfacturaDProv(idprov, nomprov ,	numfact ,  montoFactura , fechaDPago , concepto, anexoa){  
	this.idprov = idprov;
	this.nomprov = nomprov;
	this.numfact = numfact;
    this.montoFactura = montoFactura;
	this.fechaDPago = fechaDPago;
	this.concepto = concepto;
	this.partidas = new Array();
	this.numpartidas = 0;
	this.anexo= anexoa;
	
 }

function verificarExistePartidaEnFacturas(numDoc)
{
	var retval=false;
	for(i=0;i<facturasDProv.length &&  retval==false;i++)
	{
		for(j=0;j<facturasDProv[i]['partidas'].length &&  retval==false;j++)
			if(facturasDProv[i]['partidas'][j]['numDoc']==numDoc)
			{	retval=true; }
	}
	return retval;
}
//funcion que busca un numero de factura
function getIndexFactura(numfact)
{
	var retval=false;
	var index=-1;
	for(i=0;i<facturasDProv.length && retval==false;i++)
	{
		//alert(facturasDProv[i]['numfact']+"=="+numfact);
		if(numfact==facturasDProv[i]['numfact'])
		{	retval=true;
			index=i;
		}
	}	
	return index;
}

//this.agregarPartida=
 function agregarPartida(numfact, tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
		var indexT= getIndexFactura(numfact)

		var deberiaAgregar = true;
		if( facturasDProv[indexT]['numpartidas'] != 0)
		{
			for(i=0; i < facturasDProv[indexT]['partidas'].length && deberiaAgregar==true;i++)
			{
				if(tipoDoc==facturasDProv[indexT]['partidas'][i]['tipoDoc'] && numDoc==facturasDProv[indexT]['partidas'][i]['numDoc'])
					deberiaAgregar=false;
			}
		}

		if(deberiaAgregar)
		{
			deberiaAgregar=verificarExistePartidaEnFacturas(numDoc);
			if(!deberiaAgregar)
			{
				facturasDProv[indexT]['partidas'][ facturasDProv[indexT]['numpartidas'] ] = new objpartidaDFact(tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal);
				facturasDProv[indexT]['numpartidas']++;
				deberiaAgregar=true;
			}
			else
			{
				alert("Ya existe esta partida en alguna factura del proveedor.");
				deberiaAgregar=false;
			}
		}
		else
			alert("Ya existe esta partida para la factura seleccionada.");

		return deberiaAgregar;
}


function agregarPartidaAFactura(numfact,tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal)
{
	retval=false;
	var index= getIndexFactura(numfact);
	if(index<0)
	{
		alert("No se puede agregar la partida a la factura. No se encontro la Factura.");
		return false;
	}
	
	if(agregarPartida (numfact, tipoDoc,numDoc, sumSubTotal, sumIva, sumTotal))
		retval=true;

	return retval;
}

function addFactura()
{
	var numfact= $('#numfact').val();
	var montoFactura= $('#montoFactura').val();
	var fechaDPago= $('#fechaDPago').val();
	var idprov =$('#prov1').val();
	var nomprov= $('#nomprov').val();
	var concVale= $('#concVale').val();
	//alert(concVale);
	var anexo =$('#mitexto').val()
	//alert(anexo);
	if(concVale.length <= 0 )
	{
		alert("Favor de ingresar un concepto");
		return(false);
	}	

	if(nomprov.length <= 0 )
	{
		alert("Favor de ingresar proveedor");
		return(false);
	}	

	if(numfact.length <= 0 )
	{
		alert("Favor de ingresar N�mero de Factura");
		return(false);
	}	
	
	if(montoFactura.length <= 0)
	{
		alert("Favor de ingresar  Monto de monto de la factura.");
		return(false);
	}	

	if(fechaDPago.length <= 0)
	{
		alert("Favor de ingresar fecha de fecha de pago.");
		return(false);
	}	

	if(verificaSiExisteFactura(numfact))
	{	
		alert("El n�mero de factura corresponde a una agregada anteriormente.");
		return(false);
	}

	facturasDProv[countFacturasdProv]= new  objfacturaDProv(idprov,nomprov ,	numfact ,  montoFactura , fechaDPago, concVale,anexo);
	countFacturasdProv++;
	agregarFacturaATablaFacturas(idprov,nomprov,numfact,montoFactura,fechaDPago,anexo);

	$('#numfact').val("")
	$('#montoFactura').val("")
	//$('#fechaDPago').val("")
	//$('#prov1').val("")
	//$('#nomprov').val("")
}



function agregarFacturaATablaFacturas(idprov,nomprov,numfact,montoFactura,fechaDPago, anexo)
{
	var Table = document.getElementById('tfacturas');
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);
	

	var newcell1 = newrow.insertCell(0);
	if(anexo.length>0)
	{
		newcell1.innerHTML = "<input type='hidden' id='vnumfact' name='vnumfact' value='"+numfact+"' readonly tabindex='-1' ><a href='"+anexo+"' target='_blank' >"+numfact+"</a>";}
	else
		newcell1.innerHTML = "<input type='hidden' id='vnumfact' name='vnumfact' value='"+numfact+"' readonly tabindex='-1' >"+numfact;
		
		newcell1.className ="td1";
	
//	newcell1.style.width = "120px";

	var newcell5 = newrow.insertCell(1);
	newcell5.innerHTML  ="<input type='hidden' id='vmontoFactura' name='vmontoFactura' readonly value='"+montoFactura+"'>"+addCommas(montoFactura);
	newcell5.className ="td2";
	//newcell5.style.width = "120px";

	var newcell6 = newrow.insertCell(2);
	newcell6.innerHTML = "<input type='hidden' id='vfechaDPago' name='vfechaDPago' readonly  value='"+fechaDPago+"' >"+fechaDPago;
	newcell6.className ="td3";
	//newcell6.style.width = "120px";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML ="<input type='text' id='sumDocdFact"+contRow+"' name='sumDocdFact' value='0.0' readonly class='caja_toprint'>";
	newcell3.className ="td4";
	//newcell3.style.width = "120px";

	var newcell4 = newrow.insertCell(4);
	newcell4.className ="td5";
	//newcell4.style.width = "120px";

	var functiona="borrarFactura('"+numfact+"' )";
	newcell4.innerHTML ='<input type="button" id="borrarfact" name="borrarfact" value="-" onClick="'+functiona+'" >';

	newrow.onclick  = function(){
									mostrarPartidasdFactura(this,numfact,'sumDocdFact'+contRow)
								}
								
	$('#mitexto').val("");
}

function borrarFactura(numfac)
{
	var answer = confirm("�Seguro deseas borrar este documento?")
	if (answer)
	{
		var index = getIndexFactura(numfac);
		facturasDProv.splice( index ,1 );
		countFacturasdProv--;
		borrado=true;
		
		if(borrado)
		{
			refreshTablaFacturas();
			alert("Factura eliminada.");
		}
	}
}

function refreshTablaFacturas()
{
	//facturasDProv
	var Table = document.getElementById('tfacturas');
	var numren=Table.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			Table.deleteRow(i);
		}

	for (var i = 0; i < facturasDProv.length; i++) 
	{ 
		agregarFacturaATablaFacturas(facturasDProv[i]['idprov'] ,
									facturasDProv[i]['nomprov'],
									facturasDProv[i]['numfact'],
									facturasDProv[i]['montoFactura'],
									facturasDProv[i]['fechaDPago']);
	}
	//limpia tabla de documentos	
	var tabla=document.getElementById('menus');
		var numren=tabla.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			tabla.deleteRow(i);
		}
}

function mostrarPartidasdFactura(object,numfact,sumDocdFact)
{
	ObjSelsumDocdFact=document.getElementById(sumDocdFact);
	var Table = document.getElementById('tfacturas');
	for (var i = 0; i < Table.rows.length; i++) 
	{ 
		Table.rows[i].className="";//Cambia el color de renglon a blanco
	}

	$('#facSelected').val(numfact);//Coloca el numero de factura que se encuentra seleccionado actualmente
	object.className="row_selected";	//Cambia el color del renglon
	
	var index =getIndexFactura(numfact);
	if(index>=0)
	{
		var tabla=document.getElementById('menus');
		var numren=tabla.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			tabla.deleteRow(i);
		}

		for(i=0;i<facturasDProv[index]['partidas'].length;i++)
		{


			agrega_Productoddoc(	facturasDProv[index]['partidas'][i]['tipoDoc'],
									facturasDProv[index]['partidas'][i]['numDoc'],
									facturasDProv[index]['partidas'][i]['sumSubTotal'], 
									facturasDProv[index]['partidas'][i]['sumIva'],  
									facturasDProv[index]['partidas'][i]['sumTotal'],
									facturasDProv[index]['numfact'], 
									facturasDProv[index]['montoFactura'],
									facturasDProv[index]['fechaDPago'],
									facturasDProv[index]['idprov']
								);

											
		}
		sumTotalDProveedoresXFactua();
	}

	
}


function refreshPartidasdFactura(numfact)
{
	var Table = document.getElementById('tfacturas');
	for (var i = 0; i < Table.rows.length; i++) 
	{ 
		Table.rows[i].className="";//Cambia el color de renglon a blanco
	}
	$('#facSelected').val(numfact);//Coloca el numero de factura que se encuentra seleccionado actualmente
	var index =getIndexFactura(numfact);
	if(index>=0)
	{
		var tabla=document.getElementById('menus');
		var numren=tabla.rows.length;
		for(var i = numren-1; i >=0; i--)
		{
			tabla.deleteRow(i);
		}
		
		for(i=0;i<facturasDProv[index]['partidas'].length;i++)
		{
			agrega_Productoddoc(	facturasDProv[index]['partidas'][i]['tipoDoc'],
									facturasDProv[index]['partidas'][i]['numDoc'],
									facturasDProv[index]['partidas'][i]['sumSubTotal'], 
									facturasDProv[index]['partidas'][i]['sumIva'],  
									facturasDProv[index]['partidas'][i]['sumTotal'],
									facturasDProv[index]['numfact'], 
									facturasDProv[index]['montoFactura'],
									facturasDProv[index]['fechaDPago'],
									facturasDProv[index]['idprov']
								);

											
		}
		sumTotalDProveedoresXFactua();
	}
}

function verificaSiExisteFactura(numfact)
{
	var retval=false;
	
	for(i=0;i<facturasDProv.length && retval==false;i++)
	{
		if(numfact==facturasDProv[i]['numfact'])
			retval=true;
	}	
	return retval;
}

//Elimina una menu
function eliminar_ProductoARequisicion( e , s )
{
	//var table=$( 'tmenus' );
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	//alert(rowindexA+","+ document.getElementById('menus').rows.length);
	document.getElementById('menus').deleteRow( rowindexA );
}
function DefineBusca()
{
	//alert(1);
	//var tipo=$('#optipo').val()

	if($('#st').checked)
	{
		$('#nomtipo').val("Numero de Surtido:");
		$('#tipobus').val("st");
		$('#ad_colin2').val("Agregar");
	}
	if($('#nc').checked)
	{
		$('#nomtipo').val("Agregar Nota de Credito:");
		$('#tipobus').val("nc");
		$('#ad_colin2').val("Agregar");
	}
	if($('#anc').checked)
	{	
		$('#nomtipo').val("Afectar Nota de Credito:");
		$('#tipobus').val("anc");
		$('#ad_colin2').val("Agregar");
	}
}




function getTipoSeleccionado()
{
	if($('#st').is(":checked")==true)
	{
		return('st');
	}	
	if($('#nc').is(":checked")==true)
	{
		return('nc');
	}	
	if($('#anc').is(":checked")==true)
	{
		return('anc');
	}	

}

function agregarDocAFactura()
{

	var provx		 = $('#prov1').val()
	if(provx=="")
	{	alert("Favor de ingresar proveedor.");	
		return(false);
	}

	var numfact	= $('#facSelected').val()
	if(numfact=="")
	{	alert("Favor de ingresar numero de factura.");	
		return(false);
	}
	
	var index=getIndexFactura(numfact);
	if(index<0)
	{
		alert("Es un numero de factura invalido.");
		return false;
	}
	
	var montoFactura = facturasDProv[index]['montoFactura'];
	var fechaDPago = facturasDProv[index]['fechaDPago'];
	var tipox 		 = getTipoSeleccionado()
	var numdocx		 = $('#txtSearch').val()
	
	//alert("tipo	: "+tipox+",numdoc:"+ numdocx+",prov:"+ provx);
	var datas = {
					tipo	: 		tipox,
					numdoc: numdocx,
					prov: provx
    			};

	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/productoDDocumento.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
					console.log(resp);
					//alert(resp[0].sumSubTotal);
					
						if(resp!="")
						{
							if(agregarPartidaAFactura(numfact,resp[0].tipo,
														resp[0].numdoc,
														resp[0].sumSubTotal, 
														resp[0].sumIva,  
														resp[0].sumTotal))
							{
								agrega_Productoddoc(resp[0].tipo,
															resp[0].numdoc,
															resp[0].sumSubTotal, 
															resp[0].sumIva,  
															resp[0].sumTotal,
															numfact, 
															montoFactura,	
															fechaDPago,
															provx
														);

								sumTotalDProveedoresXFactua();
							}
						}
						else
							alert("No es un documento valido.");
				}
			});
	//alert('php_ajax/productoDDocumento.php?tipo='+tipo+
					 //'&numdoc='+numdoc+'&prov='+prov);
	/*new Ajax.Request('php_ajax/productoDDocumento.php?tipo='+tipo+
					 '&numdoc='+numdoc+'&prov='+prov,
					{onSuccess : function(resp) 
						{
							//alert(resp.responseText);
							if( resp.responseText ) 
							{
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									for( var ii = 0; ii < myArray.length; ii++ ) 
									{
										if(myArray[ii].sumTotal>0)
										{
											if(agregarPartidaAFactura(numfact,myArray[ii].tipo,
																		myArray[ii].numdoc,
																		myArray[ii].sumSubTotal, 
																		myArray[ii].sumIva,  
																		myArray[ii].sumTotal))
											{
												agrega_Productoddoc(myArray[ii].tipo,
																			myArray[ii].numdoc,
																			myArray[ii].sumSubTotal, 
																			myArray[ii].sumIva,  
																			myArray[ii].sumTotal,
																			numfact, 
																			montoFactura,	
																			fechaDPago,
																			prov
																		);
	
												sumTotalDProveedoresXFactua();
											}
										}
										else
											alert("No es un documento valido.");
									}
								}
							}
						}
					});*/
	
}


function agrega_Productoddoc(tipo,numdoc,
								sumSubTotal, sumIva,  
								sumTotal,	numfact, 
								montoFactura,	fechaDPago,
								prov)
{	
		
	
	var Table = document.getElementById('menus');
	//
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);

	var newcell1 = newrow.insertCell(0);
	newcell1.className ="td1";
	newcell1.innerHTML ="<input type='hidden' id='vtipo' name='vtipo' readonly value='"+tipo+"'>"+
						"<input type='hidden' id='vmontoFactura' name='vmontoFactura' value='"+montoFactura+"'>"+
						"<input type='hidden' id='vfechaDPago' name='vfechaDPago' value='"+fechaDPago+"' >"+
						"<input type='hidden' id='vprov' name='vprov' value='"+prov+"'>"+tipo;

	var newcell5 = newrow.insertCell(1);
	newcell5.className ="td2";
	
	newcell5.innerHTML  = "<input type='hidden' id='vnumdoc' name='vnumdoc' value='"+ numdoc  +"' readonly style='width:50px' tabindex='-1' >"+ numdoc;
	
	var newcell6 = newrow.insertCell(2);
	newcell6.className ="td3";
	newcell6.innerHTML = "<input type='hidden' id='vsumSubTotal' name='vsumSubTotal' value='"+ sumSubTotal +"' readonly style='width:50px'>"+ addCommas(sumSubTotal.toFixed(2));

	var newcell3 = newrow.insertCell(3);
	newcell3.className ="td4";
	newcell3.innerHTML ="<input type='hidden' id='vsumIva' name='vsumIva' value='"+sumIva+"' readonly style='width:50px' tabindex='-1'>"+addCommas(sumIva.toFixed(2));

	var newcell4 = newrow.insertCell(4);
	newcell4.className ="td5";
	newcell4.innerHTML ="<input type='hidden' id='vsumTotal' name='vsumTotal' value='"+sumTotal+"' readonly style='width:50px' tabindex='-1'>"+addCommas(sumTotal.toFixed(2));
	
	var newcell5 = newrow.insertCell(5);
	newcell5.className ="td6";
	var functiona="borrarDocto('"+numdoc+"','"+numfact+"' )";
	newcell5.innerHTML ='<input type="button" id="borrarDoc" name="borrarDoc" value="-" onClick="'+functiona+'" >';//onClick='borrarDocto(" +numdoc+","+numfac+")'


}

function borrarDocto(numdoc,numfac)
{
	var answer = confirm("�Seguro deseas borrar este documento?")
	if (answer)
	{
		var index = getIndexFactura(numfac);
		borrado=false;
		for (var j = 0; j <facturasDProv[index]['partidas'].length && borrado==false ;j++)
		{
			if(facturasDProv[index]['partidas'][j]['numDoc']==numdoc)
			{	
				facturasDProv[index]['partidas'].splice( j ,1 ); 
				borrado=true;
			}
		}
	
		if(borrado)
		{
			refreshPartidasdFactura(numfac);
			alert("Elemento borrado");
		}
	}
}


//Calcula el subtotal de los proveedores
function sumTotalDProveedoresXFactua()
{
	numfacturasel=$('#facSelected').val()
	var existe=false;
	
	var  facturacionTotal =0.0;
	var sumprov1 = 0.0;
	var sumprov2 = 0.0;
	var sumprov3 = 0.0;
	var vmontoFactura = 0.0;
	var vnumfact = "";
	var montoSumFact= new Array();
	var countPartidas=0;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < facturasDProv.length && existe==false; i++) 
	{  //Iterate through all but the first row
		//alert('row '+i)
		if(numfacturasel==facturasDProv[i]['numfact'])
		for (var j = 0; j <facturasDProv[i]['partidas'].length && existe==false ;j++)
		{
			if(  facturasDProv[i]['partidas'][j]['tipoDoc']=='st' || facturasDProv[i]['partidas'][j]['tipoDoc']=='nc')
			{
				sumprov1 = sumprov1 + parseFloat(facturasDProv[i]['partidas'][j]['sumSubTotal']);
				sumprov2 = sumprov2 + parseFloat(facturasDProv[i]['partidas'][j]['sumIva']);
				sumprov3 = sumprov3 + parseFloat(facturasDProv[i]['partidas'][j]['sumTotal']);
			}
			else
			{
				sumprov1= sumprov1 -  parseFloat(facturasDProv[i]['partidas'][j]['sumSubTotal']);
				sumprov2= sumprov2 -  parseFloat(facturasDProv[i]['partidas'][j]['sumIva']);
				sumprov3= sumprov3 -  parseFloat(facturasDProv[i]['partidas'][j]['sumTotal']);
			}
			
		}
		
		//alert(facturasDProv[i]['montoFactura']);
		//alert(parseFloat(facturasDProv[i]['montoFactura'].replace(",","")));
		facturacionTotal += parseFloat(facturasDProv[i]['montoFactura'].replace(/,/g,""));
	}	
	
	$('#tSubtotal').val(addCommas(sumprov1.toFixed(2))); 
	$('#tIva').val(addCommas(sumprov2.toFixed(2)));
	$('#tTotal').val(addCommas(sumprov3.toFixed(2)));
	$('#totalFact').val( addCommas(facturacionTotal.toFixed(2)));
	ObjSelsumDocdFact.value=  addCommas(sumprov3.toFixed(2));
	
}


function guardarVale()
{
	var  facturacionTotal =0.0;
	var sumprov1 = 0.0;
	var sumprov2 = 0.0;
	var sumprov3 = 0.0;
	
	var ajuste_vale= document.getElementById("ajuste_vale").value;
	
	var totalFact = $('#totalFact').val()
	
	
	for (var i = 0; i < facturasDProv.length ; i++) 
	{ 
		sumprov3=0;
		for (var j = 0; j <facturasDProv[i]['partidas'].length  ;j++)
		{
			//Sumar solo las notas de credito y los surtinos
			//alert(facturasDProv[i]['partidas'][j]['sumSubTotal']);
			if(facturasDProv[i]['partidas'][j]['tipoDoc']=='st' || facturasDProv[i]['partidas'][j]['tipoDoc']=='nc')
			{
				
				sumprov1 += sumprov1 + parseFloat(facturasDProv[i]['partidas'][j]['sumSubTotal']);
				sumprov2 += parseFloat(facturasDProv[i]['partidas'][j]['sumIva']);
				sumprov3 += parseFloat(facturasDProv[i]['partidas'][j]['sumTotal']);
				//alert(i+": "+j+" sumprov3="+sumprov3);
			}
		}
		
		var diffQ = sumprov3 - parseFloat(  facturasDProv[i].montoFactura.replace(/,/g , "") );
		//alert("if( "+diffQ+" >"+ ajuste_vale  +" || "+diffQ+" <  -( "+ajuste_vale+") )");
		if( diffQ > ajuste_vale   || diffQ <  -( ajuste_vale) )
		{
			alert("Favor de verificar las partidas de la factura '"+facturasDProv[i].numfact+"' exceden  a "+ajuste_vale+" pesos entre el total facturado y los documentos agregados."+ diffQ);
			return (false);
		}

	}	
	
	facturacionTotal=sumprov3;
	//
	var T1=parseFloat( totalFact.replace(/,/g , "") );
	var t1MAS2= T1.toFixed(2) - facturacionTotal.toFixed(2);

	
	if( t1MAS2.toFixed(2) >ajuste_vale || t1MAS2 < - ajuste_vale)
	{
		alert("Favor de verificar las partidas, existe una incongruencia mayor a "+ajuste_vale+" pesos entre el total facturado y los documentos agregados.");
		return(false);
	}

	//return(false);
	var datas = {
					prods		: 		facturasDProv,
					pajuste_vale: ajuste_vale
    			};

	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/generarVale.php',
				data:     datas,
				dataType: "json",
				success: function(resp) 
				{
					console.log(resp);
					//alert(resp)
					if( resp.length ) 
					{
						//var myArray = eval(resp);
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								alert("El vale "+resp[0].id+" ha sido generado!.");
								location.href='php_ajax/crear_Valepdf.php?id='+resp[0].id;
							}
							else
							{
								alert("Error en proceso de actualizacion!."+ resp[0].msgerror);
							}
						}
						else
							alert("Error en proceso de actualizacion!."+ resp);
					}
				}
			});
}



