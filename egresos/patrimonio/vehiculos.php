<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$noeco="";
$uvehiculo="";
$respbien="";
$usuario = $_COOKIE['ID_my_site'];
//$usuario = '001036';
if ($conexion)
{
	$consulta = "select * from patrimoniomsubtipos where tipo=1 and estatus<9000 order by id asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['id']= trim($row['id']);
		$datos[$i]['tipo']= trim($row['tipo']);
		$datos[$i]['descrip']= trim($row['descrip']);
		$datos[$i]['id']= trim($row['id']);
		$i++;
	}
}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/vehiculos_fun.js"></script>
<script language="javascript" src="javascript/busquedaincusu.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>

<p><span class="seccionNota"><strong>Resguardo de Vehiculos</strong></span></p>
<table width="93%" border="0">
  <tr class="texto10">
    <td width="98" height="27"><div align="right">No. Economico</div></td>
    <td width="341"><div align="left" style="position:relative; height:20px; top:1; width:360; left:22; z-index:6">
      <input class="texto8" type="text" name="noeco" id="noeco" disabled style="width:345px; left:36px; " height="18" onKeyUp="searchnumeco(this);" autocomplete="off">
      <div id="search_suggestnumeco" style="z-index:5" > </div>
    </div></td>
    <td width="116"><div align="right">Placas
      <input type="text" name="placas" id="placas" disabled onBlur="valida_placas()" size="10px">
    </div></td>
    <td width="165">Clase
    	<input type="text" name="clase" id="clase" disabled size="20px"></td>
    <td width="56">&nbsp;</td>
    <td width="18">&nbsp;</td>
  </tr>
</table>
<hr class="hrTitForma">
<table width="95%" border="0">
  <tr class="texto10">
    <td width="12%"><div align="right">Subtipo</div></td>
    <td width="10%">
      <select name="subtipo" id="subtipo" disabled class="texto10" onChange="valida_tipo()">
        <?php
				for($i = 0 ; $i<=count($datos);$i++)
				{
					echo "<option value=".$datos[$i]["id"].">".$datos[$i]["descrip"]."</option>\n";
				}
			?>
      </select>
    </td>
    <td width="5%"><div align="right">Marca</div></td>
    <td width="19%"><input type="text" name="marca" id="marca" readonly size="25px"></td>
    <td width="8%"><div align="right">Modelo</div></td>
    <td width="8%"><input type="text" name="modelo" id="modelo" readonly size="10px"></td>
    <td width="4%"><div align="right">Serie</div></td>
    <td width="15%"><input type="text" name="serie" id="serie" readonly size="20px"></td>
    <td width="4%"><div align="right">Color</div></td>
    <td width="15%"><input type="text" name="color" id="color" readonly size="20px"></td>
  </tr>
  <tr class="texto10">
    <td><input type="hidden" name="eco"  id="eco">
	<input type="hidden" name="usuario0"  id="usuario0" value="<?php echo $usuario; ?>">
    <input type="hidden" name="respbien0"  id="respbien0">
    <input type="hidden" name="uvehiculo0"  id="uvehiculo0"></td>
    <td><input type="hidden" name="accion"  id="accion"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto10">
    <td><div align="right">Descripcion</div></td>
    <td colspan="9"><input type="text" name="descrip" disabled id="descrip" size="149px"></td>
  </tr>
  <tr class="texto10">
    <td><div align="right">Responsable</div></td>
    <td colspan="3"><div align="left" style="position:relative; height:20px; top:1; width:360; left:22; z-index:4">
      <input class="texto8" type="text" name="respbien" id="respbien" disabled style="width:345px; left:36px; " height="18" onKeyUp="searchdepto1(this);" autocomplete="off">
      <div id="search_suggestdepto1" style="z-index:3" > </div>
    </div></td>
    <td colspan="2"><div align="right"></div></td>
    <td colspan="4"><div align="left"  style="position:relative; height: 15px; visibility:hidden">
      <input name="uvehiculo" type="text" class="texto8" id="uvehiculo" disabled tabindex="12" onKeyUp="searchdepto2(this);" style="width:320px; visibility:hidden " autocomplete="off">
      <span class="texto9">
      <input type="hidden" name="usu_vehiculo" id="usu_vehiculo">
      </span>      <div id="search_suggestdepto2" style="visibility:hidden "></div>
    </div></td>
  </tr>
  <tr class="texto10">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="texto10">
    <td><div align="right">Observaciones</div></td>
    <td colspan="9"><input type="text" name="observa" id="observa" disabled size="170" ></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table width="80%" border="0" align="center">
  <tr>
    <td><span class="texto9"><img src="../../imagenes/agregar2.jpg" width="50" height="45" title="Agregar Vehiculo" name="boton_agregar" id="boton_agregar" onClick="agregar()"></span></td>
    <td>
		<span class="texto9">
            <img src="../../imagenes/actualiza2.png" width="50" height="45" title="Modificar Datos de Vehiculo" name="boton_cambiar" id="boton_cambiar" onClick="cambiar()">	</span>	</td>
    <td>
		<span class="texto9">
            <img src="../../imagenes/eliminar.jpg" width="50" height="45" title="Cancelar Datos de Vehiculo" name="boton_cancelar" id="boton_cancelar" onClick="cancelar()"> </span>
	</td>
    <td>
		<span class="texto9">
            <img src="../../imagenes/deshacer.jpg" width="50" height="45" title="Deshacer Captura" name="boton_guardar" id="boton_deshacer" onClick="deshacer()" > </span>
	</td>
    <td>
		<span class="texto9">
            <img src="../../imagenes/guardar3.jpg" width="50" height="45" title="Actualizar Datos de Vehiculo" name="boton_guardar" id="boton_guardar" onClick="guardar()" style="visibility:hidden " > </span>
	</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
