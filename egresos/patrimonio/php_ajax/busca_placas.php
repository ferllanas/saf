<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$placas = trim($_REQUEST['placas']);
$serie = trim($_REQUEST['serie']);
$valida = $_REQUEST['valida'];
$datos=array();

if ($conexion)
{		
	
	if(strlen($placas)>0 || strlen($serie)>0)
	{
		$consulta = "select * from patrimoniodvehiculos where ";
		if($valida==1)
		{
			$consulta .= "placas='$placas' ";
		}
		else
		{
			$consulta .= "serie='$serie' ";
		}
		$consulta .= "and estatus<9000";
	}
	//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['numeco']= trim($row['numeco']);
			$datos[$i]['placas']= trim($row['placas']);
			$datos[$i]['serie']= trim($row['serie']);
			$i++;
		}
	}
}
echo json_encode($datos);
?>