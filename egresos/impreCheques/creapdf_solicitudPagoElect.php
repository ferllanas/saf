<?php

header("Content-type: text/html; charset=UTF8");
include('../../Administracion/globalfuncions.php');
require_once("../../connections/dbconexion.php");
require_once("../../dompdf/dompdf_config.inc.php");

$dompdf = new DOMPDF();	
$dompdf->set_paper('letter');
$folio=$_REQUEST['folio'];

$fechaactual="";
$concepto="";
$numcheque="";
		$command="SELECT a.folio, a.descrip, a.banco, b.nombanco, b.cuenta_ban, a.total, num_transf , c.nombre  
				,d.prov, d.nomprov, b.tipoche
				, e.sol_transf, e.aut_transf,
				convert(varchar(10),a.falta,103) as falta
		FROM egresosmcheque a 
		INNER JOIN egresosmbancos b ON a.banco=b.banco 
		LEFT JOIN menumusuarios c ON a.usuemision=c.usuario
		INNER JOIN compramprovs d ON a.prov=d.prov
		LEFT JOIN configmconfig e ON a.falta>=e.falta AND (a.falta<=e.fbaja OR e.fbaja is null)
		WHERE a.folio=".$folio."  AND a.tipo=20 AND a.estatus<9000";

			//echo $command;
			
			$renglones="";
			$autoriza="";
			$solicita="";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r(sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				//echo $resoponsecode;
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$numcheque=$row['num_transf'];
					$fechaactual=$row['falta'];
					$elabora=trim($row['nombre']);
					$concepto=trim($row['descrip']);
					$renglones.='<tr>
									<td style="width:100/3%">
										<table style="width:100%;">
											<tr>
												<td style="text-align:center; width:30%;">'.trim($row['banco']).'</td>
												<td style="text-align:right; width:70%;">'.trim($row['cuenta_ban']).": <br>".trim($row['nombanco']).'</td>
											</tr>
										</table>
									</td>
					<td style="width:100/3%">
									<table style="width:100%;">
										<tr>
											<td style="text-align:center; width:30%">'.$row['prov'].'</td>
											<td style="text-align:justify; width:70%">'.$row['nomprov'].'</td>
										</tr>
									</table>
					</td>
					<td style="text-align:center; vertical-align:right; width:100/3%">$'.number_format($row['total'],2).'</td></tr>
					<tr><td colspan="3">'.getValesOSolCheFromCheque($folio).'</td>
										</tr>';
					
					$solicita=trim($row['sol_transf']);
					$autoriza=trim($row['aut_transf']);
				}
			}
			
			sqlsrv_free_stmt( $stmt);
			
			
			//$fechaactual = date('m/d/Y');//"07/17/2014";//
			list($days,$month,$years)= explode('/', $fechaactual);
			$diadesem=date("w", mktime(0, 0, 0,$month, $days, $years));//date("w");//4;//
			$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
			
					
					
$htmlstr='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
		<style>
		body 
		{
			font-family: "Arial";
			font-size: 9;
			width: 601px; 
			margin-top: 30px;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		.texto8{
			font-size: 8pt;
		}
		</style>
		
		</head>
		
		<body >
		<table style="width: 100%;">
			<tr style="heigth:6px;">
            	<td style="width: 15%;" align="center">
                	<img src="'.getDirAtras(getcwd())."".$imagenPDFPrincipal.'"style="width:120px;">
                </td>
				<td style="width: 70%;" >
					<table style="width: 100%;" >
						<tr><td style="width: 50%; text-align:center;">COORDINACI&Oacute;N DE EGRESOS Y CONTROL PATRIMONIAL</td></tr>
                        <tr><td style="width: 50%; text-align:center;"><u>SOLICITUD DE PAGO ELECTRONICO</u></td></tr>
					</table>
				</td>
				<td style="width: 15%;" align="center"><img src="'.getDirAtras(getcwd())."".$imagenPDFSecundaria.'" style="width:80px;">
                </td>
			</tr>
            <tr>
            	<td colspan="3">
                	<table style="width:100%;">
                    	<tr><td style="text-align:left; width:50%;">'.$ffecha.'</td>
                			<td style="text-align:left; width:50%;">N&uacute;mero de Transferencia <b>'.$numcheque.'</b>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3" style="width:100%; text-align:right; font-size:8px;">
					<div style="position:absolute; left:0px; top:150px; width:95%; ">&nbsp;<div></td>
            </tr>
			<tr>
            	<td colspan="3" style="width:100%;">
					<div style="position:absolute; left:0px; top:150px; width:95%; height:300px; border: 1pt solid black;">
                	<table style="width:100%;vertical-align:top;">
                    	<thead style="vertical-align:top;">
                    	<tr>
                        	<th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE ORIGEN</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">BENEFICIARIO</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">IMPORTE</th>
                        </tr>
                        </thead>
                        <tbody >
						<tr><td colspan="3"><hr></td></tr>';
						

			
			 $htmlstr.=$renglones;
			 
			$htmlstr.='</tbody>
                    </table>
					</div>
                </td>
            </tr>	
            <tr>
            	<td colspan="3"><div style="position:absolute; left:0px; top:500px; width:95%%; height:18px;  border-color:#000; border-width:medium;"><b>POR CONCEPTO</b><div>
				</td>
            </tr>
            <tr><td colspan="3">
					<div style="position:absolute; left:0px; top:518px; width:95%; height:70px; border-color:#000; border-width:medium;border: 1pt solid black;">
						<table style="width:100%; height:60px; border:#000; border-width:1px; vertical-align:top">
							<tr><td style="vertical-align:top;">'.utf8_decode($concepto).'</td></tr>
						</table>
					</div>
				</td>
			</tr>
            <tr>
            	<td colspan="3">
					<div style="position:absolute; left:0px; top:708px; width:95%; height:70px;">
                	<table style="width:100%;">
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
                        </tr>
                        <tr>
                        	<td style="text-align:center; width:30%;">ELABORA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">SOLICITA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">AUTORIZA</td>
                        </tr>
                         <tr>
                        	<td style="text-align:center; width:30%;">'.utf8_decode($elabora).'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.utf8_decode($solicita).'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.utf8_decode($autoriza).'</td>
                        </tr>
                    </table>
					</div>
                </td>
            </tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
             <tr>
            	<td colspan="3" align="center" style="text-align:center;">
					<table style="width:100%; text-align:center;" align="center"><tr>
                            <td style="border-bottom-width:1; border-bottom-color:#000; width:150px; text-align:center;"><hr></td>
                        </tr>
                        <tr>
                            <td style="text-align:center; width:150px;">'.utf8_decode("APLICACIÓN").' PRESUPUESTAL</td>
                        </tr>
                         <tr>
                            <td style="text-align:center; width:150px;">&nbsp;</td>
                        </tr>
					</table>
					
                </td>
            </tr>
		</table>
	</body>
</html> ';

//echo $htmlstr;
	
$dompdf->load_html($htmlstr);

$dompdf->render();
	  
$pdf = $dompdf->output(); 

$dompdf->stream("downloaded.pdf");


		
function getValesOSolCheFromCheque($folio)
{
	global $conexion_srv;
	$returned="<table width='400px' align='center'>
					<thead>
						<tr>
							<th width='150px'>Solitud/Factura</th>
							<th width='150px'>Importe</th>
						</tr>
					</thead>
					<tbody>";
	$command="SELECT a.numdocto, a.tipodocto, a.factura, b.descrip, 
		CASE WHEN a.tipodocto = 6 THEN
					(SELECT e.importe FROM egresosmsolche e WHERE e.folio=a.numdocto )
				ELSE
					(SELECT f.total FROM egresosmvale f WHERE f.vale=a.numdocto)  END as importe,
		CASE WHEN a.tipodocto = 6 THEN
					(SELECT e.concepto FROM egresosmsolche e WHERE e.folio=a.numdocto )
				ELSE
					(SELECT f.concepto FROM egresosmvale f WHERE f.vale=a.numdocto)  END as concepto
		FROM egresosdcheque a 
		INNER JOIN egresostipodocto b ON a.tipodocto=b.id 
		WHERE a.folio=".$folio."  AND a.estatus<9000 ORDER BY a.tipodocto";

			$autoriza="";
			$solicita="";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r(sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					if($row['tipodocto']=='7')
						$returned.=  "<tr><td style='text-align=left;' align='left'>*Factura:".$row['factura']." de Vale: <b>".$row['numdocto']."</b></td><td align='right'><b>$".number_format($row['importe'],2)."</b></td></tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Concepto:</b>".$row['concepto']."</td></tr>";//." Factura: ".$factura;//
					else
						$returned.=  "<tr><td style='text-align=left;' align='left'>*".$row['descrip'].": <b>".$row['numdocto']."</b></td><td align='right'><b>$".number_format($row['importe'],2)."</b></td></tr><tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Concepto:</b>".$row['concepto']."</td></tr>";
				}
			}
			$returned.="</tbody></table>";
			sqlsrv_free_stmt( $stmt);
   return $returned;
}
?>