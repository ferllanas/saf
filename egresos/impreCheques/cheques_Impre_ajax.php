<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);

$banco="";
if(isset($_REQUEST['banco']))
	$banco = $_REQUEST['banco'];
	
$tipo="";
if(isset($_REQUEST['tipo']))
	$tipo = $_REQUEST['tipo'];
	
$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
				$command= "select a.".$tipo.", a.folio, CONVERT(varchar(12),a.falta, 103) as falta, b.nomprov, a.descrip,a.total, a.estatus from egresosmcheque a INNER JOIN compramprovs b ON a.prov=b.prov WHERE a.".$tipo." LIKE '%" . substr($_REQUEST['query'],1) . "%' ";
			if($tipo=='num_transf')
			{
				$command .=" AND a.num_transf is not NULL AND a.num_transf !='' ";
			}
			else
				$command .=" AND a.num_ch is not NULL AND a.num_ch !=0 ";
			
			$command .=" order by a.".$tipo;
	}
	//echo $command;
		
	if($opcion==2)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
				

				$command= "select a.".$tipo." , a.folio, CONVERT(varchar(12),a.falta, 103) as falta,b.nomprov , a.descrip,a.total, a.estatus from egresosmcheque a ";
				$command .="INNER JOIN compramprovs b ON a.prov=b.prov WHERE nomprov LIKE '%" . $nom1 . "%' ";
											if($cuenta>1)
											{
												$command .="and nomprov LIKE '%" . $nom2 . "%'";
											}
				if(strlen($banco)>0)
				{
					$command .=" AND a.banco='$banco'";
				}
				if($tipo=='num_transf')
				{
					$command .=" AND a.num_transf is not NULL AND a.num_transf !='' ";
				}
				else
					$command .=" AND a.num_ch is not NULL AND a.num_ch !=0 ";
				
				if($tipo=='num_ch')
					$command .=" AND a.tipo=10";
				else
					$command .=" AND a.tipo=20";
											
											$command .="  order by a.".$tipo;
	}
	
	//echo $command;
	if($opcion==3)
	{
				$command= "select a.".$tipo.", a.folio, CONVERT(varchar(12),a.falta, 103) as falta, a.descrip,a.total, a.estatus, c.nomprov from egresosmcheque a 
								INNER JOIN compramprovs c ON  a.prov=c.prov 
								
								WHERE a.folio LIKE '%" . substr($_REQUEST['query'],1) . "%'";
				if(strlen($banco)>0)
				{
					$command .=" AND a.banco='$banco'";
				}
				if($tipo=='num_transf')
				{
					$command .=" AND a.num_transf is not NULL AND a.num_transf !='' ";
				}
				else
					$command .=" AND a.num_ch is not NULL AND a.num_ch !=0 ";
				
				if($tipo=='num_ch')
					$command .=" AND a.tipo=10";
				else
					$command .=" AND a.tipo=20";
											
											$command .="  order by a.".$tipo;
								
								
	}		
		//echo $command;
	if($opcion==4)
 	{
				$command= "select a.".$tipo." , a.folio, CONVERT(varchar(12),a.falta, 103) as falta, b.nomprov , a.descrip,a.total, a.estatus from egresosmcheque a 
											INNER JOIN compramprovs b ON a.prov=b.prov WHERE descrip LIKE '%" . substr($_REQUEST['query'],1) . "%'";
				if(strlen($banco)>0)
				{
					$command .=" AND a.banco='$banco'";
				}
				
				if($tipo=='num_transf')
				{
					$command .=" AND a.num_transf is not NULL AND a.num_transf !='' ";
				}
				else
					$command .=" AND a.num_ch is not NULL AND a.num_ch !=0 ";
				
				if($tipo=='num_ch')
					$command .=" AND a.tipo=10";
				else
					$command .=" AND a.tipo=20";
				
											
				$command .="  order by a.".$tipo;

     }
	//echo "Antes de pasar al 5";
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select a.".$tipo." , a.folio, CONVERT(varchar(12),a.falta, 103) as falta, b.nomprov , a.descrip, a.total, a.estatus 
							from egresosmcheque a INNER JOIN compramprovs b ON a.prov=b.prov where a.femision >= '" . $fini . "' and a.femision <='" . $ffin . "' ";
				if(strlen($banco)>0)
				{
					$command .=" AND a.banco='$banco'";
				}
				
				if($tipo=='num_transf')
				{
					$command .=" AND (a.num_transf is not NULL AND a.num_transf !='') ";
				}
				else
					$command .=" AND a.num_ch is not NULL AND a.num_ch !=0 ";
				
				if($tipo=='num_ch')
					$command .=" AND a.tipo=10";
				else
					$command .=" AND a.tipo=20";
											
				$command .="  order by a.".$tipo;
				 
	}	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					//$datos[$i]['num_transf']=trim($row['num_transf']);
					$datos[$i]['tipo']=	$tipo;
					$datos[$i]['num_ch']=	trim($row[$tipo]);
					$datos[$i]['folio']=	trim($row['folio']);
					$datos[$i]['falta']=	trim($row['falta']);					
					$datos[$i]['nomprov']=	trim(utf8_encode($row['nomprov']));
					if($row['estatus']<'9000')
						$datos[$i]['importe']=	"$".number_format($row['total'],2);
					else
						$datos[$i]['importe']=	"$ 0.00";
					$datos[$i]['concepto']=	utf8_encode($row['descrip']);
					$datos[$i]['estatus']=	trim($row['estatus']);
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>