<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
				$command= "  SELECT a.folio, c.nombanco, a.num_ch, b.nomprov, CONVERT(varchar(12),a.femision, 103) as fpago, a.total FROM egresosmcheque a LEFT JOIN compramprovs b ON a.prov=b.prov
  LEFT JOIN egresosmbancos c  ON a.banco= c.banco WHERE a.estatus=20";
  	
		$termino=substr($_REQUEST['query'],1);
  		if(strlen($termino)>0)
  			$command.=" AND a.num_ch LIKE '%" . substr($_REQUEST['query'],1) . "%'";
			
		$command.="	order by a.num_ch";
	}
	if($opcion==2)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
				

				$command= "  SELECT a.folio, c.nombanco, a.num_ch, b.nomprov, CONVERT(varchar(12),a.femision, 103) as fpago, a.total FROM egresosmcheque a LEFT JOIN compramprovs b ON a.prov=b.prov
  LEFT JOIN egresosmbancos c  ON a.banco= c.banco WHERE a.estatus=20 AND c.nombanco LIKE '%" . $nom1 . "%'  order by num_ch";
	}
	
	//echo $command;
	if($opcion==3)
	{
				$command= " SELECT a.folio, c.nombanco, a.num_ch, b.nomprov, CONVERT(varchar(12),a.femision, 103) as fpago, a.total FROM egresosmcheque a LEFT JOIN compramprovs b ON a.prov=b.prov
  LEFT JOIN egresosmbancos c  ON a.banco= c.banco
								WHERE a.estatus=20 AND b.nomprov LIKE '%" . substr($_REQUEST['query'],1) . "%'  order by num_ch";
	}		
	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$datos[$i]['folio']=trim($row['folio']);
					$datos[$i]['nombanco']=trim($row['nombanco']);
					$datos[$i]['num_ch']=trim($row['num_ch']);
					$datos[$i]['nomprov']=trim(utf8_encode($row['nomprov']));					
					$datos[$i]['fpago']=trim($row['fpago']);
					$datos[$i]['total']=number_format($row['total'],2);
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>