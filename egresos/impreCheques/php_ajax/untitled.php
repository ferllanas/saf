<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
		<style>
		body 
		{
			font-family: "Arial";
			font-size: 9;
			width: 601px; 
			margin-top: 30px;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body >
		<table style="width: 100%;">
			<tr style="heigth:6px;">
            	<td style="width: 15%;" align="center">
                	<img src="../../../<?php echo $imagenPDFPrincipal;?>" style="width:80px;">
                </td>
				<td style="width: 70%;" >
					<table style="width: 100%;" >
						<tr><td style="width: 50%; text-align:center;">COORDINACI?N DE EGRESOS Y CONTROL PATRIMONIAL</td></tr>
                        <tr><td style="width: 50%; text-align:center;"><u>SOLICITUD DE PAGO ELECTRONICA</u></td></tr>
					</table>
				</td>
				<td style="width: 15%;" align="center"><img src="../../../<?php echo $imagenPDFSecundaria;?>" style="width:80px;">
                </td>
			</tr>
            <tr>
            	<td colspan="3">
                	<table style="width:100%;">
                    	<tr><td style="text-align:left; width:50%;">Viernes, 28 de Diciembre de 2012</td>
                			<td style="text-align:left; width:50%;">
                            	<table style="width:100%;">
                                	<tr><td style="text-align:right;">N?mero de Transferencia</td>
                                    	<td style="text-align:center;">4012120061  </td>
                                     </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3" style="width:100%; text-align:right; font-size:8px;">#N/A</td>
            </tr>
			<tr>
            	<td colspan="3" style="width:100%;">
					<div style="position:absolute; left:0px; top:150px; width:100%; height:300px; border-color:#000; border-width:medium;">
                	<table style="width:100%; border:#000; border-width:1px; vertical-align:top;">
                    	<thead style="vertical-align:top;">
                    	<tr>
                        	<th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE ORIGEN</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE DESTINO</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">IMPORTE</th>
                        </tr>
                        </thead>
                        <tbody style="height:300px;"><tr>
									<td style="width:100/3%">
										<table style="width:100%;">
											<tr>
												<td style="text-align:center; width:30%;">0040</td>
												<td style="text-align:justify; width:70%;">054-42392-6<br>BANORTE</td>
											</tr>
										</table>
									</td>
					<td style="width:100/3%">
									<table style="width:100%;">
										<tr>
											<td style="text-align:center; width:30%">148</td>
											<td style="text-align:justify; width:70%">ALLIANCE SOLUCIONES, S.A. DE C.V.                 </td>
										</tr>
									</table>
					</td>
					<td style="text-align:center; vertical-align:right; width:100/3%">$59,818.88</td></tr></tbody>
                    </table>
					</div>
                </td>
            </tr>	
            <tr>
				
            		<td colspan="3"><div style="position:absolute; left:0px; top:500px; width:100%; height:18px; border-color:#000; border-width:medium;">POR CONCEPTO<div></td>
				
            </tr>
            <tr><td colspan="3">
					<div style="position:absolute; left:0px; top:518px; width:100%; height:70px; border-color:#000; border-width:medium;">
						<table style="width:100%; height:60px; border:#000; border-width:1px; vertical-align:top">
							<tr><td style="vertical-align:top;">zaasas sasas asasas asasas</td></tr>
						</table>
					</div>
				</td>
			</tr>
            <tr>
            	<td colspan="3">
					
                	<table style="width:100%;">
                    	<tr>
                        	<td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:200px;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:200px;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:200px;">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td style="text-align:center; width:200px;">ELABORA</td>
                            <td style="text-align:center; width:200px;">SOLICITA</td>
                            <td style="text-align:center; width:200px;">AUTORIZA</td>
                        </tr>
                         <tr>
                        	<td style="text-align:center; width:200px;">CESAR AUGUSTO ACOSTA LEAL</td>
                            <td style="text-align:center; width:200px;">solicita</td>
                            <td style="text-align:center; width:200px;">autoriza</td>
                        </tr>
                    </table>
					
                </td>
            </tr>
             <tr>
            	<td colspan="3" align="center" style="text-align:center;">
                	
					<table style="width:100%; text-align:center;" align="center"><tr>
                            <td style="border-bottom-width:1; border-bottom-color:#000; width:200px; text-align:center;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align:center; width:200px;">APLICACI?N PRESUPUESTAL</td>
                        </tr>
                         <tr>
                            <td style="text-align:center; width:200px;">$aplicacionpresupuestal</td>
                        </tr>
					</table>
					
                </td>
            </tr>
		</table>
	</body>
</html> 