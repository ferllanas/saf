<?php
header("Content-type: text/html; charset=ISO-8859-1");
$fecActual= getdate();
$fecimpresion="";

$datos= array();

//echo $_REQUEST['fechaimpresion'];
if(isset($_REQUEST['fechaimpresion']))
{
	$fecimpresion= $_REQUEST['fechaimpresion'];
	
	list($dd,$mm,$aa)=explode("/",$fecimpresion);
	$end=$aa."-".$mm."-".$dd." 23:30:00";
	
//	echo $end;
	
	$fecemision= date_create($end);
	
	$diaDeLaSemana= date_format($fecemision,"N");
	$diaDeLaSemanaHoy= date_format(date_create(),"N");
	
	//echo date_format($fecemision,"%Y/%M/%d")."<br>";
	//echo date_format(date_create(),"%Y/%M/%d")."<br>";
	$intervalo = date_diff($fecemision,date_create());
	
	//print_r($intervalo);
	if($fecemision<date_create())
	{
		$datos['error']="La fecha de la emisión no puede ser antes del día de hoy .";
		$datos['numerror']=1;
	}
	else
	{
		if($intervalo->d >5)
		{
			$datos['error']="La fecha de la emisión no puede exceder a dos días habiles.";
			$datos['numerror']=1;
			$datos['days']=$intervalo->days;
		}
		else
		{
			if($intervalo->d == 0)
			{
				$datos['error']="OK.";
				$datos['numerror']=0;
			}
			else
			{
				if($diaDeLaSemana>5)
				{
					$datos['error']="La fecha de la emisión no puede establecerse en fin se semana.";
					$datos['numerror']=2;
				}
				else
				{
					if($diaDeLaSemanaHoy<$diaDeLaSemana)
					{
						if(($diaDeLaSemana-$diaDeLaSemanaHoy)>2)
						{
							$datos['error']="La fecha de la emisión no puede exceder a dos días habiles.";
							$datos['numerror']=3;
						}
						else
						{
							$datos['error']="OK.";
							$datos['numerror']=0;
						}
					}
					else
					{
						echo ($diaDeLaSemanaHoy+2)	-	($diaDeLaSemana+7)." ".($diaDeLaSemanaHoy+2)." " . ($diaDeLaSemana+7);
						if(	( ($diaDeLaSemana+7)	-	($diaDeLaSemanaHoy+2) )>2)
						{
							$datos['error']="La fecha de la emisión no puede exceder a dos días habiles.";
							$datos['numerror']=4;
						}
						else
						{
							$datos['error']="OK.";
							$datos['numerror']=0;
						}
					}
				}
			}
		}
	}
}
else
{
	$datos['error']="No ha especificado una fecha de emisión.";
	$datos['numerror']=5;
}

echo json_encode($datos);
?>
