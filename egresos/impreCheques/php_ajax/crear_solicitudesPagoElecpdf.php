<?php
header("Content-type: text/html; charset=UTF8");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
require_once("../../../dompdf/dompdf_config.inc.php");

error_reporting(0);
$usarioCreador = $_COOKIE['ID_my_site'];	

$facturas=array();

$folio_ImpCheque="";
if(isset($_POST['folio_ImpCheque']))
	$folio_ImpCheque=$_POST['folio_ImpCheque'];

if(isset($_POST['prods']))
	$facturas=$_POST['prods'];


//Cambios aqui 11/07/2014
$qfechaemision= date("Y/m/d");//"2015/12/28";//
if(isset($_POST['qfechaemision']))
{
	$qfechaemision=$_POST['qfechaemision'];
	$qfechaemision=convertirFechaEuropeoAAmericano($qfechaemision);
}

$tipoDoc=0;
if(isset($_POST['tipoDoc']))
	$tipoDoc= $_POST['tipoDoc'];
	

$htmlstr="";
$folio=0;
$fecha="";
$nomprov="";
$numprov="";
$concepto="";
$totalVale="";
$fechaPago="";
$quienReviso="";
$totalValeLetra="";
$tfacturas="";
$cantidadletras ="";
$num_che="";
$numcheque="";
$arrayCuentas=array();
$Sale=true;
$datosFin=array();
if(count($facturas)>0)
{
	//$old_limit = ini_set("memory_limit", "16M");
	
	
	$j=0;
	//$numcheque = $folio_ImpCheque;
	
	$Allcheques=getfirsthtml();
	$Allpdfs = new DOMPDF();	
	$Allpdfs->set_paper('letter');
	for( $i = 0; $i < count($facturas) ;  $i++)
	{
		
		
		if($facturas[$i]['selected']=='true' )
		{
			
			$dompdf = new DOMPDF();
	
			$dompdf->set_paper('letter');
			
			
			$j++;
			$folito= (string)$facturas[$i]['folio_cheque'];
			
			//echo $folito;
			$command = "SELECT a.folio, a.descrip, a.banco, b.nombanco, total, num_transf , c.nombre  , d.nomprov, b.tipoche
								FROM egresosmcheque a 
								INNER JOIN egresosmbancos b ON a.banco=b.banco 
								LEFT JOIN menumusuarios c ON a.usuemision=c.usuario
								INNER JOIN compramprovs d ON a.prov=d.prov
						WHERE a.folio=";
			$command .= " ".$folito." ";
			$command .=" AND a.tipo=20 AND a.estatus<9000";
			//echo $command;
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r( sqlsrv_errors(), true));
			}
			else
			{
				//echo "Si estaba pasando por aqui we";
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					
					//print_r($row);
					$Sale=false;
					//$num_che= (double)$row['folioban'] + $j; //provisional antes del procedimineto almacenado
					$TotalCadena ="(".strtoupper(num2letras($row['total'])).")";//redondear_dos_decimal(trim()))).")";
					
					$Total = number_format(trim($row['total']),2);//money_format($fmt, trim($row['total']));
					$banco = trim($row['banco']);
					$nombanco= trim($row['nombanco']);
					$nomprov = trim($row['nomprov']);
					$folio = trim($row['folio']);
					$concepto = trim($row['descrip']);
					$tipoche= trim($row['z']);
					
					//Cambios aqui 11/07/2014
					$fechaactual = date('m/d/Y');//"28/12/2015";//
					list($month,$days,$years)= explode('/', $fechaactual);
					$diadesem=1;//date("w");//
					$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
					
					
					/////////
					
					$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion

					$tsql_callSP ="{call sp_egresos_A_dcheque_ctas(?,?,?,?,?)}";//Arma el procedimeinto almacenado
					$params = array(&$folio, &$usarioCreador, &$banco,&$qfechaemision,&$tipoDoc);//Arma parametros de entrada
					//echo "AAAA";
					//print_r($params);
					//echo "BBBB";
					$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
					
					$fails=false;
					
					sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );
 
					$stmt = sqlsrv_query($conexion,$tsql_callSP, $params, $options);// $command);//
				
					if ( $stmt === false)
					{ 
						
						$resoponsecode="02";
						die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
					}
					else
					{
					
						$g=0;
						while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
						{
							
							$arrayCuentas[$g]['cons']=     $row['cons'];
							$arrayCuentas[$g]['error']=    $row['error'];
							$arrayCuentas[$g]['concepto']= $row['concepto'];
							$arrayCuentas[$g]['debe']=     $row['debe'];
							$arrayCuentas[$g]['haber']=    $row['haber'];
							$tok = explode(".",$row['cuenta']);
							
							$arrayCuentas[$g]['SSSCTA']=$tok[count($tok)-1];
							$arrayCuentas[$g]['SSCTA']=$tok[count($tok)-2];
							
							$arrayCuentas[$g]['CUENTA']="";
							for($x=0; $x < count($tok)-2; $x++)
							{
								if($x==0)
									$arrayCuentas[$g]['CUENTA'].= $tok[$x];
								else
									$arrayCuentas[$g]['CUENTA'].= ".".$tok[$x];
							}
							$g++;
							$num_che= $row['numtr'];
							
						}
						
					}
				
					sqlsrv_free_stmt( $stmt);

					
					
					$htmlstr = createPageFomat2($num_che, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
					$Allcheques .= Add_Cheque($num_che, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
					


					 if( ($i+1) < count($facturas) )
					 {
						$htmlstr.="<div style='page-break-after: always;'></div>";//$dompdf->new_page();  
						$Allcheques.="<div style='page-break-after: always;'></div>";
					 }
					else
					{
						$htmlstr .= "</body></html>";
						$Allcheques.= "</body></html>";
					}

					$dompdf->load_html($htmlstr);
					//$datosFin[$j]['html']=$htmlstr;
					//$numcheque++;
				}
			}
			
			if(!$Sale)
			{	$dompdf->render();
	  
				$pdf = $dompdf->output(); 
			
				$datos[0]['FILE'][$j-1]="pdf_files/transf_".$folio.".pdf";
				file_put_contents("../pdf_files/transf_".$folio.".pdf", $pdf);
				
				$datosFin[0]['transferencia'][$j-1]['folio']=$folio;
			}
		}
	}
	
	if(!$Sale)
	{
		$Allpdfs->load_html($Allcheques);
		$Allpdfs->render();
		$pdfalls = $Allpdfs->output(); 
		
		$datosFin['FILE']="pdf_files/transf_a_imprimir_".date("dmYHisu").".pdf";//[0]
		file_put_contents("../pdf_files/transf_a_imprimir_".date("dmYHisu").".pdf", $pdfalls);
		$datosFin['fallo']=false;
	}
	else
	{
		$datosFin['fallo']=false;
		$datosFin['msgerror']="No son validos los cheques, seleccionados";
		
	}
	//$datos[0]['Countdatos']=count($arrayCuentas).",".$command;
}

echo json_encode($datosFin);
	
	
	//location
  	//$dompdf->stream("vale_".$numvale.".pdf");/**/
	
	function getfirsthtml()
	{
		$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body style='width: 601px; margin-top:30px'>";
		
		return $htmlstr;
	}
	
	function Add_Cheque($numcheque, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas)
	{
		global $conexion_srv;
		$elabora="";
		$htmlstr='		<table style="width: 100%;">
			<tr style="heigth:6px;">
            	<td style="width: 15%;" align="center">
                	<img src="'.getDirAtras(getcwd())."".$imagenPDFPrincipal.'" style="width:80px;">
                </td>
				<td style="width: 70%;" >
					<table style="width: 100%;" >
						<tr><td style="width: 50%; text-align:center;">COORDINACI�N DE EGRESOS Y CONTROL PATRIMONIAL</td></tr>
                        <tr><td style="width: 50%; text-align:center;"><u>SOLICITUD DE PAGO ELECTRONICO</u></td></tr>
					</table>
				</td>
				<td style="width: 15%;" align="center"><img src="'.getDirAtras(getcwd())."".$imagenPDFSecundaria.'" style="width:80px;">
                </td>
			</tr>
            <tr>
            	<td colspan="3">
                	<table style="width:100%;">
                    	<tr><td style="text-align:left; width:50%;">'.$ffecha.'</td>
                			<td style="text-align:left; width:50%;">
                            	<table style="width:100%;">
                                	<tr><td style="text-align:right;">N�mero de Transferencia</td>
                                    	<td style="text-align:center;"><b>'.$numcheque.'</b></td>
                                     </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3" style="width:100%; text-align:right; font-size:8px;">
					<div style="position:absolute; left:0px; top:150px; width:95%; ">&nbsp;<div></td>
            </tr>
			<tr>
            	<td colspan="3" style="width:100%;">
					<div style="position:absolute; left:0px; top:150px; width:95%; height:300px; border: 1pt solid black;">
                	<table style="width:100%;vertical-align:top;">
                    	<thead style="vertical-align:top;">
                    	<tr>
                        	<th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE ORIGEN</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">BENEFICIARIO</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">IMPORTE</th>
                        </tr>
                        </thead>
                        <tbody >
						<tr><td colspan="3"><hr></td></tr>';
						
		$command="SELECT a.folio, a.descrip, a.banco, b.nombanco, b.cuenta_ban, a.total, num_transf , c.nombre  ,d.prov, d.nomprov, b.tipoche, 
e.sol_transf, e.aut_transf
		FROM egresosmcheque a 
		INNER JOIN egresosmbancos b ON a.banco=b.banco 
		LEFT JOIN menumusuarios c ON a.usuemision=c.usuario
		INNER JOIN compramprovs d ON a.prov=d.prov
		LEFT JOIN configmconfig e ON e.estatus=0
WHERE a.folio=".$folio."  AND a.tipo=20 AND a.estatus<9000";

			$autoriza="";
			$solicita="";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r(sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$elabora=trim($row['nombre']);
					$htmlstr.='<tr>
									<td style="width:100/3%">
										<table style="width:100%;">
											<tr>
												<td style="text-align:center; width:30%;">'.trim($row['banco']).'</td>
												<td style="text-align:justify; width:70%;">'.trim($row['cuenta_ban'])."<br>".trim($row['nombanco']).'</td>
											</tr>
										</table>
									</td>
					<td style="width:100/3%">
									<table style="width:100%;">
										<tr>
											<td style="text-align:center; width:30%">'.$row['prov'].'</td>
											<td style="text-align:justify; width:70%">'.$row['nomprov'].'</td>
										</tr>
										
									</table>
					</td>
					<td style="text-align:center; vertical-align:right; width:100/3%">$'.number_format($row['total'],2).'</td></tr>
					<tr><td colspan="3">'.getValesOSolCheFromCheque($folio).'</td>
										</tr>';
					
					$solicita=utf8_encode(trim($row['sol_transf']));
					$autoriza=utf8_encode(trim($row['aut_transf']));
				}
			}
			
			sqlsrv_free_stmt( $stmt);
                        $htmlstr.='</tbody>
                    </table>
					</div>
                </td>
            </tr>	
            <tr>
            	<td colspan="3"><div style="position:absolute; left:0px; top:500px; width:95%%; height:18px;  border-color:#000; border-width:medium;"><b>POR CONCEPTO</b><div>
				</td>
            </tr>
            <tr><td colspan="3">
					<div style="position:absolute; left:0px; top:518px; width:95%; height:70px; border-color:#000; border-width:medium;border: 1pt solid black;">
						<table style="width:100%; height:60px; border:#000; border-width:1px; vertical-align:top">
							<tr><td style="vertical-align:top;">'.$concepto.'</td></tr>
						</table>
					</div>
				</td>
			</tr>
            <tr>
            	<td colspan="3">
					<div style="position:absolute; left:0px; top:708px; width:95%; height:70px;">
                	<table style="width:100%;">
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
                        </tr>
                        <tr>
                        	<td style="text-align:center; width:30%;">ELABORA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">SOLICITA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">AUTORIZA</td>
                        </tr>
                         <tr>
                        	<td style="text-align:center; width:30%;">'.$elabora.'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.$solicita.'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.$autoriza.'</td>
                        </tr>
                    </table>
					</div>
                </td>
            </tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
             <tr>
            	<td colspan="3" align="center" style="text-align:center;">
					<table style="width:100%; text-align:center;" align="center"><tr>
                            <td style="border-bottom-width:1; border-bottom-color:#000; width:150px; text-align:center;"><hr></td>
                        </tr>
                        <tr>
                            <td style="text-align:center; width:150px;">APLICACI�N PRESUPUESTAL</td>
                        </tr>
                         <tr>
                            <td style="text-align:center; width:150px;">&nbsp;</td>
                        </tr>
					</table>
					
                </td>
            </tr>
		</table>';//
		return $htmlstr;
	}



	function createPageFomat2($numcheque, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas)
	{
		global $conexion_srv;
		$elabora="";
		$htmlstr='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
		<style>
		body 
		{
			font-family: "Arial";
			font-size: 9;
			width: 601px; 
			margin-top: 30px;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body >
		<table style="width: 100%;">
			<tr style="heigth:6px;">
            	<td style="width: 15%;" align="center">
                	<img src="'.getDirAtras(getcwd())."".$imagenPDFPrincipal.'"style="width:80px;">
                </td>
				<td style="width: 70%;" >
					<table style="width: 100%;" >
						<tr><td style="width: 50%; text-align:center;">COORDINACI�N DE EGRESOS Y CONTROL PATRIMONIAL</td></tr>
                        <tr><td style="width: 50%; text-align:center;"><u>SOLICITUD DE PAGO ELECTRONICO</u></td></tr>
					</table>
				</td>
				<td style="width: 15%;" align="center"><img src="'.getDirAtras(getcwd())."".$imagenPDFSecundaria.'" style="width:80px;">
                </td>
			</tr>
            <tr>
            	<td colspan="3">
                	<table style="width:100%;">
                    	<tr><td style="text-align:left; width:50%;">'.$ffecha.'</td>
                			<td style="text-align:left; width:50%;">
                            	<table style="width:100%;">
                                	<tr><td style="text-align:right;">N�mero de Transferencia</td>
                                    	<td style="text-align:center;"><b>'.$numcheque.'</b></td>
                                     </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3" style="width:100%; text-align:right; font-size:8px;">
					<div style="position:absolute; left:0px; top:150px; width:95%; ">&nbsp;<div></td>
            </tr>
			<tr>
            	<td colspan="3" style="width:100%;">
					<div style="position:absolute; left:0px; top:150px; width:95%; height:300px; border: 1pt solid black;">
                	<table style="width:100%;vertical-align:top;">
                    	<thead style="vertical-align:top;">
                    	<tr>
                        	<th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">CUENTA DE ORIGEN</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">BENEFICIARIO</th>
                            <th style="text-align:center; vertical-align:top; font-size:10px; width:100/3%">IMPORTE</th>
                        </tr>
                        </thead>
                        <tbody >
						<tr><td colspan="3"><hr></td></tr>';
						
		$command="SELECT a.folio, a.descrip, a.banco, b.nombanco, b.cuenta_ban, a.total, num_transf , c.nombre  ,d.prov, d.nomprov, b.tipoche, 
e.sol_transf, e.aut_transf
		FROM egresosmcheque a 
		INNER JOIN egresosmbancos b ON a.banco=b.banco 
		LEFT JOIN menumusuarios c ON a.usuemision=c.usuario
		INNER JOIN compramprovs d ON a.prov=d.prov
		LEFT JOIN configmconfig e ON e.estatus=0
		WHERE a.folio=".$folio."  AND a.tipo=20 AND a.estatus<9000";

			$autoriza="";
			$solicita="";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r(sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$elabora=trim($row['nombre']);
					$htmlstr.='<tr>
									<td style="width:100/3%">
										<table style="width:100%;">
											<tr>
												<td style="text-align:center; width:30%;">'.trim($row['banco']).'</td>
												<td style="text-align:justify; width:70%;">'.trim($row['cuenta_ban'])."<br>".trim($row['nombanco']).'</td>
											</tr>
										</table>
									</td>
					<td style="width:100/3%">
									<table style="width:100%;">
										<tr>
											<td style="text-align:center; width:30%">'.$row['prov'].'</td>
											<td style="text-align:justify; width:70%">'.$row['nomprov'].'</td>
										</tr>
									</table>
					</td>
					<td style="text-align:center; vertical-align:right; width:100/3%">$'.number_format($row['total'],2).'</td></tr>
					<tr><td colspan="3">'.getValesOSolCheFromCheque($folio).'</td>
										</tr>';
					
					$solicita=utf8_encode(trim($row['sol_transf']));
					$autoriza=utf8_encode(trim($row['aut_transf']));
				}
			}
			
			sqlsrv_free_stmt( $stmt);
                        $htmlstr.='</tbody>
                    </table>
					</div>
                </td>
            </tr>	
            <tr>
            	<td colspan="3"><div style="position:absolute; left:0px; top:500px; width:95%%; height:18px;  border-color:#000; border-width:medium;"><b>POR CONCEPTO</b><div>
				</td>
            </tr>
            <tr><td colspan="3">
					<div style="position:absolute; left:0px; top:518px; width:95%; height:70px; border-color:#000; border-width:medium;border: 1pt solid black;">
						<table style="width:100%; height:60px; border:#000; border-width:1px; vertical-align:top">
							<tr><td style="vertical-align:top;">'.$concepto.'</td></tr>
						</table>
					</div>
				</td>
			</tr>
            <tr>
            	<td colspan="3">
					<div style="position:absolute; left:0px; top:708px; width:95%; height:70px;">
                	<table style="width:100%;">
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
						<tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="border-bottom-width:1; border-bottom-color:#000; text-align:center; width:30%;"><hr></td>
                        </tr>
                        <tr>
                        	<td style="text-align:center; width:30%;">ELABORA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">SOLICITA</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">AUTORIZA</td>
                        </tr>
                         <tr>
                        	<td style="text-align:center; width:30%;">'.$elabora.'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.$solicita.'</td>
							<td style="text-align:center; width:5%;">&nbsp;</td>
                            <td style="text-align:center; width:30%;">'.$autoriza.'</td>
                        </tr>
                    </table>
					</div>
                </td>
            </tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
			<tr>
            	<td colspan="3" align="center" style="text-align:center;">&nbsp;</td>
			</tr>
             <tr>
            	<td colspan="3" align="center" style="text-align:center;">
					<table style="width:100%; text-align:center;" align="center"><tr>
                            <td style="border-bottom-width:1; border-bottom-color:#000; width:150px; text-align:center;"><hr></td>
                        </tr>
                        <tr>
                            <td style="text-align:center; width:150px;">APLICACI�N PRESUPUESTAL</td>
                        </tr>
                         <tr>
                            <td style="text-align:center; width:150px;">&nbsp;</td>
                        </tr>
					</table>
					
                </td>
            </tr>
		</table>
	</body>
</html> ';
	
		return $htmlstr;
	}


function getValesOSolCheFromCheque($folio)
{
	global $conexion_srv;
	$returned="<table width='400px' align='center'>
					<thead>
						<tr>
							<th width='150px'>Solitud/Factura</th>
							<th width='150px'>Importe</th>
						</tr>
					</thead>
					<tbody>";
	$command="SELECT a.numdocto, a.tipodocto, a.factura, b.descrip, 
		CASE WHEN a.tipodocto = 6 THEN
					(SELECT e.importe FROM egresosmsolche e WHERE e.folio=a.numdocto )
				ELSE
					(SELECT f.total FROM egresosmvale f WHERE f.vale=a.numdocto)  END as importe,
		CASE WHEN a.tipodocto = 6 THEN
					(SELECT e.concepto FROM egresosmsolche e WHERE e.folio=a.numdocto )
				ELSE
					(SELECT f.concepto FROM egresosmvale f WHERE f.vale=a.numdocto)  END as concepto
		FROM egresosdcheque a 
		INNER JOIN egresostipodocto b ON a.tipodocto=b.id 
		WHERE a.folio=".$folio."  AND a.estatus<9000 ORDER BY a.tipodocto";

			$autoriza="";
			$solicita="";
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r(sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					if($row['tipodocto']=='7')
						$returned.=  "<tr><td style='text-align=left;' align='left'>*Factura:".$row['factura']." de Vale: <b>".$row['numdocto']."</b></td><td align='right'><b>$".number_format($row['importe'],2)."</b></td></tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Concepto:</b>".$row['concepto']."</td></tr>";//." Factura: ".$factura;//
					else
						$returned.=  "<tr><td style='text-align=left;' align='left'>*".$row['descrip'].": <b>".$row['numdocto']."</b></td><td align='right'><b>$".number_format($row['importe'],2)."</b></td></tr><tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Concepto:</b>".$row['concepto']."</td></tr>";
				}
			}
			$returned.="</tbody></table>";
			sqlsrv_free_stmt( $stmt);
   return $returned;
}
	
?>