<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$tipo = $_REQUEST['tipo'];
//echo $tipo;
$consulta="";
//$usuario = $_REQUEST['usuario'];
$wdatos=array();
	if ($conexion)
	{		
		if($tipo=='Partida')
		{
			$consulta = "select partida as id, nompartida as name from presupmpartidas where estatus<9000 order by partida";
		}
		
		if($tipo=='Concepto')
		{
			$consulta = "select concepto as id, nomconcepto name  from presupmconceptos where estatus<9000 order by concepto";
		}
		
		if($tipo=='Rubro')
		{
			$consulta = "select rubro as id , nomrubro as name  from presupmrubros where estatus<9000 order by rubro";
		}
		
		/*if($tipo=='1')
		{
			$consulta = "select CUENTA as id , NOMCTA as name  from v_presup_admin where estatus<9000 order by CUENTA";
		}*/
		
		
		
		//echo $consulta;
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				//$wdatos[$i]['mes']= trim($row['mes']);
				$wdatos[$i]['name'] = $row['id'].".-".utf8_encode(trim($row['name']));
				$wdatos[$i]['id']= trim($row['id']);
				//$wdatos[$i]['descrip2']= trim($row['descrip']);
				$i++;
			}
		}
	}
echo json_encode($wdatos);
?>