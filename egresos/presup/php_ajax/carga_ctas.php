<?php
//header("Content-type: text/html; charset=UTF-8");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$anio = $_REQUEST['anio'];
$mes = $_REQUEST['mes'];
$cuenta = $_REQUEST['cuenta'];

$resultadoTipo = $_REQUEST['resultadoTipo'];
$tipo = $_REQUEST['tipo'];
//$usuario = $_REQUEST['usuario'];
$datos=array();
	if ($conexion)
	{
		//$consulta = "select a.cuenta,a.mes,a.aut,a.real,a.mod,a.ent_trasp,a.sal_trasp,a.com,a.dev,a.eje,a.pag,a.cob,a.cierre,";
		//$consulta .="b.nomcta from presupadmin a left join presupcuentasmayor b on a.cuenta=b.cuenta where a.anio=$anio";
		$consulta = "SELECT * FROM v_presup_admin a";
		$entro=false;
		if($mes<13)
		{
			$consulta .=" WHERE a.mes=$mes";
			$entro = true;
		}
		
		if(strlen($cuenta)>0)
		{
			if($entro)
				$consulta .=" and a.cuenta='$cuenta'";
			else
			{
				$consulta .=" WHERE a.cuenta='$cuenta'";
				$entro= true;
			}
		}
		
		if(strlen($anio)>0)
		{
			if($entro)
				$consulta .=" and a.anio='$anio'";
			else
			{
				$consulta .=" WHERE a.anio='$anio'";
				$entro= true;
			}
		}
		
		if(strlen($tipo)>0)
		{
			if($tipo=='Partida')
			{
				if($entro)
					$consulta .=" and a.partida like '%$resultadoTipo%'";
				else
				{
					$consulta .=" WHERE a.partida like '%$resultadoTipo%'";
					$entro= true;
				}
			}
			
			if($tipo=='Concepto')
			{
				if($entro)
					$consulta .=" and a.concepto like '%$resultadoTipo%'";
				else
				{
					$consulta .=" WHERE a.concepto like '%$resultadoTipo%'";
					$entro= true;
				}
			}
			
			if($tipo=='Rubro')
			{
				if($entro)
					$consulta .=" and a.rubro like '%$resultadoTipo%'";
				else
				{
					$consulta .=" WHERE a.rubro like '%$resultadoTipo%'";
					$entro= true;
				}
			}
		}
		
		
		$consulta .=" order by a.mes,a.cuenta";
		//echo $consulta;
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['cuenta']= trim($row['CUENTA']);
				$datos[$i]['nomcta']= utf8_encode(trim($row['NOMCTA']));
				$datos[$i]['partida']= trim($row['partida']);
				$datos[$i]['nompartida']= utf8_encode(trim($row['nompartida']));
				$datos[$i]['concepto']= trim($row['concepto']);
				$datos[$i]['nomconcepto']= utf8_encode(trim($row['nomconcepto']));
				$datos[$i]['rubro']= trim($row['rubro']);
				$datos[$i]['nomrubro']= utf8_encode(trim($row['nomrubro']));
				$datos[$i]['estatus']= trim($row['estatus']);
				$datos[$i]['ANIO']= trim($row['ANIO']);
				//////////////
				$mes = trim($row['MES']);
				$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
				$xmes=substr($meses,$mes*3-3,3);
				$datos[$i]['xmes']=$xmes;
				/////////////
				$datos[$i]['mes']= trim($row['MES']);
				$datos[$i]['aut']= "$".number_format(trim($row['AUT']),2);
				$datos[$i]['real']= "$".number_format(trim($row['REAL']),2);
				$datos[$i]['mod']= "$".number_format(trim($row['MOD']),2);
				$datos[$i]['ent_trasp']= "$".trim($row['ENT_TRASP']);
				$datos[$i]['sal_trasp']= "$".trim($row['SAL_TRASP']);
				$datos[$i]['com']= "$".number_format(trim($row['COM']),2);
				$datos[$i]['dev']= "$".number_format(trim($row['DEV']),2);
				$datos[$i]['eje']= "$".number_format(trim($row['EJE']),2);
				$datos[$i]['pag']= "$".number_format(trim($row['PAG']),2);
				$datos[$i]['cob']= "$".number_format(trim($row['COB']),2);
				$datos[$i]['cierre']= "$".number_format(trim($row['CIERRE']),2);
				$datos[$i]['ant']= "$".number_format(trim($row['ant']),2);
				$i++;
			}
		}
	}
echo json_encode($datos);
?>