<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$datos1=array();
$datos2=array();

if ($conexion)
{		
	$consulta1 = "select * from egresosmbancos where estatus=0 order by banco";
	$R = sqlsrv_query( $conexion,$consulta1);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos1[$i]['banco']= trim($row['banco']);
		$datos1[$i]['nombanco']= trim($row['nombanco']);
		$i++;
	}

	$consulta2 = "select * from bancosmtipomov where estatus=0 order by id";
	$R = sqlsrv_query( $conexion,$consulta2);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos2[$i]['id']= trim($row['id']);
		$datos2[$i]['corta']= trim($row['corta']);
		$i++;
	}
}

 $fecha_actual = date("d/m/Y");

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>


<script language="javascript" src="javascript/funcion_mov.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>


</head>

<body>

<table width="100%" border="0">
  <tr>
    <td><div align="center"><span class="seccionNota"><strong>Consulta de Movimientos Bancarios</strong></span></div></td>
  </tr>
</table>

<hr class="hrTitForma">
<table width="83%" border="0" align="center">
  <tr>
    <td class="texto10" width="40">Banco</td>
    <td width="69">
      <select name="bancos" id="bancos" onChange="bancos()">
          <?php
			for($i = 0 ; $i<count($datos1);$i++)
			{
				echo "<option value=".$datos1[$i]["banco"].">".$datos1[$i]["banco"]."  -    ".$datos1[$i]["nombanco"]."</option>\n";
			}
		?>
	   	
      </select>
    </td>
    <td class="texto10"  width="41">Movimiento</td>
    <td width="93"><select name="tipo" id="tipo" onChange="tipos()">
      <?php
			for($i = 0 ; $i<count($datos2);$i++)
			{
				echo "<option value=".$datos2[$i]["id"].">".$datos2[$i]["id"]."   -   ".$datos2[$i]["corta"]."</option>\n";
			}
		?>
      <option value=999>TODOS</option>
    </select></td>
    <td width="13">&nbsp;</td>
    <td>Fecha Inicial:</span>
		<input name="fi" type="text" size="7" id="fi" onFocus="nextfield ='ff';" readonly tabindex="10" value="<?php  echo $fecha_actual; ?>" class="required" maxlength="10"  style="width:70">
		<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle">
		<!--{literal}-->
		<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fi",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger1",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
		</script>
			
		  <span class="cajaPrecio">Fecha Final:</span>
		  
		 <input name="ff" type="text" size="7" id="ff" onFocus="nextfield ='noeco';" readonly tabindex="8" value="<?php  echo $fecha_actual; ?>" class="required" maxlength="10"  style="width:70">
		<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer; z-index:5" title="Date selector" align="absmiddle">
		<script type="text/javascript">
				Calendar.setup({
					inputField     :    "ff",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
		</script>
	</td>
	<td>
	  <input type="submit" name="Submit" value="Buscar" onClick="buscamovs()">
	  <span class="texto10"><span class="texto9"><span class="texto8"><strong><strong>
	  <input type="hidden" id="nbanco" name="nbanco" value="<?php echo $datos1[0]["banco"];?> ">
      <input type="hidden" id="nmov"  name="nmov" value="<?php echo $datos2[0]["id"];?> ">
      </strong></strong></span></span></span>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="377">&nbsp;</td>
    <td width="130">&nbsp;</td>
  </tr>
</table>

<table name="tabla_master" id="tabla_master" width="80%" height="19%" border=2 align="center" style="vertical-align:top; border-color: #006600">
  <td width="100%" height="171" style="vertical-align:top;">
      <table width="100%" border=1 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <td width="5%" align="center" class="subtituloverde">ID</td>
          <td width="10%" align="center" class="subtituloverde">Fecha</td>
          <td width="35%" align="center" class="subtituloverde" >Movimiento</td>
          <td width="35%" align="center" class="subtituloverde" >Concepto</td>
          <td width="10%" align="left" class="subtituloverde" >Monto</td>
           <td width="10%" align="left" class="subtituloverde" >&nbsp;</td>
           <td width="10%" align="left" class="subtituloverde" >CPI</td>
           
        </tr>
      </table>
      <div style="overflow:auto; height:350px; padding:0">
        <table name="datos" id="datos" width="100%" height="33" border=1 bordercolor="#006600" cellpadding=1 cellspacing=0 bgcolor=white>
          <?php 
		
			for($i=0;$i<count($datos);$i++)
			{
		?>
          <tr class="fila" >
            <td width="10%" align="center"><?php echo $datos[$i]['fecha'];?></td>
            <td width="35%" align="left"><?php echo $datos[$i]['nomcol'];?></td>
            <td width="35%" align="left"><?php echo $datos[$i]['ctapresup'];?></td>
            <td width="10%" align="right"><?php echo $datos[$i]['prov'];?></td>    
            <td width="10%" align="right"><?php echo $datos[$i]['prov'];?></td> 
            <td width="10%" align="right"><?php echo $datos[$i]['prov'];?></td>                        
          </tr>
          <?php 
		}
		?>
        </table>
    </div></td>
</table>


</body>
</html>
