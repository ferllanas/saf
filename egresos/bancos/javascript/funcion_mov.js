function valida_radio()
{
	if(document.getElementById('radio1').checked == true)
	{
		document.getElementById('carcre').value=1;
	}
	else
	{
		document.getElementById('carcre').value=2;
	}
}

function alta()
{
	var usuario=document.getElementById('usuario').value;
	var descri=document.getElementById('descri').value;
	var corta=document.getElementById('corta').value;
	var carcre=parseFloat(document.getElementById('carcre').value.replace(/,/g,''));
	var ctapresup=document.getElementById('ctapresup').value;
	
	if(document.getElementById('presupuestal').checked==true)
		var presup=1;
	else
		var presup=0;
		
	if(document.getElementById('clasificable').checked==true)
		var clasif=1;
	else
		var clasif=0;

	if(descri.length<1)
	{
		alert("Falta capturar descripcion");
		document.getElementById('descri').focus();
		return false;
	}
	if(corta.length<1)
	{
		alert("Falta capturar descripcion Corta");
		document.getElementById('corta').focus();
		return false;
	}
	if(ctapresup.length<1)
	{
		//alert("nada");
		var ctapresup=" ";
	}
	else
	{
		var ctapresup=document.getElementById('ctapresup').value;
	}
	
	//alert('php_ajax/alta_tipomov.php?descri='+descri+"&corta="+corta+"&carcre="+carcre+"&presup="
	//+presup+"&ctapresup="+ctapresup+"&clasif="+clasif+"&usuario="+usuario);
	location.href='php_ajax/alta_tipomov.php?descri='+descri+"&corta="+corta+"&carcre="+carcre+"&presup="
	+presup+"&ctapresup="+ctapresup+"&clasif="+clasif+"&usuario="+usuario;
	
	
}

function limpia()
{
	document.getElementById('descri').value="";
	document.getElementById('radio1').checked = false;
	document.getElementById('radio2').checked = false;
	document.getElementById('carcre').value=0;
	document.getElementById('descri').focus();
}

//////////////////////////////////////////////////////////
function busca_cuentas()
{
	//var afecta=document.getElementById('presupuestal').checked;
	if(document.getElementById('presupuestal').checked==true)
	{
		new Ajax.Request('php_ajax/carga_cuentas.php',
		 {onSuccess : function(resp) 
		 {
			if( resp.responseText ) 
			{
				//got an array of suggestions.
				//alert(resp.responseText);
				var myArray = eval(resp.responseText);
				//alert("22 "+resp.responseText);
				if(myArray.length>0)
				{
					$("cuentas").options.length=0;
					for(var i = 0; i <myArray.length; i++)
					{
						var objeto=new Option(myArray[i].cuenta+" "+myArray[i].nomcta, myArray[i].cuenta);
						document.getElementById('ctapresup').value = myArray[0].cuenta
						divstyle = document.getElementById("cuentas").style.visibility = "visible";
						$("cuentas")[i]=objeto;
					}					
				}
			}
		  }
		});
	}
	else
	{
		document.getElementById('ctapresup').value = "";
		document.getElementById('presupuestal').checked==false
		document.getElementById("cuentas").style.visibility = "hidden";
	}
}
function trae_cta()
{
	document.getElementById('ctapresup').value = document.getElementById("cuentas").options[document.getElementById("cuentas").selectedIndex].value;
}
/////////////////////////////////////////////////////////
function bancos()
{
	var banco = document.getElementById("bancos").options[document.getElementById("bancos").selectedIndex].value;
	document.getElementById('nbanco').value=banco;
}


function tipos()
{
	var tipo = document.getElementById("tipo").options[document.getElementById("tipo").selectedIndex].value;
	document.getElementById('nmov').value=tipo;
}


function buscamovs()
{
	var banco = document.getElementById('nbanco').value;
	var mov = document.getElementById('nmov').value;
	var fi =document.getElementById('fi').value;
	var ff =document.getElementById('ff').value;
	new Ajax.Request('php_ajax/consulta_movs.php?banco='+banco+"&mov="+mov+"&fi="+fi+"&ff="+ff,
					 {onSuccess : function(resp) 
					 {
							if( resp.responseText ) 
							{
								var tabla= $('datos');

								var numren=tabla.rows.length;
								for(var i = numren-1; i >=0; i--)
								{
									tabla.deleteRow(i);
								}
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									for(var i = 0; i <myArray.length; i++)
									{
										var newrow = tabla.insertRow(i);
										newrow.className ="fila";
										newrow.scope="row";
										
										var newcel1 = newrow.insertCell(0); //insert new cell to row
										newcel1.width="5%";
										newcel1.innerHTML =  myArray[i].id;
										newcel1.align ="center";
										
										var newcel2 = newrow.insertCell(1); //insert new cell to row
										newcel2.width="10%";
										newcel2.innerHTML =  myArray[i].fecha;
										newcel2.align ="center";
										
										var newcell3 = newrow.insertCell(2);
										newcell3.width="35%";
										newcell3.innerHTML = myArray[i].descrip;
										newcell3.align ="left";  
										
										var newcell4 = newrow.insertCell(3);
										newcell4.width="35%";
										newcell4.innerHTML =myArray[i].concepto;
										newcell4.align ="left"; 
										
										var newcell5 = newrow.insertCell(4);
										newcell5.width="10%";
										newcell5.innerHTML =myArray[i].monto;
										newcell5.align ="right"; 
										
										if(myArray[i].estatus==0)
										{
											var newcell6 = newrow.insertCell(5);
											newcell6.width="10%";
											newcell6.innerHTML ="<input type='image' src='../../imagenes/eliminar.jpg' id='elimina' name='elimina' Onclick='eliminamovbanc("+myArray[i].id+");'/>";
											newcell6.align ="center"; 
										}
										else
										{
											if(myArray[i].estatus==10)
											{
												var newcell6 = newrow.insertCell(5);
												newcell6.width="10%";
												newcell6.innerHTML ="Aplicado";
												newcell6.align ="center"; 
											}
											else
											{
												var newcell6 = newrow.insertCell(5);
												newcell6.width="10%";
												newcell6.innerHTML ="Eliminado";
												newcell6.align ="center"; 
											}											
										}
										if(myArray[i].codifica_presup==0)
										{
											var newcell7 = newrow.insertCell(6);
											newcell7.width="10%";
											newcell7.innerHTML ="NO";
											newcell7.align ="center"; 
										}
										else
										{
											var newcell7 = newrow.insertCell(6);
											newcell7.width="10%";
											newcell7.innerHTML ="SI";
											newcell7.align ="center"; 
										}																					
									}
								}
								else
								{
									alert("No existen resultados para estos criterios.");
								}
						}
					  }
				});
	
}
function eliminamovbanc(id,concepto)
{
	//alert(id);
	var pregunta = confirm("Esta seguro que desea eliminar el Movimineto por concepto de "+id)
	if (pregunta)
	{
		location.href="php_ajax/cancela_mov.php?mov="+id;
	}
}