<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
		
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$banco = trim($_REQUEST['banco']);
$mov = $_REQUEST['mov'];

$fi = trim($_REQUEST['fi']);
$ff = trim($_REQUEST['ff']);

		list($dd,$mm,$aa)= explode('/',$fi);
		$fecini=$aa.'-'.$mm.'-'.$dd;
				
		list($dd2,$mm2,$aa2)= explode('/',$ff);
		$fecfin=$aa2.'-'.$mm2.'-'.$dd2;


$datos=array();

if ($conexion)
{		
	$consulta = "select a.id,CONVERT(varchar(12),a.fecha, 103) as fecha,b.descrip,a.concepto,a.monto,a.estatus,a.codifica_presup from bancosdmovs a ";
	$consulta .= "left join bancosmtipomov b on a.tipomov=b.id where a.banco='$banco' and ";
	$consulta .="a.fecha >= CONVERT(varchar(12),'$fecini', 103) and a.fecha <=CONVERT(varchar(12),'$fecfin', 103) ";
	if($mov<999)
		 $consulta .= "and b.id=$mov";
	//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']= trim($row['id']);
			$datos[$i]['fecha']= trim($row['fecha']);
			$datos[$i]['descrip']= trim($row['descrip']);
			$datos[$i]['concepto']= trim($row['concepto']);
			$datos[$i]['monto']= number_format(trim($row['monto']),2);
			$datos[$i]['estatus']= trim($row['estatus']);
			$datos[$i]['codifica_presup']= trim($row['codifica_presup']);
			$i++;
		}
	}
}
echo json_encode($datos);
?>