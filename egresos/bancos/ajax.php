<?php
// Archivo de consultas de Solicitud de Cheques
include_once 'lib/ez_sql_core.php'; 
//include_once 'lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$query=$_REQUEST['query'];
$fecini=substr($_REQUEST['query'],1,10);
$fecfin=substr($_REQUEST['query'],11,11);

$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
		$command= "select id,descrip,corta,entsal,presup,ctapresup,clasifica,CONVERT(varchar(12),falta, 103) as fecha,estatus from bancosmtipomov 
		WHERE descrip LIKE '%" . substr($_REQUEST['query'],1) . "%' and estatus<9000 order by descrip";
	}
	if($opcion==2)
	{
		$command= "select id,descrip,corta,entsal,presup,ctapresup,clasifica,CONVERT(varchar(12),falta, 103) as fecha,estatus from bancosmtipomov 
		WHERE ctapresup LIKE '%" . substr($_REQUEST['query'],1) . "%' and estatus<9000 order by ctapresup";
	}
	
	if($opcion==3)
 	{			

		list($dd,$mm,$aa)= explode('/',$fecini);
		$fecini=$aa.'-'.$mm.'-'.$dd;
				
		list($dd2,$mm2,$aa2)= explode('/',$fecfin);
		$fecfin=$aa2.'-'.$mm2.'-'.$dd2;
				
		$command= "select id,descrip,corta,entsal,presup,ctapresup,clasifica,CONVERT(varchar(12),falta, 103) as fecha,estatus from bancosmtipomov 
				   where falta >= CONVERT(varchar(12),'$fecini', 103) and falta <=CONVERT(varchar(12),'$fecfin', 103) and estatus<9000 order by falta ";
	}
			//echo $command;
		$stmt2 = sqlsrv_query( $conexion,$command);
		$i=0;
		while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']=$row['id'];
			$datos[$i]['descrip']=utf8_decode($row['descrip']);
			$datos[$i]['corta']=utf8_decode($row['corta']);
			$datos[$i]['entsal']=$row['entsal'];
			$datos[$i]['presup']=$row['presup'];
			$datos[$i]['ctapresup']=$row['ctapresup'];
			$datos[$i]['clasifica']=$row['clasifica'];
			$datos[$i]['fecha']=$row['fecha'];
			$datos[$i]['estatus']=$row['estatus'];
			$i++;
		}

echo json_encode($datos);  
?>