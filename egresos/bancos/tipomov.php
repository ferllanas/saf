<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		require_once("../../connections/dbconexion.php");

		$usuario=$_COOKIE['ID_my_site'];
		//$usuario='001349';
		$carcre=1;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">

<script language="javascript" src="javascript/funcion_mov.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>
<p>&nbsp;</p>

<table width="722" height="319" border="0" align="center" style=" ">
  <tr>
    <td colspan="4" class="subtituloverde12">Alta de Tipo de Movimientos </td>
  </tr>
  <tr>
    <td width="91"><div align="right"><span class="texto10">Descripcion</span></div></td>
    <td colspan="2"><input type="text" name="descri" id="descri" size="60"></td>
    <td width="246"><input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
      <input type="hidden" name="carcre" id="carcre" value="<?php echo $carcre;?>">
      <input type="hidden" name="ctapresup" id="ctapresup"></td>
  </tr>
  <tr>
    <td class="texto10"><div align="right">Desc. Corta</div></td>
    <td width="190"><input name="corta" type="text" id="corta" size="10" maxlength="6"></td>
    <td width="177">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto10"><div align="right">Tipo</div></td>
    <td><input checked="true" name="radio" id="radio1" type="radio" onClick="valida_radio()" value="1">
      <span class="texto9">     Entrada</span>      <input name="radio" id="radio2" type="radio" onClick="valida_radio()" value="2">
     <span class="texto9">     Salida</span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto10"><div align="right">Presupuestal</div></td>
    <td>
      <input type="checkbox" name="presupuestal" id="presupuestal" onClick="busca_cuentas()"></td>
    <td><select name="cuentas" id="cuentas" onChange="trae_cta()" style="visibility:hidden ">
    </select></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
     <td class="texto10"><div align="right">Clasificable</div></td>
    <td><input type="checkbox" name="clasificable" id="clasificable"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="button" name="guardar" id="guardar" value="Guardar" onClick="alta()"></td>
    <td>&nbsp;</td>
    <td><input type="button" name="cancelar" id="cancelar" value="Deshacer"onClick="limpia()"></td>
  </tr>
</table>

<p>&nbsp;</p>
</body>

</html>
