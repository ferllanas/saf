<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
////
require_once("../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);

//$EditarPriv =revisaPrivilegioseditar($privilegios);
$BorrarPriv =revisaPrivilegiosBorrar($privilegios);
$verPDFPriv =revisaPrivilegiosPDF($privilegios);
////
?>

<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Egresos</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="javascript/divhide.js"></script>
        <script src="javascript/jquery.tmpl.js"></script>
		
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);

						$.post('ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			function cancela_benef(folio)
			{
			
				var pregunta = confirm("Esta seguro que desea Eliminar")
				if (pregunta)
				{
					location.href="php_ajax/cancela_fracc_benef.php?folio="+folio;
				}
			}
			function actualiza_benef(folio)
			{
			
				//alert(folio);
				var usuario=document.getElementById('usuario').value;
				var pregunta = confirm("Esta seguro que desea Actualizar")
				if (pregunta)
				{
					location.href="php_ajax/actualiza_fracc_benef.php?folio="+folio+"&usuario="+usuario;
				}
			}

			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if folio}}
					<td align="center">${folio}</td>
					<td align="center">${anio}</td>
					<td align="center">${mes}</td>
					<td align="center">${fecha}</td>
					{{if estatus==0}}
						<td width="7%" align="center"><img src="../../imagenes/actualiza1.png" width="21" height="24" onClick="actualiza_benef(${folio})"></td>
						<td width="7%" align="center"><?php if($verPDFPriv){?><img src="../../imagenes/consultar.jpg" onClick="window.open('pdf_files/detalle_benef_'+${folio}+'.pdf') "><?php } ?></td>
						<td width="7%" align="center"><?php if($BorrarPriv){?><img src="../../imagenes/eliminar.jpg" onClick="cancela_benef(${folio})"><?php } ?></td>
					{{/if}}
					{{if estatus==10}}
						<td align="center">Con Orden pago</td>
					{{/if}}
					{{if estatus==9000}}
						<td align="center"><img src="../imagenes/borrar.jpg"></td>
					{{/if}}

                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   

<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Generaci&oacute;n de Solicitudes de Cheque de Fraccionamientos en Administraci&oacute;n</span>    
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%"><select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Solicitud de Cheque</option>
              <option value="2" >Año</option>
              <option value="3" >Mes</option>
              <option value="4" >Por rango de fechas</option>
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:

<input type="text"  name="fecini" id="fecini" size="10">
	<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
		<script type="text/javascript">
			Calendar.setup({
				inputField     :    "fecini",		// id of the input field
				ifFormat       :    "%d/%m/%Y",		// format of the input field
				button         :    "f_trigger1",	// trigger for the calendar (button ID)
				//onClose        :    fecha_cambio,
				singleClick    :    true
			});
		</script>
        

        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger2",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
		<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
<input type="hidden" name="ren" id="ren">
<input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario; ?>">
</td>
</tr>
</table>
<table>
		<thead>
	<th>Folio</th>
			<th>Año</th>
			<th>Mes</th>
			<th>fecha</th>
			<th>Actualiza</th>
			<th>Consulta</th>
			<th>Cancelacíon</th>
		</thead>
			<tbody id="proveedor" name='proveedor'>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">Encontrar Resultados</td>
				</tr>
			</tbody>
</table>
    </div>
    </body>
</html>
