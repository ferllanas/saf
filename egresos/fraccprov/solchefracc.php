<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");


$nombre="";
$fracc="";
$nomcol="";
$prov=0;
$nomprov="";	
$numprov=0;	
$totald=0;
$total=0;
$usuario = $_COOKIE['ID_my_site'];
$depto = $_COOKIE['depto'];
$depto = (int)$depto;
//$usuario="001349";
//$depto=1232;
	
 $wdatos=array();	
 $anio = date("Y");//"2013";//
 $mes = date("m");//"12";//
if($mes==1)
{
	$mes=12;
	$anio = $anio - 1;
}
else
{
	$mes=$mes-1;
} 			
			
//			$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
//			$xmes=substr($meses,$mes*10-10,10);


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/busquedaincProv.js"></script>
<script language="javascript" src="javascript/solche_fracc.js"></script>

</head>

<body>
<p>&nbsp;</p>
<p class="seccionNota"><strong>Pre-Solicitud de Beneficiarios de Fracc. por Administraci&oacute;n </strong></p>
<hr class="hrTitForma">

<table width="706" border="0">
  <tr>
    <td width="47" class="texto10">A&ntilde;o</td>
    <td width="164"><input type="text" name="anio" id="anio" size="4px" readonly value="<?php echo $anio;?>" ></td>
    <td width="39" class="texto10">Mes</td>
    <td width="348"><input type="text" name="mes" id="mes" size="4px" readonly value="<?php echo $mes;?>" ></td>
    <td width="17">&nbsp;</td>
    <td width="51">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<table width="90%" border="0">
  <tr>
    <td width="46" class="texto10"><span class="texto10"><span class="texto9"><span class="texto8"><strong>
      <input class="texto8" type="hidden" id="numfracc" name="numfracc" size="8">
    </strong></span></span></span><span class="texto10"><span class="texto9"><span class="texto8"><strong>
    <input class="texto8" type="hidden" id="numprov" name="numprov" size="8">
    <input class="texto8" type="hidden" id="nomfracc" name="nomfracc" size="8"> 
	<input type="hidden" name="ren" id="ren"> 
	<input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>" >  
	<input type="hidden" name="depto" id="depto" size="4" value="<?php echo $depto;?>"> 
    <input type="hidden" name="nomprovi" id="nomprovi">
	<input type="hidden" name="numcuenta" id="numcuenta">  
    Fracc</td>
    <td width="332">
		<div align="left" style="z-index:4; position:relative; width:332px; height: 24px;" class="texto10">
		  <input type="text" class="texto9" name="fracc" id="fracc" tabindex="1" onkeyup="buscafracc2(this);" style="width:330px;" value="<?php echo $fracc;?>" autocomplete="off">
		  <div class="texto8" id="search_suggestfracc2" style="z-index:3;" > </div>
	</div>	</td>
    <td width="25" class="texto10">Prov</td>
    <td width="360" ><input class="texto10" type="text" name="nomprov" id="nomprov" readonly size="60px"></td>
    <td width="36" class="texto10">Monto</td>
    <td width="90"><input class="texto10" type="text" name="monto" id="monto" size="15px" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" style="text-align:right "></td>
    <td width="81"><span class="texto9"><img src="../../imagenes/agregar2.jpg" id="agrega_ren" width="30" height="25" title="Agregar Fracc." onClick="agregar_fracc()"><img src="../../imagenes/actualiza3.png" id="act_ren" width="30" height="25" style="visibility:hidden; " title="Actualiza Datos de Fraccionamiento." onClick="act()"></span></td>
  </tr>
  <tr>
    <td><span class="texto10"><span class="texto9"><span class="texto8"><strong>
    </strong></span></span></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<table name="enc" id="enc" width="100%" height="10%" align="left" border="1">
  <tr>
    <th width="268" align="center" class="subtituloverde">Fracc</th>
    <th width="417" align="center" class="subtituloverde">Proveedor</th>
    <th width="79" align="center" class="subtituloverde">Monto</th>
    <th width="74" align="center" class="subtituloverde">Acci&oacute;n</th>
  </tr>
</table>
<div style="overflow:auto; width: 100%; height :300px; align:center;">
  <table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
    <thead>
    </thead>
    <tbody id="datos" name="datos">
    </tbody>
  </table>
</div>

<table width="997" border="0" align="left">
    <tr>
      <td width="582" class="texto10">&nbsp;</td>
      <td width="186">&nbsp;</td>
      <td width="56"><span class="texto10">Suma</span></td>
      <td width="155"><input type="text" name="totald" id="totald" readonly style="text-align:right " value="<?php echo $totald;?>"></td>
    </tr>
    <tr>
      <td>
        <input type="button" name="Grabar" value="Grabar" id="Grabar"  onClick="guarda_datos()">
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
</table>


</body>

</html>
