<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
////
require_once("../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);

//$EditarPriv =revisaPrivilegioseditar($privilegios);
$BorrarPriv =revisaPrivilegiosBorrar($privilegios);
//$verPDFPriv =revisaPrivilegiosPDF($privilegios);
////

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$sw = 0;
$datos = array();
$nombre="";
$fracc="";
$numcuenta="";
$cuenta="";
$nomcol="";
$prov=0;
$nomprov="";

$usuario = $_COOKIE['ID_my_site'];
//$usuario="001349";


if(isset($_REQUEST['sw']))
	$sw = $_REQUEST['sw'];

if(isset($_REQUEST['fracc']))
	$fracc = $_REQUEST['fracc'].' - '. $_REQUEST['nomcol'];
	
if(isset($_REQUEST['nomcol']))
	$nomcol = $_REQUEST['nomcol'];

if(isset($_REQUEST['prov']))
	$prov = $_REQUEST['prov'];
	
if(isset($_REQUEST['nomprov']))
	$nombre = $_REQUEST['prov'].' - '.$_REQUEST['nomprov'];
	
if(isset($_REQUEST['numcuenta']))
	$numcuenta = $_REQUEST['numcuenta'];	


if ($conexion)
{
	$consulta="select a.fracc,b.nomcol,a.ctapresup,a.prov,c.nomprov from egresosmfracc_benef a ";
	$consulta.="left join fomedbe.dbo.tecnicmfracc b on a.fracc=b.fracc left join compramprovs c ";
	$consulta.="on a.prov=c.prov where a.estatus<9000 and (b.cvesit < '21049000') order by a.fracc,a.prov";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['fracc']= trim($row['fracc']);
		$datos[$i]['nomcol']= trim($row['nomcol']);
		$datos[$i]['ctapresup']= trim($row['ctapresup']);
		$datos[$i]['prov']= trim($row['prov']);
		$datos[$i]['nomprov']= trim($row['nomprov']);

		$i++;
	}
}	
	

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Contabilidad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/busquedaincProv.js"></script>
</head>

<body>

<p class="seccionNota"><strong>Asignaci&oacute;n de Beneficiarios  de Fracc. por Administraci&oacute;n </strong></p>
  <hr class="hrTitForma">
<table align="center" width="60%" border="0">
  <tr>
    <td width="25" class="texto10"><span class="texto9"><span class="texto8"><strong>
      <input class="texto8" type="hidden" id="numfracc" name="numfracc" size="8">
    </strong></span></span></td>
    <td width="67" align="right" class="texto10">Fracc.</td>
    <td width="501"><div align="left" style="z-index:6; position:relative; width:500px; height: 24px;">
      <input type="text" class="texto8" name="fracc" id="fracc" tabindex="1" onkeyup="buscafracc(this);" style="width:90%;" value="<?php echo $fracc;?>" autocomplete="off">
      <div class="texto8" id="search_suggestfracc" style="z-index:5;" > </div>
    </div></td>
    <td width="22" class="texto10"><span class="texto9"><span class="texto8"><strong>
    </strong></span></span></td>
    <td width="107">
    	<span class="texto10"><span class="texto9"><span class="texto8"><strong>    	<strong>        </strong>
   	</strong></span></span></span>	</td>
  </tr>
  <tr>
    <td class="texto10"><span class="texto9"><span class="texto8"><strong>
      <input class="texto8" type="hidden" id="numprov" name="numprov" size="8">
    </strong></span></span></td>
    <td class="texto10" align="right">Beneficiario</td>
    <td>
	<div align="left" style="z-index:4; position:relative; width:500px ;height: 24px;">
    		<input class="texto8" type="text" id="provname1" name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="2" value="<?php echo $nombre;?>">        
			<div id="search_suggestProv" style="z-index:3;" ></div>
	</div>
	</td>
    <td class="texto10">&nbsp;</td>
    <td><span class="texto10"><span class="texto9"><span class="texto8"><strong>      <strong>
      <input class="texto8" type="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>" size="8">
	  <input type="hidden" name="ren" id="ren">
    </strong></strong></span></span></span></td>
  </tr>
  <tr>
    <td class="texto10"><span class="texto9"><span class="texto8"><strong>
      <input class="texto8" type="hidden" id="numcuenta" name="numcuenta" value="<?php echo $numcuenta; ?>" size="8">
    </strong></span></span></td>
    <td class="texto10" align="right">Cuenta</td>
    <td><div align="left" style="z-index:1; position:relative; width:500px; height: 24px;">
      <input type="text" class="texto8" name="cuenta" id="cuenta" tabindex="1" onkeyup="buscacuenta(this);" style="width:90%;" value="<?php echo $cuenta;?>" autocomplete="off">
      <div class="texto8" id="search_suggestcuenta" style="z-index:2;" > </div>
    </div></td>
    <td class="texto10">&nbsp;</td>
    <td><span class="texto10"><span class="texto9"><span class="texto8"><strong>
      <input type="button" name="guardar" id="guardar" onClick="agregar()" value="Actualizar">
    </strong></span></span></span></td>
  </tr>
</table>

 <table width="60%" height="19%" border=2 align="center" style="vertical-align:top; border-color: #006600">
  <td width="90%" height="171" style="vertical-align:top;">
      <table width="100%" border=0 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <td width="37" align="center" class="subtituloverde">Fracc</td>
          <td width="123" align="center" class="subtituloverde" >Nombre Fracc.</td>
          <td width="74" align="center" class="subtituloverde" >Cuenta</td>
          <td width="72" align="left" class="subtituloverde" >No.</td>
          <td width="133" align="center" class="subtituloverde" >Nombre Beneficiario </td>
          <td width="22" align="center" class="subtituloverde" >&nbsp;</td>
        </tr>
      </table>
      <div style="overflow:auto; height:350px; padding:0">
        <table name="Tabla" id="Tabla" width="100%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white>
          <?php 
		
			for($i=0;$i<count($datos);$i++)
			{
		?>
          <tr class="fila" >
		    <td width="29" align="center"><?php echo $datos[$i]['fracc'];?>
			<input name="fraccn" type="hidden" value="<?php echo $datos[$i]['fracc'];?>" >
			</td>
            <td width="146" align="left"><?php echo $datos[$i]['nomcol'];?></td>
            <td width="79" align="left"><?php echo $datos[$i]['ctapresup'];?></td>
            <td width="45" align="left"><?php echo $datos[$i]['prov'];?></td>
            <td width="119" align="left"><?php echo $datos[$i]['nomprov'];?></td>
            <td width="31" align="center"><?php if($BorrarPriv){ ?><img src="../../imagenes/eliminar.jpg" width="26" height="26" onClick="borrar(this,<?php echo (int)$datos[$i]['fracc'];?>,<?php echo $datos[$i]['prov'];?>)"><?php } ?></td>
          </tr>
          <?php 
		}
		?>
        </table>
    </div></td>
</table>
<p>&nbsp;</p>
</body>
</html>
