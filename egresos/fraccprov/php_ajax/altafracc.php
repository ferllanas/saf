<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$fracc = trim($_REQUEST['fracc']);
$prov = $_REQUEST['prov'];
$numcuenta = $_REQUEST['numcuenta'];

$usuario = $_REQUEST['usuario'];
$error="";
$consulta = "select fracc,prov from egresosmfracc_benef where fracc='$fracc' and prov=$prov and estatus<9000";
//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['fracc']=trim($row['fracc']);
			$datos[$i]['prov']=trim($row['prov']);
			$i++;
		}
		if($i==0)
		{
			$ejecuta ="{call sp_egresos_A_mfracc_benef(?,?,?,?)}";
			$variables = array(&$fracc,&$prov,&$usuario,&$numcuenta);
			$R = sqlsrv_query($conexion, $ejecuta, $variables);
			if( $R === false )
			{
				 $fails= "Error in (detalle) statement execution.\n";
				 $fails=true;
				 print_r($variables);
				 die( print_r( sqlsrv_errors(), true));
			}
			else
			{
				$error="Alta Realizada Correctamente";
			}
		}
		else
		{
			$error="Proveedor ya existe para este Fraccionamiento";
		}
		echo json_encode(array($fracc,$error));
	}

?>