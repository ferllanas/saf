<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

$datos = array();

$nombre="";
$fracc="";
$nomcol="";
$prov=0;
$nomprov="";	
$numprov=0;	
$totald=0;
$total=0;
$usuario='001349';
	


if ($conexion)
{
	$consulta= "select a.folio,a.a�o as anio,a.mes,b.fracc,b.prov,b.importe,c.nomcol,d.nomprov from ";
	$consulta.= "egresosmsolchefracc_previo a left join egresosdsolchefracc_previo b on ";
	$consulta.= "a.folio=b.folio left join v_fomedbe_tecnic_C_mfracc c on b.fracc=c.fracc ";
	$consulta.= "left join compramprovs d on b.prov=d.prov where a.folio=2 and a.estatus<9000 ";
	$consulta.= "and b.estatus<9000 and d.estatus<9000";
	$R = sqlsrv_query($conexion,$consulta);
	$i=0;
	
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['folio']= $row['folio'];
		$datos[$i]['anio']= $row['anio'];
		$datos[$i]['mes']= $row['mes'];
		$datos[$i]['fracc']= $row['fracc'];
		$datos[$i]['prov']= $row['prov'];
		$datos[$i]['importe']= $row['importe'];
		$datos[$i]['nomcol']= $row['nomcol'];
		$datos[$i]['nomprov']= $row['nomprov'];
		$mes=$datos[$i]['mes'];
		$i++;
	}
	$i=0;
	$consulta2="select * from configmconfig where estatus<9000";
	$R = sqlsrv_query($conexion,$consulta2);
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$coord_ingresos = $row['coord_ingresos'];
		$dir_finanzas = $row['dir_finanzas'];
		$i++;
	}
	
}	

	$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
	$xmes=substr($meses,$mes*10-10,10);

 $i=0;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../../javascript_globalfunc/funcionesGlobales.js"></script>


</head>

<body>
<p>&nbsp;</p>
<div style="overflow:auto; width: 100%; height :50px; align:center;">
  <table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <?php 
		for($i=0;$i<count($datos);$i++)
		{
	?>
     <tr>
       <th colspan="4" align="center" class="texto10" >
	   <table width="99%" border="0">
         <tr>
           <td width="27%" align="left"><img src='../../../<?php echo $imagenPDFPrincipal;?>' width='174' height='59'></td>
           <td width="68%" align="center" valign="bottom"><?php echo $xmes;?><?php echo $datos[$i]['anio'];?></td>
           <td width="5%" align="right"><img src='../../../<?php echo $imagenPDFSecundaria;?>' width='137' height='72'></td>
         </tr>
       </table></th>
     </tr>
     <tr>
       <th class="texto10" align="center" >&nbsp;</th>
       <th class="texto10" align="left" ><div align="center">COBRANZA DE FRACCIONAMIENTOS EN REGULARIZACION</div></th>
       <th class="texto10" align="left" >&nbsp;</th>
       <th class="texto10" align="right" >&nbsp;</th>
     </tr>
     <tr>
       <th class="texto10" align="center" >No. Fracc</th>
       <th class="texto10" align="left" ><div align="center">Fraccionamiento</div></th>
       <th class="texto10" align="left" ><div align="center">Beneficiario</div></th>
       <th class="texto10" align="right" >Monto</th>
     </tr>
    <tr>
		<td class="texto10" width="80" align="center" ><?php echo $datos[$i]['fracc'];?></td>
	  <td class="texto10" width="435" align="left" ><?php echo $datos[$i]['nomcol'];?></td>
		<td class="texto10" width="392" align="left" ><?php echo $datos[$i]['nomprov'];?></td>
		<td class="texto10" width="82" align="right" ><?php echo number_format($datos[$i]['importe'],2);?></td>
    </tr>
 	<?php
		$total=$total+$datos[$i]['importe'];
		}
	?>
  </table>
</div>

<table width="100%" border="0" align="left">
    <tr>
      <td width="69" height="41">&nbsp;
      </td>
      <td width="379">&nbsp;</td>
      <th width="346"><div align="right"><span class="texto10">Total:</span></div></th>
      <th width="82" align="right" class="texto10_sub"><?php echo number_format($total,2);?></th>
    </tr>
</table>


<p>&nbsp;</p>
<table width="100%" border="0">
  <tr>
    <td><div align="center">______________________________________</div></td>
    <td><div align="center">______________________________________</div></td>
  </tr>
  <tr>
    <th class="texto10"><div align="center">ELABORO</div></th>
    <th class="texto10"><div align="center">AUTORIZ&Oacute;</div></th>
  </tr>
  <tr>
    <td class="texto10"><div align="center"><?php echo $coord_ingresos;?></div></td>
    <td class="texto10"><div align="center"><?php echo $dir_finanzas;?></div></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>

</html>