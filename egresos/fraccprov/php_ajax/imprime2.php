<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once('../../../connections/dbconexion.php');

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	require_once("../../../dompdf/dompdf_config.inc.php");
	include('../../../Administracion/globalfuncions.php');

$datos = array();

$nombre='';
$fracc='';
$nomcol='';
$prov=0;
$nomprov='';	
$numprov=0;	
$totald=0;
$total=0;
$usuario='001349';
$folio=3;	
$pageheadfoot='';

if ($conexion)
{
	$consulta= "select a.folio,a.a�o as anio,a.mes,b.fracc,b.prov,b.importe,c.nomcol,d.nomprov from ";
	$consulta.= "egresosmsolchefracc_previo a left join egresosdsolchefracc_previo b on ";
	$consulta.= "a.folio=b.folio left join v_fomedbe_tecnic_C_mfracc c on b.fracc=c.fracc ";
	$consulta.= "left join compramprovs d on b.prov=d.prov where a.folio=$folio and a.estatus<9000 ";
	$consulta.= "and b.estatus<9000 and d.estatus<9000";
	$R = sqlsrv_query($conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['folio']= $row['folio'];
		$datos[$i]['anio']= $row['anio'];
		$datos[$i]['mes']= $row['mes'];
		$datos[$i]['fracc']= $row['fracc'];
		$datos[$i]['prov']= $row['prov'];
		$datos[$i]['importe']= $row['importe'];
		$datos[$i]['nomcol']= $row['nomcol'];
		$datos[$i]['nomprov']= $row['nomprov'];
		$mes=$datos[$i]['mes'];
		$i++;
	}
	$i=0;
	$consulta2="select * from configmconfig where estatus<9000";
	$R = sqlsrv_query($conexion,$consulta2);
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$coord_ingresos = $row['coord_ingresos'];
		$dir_finanzas = $row['dir_finanzas'];
		$i++;
	}
	
	$i=0;
}	

	$meses='ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ';
	$xmes=substr($meses,$mes*10-10,10);

 $i=0;
$dompdf = new DOMPDF();
	$pageheadfoot='<script type="text/php"> 
	if ( isset($pdf) ) 
	{ 	$textod=utf8_encode("P�gina {PAGE_NUM}");
		$textop=utf8_encode("Asignacion de Beneficiarios x Fraccc.");
		$pdf->page_text(25, 760, "$textop ", "", 12, array(0,0,0)); 
		$pdf->page_text(550, 760, "Pagina {PAGE_NUM}", "", 12, array(0,0,0)); 
		
	 } 

</script> ';  
$html="
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'
'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-UTF-8'>
<link href='../../../css/estilos.css' rel='stylesheet' type='text/css'>

<script language='javascript' src='../../../prototype/jQuery.js'></script>
<script language='javascript' src='../../../javascript_globalfunc/funciones_AHojaDEstilos.js'></script>
<script language='javascript' src='../../../javascript_globalfunc/funcionesGlobales.js'></script>


</head>

<body>
<p>&nbsp;</p>

<table width='30%' border='0'>
  <tr>
    <td colspan='2' align='left' class='texto10'><img src='../../../$imagenPDFPrincipal' width='200' height='76'></td>
    <td width='39' class='texto10'>&nbsp;</td>
    <th width='240' align='center' valign='bottom' class='texto12'>".$xmes."  ".$datos[$i]['anio']."</th>
    <td colspan'2' align='right'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101'></td>
  </tr>
  <tr>
    <td width='47' class='texto10'>&nbsp;</td>
    <td width='164' class='texto10'>&nbsp;</td>
    <td class='texto10'>&nbsp;</td>
    <td class='texto10'>&nbsp;</td>
    <td width='17'>&nbsp;</td>
    <td width='51'>&nbsp;</td>
  </tr>
  <tr>
    <td class='texto10'>&nbsp;</td>
    <th colspan='3' class='texto10'>COBRANZA DE FRACCIONAMIENTOS EN REGULARIZACION </th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


  <table name='master' id='master' width='95%' height='10%' border=0 align='center' >
  <thead>
	  <tr>
		<th width='8%' align='center' class='texto10'>No. Fracc </th>
		<th width='40%' align='center' class=''texto10'>Fraccionamiento</th>
		<th width='30%' align='center' class=''texto10'>Propietario</th>
		<th width='12%' align='center' class=''texto10'>Monto</th>
	  </tr>
	</thead>  
	    
 ";

		for($i=0;$i<count($datos);$i++)
		{
  $html.="


    <tr>
		<td class='texto8' width='8%' align='left' >".$datos[$i]['fracc']."</td>
	  <td class='texto8' width='40%' align='left' >".$datos[$i]['nomcol']."</td>
		<td class='texto8' width='30%' align='left' >".$datos[$i]['nomprov']."</td>
		<td class='texto8' width='12%' align='right' >".number_format($datos[$i]['importe'],2)."</td>
    </tr>
	";
		$total=$total+$datos[$i]['importe'];
		}
  $html.="
  </table>
  
<table width='95%' border='0' align='center'>
    <tr>
      <td width='8%' height='41'>&nbsp;</td>
      <td width='40%'>&nbsp;</td>
      <th width='30%'><div align='right'><span class='texto10'>Total:</span></div></th>
      <th class='texto8' width='12%' align='right'>".number_format($total,2)."</th>
    </tr>
</table>

<p>&nbsp;</p>
<table width='100%' border='0'>
  <tr>
    <td><div align='center'>______________________________________</div></td>
    <td><div align='center'>______________________________________</div></td>
  </tr>
  <tr>
    <th class='texto8'><div align='center'>ELABORO</div></th>
    <th class='texto8'><div align='center'>AUTORIZ&Oacute;</div></th>
  </tr>
  <tr>
    <td class='texto8'><div align='center'>".$coord_ingresos."</div></td>
    <td class='texto8'><div align='center'>".$dir_finanzas."</div></td>
  </tr>
</table>
".$pageheadfoot.";
</body>



</html>";
  
		
  	$dompdf->set_paper('"letter","portrait"');
	
	$dompdf->load_html($html);
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	//echo $folio;
	file_put_contents("../pdf_files/detalle_benef_".$folio.".pdf", $pdf);

?>