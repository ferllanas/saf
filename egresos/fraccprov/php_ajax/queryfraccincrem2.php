<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
//$depto = $_COOKIE['depto'];
//$privsolche = $_COOKIE['privsolche'];

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	//$termino=$_GET['q'];
//	$command= "SELECT a.prov, a.oldprov, a.nomprov, b.tipo FROM compramprovs a INNER JOIN compradprovs b on a.prov=b.prov ";

	$command = "select a.fracc,b.nomcol,a.prov,c.nomprov from egresosmfracc_benef a ";
	$command.= "left join v_fomedbe_tecnic_C_mfracc b on a.fracc=b.fracc left join compramprovs c ";
	$command.= "on a.prov=c.prov where ";
	
		if (count($terminos)==1)
		{
			if (is_numeric($terminos[0]))
				$command .="a.fracc like '%".$terminos[0]."%'";
			  else
				$command .="b.nomcol LIKE '%".$terminos[0]."%' ";
		}
		   else
		   {
				$paso=false;
				for($i=0;$i<count($terminos);$i++)
				{
					if(!$paso)
					{
						$command .="b.nomcol LIKE '%".$terminos[0]."%' ";
						$paso=true;	
					}
					else
						$command.="a.fracc LIKE '%".$terminos[$i]."%'";	
				}
			}
			//$command.=" and fracc not in (select fracc from egresosmfracc_benef where estatus<9000) ";
			$command.= " and a.estatus<9000 and c.estatus<9000 ORDER BY b.nomcol ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		//$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$sct.= htmlentities(trim(utf8_encode($row['fracc'])))."@".trim(utf8_encode($row['nomcol']))."@".trim(utf8_encode($row['prov']))."@".trim(utf8_encode($row['nomprov']))."\n";
			//$i++;
		}
	}
	echo $sct;
}
?>