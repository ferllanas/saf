<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$ffecha=date("Y-m-d");
	$tipo="";
	$idProveedor="";
	$conceptos="";
	$concepto="";
	$fechaemision="";
	$msgerror="";
	$fails=false;
	$hora="";
	$id="";
	$facturas="";//variable que almacena la cadena de los facturas
	$subtotal=0.00;
	$iva=0.00;
	$total=0.00;
	$numbanco="";
	$id_facts="";
	if(isset($_POST['prods']))
			$facturas=$_POST['prods'];
	
	//print_r($facturas);
	if(isset($_POST['tipo']))
		$tipo=$_POST['tipo'];

	if(isset($_POST['idProveedor']))
		$idProveedor=$_POST['idProveedor'];

	if(isset($_POST['fechaemision']))
		$fechaemision=$_POST['fechaemision'];
		
	$fechaemision = convertirFechaEuropeoAAmericano($fechaemision);

	if(isset($_POST['numbanco']))
			$numbanco=$_POST['numbanco'];

	if(isset($_POST['concepto']))
		$concepto = $_POST['concepto'];
		
	if(isset($usuario))
	{
		if ( is_array( $facturas ) ) // Pregunta se es una array la variable facturas
		{

			if($tipo=="true" )//imprime todos|| tipo==true
			{
				//Un Solo cheque parab todas las facturas
				//suma los totales de cada una de las partidas
				for( $i=0 ; $i  < count($facturas) ; $i++ )
				{
					if($tipo=="true" )//imprime todos|| tipo==true
					{
						if($facturas[$i]['selected']=='true' )
						{
							$subtotal+= (float)$facturas[$i]['subtotal'];
							$iva+= (float)$facturas[$i]['iva'];
							$total+= (float)$facturas[$i]['total'];
							$conceptos.= $facturas[$i]['concepto']."\n";
						}
					}
					else
					{
						//echo "SSS ".$facturas[$i]['selected'];
						if($facturas[$i]['selected']=='true' )//|| $facturas[$i]['selected']==true
						{
							$subtotal+= (float)$facturas[$i]['subtotal'];
							$iva+= (float)$facturas[$i]['iva'];
							$total+= (float)$facturas[$i]['total'];
							$conceptos.= $facturas[$i]['concepto']."\n";
						}
					}
				}
		
				list($id, $fecha, $hora)= fun_egresos_A_mcheque($idProveedor,
								$fechaemision,
								$concepto,
								$usuario,
								$subtotal, 
								$iva, 
								$total,
								$numbanco ); 
				if(strlen($id)>0)//Para ver si en realidad se genero un vale
				{
					for( $i=0 ; $i  < count($facturas) ; $i++ )
					{
						if($facturas[$i]['selected']=='true' )//|| $facturas[$i]['selected']==true
						{
							//print_r($facturas[$i]);
							$fails = func_egresos_A_dcheque($id, 
											$facturas[$i]['idtipodocto'], 
											$facturas[$i]['numdocto'], 
											$facturas[$i]['subtotal'],
											$facturas[$i]['iva'] ,
											$facturas[$i]['totaldoc']);
						}
					}
				}
				else
					{	$fails=true; $msgerror="No pudo generar ningun vale";}
			}
			else
			{
				//Un cheque por factura
				
				for( $i=0 ; $i  < count($facturas) ; $i++ )
				{
					if($facturas[$i]['selected']=='true' )//|| $facturas[$i]['selected']==true
					{
						list($id, $fecha, $hora)= fun_egresos_A_mcheque($idProveedor,
								$fechaemision,
								$facturas[$i]['concepto'],
								$usuario,
								$facturas[$i]['subtotal'], 
								$facturas[$i]['iva'], 
								$facturas[$i]['totaldoc'],
								$numbanco ); 
						if(strlen($id)>0)//Para ver si en realidad se genero un vale
						{
							$id_facts.= $id.", ";
							$fails = func_egresos_A_dcheque($id, 
										$facturas[$i]['idtipodocto'], 
										$facturas[$i]['numdocto'], 
										$facturas[$i]['subtotal'],
										$facturas[$i]['iva'] ,
										$facturas[$i]['totaldoc']);
						}
						else
						{	$fails=true; $msgerror="No pudo generar ningun vale";}
					}
				}
			}
		}
		else
			{	$fails=true; $msgerror=" No es un array la variable";}
	}
	else
	{	$fails=true; $msgerror="No existe usuario Logeado.";}

	$datos=array();//prepara el aray que regresara

	if($tipo=="true" )
		$datos[0]['id']=trim($id);// regresa el id de la
	else
		$datos[0]['id']=$id_facts;
		
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['msgerror']=$msgerror;
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_egresos_A_mcheque($prov,
								$fecha_pago,
								$descripcion,
								$usuario,
								$subtotal, 
								$iva, 
								$total,
								$numbanco)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_egresos_A_mcheque(?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$prov,
								&$fecha_pago,
								&$descripcion, 
								&$usuario,
								&$subtotal, 
								&$iva,
								&$total,
								&$numbanco);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die("sp_egresos_A_mcheque".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

//Funcion para registrar el p�roductoa  la requisicion
function func_egresos_A_dcheque($folio, 
										$tipodocto, 
										$numdocto, 
										$subtotal,
										$iva ,
										$total)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$id="";//Store proc regresa id de la nueva requisicion
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_egresos_A_dcheque(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$facturas[$i]['producto'], $facturas[$i]['cantidad'], $facturas[$i]['descripcion'],'0',$facturas[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = array(&$folio, 
					&$tipodocto, 
					&$numdocto, 
					&$subtotal,
					&$iva ,
					&$total);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt )
	{
		 $fails=false;
	}
	else
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die(  print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>