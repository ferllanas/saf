<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$idProveedor="";
if(isset($_GET['idProveedor']))
	$idProveedor = $_GET['idProveedor'];

$fecini="";
if(isset($_GET['fecini']))
	$fecini = $_GET['fecini'];

$fecfin="";
if(isset($_GET['fecfin']))
	$fecfin = $_GET['fecfin'];

$entro=false;
$datos= array();
if($conexion)
{
	$command= "SELECT a.vale,a.concepto, a.total, convert(varchar(10),a.fpago,103) as fpago,
	 5 as egresostipodocto, c.descrip, (SELECT SUM(iva) FROM egresosdvale WHERE vale=a.vale) as iva
	 , (SELECT SUM(subtotal) FROM egresosdvale WHERE vale=a.vale) as subtotal FROM egresosmvale a  INNER JOIN egresostipodocto c ON c.id=5 where a.estatus= 0";
	if(strlen($idProveedor)>0)
	{
		$command.= " AND a.prov = '$idProveedor'";
	}
	
	if(strlen($fecini)>0)
	{
		$command.= " AND a.fpago >= '$fecini'";
	}
	
	if(strlen($fecfin)>0)
	{
		$command.= " AND a.fpago <= '$fecfin'";
	}

	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['descriptipodocto']= trim($row['descrip']);
			$datos[$i]['concepto']= trim($row['concepto']);
			$datos[$i]['total']= trim($row['total']);
			$datos[$i]['fpago'] = $row['fpago']; 
			$datos[$i]['tipodocto']= trim($row['egresostipodocto']);
			$datos[$i]['numdocto']= trim($row['vale']);
			$datos[$i]['totaldoc']= trim($row['total']);
			$datos[$i]['factura'] = 0; 
			$datos[$i]['subtotal'] = $row['subtotal']; 
			$datos[$i]['iva'] = $row['iva']; 
			$datos[$i]['id_dvale'] = 0; 
			$i++;
		}
	}
	
	$command= "select a.opago as vale,  b.concepto, b.importe as total, convert(varchar(10),b.falta,103) as fpago ,
6 as egresostipodocto, c.descrip, 0.0 as iva, 0.0 as subtotal  FROM egresosmopago a 
INNER JOIN egresosmsolche b ON a.folio=b.folio
INNER JOIN egresostipodocto c ON c.id=6 WHERE a.estatus=20";
	if(strlen($idProveedor)>0)
	{
		$command.= " AND a.prov = '$idProveedor'";
	}
	
	if(strlen($fecini)>0)
	{
		$command.= " AND a.fpago >= '$fecini'";
	}
	
	if(strlen($fecfin)>0)
	{
		$command.= " AND a.fpago <= '$fecfin'";
	}

	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['descriptipodocto']= trim($row['descrip']);
			$datos[$i]['concepto']= trim($row['concepto']);
			$datos[$i]['total']= trim($row['total']);
			$datos[$i]['fpago'] = $row['fpago']; 
			$datos[$i]['tipodocto']= trim($row['egresostipodocto']);
			$datos[$i]['numdocto']= trim($row['vale']);
			$datos[$i]['totaldoc']= trim($row['total']);
			$datos[$i]['factura'] = 0; 
			$datos[$i]['subtotal'] = $row['subtotal']; 
			$datos[$i]['iva'] = $row['iva']; 
			$datos[$i]['id_dvale'] = 0; 
			$i++;
		}
	}
	echo json_encode($datos);
}
?>