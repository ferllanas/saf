<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>BUSCA/CANCELA CHEQUES</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
		<!--<script src="../../cheques/javascript/funciones_cheque.js"></script>-->
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('cheques_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);

						$.post('cheques_busqueda_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			
			function cancela_cheque(foliop, chequep, objeto)
			{
				var pregunta = confirm("¿Esta seguro que desea cancelar el cheque?.")
				if (pregunta)
				{
					
					objeto.style.display='none';
					
					var datas={
						folio: foliop,
						num_ch: chequep
					};
					
					jQuery.ajax({
								type:     'post',
								cache:    false,
								url:      'php_ajax/cancela_cheque.php',
								data:     datas,
								dataType: "json",
								success: function(resp) 
								{
									console.log(resp);
										if(resp!=null)
										{
											/*alert( resp.msg );*/
											if(resp.fallo==false)
											{
												//alert(resp.msg);
												alert( "El cheque con folio " +resp.num_ch + " ha sido cancelado." );
												location.href = 'cheques_busqueda.php';//?id='+myArray[0].id;
											}
											else
											{
												alert( "Error en proceso de actualizacion!." + resp.msg );
											}
										}
										else
											alert( "Error en proceso de actualizacion!." + resp.msg );
								}
							});
				}
			}
			/*function cancela_cheque(folio)
			{
			
				var pregunta = confirm("Esta seguro que desea eliminar el vale.")
				if (pregunta)
				{
					
					location.href="php_ajax/cancela_cheque.php?folio="+folio;
				}
			}*/
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if nomprov}}
					<td>${num_ch}</td>
					<td>${tipo}</td>
					<td>${nomprov}</td>
					<td>${falta}</td>
					<td align="right">$${importe}</td>
					<td>${concepto}</td> 
					<td>
						<img src="../../imagenes/consultar.jpg" onClick="window.open('../impreCheques/pdf_files/cheque_'+${folio}+'.pdf') ">
					{{if estatus==10}}
						<img src="../../imagenes/eliminar.jpg" onClick="javascript: cancela_cheque(${folio},${num_ch}, this)">
					{{/if}}
					{{if estatus==20}}
						Cheque Entregado
					{{/if}}
					{{if estatus>9000}}
						<img src="../../imagenes/borrar.jpg">
					{{/if}}
					</td>

                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">BUSCA/CANCELA CHEQUES</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Número de Cheque</option>
              <option value="6" >Número de Transferencia Electronica</option>
              <option value="2" >Proveedor</option>
              <option value="3" >Departamento</option>
              <option value="4" >Concepto</option>              
              <option value="5" >Por rango de fechas</option>
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
        
        
        
        
        
        
        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
  
  
  
   

  
  
  
  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren">
</td>
</tr>
</table>
<table>
                <thead>
    <th>Num. Cheque/Transf. Elec.</th>       
    				<th>Tipo</th>                                
                    <th>Proveedor</th>
                    <th>Fecha de alta</th>
                    <th>Importe</th>
                    <th>Concepto</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
