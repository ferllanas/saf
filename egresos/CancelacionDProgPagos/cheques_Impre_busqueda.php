<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Solicitud de Cheques</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="../CancelacionDProgPagos/javascript/divhide.js"></script>
        <script src="../../cheques/javascript/jquery.tmpl.js"></script>
		<script src="../../cheques/javascript/funciones_cheque.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						
						var e = document.getElementById('banco');
						var banco = e.options[e.selectedIndex].value;
						var ob= document.getElementById('tipo');
var tipo= ob.options[ob.selectedIndex].value;
						var data = 'query=' + opcion + $(this).val()+'&banco='+banco+'&tipo='+tipo;     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$('#proveedor').empty();
						$.post('cheques_Impre_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                   		}, 'json');  // Json es una muy buena opcion
                });
            });
			function change_Banco()
			{
				var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var e = document.getElementById('banco');
						var banco = e.options[e.selectedIndex].value;
						
						var valDopt="";
						if(opcion==5)
						{
							var fecini = document.getElementById('fecini').value;
							var fecfin = document.getElementById('fecfin').value;
							valDopt= fecini+""+fecfin;
						}
						else
							valDopt= $('#query').val();
							
						var ob= document.getElementById('tipo');
var tipo= ob.options[ob.selectedIndex].value;
						var data = 'query=' + opcion + valDopt +'&banco='+banco+'&tipo='+tipo;     // Toma el valor de los datos, que viene del input						$(this).val()
						//alert(data);
						$('#proveedor').empty();
						$.post('cheques_Impre_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
							//alert(resp.response);
							
							$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor');
						}, 'json');
			}
			
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var e = document.getElementById('banco');
						var banco = e.options[e.selectedIndex].value;
						var ob= document.getElementById('tipo');
var tipo= ob.options[ob.selectedIndex].value;
						var data = 'query=' + opcion +fecini+""+ fecfin+'&banco='+banco+'&tipo='+tipo;      // Toma el valor de los datos, que viene del input						$(this).val()
						//alert(data);
						$('#proveedor').empty();
						$.post('cheques_Impre_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
						}, 'json');
            }
			
			function cancela_cheque(folio)
			{
			
				var pregunta = confirm("Esta seguro que desea Eliminar esta Solicitud")
				if (pregunta)
				{
					location.href="php_ajax/cancela_sol_cheque.php?folio="+folio;
				}
			}
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if nomprov}}
					<td>
						{{if tipo=="num_ch"}}
							Cheque
						{{else}}
							Transferencia
						{{/if}}
					</td>
					<td style="text-align: center;">${num_ch}</td>
					<td>${nomprov}</td>
					<td style="text-align: center;">${falta}</td>
					<td style="text-align: right;">${importe}</td>
					<td>${concepto}</td> 
					<td>
					
					
					{{if tipo=="num_ch"}}
						{{if estatus==0}}
							Cheque Programado
						{{/if}}
						{{if estatus==10}}
							Cheque Emitido
						{{/if}}
						{{if estatus==20}}
							Cheque Entregado
						{{/if}}
						{{if estatus==30}}
							Cheque Cobrado
						{{/if}}
						{{if estatus==9000}}
							Cheque Cancelado
							</td>
							<td>
							<img src="../../imagenes/borrar.jpg">
						{{/if}}
					{{else}}
						{{if estatus==9000}}
								Transferencia Cancelada
							</td>
							<td>
							<img src="../../imagenes/borrar.jpg">
						{{/if}}
					{{/if}}
					</td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
				
				{{if tipo=="num_ch"}}
					<td>	<img src="../../imagenes/consultar.jpg" onClick="window.open('pdf_files/cheque_'+${folio}+'.pdf') "></td>
				{{else}}
					<td> 	<img src="../../imagenes/consultar.jpg" onClick="window.open('pdf_files/transf_'+${folio}+'.pdf') "></td>
				{{/if}}
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">BUSQUEDA DE CHEQUES IMPRESOS</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td>
	<select id="tipo" onChange="change_Banco()"><option value="num_ch">Cheque</option><option value="num_transf">Transferencia</option></select>
</td>
<td width="23%">       <select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Número de Cheque/transfer</option>
              <option value="2" >Proveedor</option>
              <!--<option value="3" >Departamento</option>-->
              <option value="4" >Concepto</option>              
              <option value="5" >Por rango de fechas</option>
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div id="cuentacheques" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
Banco:<select id="banco" onChange="change_Banco()"><?php
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$command2="SELECT banco, nombanco FROM egresosmbancos";
	$stmt2 = sqlsrv_query( $conexion,$command2);
	while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
	{
	?>
    	<option value="<?php echo utf8_decode(trim($row['banco']));?>"><?php echo utf8_decode(trim($row['nombanco']));?></option>
    <?php
	}
	?>
    <option value="" selected>Todos</option>
    </select>
</div>
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
                                            <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="hidden" name="ren" id="ren"></td>
</tr>
</table>
<table>
              <thead>
              		 <th>Tipo</th>
                    <th>Num. Cheque/Transferencia</th>                                       
                    <th>Proveedor</th>
                    <th>Fecha de alta</th>
                    <th>Importe</th>
                    <th>Concepto</th>
                    <th>Estado</th>
                     <th>&nbsp;</th>
                      <th>&nbsp;</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
