<?php
header("Content-type: text/html; charset=ISO-8859-1");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$entro=false;
$datos= array();

$numbanco="";
if(isset($_REQUEST['banco']))
	list($numbanco,$nombrebanco) = explode(".-",$_REQUEST['banco']);
	
	
	if($conexion)
	{
		$command= "SELECT a.num_ch, a.folio, a.banco, a.total, b.nomprov, c.nombanco, a.descrip, CONVERT(VARCHAR(10),a.fpago,103) as fpago  FROM egresosmcheque a 
					INNER JOIN compramprovs b ON a.prov=b.prov
					INNER JOIN egresosmbancos c ON c.banco=a.banco where (a.estatus=10 OR a.estatus<=9000)";
		if(strlen($numbanco)>0)
			$command.= " AND c.banco='$numbanco'";
	
		$command.= " ORDER BY folio";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			die($command."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['num_ch']= trim($row['num_ch']);
				$datos[$i]['folio_cheque']= trim($row['folio']);
				$datos[$i]['banco']= trim($row['banco']);
				$datos[$i]['TotalDcheque']= trim($row['total']);
				$datos[$i]['Proveedor']= utf8_decode(trim($row['nomprov']));
				$datos[$i]['nombanco']= utf8_decode(trim($row['nombanco']));
				$datos[$i]['concepto']= utf8_decode(trim($row['descrip']));
				$datos[$i]['fpago']= trim($row['fpago']);
				$datos[$i]['error']= 'false';//trim($row[]);
				$i++;
			}
		}
		
	}
echo json_encode($datos);

?>