<?php
header("Content-type: text/html; charset=UTF-8");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
require_once("../../../dompdf/dompdf_config.inc.php");

error_reporting(0);
$usarioCreador = $_COOKIE['ID_my_site'];	

$facturas=array();

$folio_ImpCheque="";
if(isset($_POST['folio_ImpCheque']))
	$folio_ImpCheque=$_POST['folio_ImpCheque'];

if(isset($_POST['prods']))
	$facturas=$_POST['prods'];
	
$qfechaemision= date("Y/m/d");
if(isset($_POST['qfechaemision']))
{
	$qfechaemision=$_POST['qfechaemision'];
	$qfechaemision=convertirFechaEuropeoAAmericano($qfechaemision);
}

print_r($folio_ImpCheque);
$htmlstr="";
$folio=0;
$fecha="";
$nomprov="";
$numprov="";
$concepto="";
$totalVale="";
$fechaPago="";
$quienReviso="";
$totalValeLetra="";
$tfacturas="";
$cantidadletras ="";
$num_che="";
$numcheque="";
$arrayCuentas=array();
$Sale=true;
$datosFin=array();
if(count($facturas)>0)
{
	//$old_limit = ini_set("memory_limit", "16M");
	
	
	$j=0;
	//$numcheque = $folio_ImpCheque;
	
	$Allcheques=getfirsthtml();
	$Allpdfs = new DOMPDF();	
	$Allpdfs->set_paper('letter');
	for( $i = 0; $i < count($facturas) ;  $i++)
	{
		
		
		if($facturas[$i]['selected']=='true' )
		{
			$dompdf = new DOMPDF();
	
			$dompdf->set_paper('letter');
			
			
			$j++;
			$command="SELECT a.folio, a.banco, a.total, b.nomprov, c.nombanco, a.descrip, c.tipoche, c.folioban FROM egresosmcheque a 
					INNER JOIN compramprovs b ON a.prov=b.prov
					INNER JOIN egresosmbancos c ON c.banco=a.banco where  folio=".$facturas[$i]['folio_cheque'];//a.estatus=10 AND
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r( "MAMA".sqlsrv_errors(), true));
			}
			else
			{
				
				$resoponsecode="Cantidad rows=".count($getProducts);
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$Sale=false;
					//$num_che= (double)$row['folioban'] + $j; //provisional antes del procedimineto almacenado
					$TotalCadena ="(".strtoupper(num2letras($row['total'])).")";//redondear_dos_decimal(trim()))).")";
					
					$pesosImporte = (string) number_format(trim($row['total']),2);//money_format($fmt, trim($row['total']));
					$banco = trim($row['banco']);
					$nombanco= trim($row['nombanco']);
					$nomprov = trim($row['nomprov']);
					$folio = trim($row['folio']);
					$concepto = trim($row['descrip']);
					$tipoche= trim($row['tipoche']);
					
					$fechaactual = date('m/d/Y');
					list($month,$days,$years)= explode('/', $fechaactual);
					$diadesem=date("w");
					$ffecha = (string)$dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
					
					
					/////////
					
					$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion

					$tsql_callSP ="SELECT a.folio, a.cuenta, a.concepto, a.debe, a.haber, a.estatus, b.num_ch as numch from egresosdcheque_ctas a LEFT JOIN egresosmcheque b ON b.folio=a.folio WHERE a.folio=".$folio;//Arma el procedimeinto almacenado
					//echo $tsql_callSP;
					//$params = array(&$folio, &$usarioCreador, &$banco,&$qfechaemision);//Arma parametros de entrada
					//$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
					
					$fails=false;
					
					sqlsrv_configure( 'WarningsReturnAsErrors' , 0 );
 
					$stmt = sqlsrv_query($conexion,$tsql_callSP);//, $params, $options);// $command);//
				
					if ( $stmt === false)
					{ 
						$resoponsecode="02";
						die($tsql_callSP.",".print_r($params).",". print_r( sqlsrv_errors(), true));
					}
					else
					{
						$g=0;
						while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
						{
							$arrayCuentas[$g]['cons']=     $row['cons'];
							$arrayCuentas[$g]['error']=    $row['error'];
							$arrayCuentas[$g]['concepto']= $row['concepto'];
							$arrayCuentas[$g]['debe']=     $row['debe'];
							$arrayCuentas[$g]['haber']=    $row['haber'];
							$tok = explode(".",$row['cuenta']);
							
							$arrayCuentas[$g]['SSSCTA']=$tok[count($tok)-1];
							$arrayCuentas[$g]['SSCTA']=$tok[count($tok)-2];
							
							$arrayCuentas[$g]['CUENTA']="";
							for($x=0; $x < count($tok)-2; $x++)
							{
								if($x==0)
									$arrayCuentas[$g]['CUENTA'].= $tok[$x];
								else
									$arrayCuentas[$g]['CUENTA'].= ".".$tok[$x];
							}
							$g++;
							$num_che = (string)$row['numch'];
							//echo "!!!".$num_che."!!!";
						}
					}
					
					sqlsrv_free_stmt( $stmt);
					//echo $pesosImporte;
					if($tipoche=="1")
					{
						$htmlstr = createPageFomat2($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
						$Allcheques.=    Add_Cheque($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
					}
					else
						if($tipoche=="2")
						{
							$htmlstr = createPageFomat2($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
							$Allcheques.=Add_Cheque($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
						}
						else
							if($tipoche=="3")
							{
								$htmlstr = createPageFomat2($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
								$Allcheques.=Add_Cheque($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
							}
							else
							{
								$htmlstr = createPageFomat2($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
								$Allcheques.=Add_Cheque($num_che, $folio, $pesosImporte, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas);
							}


					 if(($i+1) < count($facturas))
					 {
						$htmlstr.="<div style='page-break-after: always;'></div>";//$dompdf->new_page();  
						$Allcheques.="<div style='page-break-after: always;'></div>";
					 }
					else
					{
						$htmlstr .= "</body></html>";
						$Allcheques.= "</body></html>";
					}

					$dompdf->load_html($htmlstr);
					//$datosFin[$j]['html']=$htmlstr;
					//$numcheque++;
				}
			}
			
			if(!$Sale)
			{	$dompdf->render();
	  
				$pdf = $dompdf->output(); 
			
				$datos[0]['FILE'][$j-1]="pdf_files/cheque_".$folio.".pdf";
				file_put_contents("../pdf_files/cheque_".$folio.".pdf", $pdf);
			}
		}
	}
	
	if(!$Sale)
	{
		$Allpdfs->load_html($Allcheques);
		$Allpdfs->render();
		$pdfalls = $Allpdfs->output(); 
		
		$datosFin['FILE']="pdf_files/cheques_a_imprimir_".date("dmYHisu").".pdf";//[0]
		file_put_contents("../pdf_files/cheques_a_imprimir_".date("dmYHisu").".pdf", $pdfalls);
		$datosFin['fallo']=false;
	}
	else
	{
		$datosFin['fallo']=false;
		$datosFin['msgerror']="No son validos los cheques, seleccionados";
		
	}
	//$datos[0]['Countdatos']=count($arrayCuentas).",".$command;
}

echo json_encode($datosFin);
	
	
	//location
  	//$dompdf->stream("vale_".$numvale.".pdf");/**/
	
	function getfirsthtml()
	{
		$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body style='width: 601px; margin-top:30px'>";
		
		return $htmlstr;
	}
	
	function Add_Cheque($numcheque, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas)
	{
		//echo ",".$Total;
		$htmlstr="<table style='width: 100%;'>
			<tr style='heigth:6px;'>
				<td style='width: 85%;' >
					<table style='width: 100%;' >
						<tr>
							<td style='width: 50%; text-align:right;'>$banco&nbsp;</td>
							<td style='width: 50%; text-align:right;'>$numcheque&nbsp;</td>
						</tr>
					</table>
				</td>
				<td style='width: 15%;'>&nbsp;</td>
			</tr>
			<tr style='heigth:6px;'>
				<td style='width: 85%;' width='85%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right' colspan='2'>$ffecha&nbsp;</td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td style='width: 17%'>&nbsp;</td><td align='left' style='width: 83%'>$nomprov&nbsp;</td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td align='left' colspan='2'>$TotalCadena&nbsp;</td></tr>
					</table>
				</td>
				<td style='width: 15%;' width='15%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right' colspan='3'>&nbsp;</td></tr>
						<tr><td colspan='3'>&nbsp;</td></tr>
						<tr><td style='width: 25%;'>&nbsp;</td><td align='right' style='width: 50%;'>$Total</td><td style='width: 25%;'>&nbsp;</td></tr>
						<tr><td colspan='3'>&nbsp;</td></tr>
						<tr><td align='right' colspan='3' >&nbsp;</td></tr>
					</table>
				</td>
			<tr><td colspan='2'>&nbsp;</td></tr>
			</tr><tr>
				<td colspan='2' style='heigth:6px;'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='2'>&nbsp;</td>
			</tr>
			<tr >
				<td style='width: 100%;' width='100%' colspan='2'>
					<table style='width: 100%;' width='100%'>
						<tr>
							<td style='width: 70%; text-align:left;' width='60%'>&nbsp;</td>
							<td style='width: 10%; text-align:left;' width='10%'>&nbsp;</td>
							<td style='width: 20%; text-align:left;' width='30%'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr>
				<td style='width: 85%;' width='85%'>$concepto</td>
				<td style='width: 15%;' width='15%'>&nbsp;</td>
			</tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>$ffecha</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr style='height:22px'><td colspan='2'>&nbsp;</td></tr>
			<tr style='height:50px;'>
				<td style='width:100%; vertical-align:top' colspan='2'>
				<table style='width:100%;  vertical-align:top' >";
			
		
		
		$debetotal =0.00;
		$habertota =0.00;
		for($q=0;$q<count($arrayCuentas);$q++)
		{
			$htmlstr .= "<tr>
								<td style='width:75px' align='left'>".$arrayCuentas[$q]['CUENTA'].
								"&nbsp;</td><td style='width:33px' align='left'>".$arrayCuentas[$q]['SSCTA'].
								"&nbsp;</td><td style='width:40px' align='left'>".$arrayCuentas[$q]['SSSCTA'].
								//"</td><td style='width:200px'>".$arrayCuentas[$q]['SSSCTA'].
								"&nbsp;</td><td style='width:240px' align='left'>".$arrayCuentas[$q]['concepto'];
								
								if($arrayCuentas[$q]['debe']!=null)
									$htmlstr .="</td><td style='width:50px' align='right'>".number_format(trim($arrayCuentas[$q]['debe']),2);
								else
									$htmlstr .="</td><td style='width:50px' align='right'>Error";
									
								if($arrayCuentas[$q]['haber']!=null)
									$htmlstr .="&nbsp;</td><td style='width:100px' align='right'>".number_format(trim($arrayCuentas[$q]['haber']),2)."&nbsp;</td></tr>";
								else
									$htmlstr .="&nbsp;</td><td style='width:100px' align='right'>Error</td></tr>";
		$debetotal += (float)$arrayCuentas[$q]['debe'];
			$habertota += (float)$arrayCuentas[$q]['haber'];
		}


		
		for($q=count($arrayCuentas);$q<16;$q++)
		{
			$htmlstr .= "<tr>
								<td style='width:75px' align='left'>&nbsp;</td>
								<td style='width:33px' align='left'>&nbsp;</td>
								<td style='width:40px' align='left'>&nbsp;</td>
								<td style='width:240px' align='left'>&nbsp;</td>
								<td style='width:50px' align='right'>&nbsp;</td>
								<td style='width:100px' align='right'>&nbsp;</td>
						</tr>";

		}
		
		$htmlstr .= "<tr>
								<td style='width:75px' align='left'>&nbsp;</td>
								<td style='width:33px' align='left'>&nbsp;</td>
								<td style='width:40px' align='left'>&nbsp;</td>
								<td style='width:240px' align='left'>&nbsp;</td>
								<td style='width:50px' align='right'>".number_format($debetotal,2)."&nbsp;</td>
								<td style='width:100px' align='right'>".number_format($habertota,2)."&nbsp;</td>
						</tr>";

			
			/**/
		  $htmlstr .= "</table></td></tr></table>";//
		return $htmlstr;
	}



	function createPageFomat2($numcheque, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco, $arrayCuentas)//($numcheque, $folio, $TotalCadena, $nomprov ,$command)
	{
		//echo ",".$Total;
		$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<style>
		body 
		{
			font-family: 'Arial';
			font-size: 9;
			width: 601px; 
			margin-top: 30px;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body >
		<table style='width: 100%;'>
			<tr style='heigth:6px;'>
				<td style='width: 85%;' >
					<table style='width: 100%;' >
						<tr>
							<td style='width: 50%; text-align:right;'>$banco&nbsp;</td>
							<td style='width: 50%; text-align:right;'>$numcheque&nbsp;</td>
						</tr>
					</table>
				</td>
				<td style='width: 15%;'>&nbsp;</td>
			</tr>
			<tr style='heigth:6px;'>
				<td style='width: 85%;' width='85%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right' colspan='2'>$ffecha&nbsp;</td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td style='width: 17%'>&nbsp;</td><td align='left' style='width: 83%'>$nomprov&nbsp;</td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td align='left' colspan='2'>$TotalCadena&nbsp;</td></tr>
					</table>
				</td>
				<td style='width: 15%;' width='15%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right'>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td style='width: 25%;'>&nbsp;</td><td align='right' style='width: 50%;'>$Total</td><td style='width: 25%;'>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td align='right'>&nbsp;</td></tr>
					</table>
				</td>
			<tr><td colspan='2'>&nbsp;</td></tr>
			</tr><tr>
				<td colspan='2' style='heigth:6px;'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='2'>&nbsp;</td>
			</tr>
			<tr >
				<td style='width: 100%;' width='100%' colspan='2'>
					<table style='width: 100%;' width='100%'>
						<tr>
							<td style='width: 70%; text-align:left;' width='60%'>&nbsp;</td>
							<td style='width: 10%; text-align:left;' width='10%'>&nbsp;</td>
							<td style='width: 20%; text-align:left;' width='30%'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr>
				<td style='width: 85%;' width='85%'>$concepto</td>
				<td style='width: 15%;' width='15%'>&nbsp;</td>
			</tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>$ffecha</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr style='height:22px'><td colspan='2'>&nbsp;</td></tr>
			<tr style='height:50px;'>
				<td style='width:100%; vertical-align:top' colspan='2'>
				<table style='width:100%;  vertical-align:top' >";
			
		
		
		$debetotal =0.00;
		$habertota =0.00;
		for($q=0;$q<count($arrayCuentas);$q++)
		{
			$htmlstr .= "<tr>
								<td style='width:75px' align='left'>".$arrayCuentas[$q]['CUENTA'].
								"&nbsp;</td><td style='width:33px' align='left'>".$arrayCuentas[$q]['SSCTA'].
								"&nbsp;</td><td style='width:40px' align='left'>".$arrayCuentas[$q]['SSSCTA'].
								//"</td><td style='width:200px'>".$arrayCuentas[$q]['SSSCTA'].
								"&nbsp;</td><td style='width:240px' align='left'>".$arrayCuentas[$q]['concepto'];
								
								if($arrayCuentas[$q]['debe']!=null)
									$htmlstr .="</td><td style='width:50px' align='right'>".number_format(trim($arrayCuentas[$q]['debe']),2);
								else
									$htmlstr .="</td><td style='width:50px' align='right'>Error";
									
								if($arrayCuentas[$q]['haber']!=null)
									$htmlstr .="&nbsp;</td><td style='width:100px' align='right'>".number_format(trim($arrayCuentas[$q]['haber']),2)."&nbsp;</td></tr>";
								else
									$htmlstr .="&nbsp;</td><td style='width:100px' align='right'>Error</td></tr>";
		$debetotal += (float)$arrayCuentas[$q]['debe'];
			$habertota += (float)$arrayCuentas[$q]['haber'];
		}


		
		for($q=count($arrayCuentas);$q<16;$q++)
		{
			$htmlstr .= "<tr>
								<td style='width:75px' align='left'>&nbsp;</td>
								<td style='width:33px' align='left'>&nbsp;</td>
								<td style='width:40px' align='left'>&nbsp;</td>
								<td style='width:240px' align='left'>&nbsp;</td>
								<td style='width:50px' align='right'>&nbsp;</td>
								<td style='width:100px' align='right'>&nbsp;</td>
						</tr>";

		}
		
		$htmlstr .= "<tr>
								<td style='width:75px' align='left'>&nbsp;</td>
								<td style='width:33px' align='left'>&nbsp;</td>
								<td style='width:40px' align='left'>&nbsp;</td>
								<td style='width:240px' align='left'>&nbsp;</td>
								<td style='width:50px' align='right'>".number_format($debetotal,2)."&nbsp;</td>
								<td style='width:100px' align='right'>".number_format($habertota,2)."&nbsp;</td>
						</tr>";

			
			/**/
		  $htmlstr .= "</table></td></tr></table>";//
		return $htmlstr;
	}

	function createPageFomat1($numcheque, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco)
	{
	$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body style='width: 601px; margin-top:30px'>
		<table  width='601px'>
			<tr style='heigth:6px;'>
				<td style='width: 70%;' width='70%'>
					<table style='width: 100%;' width='100%'>
						<tr>
							<td style='width: 50%; text-align:right;' width='50%'>$banco</td>
							<td style='width: 50%; text-align:right;' width='50%'>$numcheque</td></tr>
					</table>
				</td>
				<td style='width: 30%;' width='30%'>&nbsp;</td>
			</tr>
			<tr style='heigth:6px;'>
				<td style='width: 70%;' width='70%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right' colspan='2'>$ffecha</td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td style='width: 17%'>&nbsp;</td><td align='left' style='width: 83%'>$nomprov </td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td align='left' colspan='2'>$TotalCadena</td></tr>
					</table>
				</td>
				<td style='width: 30%;' width='30%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right'>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td style='width: 25%;'>&nbsp;</td><td align='right' style='width: 50%;'>$Total</td><td style='width: 25%;'>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td align='right'>&nbsp;</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan='2' style='heigth:6px;'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='2'>&nbsp;</td>
			</tr>
			<tr >
				<td style='width: 100%;' width='100%' colspan='2'>
					<table style='width: 100%;' width='100%'>
						<tr>
							<td style='width: 70%; text-align:left;' width='60%'>&nbsp;</td>
							<td style='width: 10%; text-align:left;' width='10%'>&nbsp;</td>
							<td style='width: 20%; text-align:left;' width='30%'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					$concepto
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>$ffecha</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr style='height:22px'><td>&nbsp;</td></tr>
			<tr>
			<tr style='height:50px;'>
				<td style='width:100%; vertical-align:top'>
				<table style='width:100%;  vertical-align:top' >
			
		";
		
		$debetotal =0.00;
		$habertota =0.00;
		for($q=0;$q<count($arrayCuentas);$q++)
		{
			$htmlstr .= "<tr>
								<td style='width:75px' align='left'>".$arrayCuentas[$q]['CUENTA'].
								"</td><td style='width:33px' align='left'>".$arrayCuentas[$q]['SSCTA'].
								"</td><td style='width:40px' align='left'>".$arrayCuentas[$q]['SSSCTA'].
								//"</td><td style='width:200px'>".$arrayCuentas[$q]['SSSCTA'].
								"</td><td style='width:240px' align='left'>".$arrayCuentas[$q]['concepto'];
								
								if($arrayCuentas[$q]['debe']!=null)
									$htmlstr .="</td><td style='width:50px' align='right'>".number_format(trim($arrayCuentas[$q]['debe']),2);
								else
									$htmlstr .="</td><td style='width:50px' align='right'>Error";
									
								if($arrayCuentas[$q]['haber']!=null)
									$htmlstr .="</td><td style='width:100px' align='right'>".number_format(trim($arrayCuentas[$q]['haber']),2)."</td></tr>";
								else
									$htmlstr .="</td><td style='width:100px' align='right'>Error</td></tr>";
		$debetotal += (float)$arrayCuentas[$q]['debe'];
			$habertota += (float)$arrayCuentas[$q]['haber'];
		}
		  $htmlstr .= "</table></td></tr>
		  <tr>
		  	<td><table><tr><td style='width:250px;'>&nbsp;</td><td style='width:70px; text-align:right;'>".number_format($debetotal,2)."</td><td  style='width:70px;text-align:right;'>".number_format($habertota,2)."</td></tr></table>
			</td>
			</tr></table></body></html>";
		  $htmlstr .= "</table></td></tr></table>";
		return $htmlstr;
	}
	
function createPageFomat3($numcheque, $folio, $Total, $TotalCadena, $nomprov ,$command, $ffecha, $concepto, $banco)//($numcheque, $folio, $TotalCadena, $nomprov ,$command)
	{
		$htmlstr="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
		<html>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
		}
		
		table {
			margin-top: 0em;
			margin-left: 0em;
			vertical-align:top;
			border-collapse:collapse; 
			border: none;
		}
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		.texto14{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>
		
		</head>
		
		<body style='width: 601px; margin-top:30px'>
		<table  width='601px'>
			<tr style='heigth:6px;'>
				<td style='width: 70%;' width='70%'>
					<table style='width: 100%;' width='100%'>
						<tr>
							<td style='width: 50%; text-align:right;' width='50%'>$banco</td>
							<td style='width: 50%; text-align:right;' width='50%'>$numcheque</td></tr>
					</table>
				</td>
				<td style='width: 30%;' width='30%'>&nbsp;</td>
			</tr>
			<tr style='heigth:6px;'>
				<td style='width: 70%;' width='70%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right' colspan='2'>$ffecha</td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td style='width: 17%'>&nbsp;</td><td align='left' style='width: 83%'>$nomprov </td></tr>
						<tr><td colspan='2'>&nbsp;</td></tr>
						<tr><td align='left' colspan='2'>$TotalCadena</td></tr>
					</table>
				</td>
				<td style='width: 30%;' width='30%'>
					<table style='width: 100%' width='100%'>
						<tr><td align='right'>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td style='width: 25%;'>&nbsp;</td><td align='right' style='width: 50%;'>$Total</td><td style='width: 25%;'>&nbsp;</td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td align='right'>&nbsp;</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan='2' style='heigth:6px;'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='2'>&nbsp;</td>
			</tr>
			<tr >
				<td style='width: 100%;' width='100%' colspan='2'>
					<table style='width: 100%;' width='100%'>
						<tr>
							<td style='width: 70%; text-align:left;' width='60%'>&nbsp;</td>
							<td style='width: 10%; text-align:left;' width='10%'>&nbsp;</td>
							<td style='width: 20%; text-align:left;' width='30%'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					$concepto
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>$ffecha</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr style='height:22px'><td>&nbsp;</td></tr>
			<tr>
			<tr style='height:100px;'>
				<td style='width:100%; vertical-align:top'>
				<table style='width:100%;  vertical-align:top' >
			
		";
		
		$debetotal =0.00;
		$habertota =0.00;
		for($q=0;$q<count($arrayCuentas);$q++)
		{
			$htmlstr .= "<tr>
								<td style='width:75px' align='left'>".$arrayCuentas[$q]['CUENTA'].
								"</td><td style='width:33px' align='left'>".$arrayCuentas[$q]['SSCTA'].
								"</td><td style='width:40px' align='left'>".$arrayCuentas[$q]['SSSCTA'].
								//"</td><td style='width:200px'>".$arrayCuentas[$q]['SSSCTA'].
								"</td><td style='width:240px' align='left'>".$arrayCuentas[$q]['concepto'];
								
								if($arrayCuentas[$q]['debe']!=null)
									$htmlstr .="</td><td style='width:50px' align='right'>".number_format(trim($arrayCuentas[$q]['debe']),2);
								else
									$htmlstr .="</td><td style='width:50px' align='right'>Error";
									
								if($arrayCuentas[$q]['haber']!=null)
									$htmlstr .="</td><td style='width:100px' align='right'>".number_format(trim($arrayCuentas[$q]['haber']),2)."</td></tr>";
								else
									$htmlstr .="</td><td style='width:100px' align='right'>Error</td></tr>";
		$debetotal += (float)$arrayCuentas[$q]['debe'];
			$habertota += (float)$arrayCuentas[$q]['haber'];
		}
		  $htmlstr .= "</table></td></tr>
		  <tr>
		  	<td><table><tr><td style='width:250px;'>&nbsp;</td><td style='width:70px; text-align:right;'>".number_format($debetotal,2)."</td><td  style='width:70px;text-align:right;'>".number_format($habertota,2)."</td></tr></table>
			</td>
			</tr></table></body></html>";
		  $htmlstr .= "</table></td></tr></table>";
		return $htmlstr;
	}
?>