<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$folio=0;
if(isset($_REQUEST['folio']))
	$folio=$_REQUEST['folio'];
	
$qfechaemision= date("Y/m/d");

$usarioCreador = $_COOKIE['ID_my_site'];	
$datos=array();

$command="Update egresosmcheque SET usuentrega='', fentrega=NULL, estatus=10 WHERE folio=".$folio;
$getProducts = sqlsrv_query( $conexion_srv,$command);
if ( $getProducts === false)
{ 
	$datos['fallo']=true;
	$datos['error']=1;
	$datos['msgerror']="Ha ocurrido un error al actualizar el registro en la base de datos. $command";
}
else
{
	$datos['fallo']=false;
	$datos['error']=0;
	$datos['msgerror']="El cheque ha sido marcado como no entregado.";
}

echo json_encode($datos);
?>