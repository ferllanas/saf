<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
////
require_once("../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
$BorrarPriv=revisaPrivilegiosBorrar($privilegios);
//$VerPDFPriv=revisaPrivilegiosPDF($privilegios);
//$EditarPriv=revisaPrivilegiosEditar($privilegios);
////
?><html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
<title>Impresión de Cheques</title>
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="../CancelacionDProgPagos/javascript/busquedaIncrementalBanco.js"></script>
<script language="javascript" src="../CancelacionDProgPagos/javascript/impreCheques.js"></script>
</head>
<body onLoad="$('banco').focus();">
<form>
<input type="hidden" id="EliminarPriv" value="<?php echo $BorrarPriv;?>">
<table width="100%">
	<tr>
		<td width="100%" colspan="4" align="center"class="TituloDForma" style="border-width:0px;">Cancelacion Programaci&oacute;n de Pagos
		  <hr class="hrTitForma"><input type="hidden" id="tipoDoc" value="10"></td>
	</tr>
	<tr>
		<td width="30%" class="texto8"><div align="left" style="z-index:4; position:absolute; width:295px; left: 8px; top: 47px; height: 19px;">     
										Banco:
										    <input class="texto8" type="text" id="banco" name="banco" style="width:250px;"  onKeyUp="searchBancos(this);" autocomplete="off">
										<!--<input type="text" id="prov1" name="prov1" class="caja_toprint texto8" style="width:200px;">-->
										<div id="search_suggestProv" style="z-index:5;" > </div>
</div><!--<input type="button" value="Muestra pendientes" class="texto8" onClick="buscarCheques();" tabindex="1">--></td>
		
		<td width="15%" class="texto8" align="right">Total:		  
      <input type="text" id="totcheque" name="totcheque" class="caja_toprint" readonly="true" tabindex="-1" value="0.0" style="width:100px; text-align:right" align="right" ></td>
		<!--<td width="25%"><input type="button" value="Imprime Cheques" class="texto8" tabindex="3" onClick="imprimeCheques();"></td>-->
	</tr>
</table>

 <table class="tableone" id="sortable" style="width:650px;">	
      <thead >
                    <tr>
                        <th class="th1">&nbsp;<!--<input type="checkbox" titel="Click aqui para seleccionar o deseleccionar todos."  onClick="seleccionarODescTodos();" id="selesctodos" name="selesctodos">--></th>
                        <th class="th2" style="width:200px;text-align:left;">Proveedor</th>
                        <th class="th3" style="width:200px; text-align:left;">Concepto</th>
                        <!-- <th class="th3" style="width:100px; text-align:left;">Fecha de pago</th>-->
                        <th class="th4"  style="text-align:right;">Total</th>
                    </tr>
                </thead>
                <tbody >
                    <tr>
                        <td colspan="8">
                            <div class="innerb" style="width:650px;">
                                <table class="tabletwo"  style="width:650px;">
                                    <tbody id="resultados" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
       		</table>
</form>
<!--<table width="100%">
	<thead style="width:100%; ">
		<tr>
			<th width="10%" class="subtituloverde8"><input type="checkbox" titel="Click aqui para seleccionar o deseleccionar todos."  onClick="seleccionarODescTodos();" id="selesctodos" name="selesctodos"></th>
			<th width="40%" class="subtituloverde8">Proveedor</th>
			<th width="40%" class="subtituloverde8">Concepto</th>
			<th width="10%" class="subtituloverde8">Total</th>
		</tr>
	</thead>
	<tbody id="resultados" name="resultados">
		
	</tbody>
</table>-->
</body>
</html>
