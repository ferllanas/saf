<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Rempresión de Cheques</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="../CancelacionDProgPagos/javascript/reeimpresion_busquedaIncrementalBanco.js"></script>
<script language="javascript" src="../CancelacionDProgPagos/javascript/impreCheques.js"></script>
</head>
<body onLoad="$('banco').focus();">
<form>
<table width="100%">
	<tr>
		<td width="100%" colspan="4" align="center"class="TituloDForma" style="border-width:0px;">Reimpresi&oacute;n de Cheques<hr class="hrTitForma"></td>
	</tr>
	<tr>
		<td width="30%" class="texto8"><div align="left" style="z-index:4; position:absolute; width:295px; left: 8px; top: 47px; height: 19px;">     
										Banco:
										    <input class="texto8" type="text" id="banco" name="banco" style="width:250px;"  onKeyUp="searchBancosreempresion(this);" autocomplete="off">
										<!--<input type="text" id="prov1" name="prov1" class="caja_toprint texto8" style="width:200px;">-->
										<div id="search_suggestProv" style="z-index:5;" > </div>
</div><!--<input type="button" value="Muestra pendientes" class="texto8" onClick="buscarCheques();" tabindex="1">--></td>
		<td width="20%" class="texto8" align="center">Ultimo cheque impreso:<input id="num_chUltimo" name="num_chUltimo" class="caja_toprint" readonly value="" style="width:auto; max-width:100px; overflow:auto; "></td>
        <td>Fecha emisi&oacute;n
          <input type="text" id="fecha_cheque" class="texto9" name="fecha_cheque" onBlur="javascript: validaFechaImpresionCheques(this)" style="width:70px;" value="<?php echo date('d/m/Y');?>">
<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title="Date selector" align="absmiddle">dd/mm/aaaa
  <script type="text/javascript">
													Calendar.setup({
														inputField     :    "fecha_cheque",		// id of the input field
														ifFormat       :    "%d/%m/%Y",		// format of the input field
														button         :    "f_trigger1",	// trigger for the calendar (button ID)
														//onClose        :    fecha_cambio,
														singleClick    :    true,
														onSelect   : function() { 
																					var date = this.date.print(this.params.ifFormat);
																					
																					var datas = {
																						fechaimpresion		: 		date,
																					};
																					var salir = false;
																					jQuery.ajax({
																											type:     'post',
																											cache:    false,
																											dataType: "json",
																											url:      'php_ajax/datediff_function.php',
																											data:     datas,
																											success: function(resp) 
																											{
																												console.log(resp)
																												if(resp!=null)
																												{
																													if(resp.numerror>0)
																													{
																														alert(resp.error);
																														salir=true;
																													}
																													else
																													{
																														document.getElementById("fecha_cheque").value=date;
																													}
																												}
																											}
																										});
																					if(!salir)
																					{
																						this.callCloseHandler();
																					}
																				}
													});
												</script>	 </td>
		<td width="15%" class="texto8">Total:		  
      <input type="text" id="totcheque" name="totcheque" class="caja_toprint" readonly="true" tabindex="-1" value="0.0" style="width:100px; text-align:right" align="right" ></td>
		<td width="25%"><input type="button" value="Reimprime Cheques" class="texto8" tabindex="3" onClick="reimprimeCheques();"></td>
	</tr>
</table>

 <table class="tableone" id="sortable" style="width:650px;">	
      <thead >
                    <tr>
                        <th class="th1"><input type="checkbox" titel="Click aqui para seleccionar o deseleccionar todos."  onClick="seleccionarODescTodos();" id="selesctodos" name="selesctodos"></th>
                         <th class="th2" style="width:200px;text-align:left;">Num. Cheque</th>
                        <th class="th2" style="width:200px;text-align:left;">Proveedor</th>
                        <th class="th3" style="width:200px; text-align:left;">Concepto</th>
                        <!-- <th class="th3" style="width:100px; text-align:left;">Fecha de pago</th>-->
                        <th class="th4"  style="text-align:right;">Total</th>
                    </tr>
                </thead>
                <tbody >
                    <tr>
                        <td colspan="8">
                            <div class="innerb" style="width:650px;">
                                <table class="tabletwo"  style="width:650px;">
                                    <tbody id="resultados" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
       		</table>
</form>
<!--<table width="100%">
	<thead style="width:100%; ">
		<tr>
			<th width="10%" class="subtituloverde8"><input type="checkbox" titel="Click aqui para seleccionar o deseleccionar todos."  onClick="seleccionarODescTodos();" id="selesctodos" name="selesctodos"></th>
			<th width="40%" class="subtituloverde8">Proveedor</th>
			<th width="40%" class="subtituloverde8">Concepto</th>
			<th width="10%" class="subtituloverde8">Total</th>
		</tr>
	</thead>
	<tbody id="resultados" name="resultados">
		
	</tbody>
</table>-->
</body>
</html>
