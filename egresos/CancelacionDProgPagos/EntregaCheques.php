<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../cheques/css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Entrega de Cheques</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
		<script src="../../cheques/javascript/jquery.tmpl.js"></script>
        <script src="../CancelacionDProgPagos/javascript/divhideEntregaCancela.js"></script>
		<script src="../../cheques/javascript/funciones_cheque.js"></script>
		<script type="text/javascript" src="../../cheques/javascript/jquery.ingrid-0.9.9-min.js"></script>
     <!--   
     	<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <link rel="stylesheet" type="text/css" href="css/estilos.css">-->
       
        <script>
		$(document).ready(function() {
	$(".botonExcel").click(function(event) {
		$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
		$("#FormularioExportacion").submit();
});
});
		
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + $(this).val();     // Toma el valor de los datos, que viene del input						
						//alert(data);
						$.post('EntregaCheques_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion
                });
            });
			function busca_cheque()
			{
                
					// Valida la informacion de las variables de fecha, posterior a ello se tendra que desconcatenar la fecha 01/10/2011 a 20111001, hecho en el ajax.php
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
						var data = 'query=' + opcion + fecini + fecfin;     // Toma el valor de los datos, que viene del input						
						//alert(data);

						$.post('EntregaCheques_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			
			function cambia_estatus(objects, cheque)
			{
				var pregunta = confirm("¿Esta seguro marcar como entregado?")
				if (pregunta)
				{
					var datas = {
									folio		: 		cheque
								};//folio_ImpCheque : folio_ImpCheque,
					//alert('php_ajax/crear_chequespdf.php?'+prods);
					//location.href = 'php_ajax/crear_chequespdf.php?'+datas;
					
					jQuery.ajax({
								type:     'post',
								cache:    false,
								dataType: "json",
								url:      'php_ajax/EntregaChequeUpdate.php',
								data:     datas,
								/*error: function(request,error){
									alert(error);
									alert(request);
								},*/
								success: function(request) 
								{
								
									//console.log(request)
									if(request!=null)
									{
										if(request.fallo==false)
										{	
											alert( request.msgerror );
											 $(objects).parent().parent().remove();
										}
										else
										{
											alert( "Error en proceso de actualizacion!. " + request.msgerror );
										}
									
									}
								}
							});
				}
			}
			
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr class="fila">
                {{if nomprov}}
					<td align="center"><img src="../../imagenes/actualiza3.png" onclick="javascript:cambia_estatus(this, ${folio});" width="16px" title="Click aqui para indicar que el cheque fue entregado."></td>
					<td align="left">${num_ch}</td>
					<td align="center">${nombanco}</td>
					<td align="left">${nomprov}</td>
					<td align="center">${fpago}</td>
					<td align="right">${total}</td>
					
                {{else}}
					<td colspan="7">No existen resultados</td>
                {{/if}}
            </tr>
        </script>   
   



<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
        
               
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Entrega de Cheques</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">       <select name="opc" id="opc" onChange="ShowHidden()">
              <option value="0" >-- Seleccione un opción --</option>
              <option value="1" >Número de Cheque</option>
              <option value="2" >Banco</option>
              <option value="3" >Proveedor</option>              
         <!--     <option value="5" >Por rango de fechas</option>
              <option value="6" >Por rango de fecha de pago</option>-->
			</select></td>

<!--  Selecciona el numero de Cheque  -->
<!--  Selecciona por Departamento, checa esta opcion ya que desborraste los comentarios de este y proveedor --><!--  Selecciona por Proveedor  -->
<!--  Selecciona por fecha  -->
<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:hidden; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td><!-- Asignamos variables de fechas -->
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:


<input type="text"  name="fecini" id="fecini" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>

  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
 
          <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger2",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>  
                                              <input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_cheque()">
</div>
&nbsp;&nbsp;&nbsp;&nbsp;
<!--<input type="hidden" name="ren" id="ren"><input type="button" value="Exportar a Excel" onClick="generateexcel('mytable');"></td>
<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
<p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form>-->
</tr>
</table>
<table id="Exportar_a_Excel">
                <thead>
                	<th width="15px">&nbsp;</th>   
                    <th width="30px">Num. Cheque</th> 
					<th width="70px">Banco</th>                                       
                   	<th width="100px">Proveedor</th>
                    <th width="30px">Fecha de pago</th>
                    <th width="40px">Total</th>
                </thead>
                <tbody id="proveedor" name='proveedor'>
                    <tr>
					  <td>&nbsp;</td>
					  <td colspan="2">Encontrar Resultados</td>
                    </tr>
                </tbody>
  </table>
    </div>
    </body>
</html>
