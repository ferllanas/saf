 var $j = jQuery.noConflict();

var st_Cheques=new Array(); 
var countFacturasdProv =0;


function revisa_DocSeleccionados()
{
	var selectedOne=false;

	for( var index=0;index< countFacturasdProv && selectedOne==false;index++)
		if(st_Cheques[index]['selected']==true)
			selectedOne=true;

	return selectedOne;
}

function imprimeCheques()
{
	//var folio_ImpCheque=$('folio').value;//0 todos los cheques
	
	if( !revisa_DocSeleccionados() )
	{
		alert("No ha seleccionado al menos un cheque para su impresi�n.");
		return false;
	}

	if(!confirm("Esta seguro desea imprimir los siguientes cheques."))
	{
		return false;
	}
	
	var salir=false;
	var fechaemision=$j("input[name^=fecha_cheque]").val();
	/*.each(function()
									 {
										if(validaFechaImpresionCheques(this))
										{
											this.select();
											this.focus();
											salir=true;
											return(false);
										}
									 });*/
	if(salir)
		return false;
	//return false;
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					prods		: 		st_Cheques,
					qfechaemision: fechaemision,
					tipoDoc : 10
    			};//folio_ImpCheque : folio_ImpCheque,
	//alert('php_ajax/crear_chequespdf.php?'+prods);
	//location.href = 'php_ajax/crear_chequespdf.php?'+datas;
	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				dataType: "json",
				url:      'php_ajax/crear_chequespdf.php',
				data:     datas,
				/*error: function(request,error){
                	alert(error);
					alert(request);
            	},*/
				success: function(request) 
				{
				
					//console.log(request)
					if(request!=null)
					{
						if(request.fallo==false)
						{	document.getElementById('totcheque').value="0.0";
							document.getElementById('num_chUltimo').value="";
							document.getElementById('banco').value="";
							document.getElementById('selesctodos').checked=false;
							myRef = window.open('php_ajax/impresionAutomatica.php?FILER='+ request.FILE,'archivo');
							/*for(j=0; j<request[0].FILE.length;j++)
							{
								
								
								
								
								//location.href = myArray[0].FILE;//'pdf_files/cheques_a_imprimir.pdf';
							}*/
						}
						else
						{
							alert( "Error en proceso de actualizacion!." + request.msgerror );
						}
					
					}
				}
			});
	/*jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/validaNumCheque.php',
				data:     datas,
				success: function(resp) 
				{
					//alert(resp)
					if( resp.length ) 
					{
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							if(myArray[0].fallo==false)
							{
								var answer = confirm("�Seguro deseas aplicar la generacion de cheques a los documentos seleccionados?")
								if (!answer)
								{
									return false;
								}
							
							
								
								jQuery.ajax({
											type:     'post',
											cache:    false,
											url:      'php_ajax/crear_chequespdf.php',
											data:     datas,
											success: function(resp) 
											{
												//alert(resp)
												if( resp.length ) 
												{
													var myArray = eval(resp);
													if(myArray.length>0)
													{
														if(myArray[0].fallo==false)
														{
															alert( "Se ha generado la programacion de cheque con el/los siguinetes folios: " +myArray[0].id + "." );
															//window.open('php_ajax/crear_Valepdf.php?id='+myArray[0].id,'Comparativo','width=800,height=600');
															location.href = 'prog_pagos.php';//?id='+myArray[0].id;
														}
														else
														{
															alert( "Error en proceso de actualizacion!." + myArray[0].msgerror );
														}
													}
													else
														alert( "Error en proceso de actualizacion!." + resp );
												}
											}
										});
							}
							else
							{
								alert("Ya existe un cheque impreso con alguno de los siguientes n�meros " + myArray[0].msgerror );
							}
						}
						else
							alert( "Error en proceso de actualizacion!." + resp );
					}
				}
			});*/
	//
	
}


function buscarCheques()
{
	var bancoq = $('banco').value;
	var tipoDoc = $('tipoDoc').value;
	//alert(bancoq+'  '+tipoDoc);
	var datas = {
					banco : bancoq,
					tipo: tipoDoc
    			};
				
	jQuery.ajax({
				async:          true,
				type:     'post',
				cache:    false,
				url:      'php_ajax/buscarCheques.php',
				dataType: "json",
				data:     datas,
				success: function(source)
					{
						console.log(source);
						if(source!=null)
						{
							var Table = $('resultados');
							for(var i = Table.rows.length-1; i >=0; i--)
							{
								Table.deleteRow(i);
							}
							if(source.length>0)
							{
								
								for(var i=0;i<source.length;i++)
								{
									agregar_ChequesATablaResultados(source[i].folio_cheque, source[i].Proveedor , 
																							source[i].TotalDcheque, 
																							source[i].banco, 
																							source[i].concepto, 
																							source[i].fpago);
									//alert(source[i].folio_cheque);
									//alert(source[i].Proveedor);
									//alert(source[i].TotalDcheque);
									//alert(source[i].banco);
									//alert(source[i].nombanco);
									st_Cheques[countFacturasdProv]= new  objfacturaDProv(source[i].folio_cheque, source[i].Proveedor , 
																							source[i].TotalDcheque , source[i].banco, source[i].nombanco);
									countFacturasdProv++;
									//$('numbanco').focus();
								}
							}
							else
							{
								
								document.getElementById('totcheque').value="0.0";
								document.getElementById('num_chUltimo').value="";
								alert("No existen cheques a imprimir para el banco seleccionado.");
							}
						}
						else
						{
							alert("No obtuvo resultados!.");
						}
					}
				});
}


function agregar_ChequesATablaResultados(folio_cheque ,Proveedor , TotalDcheque,banco, nombanco, fpago)
{
	var Table = $('resultados');
	var contRow = Table.rows.length;
	var $EliminarPriv = document.getElementById("EliminarPriv").value;
	//Create the new elements
	var newrow = Table.insertRow(contRow);

	var newcell4 = newrow.insertCell(0);
	newcell4.className="td3";
	newcell4.style.width ="100px";
	newcell4.align="center";
	
	if($EliminarPriv==1)
		newcell4.innerHTML ='<img src="../../imagenes/eliminar.jpg" onclick="cancelaProgPago('+folio_cheque+')">';
	else
		newcell4.innerHTML ="";
	/*var newcell1 = newrow.insertCell(0);
	newcell1.className="td1";
	newcell1.innerHTML ="<input type='checkbox' style='width: 10px;' id='selectToPago' name='selectToPago' onClick='doctoSelected("+folio_cheque+")' tabindex='-1'>";
	*/
	var newcell2 = newrow.insertCell(1);
	newcell2.className="td2";
	newcell2.style.width ="200px";
	newcell2.innerHTML ="<a class='texto8' >"+Proveedor+"</a>";

	var newcell4 = newrow.insertCell(2);
	newcell4.className="td3";
	newcell4.style.width ="200px";
	newcell4.innerHTML ="<input type='hidden' id='banco_cheque' name='banco_cheque' value='"+banco+
						"'>"+nombanco;
						

	var newcell3 = newrow.insertCell(3);
	newcell3.className="td4";
	newcell3.align="right";
	newcell3.innerHTML ="<input type='hidden' id='folio_cheque' name='folio_cheque' value='"+folio_cheque+
						"'>"+addCommas(TotalDcheque);

						

}

function objfacturaDProv(folio_cheque ,Proveedor , TotalDcheque, banco, nombanco)
{  
	this.folio_cheque = folio_cheque;
	this.Proveedor = Proveedor;
	this.TotalDcheque = TotalDcheque;
	this.banco= banco;
	this.nnombanco= nombanco;
	this.selected = false;
 }

function doctoSelected(id_doc)
{
	
	var index = getIndexDoct(id_doc);
	if(st_Cheques[index]['selected']==true)
		st_Cheques[index]['selected']=false;
	else
		st_Cheques[index]['selected']=true;
	calcularTotalCheque();
}

//funcion que busca un numero de factura
function getIndexDoct(id_doc)
{
	var retval=false;
	var index=-1;
	for(i=0;i<st_Cheques.length && retval==false;i++)
	{
		//alert(st_Cheques[i]['numfact']+"=="+numfact);
		if(id_doc==st_Cheques[i]['folio_cheque'])
		{	retval=true;
			index=i;
		}
	}	
	return index;
}

function calcularTotalCheque()
{
	
	var totalDocSelecteds=0.00;
	for(i=0;i<st_Cheques.length;i++)
	{
		//alert("Selected "+st_Cheques[i]['selected']);
		if(st_Cheques[i]['selected']==true)
		{
			
			totalDocSelecteds += parseFloat(st_Cheques[i]['TotalDcheque']);
		}
	}
	
	$('totcheque').value= addCommas(totalDocSelecteds.toFixed(2));
}


function seleccionarODescTodos()
{
	var checked_status = $('selesctodos').checked
	//$j("input[type='checkbox']:not([disabled='disabled'])").attr('checked', true);
	
	for( var index=0;index< countFacturasdProv ;index++)
	{	
		st_Cheques[index]['selected']=checked_status;
	}

	calcularTotalCheque();
	
	$j("input[name=selectToPago]").each(function()
	{
		this.checked = checked_status;
	});

}

function validaFechaImpresionCheques(myobject)
{
	
	feccompleta=myobject.value;
	var datas = {
					fechaimpresion		: 		feccompleta,
    			};
	
	var salir=false;
	jQuery.ajax({
				type:     'post',
				cache:    false,
				dataType: "json",
				url:      'php_ajax/datediff_function.php',
				data:     datas,
				success: function(resp) 
				{
					console.log(resp)
					if(resp!=null)
					{
						if(resp.numerror>0)
						{
							alert(resp.error);
							myobject.select();
							myobject.focus();
							salir=true;
						}
					}
				}
			});
	return salir;
}



function reimprimeCheques()
{
	//var folio_ImpCheque=$('folio').value;//0 todos los cheques
	
	if( !revisa_DocSeleccionados() )
	{
		alert("No ha seleccionado al menos un cheque para su impresi�n.");
		return false;
	}

	if(!confirm("Esta seguro desea imprimir los siguientes cheques."))
	{
		return false;
	}
	
	var salir=false;
	var fechaemision=$j("input[name^=fecha_cheque]").val();

	if(salir)
		return false;
	//return false;
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					prods		: 		st_Cheques,
					qfechaemision: fechaemision
    			};
	jQuery.ajax({
				type:     'post',
				cache:    false,
				//dataType: "json",
				url:      'php_ajax/reimpresionchequesCreaPDFS.php',
				data:     datas,
				/*error: function(request,error){
                	alert(error);
					alert(request);
            	},*/
				success: function(request) 
				{
				
					console.log(request)
					if(request!=null)
					{
						if(request.fallo==false)
						{	document.getElementById('totcheque').value="0.0";
							document.getElementById('num_chUltimo').value="";
							document.getElementById('banco').value="";
							document.getElementById('selesctodos').checked=false;
							myRef = window.open('php_ajax/impresionAutomatica.php?FILER='+ request.FILE,'archivo');
						}
						else
						{
							alert( "Error en proceso de actualizacion!." + request.msgerror );
						}
					
					}
				}
			});
}

function buscarChequesReemprimir()
{
	var bancoq = $('banco').value;
	//alert(bancoq);
	var datas = {
					banco : bancoq
    			};
				
	jQuery.ajax({
				async:          true,
				type:     'post',
				cache:    false,
				url:      'php_ajax/buscarChequesReempresion.php',
				dataType: "json",
				data:     datas,
				success: function(source)
					{
						console.log(source);
						if(source!=null)
						{
							var Table = $('resultados');
							for(var i = Table.rows.length-1; i >=0; i--)
							{
								Table.deleteRow(i);
							}
							if(source.length>0)
							{
								
								for(var i=0;i<source.length;i++)
								{
									agregar_ChequesATablaResultados_Reimpresion(source[i].num_ch, source[i].folio_cheque, source[i].Proveedor , 
																							source[i].TotalDcheque, source[i].banco, source[i].concepto, source[i].fpago);
									//alert(source[i].folio_cheque);
									//alert(source[i].Proveedor);
									//alert(source[i].TotalDcheque);
									//alert(source[i].banco);
									//alert(source[i].nombanco);
									st_Cheques[countFacturasdProv]= new  objfacturaDProv(source[i].folio_cheque, source[i].Proveedor , 
																							source[i].TotalDcheque , source[i].banco, source[i].nombanco);
									countFacturasdProv++;
									//$('numbanco').focus();
								}
							}
							else
							{
								
								document.getElementById('totcheque').value="0.0";
								document.getElementById('num_chUltimo').value="";
								alert("No existen cheques a imprimir para el banco seleccionado.");
							}
						}
						else
						{
							alert("No obtuvo resultados!.");
						}
					}
				});
}

function agregar_ChequesATablaResultados_Reimpresion(num_ch, folio_cheque ,Proveedor , TotalDcheque,banco, nombanco, fpago)
{
	var Table = $('resultados');
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);

/*	var newcell1 = newrow.insertCell(0);
	newcell1.className="td1";
	newcell1.innerHTML ="<input type='checkbox' style='width: 10px;' id='selectToPago' name='selectToPago' onClick='doctoSelected("+folio_cheque+")' tabindex='-1'>";
	*/
	var newcell3 = newrow.insertCell(0);
	newcell3.className="td1";
	newcell3.align="center";
	newcell3.innerHTML ="<input type='hidden' id='folio_cheque' name='folio_cheque' value='"+folio_cheque+
						"'>"+addCommas(TotalDcheque);
	var newcell9 = newrow.insertCell(1);
	newcell9.className="td2";
	newcell9.style.width ="200px";
	newcell9.innerHTML ="<a class='texto8' >"+num_ch+"</a>";
	
	var newcell2 = newrow.insertCell(2);
	newcell2.className="td2";
	newcell2.style.width ="200px";
	newcell2.innerHTML ="<a class='texto8' >"+Proveedor+"</a>";

	var newcell4 = newrow.insertCell(3);
	newcell4.className="td3";
	newcell4.style.width ="200px";
	newcell4.innerHTML ="<input type='hidden' id='banco_cheque' name='banco_cheque' value='"+banco+
						"'>"+nombanco;
						
/*	var newcell4 = newrow.insertCell(3);
	newcell4.className="td3";
	newcell4.style.width ="100px";
	newcell4.align="center";
	newcell4.innerHTML =fpago;//"<input type='text' style='text-align:center;  font-size:10px;' id='fecha_cheque' name='fecha_cheque' value='"+fpago+	"' >";//onBlur='javascript: validaFechaImpresionCheques(this)'					
*/


	
}

function imprimeSolicitudes()
{
	if( !revisa_DocSeleccionados() )
	{
		alert("No ha seleccionado al menos un solicitud para su impresi�n.");
		return false;
	}

	if(!confirm("�Esta seguro desea imprimir las siguientes solicitudes?"))
	{
		return false;
	}
	
	
	
	var salir=false;
	var fechaemision=$j("input[name^=fecha_cheque]").val();

	if(salir)
		return false;
	//return false;
	//Valida que no se haya usado el numero de cheque que se intenta imprimir en algun otro cheque
	var datas = {
					prods		: 		st_Cheques,
					qfechaemision: fechaemision,
					tipoDoc : 20
    			};//folio_ImpCheque : folio_ImpCheque,

	
	jQuery.ajax({
				type:     'post',
				cache:    false,
				dataType: "json",
				url:      'php_ajax/crear_solicitudesPagoElecpdf.php',
				data:     datas,
				success: function(request) 
				{
				
					console.log(request)
					if(request!=null)
					{
						if(request.fallo==false)
						{	document.getElementById('totcheque').value="0.0";
							document.getElementById('num_chUltimo').value="";
							document.getElementById('banco').value="";
							document.getElementById('selesctodos').checked=false;
							myRef = window.open('php_ajax/impresionAutomatica.php?FILER='+ request.FILE,'archivo');
							
						}
						else
						{
							alert( "Error en proceso de actualizacion!." + request.msgerror );
						}
					
					}
				}
			});

	
}

function cancelaProgPago(folio_cheque)
{
	var datas = {
					folio		: 		folio_cheque
    			};//folio_ImpCheque : folio_ImpCheque,

	
	alert(folio_cheque);
	jQuery.ajax({
				type:     'post',
				cache:    false,
				dataType: "json",
				url:      'php_ajax/cancelaProgramacionPagos.php',
				data:     datas,
				success: function(request) 
				{
				
					console.log(request)
					if(request=="OK")
					{
						alert("Lam programacion de pago ha sido cancelada.");
					}
				}
			});
}