<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$prov = 0;
$iva = 0;
$subtotal = 0;
//$usuario = $_COOKIE['ID_my_site'];
//$depto = $_COOKIE['depto'];
$usuario='001349';
$depto='1232';

if(isset($_REQUEST['numprov']))
	$prov = $_REQUEST['numprov'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];
	
$folio =0;
if(isset($_REQUEST['folio']))
	$folio = $_REQUEST['folio'];
		
$contrato ="";
if(isset($_REQUEST['contrato']))
	$contrato = $_REQUEST['contrato'];
		
$tipoestima =0;
if(isset($_REQUEST['tipoestima']))
	$tipoestima = $_REQUEST['tipoestima'];
		
$numestima =0;
if(isset($_REQUEST['numestima']))
	$numestima = $_REQUEST['numestima'];
		
$tiporecurso =0;
if(isset($_REQUEST['tiporecurso']))
	$tiporecurso = $_REQUEST['tiporecurso'];
		
$tipbien =0;
if(isset($_REQUEST['tipobien']))
	$tipobien = $_REQUEST['tipobien'];
	
$importe =0;
if(isset($_REQUEST['importe']))
	$importe = $_REQUEST['importe'];
	
$iva =0;
if(isset($_REQUEST['iva']))
	$iva = $_REQUEST['iva'];

$neto =0;
if(isset($_REQUEST['neto']))
	$neto = $_REQUEST['neto'];

?>	
	


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/combustible_fun.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>
<form name="form1" method="post" action="">

<table width="95%" border="0">
  <tr>
    <th scope="row"><div align="left" class="TituloDForma">Consulta de Solicitud de Pago de Obra Publica </div>
        <hr class="hrTitForma"></th>
  </tr>
</table>
<table width="89%" align="center" height="34"  border="0">
  <tr>
    <th width="12%" height="28" class="texto10" align="center" scope="row">Busqueda<span class="texto8 Estilo1">
      <input class="texto8" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
</span></th>
    <td width="21%"><select name="buscar" id="buscar" onChange="Div_visible()" tabindex="1">
      <option value=1>Folio</option>
      <option value=2>Proveedor</option>
      <option value=3>Rango de Fechas</option>
    </select></td>
    <td width="58%">&nbsp;</td>
    <td width="9%"><div align="left" id="search" name="search" style="z-index:1; visibility:hidden; position:absolute; width:402px; top: 48px; left: 350px; height: 24px;">
      <input class="texto8" type="text" id="provname1"  name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="2"  value="<?php echo $nombre;?>">
      <div id="search_suggestProv" style="z-index:2;"> </div>
    </div>
	  <div align="left" id="fol" name="fol" style="visibility:visible; position:absolute; width:auto; left: 350px; top: 48px;">
      	<input type="text" name="folio" id="folio" size="10" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)">
	  </div>
	  <div align="left" id="fecha" name="fecha" style="visibility:hidden; position:absolute; width:402px; left: 350px; top: 48px;">Del 
				 Fecha Inicial:
                    <input name="fi" type="text" size="7" id="fi" value="" class="required" maxlength="10"  style="width:70">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title="Date selector" align="absmiddle">
					<!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fi",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				
				  Fecha Final:
                    <input name="ff" type="text" size="7" id="ff" value="" class="required" maxlength="10"  style="width:70">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Date selector" align="absmiddle">
					<!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "ff",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				
	  </div>
	  <div align="right" name="boton" id="boton">
		<input type="button" name="boton" value="Busca" onClick="busca_solpago()">
		</div>
	  </td>
  </tr>
</table>

<table name="master" id="master"  width="90%" height="19%" border=2 align="center" style="vertical-align:top; visibility:hidden; border-color: #006600">
  <td width="90%" height="164" style="vertical-align:top;">
      <table width="100%" border=0 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <th width="50" align="center" class="subtituloverde">Folio</th>
          <th width="81" align="center" class="subtituloverde">Contrato</th>
          <th width="64" align="center" class="subtituloverde">Tipo Estima </th>
          <th width="67" align="center" class="subtituloverde">Num. Estima</th>
          <th width="83" align="center" class="subtituloverde">Recurso</th>
          <th width="63" align="center" class="subtituloverde">Bien</th>
          <th width="77" align="center" class="subtituloverde">Importe</th>
          <th width="78" align="center" class="subtituloverde">Iva</th>
          <th width="60" align="center" class="subtituloverde">Neto</th>
          <th width="26" align="center" class="subtituloverde">&nbsp;</th>
          <th width="27" align="center" class="subtituloverde">&nbsp;</th>
        </tr>
      </table>
      <div style="overflow:auto; height:350px; padding:0">
        <table name="ProdT" width="100%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white id="datos">
          <?php 
		
			for($i=0;$i<count($datos);$i++)
			{
		?>
            <tr><td width="7%" class="texto9" align="center"><?php echo $folio;?></td>
            <td width="11%" class="texto9" align="center"><?php echo $contrato;?></td>
            <td width="7%" class="texto9" align="center"><?php echo $tipoestima;?></td>
            <td width="6%" class="texto9" align="center"><?php echo $numestima;?></td>
            <td width="5%" class="texto9" align="center"><?php echo $tiporecurso;?></td>
            <td width="16%" class="texto9" align="center"><?php echo $tipobien;?></td>
            <td width="10%" class="texto9" align="center"><?php echo $importe;?></td>
            <td width="7%" class="texto9" align="center"><?php echo $iva;?></td>
            <td width="10%" class="texto9" align="center"><?php echo $neto;?></td>
            <td width="9%" class="texto9" align="center">
			<img src="../../imagenes/consultar.jpg" onClick="mostrarPdf()" title="Click aqui para consultar Solicitud."></td>
            <td width="4%" class="texto9" align="center"><span class="texto8"><img src="../../imagenes/eliminar.jpg" onClick="javascript: if(confirm('&iquest;Esta seguro que desea deshabilitar o borrar este proveedor?'))cancela_sol(<?php echo $datos[$i]['folio'];?>)" title="Click aqui dar de baja Solicitud."></span></td>
          </tr>
          <?php
		}
		?>
        </table>
    </div></td>
</table>
<p>&nbsp;</p>
</form>
</body>
</html>
