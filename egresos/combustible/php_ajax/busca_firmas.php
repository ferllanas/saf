<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$xdepto = $_REQUEST['xdepto'];
$datosx=array();
$fails=false;
if ($conexion)
{
	$ejecuta ="{call sp_config_c_firmas_solche(?)}";
	$variables = array(&$xdepto);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	if(!$fails)
	{
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
			$datosx[$i]['numemp']= trim($row['numemp']);
			$datosx[$i]['nomemp']= utf8_encode(trim($row['nomemp']));
			$datosx[$i]['nomdepto']= trim($row['nomdepto']);
			$i++;
//			$nomemp = $row['nomemp'];
//			$nomdepto = $row['nomdepto'];
		}
		sqlsrv_free_stmt( $R);
	}
}
echo json_encode($datosx);
?>
