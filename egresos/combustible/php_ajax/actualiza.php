<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../dompdf/dompdf_config.inc.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../connections/dbconexion.php");
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$concepto = $_REQUEST['concepto'];
$prov = $_REQUEST['prov'];
$subtotal = str_replace(',','',$_REQUEST['subtotal']);
$iva = str_replace(',','',$_REQUEST['iva']);
$total = str_replace(',','',$_REQUEST['total']);
$fi=convertirFechaEuropeoAAmericano($_REQUEST['fi']);
$ff=convertirFechaEuropeoAAmericano($_REQUEST['ff']);
$depto = $_REQUEST['depto'];
$usuario = $_REQUEST['usuario'];
$factura = $_REQUEST['factura'];
	
if ($conexion)
{
	$ejecuta ="{call sp_egresos_A_msolche_gas(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$concepto,&$prov,&$subtotal,&$iva,&$total,&$fi,&$ff,&$depto,&$usuario,&$factura);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	//echo $ejecuta;
	//print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
			
			$error = trim($row['error']);
			$datos[0]['error'] = $error;
			$folio = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$uresp = $row['uresp'];
			$nomresp = $row['nomresp'];
			$arearesp = $row['arearesp'];
			
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
		}
		sqlsrv_free_stmt( $R);
	}
}

if($error!="OK")
{
	die("Ocurrio un error: ".$error);
}

$xxprods=array();
if(isset($_REQUEST['prods']))
$xxprods=$_REQUEST['prods'];
$i=0;
for($i=0;$i<count($xxprods);$i++)
{
	$eco0=$xxprods[$i]['eco0'];
	$numres0=$xxprods[$i]['numres0'];
	$ticket0=str_replace(',','',$xxprods[$i]['ticket0']);
	$kms0=str_replace(',','',$xxprods[$i]['kms0']);
	$fecha0=convertirFechaEuropeoAAmericano($xxprods[$i]['fecha0']);
	$lts0=str_replace(',','',$xxprods[$i]['lts0']);
	$subtotal0=str_replace(',','',$xxprods[$i]['subtotal0']);
	$iva0=str_replace(',','',$xxprods[$i]['iva0']);	
	$total0=str_replace(',','',$xxprods[$i]['total0']);	
	$u_vehiculo0=$xxprods[$i]['u_vehiculo0'];
	$u_depto0=$xxprods[$i]['u_depto0'];
	$tipo0=$xxprods[$i]['tipo0'];
	
	$ejecuta ="{call sp_egresos_A_dsolche_gas(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$folio,&$eco0,&$numres0,&$ticket0,&$kms0,&$fecha0,
					   &$lts0,&$subtotal0,&$iva0,&$total0,&$u_vehiculo0,&$u_depto0,&$tipo0);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	if( $R === false )
	{
		 $fails= "Error in (detalle) statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
}

$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
	{
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Documento sin t&iacute;tulo</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../../prototype/prototype.js'></script>
</head>

<body>
";
/////
	$nomprov = $_REQUEST['nomprov'];
	$subtotal = $_REQUEST['subtotal'];
	$iva = $_REQUEST['iva'];
	$neto = $_REQUEST['total'];
	$nomcoord = $_REQUEST['nomcoord'];
	$nomdir = $_REQUEST['nomdir'];
	$nomfirma = $_REQUEST['nomfirma'];
	$netox = str_replace(',','',$_REQUEST['total']);
	$neto_let = num2letras($netox);
	$detalle = "Subtotal: $".$subtotal.", Iva: $".$iva.", Total: $".$neto;
/////
$html.="

<table width='600' border='0' align='center'>
   	    <tr>
		  <td width='100%'>
             <table width='100%' border='0'>
				<p></p>
				  <tr>
					<td align='left' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFPrincipal."' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                ".htmlentities("SOLICITUD DE CHEQUE - COMBUSTIBLE")." - </b></td>
					<td align='right' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFSecundaria."' width='169' height='101' /></td>
				  </tr>
		      </table> 
						<table width='100%'>
						   <tr>
							<td width='100%' align='left' class='texto9' colspan='2'><b>DIRECCION :</b> ". $nomdir ."</td><br>
						   </tr>
						   <tr>
							<td width='80%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
						  	<td width='20%' align='right' class='texto9'><b>FECHA:</b>".$fecha."</td>
						   </tr>
						</table>			
            <table width='100%' border='0'>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto12'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto10'><b>NUM. DE FACTURA:</b> ".$factura."</td>
              </tr>
            </table>	
            <table width='100%' border='0'>
              <tr>
                <td width='80%' align='left' class='texto10'><b>BENEFICIARIO:</b> ".$nomprov."</td>
                <td width='20%' align='right' class='texto10'><b>IMPORTE:</b> ".$neto."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto8'><b>CANTIDAD CON LETRA:</b>". strtoupper($neto_let) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'></tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td width='50%' align='left' class='texto7'>".$concepto."</td>
				<td width='50%' align='right' class='texto9'>".$detalle."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='30%'>&nbsp;</td>
                <td width='5%'>&nbsp;</td>
                <td width='20%'>&nbsp;</td>
                <td width='5%'>&nbsp;</td>
                <td width='40%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>_____________________________________</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>_____________________________________</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomfirma."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='20%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' class='texto8'><B>".$nomdir."</B></td>
                <td width='5%' class='texto8'>&nbsp;</td>
                <td width='20%' align='center' class='texto8'></td>
                <td width='5%' class='texto8'>&nbsp;</td>
                <td width='40%' align='center' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>              
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>

            </table>            
	 	  </td>
  	    </tr>  
	    <tr>
			<td width='100%'>
             <table width='100%' border='0'>
              <tr>
                <td align='left' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFPrincipal."' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                ".htmlentities("SOLICITUD DE CHEQUE - COMBUSTIBLE")." - </b></td>
                <td align='right' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFSecundaria."' width='169' height='101' /></td>
              </tr>
            </table>
			
			<table width='100%'>
			   <tr>
				<td width='100%' align='left' class='texto9' colspan='2'><b>DIRECCION :</b> ". $nomdir ."</td><br>
				</tr>
				<tr>
					<td width='80%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
					<td width='20%' align='right' class='texto9'><b>FECHA:</b>".$fecha."</td>
				</tr>
			</table>					
            <table width='100%' border='0'>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto12'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto10'><b>NUM. DE FACTURA:</B> ".$factura."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='80%' align='left' class='texto10'><B>BENEFICIARIO:</B> ".$nomprov."</td>
                <td width='20%' align='right' class='texto10'><b>IMPORTE:</b> ".$neto."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto10'><b>CANTIDAD CON LETRA:</b>". strtoupper($neto_let) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td width='50%' align='left' class='texto7'>".$concepto."</td>
				<td width='50%' align='right' class='texto9'>".$detalle."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='30%'>&nbsp;</td>
                <td width='5%'>&nbsp;</td>
                <td width='20%'>&nbsp;</td>
                <td width='5%'>&nbsp;</td>
                <td width='40%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>_____________________________________</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>_____________________________________</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomfirma."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='20%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' class='texto8'><B>".$nomdir."</B></td>
                <td width='5%' class='texto8'>&nbsp;</td>
                <td width='20%' align='center' class='texto8'></td>
                <td width='5%' class='texto8'>&nbsp;</td>
                <td width='40%' align='center' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>
              
              <tr>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'></td>
             </tr>
            </table>
	 	  </td>
  	    </tr>  
	  </table>
	</body>
</html>";	

  $dompdf = new DOMPDF();
	
	$dompdf->set_paper('"letter","portrait"');
	
	$dompdf->load_html($html);
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../../../cheques/pdf_files/$folio.pdf", $pdf);

	$datos=array();
	$mdatos=array();
	$resguarda='';
	$uvehiculo='';
	$noeco='';
	$depa='';
	$depax='';
	$nombre='';
	$prov='';
	$cvedepto="";

	//$usuario='001349';
	//$depto='1232';	
	
	//$folio=9;
	//$folio = $_REQUEST['folio'];
	
	$fecha_aut=date("d/m/Y");
	$hora = getdate(time());
	$hora_act=( $hora["hours"] . ":" . $hora["minutes"] . ":" . $hora["seconds"] ); 

if ($conexion)
{
	$consulta = "select a.folio,a.concepto,a.prov,a.subtotal,a.iva,a.total,convert(varchar(12),a.fecini,103)";
	$consulta .= " as fecini,convert(varchar(12),a.fecfin,103) as fecfin,b.nomprov,c.factura,c.depto from ";
	$consulta .= "egresosmsolchegas a left join compramprovs b on a.prov=b.prov left join egresosmsolche c ";
	$consulta .= "on a.folio=c.folio where a.folio=$folio and a.estatus<9000";
	
	//die( $consulta);
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
		$concepto=$mdatos[$i]['concepto']= trim($row['concepto']);
		$nombre=$mdatos[$i]['nomprov']= trim($row['nomprov']);
		$subtotal= number_format($mdatos[$i]['subtotal']= trim($row['subtotal']),2);
		$iva=number_format($mdatos[$i]['iva']= trim($row['iva']),2);
		$total=number_format($mdatos[$i]['total']= trim($row['total']),2);
		$fecini=$mdatos[$i]['fecini']= trim($row['fecini']);
		$fecfin=$mdatos[$i]['fecfin']= trim($row['fecfin']);
		$factura=$mdatos[$i]['factura']= trim($row['factura']);
		$depto=$mdatos[$i]['depto']= trim($row['depto']);
		$i++;
		}
}


$consulta = "select a.numeco,a.ticket,a.kms,convert(varchar(12),a.fecha,103) as fecha,a.litros,a.subtotal,a.iva,a.total,";
$consulta .= "a.numemp,a.deptoemp,a.respbien,Rtrim(b.nombre)+' '+Rtrim(b.appat)+' '+Rtrim(b.apmat) as nomresp ";
$consulta .= "from egresosdsolchegas a left join nomemp.dbo.nominadempleados b on a.respbien=b.numemp collate database_default where a.folio=$folio and ";
$consulta .= "a.estatus<9000";
$R = sqlsrv_query( $conexion,$consulta);
$i=0;
while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['numeco']= trim($row['numeco']);
		$datos[$i]['ticket']= trim($row['ticket']);
		$datos[$i]['kms']= $row['kms'];
		$datos[$i]['fecha']= $row['fecha'];
		$datos[$i]['litros']= $row['litros'];
		$datos[$i]['subtotal']= $row['subtotal'];
		$datos[$i]['iva']= $row['iva'];
		$datos[$i]['total']= $row['total'];
		$datos[$i]['numemp']= $row['numemp'];
		$datos[$i]['deptoemp']= $row['deptoemp'];
		$datos[$i]['respbien']= $row['respbien'];
		$datos[$i]['nomresp']= trim($row['nomresp']);
		$i++;
	}

$dompdf = new DOMPDF();

$html="
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>

<head>
	<style type='text/css'>
	body 
	{
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Documento sin t&iacute;tulo</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../../prototype/prototype.js'></script>
</head>


<body>
			<table width='100%' align='center' height='48' border='0'>
			  <tr>
					<td align='left' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFPrincipal."' width='200' height='76' /></td>
                <td class='texto9' align='center'><b><BR />
                </b></td>
					<td align='right' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFSecundaria."' width='169' height='101' /></td>
			  </tr>
			</table>

<p>&nbsp;</p>
<table width='100%' border='0'>
  <tr>
    <th width='9%' class='texto9' scope='row'><div align='right'>Beneficiario</div></th>
    <td width='28%'>".$nombre."</td>
    <td width='8%' class='texto9'><div align='Right'><strong>No. Factura</strong></div></td>
    <td width='10%'>".$factura."</td>
    <td width='7%' class='texto9'><strong>Subtotal</strong></td>
    <td width='10%'>".$subtotal."</td>
    <td width='3%' class='texto9'><div align='center'><strong>Iva</strong></div></td>
    <td width='10%'>".$iva."</td>
    <td width='5%' class='texto9'><strong>Total</strong><span class='texto10'><span class='texto8'></td>
    <td width='10%'>".$total."</td>
  </tr>
  <tr>
    <th rowspan='2' class='texto9' scope='row'><div align='right'>Concepto</div></th>
    <td class='texto8' rowspan='2'>".$concepto."</td>
    <td colspan='2' rowspan='2' align='right' class='texto9'><div align='center'><strong>Periodo de Consumo</strong></div></td>
    <td rowspan='2' class='texto9'><div align='center'><strong>Del</strong></div></td>
    <td rowspan='2'>".$fecini."</td>
    <td rowspan='2' class='texto9'><div align='center'><strong>Al</strong></div></td>
    <td rowspan='2' >".$fecfin."</td>
  </tr>
  <tr>
    <td class='texto9'>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>	  <hr class='hrTitForma'>	</p>
  <table name='datos' id='datos' width='87%' height='21%' border=1 align='center'>
	  <tr  bgcolor='#CCCCCC'>
		<td width='49' align='center' class='texto8'><strong># Eco</strong></td>
		<td width='53' align='center' class='texto8'><strong># Ticket</strong></td>
		<td width='41' align='center' class='texto8'><strong>Kms</strong></td>
		<td width='51' align='center' class='texto8'><strong>Fecha</strong></td>
		<td width='39' align='center' class='texto8'><strong>Litros</strong></td>
		<td width='56' align='center' class='texto8'><strong>Importe</strong></td>
		<td width='38' align='center' class='texto8'><strong>Iva</strong></td>
		<td width='55' align='center' class='texto8'><strong>Total</strong></td>
		<td width='56' align='center' class='texto8'><strong>Usuario</strong></td>
		<td width='42' align='center' class='texto8'><strong>Depto</strong></td>
		<td width='223' align='center' class='texto8'><strong>Resguardante</strong></td>
	  </tr>
";
		for($i=0;$i<count($datos);$i++)
		{
$html.="
    <tr>
      <td width='49' align='center' class='texto8'>".$datos[$i]['numeco']."</td>
      <td width='53' align='center' class='texto8'>".$datos[$i]['ticket']."</td>
      <td width='41' align='center' class='texto8'>".$datos[$i]['kms']."</td>
      <td width='49' align='center' class='texto8'>".$datos[$i]['fecha']."</td>
      <td width='39' align='right' class='texto8'>".number_format($datos[$i]['litros'],2)."</td>
      <td width='56' align='right' class='texto8'>".number_format($datos[$i]['subtotal'],2)."</td>
      <td width='38' align='right' class='texto8'>".number_format($datos[$i]['iva'],2)."</td>
      <td width='55' align='right' class='texto8'>".number_format($datos[$i]['total'],2)."</td>
      <td width='56' align='center' class='texto8'>".$datos[$i]['numemp']."</td>
      <td width='43' align='center' class='texto8'>".$datos[$i]['deptoemp']."</td>
      <td width='224' align='left' class='texto8'>".$datos[$i]['respbien'].' - '.$datos[$i]['nomresp']."</td>
    </tr>
";
		}
$html.="
  </table>

</body>
</html>";
	$dompdf->set_paper("letter","landscape");
	
	$dompdf->load_html($html);
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../../../cheques/pdf_files/detalle_".$folio.".pdf", $pdf);
	
echo json_encode($folio);
///////////////////////////////////////////////////////////////

?>