<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$num_fir = $_REQUEST['num_fir'];
$datos0=array();

if ($conexion)
{		
	
	if(strlen($num_fir)>0)
	{
		$consulta = "select a.numemp,b.nombre,b.appat,b.apmat,c.nomdepto from configdfirmas_solche a left join nomemp.dbo.nominadempleados b on a.numemp=b.numemp collate database_default left join nomemp.dbo.nominamdepto c on b.depto collate database_default = c.depto collate database_default where a.numemp=$num_fir";
	}
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos0[$i]['nomemp']= utf8_decode(trim($row['nombre']).' '.trim($row['appat']).' '.trim($row['apmat']));
			$datos0[$i]['nomdepto']= utf8_decode(trim($row['nomdepto']));
			$i++;
		}
	}
}
echo json_encode($datos0);
?>