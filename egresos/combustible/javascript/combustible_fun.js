
var $j = jQuery.noConflict();

$j( document ).ready(function() {
	console.log( "ready!" );
	var $boletasstr= $j.cookie("boletasGasolina");
	
	if($boletasstr!=undefined)
	{
		var boletas=new Array();
		boletas=eval($j.parseJSON($j.cookie("boletasGasolina")));
		
		console.log(boletas);
		for(var $i=0;$i<boletas.length;$i++)
		{
			console.log($i);
			console.log(boletas[$i]);
			console.log(boletas[$i].factura);
			agregarATabla(boletas[$i].inprov, 			boletas[$i].infactura, 		boletas[$i].insubtotal, boletas[$i].iniva, 
							boletas[$i].intotal, 		boletas[$i].inconcepto,		boletas[$i].infi, 		boletas[$i].inff, 
							boletas[$i].incvedepto, 	boletas[$i].ineco, 			boletas[$i].innoeco, 	boletas[$i].inticket, 
							boletas[$i].inkms, 			boletas[$i].infecha,  		boletas[$i].inlts, 		boletas[$i].inimporte, 
							boletas[$i].inusu_vehiculo, boletas[$i].inusu_depto, 	boletas[$i].inuvehiculo, 
							boletas[$i].indepa, 		boletas[$i].indepax, 		boletas[$i].innomres, 	boletas[$i].innumres,
							boletas[$i].inivad,			boletas[$i].intotald, 		boletas[$i].intipo)
		}
	}
});

function deshabilitar()
{
	document.getElementById('guardar').disabled=true;
}


function val_fecha()
{
	var fi =document.getElementById('fi').value;
	var ff =document.getElementById('ff').value;
	var fecha = document.getElementById('fecha').value;

	fi=parseInt(fi.substr(6,4)+fi.substr(3,2)+fi.substr(0,2));
	ff=parseInt(ff.substr(6,4)+ff.substr(3,2)+ff.substr(0,2));
	fecha=parseInt(fecha.substr(6,4)+fecha.substr(3,2)+fecha.substr(0,2));
	if(fecha<fi || fecha>ff)
	{
		alert("Fecha fuera de Rango, Capture solo fechas permitidas dentro del periodo");
		document.getElementById('lts').value='';
		document.getElementById('fecha').value='';
		document.getElementById('fecha').focus();
		return false;
	}
}

function valida_factura()
{
	var prov = document.getElementById('numprov').value;
	var factura = document.getElementById("factura").value;
	if(factura.length<=0)
	{
		alert ("Capturar Num. factura");return false;
	}
	//alert('php_ajax/busca_factura.php?factura='+factura+"&prov="+prov);
	new Ajax.Request('php_ajax/busca_factura.php?factura='+factura+"&prov="+prov,
	{onSuccess : function(resp) 
		{
			//alert(resp.responseText);
			if( resp.responseText )
			{
				var myArray = eval(resp.responseText);
				//alert("22 "+resp.responseText);
				if(myArray.length>0)
				{
					alert ("Num. de Factura no valida, ya existe con otro proveedor");
					document.getElementById("factura").value='';
					deshabilitar();
					return false
				}
				else
					return true;
			}
		}
	});
	//////////////////////////////
}

function valida_ticket()
{
	var ticket=document.getElementById('ticket').value;
	var tabla= document.getElementById('datos');
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="ticket1")
				{
					xticket=mynode.value;
					if (xticket==ticket)
						alert("Ticket No. " +document.getElementById('ticket').value+ " se ecuentra capturado.");
						return false;
				}
			}
		}
	}
	document.getElementById('fecha').value = document.getElementById('fi').value;
}

////////////// No. de Renglon en una tabla
function whichRow ( obj,eco )
{
	var par = obj . parentNode ;
	while( par . nodeName . toLowerCase ()!= 'tr' )
	{
		par = par . parentNode ;
	}
	alert ( par . rowIndex );
	alert(eco);
}
//////////////


function agregar()
{
	var prov=document.getElementById('numprov').value;
	var factura=document.getElementById('factura').value;
	var subtotal=document.getElementById('subtotal').value;
	var iva=document.getElementById('iva').value;
	var total=document.getElementById('total').value;
	var concepto=document.getElementById('concepto').value;
	var fi=document.getElementById('fi').value;
	var ff=document.getElementById('ff').value;
	var cvedepto=document.getElementById('cvedepto').value;
	
	var eco=document.getElementById('eco').value;
	var noeco=document.getElementById('noeco').value;
	var ticket=document.getElementById('ticket').value;
	var kms=document.getElementById('kms').value;
	var fecha=document.getElementById('fecha').value;
	var lts=document.getElementById('lts').value;
	var importe=document.getElementById('importe').value;
	var usu_vehiculo=document.getElementById('usu_vehiculo').value;
	var usu_depto=document.getElementById('usu_depto').value;
	var uvehiculo=document.getElementById('uvehiculo').value;
	var depa=document.getElementById('depa').value;
	var depax=document.getElementById('depax').value;
	var nomres=document.getElementById('nomres').value;
	var numres=document.getElementById('numres').value;
	var ivad=document.getElementById('ivad').value;
	var totald=document.getElementById('totald').value;
	var tipo=document.getElementById('tipo').value;
	

	
	if(prov.length<1)
	{
		alert("Falta capturar proveedor");
		document.getElementById('provname1').focus();
		return false;
	}
	if(factura.length<1)
	{
		alert("Falta capturar No. de Factura");
		document.getElementById('factura').focus();
		return false;
	}
	if(subtotal.length<1)
	{
		alert("Falta capturar Subtotal");
		document.getElementById('subtotal').focus();
		return false;
	}
	if(iva.length<1)
	{
		alert("Falta capturar Iva");
		document.getElementById('iva').focus();
		return false;
	}
	if(total.length<1)
	{
		alert("Falta capturar Total");
		document.getElementById('btotal').focus();
		return false;
	}
	if(concepto.length<1)
	{
		alert("Falta capturar Concepto");
		document.getElementById('concepto').focus();
		return false;
	}
	if(fi.length<1)
	{
		alert("Falta capturar Fecha Inicial");
		document.getElementById('fi').focus();
		return false;
	}
	if(ff.length<1)
	{
		alert("Falta capturar Fecha Inicial");
		document.getElementById('ff').focus();
		return false;
	}
	if(cvedepto.length<1)
	{
		alert("Falta capturar Departamento");
		document.getElementById('cvedepto').focus();
		return false;
	}

//////////////////////////////////////////
	if(noeco<=0)
	{
		if(noeco.length<1)
			{
			alert("Falta capturar No. Economico");
			document.getElementById('noeco').focus();
			return false;
			}
	}
	if(ticket.length<1)
		{
		alert("Falta capturar Ticket");
		document.getElementById('ticket').focus();
		return false;
		}
	if(kms.length<1)
		{
		alert("Falta capturar Kilometraje");
		document.getElementById('kms').focus();
		return false;
		}
	if(fecha.length<1)
		{
		alert("Falta capturar Fecha");
		document.getElementById('fecha').focus();
		return false;
		}
	if(lts.length<1)
		{
		alert("Falta capturar Litros");
		document.getElementById('lts').focus();
		return false;
		}
	if(importe.length<1)
		{
		alert("Falta capturar Importe");
		document.getElementById('importe').focus();
		return false;
		}
	if(usu_vehiculo.length<1)
		{
		alert("Falta capturar No. Usuario");
		document.getElementById('uvehiculo').focus();
		return false;
		}

/*	var val_fac = parseFloat(document.getElementById('total').value.replace(/,/g,''));
	var val_tot = parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) + parseFloat( totald.replace(/,/g,''));

	if(val_tot > val_fac )
	{
		alert("Error, Importe total de Tickets es mayor que el Total de la Factura");
		return false;
	}*/
	actualiza_totales();
//////////////////////////////////////////////////////
	var tabla= document.getElementById('datos');
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="ticket1")
				{
					xticket=mynode.value;
					if (xticket==ticket)
						return;
				}
			}
		}
	}

	agregarATabla(prov,factura,subtotal,iva,total,concepto,fi,ff,cvedepto,eco,noeco,ticket,kms,fecha,lts,importe,usu_vehiculo,usu_depto,uvehiculo,depa,depax,nomres,numres,ivad,totald,tipo);
	
	limpia();
	document.getElementById('guardar').disabled=false;
	
	var boletas=new Array();
	var $boletasstr= $j.cookie("boletasGasolina");
	if($boletasstr!=undefined)
	{
		boletas=eval($j.parseJSON($j.cookie("boletasGasolina")));
	}
		boletas.push({	inprov:prov,
						infactura:factura,
						insubtotal:subtotal,
						iniva:iva,
						intotal:total,
						inconcepto:concepto,
						infi:fi,
						inff:ff,
						incvedepto:cvedepto,
						ineco:eco,
						innoeco:noeco,
						inticket:ticket,
						inkms:kms,
						infecha:fecha,
						inlts:lts,
						inimporte:importe,
						inusu_vehiculo:usu_vehiculo,
						inusu_depto:usu_depto,
						inuvehiculo:uvehiculo,
						indepa:depa,
						indepax:depax,
						innomres:nomres,
						innumres:numres,
						inivad:ivad,
						intotald:totald,
						intipo:tipo});
		
	
	$j.cookie("boletasGasolina", JSON.stringify(boletas));
	//actualiza_totales();
}



function agregarATabla(prov,factura,subtotal,iva,total,concepto,fi,ff,cvedepto,eco,noeco,ticket,kms,fecha,lts,importe,usu_vehiculo,usu_depto,uvehiculo,depa,depax,nomres,numres,ivad,totald,tipo)
{
	//////////////////////////////////////////////////////////
	var tabla = document.getElementById('datos');
	var numren = tabla.rows.length;

	var newrow = tabla.insertRow(numren);
	newrow.className ="fila";
	newrow.scope="row";
	
	var newcell = newrow.insertCell(0); //insert new cell to row
	newcell.width="61";
	newcell.className ="texto8";
	newcell.innerHTML =  '<input name="eco1" id="eco1" style="width:65px" readonly value="'+eco+'" onclick="editar(this)" >';
	newcell.align ="center";
	
	var newcell1 = newrow.insertCell(1);
	newcell1.width="43";
	newcell1.className ="texto8";
	newcell1.innerHTML = '<input name="ticket1" id="ticket1" style="width:70px" readonly onclick="editar(this)" value="'+ticket+'">';
	newcell1.align ="center";

	var newcell2 = newrow.insertCell(2);
	newcell2.width="80";
	newcell2.className ="texto8";
	newcell2.innerHTML =  '<input name="kms1" id="kms1" style="width:73px" readonly onclick="editar(this)" value="'+kms+'">';
	newcell2.align ="center"; 
	
	var newcell3 = newrow.insertCell(3);
	newcell3.width="63";
	newcell3.className ="texto8";
	newcell3.innerHTML =  '<input name="fecha1" id="fecha1" style="width:69px" readonly onclick="editar(this)" value="'+fecha+'" >';
	newcell3.align ="center"; 

	var newcell4 = newrow.insertCell(4);
	newcell4.width="44";
	newcell4.className ="texto8";
	newcell4.innerHTML =  '<input name="lts1" id="lts1" style="width:53px" readonly onclick="editar(this)" value="'+lts+'" >';
	newcell4.align ="center";

	var newcell5 = newrow.insertCell(5);
	newcell5.width="7%";
	newcell5.className ="texto8";
	newcell5.innerHTML =  '<input name="importe1" id="importe1" readonly  style="width:70px" onclick="editar(this)" value="'+importe+'" >';
	newcell5.align ="center"; 
	document.getElementById('subtotalgen').value = parseFloat(document.getElementById('subtotalgen').value.replace(/,/g,'')) + parseFloat( document.getElementById('importe').value.replace(/,/g,''));
	document.getElementById('subtotalgen').value=Math.round(document.getElementById('subtotalgen').value*100)/100;
	document.getElementById('subtotalgen').value=NumberFormat(document.getElementById('subtotalgen').value, '2', '.', ',');

	var newcell6 = newrow.insertCell(6);
	newcell6.width="6%";
	newcell6.className ="texto8";
	newcell6.innerHTML =  '<input name="iva1" id="iva1" readonly  style="width:70px" onclick="editar(this)" value="'+ivad+'" >';
	newcell6.align ="center"; 
	document.getElementById('ivagen').value = parseFloat(document.getElementById('ivagen').value.replace(/,/g,'')) + parseFloat( document.getElementById('ivad').value.replace(/,/g,''));
	document.getElementById('ivagen').value=Math.round(document.getElementById('ivagen').value*100)/100;
	document.getElementById('ivagen').value=NumberFormat(document.getElementById('ivagen').value, '2', '.', ',');

	var newcell7 = newrow.insertCell(7);
	newcell7.width="6%";
	newcell7.className ="texto8";
	newcell7.innerHTML =  '<input name="total1" id="total1" readonly  style="width:70px" onclick="editar(this)" value="'+totald+'" >';
	newcell7.align ="center"; 
	document.getElementById('totalgen').value = parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) + parseFloat( document.getElementById('totald').value.replace(/,/g,''));
	document.getElementById('totalgen').value=Math.round(document.getElementById('totalgen').value*100)/100;
	document.getElementById('totalgen').value=NumberFormat(document.getElementById('totalgen').value, '2', '.', ',');

///////////*///////////////////
	var newcell8 = newrow.insertCell(8);
	newcell8.width="6%";
	newcell8.className ="texto8";
	newcell8.innerHTML = '<input name="usu_vehiculo1" id="usu_vehiculo1" readonly style="width:66px" onclick="editar(this)" value="'+usu_vehiculo+'">';
	newcell8.align ="center";

	var newcell9 = newrow.insertCell(9);
	newcell9.width="50";
	newcell9.className ="texto8";
	newcell9.innerHTML = '<input name="usu_depto1" id="usu_depto1" readonly style="width:60px" type="text" onclick="editar(this)" readonly value="'+usu_depto+'" >';
	newcell9.align ="center"; 

	var newcell10 = newrow.insertCell(10);
	newcell10.width="100";
	newcell10.align ="center"; 
	newcell10.innerHTML = '<input name="nomres" id="nomres" readonly style="width:370px" onclick="editar(this)" type="text" value="'+nomres+'">';

	var newcell11 = newrow.insertCell(11);
	newcell11.width="25";
	newcell11.className ="texto8";
	var ticket2 = ticket;
	var importe2 = importe;
	var iva2 = ivad;
	var totald2 = totald;
	newcell11.innerHTML = '<img src="../../imagenes/eliminar.jpg" onClick="borrar(this,\''+ticket2+'\',\''+importe2+'\',\''+iva2+'\',\''+totald2+'\')">' ;
	newcell1.align ="center";

	var newcell12 = newrow.insertCell(12);
	newcell12.width="1";
	newcell12.className ="texto8";
	newcell12.innerHTML = '<input name="noeco1" id="noeco1" readonly type="hidden" onclick="editar(this)" value="'+noeco+'">';
	newcell12.align ="center";

	var newcell13 = newrow.insertCell(13);
	newcell13.width="1";
	newcell13.className ="texto8";
	newcell13.innerHTML = '<input name="depa1" id="depa1" readonly type="hidden" onclick="editar(this)" value="'+depa+'">';
	newcell13.align ="center";

	var newcell14 = newrow.insertCell(14);
	newcell14.width="1";
	newcell14.className ="texto8";
	newcell14.innerHTML = '<input name="depax1" id="depax1" readonly type="hidden" onclick="editar(this)" value="'+depax+'">';
	newcell14.align ="center";

	var newcell15 = newrow.insertCell(15);
	newcell15.width="1";
	newcell15.className ="texto8";
	newcell15.innerHTML = '<input name="uvehiculo1" id="uvehiculo1" readonly type="hidden" onclick="editar(this)" value="'+uvehiculo+'">';
	newcell15.align ="center";

	var newcell16 = newrow.insertCell(16);
	newcell16.width="1";
	newcell16.className ="texto8";
	newcell16.innerHTML = '<input name="numres1" id="numres1" readonly type="hidden" onclick="editar(this)" value="'+numres+'">';
	newcell16.align ="center";

	var newcell17 = newrow.insertCell(17);
	newcell17.width="1";
	newcell17.className ="texto8";
	newcell17.innerHTML = '<input name="tipo1" id="tipo1" readonly type="hidden" onclick="editar(this)" value="'+tipo+'">';
	newcell17.align ="center";
}

function borrar(obj,ticket2,importe2,ivad2,totald2) {
  while (obj.tagName!='TR') 
    obj = obj.parentNode;
  tab = document.getElementById('datos');
  for (i=0; ele=tab.getElementsByTagName('tr')[i]; i++)
    if (ele==obj) num=i;
/////*****/////
	document.getElementById('subtotalgen').value = parseFloat(document.getElementById('subtotalgen').value.replace(/,/g,'')) - parseFloat( importe2.replace(/,/g,''));
	document.getElementById('ivagen').value = parseFloat(document.getElementById('ivagen').value.replace(/,/g,'')) - parseFloat( ivad2.replace(/,/g,''));
	document.getElementById('totalgen').value = parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) - parseFloat( totald2.replace(/,/g,''));

	document.getElementById('subtotalgen').value=Math.round(document.getElementById('subtotalgen').value*100)/100;
	document.getElementById('subtotalgen').value=NumberFormat(document.getElementById('subtotalgen').value, '2', '.', ',');

	document.getElementById('ivagen').value=Math.round(document.getElementById('ivagen').value*100)/100;
	document.getElementById('ivagen').value=NumberFormat(document.getElementById('ivagen').value, '2', '.', ',');

	document.getElementById('totalgen').value=Math.round(document.getElementById('totalgen').value*100)/100;
	document.getElementById('totalgen').value=NumberFormat(document.getElementById('totalgen').value, '2', '.', ',');

	if (document.getElementById('totalgen').value<1)
		document.getElementById('totalgen').value=0;
		
	if (document.getElementById('ivagen').value<1)
		document.getElementById('ivagen').value=0;

////*****/////

	tab.deleteRow(num);
	actualiza_totales();
	limpia();
}

function busca_resguardo()
{
	var noecox = parseFloat( document.getElementById('noeco').value)
	if(noecox<=0)
	{
		alert("Falta capturar No. Economico");
		document.getElementById('noeco').focus();
		return false;
	}	
	
	var noeco = document.getElementById('noeco').value;
	new Ajax.Request('php_ajax/busca_res.php?noeco='+noeco,
	{onSuccess : function(resp) 
		{
			if( resp.responseText ) 
				{
					//alert(resp.responseText);
					var myArray = eval(resp.responseText);
					//document.getElementById('resguarda').value=myArray[0].resguardante;
					//
					if(myArray.length==0)
						{
						alert ("Num. Economico no valido !!!");
						document.getElementById("noeco").value='';
						document.getElementById('noeco').focus();
						return false
						}
					  else
						{
						document.getElementById('resguarda').value=myArray[0].resguardante;
						}
				}
		}
		});
}

function editar(obj)
{
	var tabla = document.getElementById('datos');
	var par = obj . parentNode ;
	while( par . nodeName . toLowerCase ()!= 'tr' )
	{
		par = par . parentNode ;
	}
	document.getElementById('ren').value=par . rowIndex;
	for (var j = 0; j <tabla.rows[par . rowIndex].cells.length;j++)
	{
		var cell = tabla.rows[par . rowIndex].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
			if(mynode.name=="eco1")
			{
				document.getElementById('eco').value = mynode.value;
			}

			if(mynode.name=="noeco1")
			{
				document.getElementById('noeco').value = mynode.value;
			}
			if(mynode.name=="ticket1")
			{
				document.getElementById('ticket').value = mynode.value;
			}
			if(mynode.name=="kms1")
			{
				document.getElementById('kms').value = mynode.value;
			}
			if(mynode.name=="fecha1")
			{
				document.getElementById('fecha').value = mynode.value;
			}
			if(mynode.name=="lts1")
			{
				document.getElementById('lts').value = mynode.value;
			}
			if(mynode.name=="importe1")
			{
				document.getElementById('importe').value = mynode.value;
			}
			if(mynode.name=="usu_vehiculo1")
			{
			document.getElementById('usu_vehiculo').value = mynode.value;
			}
			if(mynode.name=="usu_depto1")
			{
				document.getElementById('usu_depto').value = mynode.value;
			}
			if(mynode.name=="uvehiculo1")
			{
				document.getElementById('uvehiculo').value = mynode.value;
			}
			if(mynode.name=="depa1")
			{
				document.getElementById('depa').value = mynode.value;
			}
			if(mynode.name=="depax1")
			{
				document.getElementById('depax').value = mynode.value;
			}
			if(mynode.name=="iva1")
			{
				document.getElementById('ivad').value = mynode.value;
			}
			if(mynode.name=="total1")
			{
				document.getElementById('totald').value = mynode.value;
			}
			if(mynode.name=="tipo1")
			{
				document.getElementById('tipo').value = mynode.value;
			}
			
		}
	}
	
}

function cal_iva()
{
	new Ajax.Request('php_ajax/cal_iva.php',
		{onSuccess : function(resp) 
			{
				//alert(resp.responseText);
				if( resp.responseText ) 
				{
					var myArray = eval(resp.responseText);
					//alert("22 "+resp.responseText);
					if(myArray.length==0)
						{
						alert ("No existe en el catalogo");
						document.getElementById("ivad").value=0;
						return false
						}
					  else
					  	{
						document.getElementById('importe').value = parseFloat( document.getElementById('importe').value.replace(/,/g,''));
						 document.getElementById('importe').value=Math.round(document.getElementById('importe').value*100)/100;
 						 document.getElementById('importe').value=NumberFormat(document.getElementById('importe').value, '2', '.', ',');
						
					  	 var totiva = parseFloat((myArray[0].iva*parseFloat(document.getElementById('importe').value.replace(/,/g,'')))/100);
						 document.getElementById('ivad').value = totiva; // parseFloat(totiva);
						 document.getElementById('ivad').value=Math.round(document.getElementById('ivad').value*100)/100;
						 document.getElementById('ivad').value=NumberFormat(document.getElementById('ivad').value, '2', '.', ',');
						 document.getElementById('totald').value = parseFloat( document.getElementById('importe').value.replace(/,/g,'')) + parseFloat(totiva);
						 document.getElementById('totald').value=Math.round(document.getElementById('totald').value*100)/100;
 						 document.getElementById('totald').value=NumberFormat(document.getElementById('totald').value, '2', '.', ',');
						}
					}
					else
						return true;
				}
		});
} 

function cal_iva_master()
{
	new Ajax.Request('php_ajax/cal_iva.php',
		{onSuccess : function(resp) 
			{
				//alert(resp.responseText);
				if( resp.responseText ) 
				{
					var myArray = eval(resp.responseText);
					//alert("22 "+resp.responseText);
					if(myArray.length==0)
						{
						alert ("No existe en el catalogo");
						document.getElementById("iva").value=0;
						return false
						}
					  else
					  	{
					  	 var totiva = parseFloat((myArray[0].iva*parseFloat(document.getElementById('subtotal').value.replace(/,/g,'')))/100);
						 document.getElementById('iva').value = totiva; // parseFloat(totiva);
						
						 document.getElementById('iva').value=Math.round(document.getElementById('iva').value*100)/100;
						 document.getElementById('total').value = parseFloat( document.getElementById('subtotal').value.replace(/,/g,'')) + parseFloat(totiva);
						 document.getElementById('total').value=Math.round(document.getElementById('total').value*100)/100;
						 document.getElementById('iva').value=NumberFormat(document.getElementById('iva').value, '2', '.', ',');
 						 document.getElementById('total').value=NumberFormat(document.getElementById('total').value, '2', '.', ',');	
						return;
						}
					}
					else
						return true;
				}
		});
} 


function actualiza_datos()
{
	var tabla= document.getElementById('datos');
	var ticket=document.getElementById('ticket').value;
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="ticket")
				{
					xticket=mynode.value;
					if (xticket==ticket)
						if(mynode.name=="kms")
							mynode.value = document.getElementById('kms').value;
				}
			}
		}
	}
}

function act()
{
	var total=document.getElementById('total').value;
	var eco=document.getElementById('eco').value;
	var noeco=document.getElementById('noeco').value;
	var ticket=document.getElementById('ticket').value;
	var kms=document.getElementById('kms').value;
	var fecha=document.getElementById('fecha').value;
	var lts=document.getElementById('lts').value;
	var importe=document.getElementById('importe').value;
	var ivad=document.getElementById('ivad').value;
	var totald=document.getElementById('totald').value;
	var usu_vehiculo=document.getElementById('usu_vehiculo').value;
	var usu_depto=document.getElementById('usu_depto').value;
	var uvehiculo=document.getElementById('uvehiculo').value;
	var depa=document.getElementById('depa').value;
	var depax=document.getElementById('depax').value;
	actualiza_totales();

	//var val_fac = Math.round(parseFloat(document.getElementById('total').value.replace(/,/g,''))*100)/100;
	//var val_totx = parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) + parseFloat( totald.replace(/,/g,''));
	//var val_tot = Math.round(val_totx*100)/100;
	
	//alert(val_fac);
	//alert(val_tot);

	/*if(val_tot > val_fac )
	{
		alert("Error, Importe total de Tickets es mayor que el Total de la Factura");
		return false;
	}*/
	if(noeco<=0)
	{
		if(noeco.length<1)
			{
			alert("Falta capturar No. Economico");
			document.getElementById('noeco').focus();
			return false;
			}
	}
	if(ticket.length<1)
		{
		alert("Falta capturar Ticket");
		return false;
		}
	if(kms.length<1)
		{
		alert("Falta capturar Kms");
		return false;
		}
	if(fecha.length<1)
		{
		alert("Falta capturar Fecha");
		return false;
		}
	if(lts.length<1)
		{
		alert("Falta capturar Litros");
		return false;
		}
	if(importe.length<1)
		{
		alert("Falta capturar Importe");
		return false;
		}
	if(usu_vehiculo.length<1)
	{
		alert("Falta capturar No. Usuario");
		return false;
	}
	
	var tabla = document.getElementById('datos');
	var ren=document.getElementById('ren').value;
	for (var j = 0; j <tabla.rows[ren].cells.length;j++)
	{
		var cell = tabla.rows[ren].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
			if(mynode.name=="eco1")
			{
				 mynode.value = document.getElementById('eco').value;
			}

			if(mynode.name=="noeco1")
			{
				 mynode.value = document.getElementById('noeco').value;
			}
			if(mynode.name=="ticket1")
			{
				mynode.value = document.getElementById('ticket').value;
			}
			if(mynode.name=="kms1")
			{
				mynode.value = document.getElementById('kms').value;
			}
			if(mynode.name=="fecha1")
			{
				mynode.value = document.getElementById('fecha').value;
			}
			if(mynode.name=="lts1")
			{
				mynode.value = document.getElementById('lts').value;
			}
			if(mynode.name=="importe1")
			{
				mynode.value = document.getElementById('importe').value;
			}
			if(mynode.name=="usu_vehiculo1")
			{
			mynode.value = document.getElementById('usu_vehiculo').value;
			}
			if(mynode.name=="usu_depto1")
			{
				mynode.value = document.getElementById('usu_depto').value;
			}
			if(mynode.name=="uvehiculo1")
			{
				mynode.value = document.getElementById('uvehiculo').value;
			}
			if(mynode.name=="depa1")
			{
				mynode.value = document.getElementById('depa').value;
			}
			if(mynode.name=="depax1")
			{
				mynode.value = document.getElementById('depax').value;
			}
			if(mynode.name=="iva1")
			{
				mynode.value = document.getElementById('ivad').value;
			}
			if(mynode.name=="total1")
			{
				mynode.value = document.getElementById('totald').value;
			}
			if(mynode.name=="tipo1")
			{
				mynode.value = document.getElementById('tipo').value;
			}
			
		}
	}
	limpia();
	actualiza_totales();
	
}

function limpia()
{
	document.getElementById('eco').value = ''; 
	document.getElementById('noeco').value = ''; 
	document.getElementById('ticket').value = ''; 
	document.getElementById('tipo').value = 1;
	document.getElementById('kms').value = ''; 
	document.getElementById('fecha').value = ''; 
	document.getElementById('lts').value = ''; 
	document.getElementById('importe').value = ''; 
	document.getElementById('ivad').value = ''; 
	document.getElementById('totald').value = ''; 
	document.getElementById('usu_vehiculo').value = ''; 
	document.getElementById('usu_depto').value = ''; 
	document.getElementById('uvehiculo').value = ''; 
	document.getElementById('depa').value = ''; 
	document.getElementById('depax').value = ''; 
	document.getElementById('noeco').focus();
}


function valida_fecha(fecha)
{
	esFechaValida(fecha)
	if(document.getElementById('fecha').value<document.getElementById('fi').value || document.getElementById('fecha').value>document.getElementById('ff').value)
		{
			alert("Error, Fecha fuera de Rango");
			document.getElementById('fecha').value='';
		}
		document.getElementById('fecha').focus();
}


function valida_fechai(fi)
{
	esFechaValida(fi)
}


function valida_fechaf(ff)
{
	esFechaValida(ff)
}




function activa2()
{
	document.getElementById('provname1').disabled = false;
	document.getElementById('provname1').value = '';
	document.getElementById('provname1').focus();
}

function guarda_datos()
{


// Tabla egresosmsolchegas

	
	if(	parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) > parseFloat(document.getElementById('total').value.replace(/,/g,'')) )
	{
		alert("Error, Importe total de Tickets es mayor que el Total de la Factura");
		return false;
	}
	if(	parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) < parseFloat(document.getElementById('total').value.replace(/,/g,'')) )
	{
		alert("Error, Importe total de Tickets es menor que el Total de la Factura");
		return false;
	}	
	var concepto = document.getElementById('concepto').value;
	var prov = document.getElementById('numprov').value;
	var nomprov = document.getElementById('provname1').value;
	var factura = document.getElementById('factura').value;
	var subtotal = document.getElementById('subtotal').value;
	var iva = document.getElementById('iva').value;
	var total = document.getElementById('total').value;
	var fi = document.getElementById('fi').value;
	var ff = document.getElementById('ff').value;
	var depto = document.getElementById('depto').value;
	var usuario = document.getElementById('usuario').value;
	var nomdir = document.getElementById('nomdepto').value;
	var nomdepto = document.getElementById('nomdepto').value;
	var nomcoord = document.getElementById('nomcoord').value;
	var nomfirma = document.getElementById('nomfirma').value;
	var tabla= document.getElementById('datos');
	var TotaProd= new Array( );
	var countPartidas=0;
	deshabilitar();
	
	
	if(prov.length<1)
	{
		alert("Falta capturar proveedor");
		document.getElementById('provname1').focus();
		deshabilitar();
		return false;
	}
	if(factura.length<1)
	{
		alert("Falta capturar No. de Factura");
		document.getElementById('factura').focus();
		deshabilitar();
		return false;
	}
	if(subtotal.length<1)
	{
		alert("Falta capturar Subtotal");
		document.getElementById('subtotal').focus();
		deshabilitar();
		return false;
	}
	if(iva.length<1)
	{
		alert("Falta capturar Iva");
		document.getElementById('iva').focus();
		deshabilitar();
		return false;
	}
	if(total.length<1)
	{
		alert("Falta capturar Total");
		document.getElementById('btotal').focus();
		deshabilitar();
		return false;
	}
	if(concepto.length<1)
	{
		alert("Falta capturar Concepto");
		document.getElementById('concepto').focus();
		deshabilitar();
		return false;
	}
	if(nomfirma.length<1)
	{
		alert("Falta capturar Departamento");
		document.getElementById('cvedepto').focus();
		deshabilitar();
		return false;
	}
	
	
	if(fi.length<1)
	{
		alert("Falta capturar Fecha Inicial");
		document.getElementById('fi').focus();
		deshabilitar();
		return false;
	}
	if(ff.length<1)
	{
		alert("Falta capturar Fecha final");
		document.getElementById('ff').focus();
		deshabilitar();
		return false;
	}
	
	
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < tabla.rows.length; i++) 
	{  //Iterate through all but the first row
		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="eco1")
				{
					eco0=mynode.value;
				}
				if(mynode.name=="numres1")
				{
					numres0=mynode.value;
				}
				if(mynode.name=="ticket1")
				{
					ticket0=mynode.value;
				}
				if(mynode.name=="kms1")
				{
					kms0=mynode.value;
				}
				if(mynode.name=="fecha1")
				{
					fecha0=mynode.value;
				}
				if(mynode.name=="lts1")
				{
					lts0=mynode.value;
				}
				if(mynode.name=="importe1")
				{
					subtotal0=mynode.value;
				}
				if(mynode.name=="iva1")
				{
					iva0=mynode.value;
				}
				if(mynode.name=="total1")
				{
					total0=mynode.value;
				}
				if(mynode.name=="usu_vehiculo1")
				{
					u_vehiculo0=mynode.value;
				}
				if(mynode.name=="usu_depto1")
				{
					u_depto0=mynode.value;
				}
				if(mynode.name=="tipo1")
				{
					tipo0=mynode.value;
				}
				
			}
		}
		TotaProd[countPartidas] = new Object;
		TotaProd[countPartidas]['eco0']		= eco0;			
		TotaProd[countPartidas]['numres0']	= numres0;
		TotaProd[countPartidas]['ticket0']	= ticket0;
		TotaProd[countPartidas]['kms0']		= kms0;
		TotaProd[countPartidas]['fecha0']	= fecha0;
		TotaProd[countPartidas]['lts0']		= lts0;
		TotaProd[countPartidas]['subtotal0']= subtotal0;
		TotaProd[countPartidas]['iva0']		= iva0;
		TotaProd[countPartidas]['total0']	= total0;
		TotaProd[countPartidas]['u_vehiculo0']	= u_vehiculo0;
		TotaProd[countPartidas]['u_depto0']	= u_depto0;
		TotaProd[countPartidas]['tipo0']	= tipo0;
		countPartidas++;
	}
	var datas = {
				concepto: concepto,
				prov: prov,
				nomprov: nomprov,
				nomcoord: nomcoord,
				nomdepto: nomdepto,
				nomdir: nomdir,
				nomfirma: nomfirma,
				factura: factura,
				subtotal: subtotal,
				iva: iva,
				total: total,
				fi: fi,
				ff: ff,
				depto: depto,
				usuario: usuario,
				factura: factura,
				eco0: eco0,
				numres0: numres0,
				ticket0: ticket0,
				tipo0: tipo0,
				kms0: kms0,
				fecha0: fecha0,
				lts0: lts0,
				subtotal0: subtotal0,
				iva0: iva0,
				total0: total0,
				u_vehiculo0: u_vehiculo0,
				u_depto0: u_depto0,
				prods: TotaProd
    };

	console.log(datas);
	jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/actualiza.php',
				data:          datas,
				dataType:	'json',
				success: function(resp) 
				{
					console.log(resp);
					if( resp.length ) 
					{
						//alert(resp);
						console.log(resp);
						if(parseInt(resp)>0)
						{
							console.log('algo paso');
							resp=resp.replace(/"/g,'');
							window.open("../../cheques/pdf_files/"+resp+".pdf");
							window.open("../../cheques/pdf_files/detalle_"+resp+".pdf");
							datosModificadosSinGuardar=false;
							location.href='solpago_combustible.php';
							$j.cookie("boletasGasolina","");
						}
						else
						{
							console.log('algo paso');
							alert(resp);
						}
					}
				}
			});
}

function actualiza_totales()
{
	document.getElementById('subtotalgen').value = 0;
	document.getElementById('ivagen').value = 0;
	document.getElementById('totalgen').value = 0;
	var tabla= document.getElementById('datos');
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="importe1")
				{
					importex2=mynode.value;
					document.getElementById('subtotalgen').value = parseFloat(document.getElementById('subtotalgen').value.replace(/,/g,'')) + parseFloat( importex2.replace(/,/g,''));
					document.getElementById('subtotalgen').value=Math.round(document.getElementById('subtotalgen').value*100)/100;
					document.getElementById('subtotalgen').value=NumberFormat(document.getElementById('subtotalgen').value, '2', '.', ',');	
				}
				if(mynode.name=="iva1")
				{
					ivadx2=mynode.value;
					document.getElementById('ivagen').value = parseFloat(document.getElementById('ivagen').value.replace(/,/g,'')) + parseFloat( ivadx2.replace(/,/g,''));
					document.getElementById('ivagen').value=Math.round(document.getElementById('ivagen').value*100)/100;
					document.getElementById('ivagen').value=NumberFormat(document.getElementById('ivagen').value, '2', '.', ',');
				}
				if(mynode.name=="total1")
				{
					totaldx2=mynode.value;
					document.getElementById('totalgen').value = parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) + parseFloat( totaldx2.replace(/,/g,''));
					document.getElementById('totalgen').value=Math.round(document.getElementById('totalgen').value*100)/100;
					document.getElementById('totalgen').value=NumberFormat(document.getElementById('totalgen').value, '2', '.', ',');
				}
				
			}
		}
	}
		var val_fac = Math.round(parseFloat(document.getElementById('total').value.replace(/,/g,''))*100)/100;
		var val_totx = parseFloat(document.getElementById('totalgen').value.replace(/,/g,''));
		var val_tot = Math.round(val_totx*100)/100;
		///////////
		if(val_tot > val_fac )
		{
			alert("Error, Importe total de Tickets es mayor que el Total de la Factura");
			return false;
		}
		//////////
	
}


function valida_total()
{
	if(	parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) > parseFloat(document.getElementById('total').value.replace(/,/g,'')) )
	{
		alert("Error, Importe total de Tickets es mayor que el Total de la Factura");
		return false;
	}
	if(	parseFloat(document.getElementById('totalgen').value.replace(/,/g,'')) < parseFloat(document.getElementById('total').value.replace(/,/g,'')) )
	{
		alert("Error, Importe total de Tickets es menor que el Total de la Factura");
		return false;
	}	
}


function busca()
{
document.getElementById('numfirma').value = document.getElementById("firma").options[document.getElementById("firma").selectedIndex].value;
var num_fir = document.getElementById("firma").options[document.getElementById("firma").selectedIndex].value;
	new Ajax.Request('php_ajax/firma.php?num_fir='+num_fir,
	 {onSuccess : function(resp) 
	 {
		//alert(resp.responseText);
		if( resp.responseText ) 
		{
			//got an array of suggestions.
			//alert(resp.responseText);
			var myArray = eval(resp.responseText);
			//alert("22 "+resp.responseText);
			if(myArray.length>0)
			{
				//document.getElementById('numfirma').value = myArray[0].numemp;
				document.getElementById('nomfirma').value = myArray[0].nomemp;
				document.getElementById('nomdepto').value = myArray[0].nomdepto;
				document.getElementById('fi').focus();
			}
		}
	  }
	});
}

function valida_depto()
{
	var firma = document.getElementById("firma").value;
	if(firma.length<=0)
	{
		alert ("Depto no existe");return false;
	}
}


function mod_iva()
{
	document.getElementById('totald').value=parseFloat(document.getElementById('importe').value.replace(/,/g,''))+parseFloat(document.getElementById('ivad').value.replace(/,/g,''));
	document.getElementById('totald').value =Math.round(document.getElementById('totald').value*100)/100;
	document.getElementById('totald').value=NumberFormat(document.getElementById('totald').value, '2', '.', ',');
	return;
}

function mod_iva_master()
{
	document.getElementById('total').value=parseFloat(document.getElementById('subtotal').value.replace(/,/g,''))+parseFloat(document.getElementById('iva').value.replace(/,/g,''));
	document.getElementById('total').value =Math.round(document.getElementById('total').value*100)/100;
	document.getElementById('total').value=NumberFormat(document.getElementById('total').value, '2', '.', ',');
	return;
}


function valida_fec()
{
		//alert("dsf");
		document.getElementById('fecha').value = document.getElementById('fi').value;
}

function valida_campos()
{
	var prov=document.getElementById('numprov').value;
	var factura=document.getElementById('factura').value;
	var subtotal=document.getElementById('subtotal').value;
	var iva=document.getElementById('iva').value;
	var total=document.getElementById('total').value;
	var concepto=document.getElementById('concepto').value;
	var fi=document.getElementById('fi').value;
	var ff=document.getElementById('ff').value;
	var cvedepto = document.getElementById('cvedepto').value;
	if(prov.length<1)
	{
		alert("Falta capturar proveedor");
		document.getElementById('provname1').focus();
		deshabilitar();
		return false;
	}
	if(factura.length<1)
	{
		alert("Falta capturar No. de Factura");
		document.getElementById('factura').focus();
		deshabilitar();
		return false;
	}
	if(subtotal.length<1)
	{
		alert("Falta capturar Subtotal");
		document.getElementById('subtotal').focus();
		deshabilitar();
		return false;
	}
	if(iva.length<1)
	{
		alert("Falta capturar Iva");
		document.getElementById('iva').focus();
		deshabilitar();
		return false;
	}
	if(total.length<1)
	{
		alert("Falta capturar Total");
		document.getElementById('btotal').focus();
		deshabilitar();
		return false;
	}
	if(concepto.length<1)
	{
		alert("Falta capturar Concepto");
		document.getElementById('concepto').focus();
		deshabilitar();
		return false;
	}
	if(cvedepto.length<1)
	{
		alert("Falta capturar Departamento");
		document.getElementById('cvedepto').focus();
		deshabilitar();
		return false;
	}
	
	
	if(fi.length<1)
	{
		alert("Falta capturar Fecha Inicial");
		document.getElementById('fi').focus();
		deshabilitar();
		return false;
	}
	if(ff.length<1)
	{
		alert("Falta capturar Fecha final");
		document.getElementById('ff').focus();
		deshabilitar();
		return false;
	}
}