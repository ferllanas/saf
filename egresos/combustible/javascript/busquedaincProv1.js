//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv1() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv1 = getXmlHttpRequestObjectProv1(); 

function searchProveedor1() {
	$('numprov').value = 0;
	//limpia_tabla();
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqProv1.readyState == 4 || searchReqProv1.readyState == 0)
	{
        var str = escape(document.getElementById('provname1').value);
		//alert('php_ajax/queryproveedorincrem.php?q=' + str);
        searchReqProv1.open("GET",'php_ajax/queryproveedorincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv1.onreadystatechange = handleSearchSuggestProv1;
        searchReqProv1.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv1() {
	
	//alert("aja 1");
    if (searchReqProv1.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv')
        ss.innerHTML = '';
		//alert(searchReqProv1.responseText);
        var str = searchReqProv1.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOverProv1(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProv1(this);" ';
				suggest += "onclick='javascript:setSearchProv1(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverProv1(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv1(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv1(value,idprod) 
{
	//alert("aja");
    document.getElementById('provname1').value = value;
	document.getElementById('provname1').disabled = true;
	var tok =idprod.split(";");
	$('numprov').value = tok[0];
	//$('nomproveedor1').value=  value;
	//$('nomproveedor1').title=  value;
	document.getElementById('factura').focus();
	document.getElementById('search_suggestProv').innerHTML = '';
//	valida_factura();
}


function limpia_tabla()
{
	var tabla= $('provsT');//Asigna a la variable tabla el objeto provsT(cuerpo de la tabla) que se encuentra declarado en buscaprov.php

	var numren=tabla.rows.length;
	for(var i = numren-1; i >=0; i--)
	{
		tabla.deleteRow(i);
	}
}

function activa2()
{
	document.getElementById('provname1').disabled = false;
	document.getElementById('provname1').value = '';
	document.getElementById('provname1').focus();
}
