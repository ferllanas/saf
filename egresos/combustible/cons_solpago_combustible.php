<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$mdatos=array();
$resguarda='';
$uvehiculo='';
$noeco='';
$depa='';
$depax='';
$nombre='';
$prov='';
$cvedepto="";
//$usuario = $_COOKIE['ID_my_site'];
//$depto = $_COOKIE['depto'];
$usuario='001349';
$depto='1232';

if(isset($_REQUEST['folio']))
	$folio = $_REQUEST['folio'];

if ($conexion)
{
	$consulta = "select a.folio,a.concepto,a.prov,a.subtotal,a.iva,a.total,convert(varchar(12),a.fecini,103)";
	$consulta .= " as fecini,convert(varchar(12),a.fecfin,103) as fecfin,b.nomprov,c.factura,c.depto from ";
	$consulta .= "egresosmsolchegas a left join compramprovs b on a.prov=b.prov left join egresosmsolche c ";
	$consulta .= "on a.folio=c.folio where a.folio=$folio and a.estatus<9000";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
		$concepto=$mdatos[$i]['concepto']= trim($row['concepto']);
		$nombre=$mdatos[$i]['nomprov']= trim($row['nomprov']);
		$subtotal=$mdatos[$i]['subtotal']= trim($row['subtotal']);
		$iva=$mdatos[$i]['iva']= trim($row['iva']);
		$total=$mdatos[$i]['total']= trim($row['total']);
		$fecini=$mdatos[$i]['fecini']= trim($row['fecini']);
		$fecfin=$mdatos[$i]['fecfin']= trim($row['fecfin']);
		$factura=$mdatos[$i]['factura']= trim($row['factura']);
		$depto=$mdatos[$i]['depto']= trim($row['depto']);
		$i++;
		}
}

$consulta = "select a.numeco,a.ticket,a.kms,convert(varchar(12),a.fecha,103) as fecha,a.litros,a.subtotal,a.iva,a.total,";
$consulta .= "a.numemp,a.deptoemp,a.respbien,Rtrim(b.nombre)+' '+Rtrim(b.appat)+' '+Rtrim(b.apmat) as nomresp ";
$consulta .= "from egresosdsolchegas a left join nominadempleados b on a.respbien=b.numemp where a.folio=$folio and ";
$consulta .= "a.estatus<9000";
$R = sqlsrv_query( $conexion,$consulta);
$i=0;
while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['numeco']= trim($row['numeco']);
		$datos[$i]['ticket']= trim($row['ticket']);
		$datos[$i]['kms']= $row['kms'];
		$datos[$i]['fecha']= $row['fecha'];
		$datos[$i]['litros']= $row['litros'];
		$datos[$i]['subtotal']= $row['subtotal'];
		$datos[$i]['iva']= $row['iva'];
		$datos[$i]['total']= $row['total'];
		$datos[$i]['numemp']= $row['numemp'];
		$datos[$i]['deptoemp']= $row['deptoemp'];
		$datos[$i]['respbien']= $row['respbien'];
		$datos[$i]['nomresp']= trim($row['nomresp']);
		$i++;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincusu.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/combustible_fun.js"></script>
<script language="javascript" src="javascript/busqueda2.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>

<SCRIPT LANGUAGE="JavaScript">
<!-- Original:  Ronnie T. Moore -->
<!-- Web Site:  The JavaScript Source -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
nextfield = "factura"; // name of first box on page
netscape = "";
ver = navigator.appVersion; len = ver.length;
for(iln = 0; iln < len; iln++) if (ver.charAt(iln) == "(") break;
netscape = (ver.charAt(iln+1).toUpperCase() != "C");

function keyDown(DnEvents) { // handles keypress
// determines whether Netscape or Internet Explorer
k = (netscape) ? DnEvents.which : window.event.keyCode;
if (k == 13) { // enter key pressed
if (nextfield == 'done') return true; // submit, we finished all fields
else { // we're not done yet, send focus to next box
eval('document.form1.' + nextfield + '.focus()');
return false;
      }
   }
}
document.onkeydown = keyDown; // work together to analyze keystrokes
if (netscape) document.captureEvents(Event.KEYDOWN|Event.KEYUP);
//  End -->
</script>


<body>

<form name="form1" method="post" action="">
<p>&nbsp;</p>
<table width="98%"  border="0">
  <tr>
    <th width="9%" class="texto9" scope="row">Beneficiario</th>
    <td width="28%">
     <input name="nombre" type="text" class="texto8" id="nombre" readonly value="<?php echo $nombre;?>" size="100">        
	</td>
    <td width="8%" align="right" class="texto9 Estilo1">No. Factura</td>
    <td width="10%"><input type="text" name="factura" id="factura" readonly value="<?php echo $factura;?>" style="width:70px "></td>
    <td width="7%" class="texto9"><strong>Subtotal</strong></td>
    <td width="10%"><input type="text" name="subtotal" id="subtotal" readonly value="<?php echo $subtotal;?>" style="width:70px "></td>
    <td width="3%" class="texto9"><strong>Iva</strong></td>
    <td width="10%"><input type="text" name="iva" id="iva" value="<?php echo $iva;?>" readonly style="width:70px "></td>
    <td width="5%" class="texto9"><strong>Total</strong><span class="texto10"><span class="texto8"><strong>
      <input class="texto8" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
    </strong></span></span></td>
    <td width="10%"><input type="text" name="total" id="total" value="<?php echo $total;?>" readonly style="width:70px "></td>
  </tr>
</table>
</p>
<table width="99%"  border="0">
  <tr>
    <th width="6%" align="right" class="texto9" scope="row">Concepto</th>
    <td width="49%"><input type="concepto" name="concepto" id="concepto" readonly value="<?php echo $concepto;?>" style="width:500px "></td>
    <td width="13%" class="texto9"><strong>Periodo de Consumo</strong></td>
    <td width="32%">
		 <span class="cajaPrecio">Fecha Inicial:</span>	
		 <input name="fi" type="text" size="7" id="fi" onFocus="nextfield ='ff';" readonly tabindex="7" value="<?php echo $fecini;?>" onBlur="valida_fechai(this)" class="required" maxlength="10"  style="width:70">
		        <span class="cajaPrecio">Fecha Final:</span>
				<input name="ff" type="text" size="7" id="ff" onFocus="nextfield ='cvedepto';" readonly tabindex="8" value="<?php echo $fecfin;?>" onBlur="valida_fechaf(this)" class="required" maxlength="10"  style="width:70">
            <span class="texto10">
            <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
        </span><span class="texto10">
            <input type="hidden" name="depto" id="depto" value="<?php echo $depto;?>">
            </span>        
	</td>
  </tr>
</table>
<p>&nbsp;</p>
<hr class="hrTitForma">
<tr><td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row" align="right">&nbsp;</th>
    <td align="center" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row" align="right">&nbsp;      </th>
    <td align="center" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table name="enc" id="enc" width="87%" height="10%" align="center" border="1">
  <tr>
    <th width="49" align="center" class="subtituloverde"># Eco</th>
    <th width="53" align="center" class="subtituloverde"># Ticket </th>
    <th width="41" align="center" class="subtituloverde">Kms</th>
    <th width="51" align="center" class="subtituloverde">Fecha</th>
    <th width="39" align="center" class="subtituloverde">Litros</th>
    <th width="56" align="center" class="subtituloverde">Importe</th>
    <th width="38" align="center" class="subtituloverde">Iva</th>
    <th width="55" align="center" class="subtituloverde">Total</th>
    <th width="56" align="center" class="subtituloverde">Usuario</th>
    <th width="42" align="center" class="subtituloverde">Depto</th>
    <th width="223" align="center" class="subtituloverde">Resguardante</th>
    </tr>
</table>
<div style="overflow:auto; width: 100%; height :130px; align:center;">  
  <table name="datos" id="datos" width="87%" height="21%" border=1 align="center" style="overflow:auto; " >
 	<?php 
		for($i=0;$i<count($datos);$i++)
		{
	?>
  <tr>
   <td width="49" align="center" class="texto8"><?php echo $datos[$i]['numeco'];?></td>
    <td width="53" align="center" class="texto8"><?php echo $datos[$i]['ticket'];?></td>
    <td width="41" align="center" class="texto8"><?php echo $datos[$i]['kms'];?></td>
    <td width="49" align="center" class="texto8"><?php echo $datos[$i]['fecha'];?></td>
    <td width="39" align="center" class="texto8"><?php echo $datos[$i]['litros'];?></td>
    <td width="56" align="center" class="texto8"><?php echo $datos[$i]['subtotal'];?></td>
    <td width="38" align="center" class="texto8"><?php echo $datos[$i]['iva'];?></td>
    <td width="55" align="center" class="texto8"><?php echo $datos[$i]['total'];?></td>
    <td width="56" align="center" class="texto8"><?php echo $datos[$i]['numemp'];?></td>
    <td width="43" align="center" class="texto8"><?php echo $datos[$i]['deptoemp'];?></td>
    <td width="224" align="left" class="texto8"><?php echo $datos[$i]['respbien'].' - '.$datos[$i]['nomresp'];?></td>
    </tr>
	<?php
		}
	?>
  </table>
</div>
<p>
<table width="50%" align="right" style="visibility:hidden " border="1">
  <tr>
    <th scope="row"><input type="text" name="subtotalgen" id="subtotalgen" value=0></th>
    <td><input type="text" name="ivagen" id="ivagen" value=0></td>
    <td><input type="text" name="totalgen" id="totalgen" value=0></td>
  </tr>
</table>

<input align="right" type="button" name="guardar" onClick="window.close();" value="Cerrar">
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
