<?php

    if (version_compare(PHP_VERSION, "5.1.0", ">="))
 	require_once("../../dompdf/dompdf_config.inc.php");
	date_default_timezone_set("America/Mexico_City");
	require_once("../../connections/dbconexion.php");
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	include("../../Administracion/globalfuncions.php");
	
    $infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
    //$usuario = $_COOKIE['ID_my_site'];

	$datos=array();
	$mdatos=array();
	$resguarda='';
	$uvehiculo='';
	$noeco='';
	$depa='';
	$depax='';
	$nombre='';
	$prov='';
	$cvedepto="";

	$usuario='001349';
	$depto='1232';	
	
	$folio=9;
	
	$fecha_aut=date("d/m/Y");
	$hora = getdate(time());
	$hora_act=( $hora["hours"] . ":" . $hora["minutes"] . ":" . $hora["seconds"] ); 

if ($conexion)
{
	$consulta = "select a.folio,a.concepto,a.prov,a.subtotal,a.iva,a.total,convert(varchar(12),a.fecini,103)";
	$consulta .= " as fecini,convert(varchar(12),a.fecfin,103) as fecfin,b.nomprov,c.factura,c.depto from ";
	$consulta .= "egresosmsolchegas a left join compramprovs b on a.prov=b.prov left join egresosmsolche c ";
	$consulta .= "on a.folio=c.folio where a.folio=$folio and a.estatus<9000";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
		$concepto=$mdatos[$i]['concepto']= trim($row['concepto']);
		$nombre=$mdatos[$i]['nomprov']= trim($row['nomprov']);
		$subtotal= number_format($mdatos[$i]['subtotal']= trim($row['subtotal']),2);
		$iva=number_format($mdatos[$i]['iva']= trim($row['iva']),2);
		$total=number_format($mdatos[$i]['total']= trim($row['total']),2);
		$fecini=$mdatos[$i]['fecini']= trim($row['fecini']);
		$fecfin=$mdatos[$i]['fecfin']= trim($row['fecfin']);
		$factura=$mdatos[$i]['factura']= trim($row['factura']);
		$depto=$mdatos[$i]['depto']= trim($row['depto']);
		$i++;
		}
}


$consulta = "select a.numeco,a.ticket,a.kms,convert(varchar(12),a.fecha,103) as fecha,a.litros,a.subtotal,a.iva,a.total,";
$consulta .= "a.numemp,a.deptoemp,a.respbien,Rtrim(b.nombre)+' '+Rtrim(b.appat)+' '+Rtrim(b.apmat) as nomresp ";
$consulta .= "from egresosdsolchegas a left join nominadempleados b on a.respbien=b.numemp where a.folio=$folio and ";
$consulta .= "a.estatus<9000";
$R = sqlsrv_query( $conexion,$consulta);
$i=0;
while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['numeco']= trim($row['numeco']);
		$datos[$i]['ticket']= trim($row['ticket']);
		$datos[$i]['kms']= $row['kms'];
		$datos[$i]['fecha']= $row['fecha'];
		$datos[$i]['litros']= $row['litros'];
		$datos[$i]['subtotal']= $row['subtotal'];
		$datos[$i]['iva']= $row['iva'];
		$datos[$i]['total']= $row['total'];
		$datos[$i]['numemp']= $row['numemp'];
		$datos[$i]['deptoemp']= $row['deptoemp'];
		$datos[$i]['respbien']= $row['respbien'];
		$datos[$i]['nomresp']= trim($row['nomresp']);
		$i++;
	}

$dompdf = new DOMPDF();

$html="
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
<link href='../../css/estilos.css' rel='stylesheet' type='text/css'>
<script language='javascript' src='../../javascript_globalfunc/funcionesGlobales.js'></script>
<script language='javascript' src='../../javascript_globalfunc/funciones_AHojaDEstilos.js'></script>

<style type='text/css'>
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>

<body>

			<table width='100%' align='center' height='48' border='0'>
			  <tr>
					<td align='left' width='20'><img src='".getDirAtras(getcwd())."".$imagenPDFPrincipal."' width='200' height='76' /></td>
                <td class='texto9' align='center'><b><BR />
                </b></td>
					<td align='right' width='20'><img src='../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
			  </tr>
			</table>

<p>&nbsp;</p>
<table width='100%' border='0'>
  <tr>
    <th width='9%' class='texto9' scope='row'><div align='right'>Beneficiario</div></th>
    <td width='28%'>".$nombre."</td>
    <td width='8%' align='right' class='texto9 Estilo1'>No. Factura</td>
    <td width='10%'>".$factura."</td>
    <td width='7%' class='texto9'><strong>Subtotal</strong></td>
    <td width='10%'>".$subtotal."</td>
    <td width='3%' class='texto9'><div align='center'><strong>Iva</strong></div></td>
    <td width='10%'>".$iva."</td>
    <td width='5%' class='texto9'><strong>Total</strong><span class='texto10'><span class='texto8'></td>
    <td width='10%'>".$total."</td>
  </tr>
  <tr>
    <th rowspan='2' class='texto9' scope='row'><div align='right'>Concepto</div></th>
    <td class='texto8' rowspan='2'>".$concepto."</textarea></td>
    <td colspan='2' rowspan='2' align='right' class='texto9'><div align='center'><strong>Periodo de Consumo</strong></div></td>
    <td rowspan='2' class='texto9 Estilo1'><div align='center'>Del</div></td>
    <td rowspan='2'>".$fecini."</td>
    <td rowspan='2' class='texto9'><div align='center'><strong>Al</strong></div></td>
    <td rowspan='2' >".$fecfin."</td>
  </tr>
  <tr>
    <td class='texto9'>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p></p>
<hr class='hrTitForma'>
<table name='enc' id='enc' width='87%' height='10%' align='center' border='1'>
  <tr  bgcolor='#CCCCCC'>
    <td width='49' align='center' class='texto8'><strong># Eco</strong></td>
    <td width='53' align='center' class='texto8'><strong># Ticket</strong></td>
    <td width='41' align='center' class='texto8'><strong>Kms</strong></td>
    <td width='51' align='center' class='texto8'><strong>Fecha</strong></td>
    <td width='39' align='center' class='texto8'><strong>Litros</strong></td>
    <td width='56' align='center' class='texto8'><strong>Importe</strong></td>
    <td width='38' align='center' class='texto8'><strong>Iva</strong></td>
    <td width='55' align='center' class='texto8'><strong>Total</strong></td>
    <td width='56' align='center' class='texto8'><strong>Usuario</strong></td>
    <td width='42' align='center' class='texto8'><strong>Depto</strong></td>
    <td width='223' align='center' class='texto8'><strong>Resguardante</strong></td>
  </tr>
</table>

  <table name='datos' id='datos' width='87%' height='21%' border=1 align='center' style='overflow:auto; ' >
";
		for($i=0;$i<count($datos);$i++)
		{
$html.="
    <tr>
      <td width='49' align='center' class='texto8'>".$datos[$i]['numeco']."</td>
      <td width='53' align='center' class='texto8'>".$datos[$i]['ticket']."</td>
      <td width='41' align='center' class='texto8'>".$datos[$i]['kms']."</td>
      <td width='49' align='center' class='texto8'>".$datos[$i]['fecha']."</td>
      <td width='39' align='right' class='texto8'>".number_format($datos[$i]['litros'],2)."</td>
      <td width='56' align='right' class='texto8'>".number_format($datos[$i]['subtotal'],2)."</td>
      <td width='38' align='right' class='texto8'>".number_format($datos[$i]['iva'],2)."</td>
      <td width='55' align='right' class='texto8'>".number_format($datos[$i]['total'],2)."</td>
      <td width='56' align='center' class='texto8'>".$datos[$i]['numemp']."</td>
      <td width='43' align='center' class='texto8'>".$datos[$i]['deptoemp']."</td>
      <td width='224' align='left' class='texto8'>".$datos[$i]['respbien'].' - '.$datos[$i]['nomresp']."</td>
    </tr>
";
		}
$html.="
  </table>

</body>
</html>";
	$dompdf->set_paper("letter","landscape");
	
	$dompdf->load_html($html);
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("pdf_files/detalle_".$folio.".pdf", $pdf);

?>