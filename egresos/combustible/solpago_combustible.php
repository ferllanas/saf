<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$resguarda='';
$uvehiculo='';
$noeco='';
$depa='';
$depax='';
$nombre='';
$prov='';
$cvedepto="";

$usuario = $_COOKIE['ID_my_site'];
$depto = $_COOKIE['depto'];
//$usuario='001349';
//$depto='1232';

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">

<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../jquery-cookie/jquery.cookie.js"></script>
<script language="javascript" src="../../javascript_globalfunc/onbeforeunload.js"></script>

<script language="javascript" src="javascript/busquedaincusu.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/combustible_fun.js"></script>
<script language="javascript" src="javascript/busqueda2.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>


<script language="javascript" src="../../prototype/prototype.js"></script>



<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
#apDiv1 {
	position:absolute;
	left:313px;
	top:706px;
	width:678px;
	height:323px;
	z-index:9;
}
.Estilo2 {
	color: #666666;
	font-weight: bold;
}
.Estilo3 {color: #666666}
-->
</style>
</head>

<SCRIPT LANGUAGE="JavaScript">
<!-- Original:  Ronnie T. Moore -->
<!-- Web Site:  The JavaScript Source -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
nextfield = "provname1"; // name of first box on page
netscape = "";
ver = navigator.appVersion; len = ver.length;
for(iln = 0; iln < len; iln++) if (ver.charAt(iln) == "(") break;
netscape = (ver.charAt(iln+1).toUpperCase() != "C");

function keyDown(DnEvents) { // handles keypress
// determines whether Netscape or Internet Explorer
k = (netscape) ? DnEvents.which : window.event.keyCode;
if (k == 13) { // enter key pressed
if (nextfield == 'done') return true; // submit, we finished all fields
else { // we're not done yet, send focus to next box
eval('document.form1.' + nextfield + '.focus()');
return false;
      }
   }
}
document.onkeydown = keyDown; // work together to analyze keystrokes
if (netscape) document.captureEvents(Event.KEYDOWN|Event.KEYUP);
//  End -->
</script>





<body >
<form name="form1" method="post" action="">
  <p class="seccionNota Estilo1">Solicitud de Pago de Combustible</p>
  <hr class="hrTitForma">

  <table width="98%"  border="0">
  <tr>
    <td width="7%" class="texto9" scope="row">Beneficiario</td>
    <td width="45%">
      <div align="left" style="z-index:1; position:relative; width:height: 24px;">
        <input class="texto8" type="text" id="provname1" name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="1" value="<?php echo $nombre;?>">        
        <img src="../../imagenes/deshacer.jpg" width="20" height="14" onClick="activa2()">
        <div id="search_suggestProv" style="z-index:2;" > </div>
    </div></td>
    <td width="10%" align="right" class="texto9">No. Factura</td>
    <td width="7%"><input type="text" name="factura" id="factura" tabindex="2" onFocus="nextfield ='subtotal';" onBlur="valida_factura()" style="width:70px "></td>
    <td width="5%" class="texto9">Subtotal</td>
    <td width="7%"><input type="text" name="subtotal" id="subtotal" tabindex="3" onFocus="nextfield ='iva';" onBlur="cal_iva_master()" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" style="width:70px "></td>
    <td width="2%" class="texto9">Iva</td>
    <td width="7%"><input type="text" name="iva" id="iva" tabindex="4" onFocus="nextfield ='total';" onKeyPress="EvaluateText('%f', this);" onBlur="mod_iva_master()" onKeyUp="asignaFormatoSiesNumerico(this, event)" style="width:70px "></td>
    <td width="3%" class="texto9">Total<span class="texto10"><span class="texto8"><strong>
      <input class="texto8" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
    </strong></span></span></td>
    <td width="7%"><input type="text" name="total" id="total" onFocus="nextfield ='concepto';" tabindex="5" onKeyPress="EvaluateText('%f', this);" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" readonly style="width:70px "></td>
  </tr>
  <tr>
    <td class="texto9" scope="row">Concepto</td>
    <td colspan="2"><input type="concepto" name="concepto" id="concepto" onFocus="nextfield ='cvedepto';" tabindex="6" style="width:400px "></td>
    <td class="texto9">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto9" scope="row"><input class="texto8" type="hidden" id="usuario" name="usuario" size="10" value="<?php echo $usuario;?>">
      <input class="texto8" type="hidden" id="depto" name="depto" size="10" value="<?php echo $depto;?>">
      <input class="texto8" type="hidden" id="nomdepto" name="nomdepto" size="10" value="<?php echo $datos[$i]['nomdir'];?>">      Firma:</td>
    <td><div align="left"  style="z-index:3; position:relative; width: height: 24px;"> <span class="texto8">
      <input class="texto8" type="text" name="cvedepto" id="cvedepto" onFocus="nextfield ='firma';" style="width:90%;" onKeyUp="searchdepto(this);" autocomplete="off" tabindex="7" value="<?php echo $cvedepto;?>">
      <img src="../../imagenes/deshacer.jpg" width="20" height="14" onClick="activa()"> </span>
          <div id="search_suggestdepto" style="z-index:4;" > </div>
    </div></td>
    <td align="right" class="texto9 Estilo1"><input class="texto8" type="hidden" id="nomfirma" name="nomfirma" size="10" value="<?php echo $datosx[$i]['nomemp'];?>">
      <span class="texto10"><span class="texto8 Estilo1">
      <input class="texto8" type="hidden" id="nomcoord" name="nomcoord" size="10" value="<?php echo $datos[$i]['nomcoord'];?>">
      </span></span>
      <input class="texto8" type="hidden" id="numfirma" name="numfirma" size="10" value="<?php echo $datosx[$i]['numemp'];?>">
      <span class="texto8">
      <select name="firma" id="firma" onFocus="nextfield ='fi';" onChange="busca()" style="visibility:hidden " onBlur="valida_depto()" tabindex="8" class="texto8">
        <?php
			for($i = 0 ; $i<count($datosx);$i++)
			{
			echo "<option value=".$datosx[$i]["numemp"].">".$datosx[$i]["numemp"]."    ".$datosx[$i]["nombre"]."</option>\n";
				//echo "<option value=".$datosx[$i]['nombre']."></option>";
			}
		?>
      </select>
      </span></td>
    <td><div align="center" class="texto9">Periodo</div></td>
    <td colspan="6" class="texto9"><span class="cajaPrecio">Fecha Inicial:</span>
      <input name="fi" type="text" size="7" id="fi" onFocus="nextfield ='ff';" readonly tabindex="10" value="" onBlur="valida_fechai(this)" class="required" maxlength="10"  style="width:70">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle">
      <!--{literal}-->
      <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fi",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger1",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script>
      <span class="cajaPrecio">Fecha Final:</span>
      <input name="ff" type="text" size="7" id="ff" onFocus="nextfield ='noeco';" readonly tabindex="8" value="" onBlur="valida_fechaf(this)" class="required" maxlength="10"  style="width:70">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer; z-index:5" title="Date selector" align="absmiddle">
	  
	   <span class="texto10">
      <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
      </span><span class="texto10">
      <input type="hidden" name="depto" id="depto" value="<?php echo $depto;?>">
      </span>
            <!--{literal}-->
	  <script type="text/javascript">
					Calendar.setup({
						inputField     :    "ff",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script></td>
	  
    </tr>
</table>
</p>
<hr class="hrTitForma">
<table width="1017" border="0">
  <tr>
    <td width="64"><span class="texto10">No. Eco.</span></td>
    <td width="358"><div align="left" style="position:relative;height: 15px; width:350; left:22 z-index:5">
      <input class="texto8" type="text" name="noeco" id="noeco" onFocus="nextfield ='uvehiculo';" style="width:320px; left:36px; " height="18" onKeyUp="searchnumeco(this);" value="<?php echo $noeco;?>"  autocomplete="off" tabindex="11">
      <div id="search_suggestnumeco" style="z-index:6;" > </div>
    </div></td>
    <td width="58" class="texto9">Usuario</td>
    <td width="519"><div align="left"  style="z-index:7; position:relative; height: 15px">
      <input name="uvehiculo" type="text" class="texto8" id="uvehiculo" onFocus="nextfield ='ticket';" tabindex="12" onKeyUp="searchdepto2(this);" value="<?php echo $uvehiculo;?>" style="width:320px " autocomplete="off">
      <input type="hidden" name="eco"  id="eco"> 
      <span class="texto10">
      <input type="hidden" name="usu_depto" id="usu_depto">
      </span>      <div id="search_suggestdepto2" style="z-index:8; "></div>
    </div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="texto9">
      <input name="depax" type="text" class="texto8" id="depax"  value="<?php echo $depax;?>" size="59" readonly>
    </span></td>
    <td>&nbsp;</td>
    <td><span class="texto9">
      <input name="depa" type="text" class="texto8" id="depa" style="width:320px; visibility:hidden; " value="<?php echo $depa;?>" size="68" readonly>
      <input type="hidden" name="ren" id="ren">
      <span class="texto10">
      <input type="hidden" name="usu_vehiculo" id="usu_vehiculo">
      </span></span></td>
  </tr>
</table>
<tr><td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>&nbsp;
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row" align="right">&nbsp;</th>
    <td align="center" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th scope="row" align="right">&nbsp;      </th>
    <td align="center" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table name="detalle2" id="detalle2" width="95%" border="0">
  <tr>
    <td width="4%" align="right" class="texto9" scope="row">No. Ticket</td>
    <th width="9%" align="left" class="texto10" scope="row"><input type="text" name="ticket" id="ticket" onFocus="nextfield ='kms';" tabindex="13" style="width:60px " onBlur="valida_ticket()" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" ></th>
    <th width="11%" align="left" class="texto10" scope="row">
	<select name="tipo" id="tipo">
      <option value=1>Ordinaria</option>
      <option value=2>Extraordinaria</option>
    </select></th>
    <td width="3%" align="right" class="texto9" scope="row"> <input type="hidden" name="nomres" id="nomres">
      Kms.</td>
    <td width="6%" align="left"><input type="text" name="kms" id="kms" onFocus="nextfield ='fecha';" tabindex="14" style="width:50px " onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" size="8px" width="70px" ></td>
    <td width="4%" scope="row" class="texto9"><span class="texto10">
      <input type="hidden" name="numres" id="numres">
    </span>Fecha</td>
    <td width="8%" align="left">
      <input type="text" name="fecha" id="fecha" onFocus="nextfield ='lts';" readonly style="width:68px " tabindex="15" size="8px" width="70px">
      <span class="texto9"><img src="../../calendario/img.gif" width="16" height="16" id="f_trigger3" style="cursor: pointer; z-index:6" title="Date selector" align="absmiddle"></span>
	        <!--{literal}-->
      <script type="text/javascript">
					Calendar.setup({
						inputField     :    "fecha",		// id of the input field
						ifFormat       :    "%d/%m/%Y",		// format of the input field
						button         :    "f_trigger3",	// trigger for the calendar (button ID)
						//onClose        :    fecha_cambio,
						singleClick    :    true
					});
				</script>
      </td>
    <td width="4%" class="texto9">Litros</td>
    <td width="5%" align="left" class="texto9"><input type="text" name="lts" id="lts" onBlur="val_fecha()" onFocus="nextfield ='importe';" tabindex="16" style="width:40px " onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" size="8px" width="70px" ></td>
    <td width="5%" align="left" class="texto9">Importe</td>
    <td width="7%" align="left" class="texto10">
      <input type="text" name="importe" id="importe" style="width:60px " onFocus="nextfield ='ivad';" onBlur="cal_iva()"  tabindex="17" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" size="8px" width="70px">    </td>
    <td width="2%" align="center" class="texto9">Iva</td>
    <td width="7%" align="center"><span class="texto10">
      <input type="text" name="ivad" id="ivad" style="width:60px " onFocus="nextfield ='totald';" tabindex="18" onBlur="mod_iva()" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)"  size="8px" width="70px">
    </span></td>
    <td width="3%" align="center" class="texto9">Total</td>
    <td width="10%" align="center"><span class="texto10">
      <input type="text" name="totald" id="totald" tabindex="19" readonly style="width:60px " onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)">
    </span></td>
    <td width="7%" align="left"><span class="texto9"><img src="../../imagenes/agregar2.jpg" width="30" height="25" title="Agregar Ticket Nuevo." onClick="agregar()"></span></td>
    <td width="5%" align="right"><span class="texto9"><img src="../../imagenes/actualiza3.png" width="30" height="25" title="Actualiza Datos de Ticket." onClick="act()"></span></td>
  </tr>
</table>

<table name="enc" id="enc" width="100%" height="10%" align="center" border="1">
  <tr>
    <th width="53" align="center" class="subtituloverde"># Eco</th>
    <th width="57" align="center" class="subtituloverde"># Ticket </th>
    <th width="60" align="center" class="subtituloverde">Kms</th>
    <th width="56" align="center" class="subtituloverde">Fecha</th>
    <th width="43" align="center" class="subtituloverde">Litros</th>
    <th width="61" align="center" class="subtituloverde">Importe</th>
    <th width="50" align="center" class="subtituloverde">Iva</th>
    <th width="53" align="center" class="subtituloverde">Total</th>
    <th width="51" align="center" class="subtituloverde">Usuario</th>
    <th width="48" align="center" class="subtituloverde">Depto</th>
    <th width="277" align="center" class="subtituloverde">Resguardante</th>
    <th width="51" align="center" class="subtituloverde">Acci&oacute;n</th>
    </tr>
</table>
<div style="overflow:auto; width: 100%; height :130px; align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
</div>
<p>
<table width="50%" align="right"  border="0">
  <tr class="texto10">
  	 <td colspan="2"><div align="center" class="Estilo2">Importe</div></td>
   	 <td colspan="2"><div align="center"><span class="Estilo3"></span></div>   	   <div align="center" class="Estilo3"><strong>Iva</strong></div></td>
  	 <td colspan="2"><div align="center"><span class="Estilo3"></span></div>  	   <div align="center" class="Estilo3"><strong>Total</strong></div></td>
    </tr>
  <tr>
    <td class="mayus10">$</td>
    <td><input type="text" name="subtotalgen" id="subtotalgen" value=0></td>
	<td class="mayus10">$</td>
    <td><input type="text" name="ivagen" id="ivagen" value=0></td>
	<td class="mayus10">$</td>
    <td><input type="text" name="totalgen" id="totalgen" value=0></td>
  </tr>
</table>

<input align="right" type="button" name="guardar" id="guardar" disabled onBlur="deshabilitar()" onClick="guarda_datos()" value="Guardar">
</p>

<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
</body>
</html>
