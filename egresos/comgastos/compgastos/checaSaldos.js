//Elimina una menu
var proveedores=new Array(); 

//Elimina una menu
function eliminar_ProductoARequisicion( e , s )
{
	//var table=document.getElementById( 'tmenus' );
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	//alert(rowindexA+","+ document.getElementById("menus").rows.length);
	document.getElementById("menus").deleteRow( rowindexA );
}

function CrearOrdenDCompra()
{
	var table=document.getElementById("menus");
	//Ciclo para obtener los productos de la requisicion
	var cadena_productosDRequi="(";
	for (var i = 0; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		cadena_productosDRequi+='array(';
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="idprodEnRequi")
					cadena_productosDRequi+='"idprodEnRequi"=>"'+mynode.value+'",';
					
				if(mynode.name=="idprod")
					cadena_productosDRequi+='"idprod"=>"'+mynode.value+'",';
					
				if(mynode.name=="producto")
					cadena_productosDRequi+='"producto"=>"'+mynode.value+'",';
				
				if(mynode.name=="uniprod")
					cadena_productosDRequi+='"uniprod"=>"'+mynode.value+'",';
				
				if(mynode.name=="cantidad")
					cadena_productosDRequi+='"cantidad"=>"'+mynode.value+'",';
					
				if(mynode.name=="desc")
					cadena_productosDRequi+='"desc"=>"'+mynode.value+'",';
				
			}
		}
		cadena_productosDRequi=cadena_productosDRequi.substring(0,cadena_productosDRequi.length-1);
		cadena_productosDRequi+='),';
	}
	
	
	cadena_productosDRequi = cadena_productosDRequi.substring(0,cadena_productosDRequi.length-1);
	cadena_productosDRequi += ')';
	
	location.href='armaOrdenDeCompra.php?productos='+cadena_productosDRequi;
}



//Verifica que un producto no exista en la taba de Productos para la orden de compra
function changeTabIndexOnTable()
{
	var tabindexs=15;
	var table=document.getElementById('menus');
	
	var existe=false;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length && existe==false; i++) 
	{  //Iterate through all but the first row
		for (var j = 0; j <table.rows[i].cells.length && existe==false ;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="cantidad"+i)
				{
					mynode.tabIndex= tabindexs;
					//cell.tabindex=tabindexs;
					tabindexs = tabindexs+i+1;
					
				}	

			}

		}
	}
	
	//alert("tabindexs="+tabindexs);
	for (var i = 0; i < table.rows.length && existe==false; i++) 
	{  //Iterate through all but the first row
		for (var j = 0; j <table.rows[i].cells.length && existe==false ;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="presUniprovA"+i)
				{
					mynode.tabIndex= tabindexs;
					tabindexs=tabindexs+i+1;
				}

			}
		}
	}
}

function revisaSaldo(saldo, object)
{
	if(saldo<object.value)
	{	
		object.value=saldo;
		alert("La cantidad debe del producto debe ser menor o igual a "+ saldo);
		object.focus()
	}
}


function calcularTotal(objecta , e , nameObjCantProd , nameobjectchange)
{

	var preciounitario = objecta.value;
	var cantidad = document.getElementById(nameObjCantProd).value; //elemcantidad.value;
	
	
	if((e.keyCode >=48 && e.keyCode<=57) || e.keyCode==44 || e.keyCode==46 || (e.keyCode>=96 && e.keyCode<=105))
	{
		//alert(e.keyCode);
		asignarFormato(objecta);
	}
	
	//alert( redondeo2decimales( parseFloat( preciounitario.replace(/,/g,"") ) * parseFloat(cantidad) ) +"=" + parseFloat( preciounitario.replace(/,/g,"") ) + "*"+parseFloat(cantidad)  );
	document.getElementById(nameobjectchange).value = redondeo2decimales( parseFloat( preciounitario.replace(/,/g,"") ) * parseFloat(cantidad) );
	asignarFormato(document.getElementById(nameobjectchange));
	//setCaretToPos(objecta,objecta.value.length);
	sumTotalDProveedores();
}



function asignarFormato(objecto)
{
	if(objecto.value.indexOf(".",0)>0)
	{
		var numero = new oNumero(objecto.value.replace(/,/g,""));
		
		var numDec = (objecto.value.length-1) - objecto.value.indexOf(".",0);
		if(numDec<=2)
			objecto.value= numero.formato(numDec,true);
		else
			objecto.value= numero.formato(2,true);
	}
	else
	{
		var numero = new oNumero(objecto.value.replace(/,/g,""));
		objecto.value= numero.formato(-1,true);
	}
}

function calcularSubtotalEnRenglon(contRow)
{
		var table=document.getElementById("menus");
		
		document.getElementById('cantxPresUniA'+contRow).value =  new oNumero(redondeo2decimales(parseFloat(document.getElementById('presUniprovA'+contRow).value.replace(/,/g,"")) * parseFloat(document.getElementById('cantidad'+contRow).value.replace(/,/g,"")))).formato(2,true);
		
		//document.getElementById('cantidad'+contRow).value=  new oNumero(document.getElementById('cantidad'+contRow).value).formato(2,true);
		sumTotalDProveedores();
}



//Calcula el subtotal de los proveedores
function sumTotalDProveedores()
{
	var table=document.getElementById("menus");
	
	var existe=false;
	
	var sumprov1 = 0.0;

	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length && existe==false; i++) 
	{  //Iterate through all but the first row
		for (var j = 0; j <table.rows[i].cells.length && existe==false ;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="cantxPresUniA"+i)
					if(mynode.value.length>0)
						sumprov1 = sumprov1 + parseFloat(mynode.value.replace(/,/g,""));					
			}
		}
	}
	
	document.getElementById('totalprov1').value = redondeo2decimales(sumprov1);
	asignarFormato(document.getElementById('totalprov1'));
}

function redondeo2decimales(numero)
{
	var original=parseFloat(numero);
	var result=Math.round(original*100)/100 ;
	return result;
}