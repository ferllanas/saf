<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$datos=array();
$folio=0;
$resguarda="";
$ugastos="";
$depa='';  // Aqui trae la informacion del Depto, sin embargo va a haber necesidad de cambiarlo o ajustarlo al que ya se tiene en cheques
$iva=0.00;
$totfact=0.00;
$efectivo=0.00;
$comprobado=0.00;
$difxdevolver=0.00;
$nombre="";
$hoy = date("d/m/Y");

// Obtenemos y traducimos el nombre del d�a
$dia=date("l");
if ($dia=="Monday") $dia="Lunes";
if ($dia=="Tuesday") $dia="Martes";
if ($dia=="Wednesday") $dia="Mi�rcoles";
if ($dia=="Thursday") $dia="Jueves";
if ($dia=="Friday") $dia="Viernes";
if ($dia=="Saturday") $dia="Sabado";
if ($dia=="Sunday") $dia="Domingo";

// Obtenemos el n�mero del d�a
$dia2=date("d");

// Obtenemos y traducimos el nombre del mes
$mes=date("F");
if ($mes=="January") $mes="Enero";
if ($mes=="February") $mes="Febrero";
if ($mes=="March") $mes="Marzo";
if ($mes=="April") $mes="Abril";
if ($mes=="May") $mes="Mayo";
if ($mes=="June") $mes="Junio";
if ($mes=="July") $mes="Julio";
if ($mes=="August") $mes="Agosto";
if ($mes=="September") $mes="Setiembre";
if ($mes=="October") $mes="Octubre";
if ($mes=="November") $mes="Noviembre";
if ($mes=="December") $mes="Diciembre";

// Obtenemos el a�o
$ano=date("Y");

// Imprimimos la fecha completa
// echo "$dia $dia2 de $mes de $ano";



/*if ($conexion)
{		
	$consulta = "select c.folio, a.nomdepto, a.nomcoord ,b.nombre, b.appat, b.apmat, c.total, c.falta from nominamdepto a inner join ";
	$consulta .= "nominadempleados b on a.depto=b.depto inner join ";
	$consulta .= "egresosmsolchegtosxcomp c on c.usuario=b.numemp ";
	$consulta .= "order by c.folio";	
	echo ($consulta);

	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['folio']= trim($row['folio']);
		$datos[$i]['nomempcheq']=trim($row['nombre']) .' '. trim($row['appat']) .' '. trim($row['apmat']);		
		$i++;
	}
}*/

if(isset($_REQUEST['subtotal']))
	$subtotal = $_REQUEST['subtotal'];
if(isset($_REQUEST['iva']))
	$iva = $_REQUEST['iva'];
if(isset($_REQUEST['efectivo'])) 
	$efectivo = $_REQUEST['efectivo'];	

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Comprobacion de Gastos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/cheque_fun.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<script language="javascript" src="javascript/calcula_iva.js"></script>
<script language="javascript" src="javascript/calcula_importes.js"></script>
<script language="javascript" src="javascript/compgastosinc.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>
<form name="form_comprueba_gastos" method="post" action="">
  <p align="center" class="texto10"><strong>DIRECCION DE ADMINISTRACION Y FINANZAS<BR>COORDINACION DE EGRESOS Y CONTRIOL PATRIMONIAL</strong></p>
<p align="center" class="texto8">COMPROBACION DE GASTOS<br><hr></p>
<input type="hidden" name="folio" id="folio">
<input type="hidden" name="nombre" id="nombre">
<table width="100%" border="0">
  <tr class="texto9">
    <td width="9%">No. Cheque :</td>
    <td width="11%"><label>
      <input type="text" name="ch" id="ch" width="80" size="15%" height="10%" class="caja_toprint texto8"> 
      </label></td>
    
    
    
    
    
    <td width="5%">
      <div align="left"  style="z-index:3; position:absolute; width:540px; top: 98px; left: 98px; height: 21px;">
        <input name="ugastos" type="text" class="texto8" id="ugastos" tabindex="7" style="background:#EBEBEB" onKeyUp="searchcompgast(this);" size="70%" autocomplete="off" height="12">
        <a href="javascript:activaCheque()">Habilitar campo</a>
        <div id="search_suggestcompgast" style="z-index:4; "></div>
      </div></td>    
    
    
    
    
    
    
    <td width="0%">&nbsp;</td>
    <td width="0%">&nbsp;</td>
    <td width="0%">&nbsp;</td>
    <td width="0%">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td width="0%">&nbsp;</td>
    <td width="0%">&nbsp;</td>
    <td width="54%" align="right">Departamento:      <input class="texto8" type="text" style="background:#EBEBEB" name="depa" id="depa" size="70%" readonly></td>
    <td width="11%">Fecha del cheque: </td>
    <td width="10%"><input type="text" name="fcheque" id="fcheque" style="background:#EBEBEB" size="12%" class="texto8" readonly></td>
    </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="2"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" class="texto9">Fecha de Comprobaci&oacute;n :</td>
    <td colspan="6" class="texto9"><input type="text" align="absmiddle" style="border:0 #EBEBEB" readonly name="fcomp" id="fcomp" size="40%" class="texto9" value="<?php echo "$dia $dia2 de $mes de $ano"?>">      
    </td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">Fecha de Evento</td>
    <td class="texto9"><input type="text" name="fevento" id="fevento" readonly size="12%" class="texto8">
    <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%y/%m/%d"" align="absmiddle">
      <label>
        <script type="text/javascript">
						Calendar.setup({
							inputField     :    "fevento",		// id of the input field
							ifFormat       :    "%d/%m/%Y",		// format of the input field
							button         :    "f_trigger2",	// trigger for the calendar (button ID)
							//onClose        :    fecha_cambio,
							singleClick    :    true
						});
		</script>
      </label>    
    </td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td colspan="2" class="texto9">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="16" class="texto10"><strong>REALIZA EL DESGLOSE DE GASTOS </strong></td>
  </tr>
  <tr>
    <td><span class="texto9"><span class="texto8">R.F.C.:</span></span></td>
    <td class="texto9"><input type="text" name="rfc" id="rfc" size="15%" class="mayus10"></td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9"><span class="texto8">FACTURA:</span></td>
    <td class="texto9"><span class="texto8">
      <input type="text" name="factura" id="factura" size="19%" class="mayus10" onBlur="valida_factura()">
      </span></td>
    <td class="texto9"><span class="texto8">FECHA</span> :</td>
    <td class="texto9"><input type="text" readonly name="fdetalle" id="fdetalle" size="12%"  class="texto8">
    <label>
    <img src="../../calendario/img.gif" alt="" width="16" height="16""" align="absmiddle" id="f_trigger3" style="cursor: pointer;" title=""%Y/%m/%d>
    <script type="text/javascript">
                                Calendar.setup({
                                    inputField     :    "fdetalle",		// id of the input field
                                    ifFormat       :    "%d/%m/%Y",		// format of the input field
                                    button         :    "f_trigger3",	// trigger for the calendar (button ID)
                                    //onClose        :    fecha_cambio,
                                    singleClick    :    true
                                });
                            </script>
      </label>
    
    </td>
    <td class="texto9"><span class="texto8">DESCRIPCION:</span></td>
    <td colspan="7" class="texto9"><span class="texto8">
      <input type="text" name="descrip" id="descrip" size="100%" align="right"  class="texto8">
      </span></td>
  </tr>
  <tr>
    <td colspan="16">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="texto9"><div align="right"><span class="texto8">SUBTOTAL:</span></div></td>
    <td class="texto9"><span class="texto8">
      <input type="text" tabindex="11" name="subtotal" value="0.00" id="subtotal" size="13%" onBlur="cal_iva()" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"></span></td>
    <td class="texto9" align="right"><span class="texto8">I.V.A.:</span></td>
    <td class="texto9"><span class="texto8">
      <input type="text" tabindex="14" name="iva"  id="iva" value="0.00" size="13%" readonly onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8">    </span></td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9"><span class="texto8">TOTAL:</span></td>
    <td class="texto9"><span class="texto8">
      <input type="text" name="total" id="total" size="13%" onKeyPress="return aceptarSoloNumeros(this, event);" value="0.00" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"></span>
      <td colspan="2"><img src="../../imagenes/agregar2.jpg" width="30" height="25" title="Agrega Nuevo Comprobante" onClick="agregar()"><span style="visibility:text "><img src="../../imagenes/eliminar.jpg" onClick="javascript:; var rowindexa=this.parentNode.parentNode.rowIndex-1;	$('datos').deleteRow(rowindexa);" title="Cancela comprobante de gasto"><img src="../../imagenes/guardar2.jpg" width="30" height="25" title="Actualiza Datos de Comprobante." onClick="actualiza_datos()"></td>
      <input type="hidden" name="usu_depto" id="usu_depto">
    </tr>
  </table>
<table name="enc" id="enc" width="100%" height="10%" align="center" border="1">
  <tr>
    <th width="10%" align="center" class="subtituloverde">R.F.C.</th>
    <th width="8%" align="center" class="subtituloverde">Factura</th>
    <th width="8%" align="center" class="subtituloverde">Fecha</th>
    <th width="30%" align="center" class="subtituloverde">Descipcion</th>
    <th width="8%" align="center" class="subtituloverde">Subtotal</th>
    <th width="8%" align="center" class="subtituloverde">I.V.A.</th>
    <th width="8%" align="center" class="subtituloverde">Total</th>
    </tr>
</table>
<div style="overflow:auto; width:auto; height: 150px;  align:center;"> 
	<table name="master" id="master" width="90%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
    <p>&nbsp;</p>
</div>
<hr>
</form>
<table width="88%" border="0" align="right">
  <tr>
    <td width="46%" height="24" class="texto8"><strong>CAPTURA LOS NUMEROS DE RECIBO</strong></td>
    <td width="21%">COMPROBADO:</td>
    <td width="33%"><form name="form_compgastos_detalle" method="post" action="php_ajax/compgastos.php">
      <label>
        <input type="text" name="comprobado" style="background-color:#EBEBEB" id="comprobado" class="texto8" value=0.00 onKeyUp="asignaFormatoSiesNumerico(this, event)" readonly> 
      </label>
    </form></td>
  </tr>
  <tr>
    <td rowspan="3"><label>
      <textarea name="no_recibos2" id="no_recibos2" cols="45" rows="2" style="width:80%"></textarea>
    </label></td>
    <td>EFECTIVO DEVUELTO:</td>
    <td><form name="form_efectivo" method="post" action="">
      <label>
        <input type="text" name="efectivo" id="efectivo" class="texto8" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')" value=0.00  onKeyPress="return aceptarSoloNumeros(this, event);">
      </label>
    </form></td>
  </tr>
  <tr>
    <td>IMPORTE DEL CHEQUE:</td>
    <td><form name="form_totales" method="totales" action="">
      <label><strong>
	  <input type="text" name="totcheque" style="background-color:#EBEBEB" id="totcheque" value=0.00 readonly class="texto8" onBlur="this.value = NumberFormat(this.value, '2', '.', ',')">        
        
        
        
        
        
      </strong></label>
    </form></td>
  </tr>
  <tr>
    <td>DIFERENCIA POR DEPOSITAR O DEVOLVER:</td>
    <td><form name="form_subtotales" method="subtotales" action="">
      <label>
        <input type="text" name="difxdevolver" id="difxdevolver" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" style="background-color:#EBEBEB" value=0.00 class="texto8" onKeyUp="asignaFormatoSiesNumerico(this, event)" readonly>
      </label>
    </form></td>
  </tr>
</table>
</body>
</html>
