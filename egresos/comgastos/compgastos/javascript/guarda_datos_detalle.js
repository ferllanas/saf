function valida_total()
{
var val_fac = parseFloat($('totcheque').value.replace(/,/g,''));
var val_tot = parseFloat($('comprobado').value.replace(/,/g,''))+parseFloat($('efectivo').value.replace(/,/g,''));
var total_final = val_tot - val_fac;
	if(val_tot > val_fac )
	{
		confirmar=confirm("Has excedido el importe del cheque por " + total_final + " pesos y se generara una solicitud de Cheque. ¿Estas de acuerdo?");
		if (confirmar) 
		{// si pulsamos en aceptar
//			window.location.href = 'cheques/solcheque.php?$folio=' + document.solcheque.folio.value;
			//showdiv();
			guarda_datos();}
		else 
			// si pulsamos en cancelar
			alert('Verifica bien la comprobacion de gastos'); 
			return false;	
    } 
	else 
	{guarda_datos();}
}

function guarda_datos()
{   // Comenzamos con la validacion de informacion del Master
//	valida_total(); // Va validar Importe del Cheque y el Saldo comprobado
// alert ("Entra guardar datos");
	var ugastos  	= document.getElementById('ugastos').value;
	var depa 	 	= document.getElementById('depa').value;       // Oculto
	var depto 	 	= document.getElementById('depto').value;       // Oculto	
	var ddepto 	 	= document.getElementById('ddepto').value;       // Oculto		
	var fcheque  	= document.getElementById('fcheque').value;
	var fcomp 	 	= document.getElementById('fcomp').value;
	var fevento	 	= document.getElementById('fevento').value;	
	var drfc 	 	= document.getElementById('drfc').value;
	var dfactura  	= document.getElementById('dfactura').value;
	var dfdetalle 	= document.getElementById('dfdetalle').value;
	var ddescrip  	= document.getElementById('ddescrip').value;
	var dsubtotal 	= document.getElementById('dsubtotal').value;
	var diva 	 	= document.getElementById('diva').value;       // Oculto
	var dtotal 	 	= document.getElementById('dtotal').value;   // Oculto
	var comprobado 	= document.getElementById('comprobado').value;   // Oculto
	var totcheque   = document.getElementById('totcheque').value; // Oculto
	var efectivo 	= document.getElementById('efectivo').value; // Oculto
	var no_recibo 	= document.getElementById('no_recibo').value; // Oculto
	var folio		= document.getElementById('folio').value; // Oculto	 // Este campo va a ser folio_solche en el SP
	var cheque		= document.getElementById('cheque').value; // Oculto	
	var chautomatico= document.getElementById('chautomatico').value; // Oculto	
	var folio_chauto= document.getElementById('folio_chauto').value; // Oculto		
	var nombre		= document.getElementById('nombre').value; // Oculto	
	var usuario		= document.getElementById('usuario').value; // Oculto	
	var dusuario		= document.getElementById('dusuario').value; // Oculto		
	var uresp		= document.getElementById('uresp').value; // Oculto		
	var dfolio		= document.getElementById('dfolio').value; // Oculto			
	var tabla= $('datos');
	var TotaComp= new Array( );
	var countPartidas=0;	
	
	/*alert (dfolio); 	
	alert (folio); 
	alert (cheque); 
	alert (comprobado); 
	alert (efectivo);
	alert (no_recibo);
	alert (chautomatico);
	alert (folio_chauto);
	alert (nombre);
	alert (usuario);
	alert (uresp);*/
	// Deben de ir los valores del Detalle para poder obtener los comprobantes de Gastos
	for (var i = 0; i < tabla.rows.length; i++) 
	{  		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="dfolio")
				{
					spdfolio=mynode.value;
					//alert (spdfolio);
				}
				if(mynode.name=="drfc")
				{
					spdrfc=mynode.value;
					//alert (spdrfc);					
				}
				if(mynode.name=="dfactura")
				{
					spdfactura=mynode.value;
					//alert (spdfactura);					
				}
				if(mynode.name=="dfdetalle")
				{
					spdfdetalle=mynode.value;
					//alert (spdfdetalle);					
				}
				if(mynode.name=="ddescrip")
				{
					spddescrip=mynode.value;
					//alert (spddescrip);					
				}
				if(mynode.name=="dsubtotal")
				{
					spdsubtotal=mynode.value;
					//alert (spdsubtotal);					
				}
				if(mynode.name=="diva")
				{
					spdiva=mynode.value;
					//alert (spdiva);					
				}
				if(mynode.name=="dtotal")
				{
					spdtotal=mynode.value;
					//alert (spdtotal);					
				}
				if(mynode.name=="ddepto")
				{
					spddepto=mynode.value;
					//alert (spddepto);					
				}
				if(mynode.name=="dusuario")
				{
					spdusuario=mynode.value;
					//alert (spdusuario);					
				}				
			}
		}
		TotaComp[countPartidas] = new Object;
		TotaComp[countPartidas]['spdfolio']		= spdfolio;			
		TotaComp[countPartidas]['spdrfc']		= spdrfc;			
		TotaComp[countPartidas]['spdfactura']	= spdfactura;
		TotaComp[countPartidas]['spdfdetalle']	= spdfdetalle;
		TotaComp[countPartidas]['spddescrip']	= spddescrip;
		TotaComp[countPartidas]['spdsubtotal']	= spdsubtotal;
		TotaComp[countPartidas]['spdiva']		= spdiva;
		TotaComp[countPartidas]['spdtotal']		= spdtotal;
		TotaComp[countPartidas]['spddepto']		= spddepto;
		TotaComp[countPartidas]['spdusuario']	= spdusuario;		
		countPartidas++;
	}
	var datas = {
		// Variables del Master
				folio		  	:folio,
				cheque  	 	:cheque,
				fcomp 	 		:fcomp,
				fevento 	 	:fevento,				
				efectivo 		:efectivo,
				comprobado 		:comprobado,
				totcheque   	:totcheque,
				no_recibo 		:no_recibo,
				chautomatico	:chautomatico,
				folio_chauto	:folio_chauto,
				nombre			:nombre,    // En el SP es usolic
				usuario			:usuario,
				uresp			:uresp,
		// Variables del Detalle	
				spdfolio	: spdfolio,		
				spdrfc		: spdrfc,
				spdfactura	: spdfactura,
				spdfdetalle	: spdfdetalle,
				spddescrip	: spddescrip,
				spdsubtotal	: spdsubtotal,
				spdiva		: spdiva,
				spdtotal	: spdtotal,
				spdusuario	: spdusuario,
				spddepto	: spddepto,
				prods		: TotaComp
    };
	//alert ("Antes de irme al AJAX");
	jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/compgastos_detalle_pdf2.php',
				data:          datas,
				success: function(resp) 
				{
					if( resp.length ) 
					{
						alert(resp);
						var myArray = eval(resp);
						if(myArray.length>0)
						{
								//alert(myArray[0].string);
								//resp=resp.replace('"','');
								resp=resp.replace(/"/g,'');
								window.open("pdf_files/comp_gastos_"+resp+".pdf");
								location.href='compgastos.php';
						}
					}
				}
			});
}