<?php
/*if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../dompdf/dompdf_config.inc.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$totcheque 		= $_REQUEST['totcheque'];
//$fcheque 		= $_REQUEST['fcheque']; Verfiicar por que marca error
$fcomp 			= $_REQUEST['fcomp'];
$fevento 		= $_REQUEST['fevento'];
$fcomp 			= $_REQUEST['fcomp'];
$fevento		= $_REQUEST['fevento'];
$comprobado		= $_REQUEST['comprobado'];
$efectivo 		= $_REQUEST['efectivo'];     // El nombre del campo en el SP es deposito
// $totchequegen 	= $_REQUEST['totchequegen'];
$no_recibo	 	= $_REQUEST['no_recibo']; 	 // El nombre del campo en el SP es recibos
// Checa como vas a obtener esta informacion
// $folio_solche 	= $_REQUEST['folio_solche']; // En caso de que se genere un cheque se ingresara en este campo
// $cheque		 	= $_REQUEST['cheque'];		 // Investigar
// $chautomatico 	= $_REQUEST['chautomatico']; // Folio del Cheque
// $folio_chauto 	= $_REQUEST['folio']; 		 // Este 
$folioch		 	= $_REQUEST['folio']; 		 // Folio del Cheque solicitado
$usolic		 	= $_REQUEST['nombre']; // El nombre del solicitante es usolic en el SP
$usuario	 	= $_REQUEST['usuario'];
$uresp		 	= $_REQUEST['uresp'];
//------------------------------------ Info del Detalle
$spddepto	 		= $_REQUEST['spddepto'];
$spdfolio	 		= $_REQUEST['spdfolio'];
$spdrfc 		 	= $_REQUEST['spdrfc'];
$spdfactura		 	= $_REQUEST['spdfactura'];
$spddescrip		 	= $_REQUEST['spddescrip'];
$spdfdetalle	 	= $_REQUEST['spdfdetalle'];
$spdsubtotal	 	= $_REQUEST['spdsubtotal'];
$spdiva		 		= $_REQUEST['spdiva']; 
$spdtotal	 		= $_REQUEST['spdtotal'];
// $formato=convertirFechaEuropeoAAmericano($_REQUEST['formato']);
if ($conexion)
{
	$ejecuta ="{call sp_egresos_A_mcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$folio_solche,&$cheque,&$comprobado,&$efectivo,&$no_recibo,&$chautomatico,&$folio_chauto,&$usolic,&$usuario,&$uresp);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	// echo $ejecuta;
	// print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
//			$error 	= $row['error'];
//			$datos[0]['error'] = $error;
			$folio = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$dir 	= $row['dir'];
			
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
		}
		sqlsrv_free_stmt( $R);
	}
}

$xxprods=array();
if(isset($_REQUEST['prods']))
$xxprods=$_REQUEST['prods'];
$i=0;
for($i=0;$i<count($xxprods);$i++)
{
	$spdfolio		= $xxprods[$i]['spdfolio'];
	$spdrfc			= $xxprods[$i]['spdrfc'];
	$spdfactura		= $xxprods[$i]['spdfactura'];
	$spdfdetalle	= convertirFechaEuropeoAAmericano($xxprods[$i]['spdfdetalle']);	
	$spddescrip		= $xxprods[$i]['spddescrip'];
	$spdsubtotal	= str_replace(',','',$xxprods[$i]['spdsubtotal']);	
	$spdiva			= str_replace(',','',$xxprods[$i]['spdiva']);	
	$spdtotal		= str_replace(',','',$xxprods[$i]['spdtotal']);	
	$spdusuario		= $xxprods[$i]['spdusuario'];	 // Este no se si se deba de contemplar en el SP
	$spddepto		= $xxprods[$i]['spddepto']; 	// Este no se si se deba de contemplar en el SP

	
	$ejecuta ="{call sp_egresos_A_dcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$spdfolio,&$spdrfc,&$spdfactura,&$spdfdetalle,&$spddescrip,&$spdsubtotal,
					   &$spdiva,&$spdtotal,&$spdusuario,&$spddepto);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	if( $R === false )
	{
		 $fails= "Error in (detalle) statement execution.\n";
		 $fails=true;
		 //print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
}*/
$html="
<DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>REPORTE DE COMPROBACION DE GASTOS</title>
<link href='../../../css/estilos.css' type='text/css' rel='stylesheet' />
<link rel='stylesheet' type='text/css' media='all' href='../../../calendario/skins/aqua/theme.css' title='Aqua' />
<script language='javascript' src='../javascript/calcula_iva.js'></script>
<script language='javascript' src='../javascript/calcula_importes.js'></script>
<script language='javascript' src='../../../prototype/jQuery.js'></script>
<script language='javascript' src='../../../prototype/prototype.js'></script>
<script language='javascript' src='../../../javascript_globalfunc/funciones_AHojaDEstilos.js'></script>
<script language='javascript' src='../../../javascript_globalfunc/funcionesGlobales.js'></script>
</head>

<body>";
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../dompdf/dompdf_config.inc.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$totcheque 		= $_REQUEST['totcheque'];
//$fcheque 		= $_REQUEST['fcheque']; Verfiicar por que marca error
$fcomp 			= $_REQUEST['fcomp'];
$fevento 		= $_REQUEST['fevento'];
$fcomp 			= $_REQUEST['fcomp'];
$fevento		= $_REQUEST['fevento'];
$comprobado		= $_REQUEST['comprobado'];
$efectivo 		= $_REQUEST['efectivo'];     // El nombre del campo en el SP es deposito
// $totchequegen 	= $_REQUEST['totchequegen'];
$no_recibo	 	= $_REQUEST['no_recibo']; 	 // El nombre del campo en el SP es recibos
// Checa como vas a obtener esta informacion
// $folio_solche 	= $_REQUEST['folio_solche']; // En caso de que se genere un cheque se ingresara en este campo
// $cheque		 	= $_REQUEST['cheque'];		 // Investigar
// $chautomatico 	= $_REQUEST['chautomatico']; // Folio del Cheque
// $folio_chauto 	= $_REQUEST['folio']; 		 // Este 
$folioch		 	= $_REQUEST['folio']; 		 // Folio del Cheque solicitado
$usolic		 	= $_REQUEST['nombre']; // El nombre del solicitante es usolic en el SP
$usuario	 	= $_REQUEST['usuario'];
$uresp		 	= $_REQUEST['uresp'];
//------------------------------------ Info del Detalle
$spddepto	 		= $_REQUEST['spddepto'];
$spdfolio	 		= $_REQUEST['spdfolio'];
$spdrfc 		 	= $_REQUEST['spdrfc'];
$spdfactura		 	= $_REQUEST['spdfactura'];
$spddescrip		 	= $_REQUEST['spddescrip'];
$spdfdetalle	 	= $_REQUEST['spdfdetalle'];
$spdsubtotal	 	= $_REQUEST['spdsubtotal'];
$spdiva		 		= $_REQUEST['spdiva']; 
$spdtotal	 		= $_REQUEST['spdtotal'];
// $formato=convertirFechaEuropeoAAmericano($_REQUEST['formato']);
if ($conexion)
{
	$ejecuta ="{call sp_egresos_A_mcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$folio_solche,&$cheque,&$comprobado,&$efectivo,&$no_recibo,&$chautomatico,&$folio_chauto,&$usolic,&$usuario,&$uresp);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	// echo $ejecuta;
	// print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
//			$error 	= $row['error'];
//			$datos[0]['error'] = $error;
			$folio = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$dir 	= $row['dir'];
			
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
		}
		sqlsrv_free_stmt( $R);
	}
}

$xxprods=array();
if(isset($_REQUEST['prods']))
$xxprods=$_REQUEST['prods'];
$i=0;
for($i=0;$i<count($xxprods);$i++)
{
	$spdfolio		= $xxprods[$i]['spdfolio'];
	$spdrfc			= $xxprods[$i]['spdrfc'];
	$spdfactura		= $xxprods[$i]['spdfactura'];
	$spdfdetalle	= convertirFechaEuropeoAAmericano($xxprods[$i]['spdfdetalle']);	
	$spddescrip		= $xxprods[$i]['spddescrip'];
	$spdsubtotal	= str_replace(',','',$xxprods[$i]['spdsubtotal']);	
	$spdiva			= str_replace(',','',$xxprods[$i]['spdiva']);	
	$spdtotal		= str_replace(',','',$xxprods[$i]['spdtotal']);	
	$spdusuario		= $xxprods[$i]['spdusuario'];	 // Este no se si se deba de contemplar en el SP
	$spddepto		= $xxprods[$i]['spddepto']; 	// Este no se si se deba de contemplar en el SP

	
	$ejecuta ="{call sp_egresos_A_dcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$spdfolio,&$spdrfc,&$spdfactura,&$spdfdetalle,&$spddescrip,&$spdsubtotal,
					   &$spdiva,&$spdtotal,&$spdusuario,&$spddepto);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	if( $R === false )
	{
		 $fails= "Error in (detalle) statement execution.\n";
		 $fails=true;
		 //print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
} 
$html.="
<table width='100%' border='0'>
<table width='100%' border='0'>
  <tr>
    <td><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
    <td class='texto8B' align='center'>DIRECCION DE ADMINISTRACION Y FINANZAS <BR />COORDINACION DE EGRESOS Y CONTROL PATROMONIAL<BR />COMPROBACION DE GASTOS</td>
    <td align='right'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
  </tr>
</table>
<table width='100%' border='0'>
  <tr>
    <td width='18%' align='right'><strong>FECHA DE COMPROBACION :    </strong></td>
    <td width='35%'><input type='text' name='repfcomp' id='repfcomp'  />'</td>
    <td width='19%' align='right'><strong>FECHA DE EVENTO :</strong></td>
    <td width='28%'><input type='text' name='repfcomp3' id='repfcomp3' /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align='right'><strong>NUMERO DE CHEQUE :</strong></td>
    <td><input type='text' name='repugastos' id='repugastos' value=". $folio ." /></td>
    <td align='right'><strong>FECHA DE CHEQUE :</strong></td>
    <td><input type='text' name='repfcheque' id='repfcheque' value=". $folio ." /></td>
  </tr>
</table>
<div style='overflow:auto; width:auto; height: 550px;  align:center;'> 
<table width='100%' border='0'>
  <tr class='subtituloverde'>
    <td class=''>R.F.C.</td>
    <td>FACTURA</td>
    <td>FECHA</td>
    <td>DESCRIPCION</td>
    <td>SUBTOTAL</td>
    <td>I.V.A.</td>
    <td>TOTAL</td>
  </tr>
  <tbody style='width:100%'>
  </tbody>
</table>
</div>  
<table width='100%' border='0'>
  <tr>
    <td width='57%' colspan='2' class='textom8'><strong class='texto8'>No. DE RECIBOS : ". $no_recibo ."</strong></td>
    <td width='28%' colspan='3' class='textom8'>COMPROBADO : ". $comprobado ." </td>
    <td width='15%' colspan='2' class='textom8'><input type='text' name='repNoRecibos2' id='repNoRecibos2' ". $no_recibo ."/></td>
  </tr>
  <tr>
    <td colspan='2' rowspan='3' class='textom8'><textarea name='repNoRecibos' cols='78' rows='4' readonly='readonly' id='repNoRecibos'></textarea></td>
    <td colspan='3' class='textom8'>EFECTIVO DEVUELTO :". $efectivo ."</td>
    <td colspan='2' class='textom8'><input type='text' name='repNoRecibos3' id='repNoRecibos3' /></td>
  </tr>
  <tr>
    <td colspan='3' class='textom8'> IMPORTE DEL CHEQUE : ". $totcheque ."</td>
    <td colspan='2' class='textom8'><input type='text' name='repNoRecibos4' id='repNoRecibos4' /></td>
  </tr>
  <tr>
    <td colspan='3' class='textom8'>DIFERENCIA POR DEPOSITAR O DEVOLVER: ". $totcheque ."</td>
    <td colspan='2' class='textom8'><input type='text' name='repNoRecibos5' id='repNoRecibos5' /></td>
  </tr>
</table>
<table width='100%' border='0'>
  <tr>
    <td width='38%'>&nbsp;</td>
    <td width='31%'>&nbsp;</td>
    <td width='31%'>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><hr width='70%' align='center' /></td>
    <td><hr width='70%' align='center' /></td>
    <td><hr width='70%' align='center' /></td>
  </tr>
  <tr>
    <td align='center'><strong>SOLICITA</strong></td>
    <td align='center'><strong>DIRECTOR</strong></td>
    <td align='center'><strong>DIRECCION DE ADMINISTRACION Y FINANZAS</strong></td>
  </tr>
</table>
</table>
</body>
</html>";

 //echo $html;

	$dompdf = new DOMPDF();	
	$dompdf->set_paper('"letter","portrait"');
	
	$dompdf->load_html($html);
	$dompdf->render();

// De aqui envia la informacion a recibir en el jQuery.Ajax
// Se debe de tener cuidado con todos los echo, alert y demas avizos que haya en el PHP o JS, ya que pueden llegar a filtrar informacion equivocada a la forma
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/comp_gastos_".$folioch.".pdf", $pdf);
	echo json_encode($folioch);
///////////////////////////////////////////////////////////////
?>