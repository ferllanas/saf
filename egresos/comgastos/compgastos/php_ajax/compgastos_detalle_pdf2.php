<?php
$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<link href='../../../css/estilos.css' rel='stylesheet' type='text/css'>
<script language='javascript' src='../../../prototype/jQuery.js'></script>
<script language='javascript' src='../../../prototype/prototype.js'></script>
<script language='javascript' src='../../../javascript_globalfunc/funciones_AHojaDEstilos.js'></script>
<script language='javascript' src='../../../javascript_globalfunc/funcionesGlobales.js'></script>
</head>
<body>";
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once('../../../dompdf/dompdf_config.inc.php');
	require_once('../../../Administracion/globalfuncions.php');
	require_once('../../../connections/dbconexion.php');
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$totcheque 		= $_REQUEST['totcheque'];
$fcomp 			= $_REQUEST['fcomp'];
$fevento 		= $_REQUEST['fevento'];
$fcomp 			= $_REQUEST['fcomp'];
$fevento		= $_REQUEST['fevento'];
$comprobado		= $_REQUEST['comprobado'];
$efectivo 		= $_REQUEST['efectivo'];     // El nombre del campo en el SP es deposito
$no_recibo	 	= $_REQUEST['no_recibo']; 	 // El nombre del campo en el SP es recibos
$folioch		= $_REQUEST['folio']; 		 // Folio del Cheque solicitado
$folio			= $_REQUEST['folio']; 		 // Folio del Cheque solicitado
$usolic		 	= $_REQUEST['nombre']; // El nombre del solicitante es usolic en el SP
$usuario	 	= $_REQUEST['usuario'];
$uresp		 	= $_REQUEST['uresp'];
$ugastos 		= 'CHEQUE No. ' . 123456 . ' A NOMBRE DE ROBERTO M. TAPIA BALLEZA';
$fcomp 			= '01/01/2011';
/*$fcheque 		= '02/01/2011';
$fcomp 			= '03/01/2011';
$fevento		= '04/01/2011';
$comprobado		= 50463;
$efectivo 		= 452;
$totcheque		= 50000;
$no_recibo	 	= 'NO HAY RECIBOS';*/
$difxdevolver 	= 11;
//$fcheque 		= $_REQUEST['fcheque']; Verificar por que marca error
// $totchequegen 	= $_REQUEST['totchequegen'];
// Checa como vas a obtener esta informacion
// $folio_solche 	= $_REQUEST['folio_solche']; // En caso de que se genere un cheque se ingresara en este campo
// $cheque		 	= $_REQUEST['cheque'];		 // Investigar
// $chautomatico 	= $_REQUEST['chautomatico']; // Folio del Cheque
// $folio_chauto 	= $_REQUEST['folio']; 		 // Este 
//------------------------------------ Info del Detalle
$spddepto	 		= $_REQUEST['spddepto'];
$spdfolio	 		= $_REQUEST['spdfolio'];
$spdrfc 		 	= $_REQUEST['spdrfc'];
$spdfactura		 	= $_REQUEST['spdfactura'];
$spddescrip		 	= $_REQUEST['spddescrip'];
$spdfdetalle	 	= $_REQUEST['spdfdetalle'];
$spdsubtotal	 	= $_REQUEST['spdsubtotal'];
$spdiva		 		= $_REQUEST['spdiva']; 
$spdtotal	 		= $_REQUEST['spdtotal'];

/*$spddepto	 		= 1981;
$spdfolio	 		= 52636;
$spdrfc 		 	= 'TABR700124';
$spdfactura		 	= 1253;
$spddescrip		 	= 'COMPROBACION BIATICOS A CANCUN';
$spdfdetalle	 	= 05/04/2011;
$spdsubtotal	 	= 21751.30;
$spdiva		 		= 3480.20; 
$spdtotal	 		= 25231.50;*/
// $formato=convertirFechaEuropeoAAmericano($_REQUEST['formato']);
if ($conexion)
{
	$ejecuta ="{call sp_egresos_A_mcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$folio_solche,&$cheque,&$comprobado,&$efectivo,&$no_recibo,&$chautomatico,&$folioch,&$usolic,&$usuario,&$uresp);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	// echo $ejecuta;
	// print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
//			$error 	= $row['error'];
//			$datos[0]['error'] = $error;
			$folio = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$dir 	= $row['dir'];
			
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
		}
		sqlsrv_free_stmt( $R);
	}
}

$xxprods=array();
if(isset($_REQUEST['prods']))
$xxprods=$_REQUEST['prods'];
$i=0;
for($i=0;$i<count($xxprods);$i++)
{
	$spdfolio		= $xxprods[$i]['spdfolio'];
	$spdrfc			= $xxprods[$i]['spdrfc'];
	$spdfactura		= $xxprods[$i]['spdfactura'];
	$spdfdetalle	= convertirFechaEuropeoAAmericano($xxprods[$i]['spdfdetalle']);	
	$spddescrip		= $xxprods[$i]['spddescrip'];
	$spdsubtotal	= str_replace(',','',$xxprods[$i]['spdsubtotal']);	
	$spdiva			= str_replace(',','',$xxprods[$i]['spdiva']);	
	$spdtotal		= str_replace(',','',$xxprods[$i]['spdtotal']);	
	$spdusuario		= $xxprods[$i]['spdusuario'];	 // Este no se si se deba de contemplar en el SP
	$spddepto		= $xxprods[$i]['spddepto']; 	// Este no se si se deba de contemplar en el SP

	
	$ejecuta ="{call sp_egresos_A_dcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$spdfolio,&$spdrfc,&$spdfactura,&$spdfdetalle,&$spddescrip,&$spdsubtotal,
					   &$spdiva,&$spdtotal,&$spdusuario,&$spddepto);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	if( $R === false )
	{
		 $fails= "Error in (detalle) statement execution.\n";
		 $fails=true;
		 //print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
} 
$html.="
<table width='100%' border='0'>
  <tr class='texto8'>
    <td align='center'><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
    <td align='center' class='texto9'>DIRECCION DE ADMINISTRACION Y FINANZAS<BR />COORDINACION DE EGRESOS Y CONTROL PATRIMONIAL<BR />COMPROBACION</td>
    <td align='center'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
  </tr>
</table>
<table width='100%' border='0'>
  <tr>
    <td width='17%' class='texto9'>FECHA DE COMPROBACION:</td>
    <td width='31%' class='texto9'>". $fcomp ."</td>
    <td width='16%'>&nbsp;</td>
    <td width='12%' class='texto9'>FECHA DE EVENTO:</td>
    <td width='24%' class='texto9'>". $fevento ."</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class='texto9'>NUMERO DE CHEQUE:</td>
    <td class='texto9'>". $ugastos ."</td>
    <td>&nbsp;</td>
    <td class='texto9'>FECHA DE CHEQUE:</td>
    <td class='texto9'></td>
  </tr>
</table>
<table width='100%' border='1'>
  <tr class='subtituloverde'>
    <td width='10%'>RFC</td>
    <td width='11%'>FACTURA</td>
    <td width='11%'>FECHA</td>
    <td width='38%'>DESCRIPCION</td>
    <td width='11%'>SUBTOTAL</td>
    <td width='10%'>IVA</td>
    <td width='9%'>TOTAL</td>
  </tr>
    <td class='texto9'>". $spdrfc ."</td>
    <td class='texto9'>". $spdfactura ."</td>
    <td class='texto9'>". $spdfdetalle ."</td>
    <td class='texto9'>". $spddescrip ."</td>
    <td class='texto9'>".  $spdsubtotal ."</td>
    <td class='texto9'>".  $spdiva ."</td>
    <td class='texto9'>".  $spdtotal ."</td>
</table>
<table width='100%' bordercolor='#CCCCCC' border='0' >
  <tr>
    <td width='73%' class='texto9'>No DE RECIBOS</td>
    <td width='18%' class='texto9'>COMPROBADO:</td>
    <td width='9%' class='texto9'>". $comprobado ."</td>
  </tr>
  <tr>
    <td rowspan='2' bordercolor='#000000' style='border:1px' class='texto9'>". $no_recibo ."</td>
    <td class='texto9'>EFECTIVO DEVUELTO:</td>
    <td class='texto9'>". $efectivo ."</td>
  </tr>
  <tr>
    <td class='texto9'>IMPORTE DE CHEQUE:</td>
    <td class='texto9'>". $totcheque ."</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='texto9'>DIFERENCIA POR DEPOSITAR O POR DEVOLVER</td>
    <td class='texto9'>". $difxdevolver ."</td>
  </tr>
</table>
</body>
</html>";

 echo $html;

	$dompdf = new DOMPDF();	
	$dompdf->set_paper('"letter","portrait"');
	
	$dompdf->load_html($html);
	$dompdf->render();

// De aqui envia la informacion a recibir en el jQuery.Ajax
// Se debe de tener cuidado con todos los echo, alert y demas avizos que haya en el PHP o JS, ya que pueden llegar a filtrar informacion equivocada a la forma
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/comp_gastos_".$folioch.".pdf", $pdf);
	echo json_encode($folioch);
///////////////////////////////////////////////////////////////
?>