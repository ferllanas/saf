<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../dompdf/dompdf_config.inc.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../connections/dbconexion.php");
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
if ($conexion)
{	

//-------------------------------------------------------------------------------------------------------------------------------
	$actualizagtosxcomp=array();
	if(isset($_REQUEST['dgastos']))
		$actualizagtosxcomp=$_REQUEST['dgastos'];
		$usuario=$_REQUEST['usuario'];
		$depto=$_REQUEST['depto'];		
		$i=0;
		for($i=0;$i<count($actualizagtosxcomp);$i++) // Comienza el FOR para anexar informacion al detalle de Comp de Gastos 
				{	
					$spfolio=$actualizagtosxcomp[$i]['spfolio'];
					$sprfc=$actualizagtosxcomp[$i]['sprfc'];
					$spfactura=$actualizagtosxcomp[$i]['spfactura'];
					$spdfdetalle=convertirFechaEuropeoAAmericano($actualizagtosxcomp[$i]['spdfdetalle']);
					$spdescrip=$actualizagtosxcomp[$i]['spdescrip'];
					$spsubtotal=str_replace(',','',$actualizagtosxcomp[$i]['spsubtotal']);	
					$spiva=str_replace(',','',$actualizagtosxcomp[$i]['spiva']);	
					$sptotal=str_replace(',','',$actualizagtosxcomp[$i]['sptotal']);	

					// Comienza el Store Procedure de Gastos por Comprobar
					$ejecuta ="{call sp_egresos_A_mcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
					$variables = array(&$spfolio,&$sprfc,&$spfactura,&$spdfdetalle,&$spdescrip,&$spsubtotal,&$spiva,&$sptotal,&$usuario,&$depto);
					$R = sqlsrv_query($conexion, $ejecuta, $variables);
					if( $R === false )
					{
						 $fails= "Error in (detalle) statement execution.\n";
						 $fails=true;
						 print_r($variables);
						 die( print_r( sqlsrv_errors(), true));
					}
					$fails=false;
					if(!$fails)
					{
							while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
							{
								$error = $row['error'];			
								$datos[0]['error'] = $error;
								$folio = $row['folio'];
								$falta = $row['fecha'];
								$halta = $row['hora'];
								$ccosto = $row['uresp'];
								$uresp = $row['numresp'];
								$nomresp = $row['nomresp'];			
								$arearesp = $row['arearesp'];
								list($mes, $dia, $anio) = explode("/", $falta);
								$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
								$falta=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
							}
							sqlsrv_free_stmt( $R);
					}
				}
}
//-------------------------------------------------------------------------------------------------------------------------------
	$actualizagtosxcomp=array();
	if(isset($_REQUEST['dgastos']))
		$actualizagtosxcomp=$_REQUEST['dgastos'];
		$usuario=$_REQUEST['usuario'];
		$depto=$_REQUEST['depto'];		
		$i=0;
		for($i=0;$i<count($actualizagtosxcomp);$i++) // Comienza el FOR para anexar informacion al detalle de Comp de Gastos 
				{
					$spfolio=$actualizagtosxcomp[$i]['spfolio'];
					$sprfc=$actualizagtosxcomp[$i]['sprfc'];
					$spfactura=$actualizagtosxcomp[$i]['spfactura'];
					$spdfdetalle=convertirFechaEuropeoAAmericano($actualizagtosxcomp[$i]['spdfdetalle']);
					$spdescrip=$actualizagtosxcomp[$i]['spdescrip'];
					$spsubtotal=str_replace(',','',$actualizagtosxcomp[$i]['spsubtotal']);	
					$spiva=str_replace(',','',$actualizagtosxcomp[$i]['spiva']);	
					$sptotal=str_replace(',','',$actualizagtosxcomp[$i]['sptotal']);	

					// Comienza el Store Procedure de Gastos por Comprobar
					$ejecuta ="{call sp_egresos_A_dcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
					$variables = array(&$spfolio,&$sprfc,&$spfactura,&$spdfdetalle,&$spdescrip,&$spsubtotal,&$spiva,&$sptotal,&$usuario,&$depto);
					$R = sqlsrv_query($conexion, $ejecuta, $variables);
					if( $R === false )
					{
						 $fails= "Error in (detalle) statement execution.\n";
						 $fails=true;
						 print_r($variables);
						 die( print_r( sqlsrv_errors(), true));
					}
					$fails=false;
					if(!$fails)
					{
							while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
							{
								$error = $row['error'];			
								$datos[0]['error'] = $error;
								$folio = $row['folio'];
								$falta = $row['fecha'];
								$halta = $row['hora'];
								$ccosto = $row['uresp'];
								$uresp = $row['numresp'];
								$nomresp = $row['nomresp'];			
								$arearesp = $row['arearesp'];
								list($mes, $dia, $anio) = explode("/", $falta);
								$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
								$falta=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
							}
							sqlsrv_free_stmt( $R);
					}
				}
//-------------------------------------------------------------------------------------------------------------------------------
$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
	{
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Documento sin t&iacute;tulo</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../../prototype/prototype.js'></script>
</head>

<body>
";
/////
	$nomprov = $_REQUEST['nomprov'];
	$subtotal = $_REQUEST['subtotal'];
	$iva = $_REQUEST['iva'];
	$neto = $_REQUEST['total'];
	$nomcoord = $_REQUEST['nomcoord'];
	$nomdir = $_REQUEST['nomdir'];
	$nomfirma = $_REQUEST['nomfirma'];
	$netox = str_replace(',','',$_REQUEST['total']);
	$neto_let = num2letras($netox);
	$detalle = "Subtotal: $".$subtotal.", Iva: $".$iva.", Total: $".$total;
/////
$html.="

<table width='600' border='0' align='center'>
   	    <tr>
		  <td width='100%'>
             <table width='100%' border='0'>
				<p></p>
				  <tr>
					<td align='left' width='20'><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                ".htmlentities("SOLICITUD DE CHEQUE - GASTOS POR COMPROBAR")." - </b></td>
					<td align='right' width='20'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
				  </tr>
		      </table> 
						<table width='100%'>
						   <tr>
							<td width='100%' align='left' class='texto9' colspan='2'><b>DIRECCION :</b> ". $nomdir ."</td><br>
						   </tr>
						   <tr>
							<td width='80%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
						  	<td width='20%' align='right' class='texto9'><b>FECHA:</b>".$fecha."</td>
						   </tr>
						</table>			
            <table width='100%' border='0'>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto12'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto10'><b>NUM. DE FACTURA:</b> ".$factura."</td>
              </tr>
            </table>	
            <table width='100%' border='0'>
              <tr>
                <td width='80%' align='left' class='texto10'><b>BENEFICIARIO:</b> ".$nomprov."</td>
                <td width='20%' align='right' class='texto10'><b>IMPORTE:</b> ".$neto."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto8'><b>CANTIDAD CON LETRA:</b>". strtoupper($neto_let) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td width='60%' align='left' class='texto7'>".$concepto."</td>
				<td width='40%' align='right' class='texto9'>".$detalle."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>_____________________________________</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>_____________________________________</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomfirma."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' class='texto8'><B>".$nomdir." </B></td>
                <td width='10%' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>              
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>

            </table>            
	 	  </td>
  	    </tr>  
	    <tr>
			<td width='100%'>
             <table width='100%' border='0'>
              <tr>
                <td align='left' width='20'><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                ".htmlentities("SOLICITUD DE CHEQUE - GASTOS POR COMPROBAR")." - </b></td>
                <td align='right' width='20'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
              </tr>
            </table>
			
			<table width='100%'>
			   <tr>
				<td width='100%' align='left' class='texto9' colspan='2'><b>DIRECCION :</b> ". $nomdir ."</td><br>
				</tr>
				<tr>
					<td width='80%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
					<td width='20%' align='right' class='texto9'><b>FECHA:</b>".$fecha."</td>
				</tr>
			</table>					
            <table width='100%' border='0'>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto12'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto10'><b>NUM. DE FACTURA:</B> ".$factura."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='80%' align='left' class='texto10'><B>BENEFICIARIO:</B> ".$nomprov."</td>
                <td width='20%' align='right' class='texto10'><b>IMPORTE:</b> ".$neto."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto10'><b>CANTIDAD CON LETRA:</b>". strtoupper($neto_let) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td width='60%' align='left' class='texto7'>".$concepto."</td>
				<td width='40%' align='right' class='texto9'>".$detalle."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
                <td width='10%'>&nbsp;</td>
                <td width='30%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>_____________________________________</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>_____________________________________</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomfirma."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' class='texto8'><B>".$nomdir."</B></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>
              
              <tr>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'></td>
             </tr>
            </table>
	 	  </td>
  	    </tr>  
	  </table>
	</body>
</html>";	
// echo $html 

	$dompdf = new DOMPDF();
	$dompdf->set_paper('"letter","portrait"');	
	$dompdf->load_html($html);
	$dompdf->render();	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/comp_gastos_".$folio.".pdf", $pdf);
echo json_encode($folio);
///////////////////////////////////////////////////////////////
?>