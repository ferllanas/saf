<?php
if (version_compare(PHP_VERSION, "5.1.0", ">="))
		date_default_timezone_set("America/Mexico_City");
		
require_once("../../connections/dbconexion.php");
$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

// Informacion desde solcheque
//	$usuario = $_COOKIE["ID_my_site"]; //Misma variable para Almacen
// 	$depto = $_COOKIE["depto"]; //Misma variable para Almacen
// 	$privsolche = $_COOKIE["privsolche"]; //Misma variable para Almacen
$privsolche = 0;
$depto = 1232;
$usuario = 1981;

$datos=array();
$folio=0;
$resguarda="";
$ugastos="";
$depa="";  // Aqui trae la informacion del Depto, sin embargo va a haber necesidad de cambiarlo o ajustarlo al que ya se tiene en cheques
$iva=0.00;
$totfact=0.00;
$efectivo=0.00;
$comprobado=0.00;
$ren="";
$difxdevolver=0.00;
$nombre="";
$cheque="0"; // En el SP se llama Cheque y es numerico por lo que hay que inv.
$chautomatico="0";  // En el SP se llama Chautomatico y es numerico por lo que hay que inv.
$folio_chauto="0"; // En el SP se llama Folio_chauto y es numerico por lo que hay que inv.
$fcomp = date("d/m/Y");

// Obtenemos y traducimos el nombre del d�a
$dia=date("l");
if ($dia=="Monday") $dia="Lunes";
if ($dia=="Tuesday") $dia="Martes";
if ($dia=="Wednesday") $dia="Mi�rcoles";
if ($dia=="Thursday") $dia="Jueves";
if ($dia=="Friday") $dia="Viernes";
if ($dia=="Saturday") $dia="Sabado";
if ($dia=="Sunday") $dia="Domingo";

// Obtenemos el n�mero del d�a
$dia2=date("d");

// Obtenemos y traducimos el nombre del mes
$mes=date("F");
if ($mes=="January") $mes="Enero";
if ($mes=="February") $mes="Febrero";
if ($mes=="March") $mes="Marzo";
if ($mes=="April") $mes="Abril";
if ($mes=="May") $mes="Mayo";
if ($mes=="June") $mes="Junio";
if ($mes=="July") $mes="Julio";
if ($mes=="August") $mes="Agosto";
if ($mes=="September") $mes="Setiembre";
if ($mes=="October") $mes="Octubre";
if ($mes=="November") $mes="Noviembre";
if ($mes=="December") $mes="Diciembre";

// Obtenemos el a�o
$ano=date("Y");

// Imprimimos la fecha completa
// echo "$dia $dia2 de $mes de $ano";


require_once("../../connections/dbconexion.php");
$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if ($conexion = sqlsrv_connect($server,$infoconexion))
{		
	$consulta = "select c.folio, a.depto, a.nomdepto, a.nomcoord ,b.nombre, b.appat, b.apmat, c.total, c.falta from nominamdepto a inner join ";
	$consulta .= "nominadempleados b on a.depto=b.depto inner join ";
	$consulta .= "egresosmsolchegtosxcomp c on c.usuario=b.numemp ";
	$consulta .= "order by c.folio";	
	//echo ($consulta);

	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]["folio"]= trim($row["folio"]);
		$datos[$i]["nomempcheq"]=trim($row["nombre"]) ." ". trim($row["appat"]) ." ". trim($row["apmat"]);		
		$i++;
	}
}

if(isset($_REQUEST["subtotal"]))
	$subtotal = $_REQUEST["subtotal"];
if(isset($_REQUEST["iva"]))
	$iva = $_REQUEST["iva"];
if(isset($_REQUEST["efectivo"])) 
	$efectivo = $_REQUEST["efectivo"];	

// --- Informaci�n del usuario responsable
		$command= "SELECT *  FROM v_respsolche b";	
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
            // Se valida que la informacion en $stmt
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
			    
 				    //Esta variable se pone cuando uno va a desplegar una tabla con informacion de detalle, como por ejemplo una factura.
					//$productos[$i]["cve"] =trim($lrow["cve"]);
  				    $catego = trim($lrow["catego"]);
  				    $nomcatego = trim($lrow["nomcatego"]);
  				    $nomdepto = trim($lrow["nomdepto"]);					
  				    $estatus = trim($lrow["estatus"]);
					$uresp = trim($lrow["numemp"]);	
					$depto = trim($lrow["depto"]);	
					$nomemp = trim($lrow["nomemp"]); // se cambia de $nomemp a $nomemp
 					$i++;
			}
		}
// -- Termina seleccion del usuario responsable		



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Comprobacion de Gastos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script language="javascript" src="javascript/valida_compgastos.js"></script>
<script language="javascript" src="javascript/calcula_iva.js"></script>
<script language="javascript" src="javascript/calcula_importes.js"></script>
<script language="javascript" src="javascript/compgastosinc.js"></script>
<script language="javascript" src="javascript/comchequegastos.js"></script>
<script language="javascript" src="javascript/guarda_datos_detalle.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
<!-- Esta es la informacion que va a utilizar el cheque -->
<script language="javascript" src="javascript/provfuncion.js"></script>
<script language="javascript" src="javascript/busqueda2.js"></script>
<script language="javascript" src="javascript/chequebusqIncProv.js"></script>
<script type="text/javascript" src="javascript/compgastos.js"></script></script>




<style type="text/css">
<!--
#apDiv1 {
	position:absolute;
	left:185px;
	top:106px;
	width:735px;
	height:271px;
	z-index:4;
	visibility: hidden;
}
-->
</style>	
</head>

<body>
<div id="ImpCheque">
	 <!-- ?php include('cheques/solcheque.php'); ? -->
</div>
<form name="form_comprueba_gastos" method="post" action="">
  <p align="center" class="texto10">
  </p>
<input type="hidden" name="folio" id="folio" size="10">
<input type="hidden" name="foliox" id="foliox" size="10">
<input type="hidden" name="nombre" id="nombre">
</p>
<input type="hidden" id="ren" width="15">
<input type="hidden" id="numren" width="15">
<input type="hidden" id="usuario" value="<?php echo $usuario; ?>" width="15">
<input type="hidden" name="depto" id="depto">
<input type="hidden" name="uresp" id="uresp" value="<?php echo $uresp; ?>">
<input type="hidden" name="nomemp" id="nomemp" value="<?php echo $nomemp; ?>">
<input type="hidden" name="nomdepto" id="nomdepto" value="<?php echo $nomdepto; ?>">
<input type="hidden" name="cheque" id="cheque" value="<?php echo $cheque; ?>">
<input type="hidden" name="fcomp" id="fcomp" value="<?php echo $fcomp; ?>">
<input type="hidden" name="chautomatico" id="chautomatico" value="<?php echo $chautomatico; ?>">
<input type="hidden" name="folio_chauto" id="folio_chauto" value="<?php echo $folio_chauto; ?>">
  <table width="100%" border="0">
    <tr class="texto9">
    <td width="8%">No. Cheque :</td>
    <td width="10%"><label>
      <input type="hidden" name="ch" id="ch" width="80" size="15%" height="10%" class="caja_toprint texto8"> 
      </label></td>
    
    
    
    
    
    <td width="2%">
   <div align="left"  style="z-index:3; position:absolute; width:540px; top: 18px; left: 105px; height: 21px;">
   <input name="ugastos" type="text" class="texto8" id="ugastos" tabindex="7" style="background:#EBEBEB" onKeyUp="searchcompgast(this);" size="40%" autocomplete="off" height="12">
        <a href="javascript:activaCheque()">Habilitar campo</a>
        <div id="search_suggestcompgast" style="z-index:4; "></div>
      </div></td>    
     
    <td width="9%">&nbsp;</td>
    <td width="24%" align="right">
      Importe del Cheque
        <input name="totcheque" type="text" size="9" class="texto8" id="totcheque" style="background-color:#EBEBEB" onBlur="this.value = NumberFormat(this.value, "2", ".", ",")" value=0.00 readonly>
    </td>
    <td width="30%" align="right">Departamento:      <input class="texto8" type="text" style="background:#EBEBEB" name="depa" id="depa" size="50%" readonly></td>
    <td width="9%">Fecha del cheque: </td>
    <td width="8%"><input type="text" name="fcheque" id="fcheque" style="background:#EBEBEB" size="12%" class="texto8" readonly></td>
    </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="2"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td> 
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" class="texto9">Fecha de Comprobaci&oacute;n :</td>
    <td colspan="6" class="texto9"><input type="text" align="absmiddle" style="border:0 #EBEBEB" readonly name="hoy" id="hoy" size="40%" class="texto9" value="<?php echo "$dia $dia2 de $mes de $ano"?>">      
    </td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">Fecha de Evento</td>
    <td class="texto9"><input type="text" name="fevento" id="fevento" readonly size="12%" class="texto8">
      <label>
        <img src="../../calendario/img.gif" alt="" width="16" height="16" align="absmiddle" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d>
        <script type="text/javascript">
                                    Calendar.setup({
                                        inputField     :    "fevento",		// id of the input field
                                        ifFormat       :    "%d/%m/%Y",		// format of the input field
                                        button         :    "f_trigger2",	// trigger for the calendar (button ID)
                                        //onClose        :    fecha_cambio,
                                        singleClick    :    true
                                    });
                                </script>
      </label>  
    </td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td colspan="2" class="texto9">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="16" class="texto10"><strong>REALIZA EL DESGLOSE DE GASTOS </strong></td>
  </tr>
  <tr>
    <td><span class="texto9"><span class="texto8">R.F.C.:</span></span></td>
    <td class="texto9"><input type="text" name="rfc" id="rfc" size="15%" class="mayus8"></td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9"><span class="texto8">FACTURA:</span></td>
    <td class="texto9"><span class="texto8">
      <input type="text" name="factura" id="factura" size="19%" class="mayus8" onBlur="valida_factura()">
      </span></td>
    <td class="texto9"><span class="texto8">FECHA</span> :</td>
    <td class="texto9"><input type="text" readonly name="fdetalle" id="fdetalle" size="12%"  class="texto8">
    <label>
    <img src="../../calendario/img.gif" alt="" width="16" height="16""" align="absmiddle" id="f_trigger3" style="cursor: pointer;" title=""%Y/%m/%d>
    <script type="text/javascript">
                                Calendar.setup({
                                    inputField     :    "fdetalle",		// id of the input field
                                    ifFormat       :    "%d/%m/%Y",		// format of the input field
                                    button         :    "f_trigger3",	// trigger for the calendar (button ID)
                                    //onClose        :    fecha_cambio,
                                    singleClick    :    true
                                });
                            </script>
      </label>
    
    </td>
    <td class="texto9"><span class="texto8">DESCRIPCION:</span></td>
    <td colspan="7" class="texto9"><span class="texto8">
      <input type="text" name="descrip" id="descrip" size="100%" align="right"  class="texto8">
      </span></td>
  </tr>
  <tr>
    <td colspan="16">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="texto9"><div align="right"><span class="texto8">SUBTOTAL:</span></div></td>
    <td class="texto9"><span class="texto8">
	  <!-- onKeyUp="asignaFormatoSiesNumerico(this, event)" Valida importe con comas -->
      <input type="text" tabindex="11" name="subtotal" id="subtotal" size="13%" onBlur="cal_iva()" onKeyPress="return aceptarSoloNumeros(this, event);" class="texto8"></span></td>
    <td class="texto9" align="right"><span class="texto8">I.V.A.:</span></td>
    <td class="texto9"><span class="texto8">
      <input type="text" tabindex="14" name="iva"  id="iva" value="0.00" size="13%" readonly onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8">    </span></td>
    <td class="texto9">&nbsp;</td>
    <td class="texto9"><span class="texto8">TOTAL:</span></td>
    <td class="texto9"><span class="texto8">
      <input type="text" name="total" id="total" size="13%" onKeyPress="return aceptarSoloNumeros(this, event);" value="0.00" onKeyUp="asignaFormatoSiesNumerico(this, event)" class="texto8"></span>
      <td colspan="2">
      <img src="../../imagenes/agregar2.jpg" width="30" height="25" title="Agrega Nuevo Comprobante" onClick="agregar()"><span style="visibility:text "><img src="../../imagenes/guardar2.jpg" width="30" height="25" title="Actualiza Datos de Comprobante." onClick="actualiza_datos()">
      </td>
      <!-- input type="text" name="depto" id="depto" -->
    </tr>
  </table>
<table name="enc" id="enc" width="100%" height="10%" align="center" border="1">
  <tr>
    <th width="10%" align="center" class="subtituloverde">R.F.C.</th>
    <th width="8%" align="center" class="subtituloverde">Factura</th>
    <th width="8%" align="center" class="subtituloverde">Fecha</th>
    <th width="50%" align="center" class="subtituloverde">Descipcion</th>
    <th width="8%" align="center" class="subtituloverde">Subtotal</th>
    <th width="8%" align="center" class="subtituloverde">I.V.A.</th>
    <th width="8%" align="center" class="subtituloverde">Total</th>
    <th width="6%" align="center" class="subtituloverde">Detalle</th>
    </tr>
</table>
<div style="overflow:auto; width:auto; height: 150px;  align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
</div>
<hr>
</form>
<table width="88%" border="0" align="right">
  <tr>
    <td width="46%" height="24" class="texto8"><strong>CAPTURA LOS NUMEROS DE RECIBO</strong></td>
    <td width="21%">COMPROBADO:</td>
    <td width="33%"><form name="form_compgastos_detalle" method="post" action="php_ajax/compgastos.php">
      <label class="texto8">
        $
        <input type="text" name="comprobado" style="background-color:#EBEBEB" id="comprobado" class="texto8" value=0.00 onKeyUp="asignaFormatoSiesNumerico(this, event)" readonly> 
      </label>
    </form></td>
  </tr>
  <tr>
    <td rowspan="3"><label>
      <textarea maxlength=20 id="no_recibo" name="no_recibo" class="texto8" cols="45" rows="2" style="width:80%"></textarea>
    </label>
          <input name="enviar" type="button" class="textom8" id="Guardar" onClick="valida_total()" value="Enviar a impresion">
</td>
    <td>EFECTIVO DEVUELTO:</td>
    <td><form name="form_efectivo" method="post" action="">
      <label class="texto8">$
        <input type="text" name="efectivo" id="efectivo" class="texto8" onKeyUp="valida_efectivo()" onBlur="this.value = NumberFormat(this.value, "2", ".", ",")" value=0.00  onKeyPress="return aceptarSoloNumeros(this, event);">
      </label>
    </form></td>
  </tr>
  <tr>
    <td>IMPORTE SOLICITUD DE CHEQUE A EMITIR:</td>
    <td><form name="form_totales" method="totales" action="">
      <label class="texto8">$
      <input type="text" name="totchequegen" style="background-color:#EBEBEB" id="totchequegen" value=0.00 readonly class="texto8" onBlur="this.value = NumberFormat(this.value, "2", ".", ",")">        
        
        
        
        
        
      </strong></label>
    </form></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><form name="form_subtotales" method="subtotales" action="">
      <label class="texto8">
        <!-- input type="text" id="difxdevolver" onBlur="valida_efectivo()" style="background-color:#EBEBEB" value=0.00 class="texto8" readonly -->
        <input type="hidden" id="difxdevolver" style="background-color:#EBEBEB" value=0.00 class="texto8" readonly>
      </label>
    </form></td>
  </tr>
</table>
</body>
</html>
