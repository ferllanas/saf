<?php
$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
	{	
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Emision de solicitud de Cheque (Comp. Gastos)</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../../prototype/prototype.js'></script>
</head>

<body>
";
    if (version_compare(PHP_VERSION, "5.1.0", ">="))
 	require_once("../../../dompdf/dompdf_config.inc.php");
	date_default_timezone_set("America/Mexico_City");
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	include("../../../Administracion/globalfuncions.php");

	if (version_compare(PHP_VERSION, '5.1.0', '>='))
			date_default_timezone_set('America/Mexico_City');
		$totdet=array();
		if(isset($_REQUEST['numren']))
			$totdet=$_REQUEST['numren'];

		$cero=0;
		for($i=0;$i<count($totdet);$i++)
		{
			$folio 		= $totdet[$i]['folio'];
			$usuario 	= $totdet[$i]['usuario'];
			$usuario2 	= $totdet[$i]['usuario2'];
			$comprobado	= str_replace(',','',$totdet[$i]['comprobado']);
			$comprobado2= $totdet[$i]['comprobado'];
			$efectivo	= $totdet[$i]['efectivo'];
			$deposito	= $totdet[$i]['deposito'];
			$no_recibos	= $totdet[$i]['no_recibos'];
			$totcheque	= $totdet[$i]['totcheque'];
			$resp 		= $totdet[$i]['resp'];
			$coordinacion	= $totdet[$i]['coordinacion'];
			$coordinador	= $totdet[$i]['coordinador'];
			$direccion		= $totdet[$i]['direccion'];
			$director		= $totdet[$i]['director'];
			$dirresp		= $totdet[$i]['dirresp'];
			$responsable	= $totdet[$i]['responsable'];
			$fechaini		= $totdet[$i]['fechaini'];
			$fechafin		= $totdet[$i]['fechafin'];
			//print_r($totdet[$i]);
		}	
			
	list($folio,$fecha,$hora) = fun_creaGastosxComp($folio, $cero, $comprobado, $deposito, $no_recibos, $cero, $cero, $usuario, $usuario2, $resp,$totdet);		
			

function fun_creaGastosxComp($folio, $cero, $comprobado, $deposito, $no_recibos, $cero, $cero, $usuario, $usuario2, $resp, $totdet)
{
	global $server,$odbc_name,$username_db ,$password_db, $imagenPDFPrincipal;

	$fails=false;
	
	//$fecoperiodo=convertirFechaEuropeoAAmericano($fecoperiodo);
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion

	// A.SP Una vez almacenados los datos en la variable del procedimiento almacenado se procede a llamarlo para indicar cuantos campos se van a ingresar
	$cheqsql_callSP ="{call sp_egresos_A_mcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	//Arma parametros de entrada al sp_egresos_A_msolche
	$params = array(&$folio, &$cero, &$comprobado, &$deposito, &$no_recibos, &$cero, &$cero, &$usuario, &$usuario2, &$resp);
	//Agrega un tiempo de espera de 180 segundos
	$options = array("QueryTimeout"=>180);
    // Verifica que informacion trae $stmt
	$stmt = sqlsrv_query($conexion, $cheqsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails="Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)." sp_egresos_A_mcomprobacion ". print_r($params,true));
	}
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$folio = $row["folio"];
			$fecha = $row["fecha"];
			$hora = $row["hora"];
			$nomdir = $row["nomdir"];
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
			//$fecha2=$dia2."/".substr($meses,$mes2*3-3,3)."/".$anio2 ;
		}
		sqlsrv_free_stmt( $stmt);
	}
	sqlsrv_close( $conexion);
	return array($folio,$fecha,$hora,$nomdir);
}

//$totdet=array();
//$i=0;
for($i=0;$i<count($totdet);$i++)
{
	//$folio=$totded[$i]['folio'];
	$rfc=$totdet[$i]['rfc0'];
	$factura=$totdet[$i]['factura0'];
	$fecha0=convertirFechaEuropeoAAmericano($totdet[$i]['fdetalle0']);
	$fecha_tit=$totdet[$i]['fdetalle0'];
	$descrip=$totdet[$i]['descrip0'];
	$subtotal=str_replace(',','',$totdet[$i]['subtotal0']);	
	$iva=str_replace(',','',$totdet[$i]['iva0']);	
	$total=str_replace(',','',$totdet[$i]['total0']);	
	$depto=0;
	$fecha_hoy = date("d/m/Y");
	$ejecuta ="{call sp_egresos_A_dcomprobacion(?,?,?,?,?,?,?,?,?,?)}";
	$variables = array(&$folio,&$rfc,&$factura,&$fecha0,&$descrip,&$subtotal,&$iva,&$total,&$usuario,&$depto);
	//print_r($fecha0);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	if( $R === false )
	{
		 $fails= "Error in (detalle) statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
}

$dompdf = new DOMPDF();
//$pdf->page_text(60, 10, $chapter, $font, 10, array(0,0,0)); $font = Font_Metrics::get_font("Helvetica", "normal"); 
$pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	$textod=utf8_encode("Página {PAGE_NUM}");
	$textop=utf8_encode("Folio");
	$pdf->page_text(25, 760, "$textop '.$folio.'", "", 12, array(0,0,0)); 
	$pdf->page_text(550, 760, "Pagina {PAGE_NUM}", "", 12, array(0,0,0)); 
	
 } 
</script> ';  

$html.="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
		{
			font-family:'Arial';
			font-size:9;
			margin:  0.25in 0.5in 0.5in 0.5in;
		}
		
		td {padding: 0;}
		
		#twmaarco
		{
			border:1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		
		.texto16b{ 
			font-size:16pt;
			font-weight:bold;
		}
		.texto14{ 
			font-size:14pt;
		}
		.texto14b{ 
			font-size:15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size:12pt;
			font-weight:bold;

		}
		.texto12{ 
			font-size:12pt;
		}
		.texto11{
			font-size:11pt;
		}
		.texto10{
			font-size:10pt;
		}
		.caja{
		position:absolute;
		left:1px; 
		top:600px;
		}
		
		</style>
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Comprobacion de Gastos</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>

</head>
<table width='100%'>
	<tr>
		<td width='100%'>
			<table width='100%' align='center' height='48' border='0'>
			  <tr>
					<td align='left' width='20'><img src='../../../$imagenPDFPrincipal' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PRESUPUESTAL<BR />
                COMPROBACION</b></td>
					<td align='right' width='20'><img src='../../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%' border='0' align='center'>
	
			  <tr>
				<td class='texto8'>FECHA COMPROBACION</td>
				<td class='texto8' align='left'>$fecha_hoy</td>
				<td></td>
				<td class='texto8'>FECHA DE EVENTO</td>
				<td class='texto8'>$fechaini</td>
			  </tr>
			  <tr>
				<td class='texto8'>No. CHEQUE</td>
				<td></td>
				<td></td>
				<td class='texto8'>FECHA DEL CHEQUE</td>
				<td></td>
			  </tr>
			  <tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width='100%' border='0' align='center'>
			  <tr bgcolor='#CCCCCC'>
				<td width='12%' align='center' class='texto8'>RFC</td>
				<td width='6%'align='center' class='texto8'>CTA. PRES.</td>
				<td width='6%' align='center' class='texto8'>CTRO. CTOS.</td>
				<td width='8%' align='center' class='texto8'>FACTURA</td>
				<td width='10%' align='center' class='texto8'>FECHA</td>
				<td width='28%' align='center' class='texto8'>DESCRIPCION</td>
				<td width='10%' align='center' class='texto8'>SUBTOTAL</td>
				<td width='8%' align='center' class='texto8'>IVA</td>
				<td width='10%' align='center' class='texto8'>TOTAL</td>
			  </tr>
			<body>";
		
			for($i=0;$i<count($totdet);$i++)
			{
			$html.="
			 <tr>
				<td class='texto8' align='center'>".$totdet[$i]['rfc0']."</td>
				<td width='6%'></td>
				<td width='6%'></td>
				<td class='texto8' align='center'>".$totdet[$i]['factura0']."</span></td>
				<td class='texto8' align='center'>".$totdet[$i]['fdetalle0']."</td>
				<td class='texto8'>".$totdet[$i]['descrip0']."</td>
				<td class='texto8' align='center'>".$totdet[$i]['subtotal0']."</td>
				<td class='texto8' align='center'>".$totdet[$i]['iva0']."</td>
				<td class='texto8' align='center'>".$totdet[$i]['total0']."</td>
			  </tr>
			";
			}
			$html.="
			</body>
			</table>
		</td>
	</tr>
	<tr>
	</tr>

		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
	
	
</table>


	<div class='caja'>
		<table style='border-color:#000000'>
		  <tr>
			<td>&nbsp;</td>
			<td class='texto8'># DE RECIBOS </td>
			<td class='texto8'>&nbsp;</td>
			<td width='30%' class='texto8'>&nbsp;</td>
			<td width='10%' align='right' class='texto8'>&nbsp;</td>
			<td width='10%' class='texto8'>&nbsp;</td>
			<td width='10%' class='texto8'>&nbsp;</td>			
			<td width='20%' align='center' class='texto8'>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td class='texto8'>$no_recibos</td>
			<td>&nbsp;</td>
			<td width='30%' class='texto8'>COMPROBADO</td>
			<td width='10%' align='right' class='texto8'>$comprobado2</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class='texto8'>EFECTIVO DEVUELTO</td>
			<td width='10%' align='right' class='texto8'>$efectivo</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class='texto8'>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class='texto8'>IMPORTE CHEQUE </td>
			<td width='10%' align='right' class='texto8'>$totcheque</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td rowspan='2' class='texto8'>DIFERENCIA POR DEPOSITAR O DEVOLVER </td>
			<td width='10%' align='right' class='texto8'>$deposito</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class='texto8'>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>SOLICITA</td>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>DIRECTOR</td>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>AUTORIZO</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
		    <td>&nbsp;</td>
	      </tr>
		  <tr>
			<td>&nbsp;</td>
			<td><div align='center'>________________________________ </div></td>
			<td>&nbsp;</td>
			<td><div align='center'>________________________________</div></td>
			<td>&nbsp;</td>
			<td><div align='center'>________________________________</div></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>$coordinador</td>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>$director</td>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>$responsable</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>$coordinacion</td>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>$direccion</td>
			<td>&nbsp;</td>
			<td class='texto8' align='center'>$dirresp</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		</table>
	</div>			

</body>
</html>";

//////////////////////////


	$dompdf->set_paper('"letter","portrait"');
	
	$dompdf->load_html($html);
	$dompdf->render();
	  
	$pdf = $dompdf->output(); 
	file_put_contents("../pdf_files/comprobacion_".$folio.".pdf", $pdf);



/////////////////////////
/*$dompdf->load_html($html);

$dompdf->render();
	  
$pdf = $dompdf->output(); 
file_put_contents("../pdf_files/".$folio.".pdf", $pdf);*/


echo json_encode($folio);			
?>

