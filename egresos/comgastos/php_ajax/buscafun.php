<?php
require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$datos=array();
$coord="";
if(isset($_REQUEST['coord']))
	$coord= $_REQUEST['coord'];
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		$command= "select a.nombre,a.appat,apmat,b.coord,b.nomcoord from nominadempleados a inner join nominamdepto b on a.coord=b.coord where a.coord=$coord and a.estatus<'90' order by nivel";	
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt))
		{
			$resoponsecode="Cantidad rows=".count($stmt);
			$i=0;
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['coord']= trim($row['coord']);
				$datos[$i]['coordinador']= trim($row['nombre']).' '.trim($row['appat']).' '.trim($row['apmat']);
				$datos[$i]['nomcoord']= trim($row['nomcoord']);
				$i++;
			}
		}
	}
	echo json_encode($datos);
?>