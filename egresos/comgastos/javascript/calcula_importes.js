function calcularTotales(obj,e)/*objecta,cantidad,preciounitario,iva*/
{
	var Table = $('datos');
	var contRow = Table.rows.length;
	var subtotal= 0.0;
	var iva= 0.0;
	var total= 0.0;

	for (var i = 0; i < contRow; i++) 
	{  
		var sdsubtotal = 0.0;
		var siva = 0.0;
		var stotal = 0.0;
		for (var j = 0; j <Table.rows[i].cells.length;j++)
		{
			var cell = Table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="diva")
				{
					siva = parseFloat(mynode.value);
					//alert(siva);
				}
				
				if(mynode.name=="dsubtotal")
				{
					sdsubtotal= parseFloat(mynode.value);//+""+GetChars(e)
				}
	
				if(mynode.name=="dtotal")
				{
					stotal= parseFloat(mynode.value);
					//alert(stotal);
				}
			}
		}
		
		 subtotal += (sprecio * sdsubtotal);
		 iva += (sprecio * sdsubtotal * (siva/100));
		 total += ((sprecio * sdsubtotal) + (sprecio * sdsubtotal * (siva/100)));	
		
	}

	$('subtotal').value = redondear(subtotal,2);
	$('iva').value = redondear(iva,2);
	$('total').value = redondear(total ,2);
}

function redondear(cantidad, decimales) 
{
	var cantidad = parseFloat(cantidad);
	var decimales = parseFloat(decimales);
	decimales = (!decimales ? 2 : decimales);
	return Math.round(cantidad * Math.pow(10, decimales)) / Math.pow(10, decimales);0
} 


//Calcula el subtotal de los proveedores
function sumTotalDProveedores()
{
	var table=$('datos');	
	var existe=false;	
	var sumprov1 = 0.0;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length && existe==false; i++) 
	{  //Iterate through all but the first row
		for (var j = 0; j <table.rows[i].cells.length && existe==false ;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="cantxPresUni1"+i)
					if(mynode.value.length>0)
						sumprov1 = sumprov1 + parseFloat(mynode.value);
			}
		}
	}	
	$('totalprov1').value = sumprov1;
	$('totalprov2').value = sumprov2;
	$('totalprov3').value = sumprov3;
	$('totalprov4').value = sumprov4;
}