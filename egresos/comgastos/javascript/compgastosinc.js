//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectcompgast() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqcompgast = getXmlHttpRequestObjectcompgast(); 
function searchcompgast() {
	//$('cvecompgast').value = "";
	//limpia_tabla();
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqcompgast.readyState == 4 || searchReqcompgast.readyState == 0)

	{
        var str = escape(document.getElementById('ugastos').value);

		if (str.length>2)		
		{
			//alert('php_ajax/queryusuariocheqgasto.php?q=' + str);
			searchReqcompgast.open("GET",'php_ajax/queryusuariocheqgasto.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
			searchReqcompgast.onreadystatechange = handleSearchSuggestcompgast;
			searchReqcompgast.send(null);
		}
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestcompgast() {
    if (searchReqcompgast.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestcompgast')
        ss.innerHTML = '';
        var str = searchReqcompgast.responseText.split("\n");
		
		
		   for(i=0; i < str.length - 1 && i<50; i++) {
				var tok = str[i].split("@");  // El arroba nos sirve para separar los datos a desplegar ya en el recuadro
				var suggest = '<div onmouseover="javascript:suggestOvercompgast(this);" '; // Cambian los colores
				suggest += 'onmouseout="javascript:suggestOutcompgast(this);" ';
				suggest += "onclick='javascript:setSearchcompgast(this.innerHTML,this.title);' "; //selecciona elemento de la lista, viene bajo variables como this y title y se va a 
			    // la variable del JS setSearchcompgast
				suggest += 'class="suggest_link" title="'+tok[0]+'@'+tok[1]+'@'+tok[2]+'@'+tok[3]+'@'+tok[4]+'@'+tok[5]+'@'+tok[6]+'@'+tok[7]+'@'+tok[8]+'@'+tok[9]+'@'+tok[10]+'@'+tok[11]+'">' + tok[0] +' - '+tok[2]+'</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOvercompgast(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutcompgast(div_value) {
    div_value.className = 'suggest_link';
}
//Click function



/*---------------------------------------- Funcion para obtener la Firma del Cheque */

function setSearchcompgast(value,clave) 
{
    document.getElementById('ugastos').disabled = true; // Desabilita el campo posterior a la captura
	var tok =clave.split("@");
	$('ugastos').value = tok[2]+" SOLICITUD No. - " + tok[0];
	$('folio').value = tok[0];
	$('depa').value = tok[1];
	$('nombre').value = tok[2];	
	$('fcheque').value = tok[3];
	$('resp').value = tok[4];
	$('totcheque').value = tok[5];
	$('depto').value = tok[6];
	$('coord').value = tok[7];
	$('dir').value = tok[8];
	$('fevento').value = tok[9];
	$('fechafin').value = tok[10];
	$('usuario2').value = tok[11];
	$('totcheque').value=NumberFormat($('totcheque').value, '2', '.', ',');
	document.getElementById('search_suggestcompgast').innerHTML = '';
	busca2(tok[6],tok[7],tok[8]);
	busca3(tok[6],tok[7],tok[8]);
	busca4(tok[4]);
}
	
function activacompgast() // Habilita nuevamente el campo Departamento
			{
				document.getElementById('ugastos').value = '';
				document.getElementById('ugastos').disabled = false;
			}
			
function valida_depto()
{
	var firma = $("firma").value;
	if(firma.length<=0)
	{
		alert ("Depto no existe");return false;
	}
}

function busca()
{
	document.getElementById('numfirma').value = $("firma").options[$("firma").selectedIndex].value;
	var num_fir = $("firma").options[$("firma").selectedIndex].value;
	new Ajax.Request('php_ajax/firma.php?num_fir='+num_fir,
	 {onSuccess : function(resp) 
	 {
		//alert(resp.responseText);
		if( resp.responseText ) 
		{
			//got an array of suggestions.
			//alert(resp.responseText);
			var myArray = eval(resp.responseText);
			//alert("22 "+resp.responseText);
			if(myArray.length>0)
			{
				document.getElementById('numfirma').value = myArray[0].numemp;
				document.getElementById('nomfirma').value = myArray[0].nomemp;
				document.getElementById('nomdepto').value = myArray[0].nomdepto;
				document.getElementById('nomcoord').value = myArray[0].nomcoord;
			}
		}
	  }
	});
}


function busca2(depto,coord,dir)
{
	new Ajax.Request('php_ajax/buscafun.php?depto='+depto+"&coord="+coord+"&dir="+dir,
	 {onSuccess : function(resp) 
	 {
		//alert(resp.responseText);
		if( resp.responseText ) 
		{
			//got an array of suggestions.
			//alert(resp.responseText);
			var myArray = eval(resp.responseText);
			//alert("22 "+resp.responseText);
			if(myArray.length>0)
			{
				//alert(myArray[0].nomcoord);
				//alert(myArray[0].coordinador);
				document.getElementById('coordinacion').value = myArray[0].nomcoord;
				document.getElementById('coordinador').value = myArray[0].coordinador;
			}
		}
	  }
	});
}

function busca3(depto,coord,dir)
{
	new Ajax.Request('php_ajax/buscadir.php?depto='+depto+"&coord="+coord+"&dir="+dir,
	 {onSuccess : function(resp) 
	 {
		if( resp.responseText ) 
		{
			//got an array of suggestions.
			//alert(resp.responseText);
			var myArray = eval(resp.responseText);
			//alert("22 "+resp.responseText);
			if(myArray.length>0)
			{
				document.getElementById('direccion').value = myArray[0].nomdir;
				document.getElementById('director').value = myArray[0].director;
			}
		}
	  }
	});
}

function busca4(xresp)
{
	new Ajax.Request('php_ajax/buscaresp.php?xresp='+xresp,
	 {onSuccess : function(resp) 
	 {
//		alert(resp.responseText);
		if( resp.responseText ) 
		{
			//got an array of suggestions.
			//alert(resp.responseText);
			var myArray = eval(resp.responseText);
			//alert("22 "+resp.responseText);
			if(myArray.length>0)
			{
				document.getElementById('dirresp').value = myArray[0].dirresp;
				document.getElementById('responsable').value = myArray[0].responsable;
			}
		}
	  }
	});
}