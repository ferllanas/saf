function agregar()
{
	var folio=$('folio').value;
	var rfc=$('rfc').value;
	var descrip=$('descrip').value;	
	var fdetalle=$('fdetalle').value; <!------>
	var factura=$('factura').value;
	var subtotal=$('subtotal').value;  <!-- Checar como manejarlo con el subtotal del cheque -->
	var iva=$('iva').value;
	var total=$('total').value;	
	var totcheque=Math.round($('totcheque').value.replace(/,/g,'')*100)/100;	
	var comprobado=Math.round($('comprobado').value.replace(/,/g,'')*100)/100;
//	=Math.round($('subtotal').value.replace(/,/g,'')*100)/100;
	var difxdevolver=Math.round($('difxdevolver').value.replace(/,/g,'')*100)/100;	
	var efectivo=$('efectivo').value;
	var fevento=$('fevento').value;
	var ugastos=$('ugastos').value;	
	var fcomp=$('fcomp').value;	
	var suma = parseFloat(subtotal)+parseFloat(iva);

					if(fcomp.length<1)
						{
						alert("Aun no has capturado la fecha del Comprobacion");
						document.getElementById('fcomp').focus();
						return false;
						}							

					if(fevento.length<1)
						{
						alert("Aun no has capturado la fecha del Evento");
						document.getElementById('fevento').focus();                            
						return false;
						}							
					if(ugastos.length<1)
						{
						alert("Aun no has capturado el Cheque");
						document.getElementById('ugastos').focus();                                                        
						return false;
						}
					if(rfc.length<1)
						{
						alert("Falta capturar el RFC");
						document.getElementById('rfc').focus();                                                                                    
						return false;
						}
					if(factura.length<1)
						{
						alert("Falta capturar No. de Factura");
						document.getElementById('factura').focus();
						return false;
						}
				
					if(fdetalle.length<1)
						{
						alert("Falta capturar Fecha");
						document.getElementById('fdetalle').focus();                            
						return false;
						}
					if (descrip==null || descrip.length<1)	
						{
						alert("Falta capturar la descripcion");
						document.getElementById('descrip').focus();                                                        
						return false;
						}				
					if (subtotal==null || subtotal==0)
						{
						alert("Falta capturar el Subtotal del ticket");
						document.getElementById('subtotal').focus();                                                                                    
						return false;
						}		
					if (iva==null || iva==0)
						{
						alert("Falta capturar el I.V.A.");
						document.getElementById('iva').focus();
						return false;
						}		                            
//////////////////////////////////////////  Checar de que es esta informacion
	// alert (val_fac); //Lleva la informacion del total
	var val_fac = parseFloat($('totcheque').value.replace(/,/g,''));
	// Valida el importe capturado 
	var val_tot = parseFloat($('comprobado').value.replace(/,/g,'')) + parseFloat( total.replace(/,/g,'')) + parseFloat( efectivo.replace(/,/g,''));
	 //alert (val_tot);
	
	if(val_tot > val_fac )
	{
		alert("Error, El importe comprobado es mayor al del Cheque");
		limpia_variables();	
		return false;
	}
//////////////////////////////////////////////////////
var tabla = document.getElementById('datos');
// var tabla= $('datos'); // Se cambiaron los valores 
var numren=tabla.rows.length;
// Se cambiaron las variables de xticket a xfactura
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="factura")
				{
					xfactura=mynode.value;
					if (xfactura==factura)
						return;
				}
			}
		}
	}
//////////////////////////////////////////////////////////
var tabla = document.getElementById('datos');
var numren = tabla.rows.length;

var newrow = tabla.insertRow(numren);
	newrow.className ="fila";
	newrow.scope="row";
	
//var newcell0 = newrow.insertCell(0); //insert new cell to row
	//newcell0.width="1";
	//newcell0.className ="texto8";
	//newcell0.innerHTML =  '<input name="dfolio" class="texto8" id="dfolio" type="hidden" readonly value="'+folio+'" onclick="editar(this)" >';
	//newcell0.align ="center";

var newcell1 = newrow.insertCell(0); //insert new cell to row
	newcell1.width="10%";
	newcell1.className ="texto8";
	newcell1.innerHTML =  '<input name="drfc" class="texto8" id="drfc" style="width:65px" readonly value="'+rfc+'" onclick="editar(this)" >';
	newcell1.align ="center";

var newcell2 = newrow.insertCell(1); //insert new cell to row
	newcell2.width="8%";
	newcell2.className ="texto8";
	newcell2.innerHTML =  '<input name="dfactura" id="dfactura" class="texto8" style="width:65px" readonly value="'+factura+'" onclick="editar(this)" >';
	newcell2.align ="center";

var newcell3 = newrow.insertCell(2); //insert new cell to row
	newcell3.width="8%";
	newcell3.className ="texto8";
	newcell3.innerHTML =  '<input name="dfdetalle" id="dfdetalle" class="texto8" style="width:65px" readonly value="'+fdetalle+'" onclick="editar(this)" >';
	newcell3.align ="center";

var newcell4 = newrow.insertCell(3); //insert new cell to row
	newcell4.width="30%";
	newcell4.className ="texto8";
	newcell4.innerHTML =  '<input name="ddescrip" id="ddescrip" class="texto8" style="width:95%" readonly value="'+descrip+'" onclick="editar(this)" >';
	newcell4.align ="center";
	
var newcell5 = newrow.insertCell(4); //insert new cell to row
	newcell5.width="8%";
	newcell5.className ="texto8";
	newcell5.innerHTML =  '<input name="dsubtotal" id="dsubtotal" class="texto8" style="width:65px" readonly value="'+subtotal+'" onclick="editar(this)" >';
	newcell5.align ="center";

var newcell6 = newrow.insertCell(5); //insert new cell to row
	newcell6.width="8%";
	newcell6.className ="texto8";
	newcell6.innerHTML =  '<input name="diva" id="diva" class="texto8" style="width:65px" readonly value="'+iva+'" onclick="editar(this)" >';
	newcell6.align ="center";
	
var newcell7 = newrow.insertCell(6); //insert new cell to row
	newcell7.width="8%";
	newcell7.className ="texto8";
	newcell7.innerHTML =  '<input name="dtotal" id="dtotal" class="texto8" style="width:65px" readonly value="'+total+'" onclick="editar(this)" >';
	newcell7.align ="center";	
	$('comprobado').value = parseFloat($('comprobado').value.replace(/,/g,'')) + parseFloat( $('total').value.replace(/,/g,''));
	$('comprobado').value =Math.round($('comprobado').value*100)/100;
	$('comprobado').value=NumberFormat($('comprobado').value, '2', '.', ',');
	
	$('difxdevolver').value = parseFloat($('totcheque').value.replace(/,/g,'')) - parseFloat( $('comprobado').value.replace(/,/g,''));	
	$('difxdevolver').value =Math.round($('difxdevolver').value*100)/100;
	$('difxdevolver').value=NumberFormat($('difxdevolver').value, '2', '.', ',');
	//alert($('difxdevolver').value);
	$('totcheque').value=NumberFormat($('totcheque').value, '2', '.', ',');
var newcell8 = newrow.insertCell(7);
	newcell8.width="8%";
	newcell8.className ="texto8";
	var dfolio2 = folio;
	var dsubtotal2 = subtotal;
	var diva2 = iva;
	var dtotal2 = total;
	newcell8.innerHTML = '<img src="../../imagenes/eliminar.jpg"  title="Cancela comprobante de gasto" onClick="borrar(this,\''+dfolio2+'\',\''+dsubtotal2+'\',\''+diva2+'\',\''+dtotal2+'\')">' ;
	newcell8.align ="center";	
	

limpia_variables();	
} // Ya quedo lista la funcion de agregar registro // Funcion agregar quedo lista // Funcion agregar quedo lista


function editar(obj)  
// Edita los campos con tan solo seleccionar el registro
{
	// var tabla = $('datos'); // Mismo caso de sustitucion
	var tabla = document.getElementById('datos');	
	var par = obj . parentNode ;
	while( par . nodeName . toLowerCase ()!= 'tr' )
	{
		par = par . parentNode ;
	}
	// var ren=document.getElementById('ren').value;
	document.getElementById('ren').value=par . rowIndex;
	for (var j = 0; j < tabla.rows[par . rowIndex].cells.length;j++)
	{
		var cell = tabla.rows[par . rowIndex].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
			if(mynode.name=="drfc")
			{
				document.getElementById('rfc').value = mynode.value;
			}
			if(mynode.name=="dfactura")
			{
				document.getElementById('factura').value = mynode.value;
			}
			if(mynode.name=="dfdetalle")
			{
				document.getElementById('fdetalle').value = mynode.value;
			}
			if(mynode.name=="ddescrip")
			{
				document.getElementById('descrip').value = mynode.value;
			}
			if(mynode.name=="dsubtotal")
			{
				document.getElementById('subtotal').value = mynode.value;
			}
			if(mynode.name=="diva")
			{
				document.getElementById('iva').value = mynode.value;
			}
			if(mynode.name=="dtotal")
			{
			document.getElementById('total').value = mynode.value;
			}
		}
	}
} // Funcion de edicion quedo lista

function cal_iva2()
// Calcula el iva acorde a la tabla configmconfig
{
	//alert("aaaaaaaaaaaaaaaaaa");
	$('subtotal').value =Math.round($('subtotal').value*100)/100;
	new Ajax.Request('php_ajax/cal_iva.php',
		{onSuccess : function(resp) 
			{
				//alert(resp.responseText);
				if( resp.responseText ) 
				{
					var myArray = eval(resp.responseText);
					//alert("22 "+resp.responseText);
					if(myArray.length==0)
						{
						alert ("No existe en el catalogo");
						$("ivad").value=0;
						return false
						}
					  else
					  	{
						// totiva => checa como funciona esta seccion
					  	 var totiva = parseFloat((myArray[0].iva*parseFloat($('totcheque').value.replace(/,/g,'')))/100);
						 $('diva').value = totiva; // parseFloat(totiva);
						 $('d').value = parseFloat( $('totcheque').value.replace(/,/g,'')) + parseFloat(totiva);
						}
					}
					else
						return true;
				}
		});
}  // funcion calcular IVA que listo

function actualiza_datos() // Actualiza la informacion de la Facura a comprobar act()
{
	var rfc=$('rfc').value;
	var factura=$('factura').value;
	var fdetalle=$('fdetalle').value;
	var descrip=$('descrip').value;			
	var subtotal=$('subtotal').value;
	var iva=$('iva').value;
	var total=$('total').value;	
// Se genera el calculo de lo comprobado y diferencia del importe del cheque
	var totcheque = parseFloat($('totcheque').value.replace(/,/g,''));
//	var comprobado = parseFloat($('comprobado').value.replace(/,/g,'')) + parseFloat( d.replace(/,/g,''));
	var comprobado = parseFloat($('comprobado').value.replace(/,/g,''));
	if(comprobado > totcheque )
	{
		alert("La comprobacion de gastos es mayor a la del Cheque");
		return false;
	}
	if(rfc.length<1)
		{
		alert("Falta capturar RFC");
		return false;
		}
	if(factura.length<1)
		{
		alert("Falta capturar Factura");
		return false;
		}
	if(fdetalle.length<1)
		{
		alert("Falta capturar Fecha");
		return false;
		}
	if(descrip.length<1)
		{
		alert("Falta capturar la Descripcion");
		return false;
		}
	if(subtotal.length<1)
		{
		alert("Falta capturar Subtotal");
		return false;
		}
	if(iva.length<1)
		{
		alert("Falta capturar el IVA");
		return false;
		}
	if(total.length<1)
		{
		alert("Falta capturar el Total a comprobar");
		return false;
		}		
	
//	var tabla = $('datos');
	var tabla = document.getElementById('datos');	
	var ren=document.getElementById('ren').value;
	for (var j = 0; j <tabla.rows[ren].cells.length;j++)
	{
		var cell = tabla.rows[ren].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
			if(mynode.name=="drfc")
			{
				 mynode.value = document.getElementById('rfc').value;
			}
			if(mynode.name=="dfactura")
			{
				mynode.value = document.getElementById('factura').value;
			}
			if(mynode.name=="dfdetalle")
			{
				mynode.value = document.getElementById('fdetalle').value;
			}
			if(mynode.name=="ddescrip")
			{
				mynode.value = document.getElementById('descrip').value;
			}
			if(mynode.name=="dsubtotal")
			{
				mynode.value = document.getElementById('subtotal').value;
			}
			if(mynode.name=="diva")
			{
				mynode.value = document.getElementById('iva').value;
			}
			if(mynode.name=="dtotal")
			{
			mynode.value = document.getElementById('total').value;
			}
		}
	}
	limpia_variables();
	actualiza_nvostot();
	valida_efectivo();
}

function valida_impcheque() // Esta funcion hay que checarla
{
	if(	parseFloat($('gen').value.replace(/,/g,'')) > parseFloat($('').value.replace(/,/g,'')) )
	{
		alert("Error, Importe comprobado es mayor que el importe del Cheque");
		limpia_variables();		
		return false;
	}
}

function valida_factura()
 {
// Valida que no se repita la factura 	 
//	var depa=$('depa').value;
	var factura=$('factura').value;
	var tabla= $('datos');
//	var tabla = document.getElementById('datos');		
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 	
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="dfactura") 
				{
					xfactura=mynode.value;
					if (xfactura==factura) 
					{
						alert ("La Factura No." + document.getElementById('dfactura').value + " esta repetida");
						limpia_variables();
						return;
					}
				}
					
			}				
		}
	}	
}  // Funcion valida factura ya quedo lista y funcionando


function guarda_datos()
{
// Tabla egresosmsolchegas, este paso se va a realizar hasta el momento en el que vaya a actualizar ya en la Base de datos
	valida_impcheque();
	var concepto = document.getElementById('concepto').value;
	var prov = document.getElementById('numprov').value;
	var nomprov = document.getElementById('provname1').value;
	var factura = document.getElementById('factura').value;
	var sub = document.getElementById('sub').value;
	var iva = document.getElementById('iva').value;
	var fi = document.getElementById('fi').value;
	var ff = document.getElementById('ff').value;
	var depto = document.getElementById('depto').value;
	var usuario = document.getElementById('usuario').value;
	var nomdir = document.getElementById('nomdepto').value;
	var nomdepto = document.getElementById('nomdepto').value;
	var nomcoord = document.getElementById('nomcoord').value;
	var nomfirma = document.getElementById('nomfirma').value;
	var tabla= $('datos');
	var TotaProd= new Array( );
	var countPartidas=0;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < tabla.rows.length; i++) 
	{  //Iterate through all but the first row
		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="eco1")
				{
					eco0=mynode.value;
				}
				if(mynode.name=="numres1")
				{
					numres0=mynode.value;
				}
				if(mynode.name=="ticket1")
				{
					ticket0=mynode.value;
				}
				if(mynode.name=="kms1")
				{
					kms0=mynode.value;
				}
				if(mynode.name=="fecha1")
				{
					fecha0=mynode.value;
				}
				if(mynode.name=="lts1")
				{
					lts0=mynode.value;
				}
				if(mynode.name=="dsubtotal")
				{
					sub0=mynode.value;
				}
				if(mynode.name=="iva1")
				{
					iva0=mynode.value;
				}
				if(mynode.name=="usu_vehiculo1")
				{
					u_vehiculo0=mynode.value;
				}
				if(mynode.name=="usu_depto1")
				{
					u_depto0=mynode.value;
				}
				
			}
		}
		TotaProd[countPartidas] = new Object;
		TotaProd[countPartidas]['eco0']		= eco0;			
		TotaProd[countPartidas]['numres0']	= numres0;
		TotaProd[countPartidas]['ticket0']	= ticket0;
		TotaProd[countPartidas]['kms0']		= kms0;
		TotaProd[countPartidas]['fecha0']	= fecha0;
		TotaProd[countPartidas]['lts0']		= lts0;
		TotaProd[countPartidas]['sub0']= sub0;
		TotaProd[countPartidas]['iva0']		= iva0;
		TotaProd[countPartidas]['0']	= 0;
		TotaProd[countPartidas]['u_vehiculo0']	= u_vehiculo0;
		TotaProd[countPartidas]['u_depto0']	= u_depto0;

		countPartidas++;
	}
	var datas = {
				concepto: concepto,
				prov: prov,
				nomprov: nomprov,
				nomcoord: nomcoord,
				nomdepto: nomdepto,
				nomdir: nomdir,
				nomfirma: nomfirma,
				factura: factura,
				sub: sub,
				iva: iva,
				fi: fi,
				ff: ff,
				depto: depto,
				usuario: usuario,
				factura: factura,
				eco0: eco0,
				numres0: numres0,
				ticket0: ticket0,
				kms0: kms0,
				fecha0: fecha0,
				lts0: lts0,
				sub0: sub0,
				iva0: iva0,
				0: 0,
				u_vehiculo0: u_vehiculo0,
				u_depto0: u_depto0,
				prods: TotaProd
    };

	//alert(datas);
	jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/actualiza.php',
				data:          datas,
				success: function(resp) 
				{
					if( resp.length ) 
					{
						alert(resp);
						var myArray = eval(resp);
						
						if(myArray.length>0)
						{
								//alert(myArray[0].string);
								alert(resp);
								//resp=resp.replace('"','');
								resp=resp.replace(/"/g,'');
								window.open("pdf_files/sol_pago_gas_"+resp+".pdf");
								location.href='solpago_combustible.php';
						}
					}
				}
			});
} // Funcion para Guardar Datos ya funciona correctamente

function actualiza_nvostot() // CHECAR
{
//	$('subgen').value = 0;
//	$('ivagen').value = 0;
//	$('gen').value = 0;
//	var tabla= $('datos');
	$('comprobado').value = 0;
	$('difxdevolver').value = 0;
	
//	var tabla= $('datos');
	var tabla = document.getElementById('datos');	
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++) // Checar este FOR donde se realizan calculos 
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="dtotal")
				{
					dtotalx2=mynode.value;
					$('comprobado').value = parseFloat($('comprobado').value.replace(/,/g,'')) + parseFloat( dtotalx2.replace(/,/g,''));
					$('comprobado').value =Math.round($('comprobado').value*100)/100;
					$('comprobado').value=NumberFormat($('comprobado').value, '2', '.', ',');
				}
				if(mynode.name=="comprobado")
				{				
					difxdevolverx2=mynode.value;
					$('difxdevolver').value = parseFloat($('totcheque').value.replace(/,/g,'')) - parseFloat( difxdevolverx2.replace(/,/g,''));
					$('difxdevolver').value =Math.round($('difxdevolver').value*100)/100;
					$('difxdevolver').value=NumberFormat($('difxdevolver').value, '2', '.', ',');
				}								
			}
		}
	}
}


//function borrar(obj,ticket2,importe2,ivad2,d2) {	
function borrar(obj,dfolio2,dsubtotal2,diva2,dtotal2) {
  while (obj.tagName!='TR') 
    obj = obj.parentNode;
  tab = document.getElementById('datos');
  for (i=0; ele=tab.getElementsByTagName('tr')[i]; i++)
    if (ele==obj) num=i;
/////*****/////
	/*alert (dfolio2);
	alert (dsubtotal2);
	alert (diva2);
	alert (dtotal2);
	alert ($('comprobado').value);	*/
	$('comprobado').value = parseFloat($('comprobado').value.replace(/,/g,'')) - parseFloat( dtotal2.replace(/,/g,''));
	$('comprobado').value =Math.round($('comprobado').value*100)/100;
	$('comprobado').value=NumberFormat($('comprobado').value, '2', '.', ',');
	
	tab.deleteRow(num);
	valida_efectivo();
//  	limpia();
}

function limpia_variables()
{
		document.getElementById('rfc').value = ''; 
		document.getElementById('factura').value = '';
		document.getElementById('fdetalle').value = '';	
		document.getElementById('descrip').value = ''; 
		document.getElementById('subtotal').value = ''; 	
		document.getElementById('iva').value = ''; 	
		document.getElementById('total').value = ''; 
	} //Funcion limpia variables funciona


function actualiza_totales()
{
	$('totcheque').value = 0;
	$('comprobado').value = 0;
	$('efectivo').value = 0;
	$('difxdevolver').value = 0;	
	//var tabla= $('datos');
	var tabla = document.getElementById('datos');	
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="dsubtotal")
				{
					importex2=mynode.value;
					$('comprobado').value = parseFloat($('comprobado').value.replace(/,/g,'')) + parseFloat( subtotalx2.replace(/,/g,''));
				}
				if(mynode.name=="diva")
				{
					ivadx2=mynode.value;
					$('ivagen').value = parseFloat($('ivagen').value.replace(/,/g,'')) + parseFloat( ivadx2.replace(/,/g,''));
				}
				if(mynode.name=="dtotal")
				{
					totaldx2=mynode.value;
					$('totalgen').value = parseFloat($('totalgen').value.replace(/,/g,'')) + parseFloat( totaldx2.replace(/,/g,''));
				}
			}
		}
	}
}

function valida_total()
{
	if(	parseFloat($('total').value.replace(/,/g,'')) > parseFloat($('totcheque').value.replace(/,/g,''))+parseFloat($('totcheque').value.replace(/,/g,'')))
	{
		alert ("Valida Totales Funcion");
		alert("Error, El importe comprobado es mayor al del Cheque");
		limpia_variables();
		return false;
	}
}


function valida_efectivo()
{
	var totdifxdev = $('difxdevolver').value = (parseFloat($('totcheque').value.replace(/,/g,'')) - (parseFloat($('efectivo').value.replace(/,/g,''))) - parseFloat($('comprobado').value.replace(/,/g,'')));
	var tott = Math.round($('totdifxdev').value.replace(/,/g,'')*100)/100;	
	document.getElementById('difxdevolver').value = tott
	$('difxdevolver').value =Math.round($('difxdevolver').value*100)/100;
	$('difxdevolver').value=NumberFormat($('difxdevolver').value, '2', '.', ',');
	$('efectivo').value =Math.round($('efectivo').value*100)/100;
	$('efectivo').value=NumberFormat($('efectivo').value, '2', '.', ',');
	

	if (totdifxdev < 0)
		{
			alert("Error, La diferencia por devolver es incorrecta");
			limpia_variables();
			document.getElementById('subtotal').value = 0; 	
			return false;			
		}
}


function mod_iva()
{
	$('total').value=parseFloat($('subtotal').value.replace(/,/g,''))+parseFloat($('iva').value.replace(/,/g,''));
	$('total').value =Math.round($('total').value*100)/100;
	$('total').value=NumberFormat($('total').value, '2', '.', ',');
	return;
}
