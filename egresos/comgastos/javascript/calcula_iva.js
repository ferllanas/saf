function cal_iva()

{
	new Ajax.Request('php_ajax/cal_iva.php',
		{onSuccess : function(resp) 
			{
				//alert(resp.responseText);
				if( resp.responseText ) 
				{
					var myArray = eval(resp.responseText);
					//alert("22 "+resp.responseText);
					if(myArray.length==0)
						{
						alert ("No existe en el catalogo");
						$("iva").value=0;
						return false
						}
					  else
					  	{
						var subtot = document.getElementById('subtotal').value;
					  	var totiva = parseFloat((myArray[0].iva*parseFloat($('subtotal').value.replace(/,/g,'')))/100);
						$('iva').value=Math.round(totiva*100)/100;
						$('total').value = parseFloat( $('subtotal').value.replace(/,/g,'')) + parseFloat(totiva);
						$('total').value=Math.round($('total').value*100)/100;
						$('iva').value=NumberFormat($('iva').value, '2', '.', ',');
						$('total').value=NumberFormat($('total').value, '2', '.', ',');
						}
					}
					else
						return true;
				}
		});
} 
	
function cal_iva2()
{
	$('total').value = parseFloat( $('subtotal').value.replace(/,/g,'')) + parseFloat($('iva').value.replace(/,/g,''));
	$('total').value=Math.round($('total').value*100)/100;
}

function cal_neto()
{
	
	if ( parseFloat($('icic').value.replace(/,/g,'')) >  parseFloat($('total').value.replace(/,/g,'')))
		{
			alert("ICIC no debe ser mayor al total");
			$('icic').value=0;
		}
	   else
	    {
			if ( parseFloat( $('icic').value.replace(/,/g,'') ) >0.00)
				{
				$('neto').value = parseFloat($('total').value.replace(/,/g,'')) - parseFloat($('icic').value.replace(/,/g,''));
				}
			   else
				{
				$('neto').value = parseFloat($('total').value.replace(/,/g,''));
				}
		}

}

function imprime()
{
	var contrato=$('contrato').value;
	var prov=$('numprov').value;
	var nomprov=$('provname1').value;
	var tipoestima=$('tipoestima').value;
	switch(tipoestima)
	{
		case '1':
			tipoestima = 'ANTICIPO';
			break;
		case '2':
			tipoestima = 'ORDINARIA';
			break;
		case '3':
			tipoestima = 'ADITIVA';
			break;
		case '4':
			tipoestima = 'EXTRAORDINARIA';
			break;
		case '5':
			tipoestima = 'DEDUCTIVAS';
			break;
	}	
	var numestima=$('numestima').value;
	var total=$('total').value;
	var amortiza=$('amortiza').value;
	var subtotal=$('subtotal').value;
	var iva=$('iva').value;
	var total=$('total').value;
	var icic=$('icic').value;
	var neto=$('neto').value;
	var tipobien=$('tipobien').value;
	var tiporecurso=$('tiporecurso').value;
	switch(tiporecurso)
	{
		case '1':
			tiporecurso = 'ESTATAL';
			break;
		case '2':
			tiporecurso = 'PROPIO';
			break;
	}	
	var concepto=$('concepto').value;
	var factura=$('factura').value;
	var usuario=$('usuario').value;
	var depto=$('depto').value;
	var nomcoord=$('nomcoord').value;
	var nomdir=$('nomdir').value;
	var concepto_con = contrato+', '+tiporecurso+', '+tipoestima+', '+numestima+', '+concepto;
	location.href='php_ajax/impsol_pago.php?contrato='+contrato+"&tiporecurso="+
	tiporecurso+"&tipoestima="+tipoestima+"&numestima="+numestima+"&concepto_con="+
	concepto_con+"&nomprov="+nomprov+"&neto="+neto+"&factura="+factura+"&nomcoord="+nomcoord+"&nomdir="+nomdir;
}

function activaCheque() // Habilita nuevamente el campo Departamento
{
	document.getElementById('ugastos').value = '';
	document.getElementById('ugastos').disabled = false;
}