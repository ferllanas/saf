<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$depto ="";
$coord ="";
$dir ="";
$nomdepto ="";
$nomdir ="";
$productos = array();
$usuario = $_COOKIE['ID_my_site'];
$str_productos="";//variable que almacena la cadena de los productos
if(isset($_GET['productos']))
{
	$str_productos=$_GET['productos'];//Obtiene cadena de productos
	$str_productos = str_replace("\\", "", $str_productos);//reemplaza
	if(strlen($str_productos)>0)
	eval( '$productos = array'.$str_productos.';' );//la funcion eval permite obtener un array a partir de la cadena str_productos
	
}	

	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($conexion)
{
	//Busqueda de datos del departamento
	$command= "select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto=b.depto where numemp='$usuario'";
	$stmt2 = sqlsrv_query( $conexion, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$depto =trim($lrow['depto']);
			$coord =trim($lrow['coord']);
			$dir =trim($lrow['dir']);
			$nomdepto =trim($lrow['nomdepto']);
			$nomdir =trim($lrow['nomdir']);
			$i++;
		}
	}
	///Obtiene proveedores
	
	$command= "select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto=b.depto where numemp='$usuario'";
	$stmt2 = sqlsrv_query( $conexion, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$depto =trim($lrow['depto']);
			$coord =trim($lrow['coord']);
			$dir =trim($lrow['dir']);
			$nomdepto =trim($lrow['nomdepto']);
			$nomdir =trim($lrow['nomdir']);
			$i++;
		}
	}
	
	
}
?>
<html>
<head>
<title>Nota de Credito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">

<!--<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>-->

<script language="javascript" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<!--<script language="javascript" src="../../prototype/prototype.js"></script>-->
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="javascript/NotaCredito.js"></script>
<script language="javascript" src="javascript/busquedaincProv.js"></script>

</head>
<body >
<form id="form_NotaCredito"  name ="form_NotaCredito" style="width:100%"  metdod="post"  action="">

<table width="100%">
	<tr>
	<td colspan="4">
		<table width="100%">
			<tr>
				<td align="center" width="100%" class="TituloDForma" style="border-width:0px;">Nota de Credito<hr class="hrTitForma">
												<input type='hidden' id='usuario' name='usuario' value='<?php echo $usuario;?>'>
				</td>
			</tr>
		</table> 									
	</td>
	</tr>
	<tr>
		<td colspan="6" align="left" class="texto8"  width="100%">&nbsp;
			<div align="left" style="z-index:2; position:absolute; width:400px; left: 3px; top: 50px;">     
							Proveedor:<input class="texto8" type="text" id="nomprovBusInc" name="nomprovBusInc" style="width:250px;"  onKeyUp="searchProveedor(this);" autocomplete="off">
							<input type="hidden" id="provBusInc" name="provBusInc" value =0>			
							<div id="search_suggestProv" style="z-index:2;" > </div>
		  </div>  
		</td>
	</tr>
	
	<tr height="30px">		
	  <td class="texto8">Numero de NC :
        <input class="texto8" type="text" id="folioprov" name="folioprov" style="width:60px;" autocomplete="off" maxlength="20"></td>	  
	</tr>	
	<tr >		
	  <td class="texto8">Observaciones :
        <input class="texto8" type="text" id="observa" name="observa" style="width:430px;" autocomplete="off" maxlength="100"></td>	  
	</tr>	
	<tr>
		<td colspan="6" class="texto8"  width="100%">
			<input type="radio" name="optipo" id="st" value="st" checked onClick="cambiarTipoDDoc(this);">No surtido
			<input type="radio" name="optipo" id="nc" value="nc" onClick="cambiarTipoDDoc(this);">Devoluci&oacute;n 
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center" width="100%">
			<div align="left" style="z-index:1; position:absolute; left:172px; top: 149px;">       
							<a class="texto8"><input class="caja_toprint" type="text" id="nomtipo" name="nomtipo" value="Orden de Compra:" size="50px"  readonly> </a>
							<input name="txtSearch" type="text" id="txtSearch" size="10" width="50px" onKeyUp= "searchSuggest();" autocomplete= "off"/>  
                            <input type="hidden" id="cuadroc" name="cuadroc" >							
              				<input type="hidden" id="ordenprov" name="ordenprov">							
							<input class="texto8" type="button" value="Agregar Productos" id="ad_colin2" name="ad_colin2" onClick="agrega_ProductosOrden();" >  
				<br />
				<div id="search_suggest" style="z-index:2;" > </div>
			</div>    
		</td>		
	</tr>
	<tr>
		<td colspan="4"  width="100%">&nbsp;</td>
	</tr>
	<tr>
		<input type="hidden" id="prov" name="prov" VALUE="" style="width:100%;">
		<input type="hidden" id="orden" name="orden" VALUE="" style="width:100%;">
		<input type="text" class="caja_toprint_blanco" id="nomprov" name="nomprov" VALUE="" style="width:100%;">
	</tr>
  	<tr>
		<td colspan="4" align="center" width="100%">
			<table border="1" id="tmenudusuario" name="tmenudusuario" width="100%" >
				<tr>
					<th class="subtituloverde8" width="60%">Producto</th>
					<th class="subtituloverde8" width="10%">Cantidad Surtida</th>
					<th class="subtituloverde8" width="10%"><a id='tipodeC'>Cantidad No Surtida</a></th>	
					<th class="subtituloverde8" width="10%">Precio Unitario </th>
					<th class="subtituloverde8" width="9%">I.V.A</th>
                    <th class="subtituloverde8" width="1%"></th>
				</tr>
				<tbody id="menus">
					<?php  if( $productos!=null) 
						for($j=0;$j<count($productos);$j++){ ?>
						<tr id="filaOculta" >
							<td>
								<a class='texto8'>
								<?php echo $productos[$j]['nomprod']." ". $productos[$j]['descrip'];?></a>
								<input type='text' id='cantidad' name='cantidad' value='<?php echo $productos[$j]['cantidad'];?>' >
								<input type='text' id='iddcuadroc' name='iddcuadroc' value='<?php echo $productos[$j]['iddcuadroc'];?>'>
								<input type='text' id='tipoNCred' name='tipoNCred' value='<?php echo $productos[$j]['tipoNCredito'];?>' >
								<input type='text' id='numdoc' name='numdoc' value='<?php echo $productos[$j]['numdoc'];?>'>
								<input type='text' id='ctapresup' name='ctapresup' value='<?php echo $productos[$j]['ctapresup'];?>'>
							</td>
							<td>
								<input type='text' id='cantsurt' name='cantsurt' value='' readonly >
							</td>
							<td>
								<input type='text' id='cantrest' name='cantrest' value='' onKeyUp='javascript:calcularTotales(this,event);' style='width:50px'>
							</td>
							<td>
								<input type='text' id='precio' name='precio' value='' readonly style='width:50px'>
							</td>
							<td>
								<input type='text' id='ivapct' name='ivapct' value='' readonly style='width:50px'>
							</td>
                            <td><img src="../../imagenes/eliminar.jpg" width="13px" onClick="javascript: document.getElementById('menus').deleteRow(<?php echo $j+1;?>); "></td>
					<?php } ?>
					</tbody>
					
			</table>
			<table align="right">
				<tbody id="Tod" bgcolor="" style="border:0px; border-width:0px;" align="right">
						<tr>
							<td   class='texto8' align="right">SubTotal</td>
							<td ><input type="text" class="texto8" name="subtotal" id="subtotal" value="" readonly style="width:150px;text-align:right;"></td>
							<td class="texto8" align="right">I.V.A</td>
							<td class="texto8" > 	<input type="text" class="texto8" name="iva" id="iva" value="" readonly style="width:150px; text-align:right;">		</td>
							<td class="texto8"align="right">Total		</td>
							<td ><input type="text" class="texto8" name="total" id="total" value="" readonly style="width:150px; text-align:right;"></td>
						</tr>
					</tbody>
			</table>
		</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr>
		<td colspan="4">
			<table border="0"  width="100%">
				
			</table>
		</td>
	</tr>
	<tr>
	  <td align="left" width="50%">&nbsp;		</td>
	  <td align="left" width="50%"><input type="button"  value="Guardar Nota de Credito" onClick="guardarNotaCredito()" class="caja_entrada" >
		</td>
	</tr>
</table>
</form>

</body>
</html>