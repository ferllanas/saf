<?php
header("Content-type: text/html; charset=UTF-8");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');	

$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if(isset($_REQUEST['cuadroc']) && isset($_REQUEST['ordenprov']) && $conexion)
{
	$cuadroc=$_REQUEST['cuadroc'];
	$orden=$_REQUEST['ordenprov'];
	$command="select a.id as iddcuadroc,a.cuadroc,a.cantidad,a.ivapct,a.precio".$orden." as precio,a.subtotal".$orden." as subtotal,a.total".$orden." as total,a.sdocant,b.prod,c.nomprod,b.descrip , a.ctapresup
				from compradcuadroc a 
				left join compradrequisi b on a.iddrequisi=b.id 
				left join compradproductos c on b.prod=c.prod where a.cuadroc = $cuadroc and a.check".$orden."=1 and a.sdocant>0";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['iddcuadroc']= trim($row['iddcuadroc']);
			$datos[$i]['cuadroc']= trim($row['cuadroc']);
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['ivapct']= trim($row['ivapct']);
			$datos[$i]['precio']= trim($row['precio']);
			$datos[$i]['subtotal']= trim($row['subtotal']);
			$datos[$i]['sdocant']= trim($row['sdocant']);
			$datos[$i]['nomprod']= trim($row['nomprod']);		
			$datos[$i]['descrip']= trim($row['descrip']);	
			$datos[$i]['ctapresup']=trim($row['ctapresup']);	
			$i++;
		}
	}
	echo json_encode($datos);

}
?>