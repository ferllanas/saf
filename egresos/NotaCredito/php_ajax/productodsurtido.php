<?php
header("Content-type: text/html; charset=ISO-8859-1");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');	

$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if(isset($_REQUEST['surtido']) && isset($_REQUEST['ordenprov']) && $conexion)
{
	$surtido=$_REQUEST['surtido'];
	$orden=$_REQUEST['ordenprov'];
	$command="SELECT a.surtido, c.precio".$orden." as precio, c.subtotal".$orden." as subtotal, c.total".$orden.
				" as total, c.sdocant, c.cuadroc, c.cantidad, c.ivapct , e.nomprod, d.descrip, c.id as iddcuadroc, c.ctapresup
	FROM compramsurtido a 
			LEFT JOIN compradsurtido b ON a.surtido=b.surtido
			LEFT JOIN compradcuadroc c ON b.iddcuadroc= c.id
			LEFT JOIN compradrequisi d ON d.id =  c.iddrequisi
			LEFT JOIN compradproductos e ON e.prod= d.prod
	WHERe a.surtido= $surtido and c.check".$orden."=1 ";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['iddcuadroc']= trim($row['iddcuadroc']);
			$datos[$i]['cuadroc']= trim($row['cuadroc']);
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['ivapct']= trim($row['ivapct']);
			$datos[$i]['precio']= trim($row['precio']);
			$datos[$i]['subtotal']= trim($row['subtotal']);
			$datos[$i]['sdocant']= trim($row['sdocant']);
			$datos[$i]['nomprod']= trim(utf8_encode($row['nomprod']));		
			$datos[$i]['descrip']= trim(utf8_encode($row['descrip']));	
			$datos[$i]['ctapresup']= trim(utf8_encode($row['ctapresup']));	
			
			$i++;
		}
	}
	//print_r($datos);
	echo json_encode($datos);

}
?>

