<?php
header("Content-type: text/html; charset=ISO-8859-1");
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$usuario = "";//Variable id del usuario
	if(isset($_POST['usuario']));
		$usuario=$_POST['usuario'];
	
	$observaciones="";//Observaciones
	if(isset($_POST['observaciones']));
		$observaciones=$_POST['observaciones'];
	
	$folioprov="";
	if(isset($_POST['folioprov']));
		$folioprov=$_POST['folioprov'];
		
	$subtotal="";
	if(isset($_POST['subtotal']));
		$subtotal=$_POST['subtotal'];
	
	$subtotal=str_replace(",","",$subtotal);

	$numdoc="";
	if(isset($_POST['numdoc']));
		$numdoc=$_POST['numdoc'];

	$iva="";
	if(isset($_POST['iva']));
		$iva=$_POST['iva'];
	$iva=str_replace(",","",$iva);
	
	$total="";
	if(isset($_POST['total']));
		$total=$_POST['total'];
	$total=str_replace(",","",$total);
	
	$prov="";
	if(isset($_POST['prov']));
		$prov=$_POST['prov'];
	
	$tipo="";
	if(isset($_POST['tipo']));
		$tipo=$_POST['tipo'];
	
	$ttipo=$tipo;
/*	if($tipo=="1")
		$ttipo="1";
	else
		$ttipo="2";
*/
	$productos="";//variable que almacena la cadena de los productos
	if(isset($_POST['prods']))
		$productos=$_POST['prods'];
		
	
	$ffecha=date("Y-m-d");
	//echo $ffecha;
	$fails=false;
	$hora="";
	$id="";
	if ( is_array( $productos ) ) // Pregunta se es una array la variable productos
	{
		//Crear en tabla de requisicion
		list($id,$fecha,$hora)= fun_creaNotadCredito($folioprov,
														$prov,
														$numdoc, 
														$usuario,
														$observaciones, 
														$ffecha, 
														$ttipo,
														$subtotal,
														$iva,
														$total ); //crea nueva requisicion
		//echo $id;
		//if(!$fails)
			for( $i=0 ; $i  < count($productos) ; $i++ )
			{
				$stotaP = (float)$productos[$i]['precio']* (float)$productos[$i]['cantrest'];
				$ivaP = (float)$productos[$i]['cantrest']* ( (float)$productos[$i]['precio'] * ( (float)$productos[$i]['ivapct']/100 ) );
				$totalP = $stotaP + $ivaP ;
				if($ttipo=="1")
					$numdoc = $productos[$i]['iddcuadroc'];
				$fails=func_creaProductoDNotaDCredito($id, 
										$numdoc,//$productos[$i]['iddcuadroc'], 
										$productos[$i]['cantrest'], 
										$stotaP,
										$ivaP ,
										$totalP,
										$productos[$i]['ctapresup'],$ttipo);
			}
	}
	
	$datos=array();//prepara el aray que regresara
	$datos[0]['id']=trim($id);// regresa el id de la
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_creaNotadCredito($folioprov,
								$prov,
								$numdoc,
								$usuario,
								$observaciones, 
								$fecha, 
								$tipo,
								$subtotal,
								$iva,
								$total)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$id = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_egresos_A_mncredito(?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$folioprov,
								&$prov,
								//&$numdoc, 
								&$usuario,
								&$observaciones, 
								&$fecha, 
								//&$tipo,
								&$subtotal,
								&$iva,
								&$total);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die($tsql_callSP." ".  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$hora);
}

///Mandar el id docto
//si es devolucion manda el id del surtido
//si es no surtido mandacuaqdri comparativo
//Funcion para registrar el p�roductoa  la requisicion
function func_creaProductoDNotaDCredito($idd, 
										$idddcuadroc, 
										$cantrest, 
										$stotaP,
										$ivaP ,
										$totalP,
										$ctapresup,$ttipo)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$id="";//Store proc regresa id de la nueva requisicion
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_egresos_A_dncredito(?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$productos[$i]['producto'], $productos[$i]['cantidad'], $productos[$i]['descripcion'],'0',$productos[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = array(&$idd, 
					&$idddcuadroc, 
					&$cantrest, 
					&$stotaP,
					&$ivaP ,
					&$totalP,
					&$ctapresup,&$ttipo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die(  print_r($params)."".print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>