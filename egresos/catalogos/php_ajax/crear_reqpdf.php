<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<?php
include('../../../pdf/class.ezpdf.php');
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
//$usarioCreador = $_COOKIE['ID_my_site'];	
$nomEncuestador="";

$idReq="080001";
if(isset($_GET['id']))
	$idReq=$_GET['id'];
	
$idcoorsol="";
$nomcoor="";
$iddirec ="";
$nomdir="";
$datos=array();

	//Imprime la primera notificacion
	$doc =& new Cezpdf('LEGAL');
	
	$doc->selectFont('../../../fonts/Times-Roman.afm');
	$datacreator = array ('Title'=>'Creacion de PDF de la notificacion','Author'=>'Fomerrey','Subject'=>'Creacion de PDF de la notificacion','Creator'=>'fernando.llanas@fomerrey.gob.mx','Producer'=>'http://blog.unijimpe.net');
	$doc->addInfo($datacreator);
	
	//http://localhost/fome/imagenes/encabezadoFnuevo.jpg
	//$doc->addJpegFromFile('../../../imagenes/encabezadoFnuevo.jpg',  20, 800, 100,300);
	$doc->addJpegFromFile('../../../$imagenPDFPrincipal',20, 940, 148.5, 57,array('gap'));
	ob_end_clean(); //opcional
	$doc->addJpegFromFile('../../../'.$imagenPDFSecundaria,460, 940, 100.5, 57,array('gap'));
	//ob_end_clean(); //opcional
	$doc->setColor(0, 0 ,0);
	$doc->ezText("Fomento Metropolitano de Monterrey\nDirección de administración y Finanzas",12,array('justification'=>'center'));
	
	$doc->ezText("\n\nRequisición No. $idReq",12,array('justification'=>'left'));
	$doc->ezText("<b>Fecha:</b> ".date("d/m/Y")."  <b>Hora:</b>".date("H:i:s") ,10);
	$doc->ezText("\n\n\n\n" ,10);
	$doc->ezText("Solicitante: $idcoorsol $nomcoor\n        $iddirec $nomdir" ,10);
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);


	$command="SELECT  dr.unidad, dr.cantidad, p.prod,p.nomprod, p.ctapresup, c.NOMCTA FROM compradrequisi dr INNER JOIN compradproductos p ON dr.prod=p.prod INNER JOIN presupcuentasmayor c ON p.ctapresup=c.cuenta where c.estatus<'9000' AND dr.requisi='$idReq'";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['Descripcion']= trim($row['nomprod']);
			$datos[$i]['cuenta']= trim($row['ctapresup']);
			//$datos[$i]['Nombre']= trim($row['concepto_2']);
			$datos[$i]['Nombre']= trim($row['NOMCTA']);
			$i++;
		}
	}
	$doc->ezText("" ,8);//presupcuentasmayor, //where estatus<9000
	
	////creamos un nuevo array en el que pondremos un borde=1
	///y las cabeceras de la tabla las pondremos ocultas
	unset ($opciones_tabla);
	
	//// mostrar las lineas
	$opciones_tabla['showlines']=1;
	
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=0;
	
	//// lineas sombreadas
	$opciones_tabla['shaded']= 1;
	
	//// tamaño letra del texto
	$opciones_tabla['fontSize']= 10;
	
	//// color del texto
	//$opcio
	 //// color del texto
	$opciones_tabla['textCol'] = array(0,0,0);
	
	//// tamaño de las cabeceras (texto)
	$opciones_tabla['titleFontSize'] = 12;
	
	//// margen interno de las celdas
	$opciones_tabla['rowGap'] = 3;
	$opciones_tabla['colGap'] = 3;
	
	$titles = array('cantidad'=>'<b>Cantidad</b>', 'unidad'=>'<b>Unidad</b>', 'Descripcion'=>'<b>Descripción</b>', 'cuenta'=>'<b>Cuenta</b>', 'Nombre'=>'<b>Descripcion Cuenta</b>');
	$doc->ezTable($datos,$titles,"",$opciones_tabla);  
	
	$pdfcode = $doc->output();
	$dir = '../pdf_files';
	if(!file_exists($dir)){
		mkdir($dir, 0777);
	}
	
	$fname = $dir.'/'.$idReq.'.pdf';//tempnam(,'PDF_').//
	//echo $dir.'/'.$fml.'.pdf';
	$fp = fopen($fname, 'w');
	fwrite($fp,$pdfcode);
	fclose($fp);
	$doc->ezStream();


?>
<body>
</body>
</html>
