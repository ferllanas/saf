<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Programación de Cheques</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<link type="text/css" href="../../css/tablesscrollbody450.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>-->
<script language="javascript" src="../../prototype/jQuery.js"></script>
<!--<script language="javascript" src="../../prototype/prototype.js"></script>-->
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/buscarVales.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/busquedaIncrementalBanco.js"></script>

</head>

<body>
<table width="100%">
	<tr>
		<td width="100%" class="TituloDForma" style="border-width:0px;">Programaci&oacute;n de Pagos<hr class="hrTitForma"></td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td class="texto8" width="55%">
			<div align="left" style="z-index:1; position:relative; width:250px; top:0px; left:0px;">Proveedor:
			  <input class="texto8" type="text" id="provname1" name="provname1" style="width:180px;"  onKeyUp="searchProveedor1(this);" autocomplete="off" value="" tabindex="1"><input type="hidden" id="provid1" name="provid1" value =""><div id="search_suggestProv" style="z-index:2;" > </div>
										</div>
	  </td>
		<td width="40%" class="texto8" style="width:45%">
       		<div align="left" style="z-index:4; position:relative; width:350px; left: 0px; top: 0px; height: 19px;">     
				Banco:<input class="texto8" type="text" id="banco" name="banco" style="width:250px;"  onKeyUp="searchBancos(this);" autocomplete="off">
			<div id="search_suggestBanco" style="z-index:5;" > </div>
        </div>
<!--Banco:<input type="text" id="numbanco" name="numbanco" value="" onBlur="buscar_Banco();" style="width:35px" tabindex="2"><input type="text" id="nombanco" name="nombanco" value="" class="caja_toprint" style="width:200px" tabindex="-1" readonly="">--></td>
		<!--<td width="10%"><input type="button" id="buscar" name="buscar" value="Buscar"  onClick="buscarVales();"></td>-->
	</tr>	
	<tr>
		<td colspan="2" width="100%">
			<table width="100%">
				<tr>
					<td width="20%" class="texto8" valign="top">
                    	<table width="100%">
							<tr>
                            	<td width="100%" class="texto8">Todos en un solo cheque<input type="radio" id="checkall" name="tipoCheck" value="1" tabindex="4" checked></td>
                            </tr>
                            <tr>
							  	<td width="100%">Cheque por factura<input  type="radio" id="checaallone" name="tipoCheck" value="2" tabindex="5"  ></td>
                            </tr>
						</table></td>
					<td class="texto8" width="20%" valign="top">
						<table width="100%" class="texto8">
							<tr>
                            	<td>Todos en una Transferencia:<input type="radio" name="tipoCheck" value="3" tabindex="6"></td>
                            </tr>
                            <tr>
                            	<td>Transferencia por factura:<input type="radio" name="tipoCheck" value="4" tabindex="6"></td>
                            </tr>
						</table>
					</td> 
					<td width="5%" align="right"><input align="right" type="text" readonly="true" id="totcheque"  name="totcheque" value="0.00"  style="font-size:9px; width:90px;" tabindex="-1" class="caja_toprint">
				  </td>
					<td width="20%"><input type="button" id="genCheck"  name="genCheck" value="Programar" onClick="generarProgCheques()" style="font-size:9px; width:110px; " tabindex="6"></td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
        
        	 <table class="tableone" id="sortable" style="width:750px;">	
                <thead >
                    <tr>
                        <th class="th1" >&nbsp;</th>
                        <th class="th2">Tipo Doc.</th>
                        <th class="th3" >Factura</th>
                        <th class="th3" >Vale/Sol. Cheque.</th>
                        <th class="th4" >Monto $</th>
                        <th class="th5" >Fecha de pago</th>
                        <th class="th6" style="width:200px;">Concepto</th>
                    </tr>
                </thead>
                <tbody >
                    <tr>
                        <td colspan="8">
                            <div class="innerb">
                              <table class="tabletwo"  style="width:750px;">
                                  <tbody id="rvales" >
                                       
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
       		</table>
		<!--	<div  id="divToTable" name="divToTable">
				<table id="tableCC" name="tableCC" border="0" cellspacing="0" cellpadding="0"  width="100%">
					<thead style="top:0px;" width="100%">
						<tr>
							<th class="subtituloverde8" style="width:10px; ">&nbsp;</th>
							<th class="subtituloverde8" style="width:55px; ">Tipo Doc.</th>
							<th class="subtituloverde8" style="width:55px; ">Num. Doc.</th>
						
							<th class="subtituloverde8" style="width:100px; ">Monto $</th>
							<th class="subtituloverde8" style="width:100px; ">Total Docs.</th>
							<th class="subtituloverde8" style="width:100px; ">Concepto</th>
						</tr>
					</thead>
					<tbody id="rvales" name="rvales" align="center" valign="top" style="height: 100%; overflow-y: auto; overflow-x: hidden; width:100%" width="100%">                
					</tbody>
				</table>
				
				</div>-->
		</td>
	</tr>
</table>

</body>
</html>
