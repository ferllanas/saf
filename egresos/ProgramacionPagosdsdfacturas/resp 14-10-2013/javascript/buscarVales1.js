var facturasDProv=new Array(); 
var countFacturasdProv =0;

function revisa_DocSeleccionados()
{
	var selectedOne=false;

	for( var index=0;index< countFacturasdProv && selectedOne==false;index++)
		if(facturasDProv[index]['selected']==true)
			selectedOne=true;

	return selectedOne;
}

function generarProgCheques()
{
	var todosLoscheques=false;//0 todos los cheques
	var tipoTransoCheqA=0;
	var fecPago= '';//document.getElementById('fecPago').value;
	var idProveedor = document.getElementById('provid1').value;
	var numbanco= document.getElementById('banco').value.split(".-")[0];

	if(numbanco.length<=0)
	{
		alert("Debe ingresar o seleccionar un banco.");
		return false;
	}
	
	/*if(fecPago.length<=0)
	{
		alert("Favor de ingresar fecha para la programaci�n de pagos.");
		return false;
	}*/
	
	if( !revisa_DocSeleccionados() )
	{
		alert("Favor de seleccionar al menos una factura.");
		return false;
	}
	
	var tipo=0;
	for(var i=0;i<document.getElementsByName('tipoCheck').length;i++)
	{
		if(document.getElementsByName('tipoCheck').item(i).checked)
		{
			tipo=document.getElementsByName('tipoCheck').item(i).value;
		}
	}
	

	if( tipo<=0 && !document.getElementById('checaallone').checked)
	{
		alert("Selecciona el tipo de generaci�n de cheque.");
		return false;
	}

	if(tipo==1 || tipo==3)
	{
		todosLoscheques=true;
		var userInput = prompt("Teclee aqui el concepto del Cheque.", "")
		if (userInput == '' || userInput == null) 
		{
			alert("Debe ingresar un concepto para el vale.");
			return false;
		}
		else
		{
			if(userInput.length>200)
			{
				alert("El concepto solo puede aceptar 200 carateres.");
				return false;
			}
		
		}
		if(tipo==1)
			tipoTransoCheqA=10;
		else
			tipoTransoCheqA=20;	
	}
	else
	{
		
		if(tipo==2)
			tipoTransoCheqA=10;
		else
			tipoTransoCheqA=20;	
	}
	

	var answer = confirm("�Seguro deseas aplicar la generaci�n de cheques a los documentos seleccionados?")
	if (!answer)
	{
		return false;
	}

	//alert(facturasDProv);
	var datas = {
					tipo : todosLoscheques,
					tipoTransoCheq: tipoTransoCheqA,
					fechaemision: fecPago,
					idProveedor: idProveedor,
					numbanco	: numbanco,
					prods		: 		facturasDProv,
					concepto : userInput
    			};
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/programarCheque.php',
				data:     datas,
				success: function(resp) 
				{
					console.log(resp);
					if( resp.length ) 
					{
						//var myArray = eval(resp);
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								alert( "Se ha generado la programaci�n de cheque con el/los siguientes folios: " +resp[0].id + "." );
								//window.open('php_ajax/crear_Valepdf.php?id='+myArray[0].id,'Comparativo','width=800,height=600');
								location.href = 'prog_pagos.php';//?id='+myArray[0].id;
							}
							else
							{
								alert( "Error en proceso de actualizacion!." + resp[0].msgerror );
							}
						}
						else
							alert( "Error en proceso de actualizacion!." + resp );
					}
				}
			});
	
}

 

function calcularTotalCheque()
{
	var totalDocSelecteds=0.00;
	for(i=0;i<facturasDProv.length;i++)
	{
		//alert("Selected "+facturasDProv[i]['selected']);
		if(facturasDProv[i]['selected']==true)
		{
			
			totalDocSelecteds += parseFloat(facturasDProv[i]['total']);
		}
	}
	
	document.getElementById('totcheque').value= addCommas(totalDocSelecteds.toFixed(2));
}

function doctoSelected(id)
{
	//alert("doctoSelected");
	var index = getIndexDoct(id);//getIndexDoct(id_doc);
	if(facturasDProv[index]['selected']==true)
		facturasDProv[index]['selected']=false;
	else
		facturasDProv[index]['selected']=true;
	calcularTotalCheque();
}



function objfacturaDProv(id, idtipodocto , numdocto, factura, concepto ,total,subtotal,iva, totaldoc, vale, cargo, retencion, identificadors )//, id_dvale
{  
	this.id= id;
	this.concepto = concepto;
	this.total = total;
    this.idtipodocto = idtipodocto;
	this.numdocto = numdocto;	
	this.factura = factura;
	this.iva = iva;
	this.subtotal = subtotal;
	this.selected = false;
	this.totaldoc = totaldoc;
	this.vale = vale;
	this.cargo= cargo;
	this.retencion= retencion;
	this.identificador= identificadors;
	//this.id_dvale = id_dvale;
 }

//funcion que busca un numero de factura
function getIndexDoct(id)
{
	var retval=false;
	var index=-1;
	for(i=0;i<facturasDProv.length && retval==false;i++)
	{
		//alert(idtipodocto+"=="+facturasDProv[i]['idtipodocto']+" && "+numdocto+"=="+facturasDProv[i]['numdocto']);
		if(id==facturasDProv[i]['id'] )
		{	
			retval=true;
			index=i;
		}
	}	
	return index;
}

function buscarVales()
{
	var idProveedorq = document.getElementById('provid1').value;
	//alert(idProveedorq)
	var feciniq = "";//document.getElementById('fecini').value;
	var fecfinq = "";//document.getElementById('fecfin').value;
	
	var datas = {
					idProveedor : idProveedorq,
					fecini: feciniq,
					fecfin: fecfinq
    			};
	jQuery.ajax({
				type:     'post',
				cache:    false,
				url:      'php_ajax/buscarVales.php',
				dataType: "json",
				data:     datas,
				success: function(source) 
				{
					console.log(source);
					if (source != null) 
					{
						if(source.length>0)
						{
							var Table = document.getElementById('rvales');
							for(var i = Table.rows.length-1; i >=0; i--)
							{
								Table.deleteRow(i);
							}
							for(var i=0;i<source.length;i++)
							{
								agregar_Vales(source[i].descriptipodocto , 
												source[i].tipodocto,
												source[i].numdocto,
												source[i].concepto , 
												source[i].total,  
												source[i].totaldoc,
												source[i].factura,
												source[i].fpago,
												source[i].id_dvale,
												source[i].id);
								//alert(source[i].tipodocto+","+source[i].total+","+source[i].subtotal+","+source[i].iva);
								facturasDProv[countFacturasdProv]= new  objfacturaDProv(source[i].id ,
																						source[i].tipodocto , 
																						source[i].numdocto, 
																						source[i].factura,
																						source[i].concepto , 
																						source[i].total ,  
																						source[i].subtotal,
																						source[i].iva,
																						source[i].totaldoc,
																						source[i].id_dvale,
																						source[i].cargo,
																						source[i].retencion,
																						source[i].identificador
																						);
								countFacturasdProv++;
								document.getElementById('banco').focus();
							}
						}
						else
						{
							alert("No obtuvo resultados!.");
						}
					}
					else
					{
						alert("No obtuvo resultados!.");
					}
				}
			});
}

function agregar_Vales(descriptipodocto, idtipodocto , numdocto , concepto , total,totaldoc, factura, fpago, vale, idd )
{
	var Table = document.getElementById('rvales');
	var contRow = Table.rows.length;

	//Create the new elements
	var newrow = Table.insertRow(contRow);

	var newcell1 = newrow.insertCell(0);

	//alert(idtipodocto+',"'+numdocto+'",'+vale);
	var functionAA='doctoSelected('+idd+')';
	newcell1.className="td1";
	newcell1.innerHTML ="<input type='checkbox' style='width: 10px;' id='selectToPago' name='selectToPago' onClick='"+functionAA+"' tabindex='-1'>";
	
	var newcell2 = newrow.insertCell(1);
	newcell2.className="td2";
	newcell2.innerHTML ="<a class='texto8' >"+descriptipodocto+"</a>";

	var newcell3 = newrow.insertCell(2);
	newcell3.className="td3";
	if(idtipodocto==7)//Vale y Factura
	{
		
		newcell3.innerHTML =factura;/*"<input type='hidden' id='numvale' name='numvale' value='"+numdocto+
						"'><input type='hidden' id='iddvale' name='iddvale' value='"+numdocto+
						"'><a href='../vales/pdf_files/vale_"+numdocto+".pdf' class='texto8' title='Click aqui para ver el vale.' style='width: 55px;' tabindex='-1'>"+numdocto+"</a>";*/
	}
	else
	{
		//alert("AAA");
		newcell3.innerHTML =factura;// "<input type='hidden' id='numvale' name='numvale' value='"+numdocto+
						//"'><input type='hidden' id='iddvale' name='iddvale' value='"+numdocto+"'>"+numdocto+"</a>";
	}

	var newcell6= newrow.insertCell(3);
	newcell6.className="td4";
	if(idtipodocto==7)//Vale y Factura
	{
		newcell6.innerHTML ="<a class='texto8' style='width: 100px;' tabindex='-1'>"+vale+"</a>";
	}
	else
	{
		newcell6.innerHTML ="<input type='hidden' id='numvale' name='numvale' value='"+numdocto+
						"'><input type='hidden' id='iddvale' name='iddvale' value='"+numdocto+"'>"+numdocto+"</a>";
	}
	
	var newcell6= newrow.insertCell(4);
	newcell6.className="td4";
	newcell6.align="right";
	newcell6.innerHTML ="<a class='texto8' style='width: 100px;' tabindex='-1'>$ "+addCommas(total)+"</a>";

	var newcell7= newrow.insertCell(5);
	newcell7.className="td5";
	newcell7.align="center";
	newcell7.innerHTML ="<a class='texto8' style='width: 100px;' tabindex='-1'>"+fpago+"</a>";

	var newcell8= newrow.insertCell(6);
	newcell8.className="td6";
	newcell8.style.width="200px";
	newcell8.innerHTML =concepto;
	
	
	/*"<input type='text' name='concepto' id='concepto' value='"++"' style='width: 200px; text-align:left' onKeyUp='cambia_concepto(this,"+idtipodocto+","+numdocto+");' tabindex='-1'>";*/

}

/*function cambia_concepto(object, idtipodoc, numdoc)
{

	var indexVale= getIndexDoct(idtipodoc, numdoc);//getIndexDoct(id_vale);
	facturasDProv[indexVale]['concepto'] = object.value;
	//alert(facturasDProv[indexVale]['concepto']);
}

function cambia_pago(object, idtipodoc, numdoc)
{

	var indexVale= getIndexDoct(idtipodoc, numdoc);
	facturasDProv[indexVale]['totaldoc'] = object.value;
	//alert(facturasDProv[indexVale]['concepto']);
}*/

function buscar_Banco()
{

	var numbanco= document.getElementById('banco').value;
	if(numbanco.length>0)
	{
		numbanco= '0000'+numbanco;
		//alert(numbanco +","+ numbanco.length);
		numbanco = numbanco.substr( ( numbanco.length - 4) , 4 );
		 document.getElementById('banco').value = numbanco;
		new Ajax.Request('php_ajax/buscar_Banco.php?numbanco='+numbanco,
					{onSuccess : function(resp) 
						{
							if( resp.responseText ) 
							{
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									if(myArray[0].error=='false')
									{
										document.getElementById('nombanco').value = myArray[0].nombanco;

										var d = new Date();
										var curr_date = "0"+d.getDate();
										var curr_month = "0"+d.getMonth();
										var curr_year = d.getFullYear();
										curr_date = curr_date.substr( ( curr_date.length - 2) , 2 );
										curr_month = curr_month.substr( ( curr_month.length - 2) , 2 );
										document.getElementById('fecPago').value= curr_year+"/"+curr_month+"/"+curr_date;
									}
									else
										alert("Favor de verificar el n�mero de Banco!.");
								}
								else
								{
									document.getElementById('nombanco').value ="";
								}
							}
							else
							{
								document.getElementById('nombanco').value ="";
							}
						}
					});
	}
}


function deshabilitaConceptos()
{
	/*if(document.getElementById('checkall').checked==true)
		document.getElementById('concepto').readOnly = true;
	else
		document.getElementById('concepto').readOnly = false;*/
}