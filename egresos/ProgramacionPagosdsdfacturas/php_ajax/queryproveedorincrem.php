<?php
header("Content-type: text/html; charset=ISO-8859-1");

require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_REQUEST['q'] && $conexion)
{

	$terminos = explode(" ",$_REQUEST['q']);

	//$termino=$_GET['q'];
	$command= "SELECT a.prov, a.nomprov FROM compramprovs a INNER JOIN compradprovs b on a.prov=b.prov ";
	
	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if(!$paso)
		{
			$command.=" WHERE nomprov LIKE '%".$terminos[$i]."%'";
			$paso=true;	
		}
		else
			$command.=" AND nomprov LIKE '%".$terminos[$i]."%'";	
	}
	
	$command.=" AND a.estatus<9000 and b.estatus<9000  group by a.prov, a.nomprov ORDER BY a.nomprov ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['prov']= trim($row['prov']);
			$datos[$i]['nomprov']= trim( utf8_encode( $row['nomprov']));
			$datos[$i]['tipo']= "0";//trim($row['tipo']);
			//$sct.= trim($row['nomprov']).";".trim($row['prov']).";".trim($row['tipo'])."\n";//.";".trim($row['unidad']).
			$i++;
		}
	}
	echo json_encode($datos);//$sct);

}
?>