<?php
header("Content-type: text/html; charset=ISO-8859-1");
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$idProveedor="";
if(isset($_REQUEST['idProveedor']))
	$idProveedor = $_REQUEST['idProveedor'];
	
//print_r($idProveedor);

$fecini="";
if(isset($_POST['fecini']))
	$fecini = $_POST['fecini'];

$fecfin="";
if(isset($_POST['fecfin']))
	$fecfin = $_POST['fecfin'];

$entro=false;
$datos= array();
if($conexion)
{
	$command= "SELECT a.vale, b.factura , a.concepto, a.prov, b.total, 
		convert(varchar(10),a.fpago,103) as fpago, b.tipodocto as egresostipodocto, c.descrip
		, b.subtotal, b.iva, b.id 
		FROM egresosmvale a  
		INNER JOIN egresosdvale b ON a.vale=b.vale
		INNER JOIN egresostipodocto c ON c.id=b.tipodocto
		where b.estatus= 0 AND a.estatus=0 AND (b.anexo=0 OR ( b.anexo=1 AND  b.ruta_anexo IS NOT NULL) )";// AND a.prov=$idProveedor
	//echo $command;
	if(strlen($idProveedor)>0)
	{
		$command.= " AND a.prov = '$idProveedor'";
	}
	
	if(strlen($fecini)>0)
	{
		$command.= " AND a.fpago >= '$fecini'";
	}
	
	if(strlen($fecfin)>0)
	{
		$command.= " AND a.fpago <= '$fecfin'";
	}

	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']=$i;
			$datos[$i]['concepto']= trim($row['concepto']);
			$datos[$i]['tipodocto']= trim($row['egresostipodocto']);
			$datos[$i]['descriptipodocto']= trim($row['descrip']);
			
			if($datos[$i]['tipodocto']==1)
			{
				$datos[$i]['tipodocto']=7;
				$datos[$i]['descriptipodocto']="Vale";
			}
				
			if($datos[$i]['tipodocto']==4)
			{	$datos[$i]['total']= "-".trim($row['total']);
				$datos[$i]['iva'] = "-".$row['iva']; 
				$datos[$i]['totaldoc']= "-".trim($row['total']);
				$datos[$i]['subtotal'] = "-".$row['subtotal']; 
			}
			else
			{	$datos[$i]['total']= trim($row['total']);
				$datos[$i]['iva'] = $row['iva']; 
				$datos[$i]['totaldoc']= trim($row['total']);
				$datos[$i]['subtotal'] = $row['subtotal']; 
			}
				
				
			$datos[$i]['numdocto']= $row['vale'];//trim($row['factura']);
			$datos[$i]['factura'] = trim($row['factura']); 
			$datos[$i]['fpago'] = $row['fpago']; 
			$datos[$i]['id_dvale'] = $row['vale']; 
			$datos[$i]['cargo'] = 0.00; 
			$datos[$i]['retencion'] = 0.00;
			$datos[$i]['identificador']= trim($row['id']);
			$i++;
		}
	}
	
	$command= "select b.folio as vale,  b.c_definitivo as concepto, b.importe as total, convert(varchar(10),b.falta,103) as fpago ,
6 as egresostipodocto, c.descrip, b.iva as iva, b.subtotal as subtotal , b.factura, b.cargo, b.retencion, a.folio FROM egresosmopago a 
INNER JOIN egresosmsolche b ON a.folio=b.folio
INNER JOIN egresostipodocto c ON c.id=6 WHERE a.estatus=20 AND b.prov=$idProveedor" ;//  AND (b.anexo=0 OR ( b.anexo=1 AND  b.ruta_anexo IS NOT NULL) )

//echo $idProveedor;
	if(strlen($idProveedor)>0)
	{
		$command.= " AND b.prov = '$idProveedor'";
	}
	
	if(strlen($fecini)>0)
	{
		$command.= " AND a.fpago >= '$fecini'";
	}
	
	if(strlen($fecfin)>0)
	{
		$command.= " AND a.fpago <= '$fecfin'";
	}
	
//	echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']=$i;
			$datos[$i]['descriptipodocto']= trim($row['descrip']);
			$datos[$i]['concepto']= trim($row['concepto']);
			$datos[$i]['total']= trim($row['total']);
			$datos[$i]['fpago'] = $row['fpago']; 
			$datos[$i]['tipodocto']= trim($row['egresostipodocto']);
			$datos[$i]['numdocto']= trim($row['vale']);
			$datos[$i]['totaldoc']= trim($row['total']);
			$datos[$i]['factura'] = trim($row['factura']);
			
			if($row['subtotal']==null || $row['subtotal']=="null") 
				$datos[$i]['subtotal'] =0.00;
			else
				$datos[$i]['subtotal'] = $row['subtotal']; 
				
			if($row['iva']==null || $row['iva']=="null") 
				$datos[$i]['iva'] =0.00;
			else
				$datos[$i]['iva'] = $row['iva']; 
				
			$datos[$i]['id_dvale'] =  trim($row['folio']);; 
			if($row['cargo']!=null)
				$datos[$i]['cargo'] = $row['cargo']; 
			else
				$datos[$i]['cargo'] =0.00;
				
			if($row['cargo']!=null)
				$datos[$i]['retencion'] = $row['retencion']; 
			else
				$datos[$i]['retencion'] =0.00;
			$datos[$i]['identificador']= trim($row['folio']);
			$i++;
		}
	}
	echo json_encode($datos);
}
?>