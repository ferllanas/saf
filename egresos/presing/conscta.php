<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$wdatos=array();
$cuenta='';
$año='';

$cuenta='';
$mes='';
$aut='';
$real='';
$mod='';
$ent_trasp='';
$sal_trasp='';
$com='';
$dev='';
$eje='';
$pag='';
$cob='';
$cierre='';

//$usuario=$_COOKIE['ID_my_site'];
$usuario='001349';

if ($conexion)
{		
	$consulta = "select anio from presupadmin group by anio order by anio desc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['anio']= trim($row['anio']);
		$i++;
	}
}
///////
	
	$consulta = "select mes from presupadmin group by mes order by mes";
	$R = sqlsrv_query( $conexion,$consulta);
	if ( $R === false)
	{ 
		$resoponsecode="02";
		die($consulta."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($R);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			//$wdatos[$i]['mes']= trim($row['mes']);
			$mes = trim($row['mes']);
			$meses="ENERO     FEBRERO   MARZO     ABRIL     MAYO      JUNIO     JULIO     AGOSTO    SEPTIEMBREOCTUBRE   NOVIEMBRE DICIEMBRE ";
			$xmes=substr($meses,$mes*10-10,10);
			$wdatos[$i]['mes']=$xmes;
			$i++;
		}
		$wdatos[$i]['mes']="TODOS";
	}
$mes='';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincPres.js"></script>
<script language="javascript" src="javascript/conscta_fun.js"></script>
<!--<script language="javascript" src="../../prototype/jQuery.js"></script>-->
<script src="../../javascript_globalfunc/144jquery.min.js"></script>
<script language="javascript" src="../../cheques/javascript/jquery.tmpl.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script>
jQuery(document).ready(function() {
	jQuery(".botonExcel").click(function(event) {
		jQuery("#datos_a_enviar").val( jQuery("<div>").append( jQuery("#Exportar_a_Excel").eq(0).clone()).html());
		jQuery("#FormularioExportacion").submit();
	});
});
</script>
<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
		<tr>
			{{if cuenta}}
				<td width='50px' align="center">${cuenta}</td>
				<td width='100px' align="rigth">${nomcta}</td>
				<td width='50px' align="center">${rubro}</td>
				<td width='100px'>${nomrubro}</td>
				
				<td width='50px' align="center">${ANIO}</td>
				<td width='50px' align="center">${mes}</td>
				<td width='50px' align="right" style="text-align: right;">${aut}</td>
				<td width='50px' align="right" style="text-align: right;">${mod}</td>
				<td width='50px' align="right" style="text-align: right;">${real}</td>
				<td width='50px' align="right" style="text-align: right;">${dev}</td>
				<td width='50px' align="right" style="text-align: right;">${pag}</td>
				<td width='50px' align="right" style="text-align: right;">${cierre}</td>
			{{else}}
				<td colspan="2">No existen resultados</td>
				
			{{/if}}
		</tr>
</script>   
</head>

<body onLoad="llenarResultadoTipo()">
<table width="883" border="0">
  <tr>
    <td width="121">&nbsp;</td>
    <td width="158">&nbsp;</td>
    <td width="37">&nbsp;</td>
    <td width="471">&nbsp;</td>
    <td rowspan="3"><form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
<p>Exportar a Excel  <img src="../../imagenes/export_to_excel.gif" class="botonExcel" /></p>
<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
</form></td>
  </tr>
  <tr>
    <td>A&ntilde;o 
     
        <select name="anio" id="anio" onChange="meses()">
		<?php
			for($i = 0 ; $i<=count($datos);$i++)
			{
			echo "<option value=".$datos[$i]["anio"].">".$datos[$i]["anio"]."</option>\n";
			}
		?>
    </select>   </td>
    <td colspan="3">Periodo 
      <select name="mes" id="mes" onChange="filtro_mes()">
        <?php
			for($i = 0 ; $i<=count($wdatos);$i++)
			{
			echo "<option value=".($i+1).">".$wdatos[$i]["mes"]."</option>\n";
			}
		?>
      </select><div align="left" style="z-index:4; position:absolute; width:467px; top: 69px; left: 1px; height: 24px;">
      Cuenta: <input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" onkeyup="searchdepto2(this);" style="width:420px; height:18px; " autocomplete="off">
      <div class="texto8" id="search_suggestdepto2" style="z-index:3;" > </div>
    </div></td>
  </tr>
  <tr>
  	<td><select id="tipo" name="tipo" onChange="javascript: llenarResultadoTipo()">
    		<option value="Partida">Partida</option>
            <option value="Concepto">Concepto</option>
            <option value="Rubro">Rubro</option>
            <option value="1">TODOS</option>
        </select></td>
    <td><select id="resultadoTipo" name="resultadoTipo" onChange="filtro_mes()">
    <option value="">TODOS</option>
    	</select>
    </td>
    <td></td>
    <td></td>
  </tr>
</table>
<p>&nbsp;</p>
<table name="Exportar_a_Excel" id="Exportar_a_Excel" width="100%" height="10%" align="center">
	<thead>
        <tr>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde" >Cuenta</th>
             <th width="100px" style="font-size:10px;" align="center" class="subtituloverde" >Nom. Cuenta</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Rubro</th>
            <th width="100px" style="font-size:10px;" align="center" class="subtituloverde">Nom. Rubro</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">A&ntilde;o</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Mes</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Estimado</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Modificado</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Disponible</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Devengado</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">Recaudado</th>
            <th width="50px" style="font-size:10px;" align="center" class="subtituloverde">CIERRE</th>
        </tr>
    </thead>
    <tbody name="datos" id="datos" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
  	</tbody>
</table>


<!--<div style="overflow:auto; width: 100%; height :700px; align:center;">
</div>-->
<p>&nbsp;</p>
</body>
</html>
