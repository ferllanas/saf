<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$opago='';
$cta='';
$nomcta='';

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busqueda2.js"></script>
<script language="javascript" src="javascript/solche_fun.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>
<p class="TituloDForma">Cancelaci&oacute;n de Codificaci&oacute;n</p>
  <input type="hidden" name="opago" id="opago" value="<?php echo $opago?>;">
  <input type="hidden" name="cta" id="cta" value="<?php echo $cta?>;">
  <input type="hidden" name="nomcta" id="nomcta" value="<?php echo $nomcta?>;">

<hr class="hrTitForma">

<table width="1004" border="0" style="z-index:1 ">
  <tr>
    <td width="109"><span class="texto12">
      <select name="codifica" id="codifica" onChange="fun2()" tabindex="1">
        <option value=1>Contable</option>
        <option value=2>Presupuestal</option>
      </select>
    </span> </td>
    <td width="55" class="texto9"><select name="buscar" id="buscar" onChange="fun()" tabindex="2">
      <option value=1>Cuenta</option>
      <option value=2>Orden de Pago</option>
    </select></td>
    <td width="376">
		<div align="left" style="z-index:2; position:absolute; width:659px; left:40 height: 24px;left: 245px; top: 72px;">
    	  <input type="text" name="busca" id="busca"onkeyup="searchdepto2(this);" style="width:550px; height:18px; " autocomplete="off">
		<div class="texto8" id="search_suggestdepto2" style="z-index:3;" > </div>
	  </div>
    </td>
    <td width="436">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

 <table name="enc" id="enc" width="100%" height="10%" align="left" border="1">
   <tr>
     <th width="114" align="center" class="subtituloverde">Orden de Pago </th>
     <th width="116" align="center" class="subtituloverde">Cuenta</th>
     <th width="483" align="center" class="subtituloverde">Nombre de la Cuenta </th>
     <th width="91" align="center" class="subtituloverde">Monto</th>
     <th width="89" align="center" class="subtituloverde">Acci&oacute;n</th>
   </tr>
 </table>
<div style="overflow:auto; width: 100%; height :200px; align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos">
	  
	  </tbody>
	</table>
    <p>&nbsp;</p>
	<div align="left" style="position:absolute; width:1021px; top:429px; height: 227px;"></div>
</div>

<p>&nbsp; </p>
</body>
</html>
