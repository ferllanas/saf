<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	//$termino=$_GET['q'];
//	$command= "SELECT a.prov, a.oldprov, a.nomprov, b.tipo FROM compramprovs a INNER JOIN compradprovs b on a.prov=b.prov ";
	$command= "SELECT a.opago,a.folio,b.concepto,b.importe,a.estatus,b.prov,b.factura,CONVERT(varchar(10),b.falta, 103) as falta,";
	$command.=" c.nomprov from egresosmopago a left join egresosmsolche b on a.folio=b.folio left join compramprovs c ";
	$command.="on b.prov=c.prov WHERE ";	
	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if(!$paso)
		{
			$command.=" b.folio LIKE '%".$terminos[$i]."%'";
			$paso=true;	
		}
		else
			$command.=" b.concepto LIKE '%".$terminos[$i]."%'";	
	}
	
	$command.=" AND a.estatus=10  ORDER BY a.folio ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		//$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$datos[$i]['id']= trim($row['prov']);
			//$datos[$i]['valor']= trim($row['nomprov']);
			//$datos[$i]['unidad']= trim($row['unidad']);
		
			//$sct.= htmlentities(trim($row['opago']))."@".trim($row['concepto'])."@".trim($row['importe'])."@".trim($row['falta'])."@".trim($row['factura'])."@".trim($row['nomprov'])."\n";//.";".trim($row['unidad']).
			$sct.= htmlentities(trim($row['folio']))."@".htmlspecialchars (trim($row['concepto']))."@".trim($row['importe'])."@".trim($row['falta'])."@".trim($row['factura'])."@".trim($row['nomprov'])."\n";//.";".trim($row['unidad']).
			
		}
	}
	echo $sct;
}
?>