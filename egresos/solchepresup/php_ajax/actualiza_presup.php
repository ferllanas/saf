<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

// Date in the past 
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 

// always modified 
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 

// HTTP/1.1 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 

// HTTP/1.0 
header("Pragma: no-cache");  
	require_once("../../../dompdf/dompdf_config.inc.php");
	require_once("../../../Administracion/globalfuncions.php");
	require_once("../../../connections/dbconexion.php");
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$folio = $_REQUEST['folio0'];
$usuario = $_REQUEST['usuario0'];
$afecta = $_REQUEST['afecta0'];
$opago = $_REQUEST['opago'];
$variables = array();
$msg = "";
$cuenta0=" ";
$ccosto0=0;
$monto0=0;	
if ($conexion)
{

	$xxprods=array();
	if(isset($_REQUEST['prods']))
	$xxprods=$_REQUEST['prods'];
	$i=0;
	if(count($xxprods)==0)
	{
		$ejecuta ="{call sp_egresos_A_dopago_presup(?,?,?,?,?,?)}";
		$variables = array(&$opago,&$afecta,&$cuenta0,&$ccosto0,&$monto0,&$usuario);
		$R = sqlsrv_query($conexion, $ejecuta, $variables);
		if( $R === false )
		{
			 $msg = "Error in (detalle) statement execution.\n";
			 $fails=true;
			 print_r($variables);
			 die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$msg = trim($row['msg']);
			}
		}

	}
	else
	for($i=0;$i<count($xxprods);$i++)
	{
		$cuenta0=$xxprods[$i]['cuenta0'];
		$ccosto0=$xxprods[$i]['ccosto0'];
		$monto0=str_replace(',','',$xxprods[$i]['monto0']);	
	//	$fecha0=convertirFechaEuropeoAAmericano($xxprods[$i]['fecha0']);
		
		$ejecuta ="{call sp_egresos_A_dopago_presup(?,?,?,?,?,?)}";
		if($afecta=='SI')
			{
				$variables = array(&$opago,&$afecta,&$cuenta0,&$ccosto0,&$monto0,&$usuario);
			}
			else
			{
				$variables = array(&$opago,&$afecta,&$cuenta0,&$ccosto0,&$monto0,&$usuario);
			}
		$R = sqlsrv_query($conexion, $ejecuta, $variables);
		if( $R === false )
		{
			 $msg = "Error in (detalle) statement execution.\n";
			 $fails=true;
			 print_r($variables);
			 die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$msg = trim($row['msg']);
			}
		}
	
	}
}
//$msg.=$opago." ".$afecta." ".$cuenta0." ".$ccosto0." ".$monto0." ".$usuario;
echo json_encode($folio);
//echo json_encode($msg);
///////////////////////////////////////////////////////////////
?>