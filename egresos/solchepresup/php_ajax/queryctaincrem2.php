<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$sct="";
$datos= array();
$cod = $_REQUEST['cod'];
$bus = $_REQUEST['bus'];
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{
	$terminos = explode(" ",$_GET['q']);
	if ($cod==1)
	{
		$command = "select a.opago,a.ctacontab,b.cuenta,b.nombre,a.estatus,a.monto,c.folio from egresosdopago_contab a left join ";
		$command .="contabmcuentas b on a.ctacontab=b.cuenta where ";
		if($bus==1)
		{
			if (count($terminos)==1)
			{
				if (is_numeric($terminos[0]))
					$command .="b.cuenta like '%".$terminos[0]."%'";
				  else
					//$command .="(b.nombre LIKE '%".$terminos[0]."%') ";
					$command .="(b.nombre LIKE '%".$terminos[0]."%')";
			}
			   else
			   {
					$paso=false;
					for($i=0;$i<count($terminos);$i++)
					{
						if(!$paso)
						{
							$command .="(b.nombre LIKE '%".$terminos[0]."%' or c.folio LIKE '%".$terminos[0]."%') ";
							$paso=true;	
						}
						else
							$command.=" b.cuenta LIKE '%".$terminos[$i]."%'";
					}
				}
		}
		else
		{
		$command .="a.opago like '%".$terminos[0]."%'";
		}
		$command.= " and a.estatus<9000 ORDER BY b.nombre ASC";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$sct.= htmlentities(trim(utf8_encode($row['opago'])))."@".trim(utf8_encode($row['cuenta']))."@".trim(utf8_encode($row['nombre']))."\n";
			}
		}
		echo $sct;
	}
	
   else
   {
   
		$command = "select a.opago,a.ctapresup,b.cuenta,b.nomcta,a.estatus,a.monto from egresosdopago_presup a left join ";
		$command .="presupcuentasmayor b on a.ctapresup= b.cuenta where ";
		if($bus==1)
		{
	
			if (count($terminos)==1)
			{
				if (is_numeric($terminos[0]))
					$command .="b.cuenta like '%".$terminos[0]."%'";
				  else
					$command .="(b.nomcta LIKE '%".$terminos[0]."%') ";
			}
			   else
			   {
					$paso=false;
					for($i=0;$i<count($terminos);$i++)
					{
						if(!$paso)
						{
							$command .="(b.nomcta LIKE '%".$terminos[0]."%') ";
							$paso=true;	
						}
						else
							$command.=" b.cuenta LIKE '%".$terminos[$i]."%'";	
					}
				}
		}		
		else
		{
		$command .="a.opago like '%".$terminos[0]."%'";
		}
				
		$command.= " and a.estatus < 9000 ORDER BY b.nomcta ASC";
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$sct.= htmlentities(trim(utf8_encode($row['opago'])))."@".trim(utf8_encode($row['cuenta']))."@".trim(utf8_encode($row['nomcta']))."@".utf8_encode($row['monto'])."\n";
			}
		}
		echo $sct;
	}	
}
?>