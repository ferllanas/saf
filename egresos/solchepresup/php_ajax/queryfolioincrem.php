<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	//$termino=$_GET['q'];
//	$command= "SELECT a.folio,a.concepto,a.importe,a.prov,a.factura,CONVERT(varchar(10),a.falta, 103) as falta, ";
//	$command.= "b.nomprov, c.opago from egresosmsolche a left join compramprovs b on ";
//	$command.= "a.prov=b.prov left join egresosmopago c on a.folio=c.folio WHERE";
	$command= "SELECT a.opago, b.folio,b.concepto,b.importe,b.prov,b.factura,CONVERT(varchar(10),b.falta, 103) ";
	$command.= "as falta, c.nomprov from egresosmopago a left join egresosmsolche b on ";
	$command.= "a.folio=b.folio left join compramprovs c on b.prov=c.prov WHERE";

	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if(!$paso)
		{
			$command.=" a.folio LIKE '%".$terminos[$i]."%'";
			$paso=true;	
		}
		else
		$command.="b.concepto LIKE '%".$terminos[0]."%'";
	}
	$command.=" AND a.estatus=0  ORDER BY b.folio ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		//$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$sct.= utf8_encode(trim($row['folio']))."@".html_entity_decode(str_replace('"','',trim($row['concepto'])))."@".trim($row['importe'])."@".trim($row['falta'])."@".trim($row['factura'])."@".trim($row['prov'])."@".utf8_encode(trim($row['nomprov']))."@".trim($row['opago'])."\n";
		}
	}
	echo $sct;

}
?>