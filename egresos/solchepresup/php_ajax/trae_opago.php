<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$sct="";
$datos= array();
$cod = $_REQUEST['cod'];
$bus = $_REQUEST['bus'];
$cta = $_REQUEST['cta'];
$nomcta = $_REQUEST['nomcta'];
$opago = $_REQUEST['opago'];
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if ($cod==1)
{
	$command = "select a.opago,a.ctacontab,b.cuenta,b.nombre,a.estatus,a.monto from egresosdopago_contab a left join ";
	$command .="contabmcuentas b on a.ctacontab=b.cuenta where b.cuenta=$cta";
	$command .=" and a.opago=$opago and a.estatus < 9000 ORDER BY b.nombre ASC";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['opago']= trim($row['opago']);
			$datos[$i]['cuenta']= trim($row['cuenta']);
			$datos[$i]['nombre']= trim($row['nombre']);
			$datos[$i]['monto']= trim($row['monto']);
			$i++;
		}
	}
}
else
{
	$command = "select a.opago,a.ctapresup,b.cuenta,b.nomcta,a.estatus,a.monto from egresosdopago_presup a left join ";
	$command .="presupcuentasmayor b on a.ctapresup=b.cuenta where b.cuenta='$cta' ";
	$command .="and a.opago=$opago and a.estatus < 9000 ORDER BY b.nomcta ASC";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['opago']= trim($row['opago']);
			$datos[$i]['cuenta']= trim($row['cuenta']);
			$datos[$i]['nombre']= trim($row['nomcta']);
			$datos[$i]['monto']= trim($row['monto']);
			$i++;
		}
	}
}
echo json_encode($datos);
?>