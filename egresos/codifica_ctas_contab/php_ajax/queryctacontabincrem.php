<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	//$termino=$_GET['q'];
	$command = "select cuenta,nombre from contabmcuentas where ";
	if (count($terminos)==1)
	{
		$patron = "/^[[:digit:]]|[[:punct:]]$/";

			if (preg_match($patron, $terminos[0]))
			$command .="cuenta like '%".$terminos[0]."%'";
		  else
			$command .="(nombre LIKE '%".$terminos[0]."%') ";
	}
	   else
	   {
			$paso=false;
			for($i=0;$i<count($terminos);$i++)
			{
				if(!$paso)
				{
					$command .="(nombre LIKE '%".$terminos[0]."%') ";
					$paso=true;	
				}
				else
					$command.=" cuenta LIKE '%".$terminos[$i]."%'";	
			}
		}
	$command.= " and ultimonivel=1 and estatus = 0 ORDER BY cuenta ASC";

	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		//$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$datos[$i]['id']= trim($row['prov']);
			//$datos[$i]['valor']= trim($row['nomprov']);
			//$datos[$i]['unidad']= trim($row['unidad']);
			$sct.= htmlentities(trim($row['cuenta']))."@".trim($row['nombre'])."\n";//.";".trim($row['unidad']).
		}
	}
	echo $sct;

}
?>