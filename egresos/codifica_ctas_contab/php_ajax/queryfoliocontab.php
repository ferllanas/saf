<?php



require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$folio = $_REQUEST['folio'];
$datos= array();

$command="";
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

if($conexion)
{

	$command.= "select b.folio,b.opago,a.ctapresup,d.NOMCTA,a.monto,c.prov,CONVERT(varchar(10),c.falta, 103) as falta,";
	$command.= "c.importe,c.concepto,c.factura,e.nomprov from egresosdopago_presup a left join egresosmopago b on ";
	$command.= "a.opago=b.opago left join egresosmsolche c on b.folio=c.folio left join presupmcuentas d ";
	$command.= "on a.ctapresup=d.cuenta left join compramprovs e on c.prov = e.prov where ";
	$command.= " b.folio LIKE '%" .$folio. "%' AND b.estatus=10 ORDER BY b.folio ASC";
	//echo $command;

	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['folio']= trim($row['folio']);
			$datos[$i]['opago']= trim($row['opago']);
			$datos[$i]['ctapresup']= trim($row['ctapresup']);
			$datos[$i]['nomcta']= htmlentities(trim($row['NOMCTA']));
			$datos[$i]['monto']=  trim($row['monto']);
			$datos[$i]['nomprov']= trim($row['nomprov']);
			$datos[$i]['falta']= trim($row['falta']);
			$datos[$i]['importe']= trim($row['importe']);
			$datos[$i]['factura']= trim($row['factura']);
			$i++;		
			//$sct.= utf8_encode(trim($row['folio']))."@".html_entity_decode(str_replace('"','',trim($row['concepto'])))."@".trim($row['importe'])."@".trim($row['falta'])."@".trim($row['factura'])."@".trim($row['prov'])."@".utf8_encode(trim($row['nomprov']))."@".trim($row['opago'])."\n";
		}
	}
	echo json_encode($datos);
}
?>