<?php

// Date in the past 
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 

// always modified 
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 

// HTTP/1.1 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 

// HTTP/1.0 
header("Pragma: no-cache");  

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$cta = "";
$monto = 0;
$cta = trim($_REQUEST['cta']);
$monto = str_replace(',','',$_REQUEST['monto']);
if ($conexion)
{
	$ejecuta ="{call sp_presup_C_disponible(?,?)}";
	$variables = array(&$cta,&$monto);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	//echo $ejecuta;
	//print_r($variables);
	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		// print_r($variables);
		 //die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	if(!$fails)
	{
		//$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
	
			$datos[0]['cierre']= $row['cierre'];
			$datos[0]['disponible']= $row['disponible'];

		}
		sqlsrv_free_stmt( $R);
	}
}
echo json_encode($datos);
?>