<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$cuentacontab='';
$folio='';
$nomctacontab='';
$total=0;
$totald=0;
$tdebe=0;
$thaber=0;

$falta="";
$factura="";
$proveedor="";
$usuario=$_COOKIE['ID_my_site'];
//$usuario='002294';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincPres.js"></script>
<script language="javascript" src="javascript/solche_funcontab.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>
 <p class="TituloDForma">Codificaci&oacute;n Contable<span class="texto10"><strong>
   <input type="hidden" name="ctacontab" id="ctacontab" value="<?php echo $ctacontab;?>">
   <input type="hidden" name="nomctacontab" id="nomctacontab" value="<?php echo $nomctacontab;?>">
   <input type="hidden" name="ren" id="ren">
   <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
 </strong></span></p>
 <table width="1234" height="36" border="0">
   <tr>
     <td width="122"><span class="texto10">Solicitud de Cheque </span></td>
     <td width="146"><div align="left" style="z-index:8; position:relative; width:140px; height: 24px;">
       <input class="texto8" type="text" id="folio" name="folio" style="width:40%; height:18px;" onKeyUp="searchcontabx();" autocomplete="off" tabindex="1">
       <div id="search_suggestcontabx" style="z-index:7;" > </div>
     </div></td>
     <td width="952" id="datos_prov" style="visibility:hidden " ><span class="texto10"><strong>Fecha</strong></span>
         <input type="text" name="falta" id="falta" size="10" style="background-color:#EEFDB3 " readonly value="<?php echo $falta;?>">
         <span class="texto10"><strong>Factura</strong></span>
         <input type="text" name="factura" id="factura" style="background-color:#EEFDB3 " readonly value="<?php echo $factura;?>">
         <span class="texto10"><strong>Proveedor</strong></span><span class="texto10">
         <input type="text" name="proveedor" id="proveedor" style="background-color:#EEFDB3 " readonly size="80" value="<?php echo $proveedor;?>">
       </span></td>
   </tr>
 </table>
 <table width="915" height="33" border="0">
   <tr>
     <td width="89" height="29"><span class="texto10">Cta. Contable</span>	    </td>
        <td width="378"><div align="left" style="z-index:4; position:relative; width:340px; height: 24px;">
          <input name="cuentacontab" type="text" class="texto8" disabled id="cuentacontab" tabindex="2" onkeyup="searchcontab(this);" style="width:340px; height:18px; " autocomplete="off">
          <div class="texto8" id="search_suggestcontab" style="z-index:3;" > </div>
        </div></td>
     <td width="35"><span class="texto10">Debe</span></td>
     <td width="104"><input type="text" name="debe" id="debe" size="15" disabled tabindex="3" onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" value=0></td>
     <td width="35" class="texto10">Haber</td>
     <td width="149"><input type="text" name="haber" id="haber" size="15" tabindex="4" disabled onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" value=0></td>
     <td width="84"><div align="center"><span class="texto9"><img src="../../imagenes/agregar2.jpg" id="agrega_ren" width="30" height="25" title="Agregar Cuenta." onClick="agregar_contab()"><img src="../../imagenes/actualiza3.png" id="act_ren" width="30" height="25" style="visibility:hidden; " title="Actualiza Datos de Cuenta." onClick="act_contab()"></span></div></td>
     <td width="7"><div align="center"></div></td>
   </tr>
</table>
 <table name="enc" id="enc" width="100%" height="10%" align="left" border="1">
   <tr>
     <th width="102" align="center" class="subtituloverde">Cuenta</th>
     <th width="428" align="center" class="subtituloverde">Nombre de la Cuenta </th>
     <th width="82" align="center" class="subtituloverde">Debe</th>
     <th width="82" align="center" class="subtituloverde">Haber</th>
     <th width="80" align="center" class="subtituloverde">Acci&oacute;n</th>
   </tr>
 </table>
<div style="overflow:auto; width: 100%; height :200px; align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
    <p>&nbsp;</p>
	<div align="left" style="position:absolute; width:1021px; top:429px; height: 227px;">
    <table width="1035" border="0" align="left">
      <tr>
        <td width="516" class="texto10">&nbsp;</td>
        <td width="85" class="texto10">Tot. Debe </td>
        <td width="165" class="texto10"><input type="text" name="tdebe" id="tdebe" align="right" readonly value="<?php echo $tdebe;?>"></td>
        <td width="68" class="texto10">Tot. Haber</td>
        <td width="179" class="texto10"><span class="texto10">
          <input type="text" name="thaber" id="thaber" align="right" readonly value="<?php echo $thaber;?>">
        </span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><span class="texto10">
          <input type="hidden" name="totald" id="totald" readonly value="<?php echo $totald;?>">
        </span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <input type="button" name="Grabar" value="Grabar" id="Grabar" readonly onClick="guarda_datos()">
       </td>
        <td class="texto10"><form name="form1" method="post" action="">
          <input type="button" name="cancela" id="cancela" value="Cancelar" onClick="limpia_pantalla()">
        </form></td>
        <td colspan="2"><div align="right"><span class="texto10">
        </span><span class="texto10">Monto de la Solicitud del Cheque </span></div></td>
        <td><span class="texto10">
          <input type="text" name="total" id="total" align="right" readonly value="<?php echo $total;?>">
        </span></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
