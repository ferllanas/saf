<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$datos2=array();
$datos3=array();
$total=0;
$fecha="";
$factura="";
$prov="";
$importe=0;
$tdebe=0;
$thaber=0;

$folio=0;
$opago=0;
if(isset($_REQUEST['folio']))
	$folio = $_REQUEST['folio'];

if(isset($_REQUEST['opago']))
	$opago = $_REQUEST['opago'];

//$usuario=$_COOKIE['ID_my_site'];
$usuario='001349';

if ($conexion)
{
	$consulta = "select a.folio,CONVERT(varchar(10),a.falta, 103) as fecha,a.factura,a.importe,d.prov,d.nomprov ";
	$consulta .= "from egresosmsolche a left join egresosmopago b on a.folio=b.folio left join egresosdopago_presup c ";
	$consulta .= "on b.opago=c.opago left join compramprovs d on a.prov=d.prov where a.folio=$folio and b.estatus<9000 and ";
	$consulta .= "c.estatus<9000 group by a.folio,a.falta,a.factura,a.importe,d.prov,d.nomprov";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
		$folio=$datos[$i]['folio']= trim($row['folio']);
		$fecha=$datos[$i]['fecha']= trim($row['fecha']);
		$factura=$datos[$i]['factura']= trim($row['factura']);
		$prov=$datos[$i]['prov']= trim($row['prov']).' '.trim($row['nomprov']);
		$importe=$datos[$i]['importe']= trim($row['importe']);
		$i++;
		}
		
	$consulta1 = "select a.ctapresup,b.nomcta,a.ccostos,c.descrip,a.monto from egresosdopago_presup a left join ";
	$consulta1 .= "presupcuentasmayor b on a.ctapresup=b.cuenta left join configmccostos c on a.ccostos=c.ccostos ";
	$consulta1 .= "where a.opago=$opago and a.estatus<9000 and b.estatus<9000";
	$R = sqlsrv_query( $conexion,$consulta1);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
		$datos2[$i]['ctapresup']= trim($row['ctapresup']);
		$datos2[$i]['nomcta']= trim($row['nomcta']);
		$datos2[$i]['ccostos']= trim($row['ccostos']);
		$datos2[$i]['descrip']= trim($row['descrip']);
		$datos2[$i]['monto']= trim($row['monto']);
		$i++;
		}		

		
	$consulta2 = "select a.opago,a.ctacontab,a.debe,a.haber,b.nombre from egresosdopago_contab a left join ";
	$consulta2 .= "contabmcuentas b on a.ctacontab=b.cuentax where a.opago=$opago and a.estatus<9000";
	$R = sqlsrv_query( $conexion,$consulta2);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
		$datos3[$i]['ctacontab']= trim($row['ctacontab']);
		$datos3[$i]['nombre']= trim($row['nombre']);
		$datos3[$i]['debe']= trim($row['debe']);
		$datos3[$i]['haber']= trim($row['haber']);
		$tdebe=$tdebe+($row['debe']);
		$thaber=$thaber+($row['haber']);
		$i++;
		}				
		
}




?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincPres.js"></script>
<script language="javascript" src="javascript/solche_fun.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>
 <p class="texto13B">Codificaci&oacute;n Presupuestal<span class="texto9"><span class="texto10"><strong>
   <input type="hidden" name="cta" id="cta" value="<?php echo $cta;?>">
   <input type="hidden" name="nomcta" id="nomcta" value="<?php echo $nomcta;?>">
   <input type="hidden" name="ccos" id="ccos" value="<?php echo $ccos;?>">
   <input type="hidden" name="nomcto" id="nomcto" value="<?php echo $nomcto;?>">
   <input type="hidden" name="ren" id="ren">
   <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
   <strong>
   <input type="hidden" name="opago" id="opago" value="<?php echo $opago;?>">
</strong></strong></span></span></p>
 <hr class="hrTitForma">
 <table width="1250" height="36" border="0">
   <tr>
     <td width="226"><span class="texto10"><strong>Solicitud de Cheque</strong>      
       <input class="texto8" type="text" id="folio" name="folio" style="width:20%; height:18px;" value="<?php echo $folio; ?>">
     </span></td>
     <td width="26">&nbsp;
     </td>
     <td width="984" ><span class="texto10"><strong>Fecha</strong></span>
       <input type="text" name="falta" id="falta" size="10"  readonly value="<?php echo $fecha;?>">
       <span class="texto10"><strong>Factura</strong></span>
       <input type="text" name="factura" id="factura" readonly value="<?php echo $factura;?>">       <span class="texto10">       <strong>Importe</strong>
       <input align="right" class="texto10" style="text-align:right " type="text" name="importe" id="importe" readonly size="10" value="<?php echo number_format($importe,2);?>"> 
     </span></td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td > <span class="texto10"><strong>Proveedor</strong></span><span class="texto10">
     <input type="text" name="proveedor" id="proveedor" readonly size="80" value="<?php echo $prov;?>">
     </span></td>
   </tr>
</table>
 <table name="enc" id="enc" width="95%" height="10%" align="left" border="1">
   <tr>
     <th width="102" align="center" class="subtituloverde">Cuenta</th>
     <th width="113" align="center" class="subtituloverde">Centro de Costos</th>
     <th width="428" align="center" class="subtituloverde">Nombre de la Cuenta</th>
     <th width="82" align="center" class="subtituloverde">Monto</th>

   </tr>
 </table>
<div style="overflow:auto; width: 95%; height :200px; align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="left" style="overflow:auto; " >
	 <?php 
		for($i=0;$i<count($datos2);$i++)
		{
	?>
	<tr>
		 <td width="102" align="center" class="texto10"><?php echo $datos2[$i]['ctapresup'];?></td>
		 <td width="113" align="center" class="texto10"><?php echo $datos2[$i]['ccostos'];?></td>
		 <td width="428" align="center" class="texto10"><?php echo $datos2[$i]['nomcta'];?></td>
		 <td width="82" align="center" class="texto10"><?php echo number_format($datos2[$i]['monto'],2);?></td>
	 </tr>
 	<?php
		}
	?>
	</table>
</div>
    </div>
<p class="TituloDForma">&nbsp;</p>
 <p class="TituloDForma">Codificaci&oacute;n Contable<span class="texto10"><strong>
   <input type="hidden" name="ctacontab" id="ctacontab" value="<?php echo $ctacontab;?>">
   <input type="hidden" name="nomctacontab" id="nomctacontab" value="<?php echo $nomctacontab;?>">
   <input type="hidden" name="ren" id="ren">
   <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
</strong></span></p>
 <hr class="hrTitForma">

 <table name="enc" id="enc" width="95%" height="10%" align="left" border="1">
   <tr>
     <th width="102" align="center" class="subtituloverde">Cuenta</th>
     <th width="428" align="center" class="subtituloverde">Nombre de la Cuenta </th>
     <th width="82" align="center" class="subtituloverde">Debe</th>
     <th width="82" align="center" class="subtituloverde">Haber</th>
   </tr>
 </table>
<div style="overflow:auto; width: 100%; height :200px; align:center;"> 
	<table name="master" id="master" width="95%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <?php 
		for($i=0;$i<count($datos3);$i++)
		{
	?>

	<tr>
		 <td width="102" align="center" class="texto10"><?php echo $datos3[$i]['ctacontab'];?></td>
		 <td width="428" align="center" class="texto10"><?php echo $datos3[$i]['nombre'];?></td>
		 <td width="82" align="center" class="texto10"><?php echo number_format($datos3[$i]['debe'],2);?></td>
		 <td width="82" align="center" class="texto10"><?php echo number_format($datos3[$i]['haber'],2);?></td>
	 </tr>
 	<?php
		}
	?>	 
	</table>
    <p>&nbsp;</p>
  <div align="left" style="position:absolute; width:100%; top:447px; height: 209px;">  </div>
    <table width="95%" border="0" align="left">
      <tr>
        <td width="89" class="texto10">&nbsp;</td>
        <td width="380" class="texto10">&nbsp;</td>
        <td width="159" class="texto10">&nbsp;</td>
        <td width="97" align="right" class="texto10"><input type="text" style="text-align:right " name="tdebe" id="tdebe" size="12px" align="right" readonly value="<?php echo number_format($tdebe,2);?>"></td>
        <td width="90" align="right" class="texto10">
          <input type="text" name="thaber" id="thaber" style="text-align:right " align="right" size="12px" readonly value="<?php echo number_format($thaber,2);?>">
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><span class="texto10">
        </span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
</div>



</body>
</html>
