function limpia_pantalla()
{
	location.href='solche_contab.php';
}

function valida_folio()
{
	var folio = $('folio').value;
	if(folio.length<=0)
	{
		alert ("Capturar Folio");return false;
	}
	new Ajax.Request('php_ajax/valida_folio.php?folio='+folio,
	{onSuccess : function(resp) 
		{
			//alert(resp.responseText);
			if( resp.responseText )
			{
				var myArray = eval(resp.responseText);
				//alert("22 "+resp.responseText);
				if(myArray.length>0)
				{
					alert ("Num. de Folio no valido");
					return false
				}
				else
					return true;
			}
		}
	});
	//////////////////////////////
}


function agregar_contab()
{
	var folio=$('folio').value;
	var cuentacontab=$('cuentacontab').value;
	var ctacontab=$('ctacontab').value;
	var nomctacontab=$('nomctacontab').value;
	var debe=$('debe').value;
	var haber=$('haber').value;

	var total=$('total').value;
	var totald=$('totald').value;
	if(folio.length<1)
	{
		alert("Falta capturar folio");
		document.getElementById('folio').focus();
		return false;
	}
	if(ctacontab.length<1)
	{
		alert("Falta capturar la Cuenta Contable");
		document.getElementById('cuentacontab').focus();
		return false;
	}

	if(debe.length<1)
	{
		debe = 0;
	}
	if(haber.length<1)
	{
		haber = 0;
	}

	/*var totd=parseFloat($('totald').value.replace(/,/g,''))+parseFloat($('debe').value.replace(/,/g,''));
	var tot=parseFloat($('total').value.replace(/,/g,''));
	if (totd>tot)
	{
		alert("Error, Importe total de cuentas es diferente que el Total General, modifique el monto a ingresar");
		return false;
	}*/
//////////////////////////////////////////////////////
	var tabla= $('datos');
	var numren=tabla.rows.length;
//////////////////////////////////////////////////////////
	//var tabla = document.getElementById('datos');
	//var numren = tabla.rows.length;
	var newrow = tabla.insertRow(numren);

	//var nren =numren+1;

	newrow.className ="fila";
	newrow.scope="row";

	var newcell = newrow.insertCell(0); //insert new cell to row
	newcell.width="135";
	newcell.className ="texto8";
	newcell.innerHTML =  '<input name="cuenta1" id="cuenta1" style="width:120px" readonly value="'+ctacontab+'" onclick="editar(this)" >';
//	newcell.innerHTML =  nren;
	newcell.align ="center";
	
	var newcell1 = newrow.insertCell(1);
	newcell1.width="549";
	newcell1.className ="texto8";
	newcell1.innerHTML = '<input name="nomcta1" id="nomcta1" style="width:500px" readonly onclick="editar(this)" value="'+nomctacontab+'">';
	newcell1.align ="center";
	
	var newcell2 = newrow.insertCell(2);
	newcell2.width="86";
	newcell2.className ="texto8";
	newcell2.innerHTML = '<input name="debe1" id="debe1" readonly  style="width:100px" onclick="editar(this)" value="'+debe+'" >';
	newcell2.align ="right"; 
	if(debe>0)
	{
		$('tdebe').value = parseFloat($('tdebe').value.replace(/,/g,'')) + parseFloat( $('debe').value.replace(/,/g,''));
		$('tdebe').value=Math.round($('tdebe').value*100)/100;
		$('tdebe').value=NumberFormat($('tdebe').value, '2', '.', ',');
	}
	
	var newcell3 = newrow.insertCell(3);
	newcell3.width="86";
	newcell3.className ="texto8";
	newcell3.innerHTML = '<input name="haber1" id="haber1" readonly  style="width:100px" onclick="editar(this)" value="'+haber+'" >';
	newcell3.align ="right"; 
	if(haber>0)
	{
		$('thaber').value = parseFloat($('thaber').value.replace(/,/g,'')) + parseFloat( $('haber').value.replace(/,/g,''));
		$('thaber').value=Math.round($('thaber').value*100)/100;
		$('thaber').value=NumberFormat($('thaber').value, '2', '.', ',');
	}
	
	var newcell4 = newrow.insertCell(4);
	newcell4.width="108";
	newcell4.className ="texto8";
	var cuenta2 = ctacontab;
	var nomcta2 = nomctacontab;
	var debe2 = debe;
	var haber2 = haber;
	
	newcell4.innerHTML = '<img src="../../imagenes/eliminar.jpg" onClick="borrar(this,\''+debe2+'\',\''+haber2+'\')"><input type="hidden" name="nomctad" id="nomctad" readonly  style="width:100px" onclick="editar(this)" value="'+cuentacontab+'" >' ;
	newcell4.align ="center";
	limpia();
	actualiza_totales();
}

function borrar(obj,debe2,haber2) {
  while (obj.tagName!='TR') 
    obj = obj.parentNode;
  tab = document.getElementById('datos');
  for (i=0; ele=tab.getElementsByTagName('tr')[i]; i++)
    if (ele==obj) num=i;
/////*****/////
//		actualiza_totales();
	$('tdebe').value = parseFloat($('tdebe').value.replace(/,/g,'')) - parseFloat(debe2.replace(/,/g,''));
	$('tdebe').value = addCommas($('tdebe').value);
	$('thaber').value = parseFloat($('thaber').value.replace(/,/g,'')) - parseFloat(haber2.replace(/,/g,''));
	$('thaber').value = addCommas($('thaber').value);
	//$('thaber').value=$('thaber').value-debe2;
	

////*****/////

		tab.deleteRow(num);
  		limpia();
		//actualiza_totales();
}


function editar(obj)
{
	document.getElementById("act_ren").style.visibility = "visible";
	document.getElementById("agrega_ren").style.visibility = "hidden";
	var debe = document.getElementById('debe').value
	var haber = document.getElementById('haber').value
	
	var tabla = $('datos');
	var par = obj . parentNode ;
	while( par . nodeName . toLowerCase ()!= 'tr' )
	{
		par = par . parentNode ;
	}

	if(debe.length<1)
		{
			document.getElementById('debe').value = 0;
		}
	if(haber.length<1)
		{
			document.getElementById('haber').value = 0;
		}

	document.getElementById('ren').value=par . rowIndex;
	for (var j = 0; j <tabla.rows[par . rowIndex].cells.length;j++)
	{
		var cell = tabla.rows[par . rowIndex].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
			if(mynode.name=="nomctad")
			{
				document.getElementById('cuentacontab').value = mynode.value;
			}
			if(mynode.name=="debe1")
			{
				document.getElementById('debe').value = addCommas(mynode.value);
			}
			if(mynode.name=="haber1")
			{
				document.getElementById('haber').value = addCommas(mynode.value);
			}
			if(mynode.name=="cuenta1")
			{
				document.getElementById('ctacontab').value = mynode.value;
			}
		}
	}
}

function act_contab()
{
	document.getElementById("act_ren").style.visibility = "hidden";
	document.getElementById("agrega_ren").style.visibility = "visible";
	
	var tdebe=parseFloat($('tdebe').value.replace(/,/g,''))+parseFloat($('debe').value.replace(/,/g,''));
	var thaber=parseFloat($('tdebe').value.replace(/,/g,''))+parseFloat($('haber').value.replace(/,/g,''));

	var tot=parseFloat($('total').value.replace(/,/g,''));
	//*******************************************
/*/	if (totd>tot)
	{
		alert("Error, Importe total de cuentas es mayor que el Total General, modifique el monto a ingresar");
		return false;
	}
	if (totd<tot)
	{
		alert("Error, Importe total de cuentas es menor que el Total General, modifique el monto a ingresar");
		return false;
	}*/
	//*******************************************
	var folio=$('folio').value;
	var cuentacontab=$('cuentacontab').value;
	var ctacontab=$('ctacontab').value;
	var nomctacontab=$('nomctacontab').value;
	var debe=$('debe').value;
	var haber=$('haber').value;
	if(folio.length<1)
	{
		alert("Falta capturar Folio");
		return false;
	}
	if(cuentacontab.length<1)
	{
		alert("Falta capturar Cuenta");
		return false;
	}
	if(debe.length<1)
	{
		debe = 0;
	}
	if(haber.length<1)
	{
		haber = 0;
	}

	var tabla = $('datos');
	var ren=document.getElementById('ren').value;
	for (var j = 0; j <tabla.rows[ren].cells.length;j++)
	{
		var cell = tabla.rows[ren].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
			if(mynode.name=="cuenta1")
			{
				 mynode.value = document.getElementById('ctacontab').value;
			}

			if(mynode.name=="debe1")
			{
				mynode.value = document.getElementById('debe').value;
			}
			if(mynode.name=="haber1")
			{
				mynode.value = document.getElementById('haber').value;
			}

			if(mynode.name=="nomctad")
			{
				mynode.value = document.getElementById('cuentacontab').value;
			}
		}
	}
	limpia();
	actualiza_totales();
}

function limpia()
{
	document.getElementById('cuentacontab').value = ''; 
	document.getElementById('ctacontab').value = ''; 
	document.getElementById('nomctacontab').value = ''; 
	document.getElementById('debe').value = 0; 
	document.getElementById('haber').value = 0; 
	document.getElementById('cuentacontab').focus();
}

function actualiza_totales()
{
	$('totald').value = 0;
	$('tdebe').value = 0;
	$('thaber').value = 0;
	var tabla= $('datos');
	var numren=tabla.rows.length;
	for (var i = 0; i < tabla.rows.length; i++) 
	{ 
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
				if(mynode.name=="debe1")
				{
					debe2=mynode.value;
					$('tdebe').value = parseFloat($('tdebe').value.replace(/,/g,'')) + parseFloat( debe2.replace(/,/g,''));
					$('tdebe').value=Math.round($('tdebe').value*100)/100;
					$('tdebe').value=NumberFormat($('tdebe').value, '2', '.', ',');
					//if(parseFloat($('tdebe').value.replace(/,/g,'')) > parseFloat($('total').value.replace(/,/g,'')))
					//{
						//alert("Error, Importe total de cuentas es mayor que el Total General");
						//return false;
					//}
				}
				if(mynode.name=="haber1")
				{
					haber2=mynode.value;
					$('thaber').value = parseFloat($('thaber').value.replace(/,/g,'')) + parseFloat( haber2.replace(/,/g,''));
					$('thaber').value=Math.round($('thaber').value*100)/100;
					$('thaber').value=NumberFormat($('thaber').value, '2', '.', ',');

					//if(parseFloat($('thaber').value.replace(/,/g,'')) > parseFloat($('total').value.replace(/,/g,'')))
					//{
					//	alert("Error, Importe total de cuentas es mayor que el Total General");
						//return false;
					//}
				}
				
			}
		}
	}
	
}


function valida_total()
{
	/*if(parseFloat($('totald').value.replace(/,/g,'')) > parseFloat($('total').value.replace(/,/g,'')))
	{
		alert("Error, Importe total de cuentas es mayor que el Total General");
		return false;
	}*/
} 


function guarda_datos()
{
// Tabla egresosmsolchegas
	//var pregunta=true;

	if(parseFloat($('tdebe').value.replace(/,/g,'')) != parseFloat($('thaber').value.replace(/,/g,'')))
	{
		alert("Error, El Debe es diferente al Haber");
		return false
	}
	
	if(parseFloat($('tdebe').value.replace(/,/g,'')) == 0)
	{
		alert("Error, El Debe no tiene valor");
		return false
	}
	if(parseFloat($('thaber').value.replace(/,/g,'')) == 0)
	{
		alert("Error, El Haber no tiene valor");
		return false
	}

	/*if(parseFloat($('tdebe').value.replace(/,/g,'')) != parseFloat($('thaber').value.replace(/,/g,'')))
	{
		alert("Error, El Debe es diferente que el Haber");
		return false
	}*/

		var folio = document.getElementById('folio').value;
		var usuario = document.getElementById('usuario').value;
		var tabla= $('datos');
		var TotaProd= new Array();
		var countPartidas=0;
		for (var i = 0; i < tabla.rows.length; i++) 
		{  //Iterate through all but the first row
			
			for (var j = 0; j <tabla.rows[i].cells.length;j++)
			{
				var cell = tabla.rows[i].cells[j];
				for (var k = 0; k < cell.childNodes.length; k++) 
				{
					var mynode = cell.childNodes[k];
													
					if(mynode.name=="cuenta1")
					{
						cuenta0=mynode.value;
					}
					if(mynode.name=="debe1")
					{
						debe0=mynode.value;
					}
					if(mynode.name=="haber1")
					{
						haber0=mynode.value;
					}
					
				}
			}
			TotaProd[countPartidas] = new Object;
			TotaProd[countPartidas]['cuenta0']	= cuenta0;			
			TotaProd[countPartidas]['debe0']	= debe0;
			TotaProd[countPartidas]['haber0']	= haber0;
	
			countPartidas++;
		}
		var datas = {
					folio: folio,
					usuario: usuario,
					cuenta0: cuenta0,
					debe0: debe0,
					haber0: haber0,
					prods: TotaProd
		};
	
		//alert(datas);
		jQuery.ajax({
					type:           'post',
					cache:          false,
					url:            'php_ajax/actualiza_contab.php',
					data:          datas,
					success: function(resp) 
					{
						if( resp.length ) 
						{
							//alert(resp);
							var myArray = eval(resp);
							
							if(myArray.length>0)
							{
								location.href='solche_contab.php';
							}
						}
					}
				});
}
//////////////////////////////////////////////////////////////////////////////////
