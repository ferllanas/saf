<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
// Date in the past 
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 

// always modified 
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 

// HTTP/1.1 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 

// HTTP/1.0 
header("Pragma: no-cache");  

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$cuenta='';
$ccosto='';
$folio='';
$nomcta='';
$nomcto='';
$total=0;
$totald=0;
$falta="";
$factura="";
$proveedor="";
$opago=0;

$usuario=$_COOKIE['ID_my_site'];
//$usuario='001349';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-UTF-8">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="javascript/busquedaincPres.js"></script>
<script language="javascript" src="javascript/solche_fun.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>

</head>

<body>
 <p class="TituloDForma">Codificaci&oacute;n Presupuestal<span class="texto9"><span class="texto10"><strong>
   <input type="hidden" name="cta" id="cta" value="<?php echo $cta;?>">
   <input type="hidden" name="nomcta" id="nomcta" value="<?php echo $nomcta;?>">
   <input type="hidden" name="ccos" id="ccos" value="<?php echo $ccos;?>">
   <input type="hidden" name="nomcto" id="nomcto" value="<?php echo $nomcto;?>">
   <input type="hidden" name="ren" id="ren">
   <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
   <strong>
   <input type="hidden" name="opago" id="opago" value="<?php echo $opago;?>">
   <strong><strong>
   <input type="hidden" name="rc" id="rc" value=0>
</strong></strong></strong></strong></span></span></p>
 <table width="1203" height="36" border="0">
   <tr>
     <td width="122"><span class="texto10">Solicitud de Cheque </span></td>
     <td width="154"><div align="left" style="z-index:6; position:relative; width:140px; height: 24px;">
	  <input class="texto8" type="text" id="folio" name="folio" style="width:50%; height:18px;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="1">        
		        <div id="search_suggestProv" style="z-index:5;" > </div>
   		  </div></td>
     <td width="913" id="datos_prov" style="visibility:hidden " ><span class="texto10"><strong>Fecha</strong></span>
       <input type="text" name="falta" id="falta" size="10" style="background-color:#EEFDB3 " readonly value="<?php echo $falta;?>">
       <span class="texto10"><strong>Factura</strong></span>
       <input type="text" name="factura" id="factura" style="background-color:#EEFDB3 " readonly value="<?php echo $factura;?>">
       <span class="texto10"><strong>Proveedor</strong></span><span class="texto10">
       <input type="text" name="proveedor" id="proveedor" style="background-color:#EEFDB3 " readonly size="80" value="<?php echo $proveedor;?>">
       </span></td>
   </tr>
 </table>
 <hr class="hrTitForma" id="linea1" style="visibility:hidden ">
 <table width="1206" border="0">
   <tr>
     <td height="29">&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td><input name="radio" id="radio1" type="radio" onClick="valida_radio()" value="1">
     Retencion
       <input name="radio" id="radio2" type="radio" onClick="valida_radio()" value="2">
     Cargo</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td width="50" height="29"><span class="texto10">Cuenta</span></td>
        <td width="336">
		<div align="left" style="z-index:4; position:relative; width:324px; height: 24px;">
				<input name="cuenta" type="text" class="texto8" id="cuenta" tabindex="2" disabled onkeyup="searchdepto2(this);" style="width:300px; height:18px; " autocomplete="off">        
				<div class="texto8" id="search_suggestdepto2" style="z-index:3;" > </div>
		  </div>
		</td>
        <td width="58"><span class="texto10">C. Costo</span></td>
        <td width="360">
		<span class="texto8"><div align="left"  style="z-index:2; position:relative; width:345px; height: 24px;">
         <input class="texto8" type="text" name="ccosto" id="ccosto" disabled tabindex="3" onKeyUp="searchdeptox(this);" style="width:330px; height:18px;" autocomplete="off">
         <div id="search_suggestdepto" style="z-index:1;"></div>
        </div></span>
		</td>
        <td width="39"><span class="texto10">Monto</span></td>
     <td align="center" width="128"><span class="texto8">
       <input type="text" name="monto" id="monto" tabindex="4" disabled onKeyPress="EvaluateText('%f', this);" onChange="this.value = NumberFormat(this.value, '2', '.', ',')" onKeyUp="asignaFormatoSiesNumerico(this, event)" style="width:100px; height:18px; text-align:right ">
     </span></td>
     <td width="82"><div align="center"><span class="texto10"><strong><span class="texto9"><img src="../../imagenes/agregar2.jpg" id="agrega_ren" width="30" height="25" title="Agregar Cuenta." onClick="agregar()"><img src="../../imagenes/actualiza3.png" id="act_ren" width="30" height="25" style="visibility:hidden; " title="Actualiza Datos de Cuenta." onClick="act()"></span></strong></span></div></td>
     <td width="8"><div align="center"><span class="texto10"></span></div></td>
     <td width="107"><span class="texto9"><span class="texto10"><strong><strong><strong>
       <input class="texto10" type="checkbox" name="afecta" id="afecta" disabled onClick="afectar()">
       <strong><strong><strong>Sin Afectar</strong></strong></strong></strong></strong></strong></span></span></td>
   </tr>
</table>

 <table name="enc" id="enc" width="100%" height="10%" align="left" border="1">
   <tr>
     <th width="109" align="center" class="subtituloverde">Cuenta</th>
     <th width="98" align="center" class="subtituloverde">Centro de Costos</th>
     <th width="458" align="center" class="subtituloverde">Nombre de la Cuenta</th>
     <th width="92" align="center" class="subtituloverde">Monto</th>
     <th width="67" align="center" class="subtituloverde">Acci&oacute;n</th>
   </tr>
 </table>
<div style="overflow:auto; width: 100%; height :300px; align:center;"> 
	<table name="master" id="master" width="100%" height="10%" border=0 align="center" style="overflow:auto; " >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
</div>
    <table width="997" border="0" align="left">
        <tr>
          <td width="582" class="texto10">&nbsp;</td>
          <td width="186">&nbsp;</td>
          <td width="56"><span class="texto10">Suma</span></td>
          <td width="155"><input type="text" name="totald" id="totald" readonly value="<?php echo $totald;?>"></td>
        </tr>
        <tr>
          <td>
            <input type="button" name="Grabar" value="Grabar" id="Grabar" disabled onClick="guarda_datos()">
          </td>
          <td>&nbsp;</td>
          <td><span class="texto10">Cantidad</span></td>
          <td><input type="text" name="total" id="total" readonly value="<?php echo $total;?>"></td>
        </tr>
</table>
</div>
</body>
</html>
