<?php
// Archivo de consultas de Solicitud de Cheques
include_once '../../cheques/lib/ez_sql_core.php'; 
//include_once '../../cheques/lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
	//echo $opcion;
	
	if($opcion==2)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
				

				$command= "select a.factura,  a.prov, a.tipo, CONVERT(varchar(12),a.falta, 103) as falta,a.nomprov , a.concepto,a.total, a.vale from v_egresos_ctas_x_pagar a ";
				$command .=" WHERE nomprov LIKE '%" . $nom1 . "%' ";
				$command .=" order by vale";
	}
	
	
	//	echo $opcion;
	if($opcion==4)
 	{
				$command= "select a.factura,  a.prov, a.tipo, CONVERT(varchar(12),a.falta, 103) as falta,a.nomprov , a.concepto,a.total, a.vale from v_egresos_ctas_x_pagar a WHERE concepto LIKE '%" . substr($_REQUEST['query'],1) . "%' order by vale";

     }
	//echo "Antes de pasar al 5";
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select a.factura, a.prov, a.tipo, CONVERT(varchar(12),a.falta, 103) as falta,a.nomprov , a.concepto,a.total, a.vale from v_egresos_ctas_x_pagar a where a.falta >= '" . $fini . "' and a.falta <='" . $ffin . "'";
				 
	}	
	
	if($opcion==8)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select a.factura, a.prov, a.tipo, CONVERT(varchar(12),a.fpago, 103) as falta,a.nomprov , a.concepto,a.total, a.vale from v_egresos_ctas_x_pagar a where a.fpago >= '" . $fini . "' and a.fpago <='" . $ffin . "'";
				 
	}	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$datos[$i]['tipo']=trim($row['tipo']);
					
					$datos[$i]['folio']=trim($row['vale']);
					$datos[$i]['falta']=trim($row['falta']);					
					$datos[$i]['nomprov']=utf8_decode(trim($row['nomprov']));
					$datos[$i]['prov']=trim($row['prov']);
					$datos[$i]['importe']=number_format(trim($row['total']),2,".",",");
					$datos[$i]['concepto']=utf8_decode(trim($row['concepto']));
					$datos[$i]['factura']=utf8_decode(trim($row['factura']));
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>