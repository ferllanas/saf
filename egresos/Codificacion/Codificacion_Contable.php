<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$cuentacontab='';
$folio='';
$nomctacontab='';
$total=0;
$totald=0;
$tdebe=0;
$thaber=0;

$falta="";
$factura="";
$proveedor="";
$usuario=$_COOKIE['ID_my_site'];
//$usuario='002294';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="codificacion_contable.js"></script>
<script type="text/javascript" src="../../javascript_globalfunc/jquery.tmpl.js"></script>
<script language="javascript">
function getfirst(str)
{
	return str.split(".")[0];
}
</script>
<script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
	<tr>
			<td align="center">${ctapresup}</td>
			<td align="right">$ ${monto}</td>
			<td align="right"><span name='inicuenta' value=${id}>51${getfirst(ctapresup)}.</span><input type="text" class="restCuentaContable" id=${"cuentaini_"+id} value=""  style="height:12px; font-size:10px;width:50px;"></td>
			<td align="left"><input  type="text" class="nomCuentaContable" id=${"nomCuentaContable_"+id} value="" style="height:12px;font-size:10px;width:390px; border-left:0px; border-top:0px;border-right:0px; border-bottom:0px;" readonly="readonly"><input type="hidden"  id=${"cuenta_"+id} value="" ></td>
	</tr>
</script>   
</head>

<body>
 <p class="TituloDForma">Codificaci&oacute;n Contable<span class="texto10"><strong>
   <input type="hidden" name="ctacontab" id="ctacontab" value="<?php echo $ctacontab;?>">
   <input type="hidden" name="nomctacontab" id="nomctacontab" value="<?php echo $nomctacontab;?>">
   <input type="hidden" name="ren" id="ren">
   <input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario;?>">
 </strong></span></p>
 <table width="1234" height="36" border="0">
   <tr>
     <td width="122"><span class="texto10">Solicitud de Cheque </span></td>
     <td width="146"><div align="left" style="z-index:8; position:relative; width:140px; height: 24px;">
       <input class="texto8" type="text" id="folio" name="folio" style="height:12px; font-size:10px;width:50px;" onKeyUp="busquedaIncCheque();" autocomplete="off" tabindex="1">
       <div id="search_suggestcontabx" style="z-index:7;" > </div>
     </div></td>
     <td width="952" id="datos_prov" style="visibility:hidden " ><span class="texto10"><strong>Fecha</strong></span>
         <input type="text" name="falta" id="falta" size="10" style="background-color:#EEFDB3 " readonly value="<?php echo $falta;?>">
         <span class="texto10"><strong>Factura</strong></span>
         <input type="text" name="factura" id="factura" style="background-color:#EEFDB3 " readonly value="<?php echo $factura;?>">
         <span class="texto10"><strong>Proveedor</strong></span><span class="texto10">
         <input type="text" name="proveedor" id="proveedor" style="background-color:#EEFDB3 " readonly size="80" value="<?php echo $proveedor;?>">
       </span></td>
   </tr>
 </table>
	<table  width="800px" height="10%" border=0 align="center"  >
	 <thead>
     	<tr>
         <th width="200px" align="center" class="subtituloverde">Cuenta Presupuestal</th>
         <th width="100px" align="center" class="subtituloverde">Monto</th>
         <th width="100px" align="center" class="subtituloverde">Cuenta Contable</th>
         <th width="400px" align="center" class="subtituloverde">Nombre</th>
       </tr>
	</thead>
	  <tbody id="datos" name="datos">
	  </tbody>
	</table>
	
	<div align="center" style="position:absolute; width:800px; top:430px; height: 227px; left: 360px;">
    <table width="800px" border="0" align="center" >
    	<thead>
        	<tr>
            	<th colspan="4" align="left">Cuentas por Pagar:</th>
            </tr>
        </thead>
      <tr>
        <td class="texto10" align="right">Cuenta Contable:</td>
        <td><input type="text" id="cuentaContable" value="" style="height:12px; font-size:10px;width:50px;"></td>
        <td><u><span id="nomcuentacontable" style="height:12px; font-size:12px;width:50px;"></span></u></td>
        <td><span class="texto10" name="total" id="total"></span></td>
      </tr>
      </tr>
      	<td colspan="4">&nbsp;</td>
      <tr>
      <tr>
        <td width="153" class="texto10" align="right">Tot. Debe:</td>
        <td width="105" class="texto10"><span id="tdebe" name="tdebe"></span></td>
        <td width="302" class="texto10" align="right">Tot. Haber:</td>
        <td width="222" class="texto10"><span id="thaber" name="thaber"></span></td>
      </tr>
      <!--
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><span class="texto10">
          <input type="hidden" name="totald" id="totald" readonly value="<?php echo $totald;?>">
        </span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <input type="button" name="Grabar" value="Grabar" id="Grabar" readonly onClick="guarda_datos()">
       </td>
        <td class="texto10"><form name="form1" method="post" action="">
          <input type="button" name="cancela" id="cancela" value="Cancelar" onClick="limpia_pantalla()">
        </form></td>
        <td colspan="2"><div align="right"><span class="texto10">
        </span><span class="texto10">Monto de la Solicitud del Cheque </span></div></td>
       
      </tr>-->
    </table>
  </div>
</div>
</body>
</html>
