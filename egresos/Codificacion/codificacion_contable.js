// JavaScript Document
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////      solchecontab     //////////////////////////////////////////////

$( document ).ready(function() {
// Handler for .ready() called.
	$('#cuentaContable').keyup(function() {
		$('#divchido').remove();
		//var cuenta=$(this).parent().find('.inicuenta').html;						
		var cuentaContable=$(this).val();
		
		var div_obj = $(document.createElement("div"));
		$(div_obj).attr('id','divchido');	
		$(div_obj).css('z-index',20);													
		$(div_obj).css('position','absolute');												  	
		$(div_obj).css('left' , $(this).position().left);		
		
		$(div_obj).css('top' , (parseFloat($(this).position().top)+20));
				
		var datas={ctacontable:cuentaContable};
		$.ajax({ type: 'post',
					   data: datas,
					   url: 'php_ajax/buscquedaincctacontable.php',
					   dataType: 'json',
					   success: function(resp)
						{
							console.log(resp);
							var suggest='';
							for(var i=0; i<resp.length;i++)
							{
								suggest += '<div onmouseover="javascript:suggestOvercontabx(this);" ';
								suggest += 'onmouseout="javascript:suggestOutcontabx(this);" ';
								
								var nombrefun="javascript:seleccionCtaContable(this,'"+resp[i]['cuenta']+"','"+resp[i]['nombre']+"')";//,'"+cuenta+"'
								suggest += 'onclick="'+nombrefun+'"';
								suggest += 'class="suggest_link" title="'+resp[i]['cuenta']+'�'+resp[i]['nombre']+'">' + 
												resp[i]['cuenta'] + ' - '+ resp[i]['nombre']+'</div>';
							}
							$(div_obj).html(suggest);
							$('body').append($(div_obj));
						}
					
				});  // Json es una muy buena opcion    
		
		$(div_obj).html(suggest);
	});
});

function seleccionCtaContable(divClick,cuenta,nombre)
{
	$('#cuentaContable').val(cuenta);
	$('#nomcuentacontable').html(nombre);
	$(divClick).parent().remove();
}

function getXmlHttpRequestObjectcontabx() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 
var searchReqcontabx = getXmlHttpRequestObjectcontabx(); 

function busquedaIncCheque() {
    if (searchReqcontabx.readyState == 4 || searchReqcontabx.readyState == 0)
	{
        var str = escape(document.getElementById('folio').value);
        searchReqcontabx.open("GET",'php_ajax/queryfolioincrem2.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqcontabx.onreadystatechange = handleSearchSuggestcontabx;
        searchReqcontabx.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestcontabx() {
	
    if (searchReqcontabx.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestcontabx')
        ss.innerHTML = '';
		//alert(searchReqcontab.responseText);
        var str = searchReqcontabx.responseText.split("\n");
		console.log(searchReqcontabx.responseText);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOvercontabx(this);" ';
				suggest += 'onmouseout="javascript:suggestOutcontabx(this);" ';
				suggest += "onclick='javascript:setbusquedaIncCheque(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[0]+'�'+tok[1]+'�'+tok[2]+'�'+tok[3]+'�'+tok[4]+'�'+tok[5]+'">' + tok[0] + ' - '+ tok[1]+'</div>';
				ss.innerHTML += suggest;
			}//
			if(str.length<=1)
			{
				document.getElementById('folio').value ='';
				return false;
			}
			else
			{
				//document.getElementById('cuentacontab').disabled = false;
				//document.getElementById('debe').disabled = false;
				//document.getElementById('haber').disabled = false;
				document.getElementById('Grabar').disabled = false;
			}
			
    }
}
//Mouse over function
function suggestOvercontabx(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutcontabx(div_value) {
    div_value.className = 'suggest_link';
}

function setbusquedaIncCheque(value,idprod) 
{
	console.log(value);
	console.log(idprod);
	var tok =idprod.split("�");
	document.getElementById('folio').value = tok[0];
	busca_cheque(tok[0])
	document.getElementById('falta').value = tok[3];
	document.getElementById('factura').value = tok[4];
	document.getElementById('proveedor').value = tok[5];
	document.getElementById('datos_prov').style.visibility = "visible";
	document.getElementById('folio').disabled = true;
	document.getElementById('search_suggestcontabx').innerHTML = '';
}



function busca_cheque($folio)
{

	var data = 'folio=' + $folio;     // Toma el valor de los datos, que viene del input						
	//alert(data);
	
	$.post('php_ajax/getDatelleSolCheque.php',data, function(resp)
	{ //Llamamos el arch ajax para que nos pase los datos
		console.log(resp);
		var $total=0.00;
		var entry=new Array;
		for(var i=0;i<resp.length;i++){
			console.log(resp[i]['monto']);
			$total += parseFloat( resp[i]['monto'].replace(",","") );
		}
		
		$("#total").html("$"+addCommas($total.toFixed(2)));
		$("#thaber").html("$"+ addCommas($total.toFixed(2)));
		$("#tdebe").html("$"+addCommas($total.toFixed(2)));
		$('#datos').empty();
		$('#tmpl_proveedor').tmpl(resp).appendTo('#datos'); 
			
		$('.restCuentaContable').keyup(function() {
				$('#divchido').remove();
				var $objecto=$(this).parent().find('span[name="inicuenta"]').attr("value");//
				var cuenta=$(this).parent().find('span[name="inicuenta"]').html();	
				
				console.log("cuenta:"+cuenta);
				var cuentafinal= cuenta+""+$(this).val();
				console.log("cuentafinal:"+cuentafinal);
				
				var div_obj = $(document.createElement("div"));//.attr({'class': 'test'})
				$(div_obj).attr('id','divchido');	
				$(div_obj).css('z-index',20);													
				$(div_obj).css('position','absolute');												  	
				$(div_obj).css('left' , $(this).position().left);		
				
				$(div_obj).css('top' , (parseFloat($(this).position().top)+20));	
				
				console.log(cuentafinal);
				var datas={ctacontable:cuentafinal};
				
				$.ajax({ type: 'post',
					   data: datas,
					   url: 'php_ajax/buscquedaincctacontable.php',
					   dataType: 'json',
					   success: function(resp)
						{
							console.log(resp);
							var suggest='';
							for(var i=0; i<resp.length;i++)
							{
								
								suggest += '<div onmouseover="javascript:suggestOvercontabx(this);" ';
								suggest += 'onmouseout="javascript:suggestOutcontabx(this);" ';
								
								var nombrefun="javascript:seleccionMCuenta(this,"+ $objecto +",'"+resp[i]['cuenta']+"','"+resp[i]['nombre']+"','"+cuenta+"')";//
								suggest += 'onclick="'+nombrefun+'"';
								suggest += 'class="suggest_link" title="'+resp[i]['cuenta']+'�'+resp[i]['nombre']+'">' + 
												resp[i]['cuenta'] + ' - '+ resp[i]['nombre']+'</div>';
							}
							$(div_obj).html(suggest);
							$('body').append($(div_obj));
						}
					
				});  // Json es una muy buena opcion    
				
			});
	}, 'json');  // Json es una muy buena opcion               
}

function seleccionMCuenta(objecto, obkect, cuenta, nombre,cuentaini)//
{
	$('#cuentaini_'+obkect).val(cuenta.substr(cuentaini.length));
	$('#cuenta_'+obkect).val(cuenta);
	$('#nomCuentaContable_'+obkect).val(nombre);
	$(objecto).parent().remove();
}