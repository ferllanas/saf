<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_REQUEST['ctacontable'] && $conexion)
{

	$ctacontable = $_REQUEST['ctacontable'];
	$command= "SELECT nombre, cuenta FROM contabmcuentas WHERE cuenta like '%".$_REQUEST['ctacontable']."%' AND estatus <9000";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()); 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['nombre']= trim($row['nombre']);
			$datos[$i]['cuenta']= trim($row['cuenta']);
			$i++;
		}
	}
	
}
echo json_encode($datos);
?>