<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	$command= "SELECT a.opago,a.folio,b.concepto,b.importe,a.estatus,b.prov,b.factura,CONVERT(varchar(10),b.falta, 103) as falta, c.nomprov from egresosmopago a left join egresosmsolche b on a.folio=b.folio left join compramprovs c on b.prov=c.prov WHERE ";	
	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if(!$paso)
		{
			$command.=" b.folio LIKE '%".$terminos[$i]."%'";
			$paso=true;	
		}
		else
			$command.=" b.concepto LIKE '%".$terminos[$i]."%'";	
	}
	
	$command.="  ORDER BY a.folio ASC";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$sct.= htmlentities(trim($row['folio']))."@".htmlspecialchars (trim($row['concepto']))."@".trim($row['importe'])."@".trim($row['falta'])."@".trim($row['factura'])."@".trim($row['nomprov'])."\n";
			
		}
	}
	echo $sct;
}
?>