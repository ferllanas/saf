<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_REQUEST['folio'] && $conexion)
{

	$folio = $_REQUEST['folio'];
	$command= "SELECT TOP(50) c.ctapresup, c.monto, c.id FROM egresosmopago a 
	LEFT JOIN egresosmsolche b ON a.folio=b.folio
	LEFT JOIN egresosdopago_presup c ON a.opago=c.opago
 WHERE a.folio=$folio  ORDER BY c.ctapresup ASC";
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()); 
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['monto']= number_format(trim($row['monto']),2);
			$datos[$i]['ctapresup']= trim($row['ctapresup']);
			$datos[$i]['id']= trim($row['id']);
			$i++;
		}
	}
	
}
echo json_encode($datos);
?>