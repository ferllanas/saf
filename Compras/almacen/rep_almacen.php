<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	require_once("../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//$usuario = $_COOKIE['ID_my_site'];
//$usuario='001349';
$sw = 0;
$datos2 = array();
$prod = 0;
$nomprod = "";
$unidad = "";
$almacen=0;
$cantidad=0;
$minimo=0;
$excel="";
if(isset($_REQUEST['prodname1']))
	$nomprod2 = $_REQUEST['prodname1'];

if(isset($_REQUEST['sw']))
	$sw = $_REQUEST['sw'];
	
if(isset($_REQUEST['prod']))
	$prod = $_REQUEST['prod'];

if(isset($_REQUEST['nomprod']))
	$nomprod = $_REQUEST['nomprod'];
	
if(isset($_REQUEST['unidad']))
	$unidad = $_REQUEST['unidad'];

if(isset($_REQUEST['cantidad']))
	$cantidad = $_REQUEST['cantidad'];

if(isset($_REQUEST['minimo']))
	$minimo = $_REQUEST['minimo'];

if(isset($_REQUEST['almacen']))
	$almacen = $_REQUEST['almacen'];
	
if ($conexion)
{
	if ($sw==1)
		{
			$consulta = "select a.*,b.nomprod,b.unalmacen from almacendstock a left join compradproductos b on a.producto=b.prod where a.estatus < 9000 and b.estatus< 9000 and b.almacen='SI' order by b.nomprod";
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos2[$i]['producto']= trim($row['producto']);
				$datos2[$i]['nomprod']= trim($row['nomprod']);
				$datos2[$i]['unidad']= trim($row['unalmacen']);
				$datos2[$i]['cantidad']= trim($row['cantidad']);
				$datos2[$i]['minimo']= trim($row['minimo']);
				$datos2[$i]['almacen']= trim($row['almacen']);
				
				if($i==0)
				{
					$datos2[$i]['producto']= "Producto";
					$datos2[$i]['nomprod']= "Nombre del Producto";
					$datos2[$i]['unidad']= "Unidad";
					$datos2[$i]['cantidad']= "Cantidad";
					$datos2[$i]['minimo']= "Cant. Minima";
				}	
				$excel.=$datos2[$i]['producto']."\t".str_replace ('"','PULG.',$datos2[$i]['nomprod'])."\t".$datos2[$i]['unidad']."\t";
				$excel.=$datos2[$i]['cantidad']."\t".$datos2[$i]['minimo']."\n";
				//$excel.=$datos2[$i]['almacen']."\n"; 				
				$i++;
			}
		}
	if ($sw==2)	
		{		
			$consulta = "select * from compradproductos where prod not in (select producto from almacendstock) and estatus < 9000 and almacen='SI' order by nomprod";
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos2[$i]['producto']= trim($row['prod']);
				$datos2[$i]['nomprod']= trim($row['nomprod']);
				$datos2[$i]['unidad']= trim($row['unalmacen']);
				$datos2[$i]['cantidad']= 0;
				$datos2[$i]['minimo']= 0;
				$datos2[$i]['almacen']= 0;

				if($i==0)
				{
					$datos2[$i]['producto']= "Producto";
					$datos2[$i]['nomprod']= "Nombre del Producto";
					$datos2[$i]['unidad']= "Unidad";
					$datos2[$i]['cantidad']= "Cantidad";
					$datos2[$i]['minimo']= "Cant. Minima";
				}					
				$excel.=$datos2[$i]['producto']."\t".$datos2[$i]['nomprod']."\t".$datos2[$i]['unidad']."\t";
				$excel.=$datos2[$i]['cantidad']."\t".$datos2[$i]['minimo']."\n";
				//$excel.=$datos2[$i]['almacen']."\n"; 				
				
				
				
				$i++;
			}
		}
}
//echo $excel;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href='../../css/estilos.css' rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
</head>

<body>
<p class="seccionNota"><strong>Reporte de Productos</strong></p>
<hr class="hrTitForma">
<table width="359" border="0" align="center">
  <tr>
    <td width="147">
      <p>
        <label>
        <span class="texto12"><strong>
    <input type="radio" name="productos" id="op1"  value=1 onClick="javascript: ;location.href='rep_almacen.php?&sw=1'"<?php if($sw=="1") echo "checked"; ?>>
    En Stock</strong></span></label><span class="texto12"><strong>
    <label></label>
    </strong></span><br>
      </p>
    </td>
    <td width="196"><span class="texto12"><strong>
      <label>
      <input type="radio" name="productos" id="op2" value=2 onClick="javascript: ;location.href='rep_almacen.php?&sw=2'"<?php if($sw=="2") echo "checked"; ?>>
En Catalogo</label>
    </strong></span></td>
  </tr>
</table>

<table width="584" height="100%" border=0 align="center" style="vertical-align:top">
  <td width="90%" height="404" style="vertical-align:top;">
      <table width="100%" border=1 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <th width="49" align="center" class="subtituloverde">Prod</th>
          <th width="278" align="center" class="subtituloverde" >Descripci&oacute;n</th>
          <th width="49" align="center" class="subtituloverde" ><div align="left">Unidad</div></th>
          <th width="64" align="center" class="subtituloverde" >Existencia</th>
          <th width="70" align="center" class="subtituloverde" >Cantidad Min.</th>
        </tr>
      </table>
      <div style="overflow:auto; height:400px; padding:0;">
        <table name="Tabla2" id="Tabla2" width="100%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white>
          <?php 
		
			for($i=0;$i<count($datos2);$i++)
			{
		?>
          <tr class="fila">
            <td width="11%" align="center"><?php echo $datos2[$i]['producto'];?></td>
            <td width="53%" align="left"><?php echo $datos2[$i]['nomprod'];?></td>
            <td width="10%" align="center"><?php echo $datos2[$i]['unidad'];?>
              <div align="center"></div></td>
            <td width="12%" align="center"><?php echo $datos2[$i]['cantidad'];?></td>
            <td width="14%" align="center"><?php echo $datos2[$i]['minimo'];?></td>
          </tr>
          <?php 
		}
		?>
        </table>
        <p>&nbsp;</p>
    </div></td>
</table>

<table width="337" border="0" align="center">
  <tr>
    <td width="103"><div align="center"></div></td>
    <td width="204">
	<form action="ficheroExcel.php" method = "POST">
	<input type="hidden" name="export" id="export" value="<?php echo $excel;?>"/>
	<input type = "submit" value =" Importar a Excell">
	</form>
	</td>
    <td width="16"><div align="center"></div></td>
  </tr>
  <tr class="texto9">
    <td height="17"><div align="center"></div></td>
    <td>&nbsp;</td>
    <td><div align="center"></div></td>
  </tr>
</table>
</body>
</html>
