//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv1() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv1 = getXmlHttpRequestObjectProv1(); 

function searchProducto1() 
{
	
	$('nomprod').value = "";
	limpia_tabla();
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqProv1.readyState == 4 || searchReqProv1.readyState == 0)
	{
        var str = escape(document.getElementById('prodname1').value);
        searchReqProv1.open("GET",'php_ajax/queryprodincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv1.onreadystatechange = handleSearchSuggestProv1;
        searchReqProv1.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv1() {
	
	//alert("aja 1");
    if (searchReqProv1.readyState == 4) 
	{
        var ss = document.getElementById('search_suggest')
        ss.innerHTML = '';
		//alert(searchReqProv1.responseText);
        var str = searchReqProv1.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split(";");
				var suggest = '<div onmouseover="javascript:suggestOverProv1(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProv1(this);" ';
				suggest += "onclick='javascript:setSearchProv1(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverProv1(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv1(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv1(value,nomprod) 
{
    document.getElementById('prodname1').value = value;
	var tok =nomprod.split(";");
//	alert(tok);
	$('prod').value = tok[0];
	$('unidad').value = tok[1];
	$('nomprod').value = value;
//$('nomproveedor1').value=  value;
	//$('nomproveedor1').title=  value;
	document.getElementById('search_suggest').innerHTML = '';
	agrega_prod_tabla(tok[0]);
}


function limpia_tabla()
{
	var tabla= $('datos');//Asigna a la variable tabla el objeto provsT(cuerpo de la tabla) que se encuentra declarado en buscaprov.php

	var numren=tabla.rows.length;
	for(var i = numren-1; i >=0; i--)
	{
		tabla.deleteRow(i);
	}
}

function agrega_prod_tabla(prod)
{

//var nomprod = $("provname1").value;

//alert ('php_ajax/busca_usuario.php?nombre='+nombre);
	new Ajax.Request('php_ajax/busca_prod.php?prod='+prod,
					 {onSuccess : function(resp)
					 {
							// alert(resp.responseText);
							if( resp.responseText ) 
							{
								var tabla= $('datos');//Asigna a la variable tabla el objeto provsT(cuerpo de la tabla) que se encuentra declarado en buscaprov.php

								var numren=tabla.rows.length;
								for(var i = numren-1; i >=0; i--)
								{
									tabla.deleteRow(i);
								}
								//got an array of suggestions.
								//alert(resp.responseText);
								var myArray = eval(resp.responseText);
								//alert("22 "+resp.responseText);
								if(myArray.length>0)
								{
											
									for(var i = 0; i <myArray.length; i++)
									{
										var newrow = tabla.insertRow(i);
										newrow.className ="fila";
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										//newcell.width="15%";
										newcell.innerHTML = myArray[i].prod;
										newcell.align ="center"; 
										
										var newcell2 = newrow.insertCell(1);
										//newcell2.width="50%";
										newcell2.innerHTML = myArray[i].nomprod;
										newcell2.align ="left";  
										
										var newcell3 = newrow.insertCell(2);
										//newcell3.width="15%";
										newcell3.innerHTML =myArray[i].unidad;
										newcell3.align ="center"; 
									}
								}
								else
								{
									alert("No existen resultados para estos criterios.");
								}
							}
					  }
				});
}
