<?php
////
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
//$borrar=revisaPrivilegiosBorrar($privilegios);
$verPDF=revisaPrivilegiosPDF($privilegios);
////

?>
<html>
    <head>
		<!-- Archivo de consultas de Solicitud de Cheques -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/style.css">         
		<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />        
        <title>SAF- Egresos</title>
        <script src="../../javascript_globalfunc/144jquery.min.js"></script>
        <script src="javascript/divhide.js"></script>
        <script src="javascript/jquery.tmpl.js"></script>
		
        <link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
        <script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
        <script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
        <script type="text/javascript" src="javascript/jquery.ingrid-0.9.9-min.js"></script>
        <script>
            $(function()
			{
                $('#query').live('keyup', function()
				{ // cuando se realiza el tecleo, recomendado metodo live
					//
						var opcion= document.getElementById('opc').value;
						var data = 'query=' + opcion + $(this).val();
						//alert(data);
						$.post('bus_solalmacen_ajax.php',data, function(resp)
						{ 
							console.log(resp);
							//alert(resp);
							$('#proveedor').empty();
							$('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor');
                    }, 'json');  
                });
            });
			
			function busca_sol()
			{
						var opcion = document.getElementById('opc').value;
						var fecini = document.getElementById('fecini').value;
						var fecfin = document.getElementById('fecfin').value; 		  	
						var data = 'query=' + opcion + fecini + fecfin;

						$.post('bus_solalmacen_ajax.php',data, function(resp)
						{ //Llamamos el arch ajax para que nos pase los datos
						
						//alert(resp.response);
                        $('#proveedor').empty();
                        $('#tmpl_proveedor').tmpl(resp).appendTo('#proveedor'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							     // los va colocando en la forma de la tabla
                    }, 'json');  // Json es una muy buena opcion               
            }
			
        </script>
        
        <script id="tmpl_proveedor" type="text/x-jquery-tmpl">   
            <tr>
                {{if solic}}
					<td>${solic}</td>
					<td>${nomprod}</td>
					<td>${nomdepto}</td>
					<td>${fecha}</td>
					
					<td><?php if($verPDF){ ?><img src="../../imagenes/consultar.jpg" onClick="window.open('php_ajax/imprime_solic.php?solic='+${solic}) "><?php } ?></td>
                {{else}}
					<td colspan="2">No existen resultados</td>
                {{/if}}
            </tr>
        </script> 

<!-- Termina Script Fechas  ------------------------------------------------------------------------------------------>
       <style type="text/css">
			.Estilo1 {
				font-family: Arial;
				font-size: 13px;
			}
        </style>
</head>
    <body>
    <span class="TituloDForma">Consulta de Solicitudes de Almacen</span>
    <hr class="hrTitForma">
    <div id="main">
  <h1 class="Estilo1">&nbsp;</h1>
<table>   
<tr>
<td width="23%">
	<select name="opc" id="opc" onChange="ShowHidden()">
    	<option value="1" >No. Solicitud</option>
    	<option value="2" >Descripcion</option>
		<option value="3" >Departamento</option>
    	<option value="4" >Por rango de fechas</option>
	</select>
</td>

<td width="77%">
<div align="left" id="cheques" style="z-index:1; position:relative; visibility:visible; width:126px; top: 0px; left: 0px; height: 21px;">
  <input type="text" name="query" id="query" size="40"> 
</div>
</td>
<td>
<div align="left" id="rangofecha" style="z-index:1; position:relative; visibility:hidden; width:367px; top: 0px; left: 0px; height: 26px;">
  Desde:

<input type="text"  name="fecini" id="fecini" size="10">
	<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
		<script type="text/javascript">
			Calendar.setup({
				inputField     :    "fecini",		// id of the input field
				ifFormat       :    "%d/%m/%Y",		// format of the input field
				button         :    "f_trigger1",	// trigger for the calendar (button ID)
				//onClose        :    fecha_cambio,
				singleClick    :    true
			});
		</script>
        

        
  Hasta:<input type="text" name="fecfin" id="fecfin" size="10">
        <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger2",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
		<input type="submit" name="buscheq" id="buscheq" value="Buscar" onClick="busca_sol()">
</div>
</td>
</tr>
</table>
<table>
		<thead>
			<th>Solicitud</th>
			<th>Producto</th>
			<th>Departamento</th>
			<th>fecha</th>
		</thead>
			<tbody id="proveedor" name='proveedor'>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">Encontrar Resultados</td>
				</tr>
			</tbody>
</table>
    </div>
    </body>
</html>
