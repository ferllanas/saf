<?php

require_once("../../connections/dbconexion.php");
// Archivo de consultas de Solicitud de Cheques
include_once 'lib/ez_sql_core.php'; 
//include_once 'lib/ez_sql_mysql.php';
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		

	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$query=$_REQUEST['query'];
$fecini=substr($_REQUEST['query'],1,10);
$fecfin=substr($_REQUEST['query'],11,11);

$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
		$command= "select a.solic,CONVERT(varchar(12),e.falta, 103) as fecha,b.nomprod,d.nomdepto from almacendsolic a 
		left join almacenmsolic e on a.solic=e.solic left join compradproductos b 
		on a.producto=b.prod left join almacenmsolic c on a.solic=c.solic left join nomemp.dbo.nominamdepto d on c.depto =d.depto  
		WHERE a.solic LIKE '%" . substr($_REQUEST['query'],1) . "%' and a.estatus=50 order by a.solic";
	}
	if($opcion==2)
	{
		$command= "select a.solic,CONVERT(varchar(12),e.falta, 103) as fecha,b.nomprod,d.nomdepto from almacendsolic a 
		left join almacenmsolic e on a.solic=e.solic left join compradproductos b 
		on a.producto=b.prod left join almacenmsolic c on a.solic=c.solic left join nomemp.dbo.nominamdepto d on c.depto =d.depto  
		WHERE b.nomprod LIKE '%" . substr($_REQUEST['query'],1) . "%' and a.estatus=50 order by a.solic";
	}
	if($opcion==3)
	{
		$command= "select a.solic,CONVERT(varchar(12),e.falta, 103) as fecha,b.nomprod,d.nomdepto from almacendsolic a 
		left join almacenmsolic e on a.solic=e.solic left join compradproductos b 
		on a.producto=b.prod left join almacenmsolic c on a.solic=c.solic left join nomemp.dbo.nominamdepto d on c.depto =d.depto  
		WHERE d.nomdepto LIKE '%" . substr($_REQUEST['query'],1) . "%' and a.estatus=50 order by a.solic";
	}
	
	if($opcion==4)
 	{			

		list($dd,$mm,$aa)= explode('/',$fecini);
		$fecini=$aa.'-'.$mm.'-'.$dd;
				
		list($dd2,$mm2,$aa2)= explode('/',$fecfin);
		$fecfin=$aa2.'-'.$mm2.'-'.$dd2;
				
		$command= "select a.solic,CONVERT(varchar(12),e.falta, 103) as fecha,b.nomprod,d.nomdepto from almacendsolic a 
		left join almacenmsolic e on a.solic=e.solic left join compradproductos b 
		on a.producto=b.prod left join almacenmsolic c on a.solic=c.solic left join nomemp.dbo.nominamdepto d on c.depto =d.depto 
		where e.falta >= CONVERT(varchar(12),'$fecini', 103) and e.falta <=CONVERT(varchar(12),'$fecfin', 103) and a.estatus=50 order by a.solic ";
	}
		//echo $command;
		$stmt2 = sqlsrv_query( $conexion,$command);
		$i=0;
		while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['solic']=$row['solic'];
			$datos[$i]['nomprod']=utf8_decode($row['nomprod']);
			$datos[$i]['nomdepto']=utf8_decode($row['nomdepto']);
			$datos[$i]['fecha']=$row['fecha'];
			$i++;
		}

echo json_encode($datos);  
?>