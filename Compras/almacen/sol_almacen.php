<?php
	require_once("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site']; //Misma variable para Almacen
	//$depto = $_COOKIE['depto'];//$_GET['usuario']; //Misma variable para Almacen

	//$usuario = '001981';
	//$depto='1232';
	$productosdeusuario= null;
	$area=null;
	$entregaren="";
	$observaciones="";
	$id_req="";
	
	$fecha_req="";   //Misma variable para Almacen
	$hora_req="";    //Misma variable para Almacen
	$depto = "";     //Misma variable para Almacen
	$coord = "";
	$dir = "";
	$nomdepto = "";  //Misma variable para Almacen
	$nomdir = "";
	
	$productos= array();
	$personasAutori=array();
	$tiposDRequi=array();

//	$personasAutori = fun_ObtienePosiblesfirmas($usuario);//llama a funcion para obtener posibles firmantes
	$tiposDRequi = fun_ObtenertipoAlmacen(); // Cadena de conexi�n

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		//Busqueda de Productos
//		$command= "SELECT prod, nomprod, cve,unidad  FROM compradproductos b  ORDER BY prod ASC";	
		$command= "select a.producto as prod,b.nomprod, b.cve,b.unidad from almacendstock a left join compradproductos b on a.producto=b.prod where a.estatus < 9000 and b.estatus< 9000 order by b.nomprod";
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$productos[$i]['cve'] =trim($lrow['cve']);
				$productos[$i]['prod'] =trim($lrow['prod']);
				$productos[$i]['nomprod'] =trim($lrow['nomprod']);
				$productos[$i]['unidad'] =trim($lrow['unidad']);
				$i++;
			}
		}
		
		//Busqueda de datos del departamento
		$command= "select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto=b.depto where numemp='$usuario'";
		$stmt2 = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt2))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
			{
				$depto =trim($lrow['depto']);
				$coord =trim($lrow['coord']);
				$dir =trim($lrow['dir']);
				$nomdepto =trim($lrow['nomdepto']);
				$nomdir =trim($lrow['nomdir']);
				$i++;
			}
		}
		
		//mconfigconfig
	}

function fun_ObtenertipoAlmacen()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="SELECT tiporeq, nomtiporeq FROM compramtiporeq";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
	}
	
	if(!$fails)
	{
		
		// Retrieve and display the first result. 
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$datos[$i]['tiporeq'] = $row['tiporeq'];
			$datos[$i]['nomtiporeq'] = $row['nomtiporeq'];
			$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $datos;
}

//Funcion que llama procedimiento almacenado para obtener	
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Control de Almacen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>



<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/comrequisicion.js"></script>
</head>
<body >
<form id="form_requisicion"  name ="form_requisicion" style="width:100%"  metdod="post"  action="php_ajax/crear_reqpdf.php?">
<table width="100%">
	<tr>
	<td colspan="4">
		<table width="100%">
			<tr>
				<td colspan="2" width="90%" align="center" class="TituloDForma">Captura Solicitud de Almacen<hr class="hrTitForma">
				</td>
			</tr>
		</table> 
	</td>
	</tr>
	<tr>
		<td class="texto8" width="5%"> Departamento:</td>
  	  <td width="40%" align="left">
				<input type="text"  style="width:100%" id="pswd"  name="pswd" value="<?php echo $nomdepto;?>" class="caja_toprint texto8" readonly >
		</td>
		<td width="40%" align="right">	<input type="hidden" value="<?php echo $fecha_req;?>" id="fecha_req"  name="fecha_req" style="visibility:hidden ">
		  <input type="hidden" value="<?php echo $hora_req;?>"  id="hora_req"   name="hora_req"  style="visibility:hidden ">
		  <input type="hidden" id="usuario"  name="usuario"  value="<?php echo $usuario;?>" class="required">
                        <input type="text" width="50%"  value="Numero de Solicitud:" id="msgnumreq" name="msgnumreq" class="caja_grande" style="visibility:hidden " readonly>
                        <input type="text" width="50%" value="<?php echo $id_req;?>" id="id_req"    name="id_req"    class="caja_grande" style="visibility:hidden " readonly>                   	    
		</td>
	</tr>
	
	<tr>
		<td class="texto8" width="5%">&nbsp;</td>
			<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><hr color="#CCCCCC"></td>
	</tr>
	<tr>
		<td colspan="4" align="center">
			<table width="100%"> 
				<tr>   
				  <td class="texto8" width="59%" style="z-index:0;">
					  <div align="left" style="z-index:2; position:absolute; width:400px; left: 15px; top: 155px;">Producto:
							  <input class="texto8" name="txtSearch" type="text" id="txtSearch" size="40" style="width:250px;"  onKeyUp="searchSuggest();" autocomplete="off"/>  
						<div id="search_suggest" style="z-index:1; position:absolute;" > </div>
		   		    </div>
					  <blockquote>&nbsp;</blockquote>
				  </td>
						<td width="9%" class="texto8">&nbsp;				  </td>
						<td width="20%" class="texto8">
							<table width="50%" height="25">
							<tr>
								<td width="55" align="left">Cantidad:</td>
								<td width="279" align="left"><input class="texto8" name="txtSearch" type="text" id="can_ent" size="40" style="width:50px; " autocomplete="off" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
												<input type="hidden" id="unidaddPro" name="unidaddPro" >
												<input type="hidden" id="busProd" name="busProd" ></td>
							</tr>
						</table>
						</td>
						<td width="6%" align="left"><img src="../../imagenes/deshacer.jpg" width="31" height="28" title="Deshacer alta de producto" onClick="deshacer()"></td>
						<td width="2%" align="left">&nbsp;</td>
						<td width="4%" align="left">
							<input type="button" value="+" id="ad_colin2" name="ad_colin2" onClick="agrega_ProductoARequisicion();" class="texto8">
						</td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="4"></td>
	</tr>
  	<tr>
		<td colspan="4" align="center">
			<table border="1" id="tmenudusuario" name="tmenudusuario" width="100%">
				<tr>
					<th class="subtituloverde" width="20%">Producto</th>
					<th class="subtituloverde" width="5%">Unidad</th>
					<th class="subtituloverde" width="4%">Cantidad</th>
					<th class="subtituloverde" width="4%">&nbsp;</th>
				</tr>
				<tbody id="menus">
					<!--<?php  if( $productosdeusuario!=null) for($j=0;$j<count($productosdeusuario);$j++){ ?>
						<tr id="filaOculta" >
						<td width="14%">
                          <input type="hidden" id="idprod" name="idprod"  value="">
							<input type="text" id="producto" name="producto"  value=""  class="caja_toprint"  style="width:100% " >
						</td>
						<td width="14%"><input type="text" id="desc" name="desc"  value="<?php echo $productosdeusuario[$j]['posicion'];?>"></td>
						<td width="14%"><input type="text" id="cantidad" name="cantidad"  value="<?php echo $productosdeusuario[$j]['posicion'];?>" ></td>
						
						<td width="14%">									   
							<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_ProductoARequisicion(event,this);" >
						</td width="25%"></tr>
					<?php }else{ ?>
					<div style="position:absolute ">
					<tr id="filaOculta" style="visibility:hidden; ">
					 
					  	<td><input type="hidden" id="idprod" name="idprod"  value="">
							<input type="text" id="producto" name="producto"  value=""  class="caja_toprint" style="width:100% " >
						</td>
						<td><input type="text" id="desc" name="desc"  value=""  ></td>
						<td><input type="text" id="cantidad" name="cantidad"  value=""  ></td>
						
						<td width="14%"><input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_ProductoARequisicion(event,this);" >
										  <input type="hidden" id="id_modulo" name="id_modulo" value="">
										  
						</td></tr></div>
					<?php } ?>-->
				</tbody>
			</table>
		</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr><td align="center" width="100%"><input type="button"  value="Guardar" onClick="GuardarRequisicion();" class="caja_entrada" id="btnguardar" name="btnguardar"></td>
		<!--<td align="center" width="50%" class="texto8"><a href='php_ajax/crear_reqpdf.php?idreq=<?php echo $id_req;?>'>Imprime requisici�n</a></td>-->
	</tr>
</table>
</form>

</body>
</html>
