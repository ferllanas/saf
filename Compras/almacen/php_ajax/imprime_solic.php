<?php 
$html=" 
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Impresion de lmacen</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
<script language='javascript' src='../../../prototype/jQuery.js'></script>
<script language='javascript' src='../../../prototype/prototype.js'></script>
<style type='text/css'>
		body 
		{
			font-family:'Arial';
		}
</style>

	</head>
<body align='center'>

";

    if (version_compare(PHP_VERSION, '5.1.0', '>='))
 	require_once("../../../dompdf/dompdf_config.inc.php");
	date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
    
    
    $infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$producto = 0;
	$surtido = 0;
	$datos=array();
	$producto = 0;
	$nomdepto = "";
	$codigoCalidad='';
    $usuario = $_COOKIE['ID_my_site'];
    //$usuario ='001981';
    $solic = strtoupper($_REQUEST['solic']);
	// $surtido = strtoupper($_REQUEST['surtido']);
	// $producto = strtoupper($_REQUEST['produc']);
	
	$fecha_aut=date("d/m/Y");
	$hora = getdate(time());
	$hora_act=( $hora["hours"] . ":" . $hora["minutes"] . ":" . $hora["seconds"] ); 

		if ($conexion)
		{
			$consulta  = "select a.*,b.nomprod,b.unidad,c.usuario,c.depto,d.nomdepto, (SELECT codigo FROM configdcodigos where descrip='entrega almacen' AND estatus<9000) as codigo
							from almacendsolic a left join compradproductos b on a.producto=b.prod left join almacenmsolic c on a.solic=c.solic left join nomemp.dbo.nominamdepto d on c.depto=d.depto collate database_default where a.solic=$solic and a.estatus=50 order by a.mov";
			
			//echo $consulta;
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				//print_r($row);
				$datos[$i]['producto']= trim($row['producto']);
				$datos[$i]['nomprod']= trim($row['nomprod']);
				$datos[$i]['unidad']= trim($row['unidad']);
				$datos[$i]['cantidad']= trim($row['cantidad']);
				$datos[$i]['cantaut']= trim($row['cantaut']);
				$datos[$i]['cantentrega']= trim($row['cantentrega']);
				$datos[$i]['nomdepto']= trim($row['nomdepto']);
				$codigoCalidad=trim($row['codigo']);
				$i++;
			}
		}
$html.="
<table width='100%' border='0' >
<thead  width='100%'>
    <tr>
	  <td colspan='1' align='left'><img src='../../../".$imagenPDFSecundaria."' width='100%' height='150' /></td>
	   <td colspan='4' align='CENTER' class='caja_grande'><b>SALIDA DE MATERIALES DE ALMACEN</b></td>
	  <td colspan='1' align='right'><img src='../../../$imagenPDFPrincipal' width='100%' height='150' /></td>
    </tr>	
	<tr>
      <td colspan='6'>&nbsp;</td>
    </tr>  
    <tr>
      <th colspan='6' align='left' class='texto12'>Departamento solicitante: <u>". $datos[0]['nomdepto']."</u></th>
    </tr>
	<tr>
	  <td colspan='6' align='left' class='texto12'>No. Solicitud: ".$solic."</td>
    </tr>
    <tr>
      <td colspan='3' align='left' class='texto10'>Productos entregados por almacen</td>
         <td colspan='3' align='right' class='texto10'>Fecha : ".$fecha_aut."</td>      
    </tr>
	<tr>
      <td colspan='6'>&nbsp;</td>
    </tr>	
    <tr align='center'>
      <td width='100/2' align='center' class='texto11B'>Cve </td>
      <td width='100/50' align='center' class='texto11B'>Descripci&oacute;n</td>
      <td width='100/2' align='center' class='texto11B'>Unidad</td>
      <td width='100/2' align='center' class='texto11B'>Cant. Solicitada</td>
      <td width='100/2' align='center' class='texto11B'>Cant. Autotizada</td>
      <td width='100/2' align='center' class='texto11B'>Cant. Entregada</td>
    </tr>
	  <tr>
	  	<td colspan='6'><hr></td>
	  </tr>
        
</thead>    
	";
 
for($i=0;$i<count($datos);$i++)
{

  $html.="
  	<tr  align='center'>         
  	  <td align='center' class='texto10' width='50'>".$datos[$i]['producto']."</td>
      <td align='left' class='texto10' width='200'>".$datos[$i]['nomprod']."</td>
      <td align='center' class='texto10' width='50'>". $datos[$i]['unidad']."</td>
      <td align='center' class='texto10' width='50'>".$datos[$i]['cantidad']."</td>
      <td align='center' class='texto10' width='50'>".$datos[$i]['cantaut']." </td>
      <td align='center' class='texto10' width='50'>".$datos[$i]['cantentrega']."</td>
    </tr>";

}

  $html.="
</table>
<table style='width:100%; heigth:70px; vertical-align:bottom;'>
	<tr><td style='height:70px; vertical-align:bottom;' align='center'>Firma de quien recibe</td>
	</tr>
</table>";



 //echo $html;
// Variables de impresión del sistema de Almacen a PDF
  $dompdf = new DOMPDF();
  //$textod=utf8_encode("P�gina {PAGE_NUM}");
	//	$textop=utf8_encode("Requisici�n");
	//$pdf->page_text(25, 760, "$textop '.$idReq.'", "", 12, array(0,0,0));
  $pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	
	$pdf->page_text(670, 570, "'.$codigoCalidad.'", "", 12, array(0,0,0));
	
 } 

</script> ';  //$pdf->page_text(500, 772, "MO.2000.2044.1", "", 12, array(0,0,0));

 $html.=$pageheadfoot."</body>
</html> ";
  $dompdf->set_paper('letter','landscape');
  $dompdf->load_html($html);
  $dompdf->render();
  $pdf = $dompdf->output(); 

	$tmpfile = tempnam(".", "dompdf_.pdf");
	file_put_contents($tmpfile, $pdf ); // Replace $smarty->fetch()
	rename($tmpfile,$tmpfile.".pdf");
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment; filename="ordenes de compra"');
	header("Location: ".basename($tmpfile.".pdf"));
	
	$files = glob('*.tmp.pdf'); // get all file names
	foreach($files as $file){ // iterate files
	  if(is_file($file) && basename($tmpfile.".pdf")!= basename($file))
		unlink($file); // delete file
	}
/*  file_put_contents("../pdf_files/$solic.pdf", $pdf);
  $dompdf->stream("../pdf_files/$solic.pdf");
  */
?>
