<?php

	///Modificado por Fernando Llanas 10/12/2012
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$id="";
		
	$usuario = "";//Variable id del usuario
	if(isset($_POST['usuario']))
		$usuario=$_POST['usuario'];
	else
			$usuario = $_COOKIE['ID_my_site'];	
			//$usuario = '001981'; // FLL Dejo usuario por default 10/12/2012
			
	$productos="";//variable que almacena la cadena de los productos
	if(isset($_POST['prods']))
		$productos=$_POST['prods'];

	//$str_productos = str_replace("\\", "", $str_productos);//reemplaza
	//echo $str_productos;
	//eval( '$productos = array'.$str_productos.';' );//la funcion eval permite obtener un array a partir de la cadena str_productos
	
	
	$fecha="";
	$fails=false;
	$hora="";
	$depto="";
	$nomdepto="";
	$dir=""; 
	$nomdir="";
	if ( is_array( $productos ) ) // Pregunta se es una array la variable productos
	{
		//Crear en tabla de requisicion (envio)
		list($id,$fecha,$fails,$hora,$depto,$nomdepto)= fun_creaAlmacen($usuario); //crea nueva requisicion
		if(!$fails)
			for($i=0;$i<count($productos);$i++)
			{
				//Agrega producto a requisicion
				$fails=func_creaDetSolAlmacen($id, 
										$productos[$i]['producto'], 
										str_replace(",","",$productos[$i]['cantidad']),(int)$i+1); 
									//	$productos[$i]['descripcion'], Agrega la descripción que se hace bajo el requerimiento del producto BOBY
										//$i;
			}
	}
	
	$datos=array();//prepara el aray que regresara
	$datos[0]['id']=trim($id);// regresa el id de la
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['depto']=trim($depto);
	$datos[0]['nomdepto']=trim($nomdepto);	
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	
function fun_creaAlmacen($usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";	
	$almacen=1;
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_almacen_A_msolic(?,?)}";//Arma el procedimeinto almacenado (Llamdo al procedimiento)
	$params = array(&$almacen,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)." sp_almacen_A_msolic ".print_r($params,true).$usuario);
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$depto = $row['depto'];
			$nomdepto = $row['nomdepto'];
//			$dir = $row['dir'];
//			$nomdir = $row['nomdir'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$fails,$hora,$depto,$nomdepto);
}

//Funcion para registrar el productoa  la requisicion
//function func_creaProductoDRequisicion($id_Requi, $producto, $cantida, $descripcion, $unidad)
function func_creaDetSolAlmacen($folio, $producto, $cantidad, $mov)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$id="";//Store proc regresa id de la nueva requisicion
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_almacen_A_dsolic(?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$productos[$i]['producto'], $productos[$i]['cantidad'], $productos[$i]['descripcion'],'0',$productos[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
//	$params = array(&$id_Requi, &$producto, &$descripcion, &$cantida, &$mov, &$unidad);//Arma parametros de entrada
	$params = array(&$folio, &$producto, &$cantidad, &$mov);//Arma parametros de entrada sin descripcion
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
//		 die( print_r( sqlsrv_errors(), true). $id_Requi.",".$producto.",".$descripcion.",".$cantida.",".$mov.",".$unidad); Se deja original por si falla
		 die( print_r( sqlsrv_errors(), true). $folio.",".$producto.",".$cantidad.",".$mov);
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>