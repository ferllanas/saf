<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$prod = $_REQUEST['prod'];
$datos=array();

	if ($conexion)
	{		
		
		if($prod>0)
		{
			$consulta = "select * from compradproductos where prod not in (select producto from almacendstock) and prod=$prod and almacen='SI' order by nomprod";
		}
		else
		{
			$consulta = "select * from compradproductos where prod not in (select producto from almacendstock) and almacen='SI'";
		}
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['prod']= trim($row['prod']);
				$datos[$i]['nomprod']= trim($row['nomprod']);
				$datos[$i]['unidad']= trim($row['unidad']);
				$i++;
			}
		}
	}
echo json_encode($datos);
?>