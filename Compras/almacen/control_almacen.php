<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	require_once("../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//$usuario = $_COOKIE['ID_my_site'];
$usuario='001981';
$sw = 0;
$datos = array();
$datos2 = array();
$prod = 0;
$nomprod = "";
$unidad = "";
$almacen=0;
$cantidad=0;
$minimo=0;
if(isset($_REQUEST['prodname1']))
	$nomprod2 = $_REQUEST['prodname1'];

if(isset($_REQUEST['sw']))
	$sw = $_REQUEST['sw'];
	
if(isset($_REQUEST['prod']))
	$prod = $_REQUEST['prod'];

if(isset($_REQUEST['nomprod']))
	$nomprod = $_REQUEST['nomprod'];
	
if(isset($_REQUEST['unidad']))
	$unidad = $_REQUEST['unidad'];

if(isset($_REQUEST['cantidad']))
	$cantidad = $_REQUEST['cantidad'];

if(isset($_REQUEST['minimo']))
	$minimo = $_REQUEST['minimo'];

if(isset($_REQUEST['almacen']))
	$almacen = $_REQUEST['almacen'];
	
if ($conexion)
{		
//	$consulta = "select * from compradproductos where estatus < 9000 order by nomprod";
	$consulta = "select * from compradproductos where prod not in (select producto from almacendstock) and estatus < 9000 and almacen='SI' order by nomprod";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['prod']= trim($row['prod']);
		$datos[$i]['nomprod']= trim($row['nomprod']);
		$datos[$i]['unidad']= trim($row['unalmacen']);
		$i++;
	}
}

{
	$consulta = "select a.*,b.nomprod,b.unalmacen from almacendstock a left join compradproductos b on a.producto=b.prod where a.estatus < 9000 and b.estatus< 9000 and b.almacen='SI' order by b.nomprod";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos2[$i]['producto']= trim($row['producto']);
		$datos2[$i]['nomprod']= trim($row['nomprod']);
		$datos2[$i]['unidad']= trim($row['unalmacen']);
		$datos2[$i]['cantidad']= trim($row['cantidad']);
		$datos2[$i]['minimo']= trim($row['minimo']);
		$datos2[$i]['almacen']= trim($row['almacen']);
		
		$i++;
	}
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href='../../css/estilos.css' rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/almacenfuncion.js"></script>
<script language="javascript" src="javascript/comrequisicion.js"></script>
<script language="javascript" src="javascript/busquedaincprod.js"></script>

<TITLE></TITLE>
<SCRIPT language= "JavaScript">
	var ancho1,ancho2,i;
	var columnas=4; //CANTIDAD DE COLUMNAS//
	
	function ajustaCeldas()
	{
		for(i=0;i<columnas;i++)
		{
			ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
			ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
			if(ancho1>ancho2){
			document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-5;}
			else{
			document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-5;}
		}
	}
</SCRIPT>
<STYLE>
#encabezado{border:0}
#encabezado th{border-width:1px}
#datos{border:0}
#datos td{border-width:1px}
.Estilo1 {font-size: 12px}
.Estilo2 {font-size: 12}
</STYLE>

</HEAD>
<form name="form1" method="post" action="">
<BODY onload=ajustaCeldas()>

<h2>Catalogo de Almacen</h2>
<table width="68%"  border="0">
  <tr>
    <th align="left">
      <div align="left" style="z-index:2; position:relative; width:400px">Producto:
          <input class="texto8" name="prodname1" type="text" id="prodname1" size="40" onBlur="regresa(this)" style="width:250px;"  onKeyUp="searchProducto1(this);" autocomplete="off">
        <div id="search_suggest" style="z-index:1; position:absolute; height: 1px;" > </div>
      </div>
    </th>
  </tr>
</table>
<table width="705" height="19%" border=2 align="left" style="vertical-align:top; border-color: #006600">
  <td width="90%" height="164" style="vertical-align:top;">
      <table width="100%" border=0 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <th width="82" align="center" class="subtituloverde">Prod</th>
          <th width="434" align="center" class="subtituloverde">Descripci&oacute;n</th>
          <th width="87" align="center" class="subtituloverde">Unidad</th>
          <th width="103" align="center" class="subtituloverde">&nbsp;</th>
        </tr>
      </table>
      <div style="overflow:auto; height:150px; padding:0">    
        <table name="ProdT" width="100%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white id="datos">
        <?php 
		
			for($i=0;$i<count($datos);$i++)
			{
		?>
          <tr class="fila" onClick="javascript: ;location.href='control_almacen.php?editar=true&prod=<?php echo $datos[$i]['prod']?>&nomprod=<?php echo $datos[$i]['nomprod']?>&unidad=<?php echo $datos[$i]['unidad']?>&sw=0'">
            <td width="12%" align="center"><?php echo $datos[$i]['prod'];?></td>
            <td width="61%" align="left"><?php echo $datos[$i]['nomprod'];?></td>
            <td width="13%" align="center"><?php echo $datos[$i]['unidad'];?></td>
            <td width="14%" align="center">&nbsp;</td>
          </tr>
        <?php
		}
		?>
        </table>
    </div></td>
</table>

  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <table width="710" border="0" align="left">
    <tr>
      <th width="19%" scope="row"><div align="right" class="Estilo1">Producto:</div></th>
      <td width="2%">&nbsp;</td>
      <td width="61%"><input tabindex=1 type="text" name="prod" id="prod" size="20" readonly value="<?php echo $prod;?>"></td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Descripci&oacute;n:</div></th>
      <td>&nbsp;</td>
      <td><input tabindex=2 type="text" name="nomprod" id="nomprod" size="70" readonly value="<?php echo $nomprod;?>"></td>
      <td width="7%">&nbsp;</td>
      <td width="11%"><input type="button" name="agregaprod" id="agregaprod" value="Agrega" onClick="add()" <?php if(isset($_REQUEST['sw']))if($_REQUEST['sw']==1) echo "disabled='disabled'";?>></td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Unidad de Medida </div></th>
      <td>&nbsp;</td>
      <td><input tabindex=3 type="text" name="unidad" id="unidad" size="20" readonly value="<?php echo $unidad;?>"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Existencia</div></th>
      <td>&nbsp;</td>
      <td><input tabindex=4 type="text" name="cantidad" id="cantidad" size="20" value="<?php echo $cantidad;?>"></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Cant. Minina </div></th>
      <td>&nbsp;</td>
      <td><input tabindex=5 type="text" name="minimo" id="minimo" size="20" value="<?php echo $minimo;?>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th scope="row"><div align="right"><span class="Estilo1">Almacen</span></div></th>
      <td>&nbsp;</td>
      <td><input tabindex=6 type="text" name="almacen" id="almacen" size="20" value="<?php echo $almacen;?>"></td>
      <td>&nbsp;</td>
      <td><input type="button" name="Actualizar" id="Actualizar" value="Actualza" onClick="Actualiza_Cambios()" <?php if(isset($_REQUEST['sw']))if($_REQUEST['sw']==0) echo "disabled='disabled'";?>></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>

  <p>&nbsp;</p>
  <table width="694" height="19%" border=2 align="left" style="vertical-align:top; border-color: #006600">
  <td width="90%" height="171" style="vertical-align:top;">
      <table width="100%" border=0 cellpadding=0 cellspacing=0 id="encabezado">
        <tr>
          <th width="43" align="center" class="subtituloverde">Prod</th>
          <th width="406" align="center" class="subtituloverde" >Descripci&oacute;n</th>
          <th width="48" align="center" class="subtituloverde" ><div align="left">Unidad</div></th>
          <th width="82" align="center" class="subtituloverde" >Existencia</th>
          <th width="89" align="center" class="subtituloverde" >Cantidad Min.</th>
          <th width="14" align="center" class="subtituloverde" >&nbsp;</th>
        </tr>
      </table>
      <div style="overflow:auto; height:150px; padding:0">
        <table name="Tabla2" id="Tabla2" width="100%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white>
          <?php 
		
			for($i=0;$i<count($datos2);$i++)
			{
		?>
          <tr class="fila" onClick="javascript: ;location.href='control_almacen.php?editar=true&prod=<?php echo $datos2[$i]['producto']?>&producto=<?php echo $datos2[$i]['producto']?>&nomprod=<?php echo $datos2[$i]['nomprod']?>&unidad=<?php echo $datos2[$i]['unidad']?>&cantidad=<?php echo $datos2[$i]['cantidad']?>&minimo=<?php echo $datos2[$i]['minimo']?>&almacen=<?php echo $datos2[$i]['almacen']?>&sw=1'">
		    <td width="7%" align="center"><?php echo $datos2[$i]['producto'];?></td>
            <td width="59%" align="left"><?php echo $datos2[$i]['nomprod'];?></td>
            <td width="7%" align="left"><?php echo $datos2[$i]['unidad'];?></td>
            <td width="12%" align="center"><?php echo $datos2[$i]['cantidad'];?></td>
            <td width="13%" align="center"><?php echo $datos2[$i]['minimo'];?></td>
            <td width="2%" align="center">&nbsp;</td>
          </tr>
          <?php 
		}
		?>
        </table>
    </div></td>
</table>
  <p>&nbsp;</p>
  </form>
</BODY>
</HTML>