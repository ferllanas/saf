<?php 
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');

	require_once("../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$solic="";
if(isset($_REQUEST['solic']))
	$solic = $_REQUEST['solic'];


$datos3=array();
			$consulta="select a.*,b.nomprod,b.unidad, c.cantidad as stock 
						from almacendsolic a 
						left join compradproductos b on a.producto=b.prod 
						left join almacendstock c on a.producto=c.producto 
						where a.solic=$solic and a.estatus<9000 
						order by a.producto";
						
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos3[$i]['producto']= trim($row['producto']);
		$datos3[$i]['nomprod']= utf8_encode(trim($row['nomprod']));
		$datos3[$i]['unidad']= trim($row['unidad']);
		$datos3[$i]['cantidad']= trim($row['cantidad']);
		$datos3[$i]['cantaut']= trim($row['cantaut']);
		$datos3[$i]['cantentrega']= trim($row['cantentrega']);
		$datos3[$i]['stock']= trim($row['stock']);		
		$i++;
	}
	
	echo json_encode($datos3);
?>