<?php
// Archivo de consultas de Solicitud de Cheques
include_once 'lib/ez_sql_core.php'; 
//include_once 'lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$fecini = "";
$datos=array();
$solic="0";
$query="";
$opcion=$_REQUEST['opcion'];

$fecini='01/01/1900';
$fecfin='01/01/1900';

$usuario = $_COOKIE['ID_my_site'];
$depto = $_COOKIE['depto'];
$sino=0;

//print_r( $_REQUEST);

switch($opcion)
{
		case 1:
			$query=$_REQUEST['query'];
			$solic=$_REQUEST['query'];
			$sino=0;
			break;
		case 2:
			$query="";
			$sino=1;
			$fecini=substr($_REQUEST['query'],0,10);
			$fecini= substr($fecini,6,4)."/".substr($fecini,3,2)."/".substr($fecini,0,2);
			$fecfin=substr($_REQUEST['query'],10,11);
			$fecfin= substr($fecfin,6,4)."/".substr($fecfin,3,2)."/".substr($fecfin,0,2);
			break;
}		

if ($conexion)
{
	$ejecuta ="{call sp_almacen_C_solic_x_coord(?,?,?,?,?)}";
	//echo $ejecuta;
	$variables = array(&$depto,&$solic,&$sino,&$fecini,&$fecfin);
	//print_r($variables);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	//echo $ejecuta;
	//print_r($variables);
	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		//
		
		print_r($variables);
		die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
	$i=0;
	if(!$fails)
	{
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC ))
		{
			$datos[$i]['solic']=$row['solic'];
			$datos[$i]['nomdepto']=utf8_decode($row['nomdepto']);
			$datos[$i]['falta']=$row['falta2'];
			$datos[$i]['estatus']=$row['estatus'];
			$i++;
		}
		//sqlsrv_free_stmt( $row );
	}
}

echo json_encode($datos);  
?>