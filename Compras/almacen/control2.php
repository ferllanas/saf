<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	require_once("../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos = array();
$idprod = "";
$descrip = "";
$cantidad = 0;
$cantmin = 0;
$unidad = "";

if(isset($_REQUEST['idprod']))
	$idprod = $_REQUEST['idprod'];

if(isset($_REQUEST['descrip']))
	$descrip = $_REQUEST['descrip'];

if(isset($_REQUEST['cantidad']))
	$cantidad = $_REQUEST['cantidad'];

if(isset($_REQUEST['cantmin']))
	$cantmin = $_REQUEST['cantmin'];
	
if(isset($_REQUEST['unidad']))
	$unidad = $_REQUEST['unidad'];

if ($conexion)
{		
	$consulta = "select * from compradproductos where estatus<'9000' order by descrip";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['idprod']= trim($row['idprod']);
		$datos[$i]['descrip']= trim($row['descrip']);
		$datos[$i]['cantidad']= trim($row['cantidad']);
		$datos[$i]['cantmin']= trim($row['cantmin']);
		$datos[$i]['unidad']= trim($row['unidad']);
		$i++;
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href='../../css/estilos.css' rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/almacenfuncion.js"></script>

<TITLE></TITLE>
<SCRIPT language= "JavaScript">
	var ancho1,ancho2,i;
	var columnas=5; //CANTIDAD DE COLUMNAS//
	
	function ajustaCeldas()
	{
		for(i=0;i<columnas;i++)
		{
			ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
			ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
			if(ancho1>ancho2)
			{
			document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-6;}
			else
			{
			document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-6;}
		}
	}
</SCRIPT>
<STYLE>
#encabezado{border:0}
#encabezado th{border-width:1px}
#datos{border:0}
#datos td{border-width:1px}
.Estilo1 {font-size: 12px}
.Estilo2 {font-size: 12}
</STYLE>

</HEAD>
<form name="form1" method="post" action="">
<BODY onload=ajustaCeldas()>

<h2>Catalogo de Almacen</h2>
<table height="133" border=0 align="left"  >
  <td width="797" height="129">
      <table width="775" border=0 cellpadding=2 cellspacing=0 id="encabezado">
        <tr>
          <th width="44" align="center" class="subtituloverde">Prod</th>
          <th width="475" align="center" class="subtituloverde" calss="subtituloverde12">Descripci&oacute;n</th>
          <th width="96" align="center" class="subtituloverde" calss="subtituloverde12">Existencia</th>
          <th width="67" align="center" class="subtituloverde" calss="subtituloverde12">Cant. Min. </th>
          <th width="73" align="center" class="subtituloverde" calss="subtituloverde12">Unidad</th>
        </tr>
      </table>
      <div style="overflow:auto; height:100px; padding:0">
        <table width="101%" height="33" border=0 cellpadding=1 cellspacing=0 bgcolor=white id="datos">
          <tr>
            <?php 
		
			for($i=0;$i<count($datos);$i++)
			{
		?>
<!--          <tr class="d<?php echo ($i % 2);?>"> -->
		  <tr class="fila" onClick="javascript: ;location.href='control2.php?editar=true&idprod=<?php echo $datos[$i]['idprod']?>&descrip=<?php echo $datos[$i]['descrip']?>&cantidad=<?php echo $datos[$i]['cantidad']?>&cantmin=<?php echo $datos[$i]['cantmin']?>&unidad=<?php echo $datos[$i]['unidad']?>'">
            <td align="center"><?php echo $datos[$i]['idprod'];?></td>
            <td align="left"><?php echo $datos[$i]['descrip'];?></td>
            <td align="left"><?php echo $datos[$i]['cantidad'];?></td>
            <td align="center"><?php echo $datos[$i]['cantmin'];?></td>
            <td align="center"><?php echo $datos[$i]['unidad'];?></td>
          </tr>
          <?php 
		}
		?>
        </table>
    </div></td>
</table>

  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <table border="0" align="left">
    <tr>
      <th width="24%" scope="row"><div align="right" class="Estilo1">Producto:</div></th>
      <td width="2%">&nbsp;</td>
      <td width="74%"><form name="form2" method="" action="">
        <input tabindex="1" type="text" name="idprod" id="idprod" size="20" readonly value="<?php echo $idprod;?>">
      </form></td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Descripci&oacute;n:</div></th>
      <td>&nbsp;</td>
      <td><input tabindex="2" type="text" name="descrip" id="descrip" size="80" value="<?php echo $descrip;?>"></td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Existencia:</div></th>
      <td>&nbsp;</td>
      <td><input tabindex="3" type="text" name="cantidad" id="cantidad" size="20" value="<?php echo $cantidad;?>"></td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Cantidad Minima: </div></th>
      <td>&nbsp;</td>
      <td><input tabindex="4" type="text" name="cantmin" id="cantmin" size="20" value="<?php echo $cantmin;?>"></td>
    </tr>
    <tr>
      <th scope="row"><div align="right" class="Estilo1">Unidad de Medida: </div></th>
      <td>&nbsp;</td>
      <td><input tabindex="5" type="text" name="unidad" id="unidad" size="20" value="<?php echo $unidad;?>"></td>
    </tr>
    <tr>
      <th scope="row">&nbsp;</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </form>
  <table width="64%" align="center" border="0">
    <tr>
      <td width="25%"></form>
        <input type="button" name="agregar" id="agregar" value="Agregar" onClick="limpia()">      
      <td width="29%"><input type="button" name="actualiza" id="actualiza" value="Actualizar" onClick="Actualiza()"></td>
            <td width="32%"></form>
            <input type="button" name="Submit3" value="Eliminar">
    </tr>
  </table>
</BODY>
</HTML>