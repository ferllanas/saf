<?php
	require_once("../../connections/dbconexion.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta content="no-cache" http-equiv="Pragma" />
		<meta content="no-cache" http-equiv="Cache-Control" />
		<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../calendario/calendar.js"></script>
		<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
		<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
		
		<script language="javascript" type="text/javascript"></script>
		<script language="javascript" src="../../prototype/prototype.js"></script>
		<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
		<script language="javascript" src="javascript/comrequisicion.js"></script>
		<style type="text/css" media="screen">
            body 
			{
                font-size:10;
      			font-family:verdana;
            }
            .suggest_link {
                background-color: #FFFFFF;
                border: 0px solid #000000;
                padding: 2px 6px 2px 6px;
                
            }                 
            .suggest_link_over {
                background-color: #FF7F00;
                color: white;
                border: 0px solid #000000;
                padding: 2px 6px 2px 6px;
                border-bottom: none;
            }
            .search_suggest 
			{
                position: absolute; 
				overflow:visible;
				z-index:2;
                right: 577px;
       			cursor: pointer;        
        		width:195px;
                background-color: #FFFFFF;
                text-align: left;
       			border: 1px solid #000000;
				padding:1px 1px 1px 1px ;
        		border-style: hidden solid solid solid;  
      		}
    
              
        </style> 
			<font face="Arial">
				<title>Modificacion de Producto</title>
				
			<style type="text/css">
					#altas span.errorphp{color:red; margin:0px; padding:0px; text-align:center;}
				h1 {
					margin: 0;
					background-color: ;
					color: white;
					font-size: 25px;
					padding: 3px 5px 3px 10px;
					border-bottom-widtd: 1px; 
					border-bottom-style: solid; 
					border-bottom-color: white;
					border-top-widtd: 1px; 
					border-top-style: solid; 
					border-top-color: white;
					background-repeat: repeat;
					font-weight: bolder;
					background-image: nombre.png;
					font-family: Georgia, "Times New Roman", Times, serif;
				} 
			</style>
		<link rel="stylesheet" type="text/css" href="body.css">
	</head>

	<body>
		<?php	
		$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
		$conexion = sqlsrv_connect($server,$infoconexion);
		if (!$conexion)		
		{
			exit("Error de Conexion SQL");
		}
		?>
			<h1 align="center" class="subtituloverde12"> Modificaci&oacute;n de Productos </h1> 
			<form name="altas" class="altas" metdod="post" action="valaltaproducto.php"> 
			     <table widtd="90%"> 
			      	<tr>
						<td colspan="4" align="center">	
						<table>		
						<tr>	
						<td>			
								<div align="left" style="z-index:1; position:absolute; left:7px; top: 98px; width: 412px;">       
									<a class="texto8">Buscar producto: </a>
									<input name="txtSearch" type="text" id="txtSearch" size="20" width="100px" onKeyUp="searchSuggest();" autocomplete="off"/>  
									<input type="hidden" id="busProd" name="busProd" >
									<input type="hidden" id="unidaddPro" name="unidaddPro" >
									<input type="button" value="Seleccionar" id="ad_colin2" name="ad_colin2" onclick="agrega_ProductoARequisicion();" />								
								  <br />
									<div id="search_suggest" style="z-index:2;" > </div>
							  </div>
							  </td>
						  </tr>
						 </table>
						</td>  
					</tr>
					<tr><td>&nbsp;</td></tr>					
					<tr><td>&nbsp;</td></tr>
			         <tr> 
			        <td class="subtituloverde" colspan="4"><div align="center"><span >INFORMACION GENERAL </span></div></td> 
			        </tr>
					<tr> 
			          <td class="texto8" scope="row" ><div align="left">Descripci&oacute;n: * </div></td> 
			          <td ><input tabindex="1" type="text" name="descrip" id= "descrip" size="40" /></td> 
			        </tr> 
			        <tr> 			
			          <td class="texto8" scope="row"><div align="left">Unidad de Medidas:</div></td> 
			          <td ><input tabindex="2" type="text" name="unidad" id= "unidad" size="20" /></td> 
			        </tr> 
			        <tr> 
			          <td class="texto8" scope="row"><div align="left">Equivalencia Almacen:  </div></td> 
			          <td ><input tabindex="3" type="text" name="eqalmacen" id= "eqalmacen" size="10" /></td> 
			        </tr> 
			        <tr> 
			          <td class="texto8" scope="row"><div align="left">Unidad Almacen:  </div></td> 
			          <td ><input tabindex="4" type="text" name="unalmacen" id= "unalmacen" size="20" /></td> 
			        </tr> 
			        <tr> 			
			          <td class="texto8" scope="row"><div align="left">% IVA:</div></td> 
			          <td ><input tabindex="5" type="text" name="iva" id= "iva" size="10" /></td> 
			       </tr> 
			        <tr> 
			          <td class="texto8" scope="row"><div align="left">Cuenta:  </div></td> 
			           <td class="texto8"><select tabindex="6" name="cta" id="cta" style="width:300px" size="1" > 
								<option value="0">---------</option> 
								<?php
								$consulta="select CUENTA,NOMCTA from presupcuentasmayor where estatus<'9000' order by nomcta";
								$dbRes = sqlsrv_query($conexion,$consulta);
								while($row = sqlsrv_fetch_array($dbRes, SQLSRV_FETCH_ASSOC))
								{
									$descrip = $row['NOMCTA'];
									$id = $row['CUENTA'];												
									echo "<option value='$id'>$id $descrip</option>";						
								}
								?>								
			          </select></td> 
						
			        </tr> 
			        <tr>         
			          <td scope="row"><div align="right"> 
			              <input tabindex="7" type="submit" id="Enviar" name="Enviar" value="Agregar" /> 
			          </div></td> 
			          <td width="225" scope="row"><div align="left"> 
			              <input tabindex="8" name="restablecer" type="reset" id="restablecer" value="Restablecer" /> 
			          </div></td> 
			        </tr> 
			     </table> 
			</form> 
		
	</body>
</html>
