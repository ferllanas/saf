<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$nivel = $_REQUEST['nivel'];
//$datos=array();
if ($conexion)
{		
	$consulta = "select * from menummenu where nivel=$nivel and estatus<'90'";
	$R = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$nivel = strtoupper( trim($row['nivel']) );
		$descrip = strtoupper( trim($row['descrip']) );
		$programa = strtoupper( trim($row['programa']) );
		$orden = strtoupper( trim($row['orden']) );
		$estatus = strtoupper( trim($row['estatus']) );
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
</head>
<body>
<h5>&nbsp;</h5>
<form name="form1" method="post" action="php_ajax/valcammenu.php">
	<table width="800" border="1" align="center">
  <tr>
    <th class="subtituloverde" colspan="2" >CAMBIOS DE PROGRAMA</th>
    <th scope="col"><div align="center"></div></th>
  </tr>
  <tr>
    <th width="160" scope="col" class="texto8" align="left">Nivel</th>
	   <td><input class="texto8" tabindex="1" type="text" name="nivel" readonly id="nivel" size="4" value="<?php echo $nivel;?>"/></td>
  </tr> 
  <tr>
    <th scope="row" class="texto8" align="left">Descripci&oacute;n</th>
	   <td><input class="texto8" tabindex="2" type="text" name="descrip" id="descrip" size="100" value="<?php echo $descrip;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Programa</th>
       <td><input class="texto8" tabindex="3" type="text" name="programa" id="programa" size="100" value="<?php echo $programa;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Orden</th>
       <td><input class="texto8" tabindex="4" type="text" name="orden" id="orden" size="4" value="<?php echo $orden;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Estatus</th>
       <td><input class="texto8" tabindex="4" type="text" name="estatus" id="estatus" size="2" value="<?php echo $estatus;?>"/></td>
  </tr>
	<tr>
		<td scope="row" colspan="2" align="center">
 				<input class="texto8" tabindex="12" type="submit" id="Enviar2" name="Enviar" value="Guardar" /> 
				<input class="texto8" tabindex="13" name="restablecer" type="reset" id="restablecer" value="Restablecer" />
		</td>
	</tr>
</table>
</form>
</body>
</html>
