<?php
require_once("../../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
////
require_once("../../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
//print_r($privilegios);
$BorrarPriv=revisaPrivilegiosBorrar($privilegios);
//$VerPDFPriv = revisaPrivilegiosPDF($privilegios);
$EditarPriv=revisaPrivilegiosEditar($privilegios);

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
if ($conexion)
{		
	$consulta = "select * from menummenu where estatus<'90' and nivel%1000=0 order by descrip asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['nivel']= trim($row['nivel']);
		$datos[$i]['descrip']= trim($row['descrip']);
		$datos[$i]['programa']= trim($row['programa']);
		$datos[$i]['orden']= $row['orden'];
		$datos[$i]['feccap']= $row['feccap'];
		$datos[$i]['estatus']= trim($row['estatus']);
		$datos[$i]['id']= trim($row['id']);
		$i++;
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<link type="text/css" href='../../../css/estilos.css' rel="stylesheet">
<script language="javascript" src="javascript/nivelfuncion.js"></script>
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<style type="text/css">
<!--
.Estilo3 {font-size: 14px}
-->
</style>
</head>

<body>
<input type="hidden" value="<?php echo $BorrarPriv;?>" id="BorrarPriv">
<input type="hidden" value="<?php echo $EditarPriv;?>" id="EditarPriv">
<table name="tabla1" width="100%" border="0">
  <tr>
    <th width="945" height="33" scope="row" class="TituloDForma">Administrador de Menus <hr class="hrTitForma"></th>
  </tr>
</table>

</form>
<table width="960" height="33" border="0">
  <tr>
  	<td class="texto8" width="200">Menu Principal:</td>
    <td width="231" height="27" scope="row">
      <select name="nivel" class="texto8" id="nivel" onChange="ffff()">
	<?php
		for($i = 0 ; $i<=count($datos);$i++)
		{
			echo "<option value=".htmlentities(trim($datos[$i]["nivel"])).">".htmlentities(trim($datos[$i]["descrip"]))."</option>\n";
			$nivel=$datos[$i]["nivel"];
		}
	?>
      </select></td>
      <td width="561" class="texto8"><a id="agregProg" name="agregProg" href="#" onclick="alta_menudnivel();" class="texto8">Agregar Submenu</a></td> 
    <td width="561" class="texto8"><a id="agregProg" name="agregProg" href="alta_programa.php?nivel=0000&menuprincipal=si" class="texto8">Agregar Menu</a></td>
  </tr>
</table>

<table name="tabla2" width="100%" border="0" bordercolor="#FFFFFF">
	<thead>
      <tr>
        <th width="43" class="subtituloverde" align="center">Nivel</th>
        <th width="302" class="subtituloverde" align="center">Submenu</th>
        <th width="336" class="subtituloverde" align="center">Ruta de Modulo</th>
        <th width="63" class="subtituloverde" align="center">Orden</th>
        <th width="108" class="subtituloverde" align="center">Estatus</th>
        <th width="36" class="subtituloverde" align="center">&nbsp;</th>
        <th width="36" class="subtituloverde" align="center"></th>
      </tr>
     </thead>
  <tbody id="provsT" name="provsT" >
  </tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
