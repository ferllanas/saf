﻿<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
?>
<?php

$usuario ="";
if(isset($_COOKIE['ID_my_site']))
	$usuario = $_COOKIE['ID_my_site'];


$idpr = strtoupper($_REQUEST['idpr']);
$descrip = strtoupper($_REQUEST['nomprod']);
$unidad = strtoupper($_REQUEST['unidad']);
$eqalmacen = $_REQUEST['eqalmacen'];
$unalmacen = strtoupper($_REQUEST['unalmacen']);
$enalmacen = strtoupper($_REQUEST['almacen']);
$tiporeq = strtoupper($_REQUEST['tiporeq']);
$iva = $_REQUEST['ivapct'];
$cta = "";
$fecha = date('Y-m-d',time());
$hora =  date('H:i',time());

$fails=false;
$datos=array();
if ($descrip == "" |$unidad == "" ) 
{
	exit("No se llenaron campos necesarios");
}
else
{
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if ($conexion)
	{		
		$tsql_callSP ="{call sp_compras_C_dprod(?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
		$params = array(&$idpr,&$descrip,&$unidad,&$eqalmacen,&$unalmacen,&$iva,&$cta,&$enalmacen,&$tiporeq,&$usuario);//Arma parametros de entrada
		$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
		//echo $tsql_callSP;
		//print_r($options);
		$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
		if (! $stmt)
		{
			$datos['error']="1";
			$datos['msg']="El Producto no pudo ser Dado de Alta";
			
		}				
	}
	else
	{ 
		$datos['error']="1";
		$datos['msg']="Falló la conexión de base de datos";
	}
	//sql_close($conexion_db);
}
echo json_encode($datos);
 

?>