<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$id="";
	if(isset($_POST['id_req']));
		$id=$_POST['id_req'];
		
	$usuario = "";//Variable id del usuario
	if(isset($_POST['usuario']));
		$usuario=$_POST['usuario'];
			
	$entregaen="";//variable se entrea el producto en
	if(isset($_POST['entreganen']));
		$entregaen=$_POST['entreganen'];
		
	$observaciones="";//Observaciones
	if(isset($_POST['observaciones']));
		$observaciones=$_POST['observaciones'];
	
	
	$productos="";//variable que almacena la cadena de los productos
	if(isset($_POST['prods']))
		$productos=$_POST['prods'];
		
	//$str_productos = str_replace("\\", "", $str_productos);//reemplaza
	//echo $str_productos;
	//eval( '$productos = array'.$str_productos.';' );//la funcion eval permite obtener un array a partir de la cadena str_productos
	
	
	$fecha="";
	$fails=false;
	$hora="";
	$depto="";
	$nomdepto="";
	$dir=""; 
	$nomdir="";
	
	if ( is_array( $productos ) ) // Pregunta se es una array la variable productos
	{
		//Crear en tabla de requisicion
		list($id,$fecha,$fails,$hora,$depto,$nomdepto,$dir, $nomdir)= fun_creaRequisicion($entregaen,$observaciones,$usuario); //crea nueva requisicion
		if(!$fails)
			for($i=0;$i<count($productos);$i++)
			{
				//Agrega producto a requisicion
				$fails=func_creaProductoDRequisicion($id, 
										$productos[$i]['producto'], 
										$productos[$i]['cantidad'], 
										$productos[$i]['descripcion'],
										(int)$i+1,
										$productos[$i]['uniprod']);
			}
	}
	
	$datos=array();//prepara el aray que regresara
	$datos[0]['id']=trim($id);// regresa el id de la
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	$datos[0]['depto']=trim($depto);
	$datos[0]['nomdepto']=trim($nomdepto);
	$datos[0]['nomdir']=trim($nomdir);
	$datos[0]['dir']=trim($dir);
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_creaRequisicion($entregaen,$observaciones,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";
	$depto = "";
	$nomdepto = "";
	$dir = "";
	$nomdir = "";
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_mrequisi(?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$entregaen,&$observaciones,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
			$depto = $row['depto'];
			$nomdepto = $row['nomdepto'];
			$dir = $row['dir'];
			$nomdir = $row['nomdir'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$fails,$hora,$depto,$nomdepto,$dir, $nomdir);
}

//Funcion para registrar el p�roductoa  la requisicion
function func_creaProductoDRequisicion($id_Requi, $producto, $cantida, $descripcion,$mov, $unidad)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$id="";//Store proc regresa id de la nueva requisicion
	$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_drequisi(?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$productos[$i]['producto'], $productos[$i]['cantidad'], $productos[$i]['descripcion'],'0',$productos[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = array(&$id_Requi, &$producto, &$descripcion, &$cantida, &$mov, &$unidad);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>