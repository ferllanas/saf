<?php
	require_once("../../../../connections/dbconexion.php");
	require_once("../../../../Administracion/globalfuncions.php");
////
validaSession(getcwd());
$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
$editar=revisaPrivilegioseditar($privilegios)? 1:0;
//print_r($privilegios);
///
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	$datos=array();
	//$datos=$_REQUEST;
	//print_r($_REQUEST[]);
	
	if(!isset($_REQUEST['tipobusqueda']) && $_REQUEST['tipobusqueda']!='all')
	{
		if(!isset($_REQUEST['tipobusqueda']) && !isset($_REQUEST['nomprod']))
		{
			$datos[0]['error']="1";
			$datos[0]['string']="No se han recibido ningun dato para hacer la busqueda. 1";
		}
		else
		{
			
			if(!isset($_REQUEST['nomprod']) && $_REQUEST['tipobusqueda']!='all' )
			{
				$datos[0]['error']="1";
				$datos[0]['string']="No se ha ingresado el numero de nomprod";
			}
			else
			{
				$datos[0]['error']="1";
				$datos[0]['string']="No se ha especificado el tipo de busqueda 2";
			}
		}
	}
	else
	{
		
		if($conexion)
		{
			$tipobusqueda=$_REQUEST['tipobusqueda'];
			$nomprod=$_REQUEST['nomprod'];
			$comando="";
			if($tipobusqueda=="all")
			{
				$comando="select a.*, b.tiporeq from compradproductos a LEFT JOIN compradprod_tiporeq b ON b.producto=a.prod where  a.estatus=0 AND b.estatus=0 ORDER BY prod ASC";
			}
			else
			{
				$comando="select a.*, b.tiporeq from compradproductos a LEFT JOIN compradprod_tiporeq b ON b.producto=a.prod where nomprod like '%$nomprod%' and a.estatus=0 AND b.estatus=0 ORDER BY prod";
			}
			$i=0;
			
			//echo $comando;
			$getProducts = sqlsrv_query( $conexion_srv,$comando);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$datos[$i]['prod'] = trim($row['prod']);
					$datos[$i]['nomprod'] = html_entity_decode(trim($row['nomprod']));
					$datos[$i]['unidad'] = html_entity_decode(trim($row['unidad']));
					$datos[$i]['eqalmacen'] = html_entity_decode(trim($row['eqalmacen']));
					$datos[$i]['unalmacen'] = html_entity_decode(trim($row['unalmacen']));
					$datos[$i]['almacen'] = html_entity_decode(trim($row['almacen']));
					$datos[$i]['ivapct'] = trim($row['ivapct']);	
					$datos[$i]['ctapresup'] = trim($row['ctapresup']);
					$datos[$i]['tiporeq'] = trim($row['tiporeq']);
					$datos[$i]['Editar'] = $editar;
					$i++;
				}
			}
			/*if($i==0)
			{
				$datos[0]['error']="1";
				$datos[0]['string']=$comando;
			}*/
			sqlsrv_free_stmt( $getProducts );
		}
		else
		{
				$datos[0]['error']="1";
				$datos[0]['string']="no hay conexion";
		}
	}
	echo json_encode($datos);
?>