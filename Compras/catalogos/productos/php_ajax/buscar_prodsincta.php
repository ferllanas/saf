<?php
	require_once("../../../../connections/dbconexion.php");
	require_once("../../../../Administracion/globalfuncions.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	
	////
validaSession(getcwd());
$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
$editar=revisaPrivilegioseditar($privilegios)? 1:0;
////
	
	$datos=array();
	
	if(!isset($_REQUEST['tipobusqueda']) || !isset($_REQUEST['cuenta']))
	{
		if(!isset($_REQUEST['tipobusqueda']) && !isset($_REQUEST['cuenta']))
		{
			$datos[0]['error']="1";
			$datos[0]['string']="No se han recibido ningun dato para hacer la busqueda.";
		}
		else
		{
			if(!isset($_REQUEST['cuenta']))
			{
				$datos[0]['error']="1";
				$datos[0]['string']="No se ha especificado el tipo de busqueda";
			}
			else
			{
				$datos[0]['error']="1";
				$datos[0]['string']="No se ha ingresado el numero de cuenta";
			}
		}
	}
	else
	{
		
		if($conexion)
		{
			$tipobusqueda=$_REQUEST['tipobusqueda'];
			$cuenta=$_REQUEST['cuenta'];
			$comando="";
			if($tipobusqueda=="prodSinCuenta")
			{
				$comando="select prod,nomprod,unidad,ivapct,ctapresup from compradproductos where (ctapresup='' or ctapresup='0' or ctapresup is null) and estatus=0 ORDER BY prod";
			}
			else
			{
				$comando="select prod,nomprod,unidad,ivapct,ctapresup from compradproductos where ctapresup='$cuenta' and estatus=0 ORDER BY prod";
			}
			
			
			//echo $comando;
			$getProducts = sqlsrv_query( $conexion_srv,$comando);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
			}
			else
			{
				$resoponsecode="Cantidad rows=".count($getProducts);
				$i=0;
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$datos[$i]['prod'] = trim($row['prod']);
					$datos[$i]['nomprod'] = utf8_encode(trim($row['nomprod']));
					$datos[$i]['unidad'] = utf8_encode(trim($row['unidad']));
					$datos[$i]['ivapct'] = trim($row['ivapct']);	
					$datos[$i]['ctapresup'] = trim($row['ctapresup']);
					$datos[$i]['Editar'] = $editar;
					$i++;
				}
			}
			sqlsrv_free_stmt( $getProducts );
		}
		else
		{
				$datos[0]['error']="1";
				$datos[0]['string']="no hay conexion";
		}
	}
	echo json_encode($datos);
?>