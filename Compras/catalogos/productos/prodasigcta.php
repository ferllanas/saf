<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

////
validaSession(getcwd());
$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
////

?>
<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">-->
<html>
<head>
<title>PRODUCTOS- CUENTA PRESUPUESTAL</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo getDirAtras(getcwd());?>x-jquery-plugins/functiones_Globales.js"></script>	
<script language="javascript" src="javascript/prodasigcta.js"></script>

<link type="text/css" href="<?php echo getDirAtras(getcwd());?>css/reveal.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>javascript_globalfunc/jquery.reveal.js"></script>
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo getDirAtras(getcwd());?>calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar-setup.js"></script>
<!-- Calendario -->

<?php  if($dependencia=="fomerrey") { ?>
		<link type="text/css" href="<?php echo getDirAtras(getcwd());?>css/estilos.css" rel="stylesheet">
		<!--<link href="<?php echo getDirAtras(getcwd());?>include/estilos.css" rel="stylesheet" type="text/css">-->
        <!--<script language="javascript" src="<?php echo getDirAtras(getcwd());?>prototype/prototype.js"></script>-->
        <!--<script language="javascript" src="javascript/funciones.js"></script>-->
        
 <?php  } ?>
 <!-- Menu Desplegable -->
<?php  if($dependencia!="fomerrey") { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/component.css" />
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/modernizr.custom.js"></script>
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/jquery.dlmenu.js"></script>
    <script>
     $(function() {
            $( '#dl-menu' ).dlmenu({
                animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
            });
        });
    </script>
    
<?php } ?>
<!-- Menu Desplegable -->
</head>
<?php  if($dependencia!="fomerrey") include_once( getDirAtras(getcwd())."indexMenu.php");?>
<body >
<form id="forma">
<table width="100%" border="0">
  	<tr >
  		<td colspan="2" width="90%" align="center" class="subtituloverde12">PRODUCTOS- CUENTA PRESUPUESTAL</td>		
	</tr>
	<tr>		
	  <td width="30%" >
        <tr>
			<td class="texto8"><input type="radio" name="tipobusqueda" value="chkcta" > Cuenta Presupuestal: <input type="text" id="cuenta"  name="cuenta" value="" > </td>			
		</tr>
		<tr>
			<td class="texto8"><input type="radio" name="tipobusqueda" value="prodSinCuenta" > Productos sin cuenta Presupuestal </td>
			<td><input name="button" type="button" onClick="buscar_prodsincta();" value="Buscar"></td>   
		</tr>
		</td>
	</tr>
</table>
</form>
<table  width="100%">
    <thead >
        <tr class="subtituloverde">
            <th>Producto</th>
            <th>Descripci&oacute;n</th>
            <th>Unidad</th>
            <th>IVA</th>
            <th>Cuenta</th>
            <th>Acciones</th>
        </tr>
    </thead>	
	<tbody id="busqueda_ef" class="resultadobusqueda"></tbody> 
</table>

<div id="myModal" class="reveal-modal medium">
	<div class="mywrapper">
    	<div id="modalInfo"></div>
		 <div class="divLogoHeader">
        	<img class="logoheader" src="<?php echo getDirAtras(getcwd());?>imagenes/xlogo_empresa.jpg" onClick="javascript:location.href='<?php echo getDirAtras(getcwd());?>inicio.php'"  />
        </div>
		<div id="myModalId">
        	<form name="modalFormPass" id="modalFormPass" method="post">
            	<div class="modalItem">
                	<span class="label">Modificacion de Productos</span>
                    <table>
                    	<tr>
                        	<td>Descripci&oacute;n:*</td>
                            <td><input tabindex="5" type="text" readonly="true" name="descrip" id= "descrip" value="<?php echo $nomprod; ?>" size="40" /></td>
                        </tr>
                        <tr>
                        	<td>Cuenta: 
							<?php
								$consulta="select CUENTA,NOMCTA from presupcuentasmayor where estatus<'9000' order by nomcta";
								
								?>
								</td>
                            <td><select tabindex="1" name="cta" id="cta" style="width:200px" size="1" > 
								<option value="0">---------</option> 
								<?php
								$dbRes = sqlsrv_query($conexion,$consulta);
								while($row = sqlsrv_fetch_array($dbRes, SQLSRV_FETCH_ASSOC))
								{
									$descrip = utf8_encode(trim($row['NOMCTA']));
									$id = $row['CUENTA'];												
									echo "<option value='$id'>$id $descrip</option>";						
								}
								?>								
			          </select></td>
                        </tr>
                    </table>
					<div class="clear"></div>
                    <div class="modalItemRight"><input tabindex="6" type="hidden" readonly="true" name="prod" id= "prod" value="<?php echo $prod; ?>" size="40" /></div>
                   
                    <div class="clear"></div>
                </div>
                
                	<input type="button" class="btsubmit" id="btsubmit" value="Enviar">
                	<input type="button" class="btcancel" id="btcancel" value="Cancelar">
            </form>
    	</div>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</div>
</body>
</html>
