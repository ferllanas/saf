//Elimina una menu
$(document).ready(function(){
	//Se le da acci�n al formulario
	
	$('#btsubmit').click(function(e){
		checkSubmit();
		console.log("Guardar Producto");
		e.preventDefault();
		//Se verifica si el usuario a ingresado algun valor
		var $cuenta =  $('#cta').val();
		var $producto = $('#prod').val();
		var $descrip = $('#descrip').val();
		
		
		console.log( $cuenta +','+ $producto);
		if($producto.length == 0){
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Regresa he intenta de nuevo. No se detecto el id del producto.</div>')
				.show(500);
				return false;
		}
		
		if($cuenta.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor selecciona una cuenta para el producto.</div>')
				.show(500);
				return false;
		}		
		
		if($descrip.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor de ingresar el nombre del producto.</div>')
				.show(500);
				return false;
		}		
		
		var $datos={
			prod: $producto,
			cta: $cuenta,
			descrip: $descrip
		}
		
			$.ajax({
				type: 'post',
				data: $datos,
				url: 'valmodproducto.php',
				dataType: 'json',
				beforeSend: function(){
					$('input',this).each(function(){
						$(this).attr('disabled','disabled');
					});
				},
				error: function(xhr, status, error){
				console.log(xhr.responseText);
					$('#modalInfo')
						.hide()
						.html('<div class="infoModal infoError"><b>Warning</b><br>Something went wrong, please try again later.</div>')
						.show(500);
						habilita_btsubmit();
				},
				success: function($msg){
					if($msg.error == 1){
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoError"><b>Precauci&oacute;n</b><br>'+$msg.msg+'</div>')
							.show(500);
							habilita_btsubmit();
					}else{
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoCorrecto"><b>Success!</b><br>El Producto ha sido actualizado con exito.</div>')
							.show(500);
						//Se cierra el modal
						habilita_btsubmit();
						buscar_prodsincta();
						setTimeout("cerrarModal()",1300);
						
					}
				}
			});
		
	});
});


function buscar_prodsincta()
{	
	//alert("Entra");
	var tabla=document.getElementById('busqueda_ef');		
	
	var myRadio = $('input[name=tipobusqueda]');
	var $tipobusqueda = myRadio.filter(':checked').val();

	
	console.log($tipobusqueda);
	if($tipobusqueda==undefined || $tipobusqueda.length=="undefined"  || $tipobusqueda.length<=0)
	{
		alert("Favor de seleccionar el tipo de busqueda.");
		return false;
	}
	var $cuenta=$('#cuenta').val();		
	
	var datas={
		tipobusqueda: $tipobusqueda,
		cuenta: $cuenta
		
	};
	console.log(datas);
	$.ajax({
		   type: 		'post',
		   cache: 		false,
		   	url:		'php_ajax/buscar_prodsincta.php',
			dataType: 	"json",
			data:		datas,
			beforeSend: function () {
				$("#resultado").html("Procesando, espere por favor...");
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr);
				console.log(thrownError);
				console.log(ajaxOptions);
			  },
			 success:  function (response) 
			 {
				 console.log(response);
				 myArray=response;
								$('#busqueda_ef').empty();								
								if(myArray.length>0)
								{
									
											
									for(var i = 0; i <myArray.length; i++)
									{
										//alert(i);
										var newrow = tabla.insertRow(i);
										newrow.className ="d"+(i%2);
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.width="5%";
										newcell.className ="texto8";
										newcell.innerHTML =  myArray[i].prod;
										newcell.align ="center";
										
										var newcell1 = newrow.insertCell(1);
										newcell1.width="30%";
										newcell1.className ="texto8";
										newcell1.innerHTML =  myArray[i].nomprod;
										newcell1.align ="left";  
										
										var newcell2 = newrow.insertCell(2);
										newcell2.width="5%";
										newcell2.className ="texto8";
										//alert( myArray[i].titulo);
										newcell2.innerHTML = myArray[i].unidad;
										newcell2.align ="center";  
										
										var newcell3 = newrow.insertCell(3);
										newcell3.width="5%";
										newcell3.className ="texto8";
										newcell3.innerHTML =myArray[i].ivapct;
										newcell3.align ="left"; 
										
										var newcell4 = newrow.insertCell(4);
										newcell4.width="5%";
										newcell4.className ="texto8";
										newcell4.innerHTML =myArray[i].ctapresup;
										newcell4.align ="left"; 
										
										var newcell5 = newrow.insertCell(5);
										newcell5.width="5%";
										newcell5.className ="texto8";
										newcell5.align ="center"; 
										
										//console.log("!"+myArray[i].ctapresup+"!");
										if(myArray[i].ctapresup=="")
										{
											var SD=myArray[i].nomprod.replace(/"/g,'&COLLAS');
											SD=SD.replace(/'/g,'&COLLA');
											var dato="\""+ SD+ "\"";
											
											var btnagregar="onClick='javascript:popUp("+ myArray[i].prod +","+ dato+")'";
											newcell5.innerHTML ="<input type='image' src='../../../imagenes/agregar.jpg'  value='"+ myArray[i].prod +"'  "+ btnagregar +";>";//popUp(modificaprod.php?id="+ myArray[i].prod+")
										}
										else
										{
											var dato='';
											if(myArray[i].nomprod!=null || myArray[i].nomprod!="null")
												var dato="'"+myArray[i].nomprod+"'";
											console.log(dato);
											var btnagregar="onClick='javascript:popUp("+ myArray[i].prod +","+ dato+")'";
											if(myArray[i].Editar==1)
												newcell5.innerHTML ="<input type='image' src='../../../imagenes/edit.png'  value='agregar'  "+ myArray[i].prod +"'  "+ btnagregar +">";
										}
										
									}
								}
								else
								{
									alert("No existen resultados para estos criterios.");
								}
					  }
				});
}



function popUp(idprd,nomprod) 
{
	console.log("Entro");
	var SA=nomprod.replace(/&COLLAS/g,'"');
	SA=SA.replace(/&COLLA/g,"'");
	$('#descrip').val(SA);
	$('#prod').val(idprd);
	$('#myModal').reveal();
	$('#modalInfo')
				.hide()
				.html('');
				
	
}

/*
function activatxtcta()
{ 
   var chkcta=$('#chkcta').is('checked');	
   alert(chkcta);
   if(chkcta)
   {	  
	   $('#cuenta').show();
   }
   else
   {	   
		$('#cuenta').hide();
   }
} */
function Busca(objeto,e)
{
	tecla=(document.all) ? e.keyCode : e.whish;	
	if(tecla==38 || tecla==64)
	{			
		alert(tecla);return false;
	}		
	else
	{		
		return true;
	}
}

function llenar_detalleArticulo(id_articulo)
{
	//alert('php_ajax/ver_detalleArticulo.php?idef='+id_articulo);
	new Ajax.Request('php_ajax/ver_detalleArticulo.php?idef='+id_articulo,
					 {onSuccess : function(resp) 
					 {
						if( resp.responseText ) 
						{
							//got an array of suggestions.
							//alert("hay tomala papa"+resp.responseText);
							var myArray = eval(resp.responseText);
							if(myArray.length>0)
							{
								$('#titulo').innerHTML 		 = myArray[0].titulo;
								$('#Folio').innerHTML		= myArray[0].folio;
								$('#introduccion').innerHTML  = myArray[0].introduccion;
								$('#contenido').innerHTML  	= myArray[0].contenido;
								
								var tabla=$('#tfotos');
								/*var newrow2 = tabla.insertRow(i+2);
								newrow2.className ="texto8";
								newrow2.scope="row";
								newrow2.width="100%";
								newrow2.height="30%";
								newrow2.align="left";
								*/
								var newrow = tabla.insertRow(i+1);
								newrow.className ="texto8";
								newrow.scope="row";
								newrow.width="100%";
								newrow.height="70%";
								newrow.align="left";
								
								for (var i in  myArray[0].multimedias)
								{
									if(myArray[0].multimedias[i].id!=null)
									{
										/*var newcell2 = newrow2.insertCell(0);
										newcell2.align ="left";  
										newcell2.width="25%";
										newcell2.innerHTML = myArray[0].multimedias[i].descripcion;
										*/
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.align ="left";
										newcell.style.width="25%";
										myArray[0].multimedias[i].path = myArray[0].multimedias[i].path.substring(2,myArray[0].multimedias[i].path.length);
										myArray[0].multimedias[i].path = "../fome"+myArray[0].multimedias[i].path;
										
										if(i==0)
										{
											$('#imagen_Principal').src=myArray[0].multimedias[i].path;
										}
										
										newcell.innerHTML = "<img src='"+myArray[0].multimedias[i].path+"' style='width:50;height=50;' onClick='ChangeImagePrincipal(this.src)'>	";
									}
								}
							}
						}
					}
				});
}

function stripHtml(s) { 
    s= s.replace(/\\&/g, '&amp;').replace(/\\</g, '&lt;').replace(/\\>/g, '&gt;').replace(/\\t/g, '&nbsp;&nbsp;&nbsp;').replace(/\\n/g, '<br />'); 
	s.replace(/\\&/g, '&amp;');
	return s;
} 