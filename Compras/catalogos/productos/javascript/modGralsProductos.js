//Elimina una menu
$(document).ready(function(){
	//Se le da acci�n al formulario
	
	$('#btsubmit').click(function(e){
		checkSubmit();
		console.log("Guardar Producto");
		e.preventDefault();
		//Se verifica si el usuario a ingresado algun valor
		$idprd=$('#idprd').val();
		$nomprod=$('#descrip').val();
		$unidad=$('#unidad').val();
		$eqalmacen=$('#eqalmacen').val();
		$unalmacen=$('#unalmacen').val();
		$ivapct=$('#iva').val();
		$almacen=$( "#enalmacen option:selected" ).val();
		$tiporeq=$( "#tiporeq option:selected" ).val();
		
		
		//console.log( $cuenta +','+ $producto);
		if($nomprod.length == 0){
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Regresa he intenta de nuevo. No se detecto el id del producto.</div>')
				.show(500);
				return false;
		}
		
		if($unidad.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor ingrese la unidad del producto.</div>')
				.show(500);
				return false;
		}		
		
		if($eqalmacen.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor de ingrese equivalencia de almacen.</div>')
				.show(500);
				return false;
		}		
		
		if($unalmacen.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor de ingrese unidad de almacen.</div>')
				.show(500);
				return false;
		}		
		
		if($ivapct.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor de ingrese iva del producto.</div>')
				.show(500);
				return false;
		}		
		
		var $datos={
			idpr:$idprd,
			nomprod:$nomprod,
			unidad:$unidad,
			eqalmacen:$eqalmacen,
			unalmacen:$unalmacen,
			ivapct:$ivapct,
			almacen:$almacen,
			tiporeq:$tiporeq
		}
		
			$.ajax({
				type: 'post',
				data: $datos,
				url: 'sp_compras_C_dprod.php',
				dataType: 'json',
				beforeSend: function(){
					$('input',this).each(function(){
						$(this).attr('disabled','disabled');
					});
				},
				error: function(xhr, status, error){
				console.log(xhr.responseText);
					$('#modalInfo')
						.hide()
						.html('<div class="infoModal infoError"><b>Warning</b><br>Something went wrong, please try again later.</div>')
						.show(500);
						habilita_btsubmit();
				},
				success: function($msg){
					if($msg.error == 1){
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoError"><b>Precauci&oacute;n</b><br>'+$msg.msg+'</div>')
							.show(500);
							habilita_btsubmit();
					}else{
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoCorrecto"><b>Success!</b><br>El Producto ha sido actualizado con exito.</div>')
							.show(500);
						//Se cierra el modal
						habilita_btsubmit();
						buscar_prodsincta();
						setTimeout("cerrarModal()",1300);
						
					}
				}
			});
		
	});
});


function buscar_prodsincta()
{	
	//alert("Entra");
	var tabla=document.getElementById('busqueda_ef');		
	
	var myRadio = $('input[name=tipobusqueda]');
	var $tipobusqueda = myRadio.filter(':checked').val();

	
	if($tipobusqueda==undefined || $tipobusqueda.length=="undefined"  || $tipobusqueda.length<=0)
	{
		alert("Favor de seleccionar el tipo de busqueda.");
		return false;
	}
	var $cuenta=$('#cuenta').val();		
	
	//console.log($tipobusqueda);
	//console.log($cuenta);
	var datas={
		tipobusqueda: $tipobusqueda,
		nomprod: $cuenta
		
	};
	
	$.ajax({
		   type: 		'post',
		   cache: 		false,
		   	url:		'php_ajax/modGralsProductos_buscar.php',
			dataType: 	"json",
			data:		datas,
			beforeSend: function () {
				$("#resultado").html("Procesando, espere por favor...");
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr);
				console.log(thrownError);
				console.log(ajaxOptions);
			  },
			 success:  function (response) 
			 {
					console.log(response);
				 	myArray=response;
								$('#busqueda_ef').empty();								
								if(myArray.length>0)
								{
									
											
									for(var i = 0; i <myArray.length; i++)
									{
										//alert(i);
										var newrow = tabla.insertRow(i);
										newrow.className ="d"+(i%2);
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.width="5%";
										newcell.className ="texto8";
										newcell.innerHTML =  myArray[i].prod;
										newcell.align ="center";
										
										var newcell1 = newrow.insertCell(1);
										newcell1.width="30%";
										newcell1.className ="texto8";
										newcell1.innerHTML =  myArray[i].nomprod;
										newcell1.align ="left";  
										
										var newcell2 = newrow.insertCell(2);
										newcell2.width="5%";
										newcell2.className ="texto8";
										//alert( myArray[i].titulo);
										newcell2.innerHTML = myArray[i].unidad;
										newcell2.align ="center";  
										
										var newcell3 = newrow.insertCell(3);
										newcell3.width="5%";
										newcell3.className ="texto8";
										newcell3.innerHTML =myArray[i].ivapct;
										newcell3.align ="left"; 
										
										var newcell4 = newrow.insertCell(4);
										newcell4.width="5%";
										newcell4.className ="texto8";
										newcell4.innerHTML =myArray[i].ctapresup;
										newcell4.align ="left"; 
										
										var newcell5 = newrow.insertCell(5);
										newcell5.width="5%";
										newcell5.className ="texto8";
										newcell5.align ="center"; 
										
										//console.log("!"+myArray[i].ctapresup+"!");
										
										var dato='';
										if(myArray[i].nomprod!=null || myArray[i].nomprod!="null")
											var dato='"'+myArray[i].nomprod+'"';
											
										var dato2='';
										if(myArray[i].unidad!=null || myArray[i].unidad!="null")
											var dato2='"'+myArray[i].unidad+'"';
											
										var dato3='';
										if(myArray[i].eqalmacen!=null || myArray[i].eqalmacen!="null")
											var dato3='"'+myArray[i].eqalmacen+'"';
											
										var dato4='';
										if(myArray[i].unalmacen!=null || myArray[i].unalmacen!="null")
											var dato4='"'+myArray[i].unalmacen+'"';
											
										var dato5='';
										if(myArray[i].almacen!=null || myArray[i].almacen!="null")
											var dato5='"'+myArray[i].almacen+'"';
										
										var dato6='';
										if(myArray[i].ivapct!=null || myArray[i].ivapct!="null")
											var dato6='"'+myArray[i].ivapct+'"';
											
										var dato7='';
										if(myArray[i].tiporeq!=null || myArray[i].tiporeq!="null")
											var dato7='"'+myArray[i].tiporeq+'"';
										
										var btnagregar="onClick='javascript:popUp("+ myArray[i].prod +","+ dato+","+ 
																							 dato2+","+ 
																							 dato3+","+ 
																							 dato4+
																							 ","+ dato5+
																							 ","+ dato6+
																							 ","+ dato7+")'";
										if(myArray[i].Editar==1)
											newcell5.innerHTML ="<input type='image' src='../../../imagenes/edit.png'  value='agregar'  "+ myArray[i].prod +"'  "+ btnagregar +">";
										
									}
								}
								else
								{
									alert("No existen resultados para estos criterios.");
								}
					  }
				});
}



function popUp(idprd,nomprod,unidad, eqalmacen,unalmacen, almacen, ivapct, tiporeq) 
{
	console.log("Entro");
	$('#descrip').val(nomprod);
	$('#prod').val(idprd);
$('#myModal').reveal();
	$('#modalInfo')
				.hide()
				.html('');
	
	console.log("idprd:"+idprd);
	$('#idprd').val(idprd);
	$('#descrip').val(nomprod);
	$('#unidad').val(unidad);
	$('#eqalmacen').val(eqalmacen);
	$('#unalmacen').val(unalmacen);
	$('#iva').val(ivapct);
	$('#enalmacen option').filter(function() { 
			return ($(this).text() == almacen); //To select Blue
		}).prop('selected', true); 
	
	$('#tiporeq option').filter(function() { 
			return ($(this).text() == tiporeq); //To select Blue
		}).prop('selected', true); 
	
	//$('#enalmacen').val(ivapct);
}

/*
function activatxtcta()
{ 
   var chkcta=$('#chkcta').is('checked');	
   alert(chkcta);
   if(chkcta)
   {	  
	   $('#cuenta').show();
   }
   else
   {	   
		$('#cuenta').hide();
   }
} */
function Busca(objeto,e)
{
	tecla=(document.all) ? e.keyCode : e.whish;	
	if(tecla==38 || tecla==64)
	{			
		alert(tecla);return false;
	}		
	else
	{		
		return true;
	}
}

function llenar_detalleArticulo(id_articulo)
{
	//alert('php_ajax/ver_detalleArticulo.php?idef='+id_articulo);
	new Ajax.Request('php_ajax/ver_detalleArticulo.php?idef='+id_articulo,
					 {onSuccess : function(resp) 
					 {
						if( resp.responseText ) 
						{
							//got an array of suggestions.
							//alert("hay tomala papa"+resp.responseText);
							var myArray = eval(resp.responseText);
							if(myArray.length>0)
							{
								$('#titulo').innerHTML 		 = myArray[0].titulo;
								$('#Folio').innerHTML		= myArray[0].folio;
								$('#introduccion').innerHTML  = myArray[0].introduccion;
								$('#contenido').innerHTML  	= myArray[0].contenido;
								
								var tabla=$('#tfotos');
								/*var newrow2 = tabla.insertRow(i+2);
								newrow2.className ="texto8";
								newrow2.scope="row";
								newrow2.width="100%";
								newrow2.height="30%";
								newrow2.align="left";
								*/
								var newrow = tabla.insertRow(i+1);
								newrow.className ="texto8";
								newrow.scope="row";
								newrow.width="100%";
								newrow.height="70%";
								newrow.align="left";
								
								for (var i in  myArray[0].multimedias)
								{
									if(myArray[0].multimedias[i].id!=null)
									{
										/*var newcell2 = newrow2.insertCell(0);
										newcell2.align ="left";  
										newcell2.width="25%";
										newcell2.innerHTML = myArray[0].multimedias[i].descripcion;
										*/
										
										var newcell = newrow.insertCell(0); //insert new cell to row
										newcell.align ="left";
										newcell.style.width="25%";
										myArray[0].multimedias[i].path = myArray[0].multimedias[i].path.substring(2,myArray[0].multimedias[i].path.length);
										myArray[0].multimedias[i].path = "../fome"+myArray[0].multimedias[i].path;
										
										if(i==0)
										{
											//alert(myArray[0].multimedias[i].path);
											$('#imagen_Principal').src=myArray[0].multimedias[i].path;
										}
										
										newcell.innerHTML = "<img src='"+myArray[0].multimedias[i].path+"' style='width:50;height=50;' onClick='ChangeImagePrincipal(this.src)'>	";
									}
								}
							}
						}
					}
				});
}

function stripHtml(s) { 
    s= s.replace(/\\&/g, '&amp;').replace(/\\</g, '&lt;').replace(/\\>/g, '&gt;').replace(/\\t/g, '&nbsp;&nbsp;&nbsp;').replace(/\\n/g, '<br />'); 
	s.replace(/\\&/g, '&amp;');
	return s;
} 