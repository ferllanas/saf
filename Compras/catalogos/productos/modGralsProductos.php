<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
////
validaSession(getcwd());
$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

//$privilegios= get_Privilegios($usuario, $idPantalla);
//$editar=revisaPrivilegioseditar($privilegios)==0? 1:0;
///

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

function fun_ObtenertipoRequisiciones()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="SELECT tiporeq, nomtiporeq FROM compramtiporeq";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		// die( print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		
		// Retrieve and display the first result. 
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$datos[$i]['tiporeq'] = $row['tiporeq'];
			$datos[$i]['nomtiporeq'] = $row['nomtiporeq'];
			$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $datos;
}
?>
<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">-->
<html>
<head>
<title>MODIFICACI&Oacute;N DE GENERALES DE PRODUCTOS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo getDirAtras(getcwd());?>x-jquery-plugins/functiones_Globales.js"></script>	
<script language="javascript" src="javascript/modGralsProductos.js"></script>

<link type="text/css" href="<?php echo getDirAtras(getcwd());?>css/reveal.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>javascript_globalfunc/jquery.reveal.js"></script>
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo getDirAtras(getcwd());?>calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar-setup.js"></script>
<!-- Calendario -->

<?php  if($dependencia=="fomerrey") { ?>
		<link type="text/css" href="<?php echo getDirAtras(getcwd());?>css/estilos.css" rel="stylesheet">
		<!--<link href="<?php echo getDirAtras(getcwd());?>include/estilos.css" rel="stylesheet" type="text/css">-->
        <!--<script language="javascript" src="<?php echo getDirAtras(getcwd());?>prototype/prototype.js"></script>-->
        <!--<script language="javascript" src="javascript/funciones.js"></script>-->
        
 <?php  } ?>
 <!-- Menu Desplegable -->
<?php  if($dependencia!="fomerrey") { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/component.css" />
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/modernizr.custom.js"></script>
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/jquery.dlmenu.js"></script>
    <script>
     $(function() {
            $( '#dl-menu' ).dlmenu({
                animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
            });
        });
    </script>
    
<?php } ?>
<!-- Menu Desplegable -->
</head>
<?php  if($dependencia!="fomerrey") include_once( getDirAtras(getcwd())."indexMenu.php");?>
<body >
<form id="forma">
<table width="100%" border="0">
  	<tr >
  		<td colspan="2" width="90%" align="center" class="subtituloverde12">MODIFICACI&Oacute;N DE GENERALES DE PRODUCTOS</td>		
	</tr>
	<tr>		
	  <td width="30%" >
        <tr>
			<td class="texto8"><input type="radio" name="tipobusqueda" value="nomprod" >Nombre de Producto: <input type="text" id="cuenta"  name="cuenta" value="" > </td>			
		</tr>
		<tr>
			<td class="texto8"><input type="radio" name="tipobusqueda" value="all" onChange="buscar_prodsincta();" > Mostrar todos </td>
			<td><input name="button" type="button" onClick="buscar_prodsincta();" value="Buscar"></td>   
		</tr>
		</td>
	</tr>
</table>
</form>
<table  width="100%">
    <thead >
        <tr class="subtituloverde">
            <th>Producto</th>
            <th>Descripci&oacute;n</th>
            <th>Unidad</th>
            <th>IVA</th>
            <th>Cuenta</th>
            <th>Acciones</th>
        </tr>
    </thead>	
	<tbody id="busqueda_ef" class="resultadobusqueda"></tbody> 
</table>

<div id="myModal" class="reveal-modal medium">
	<div class="mywrapper">
    	<div id="modalInfo"></div>
		 <div class="divLogoHeader">
        	<img class="logoheader" src="<?php echo getDirAtras(getcwd());?>imagenes/xlogo_empresa.jpg" onClick="javascript:location.href='<?php echo getDirAtras(getcwd());?>inicio.php'"  />
        </div>
		<div id="myModalId">
        	<form name="modalFormPass" id="modalFormPass" method="post">
            	<div class="modalItem">
                    <table widtd="90%"> 
			       <tr> 
			        <td class="subtituloverde" colspan="4"><div align="center"><span >Edici&oacute; de Productos </span></div></td> 
			        </tr> 
			        <tr> 
			          <td class="texto8" scope="row" ><div align="left">Descripci&oacute;n: * </div></td> 
			          <td ><input tabindex="1" type="text" name="descrip" id= "descrip" title="Descripci�n del Producto" size="40" />
                      <input tabindex="1" type="hidden" name="idprd" id= "idprd" title="" size="40" value="0" /></td>
			        </tr> 
			        <tr> 			
			          <td class="texto8" scope="row"><div align="left">Unidad de Medidas:</div></td> 
			          <td ><input tabindex="2" type="text" name="unidad" id= "unidad" title="Tipo de unidades del Producto (PIEZA, MTS. ETC...)." size="20" /></td> 
			        </tr> 
			        <tr> 
			          <td class="texto8" scope="row"><div align="left">Equivalencia Almacen:  </div></td> 
			          <td ><input tabindex="3" type="text" name="eqalmacen" id= "eqalmacen" title="Tipo de unidades en almacen (PIEZA, MTS. ETC...)." size="10" /></td> 
			        </tr> 
			        <tr> 
			          <td class="texto8" scope="row"><div align="left">Unidad Almacen:  </div></td> 
			          <td ><input tabindex="4" type="text" name="unalmacen" id= "unalmacen" title="Equivalencia en almacen de producto, cantidad de unidades(es numerico)." size="20" /></td> 
			        </tr> 
			        <tr> 			
			          <td class="texto8" scope="row"><div align="left">% IVA:</div></td> 
			          <td ><input tabindex="5" type="text" name="iva" id= "iva" title="Iva del Producto (es n�merico)." size="10" /></td> 
			       </tr> 			        
                    <tr>
                    	<td class="texto8">Para Almacen:</td>
                        <td><select id="enalmacen" name="enalmacen" title="Producto puede almacenarse en almacen."  >
                        		<option value="SI">SI</option>
                        		<option value="NO">NO</option>
                             </select>
                         </td>
                    </tr>
                      <tr>
                    	<td class="texto8">Tipo de Requisici&oacute;n:</td>
                        <td><select  class="texto8" id="tiporeq" name="tiporeq" style="width:100%" title="El tipo de requisi�n.">
                          <?php
												$tiposDRequi=array();
												$tiposDRequi = fun_ObtenertipoRequisiciones();
												//$datos[$i]['tiporeq'] = $row['tiporeq'];
												//$datos[$i]['nomtiporeq'] = $row['nomtiporeq'];
												for($i=0;$i<count($tiposDRequi);$i++)
													echo "<option value='".$tiposDRequi[$i]['tiporeq']."' title=".$tiposDRequi[$i]['nomtiporeq'].">".$tiposDRequi[$i]['nomtiporeq']."</option>";
											?>
                        </select></td>
                    </tr>
			     </table> 
					<div class="clear"></div>
                    <div class="modalItemRight"><input tabindex="6" type="hidden" readonly="true" name="prod" id= "prod" value="<?php echo $prod; ?>" size="40" /></div>
                   
                    <div class="clear"></div>
                </div>
                	<input type="button" class="btsubmit" id="btsubmit" value="Enviar">
                	<input type="button" class="btcancel" id="btcancel" value="Cancelar">
            </form>
    	</div>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</div>
</body>
</html>
