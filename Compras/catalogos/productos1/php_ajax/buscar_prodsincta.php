<?php
	require_once("../../../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		
		$comando="select prod,nomprod,unidad,ivapct,ctapresup from compradproductos where (ctapresup='' or ctapresup='0' or ctapresup is null) and estatus=0 ORDER BY prod";
		//echo $comando;
		$getProducts = sqlsrv_query( $conexion_srv,$comando);
		if ( $getProducts === false)
     	{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
            {
            	$datos[$i]['prod'] = $row['prod'];
				$datos[$i]['nomprod'] = html_entity_decode(trim($row['nomprod']));
				$datos[$i]['unidad'] = html_entity_decode(trim($row['unidad']));
				$datos[$i]['ivapct'] = $row['ivapct'];	
				$datos[$i]['ctapresup'] = $row['ctapresup'];
				$i++;
            }
		}
		sqlsrv_free_stmt( $getProducts );
	}
	else
	{
			$datos[0]['error']="1";
			$datos[0]['string']="no hay conexion";
	}
	echo json_encode($datos);
?>