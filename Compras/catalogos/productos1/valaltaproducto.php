﻿<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
?>
<?php

$usuario ="";
if(isset($_COOKIE['ID_my_site']))
	$usuario = $_COOKIE['ID_my_site'];

$descrip = strtoupper($_REQUEST['descrip']);
$unidad = strtoupper($_REQUEST['unidad']);
$eqalmacen = $_REQUEST['eqalmacen'];
$unalmacen = strtoupper($_REQUEST['unalmacen']);
$enalmacen = strtoupper($_REQUEST['enalmacen']);
$tiporeq = strtoupper($_REQUEST['tiporeq']);
$iva = $_REQUEST['iva'];
$cta = "";
$fecha = date('Y-m-d',time());
$hora =  date('H:i',time());

$fails=false;

if ($descrip == "" |$unidad == "" ) 
{
	exit("No se llenaron campos necesarios");
}
else
{
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if ($conexion)
	{		
		$tsql_callSP ="{call sp_compras_A_dprod(?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
		$params = array(&$descrip,&$unidad,&$eqalmacen,&$unalmacen,&$iva,&$cta,&$enalmacen,&$tiporeq,&$usuario);//Arma parametros de entrada
		$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
		//echo $tsql_callSP;
		//print_r($options);
		$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
		if( $stmt === false )
		{
			 $fails= "Error in statement execution.\n";
			 $fails=true;
			 die( print_r( sqlsrv_errors(), true).",<br>".print_r($params));
		}	
		if(!$fails)
		{
			// Retrieve and display the first result. 
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$id = $row['folio'];	
				$fecha = $row['fecha'];
				$hora = $row['hora'];					
			}
			sqlsrv_free_stmt( $stmt);
			
			exit("El producto $descrip ha sido dado de alta con el número de folio: ".$id);
		}
		/*$consulta = "insert into compradproductos(nomprod,unidad,eqalmacen,unalmacen,ivapct,ctapresup,falta,estatus, almacen,enalmacen) values
			('$descrip','$unidad',$eqalmacen,'$unalmacen',$iva,'$cta','$fecha',0, $enalmacen)";//,'$fecha',festatus
		echo $consulta;
		if (! sqlsrv_query($conexion, $consulta))
		{
			exit("El Producto no pudo ser Dado de Alta");
		}	*/			
	}
	else
	{ 
		exit ("Falló la conexión de base de datos") ;
	}
}
include('agregar_producto.php');  

?>