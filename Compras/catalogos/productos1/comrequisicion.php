<?php
	require_once("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	$depto = "";//$_GET['usuario'];
	$productosdeusuario= null;
	$area=null;
	$entregaren="";
	$observaciones="";
	$id_req="";
	
	$fecha_req="";
	$hora_req="";
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
	
	$productos= array();
	$personasAutori=array();
	$personasAutori = fun_ObtienePosiblesfirmas($usuario);//llama a funcion para obtener posibles firmantes
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		//Busqueda de Productos
		$command= "SELECT prod, nomprod, cve,unidad  FROM compradproductos b  ORDER BY prod ASC";	
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$productos[$i]['cve'] =trim($lrow['cve']);
				$productos[$i]['prod'] =trim($lrow['prod']);
				$productos[$i]['nomprod'] =trim($lrow['nomprod']);
				$productos[$i]['unidad'] =trim($lrow['unidad']);
				$i++;
			}
		}
		
		//Busqueda de datos del departamento
		$command= "select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nominadempleados a left join nominamdepto b on a.depto=b.depto where numemp='$usuario'";
		$stmt2 = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt2))
		{
			$i=0;
			while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
			{
				$depto =trim($lrow['depto']);
				$coord =trim($lrow['coord']);
				$dir =trim($lrow['dir']);
				$nomdepto =trim($lrow['nomdepto']);
				$nomdir =trim($lrow['nomdir']);
				$i++;
			}
		}
		
		//mconfigconfig
	}

//Funcion que llama procedimiento almacenado para obtener	
function fun_ObtienePosiblesfirmas($usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_firmas_dir(?)}";//Arma el procedimeinto almacenado
	$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		// die( print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		
		// Retrieve and display the first result. 
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$datos[$i]['nombre'] = $row['nombre'];
			$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $datos;
}
 ?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Requisiciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>

<script language="javascript" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/comrequisicion.js"></script>
</head>
<body >
<form id="form_requisicion"  name ="form_requisicion" style="width:100%"  metdod="post"  action="php_ajax/crear_reqpdf.php?">
<table width="100%">
	<tr>
	<td colspan="4">
		<table width="100%">
			<tr>
				<td align="center" width="100%" class="encabezadopag">  <b>Captura de Requisición</b>	</td>
			</tr>
			<tr>
				<td align="center" width="100%">
					<input type="text" width="50%"  value="Numero de requisición:" id="msgnumreq" name="msgnumreq" class="caja_toprint" style="visibility:hidden ">
					<input type="text" width="50%" value="<?php echo $id_req;?>" id="id_req"    name="id_req"    class="caja_toprint" style="visibility:hidden ">
					<input type="hidden" value="<?php echo $hora_req;?>"  id="hora_req"   name="hora_req"  style="visibility:hidden ">	
					<input type="hidden" value="<?php echo $fecha_req;?>" id="fecha_req"  name="fecha_req" style="visibility:hidden ">					
				</td>
			</tr>
		</table> 
										
										
	</td>
	</tr>
	<tr>
	 	
		<td class="texto8" width="15%">Departamento:</td>
			<td colspan="2" width="35%"><input type="text"  style="width:90% " id="pswd"  name="pswd" value="<?php echo $nomdepto;?>" class="caja_toprint" readonly="true"></td>
			<!--<td class="texto8" width="15%">Usuario:</td>-->
			<td width="35%"><input type="hidden" style="width:90% " id="usuario"  name="usuario"  value="<?php echo $usuario;?>" class="required"></td>
	</tr>
	
	<tr>
		<td class="texto8" width="15%">Entregar en:</td>
			<td width="35%"><input type="text" style="width:90% " id="entreganen"  name="entreganen" value="<?php if(strlen($entregaren)<=0) echo "Almacen de Fomerrey cita en Ave. Gonzalitos Norte Número 292 Primer Piso, Colonia Urdiales Monterrey N.L. C.P. 64430 en el siguiente horario de: de 8:00 a 15:00 horas"; else echo $entregaren;?>"></td>
		<td class="texto8" width="15%">Observaciones:</td>
			<td width="35%"><input type="text" style="width:90% " id="observaciones"  name="observaciones" value="<?php echo $observaciones;?>"></td>
	</tr>
	<tr>
		<td  width="100%" colspan="4" class="texto8">
			Autorización de:<select  class="texto8" id="firmaquien" name="firmaquien" >
					<?php
						for($i=0;$i<count($personasAutori);$i++)
							echo "<option value='".$personasAutori[$i]['nombre']."' title=".$personasAutori[$i]['nombre'].">".$personasAutori[$i]['nombre']."</option>";
					?>
				</select>
			
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center">
			<table width="100%"> 
				<tr>   
						<td class="texto8" style="width:220px; ">
							<div align="left" style="z-index:1; position:absolute; left:4px; top: 106px; width:250px">Producto:
							  <input class="texto8" name="txtSearch" type="text" id="txtSearch" size="40" style="width:150px;"  onKeyUp="searchSuggest();" autocomplete="off"/>  
							<div id="search_suggest" style="z-index:2;" > </div>
						  </div>
						</td>
						<td class="texto8">
							Descripción: <input class="texto8" type="text" id="des_ent" size="40" style="width:210px; "autocomplete="off"/>
							
						</td>
						<td class="texto8">
							Cantidad:<input class="texto8" name="txtSearch" type="text" id="can_ent" size="40" style="width:40px; " autocomplete="off"/>
							<input type="hidden" id="unidaddPro" name="unidaddPro" >
							<input type="hidden" id="busProd" name="busProd" >
						</td>
						<td ><input type="button" value="+" id="ad_colin2" name="ad_colin2" onClick="agrega_ProductoARequisicion();" class="texto8"></td>
			  </tr>
					<tr>
						
					</tr>
				</table>
				
		    
		</td>
	</tr>
	<tr>
		<td colspan="4"></td>
	</tr>
  	<tr>
		<td colspan="4" align="center">
			<table border="1" id="tmenudusuario" name="tmenudusuario" width="100%">
				<tr>
					<th class="subtituloverde" width="24%">Producto</th>
					<th class="subtituloverde" width="8%">Unidad</th>
					<th class="subtituloverde" width="30%">Descripcion</th>
					<th class="subtituloverde" width="4%">Cantidad</th>
					<th class="subtituloverde" width="4%">&nbsp;</th>
				</tr>
				<tbody id="menus">
					<!--<?php  if( $productosdeusuario!=null) for($j=0;$j<count($productosdeusuario);$j++){ ?>
						<tr id="filaOculta" >
						<td width="14%">
                          <input type="hidden" id="idprod" name="idprod"  value="">
							<input type="text" id="producto" name="producto"  value=""  class="caja_toprint"  style="width:100% " >
						</td>
						<td width="14%"><input type="text" id="desc" name="desc"  value="<?php echo $productosdeusuario[$j]['posicion'];?>"></td>
						<td width="14%"><input type="text" id="cantidad" name="cantidad"  value="<?php echo $productosdeusuario[$j]['posicion'];?>" ></td>
						
						<td width="14%">									   
							<input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_ProductoARequisicion(event,this);" >
						</td width="25%"></tr>
					<?php }else{ ?>
					<div style="position:absolute ">
					<tr id="filaOculta" style="visibility:hidden; ">
					 
					  	<td><input type="hidden" id="idprod" name="idprod"  value="">
							<input type="text" id="producto" name="producto"  value=""  class="caja_toprint" style="width:100% " >
						</td>
						<td><input type="text" id="desc" name="desc"  value=""  ></td>
						<td><input type="text" id="cantidad" name="cantidad"  value=""  ></td>
						
						<td width="14%"><input type="button" value="-" id="rem_colin" name="rem_colin" onClick="eliminar_ProductoARequisicion(event,this);" >
										  <input type="hidden" id="id_modulo" name="id_modulo" value="">
										  
						</td></tr></div>
					<?php } ?>-->
				</tbody>
			</table>
		</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr><td align="center" width="100%"><input type="button"  value="Guardar" onClick="GuardarRequisicion();" class="caja_entrada"></td>
		<!--<td align="center" width="50%" class="texto8"><a href='php_ajax/crear_reqpdf.php?idreq=<?php echo $id_req;?>'>Imprime requisición</a></td>-->
	</tr>
</table>
</form>

</body>
</html>
