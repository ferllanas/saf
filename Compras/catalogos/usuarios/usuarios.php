<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$usuario="";
$nombre="";
$appat="";
$apmat="";
$nombrecom="";
$pasw="";
$tipo="";
$datos=array();
if(isset($_REQUEST['usuario']))
	$usuario = $_REQUEST['usuario'];

if(isset($_REQUEST['nombre']))
	$nombre = $_REQUEST['nombre'];

if(isset($_REQUEST['appat']))
	$appat = $_REQUEST['appat'];
	
if(isset($_REQUEST['apmat']))
	$apmat = $_REQUEST['apmat'];

if(isset($_REQUEST['nombrecom']))
	$nombrecom = $_REQUEST['nombrecom'];
	
if(isset($_REQUEST['pasw']))
	$pasw = $_REQUEST['pasw'];
	
if(isset($_REQUEST['tipo']))
	$tipo = $_REQUEST['tipo'];
	
		
if ($conexion)
{		
	if (strlen($usuario)>0)
	{
		$usuario=str_repeat("0",6 - strlen($usuario)).$usuario;
		$consulta = "select * from v_nomina_nomiv_nomhon6_nombres_activos_depto_dir where numemp='$usuario' ";//"select * from nomemp.dbo.nominadempleados where numemp='$usuario' and estatus<'90'";
		//"select * from nominadempleados where numemp='$usuario' and estatus<'90'";
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$usuario = strtoupper( trim($row['numemp']) );
			$nombrecom = strtoupper(trim($row['nomemp']) );
			//$appat = strtoupper( trim($row['appat']) );
			//$apmat = strtoupper( trim($row['apmat']) );
			$i++;
		}
		
		sqlsrv_free_stmt($R );
		
		if ($i<1)
		{
			echo "<script language='JavaScript'>";
			echo "alert('Este Usuario no existe en Base de Datos, intente con otro');";
			echo "</script>";
//			echo "Este Usuario no existe en Base de Datos, intente con otro";
			$usuario="";
			$nombrecom="";
		}
	
		$consulta  = "select * from menumusuarios a inner join v_nomina_nomiv_nomhon6_nombres_activos_depto_dir b on a.usuario COLLATE DATABASE_DEFAULT=b.numemp where usuario='$usuario' and b.estatus<'90' AND a.estatus<90";
		//echo $consulta;
		//"select * from menumusuarios where usuario='$usuario' and estatus<'90'";
		$R = sqlsrv_query( $conexion,$consulta);
		if(!$R)
			die( print_r( sqlsrv_errors(), true));
	   	else	
			$R = sqlsrv_query( $conexion,$consulta);
			
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$usuario = strtoupper( trim($row['usuario']) );
			$i++;
		}
		if ($i>0)
		{
			echo "<script language='JavaScript'>";
			echo "alert('Este Usuario ya existe, intente con otro');";
			echo "</script>";
//			echo "Este Usuario ya existe, intente con otro";
			$usuario="";
			$nombre="";
			$appat="";
			$apmat="";
			$nombrecom="";
			$tipo="";
//			die();
		}	
	}	
}
//$nombrecom = $nombre .' '. $appat .' '. $apmat;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/usuariofuncion.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>
</head>

<body>
<h5><span class="TituloDForma">Altas
    <hr align="left" class="hrTitForma">
</span></h5>
<form name="form1" method="post" action="php_ajax/altausu.php">
	<table width="800" border="1" align="center">
  <tr>
    <th class="subtituloverde" colspan="2" >USUARIOS</th>
  </tr>
  <tr>
    <th width="160" scope="id" class="texto8" align="left">ID</th>
    <td><input class="texto8" tabindex="1" type="text" name="usuario" id="usuario" size="20" value="<?php echo $usuario;?>"  onblur="javascript: location.href='usuarios.php?usuario='+this.value;"></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Usuario</th>
    <td><input class="texto8" tabindex="2" type="text" readonly name="nombrecom" id="nombrecom" size="100" value="<?php echo $nombrecom;?>" /></div></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Password</th>
    <td><div align="left"><input class="texto8" tabindex="3" type="text" name="pasw" id="pasw" size="15" /></div></td>
  </tr>
    <tr><th class="texto8" align="left">Privilegios de requisiciones</th>
  	  <td><select name="req"><option value="0" class="texto8">Solo las que hizo</option>
      				<option value="20" class="texto8">De su area</option>
                    <option value="80" class="texto8">Todas</option>
          </select></td>
  <tr>
   <tr><th class="texto8" align="left">Privilegios de Solicitud de Cheques</th>
  	  <td><select name="solche"><option value="0" class="texto8">Solo las que hizo</option>
      				<option value="20" class="texto8">De su area</option>
                    <option value="80" class="texto8">Todas</option>
          </select></td>
  <tr>
  <tr><th class="texto8" align="left">Privilegios de Almacen</th>
  	  <td><select name="almacen"><option value="0" class="texto8">Solo las que hizo</option>
      				<option value="20" class="texto8">De su area</option>
                    <option value="80" class="texto8">Todas</option>
          </select></td>
  <tr>
  <tr>
    <th scope="row" class="texto8" align="left">Tipo</th>
    <td>
      <input class="texto8" tabindex="4" type="text" name="tipo" id="tipo" size="40" />50 Usuario Gral, 10 Manejo de Varias Areas</td>
  </tr>
</table>
    <table width="243" border="0" align="center">
      <tr>
        <th scope="row"><div align="center"><img src="../../../imagenes/agregar.jpg" onClick="document.forms['form1'].submit();" title="Click aqui para guardar usuario."></div></th>
        <td><div align="center"><img src="../../../imagenes/deshacer.jpg" onClick="limpia_campos()" title="Click aqui para deshacer captura."></div></td>
      </tr>
    </table>
</form>
</body>
</html>