<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$nombre="";
$nombre = $_REQUEST['nombre'];
$req = 0;
$solche =0;
$almacen = 0;
//$usuario="";
if ($conexion)
{		
	$consulta = "select * from menumusuarios where nombre='$nombre'";
	$R = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$usuario = trim($row['usuario']);//strtoupper(  );
		$nombre = trim($row['nombre']);//strtoupper(  );
		$pasw =  trim($row['pasw']);//strtoupper( );
		$enuso = trim($row['enuso']);//strtoupper(  );
		$estatus =  trim($row['estatus']);//strtoupper( );
		$tipo = trim($row['tipo']);//strtoupper(  );
		$req = $row['req'];
		$solche = $row['solche'] ;
		$almacen = $row['almacen'];
	}
}

echo $req.",".$solche .",".$almacen;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
</head>

<body>
<h5 align="left"><span class="TituloDForma">Modificaci&oacute;n<hr align="left" class="hrTitForma"></span></h5>
<form name="form1" method="post" action="php_ajax/valcamusuario.php">
	<table width="800" border="1" align="center">
  <tr>
    <th class="subtituloverde" colspan="2" >USUARIOS</th>
  </tr>
  <tr>
    <th width="252" scope="id" class="texto8" align="left">ID</th>
    <td width="532"><input tabindex="1" type="text" name="usuario" id="usuario" size="20" readonly value="<?php  echo $usuario;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Usuario</th>
    <td><input tabindex="2" type="text" name="nombre" id="nombre" size="70" readonly value="<?php  echo $nombre;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Password</th>
    <td><input tabindex="3" type="text" name="pasw" id="pasw" size="20" value="<?php  echo $pasw;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Estatus</th>
    <td>
      <table width="100%"  border="0">
        <tr>
          <th width="24%" scope="row" align="left"><input tabindex="4" type="text" name="estatus" id="estatus" size="20" value="<?php  echo $estatus;?>"/>&nbsp;</th>
          <td width="76%"> 00-&gt; ACTIVO 90 -&gt;CANCELADO</td>
        </tr>
      </table></td>
  </tr>
   <tr><th class="texto8" align="left">Privilegios de requisiciones</th>
  	  <td><select name="req"><option value="0" class="texto8" <?php if($req==0) echo "SELECTED";?>>Solo las que hizo</option>
      				<option value="20" class="texto8" <?php if($req==20) echo "SELECTED";?>>De su area</option>
                    <option value="80" class="texto8" <?php if($req==80) echo "SELECTED";?>>Todas</option>
          </select></td>
  <tr>
   <tr><th class="texto8" align="left">Privilegios de Solicitud de Cheques <?php echo $solche;?></th>
  	  <td><select name="solche">
      				<option value="0" class="texto8"  <?php if($solche==0)   echo "SELECTED";?> >Solo las que hizo</option>
                    <option value="20" class="texto8" <?php if($solche==20)  echo "SELECTED";?> >De su area</option>
                    <option value="80" class="texto8" <?php if($solche==80)  echo "SELECTED";?> >Todas</option>
          </select></td>
  <tr>
  <tr><th class="texto8" align="left">Privilegios de Almacen <?php echo $almacen;?></th>
  	  <td><select name="almacen"><option value="0" class="texto8" <?php if($almacen==0) echo "SELECTED";?>>Solo las que hizo</option>
      				<option value="20" class="texto8" <?php if($almacen==20) echo "SELECTED";?>>De su area</option>
                    <option value="80" class="texto8" <?php if($almacen=="80") echo "SELECTED";?>>Todas</option>
          </select></td>
  <tr>
	<tr>
	  <th scope="row" class="texto8" align="left">Tipo</th>
	  <td><input tabindex="5" type="text" name="tipo" id="tipo" size="20" value="<?php  echo $tipo;?>"/></td>
	  </tr>
	<tr>
		<td scope="row" colspan="2" align="center">
 				<input tabindex="6" type="submit" id="Enviar2" name="Enviar" value="Grabar" /> 
				<input tabindex="7" name="restablecer" type="reset" id="restablecer" value="Restablecer" />
		</td>
	</tr>
</table>
</form>
</body>
</html>
