<?php
require_once("../../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
////
require_once("../../../Administracion/globalfuncions.php");
validaSession(getcwd());

$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
//print_r($privilegios);
$BorrarPriv=revisaPrivilegiosBorrar($privilegios);
//$VerPDFPriv = revisaPrivilegiosPDF($privilegios);
$EditarPriv=revisaPrivilegiosEditar($privilegios);
////
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$usuario = '';
$opc=0;
if(isset($_REQUEST['usuario']))
	$usuario = $_REQUEST['usuario'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];
$datos=array();
if(isset($_REQUEST['opc']))
	$opc = $_REQUEST['opc'];
if ($conexion)
{		
	if ($opc==1)
		$consulta = "select * from menumusuarios where estatus<'90' order by nombre asc";
	   else	
		$consulta = "select * from menumusuarios where estatus>='90' order by nombre asc";

	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['usuario']= trim($row['usuario']);
		$datos[$i]['nombre']= utf8_encode(trim($row['nombre']));
		$datos[$i]['pasw']= trim($row['pasw']);
		$datos[$i]['enuso']= trim($row['enuso']);
		$datos[$i]['tipo']= trim($row['tipo']);
		$i++;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/usuariofuncion.js"></script>
<script language="javascript" src="javascript/busquedaincusuario.js"></script>
</head>

<body>
<form name="form1" method="post" action="">
<table width="100%" height="43" border="0">
<tr>
    <td colspan="8" align="center" class="TituloDForma">Catalogo de Usuarios<hr class="hrTitForma">	</tr>
</tr>
  <tr>
    <td width="460" height="39" class="texto8">Nombre del Usuario  
      <div align="left" style="z-index:1; position:absolute; width:311px; top: 66px; left: 128px;">     
        <input name="provname1" type="text" class="texto8" id="provname1" style="width:100%;" tabindex="4"  onKeyUp="searchProveedor1(this);" value="<?php echo $nombre;?>" size="60" autocomplete="off">        
        <div id="search_suggestProv" style="z-index:2;" > </div>
		</div>
		</div>  
        <input class="texto8" tabindex="2" type="hidden" id="nombre" name="nombre" size="10">
        </td>
      <td width="128">
	
    <a href="usuarios.php" class="texto9"></a>
    <p>
      <label>
      <input type="radio" name="opc" id="opc" value="1" onClick="submit()">
  Activos</label>
      <label>
      <input type="radio" name="opc" id="radio" value="2" onClick="submit()">
Inactivos</label>
      <br>
      <label>      </label>
      <label></label>
      <br>
    </p></td>
      <td width="102"><a href="usuarios.php" class="texto9"><strong>Agregar Usuario</strong></a></td>
      <td width="58">
      <input name="button" type="button" class="texto8" onClick="busqueda();" value="Buscar">
</td>
  </tr>
</table>
</form>
<table width="100%" border="0" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
  <tr>
    <td width="64" class="subtituloverde"><div align="center"><strong>Clave</strong></div></td>
    <td width="411" class="subtituloverde"><div align="center"><strong>Usuario</strong></div></td>
    <td width="62" class="subtituloverde"><div align="center"><strong>Pasword</strong></div></td>
    <td width="47" class="subtituloverde"><div align="center"><strong>En Uso </strong></div></td>
    <td width="40" class="subtituloverde"><strong>Tipo</strong></td>
    <td colspan="2" class="subtituloverde">Acci&oacute;n</td>
  </tr>
  <tbody id="provsT" class="resultadobusqueda">
  <?php 
  for ($i=0;$i<count($datos);$i++)
  	  {
  ?>
<!--    <tr class="d<?php echo ($i % 2);?>">   -->
		<tr class="fila" onClick="javascript: ;location.href='camusuario.php?editar=true&usuario=<?php echo $datos[$i]['usuario']?>&nombre=<?php echo $datos[$i]['nombre']?>&pasw=<?php echo $datos[$i]['pasw']?>&enuso=<?php echo $datos[$i]['enuso']?>&tipo=<?php echo $datos[$i]['tipo']?>'">
      <td scope="row"><div align="center"><?php echo $datos[$i]['usuario'];$usuario=$datos[$i]['usuario'];?></div>
      <td><div align="left"><?php echo $datos[$i]['nombre'];?>
      </div>  </td>
      <td><?php echo $datos[$i]['pasw'];?></td>
      <td><?php echo $datos[$i]['enuso'];?></td>
      <td><?php echo $datos[$i]['tipo'];?></td>

      <td width="41">
        <div align="center">
        <?php if($EditarPriv){?>
		  <img src="../../../imagenes/edit-icon.png" width="19" height="22" title="Editar datos de Usuario." onClick="javascript: ;location.href='camusuario.php?editar=true&usuario=<?php echo $datos[$i]['usuario']?>&nombre=<?php echo $datos[$i]['nombre']?>&pasw=<?php echo $datos[$i]['pasw']?>&enuso=<?php echo $datos[$i]['enuso']?>&tipo=<?php echo $datos[$i]['tipo']?>'"><?php }?>
          <!--abc(event,this,"<?php echo $datos[$i]["usuario"];?>")-->
</div>
      </td>
      <td width="41">
        <div align="center">
         <?php if($BorrarPriv){?> <img src="../../../imagenes/eliminar.jpg" width="19" height="22" title="Click aqui para dar de baja usuario." onClick="javascript: if(confirm('�Esta seguro que desea deshabilitar o borrar este Usuario?'))cancela('<?php echo $datos[$i]['nombre'];?>')">
      </div> <?php } ?>     </td></tr>
     <?php 
   }
   ?>	
	</tbody>
</table>
<p>&nbsp;</p>
</body>
</html>

