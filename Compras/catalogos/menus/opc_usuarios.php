<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
$wdatos=array();
$datosnivel=array();
$nivel='';
$descrip='';
$nivel2='';
$descrip2='';
$swich = 0;

$usuario = '';
if(isset($_REQUEST['usuario']))
	$usuario = $_REQUEST['usuario'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];

if ($conexion)
{		
	$consulta = "select * from menumusuarios where estatus<'90' order by nombre asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['usuario']= trim($row['usuario']);
		$datos[$i]['nombre']= trim($row['nombre']);
		$datos[$i]['pasw']= trim($row['pasw']);
		$datos[$i]['enuso']= trim($row['enuso']);
		$datos[$i]['tipo']= trim($row['tipo']);
		$i++;
	}
}

{		
	$consulta = "select * from menummenu where estatus<'90' and nivel%1000=0 order by descrip asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datosnivel[$i]['nivel']= trim($row['nivel']);
		$datosnivel[$i]['descrip']= utf8_encode(trim($row['descrip']));
		$i++;
	}
}

//$idPantalla=$_REQUEST['nivel'];
//$privilegios= get_Privilegios($usuario, $idPantalla);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Modulo de Acceso</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="javascript/funcion_menu.js"></script>
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
</head>
<body>
<form name="form1" method="post" action="">
<table name="tabla1" width="100%" border="0">
  <tr>
    <td height="45" colspan="6"  class="TituloDForma" align="left">Opciones de Usuarios
        <hr class="hrTitForma"></td>
  </tr>
  <tr>
    <th width="140" scope="row" align="left" class="texto8">Menu de Privilegios</td>
    <td width="63" scope="row" align="left">
      <select name="usuario" class="texto8" id="usuario">
          <?php
			for($i = 0 ; $i<=count($datos);$i++)
			{
			echo "<option value=".$datos[$i]["usuario"].">".$datos[$i]["usuario"]."      ".$datos[$i]["nombre"]."</option>\n";
			$usuario=$datos[$i]["usuario"];
			}
		?>
      </select>
    </div></td>
    <td width="53" scope="row" align="left" class="texto8">Modulos</th>
    <td width="136" scope="row"><select name="nivel" id="nivel" onChange="busca()" class="texto8">
      <?php
			for($i = 0 ; $i<=count($datosnivel);$i++)
			{
			echo "<option value=".$datosnivel[$i]["nivel"].">".$datosnivel[$i]["descrip"]."</option>\n";
			$nivel=$datosnivel[$i]["nivel"];
			}
		?>
    </select><!--<input type="button" name="addmenu" id="addmenu" onClick="AddMenuPrincipal()" value="+">--></td>
    <td width="56" scope="row" align="left" class="texto8">Opciones</td>
    <td width="358" scope="row" align="left">
      <select name="nivel2" id="nivel2" class="texto8" onChange="busca2()">

      </select>
    </div></td>
	
  </tr>
</table>

<table width="93%"  border="1">
<thead>
  <tr  class="subtituloverde">
    <th width="5%" scope="row">Nivel</th>
    <th width="50%">Descripci&oacute;n</td>
    <th width="5%"><div align="center">Eliminar</div></td>
    <th width="40%"><table width="100%"><tr><td colspan="3" >Privilegios</td></tr>
    <tr><?php 
			$SAF_Privilegios=get_SAF_Privilegios();
			//print_r($SAF_Privilegios);
			for( $j=0; $j< count( $SAF_Privilegios ) ; $j++)
			{
				echo "<td title='".$SAF_Privilegios[$j]['descripcion']."'>".$SAF_Privilegios[$j]['nombre']."</td>"; 
				
			}
	?></tr>
    </table></td>
  </tr>
  
</thead>
<tbody name="provsT" id="provsT">
</tbody>
</table>
</form>
</body>
</html>
