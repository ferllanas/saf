<?php
header("Content-type: text/html; charset=UTF8");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../../connections/dbconexion.php");
	require_once("../../../../Administracion/globalfuncions.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$nivel = $_REQUEST['nivel'];
$usuario = $_REQUEST['usuario'];
$datos=array();
	if ($conexion)
	{		
		
		if(strlen($nivel)>0)
		{
//			$consulta = "select * from menummenu where nivel>$nivel and nivel<$nivel+1000 and estatus<'90' order by nivel";
			$consulta = "select a.ID, a.nivel,b.descrip from menudnivel a left join menummenu b on a.nivel=b.nivel where a.usuario='$usuario' and a.estatus<90 and b.estatus<90 and a.nivel>=$nivel and a.nivel<($nivel+1000) order by a.nivel";
		}
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['id']= htmlentities(trim($row['ID']));
				$datos[$i]['nivel']= htmlentities(trim($row['nivel']));
				$datos[$i]['descrip']= utf8_encode(trim($row['descrip']));
				$datos[$i]['privilegios'] = get_Privilegios($usuario, $datos[$i]['nivel']);
				$i++;
			}
		}
	}
echo json_encode($datos);
?>