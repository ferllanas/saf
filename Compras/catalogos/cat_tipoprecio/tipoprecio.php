<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos = array();
$tipopcio = 0;
$descrip="";
$sw=0;

if(isset($_REQUEST['sw']))
	$sw = $_REQUEST['sw'];

if(isset($_REQUEST['tipopcio']))
	$tipopcio = $_REQUEST['tipopcio'];

if(isset($_REQUEST['descrip']))
	$descrip = $_REQUEST['descrip'];
	
	
if ($conexion)
{		
	$consulta = "select * from compramtipoprecio where estatus<'90' order by tipopcio asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['tipopcio']= trim($row['tipopcio']);
		$datos[$i]['descrip']= trim($row['descrip']);
		$i++;
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/precios.js"></script>
<style type="text/css">
<!--
.Estilo1 {color: #FFFFFF}
-->
</style>
</head>

<body>

<div align="center">
<form name="form1" method="post" action="">
  <table width="100%" border="0" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
  	<tr>
    <td colspan="4" align="center" class="TituloDForma">Catalogo de precios<hr class="hrTitForma">	</tr>
    <tr>
      <td width="260" class="credenciastexto" scope="row"><div align="right"><strong>Tipo de Precio</strong></strong>
            </th>
      </div>
      <td width="214" class="credenciastexto">
        <input name="tipoprecio" id="tipoprecio" type="text" size="30" readonly onBlur="pregunta_tipo()" value="<?php echo $tipopcio;?>">
        <input type="hidden" name="sw" id="sw" value="<?php echo $sw;?>">		
        <input type="hidden" name="tipopcio" id="tipopcio" size="3" value="<?php echo $tipopcio;?>">
      </td>
    </tr>
    <tr>
      <td class="credenciastexto" scope="row"><div align="right"><strong>Descripci&oacute;n</strong></div></td>
      <td width="214" class="credenciastexto">
        <input name="descrip" id="descrip" type="text" size="100px" width="200px" value="<?php echo $descrip;?>"></td>
	  <td width="226"><span class="Estilo1">___</span><img src="../../../imagenes/agregar2.jpg" width="25" height="28" title="Agregar tipo de Precio" onClick="limpia()"><span class="Estilo1">____</span><img src="../../../imagenes/guardar2.jpg" width="30" height="33" title="Guardar" onClick="Actualiza()" >
  </table>
</form>
</div>
<table name="tipo_precio" width="100%" border="0" class="resultadobusqueda">
    <tr class="subtituloverde">
      <th width="62" scope="row">Tipo</th>
      <td width="584"><div align="center">Descripci&oacute;n</div></td>
      <td colspan="3"><div align="center" class="subtituloverde"></div>        Acci&oacute;n</td>
    </tr>
	 <tbody id="tipo_precio" class="resultadobusqueda">
  <?php 
  for ($i=0;$i<count($datos);$i++)
  	  {
  ?>
     <tr class="d<?php echo ($i % 2);?>">
	 <tr class="fila" onClick="javascript: ;location.href='tipoprecio.php?editar=true&tipopcio=<?php echo $datos[$i]['tipopcio']?>&descrip=<?php echo $datos[$i]['descrip']?>&sw=<?php echo $sw=1;?>'">
      <td scope="row"><div align="center"><?php echo $datos[$i]['tipopcio'];$tiporeq=$datos[$i]['tipopcio'];?></div>
       </td>
      <td><?php echo $datos[$i]['descrip'];?></td>
      <td width="48">
        <div align="center">
          <img src="../../../imagenes/eliminar.jpg" width="19" height="22" title="Click aqui para dar de baja tipo de precio." onClick="javascript: if(confirm('�Esta seguro que desea deshabilitar o borrar este tipo de Precio?'))cancela(<?php echo $datos[$i]["tipopcio"];?>)">
        </div>
      </td>
     <?php 
   }
   ?>	
    </tr>
</table>

</body>
</html>
