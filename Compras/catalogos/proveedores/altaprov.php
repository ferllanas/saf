<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos = array();
$swich = 0;
$wtipo = " ";
$prov = 0;
if ($conexion)
{		
	$consulta = "select * from compramtipoprov where estatus<'90' order by tipo asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['tipo']= trim($row['tipo']);
		$datos[$i]['nomtipo']= trim($row['nomtipo']);
		$i++;
	}

	$consulta = "select * from compramprovbanco where estatus<'90' order by banco asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$xbancos[$i]['banco']= trim($row['banco']);
		$xbancos[$i]['nombanco']= trim($row['nombanco']);
		$banco=$xbancos[$i]['banco'];
		$nombanco=$xbancos[$i]['nombanco'];
		$i++;
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href='../../../css/estilos.css' rel="stylesheet">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/provfuncion.js"></script>


</head>

<body>
<form name="form1" method="post" action="php_ajax/valaltaprov.php">
	<table width="100%" align="center">
		<tr>
			<td colspan="2" align="center" class="TituloDForma">Alta Proveedor<hr class="hrTitForma">
		</tr>
		<tr>
			<td>
				<table width="100%" align="center">
					<tr>
						<th width="142" align="left" class="texto8 Estilo1" scope="col">Nombre del Proveedor:</th>
					  	<td width="1016"><input class="texto8" name="nomprov" type="text" id="nomprov" tabindex="1" size="67" maxlength="100" />
							  <input type="hidden" name="wtipo" id="wtipo" size="4" value="wtipo">
							  <input type="hidden" name="prov" id="prov" size="6" value="<?php echo $prov?>">
						</td>
					</tr>
					<tr>
						<th align="left" class="texto8 Estilo1" scope="row">Tipo de Proveedor:</th>
					  	<td><span class="Estilo1">
							<select class="texto8" name="tipo" id="tipo" onChange="tipo_prov2()">
										<?php
											echo "<option value=''> </option>";
											for($i = 0 ; $i<count($datos);$i++)
											{
												echo "<option value=".$datos[$i]["tipo"].">".$datos[$i]["nomtipo"]."</option>\n";
											}
										?>
							</select>
							</span>   
						</td>
					</tr>
					<tr>
						<th align="left" class="texto8 Estilo1" scope="row">Contacto:</th>
						<td><div align="left" class="Estilo1">
							  <input class="texto8" tabindex="2" type="text" name="contacto" id="contacto" size="67" maxlength="50" />
						</div>
						</td>
					</tr>
					<tr>
					  	<th align="left" class="texto8 Estilo1" scope="row">Calle:</th>
					  	<td><div align="left" class="Estilo1">
							<input class="texto8" tabindex="4" type="text" name="calle" id="calle" size="67" maxlength="40" />
							</div>
						</td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">Colonia:</th>
					  <td><div align="left" class="Estilo1">
						  <input class="texto8" tabindex="5" type="text" name="colonia" id="colonia" size="67" maxlength="40"/>
					  </div></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">Ciudad:</th>
					  <td><div align="left" class="Estilo1">
						  <input class="texto8" tabindex="6" type="text" name="ciudad" id="ciudad" size="67"  maxlength="40"/>
					  </div></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">Telefono(s):</th>
					  <td>
						<div align="left" class="Estilo1">
						  <input class="texto8" tabindex="7" type="text" name="tel_1" id="tel_1" size="18" maxlength="18">
						  <input class="texto8" tabindex="8" type="text" name="tel_2" id="tel_2" size="18" maxlength="18">
						  <input class="texto8" tabindex="9" type="text" name="tel_3" id="tel_3" size="18" maxlength="18">
					  </div></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">RFC:</th>
					  <td><div align="left" class="Estilo1">
						  <input name="rfc" type="text" class="texto8" id="rfc" tabindex="10" onBlur="pregunta_rfc()" size="67" maxlength="20">
					  </div></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">E-mail:</th>
					  <td><span class="Estilo1">
						<input class="texto8" tabindex="11" type="text" name="email" id="email" size="67" maxlength="50">
						<input name="swich" type="hidden" id="swich" value="<?php echo $swich;?>" size="2">
					  </span></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">Banco:</th>
					  <td><span class="Estilo1">
						<select class="texto8" name="banco" id="banco">
					<?php
						for($i = 0 ; $i<=count($xbancos);$i++)
						{
						echo "<option value=".$xbancos[$i]["banco"].">".$xbancos[$i]["nombanco"]."</option>\n";
						}
					?>
						</select>
				</span></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">Clave Interbancaria: </th>
					  <td><span class="Estilo1">
						<input class="texto8" tabindex="11" type="text" name="cveinterbco" id="cveinterbco" size="67" maxlength="18">
					  </span></td>
					</tr>
					<tr>
					  <th align="left" class="texto8 Estilo1" scope="row">Cuenta Bancaria: </th>
					  <td><div align="left" class="Estilo1">
						<input class="texto8" tabindex="11" type="text" name="ctabanco" id="ctabanco" size="67" maxlength="20">
					  </div></td>
				</tr>
			</table>
		</td>
    </tr>
    <tr>
		<td colspan="2">
			<table width="550px" border="1" align="left">
			  <tr>
				<th width="27" class="subtituloverde" scope="row">#</th>
				<th width="232" class="subtituloverde" scope="row"><strong>Tipos de Proveedor</strong></th>
				<th width="59" class="subtituloverde" scope="row"><div align="center">Acci&oacute;n</div></th>
				</tr>
					<tbody id="provsTipo" name="provsTipo">
			</table>
		</td>
    </tr>
	<tr>
		<td> 
			<table width="60%">
				<tr>
					<td width="104" align="center"><input class="texto8" tabindex="12"  type="button" id="Enviar" name="Enviar" value="Agregar" onclick="guardartipos()"/></td>
					<td width="85" align="center"><input class="texto8" tabindex="13" name="restablecer" type="reset" id="restablecer2" value="Restablecer" /></td>
					<td width="295" align="center"><span class="texto8"><a href="buscaprov.php" class="Estilo8">Regresar a Busqueda de Proveedores</a></span> </td>
				</tr>
		  </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
