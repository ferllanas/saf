<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../../../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$prov = '';
if(isset($_REQUEST['numprov']))
	$prov = $_REQUEST['numprov'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];
$datos=array();
//echo $nombre.",".$prov;
	if ($conexion && (isset($_REQUEST['numprov']) || isset($_REQUEST['provname1'])))
	{		
		
		if( $prov > 0)
		{
			$consulta = "select * from compramprovs where prov=$prov and estatus<'9000'";
		}
		else
		{
			$consulta = "select * from compramprovs where nomprov like '%$nombre%' and estatus<'9000'";
		}
		//echo $consulta ;
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			//echo $resoponsecode;
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['prov']= trim($row['prov']);
				$datos[$i]['nombre']= htmlentities(trim($row['nomprov']));
				$datos[$i]['contacto']= htmlentities(trim($row['contacto']));
				$datos[$i]['rfc']= trim($row['rfc']);
				$datos[$i]['tel_1']= trim($row['tel_1']);
				$datos[$i]['tel_2']= trim($row['tel_2']);
				$datos[$i]['tel_3']= trim($row['tel_3']);
				$i++;
			}
		}
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Control de Proveedores  -----------------------  Busqueda  ----------------------- </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/provfuncion.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<style type="text/css">
<!--
.Estilo1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="buscaprov.php">
  <table width="100%" border="0">
	<tr >
  		<td colspan="4" align="center" class="TituloDForma">Proveedores<hr class="hrTitForma">
	</tr>
  <tr>
    <td width="142" class="texto8 Estilo1">No. Prov. 
      <input class="texto8" tabindex="1" type="text" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
	</td>
    <td width="672" class="texto8 Estilo1">Proveedor:<div align="left" style="z-index:1; position:absolute; width:302px; top: 43px; left: 255px; height: 21px;">  
							 <input class="texto8" type="text" id="provname1" name="provname1" style="width:70%;"  onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="4"  value="<?php echo $nombre;?>">   
							<div id="search_suggestProv" style="z-index:2;" > </div>
		</div>  
   	  </td>
	<td width="205" class="texto8">
    <a href="altaprov.php" class="Estilo8">
    <input name="button" type="submit" class="texto8" value="Buscar"><!--onClick="ffff();" -->
    </a></td>
    <td width="158"><span class="texto8"><a href="altaprov.php" class="Estilo8"> Agregar Proveedor</a></span>
    </td>
  </tr>
</table>
</form>
<table width="103%" class="resultadobusqueda">
  <tr>
    <th width="66"  class="subtituloverde"><div align="center" class="Estilo1"># Prov.</div></th>
    <th width="232" class="subtituloverde"><div align="center" class="Estilo1">Nombre</div></th>
    <th width="193" class="subtituloverde"><div align="center" class="Estilo1">Contacto</div></th>
    <th width="99" class="subtituloverde"><span class="Estilo1">Telefonos</span></th>
    <th width="121" class="subtituloverde">&nbsp;</th>
  </tr>
	<tbody id="provsT" class="resultadobusqueda">
	<?php
		for($i=0;$i<count($datos);$i++)
		{
	?>
		<tr class="d<?php echo ($i % 2);?>">
			<td class="texto8" align="center"><?php echo $datos[$i]['prov'];?></td>
			<td class="texto8" align="center"><?php echo $datos[$i]['nombre'];?></td>
			<td class="texto8"><?php echo $datos[$i]['contacto'];?></td>
			<td class="texto8"><?php echo $datos[$i]['tel_1'].",".$datos[$i]['tel_2'].",".$datos[$i]['tel_3'];?></td>
			<td class="texto8" align="center">
				<img src="../../../imagenes/edit-icon.png" onClick="javascript:location.href='camprov.php?prov=<?php echo $datos[$i]['prov'];?>'" title="Click aqui para editar proveedor."> 
				<img src="../../../imagenes/eliminar.jpg" onClick="javascript: if(confirm('¿Esta seguro que desea deshabilitar o borrar este proveedor?'))cancela_proveedor(<?php echo $datos[$i]['prov'];?>)" title="Click aqui dar de baja el proovedor.">
<!--
								<?php 
									if( (int)$datos[$i]['estatus'] < 90 )
									{ 
											//echo  strlen( $datos[$i]['orden']);
										if( strlen( $datos[$i]['orden'] )>0 && $datos[$i]['orden']!="null")
										{
											echo "<a title='La Requisición se encuentra actualmente  ".$datos[$i]['orden']."' class='texto8'> OC:".$datos[$i]['orden']."</a>";
										}
										else
										{
											if( strlen( $datos[$i]['cuadroc'])>0 && $datos[$i]['cuadroc']!="null" )
											{
												echo "<a title='La Requisición se encuentra actualmente sobre el Cuadro Comparativo ".$datos[$i]['cuadroc']."' class='texto8'> CC:".$datos[$i]['cuadroc']."</a>";
											}
											else
											{ ?>
											
											<img src="../../imagenes/eliminar.jpg" onClick="cancelarRequi(<?php echo $datos[$i]['requisi'];?>)" title="Click aqui para cancelar la requisición.">
									<?php }
										}
									}
									else {?>
											<img src="../../imagenes/borrar.jpg"  title="Esta requisición se encuentra cancelada.">
									<?php }?>
													-->
				</td>
		</tr>
	<?php
		}
	?>
	</tbody>
</table>
<p>&nbsp;</p>
</body>
</html>
