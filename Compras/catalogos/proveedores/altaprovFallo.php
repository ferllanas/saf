<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
if ($conexion)
{		
	$consulta = "select * from compramtipoprov where estatus<'90' order by tipo asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['prov']= trim($row['prov']);
//		$datos[$i]['nombre']= trim($row['nomprov']);
//		$i++;
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
</head>

<body>
<h5>&nbsp;</h5>
<form name="form1" method="post" action="php_ajax/valaltaprov.php">
	<table width="800" border="1" align="center">
  <tr>
    <th class="subtituloverde" colspan="2" >ALTA DE PROVEEDOR</th>
    <th scope="col"><div align="center"></div></th>
  </tr>
  <tr>
    <th scope="col" class="texto8" align="left">Nombre del Proveedor</th>
    <td><input tabindex="1" type="text" name="nomprov" id="nomprov" size="100" /></td>
  </tr>
  <tr>
    <th width="160" scope="col" class="texto8" align="left">Tipo Prov.</th>
      <select name="tipo" id="nivel">
<?php
	for($i = 0 ; $i<=count($datos);$i++)
	{
	echo "<option value=".$datos[$i]["tipo"].">".$datos[$i]["nomtipo"]."</option>\n";
	$tipo=$datos[$i]["tipo"];
	}
?>
      </select>
    </form></th>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Contacto</th>
    <td><div align="left"><input tabindex="2" type="text" name="contacto" id="contacto" size="50" /></div></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Puesto</div></th>
    <td><div align="left"><input tabindex="3" type="text" name="puesto" id="puesto" size="30" /></div></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Calle</div></th>
    <td><div align="left"><input tabindex="4" type="text" name="calle" id="calle" size="40" /></div></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Colonia</div></th>
    <td><div align="left"><input tabindex="5" type="text" name="colonia" id="colonia" size="40" /></div></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">ciudad</div></th>
    <td><div align="left"><input tabindex="6" type="text" name="ciudad" id="ciudad" size="40" /></div></td>

  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Telefono(s)</div></th>
    <td>
      <div align="left">
        <input tabindex="7" type="text" name="tel_1" id="tel_1" size="18">
        <input tabindex="8" type="text" name="tel_2" id="tel_2" size="18">
        <input tabindex="9" type="text" name="tel_3" id="tel_3" size="18"> 
        </div>
    </td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Rfc</div></th>
    <td><div align="left"><input tabindex="10" type="text" name="rfc" id="rfc" size="20"></div></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">E-mail</div></th>
    <td><div align="left"><input tabindex="11" type="text" name="email" id="email" size="50"></div></td>
  </tr>
	<tr>
		<td scope="row" colspan="2" align="center">
 				<input tabindex="12" type="submit" id="Enviar2" name="Enviar" value="Agregar" /> 
				<input tabindex="13" name="restablecer" type="reset" id="restablecer" value="Restablecer" />
		</td>
	</tr>
</table>
</form>
</body>
</html>
