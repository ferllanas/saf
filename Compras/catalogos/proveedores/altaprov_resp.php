<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$datos=array();
if ($conexion)
{		
	$consulta = "select * from compramtipoprov where estatus<'90' order by tipo asc";
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['tipo']= trim($row['tipo']);
		$datos[$i]['nomtipo']= trim($row['nomtipo']);
		$datos[$i]['estatus']= trim($row['estatus']);
		$datos[$i]['usuario']= $row['usuario'];
		$datos[$i]['falta']= $row['falta'];
		$datos[$i]['fbaja']= trim($row['fbaja']);
		$nivel=$datos[$i]['tipo'];
		$i++;
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href='../../../css/estilos.css' rel="stylesheet">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/provfuncion.js"></script>

<style type="text/css">
<!--
.Estilo1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>

<body>
<h5>&nbsp;</h5>
<form name="form1" method="post" action="php_ajax/valaltaprov.php">
  <table width="800" border="1" align="center">
    <tr>
      <th colspan="2" class="subtituloverde Estilo1" >CONTROL DE PROVEEDORES (ALTAS) </th>
      <th width="3" scope="col"><div align="center"></div></th>
    </tr>
    <tr>
      <th width="125" align="left" class="texto8 Estilo1" scope="col">Nombre del Proveedor</th>
      <td width="650"><input class="texto8" name="nomprov" type="text" id="nomprov2" tabindex="1" size="100" /></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Tipo de Proveedor</th>
      <td><span class="Estilo1">
        <select class="texto8" name="tipo" id="tipo" >
          <?php
	for($i = 0 ; $i<=count($datos);$i++)
	{
	echo "<option value=".$datos[$i]["tipo"].">".$datos[$i]["nomtipo"]."</option>\n";
	}
	?>
        </select>
      </span>   
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Contacto</th>
      <td><div align="left" class="Estilo1">
          <input class="texto8" tabindex="2" type="text" name="contacto" id="contacto2" size="67" />
      </div></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Calle</th>
      <td><div align="left" class="Estilo1">
        <input class="texto8" tabindex="4" type="text" name="calle" id="calle2" size="67" />
</div></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Colonia</th>
      <td><div align="left" class="Estilo1">
          <input class="texto8" tabindex="5" type="text" name="colonia" id="colonia2" size="67" />
      </div></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Ciudad</th>
      <td><div align="left" class="Estilo1">
          <input class="texto8" tabindex="6" type="text" name="ciudad" id="ciudad2" size="67" />
      </div></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Telefono(s)</th>
      <td>
        <div align="left" class="Estilo1">
          <input class="texto8" tabindex="7" type="text" name="tel_1" id="tel_1" size="18">
          <input class="texto8" tabindex="8" type="text" name="tel_2" id="tel_2" size="18">
          <input class="texto8" tabindex="9" type="text" name="tel_3" id="tel_3" size="18">
      </div></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">Rfc</th>
      <td><div align="left" class="Estilo1">
          <input class="texto8" tabindex="10" type="text" name="rfc" id="rfc" size="67" onBlur="pregunta_rfc()">
      </div></td>
    </tr>
    <tr>
      <th align="left" class="texto8 Estilo1" scope="row">E-mail</th>
      <td><div align="left" class="Estilo1">
          <input class="texto8" tabindex="11" type="text" name="email" id="email" size="67">
      </div></td>
    </tr>
    <tr>
      <td scope="row" colspan="2" align="center">
        <input class="texto8" tabindex="12" type="submit" id="Enviar" name="Enviar" value="Agregar" />
        <input class="texto8" tabindex="13" name="restablecer" type="reset" id="restablecer2" value="Restablecer" />
      <td><div align="left">
      </div></td>
    </tr>
  </table>
</form>
</body>
</html>

