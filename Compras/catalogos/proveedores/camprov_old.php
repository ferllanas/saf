<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$prov = $_REQUEST['prov'];
//$datos=array();
$nomprov ="";
if ($conexion)
{		
	$consulta = "select * from compramprovs where prov=$prov and estatus<'9000'";
	$R = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$prov = strtoupper( trim($row['prov']) );
		$nomprov = strtoupper( trim($row['nomprov']) );
		$contacto = strtoupper( trim($row['contacto']) );
		$calle = strtoupper( trim($row['calle']) );
		$colonia = strtoupper( trim($row['colonia']) );
		$ciudad = strtoupper( trim($row['ciudad']) );
		$tel_1 = strtoupper( trim($row['tel_1']) );
		$tel_2 = strtoupper( trim($row['tel_2']) );
		$tel_3 = strtoupper( trim($row['tel_3']) );
		$rfc = strtoupper( trim($row['rfc']) );
		$email = $row['email'];
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
</head>

<body>
<h5>&nbsp;</h5>
<form name="form1" method="post" action="php_ajax/valcamprov.php">
	<table width="800" border="1" align="center"><input type="hidden" id="prov" name="prov" value="<?php echo $prov;?>">
  <tr>
    <th class="subtituloverde" colspan="2" >MODIFICA DE PROVEEDOR</th>
    <th scope="col"><div align="center"></div></th>
  </tr>
  <tr>
    <th width="160" scope="col" class="texto8" align="left">Nombre del Proveedor</th>
   <td><input tabindex="1" type="text" name="nomprov" id="nomprov" size="100" value="<?php echo $nomprov;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Contacto</th>
    <td><div align="left"><input tabindex="2" type="text" name="contacto" id="contacto" size="50" value="<?php  echo $contacto;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Calle</div></th>
    <td><div align="left"><input tabindex="4" type="text" name="calle" id="calle" size="40" value="<?php echo $calle;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Colonia</div></th>
    <td><div align="left"><input tabindex="5" type="text" name="colonia" id="colonia" size="40" value="<?php echo $colonia;?>"/></td>
  <tr>
    <th scope="row" class="texto8" align="left">ciudad</div></th>
    <td><div align="left"><input tabindex="6" type="text" name="ciudad" id="ciudad" size="40" value="<?php echo $ciudad;?>"/></td>

  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Telefono(s)</div></th>
    <td>
      <div align="left">
        <input tabindex="7" type="text" name="tel_1" id="tel_1" size="18" value="<?php echo $tel_1;?>"/>
        <input tabindex="8" type="text" name="tel_2" id="tel_2" size="18" value="<?php echo $tel_2;?>"/>
        <input tabindex="9" type="text" name="tel_3" id="tel_3" size="18" value="<?php echo $tel_3;?>"/>
        </div>
    </td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">Rfc</div></th>
    <td><div align="left"><input tabindex="10" type="text" name="rfc" id="rfc" size="20" value="<?php echo $rfc;?>"/></td>
  </tr>
  <tr>
    <th scope="row" class="texto8" align="left">E-mail</div></th>
    <td><div align="left"><input tabindex="11" type="text" name="email" id="email" size="50" value="<?php echo $email;?>"/></td>
  </tr>
	<tr>
		<td scope="row" colspan="2" align="center">
 				<input tabindex="12" type="submit" id="Enviar2" name="Enviar" value="Grabar" /> 
				<input tabindex="13" name="restablecer" type="reset" id="restablecer" value="Restablecer" />
		</td>
	</tr>
</table>
</form>
</body>
</html>
