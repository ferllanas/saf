<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	require_once("../../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
$prov = $_REQUEST['prov'];
//$datos=array();
$swich = 1;

$nomprov ="";
$wtipo ="";

if ($conexion)
	{
		$datosdprov0=array();
		$consulta = "select a.*,b.nomtipo,b.estatus from compradprovs a left join compramtipoprov b on a.tipo=b.tipo where a.prov='$prov' and a.estatus<'9000' and b.estatus<'90' order by a.tipo";
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datosdprov0[$i]['tipo']= trim($row['tipo']);
			$datosdprov0[$i]['nomtipo']= trim($row['nomtipo']);
			$i++;
		}
	}
	if ($i=0)
		$wtipo="";

{		
	$consulta = "select a.*,b.tipo from compramprovs a left join compradprovs b on a.prov=b.prov where a.prov=$prov and a.estatus<'9000' and b.estatus<'9000'";
	$consulta = "select * from compramprovs where prov=$prov";	
	$R = sqlsrv_query( $conexion,$consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$prov = strtoupper( trim($row['prov']) );
		$nomprov = strtoupper( trim($row['nomprov']) );
//		$tipo = strtoupper( trim($row['tipo']) );
		$contacto = strtoupper( trim($row['contacto']) );
		$calle = strtoupper( trim($row['calle']) );
		$colonia = strtoupper( trim($row['colonia']) );
		$ciudad = strtoupper( trim($row['ciudad']) );
		$tel_1 = strtoupper( trim($row['tel_1']) );
		$tel_2 = strtoupper( trim($row['tel_2']) );
		$tel_3 = strtoupper( trim($row['tel_3']) );
		$rfc = strtoupper( trim($row['rfc']) );
		$email = trim($row['email']);
		$wbanco = trim($row['banco']);
		$cveinterbco = trim($row['cveinterbco']);
		$ctabanco = trim($row['ctabanco']);
		$i++;

	}
	{		
		$consulta = "select * from compramtipoprov where estatus<'90' order by tipo desc";
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['tipo']= trim($row['tipo']);
			$datos[$i]['nomtipo']= trim($row['nomtipo']);
			$datos[$i]['estatus']= trim($row['estatus']);
			$datos[$i]['usuario']= trim($row['usuario']);
			$datos[$i]['falta']= $row['falta'];
			$datos[$i]['fbaja']= $row['fbaja'];
			$tipo= trim($datos[$i]['tipo']);
			$nomtipo= trim($datos[$i]['nomtipo']);
			$i++;
		}
	}
	{
		$datosdprov=array();
		$consulta = "select a.*,b.nomtipo,b.estatus from compradprovs a left join compramtipoprov b on a.tipo=b.tipo where a.prov='$prov' and a.estatus<'9000' and b.estatus<'90' order by a.tipo";
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$datosdprov[$i]['tipo']= trim($row['tipo']);
			$datosdprov[$i]['nomtipo']= trim($row['nomtipo']);
			$i++;
		}
	}
	{		
		$consulta = "select * from compramprovbanco where estatus<'90' order by banco asc";
		$R = sqlsrv_query( $conexion,$consulta);
		$i=0;
		while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
		{
			$xbancos[$i]['banco']= trim($row['banco']);
			$xbancos[$i]['nombanco']= trim($row['nombanco']);
			$i++;
		}
	}
	
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../../prototype/jQuery.js"></script>
<script language="javascript" src="../../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/provfuncion.js"></script>

<style type="text/css">
<!--
.Estilo1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>
<body>

<form name="form1" id="form1" method="post" action="php_ajax/valcamprov.php">
 <table width="100%" align="center">
		<tr>
			<td colspan="2" align="center" class="TituloDForma">Editar Proveedor<hr class="hrTitForma">
		</tr>
		<tr>
			<td>
				<table width="100%" align="center">
					  <tr>
						<th width="131" scope="col" class="texto8" align="left">Nombre del Proveedor:</th>
					    <td width="1027"><input class="texto8" tabindex="1" type="text" name="nomprov" id="nomprov" size="67" value="<?php echo $nomprov;?>"/>
										<input type="hidden" id="prov" name="prov" value="<?php echo $prov;?>">
						</td>
					  </tr>
					  <tr>
						<th align="left" class="texto8 Estilo1" scope="row">Tipo de Proveedor:</th>
						<td><span class="Estilo1">
						  <select class="texto8" name="tipo" id="tipo" onChange="tipo_prov2()" >
					   <?php
						
						for($i = 0 ; $i<count($datos);$i++)
						{
							echo "<option value=".$datos[$i]["tipo"].">".$datos[$i]["nomtipo"]."</option>";
						}
						?>
						  </select>
					</span>         </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Contacto:</th>
						<td><input class="texto8" tabindex="2" type="text" name="contacto" id="contacto" size="67" value="<?php  echo $contacto;?>"/></td>
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Calle:</th>
						<td><input class="texto8" tabindex="4" type="text" name="calle" id="calle" size="67" value="<?php echo $calle;?>"/></td>
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Colonia:</th>
						<td><input class="texto8" tabindex="5" type="text" name="colonia" id="colonia" size="67" value="<?php echo $colonia;?>"/></td>
					  <tr>
						<th scope="row" class="texto8" align="left">ciudad:</th>
						<td><input class="texto8" tabindex="6" type="text" name="ciudad" id="ciudad" size="67" value="<?php echo $ciudad;?>"/></td>
					
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Telefono(s):</th>
						<td>
						  
							<input class="texto8" tabindex="7" type="text" name="tel_1" id="tel_1" size="18" value="<?php echo $tel_1;?>"/>
							<input class="texto8" tabindex="8" type="text" name="tel_2" id="tel_2" size="18" value="<?php echo $tel_2;?>"/>
							<input class="texto8" tabindex="9" type="text" name="tel_3" id="tel_3" size="18" value="<?php echo $tel_3;?>"/>
							</div>
						</td>
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">RFC:</th>
						<td><input name="rfc" type="text" class="texto8" id="rfc" tabindex="10" onBlur="pregunta_rfc()" value="<?php echo $rfc;?>" size="67" maxlength="20" /></td>
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">E-mail:</th>
						<td><input class="texto8" tabindex="11" type="text" name="email" id="email" size="67" value="<?php echo $email;?>"/></td>
						<input name="swich" type="hidden" id="swich" value="<?php echo $swich;?>" size="2">
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Banco:</th>
						<td><span class="Estilo1">
						  <select class="texto8" name="banco" id="banco" >
							<?php
								for($i = 0 ; $i<=count($xbancos);$i++)
								{
									if ($wbanco==$xbancos[$i]["banco"])
									   echo "<option value=".$xbancos[$i]["banco"]." selected>".$xbancos[$i]["nombanco"]."</option>\n";
									else
									 echo "<option value=".$xbancos[$i]["banco"].">".$xbancos[$i]["nombanco"]."</option>\n";	
								}
							?>
						  </select>
					</span></td>
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Clave Interbancaria : </th>
						<td><span class="Estilo1">
						  <input class="texto8" tabindex="11" type="text" name="cveinterbco" id="cveinterbco" size="67" value="<?php echo $cveinterbco;?>">
						</span></td>
					  </tr>
					  <tr>
						<th scope="row" class="texto8" align="left">Cuenta Bancaria: </th>
						<td><span class="Estilo1">
						  <input class="texto8" tabindex="11" type="text" name="ctabanco" id="ctabanco" size="67" value="<?php echo $ctabanco;?>">
						  </span></td>
					  </tr>
					</table>
				</td>
			</tr>
			<tr>
				<td scope="row" colspan="2" align="left">
					<table width="400px" border="1" align="left" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
						<thead>
							<th width="27" class="subtituloverde" scope="row">#</th>
							<th width="232" bordercolor="#FFFFFF" class="subtituloverde" scope="row"><strong>Tipos de Proveedor</strong></th>
							<th width="59" class="subtituloverde" scope="row"><div align="center">Acci&oacute;n</div></th>
						</thead>
						<tbody id="provsTipo" name="provsTipo">
						  <?php 
							  for ($i=0;$i<count($datosdprov0);$i++)
							  {
						   ?>	  
							<tr>
								<td width="3" bordercolor="#FFFFFF" class="texto8" scope="row" align="center"><?php echo $datosdprov0[$i]["tipo"];?>
								</td>
								<td width="232" bordercolor="#FFFFFF" class="texto8" scope="row" align="center">
									<input name="wtipo" type="hidden" id="wtipo" value="<?php echo $datosdprov0[$i]['tipo'];?>" size="2">
									<?php echo $datosdprov0[$i]["nomtipo"];?>
								</td>
								<td width="59" bordercolor="#FFFFFF" class="texto8" scope="row" align="center">
									<input type='button' class='texto8' id='borrarcelda' name='borracelda' value='Quitar' onclick='Borra_Celdas(event,this,"<?php echo $datosdprov0[$i]["tipo"];?>")'/>
								</td>
							  </tr>
						   <?php 
						   }
						   ?>	
							</tbody>
						</table>
	</tr>
	<tr>
		<td>
			<table width="400px">
				<tr>
					<td align="center"><input class="texto8" tabindex="13" type="button" id="Enviar" name="Enviar" value="Grabar" onclick="enviar_forma()"/> </td>
					<td align="center"><input class="texto8" tabindex="14" name="restablecer" type="reset" id="restablecer" value="Restablecer" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
