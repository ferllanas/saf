<?php
	
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);

$nomprov1="";
$numreq="";
if(isset($_REQUEST['txtSearch']))
	$numreq=$_REQUEST['txtSearch'];
//echo $numreq;
$cotizaciones=array();

$conexion = sqlsrv_connect($server,$infoconexion);
if( $conexion && isset($_REQUEST['txtSearch']))
{
	$command= "SELECT a.cotiza, c.prov  FROM compramcotiza a INNER JOIN compradcotiza b ON b.cotiza=a.cotiza INNER JOIN compradcotizaprov c ON b.id=c.iddcotiza WHERE a.requisi=$numreq AND a.estatus<9000";	
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/cotizaciones.js"></script>
<script language="javascript" src="javascript/busquedaIncRequisicion.js"></script>
</head>

<body>
<form style="width:100%;" action="seleccionar_CotizacionesPCC.php">
<table width="100%">
	<tr><td width="100%">Selección de Cotizaciones</td></tr>
	<tr>
		<td width="100%">
			<table width="100%">
				<tr>
					<td>Requisición:</td>
					<td><div align="left" style="z-index:3; position:relative; left:0px">
							<input name="txtSearch" type="text" id="txtSearch" size="10" width="50px" onKeyUp="searchSuggest();" autocomplete="off"/ tabindex="14">  
							<input type="hidden" id="busProd" name="busProd" >
							<input type="hidden" id="unidaddPro" name="unidaddPro" >
							<!--<input class="texto8" type="button" value="Agregar Requisici&oacute;n" id="ad_colin2" name="ad_colin2" onClick="agrega_ProductosDreqAOrdeNCom();" tabindex="15" >  -->
							<br />
							<div id="search_suggest" style="z-index:2;" > </div>
						</div>
					</td>
					<td><input type="submit" value="Mostrar cotizaciones"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="subtituloverde12">
			Cotizaciones:
		</td>
	</tr>
	<tr>
		<td width="100%">
			<table width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Proveedor</th>
						<th>Total Cotizado</th>
					</tr>
				</thead>
				<tbody>
				<?php for($i=0;$i<count($cotizaciones);$i++) { ?>
					<tr>
						<td><input type="checkbox" id="selectCotiza" name="selectCotiza"></td>
						<td><input type="hidden" id="prov" name="prov"><?php $cotizaciones[$i]['nomprov']?></td>
						<td><?php $cotizaciones[$i]['totalCotizado']?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center"><input type="button" value="Ir a Cuadro Comprativo" onClick="alert('Enviando a cuadro comparativo')"></td>
	</tr>
</table>
</form>
</body>
</html>
