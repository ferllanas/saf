<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);

$nomprov1="";
$numreq="";
if(isset($_REQUEST['txtSearch']))
	$numreq=$_REQUEST['txtSearch'];
//echo $numreq;
$productos= array();

$conexion = sqlsrv_connect($server,$infoconexion);
if( $conexion && isset($_REQUEST['txtSearch']))
{
	$command= "SELECT b.id, b.requisi,a.estatus,b.estatus, b.prod, b.unidad, b.saldo, b.descrip, c.nomprod 
				FROM compramrequisi a 
				LEFT JOIN compradrequisi b  ON a.requisi=b.requisi 
				LEFT JOIN compradproductos c ON c.prod = b. prod 
				WHERE a.requisi=$numreq AND b.saldo>0 and a.estatus<9000 AND b.saldo<9000";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$productos[$i]['iddrequisi']= trim($row['id']);
			$productos[$i]['requisi']= utf8_decode($row['requisi']);
			$productos[$i]['prod']= utf8_decode($row['prod']);
			$productos[$i]['unidad']= utf8_decode($row['unidad']);
			$productos[$i]['saldo']= utf8_decode($row['saldo']);
			$productos[$i]['descrip']= utf8_decode(trim($row['descrip']));
			$productos[$i]['nomprod']= utf8_decode(trim($row['nomprod']));
			$i++;
		}
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Cotizaciones Requisición y Proveedores</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/cotizaciones.js"></script>
<script language="javascript" src="javascript/busquedaIncRequisicion.js"></script>
</head>

<body>
<form name="formEnvioCotizaciones" id="formEnvioCotizaciones" action="envioDReqACotizacion.php" width="100%" style="z-index:1;">
	<table width="100%">
		<tr>
			<td class="TituloDForma" width="100%">Cotizaciones Requisición y Proveedores<hr class="hrTitForma"></td>
		</tr>
		<tr>
			<td>
				<table  width="100%">
					<tr>
						<td width="7%">Requisición</td>
						<td width="56%"><!--<input type="text" name="numreq" id="numreq" value="<?php echo $numreq;?>">-->
							<div align="left" style="z-index:3; position:relative; left:0px">
							<input name="txtSearch" type="text" id="txtSearch" size="10" width="50px" onKeyUp="searchSuggest();" autocomplete="off"/ tabindex="14" value="<?php echo $numreq;?>">  
							<input type="hidden" id="busProd" name="busProd" >
							<input type="hidden" id="unidaddPro" name="unidaddPro" >
							<!--<input class="texto8" type="button" value="Agregar Requisici&oacute;n" id="ad_colin2" name="ad_colin2" onClick="agrega_ProductosDreqAOrdeNCom();" tabindex="15" >  -->
							<br />
							<div id="search_suggest" style="z-index:2;" > </div>
						</div>
						</td>
						<td width="37%"><input type="submit" value="Muestra Productos"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100%">
				<table width="100%">
					<thead>
						<tr class="subtituloverde">
							<th width="10%" class="texto8"></th>
							<th width="15%" class="texto8">Saldo Cantidad</th>
							<th width="10%" class="texto8">Catidad a Cotizar</th>
							<th width="60%" class="texto8">Nombre Descripción de Producto</th>				
						</tr>
					</thead>
					<tbody name="productos" id="productos" class="resultadobusqueda">
					<?php	
						for($i=0;$i<count($productos);$i++)
						{
					?>
						<tr class="d"<?php echo ($i%2);?>)>
							<td class="texto8"><input type="checkbox" name="cheEnviar" id="cheEnviar" checked></td>
							<td class="texto8"><input type="hidden" id="cantidad" name="cantidad" value="<?php echo $productos[$i]['saldo'];?>">
								<input type="hidden" id="iddrequisi" name="iddrequisi" value="<?php echo $productos[$i]['iddrequisi'];?>">
								<input type="hidden" id="requisi" name="requisi" value="<?php echo $productos[$i]['requisi'];?>">
								<input type="hidden" id="prod" name="prod" value="<?php echo $productos[$i]['prod'];?>">
									 <?php echo $productos[$i]['saldo'];?></td>
							<td class="texto8"><input type="text" id="cantidad" name="cantidad" value="<?php echo $productos[$i]['saldo'];?>" onBlur="javascript: revisaSaldo(<?php echo $productos[$i]['saldo'];?>,this)"></td>
							<td class="texto8"><input type="text" id="obs" name="obs" value="<?php echo $productos[$i]['nomprod'].", ". $productos[$i]['descrip'];?>" width="100%" style="width:100%; "> </td>		
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</td>
		</tr>
		<tr><td class="subtituloverde12">Selección de Proveedores</td></tr>
		<tr>
			<td width="100%">
				<table width="100%">
					<tr>
						<td width="35%"><div align="left" style="z-index:3; position:realtive; width:25%;">     
										<input class="texto8" type="text" id="provname1" name="provname1" style="width:400px; " onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="4" value="<?php if(strlen($nomprov1)>=0) echo $nomprov1; else echo 'Proveedor 1';?>">
										<input type="hidden" id="provid1" name="provid1" value ="<?php if(strlen($prov1)>0) echo $prov1; else echo '0'; ?>">
										<input type="hidden" id="emailocu" name="emailocu" value ="">
										<div id="search_suggestProv" style="z-index:4; width:400px;" > </div>
									</div>  </td>
						<td width="65%"><input type="button" value="+" onClick="addProveedor();"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100%">
				<table width="100%">
					<thead>
						<tr class="subtituloverde">
							<th width="50%">Proveedor</th>
							<th width="25%">E-mail</th>
							<th width="2%">&nbsp;</th>
						</tr>
					</thead>
					<tbody id="proveedores" name="proveedores">
					
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center"><input type="button" name="guardar" id="guardar" value="Enviar Cotizaciones" onClick="guardarYEnviarCotizaciones()"></td>
		</tr>
	</table >
</form>
</body>
</html>
