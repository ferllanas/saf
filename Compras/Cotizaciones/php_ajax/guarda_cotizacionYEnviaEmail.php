<?php
include("../../../PHPMailer_v5.1/class.phpmailer.php");
include("../../../PHPMailer_v5.1/class.smtp.php"); // note, this is optional - gets called from main class if not already loaded
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$usuario = '001981';//$_COOKIE['ID_my_site'];

$fails=false;
$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

//print_r($_POST['Aproductos']);
//print_r($_POST['Aprovedores']);
//echo $usuario;
if(isset($_POST['Aproductos']) && isset($_POST['Aprovedores']) && strlen($usuario)>0)
{
	$productos="";//variable que almacena la cadena de los productos
	if(isset($_POST['Aproductos']))
		$productos=$_POST['Aproductos'];
	
	$proveedores="";//variable que almacena la cadena de los productos
	if(isset($_POST['Aprovedores']))
		$proveedores=$_POST['Aprovedores'];
	
	$requisi=$productos[0]['requisi'];


	list($id,$fecha,$fails,$hora) =  fun_creaCotizacion($requisi,$usuario);
	if(!$fails)
	{
		for($i=0;$i<count($productos) && $fails==false;$i++)
		{
			list($iddcotiza,$fails) = fun_compras_A_dcotiza($id,$productos[$i]['iddrequisi'],$productos[$i]['cantidad'], $productos[$i]['obs']);
			if(!$fails)
			{
				for($j=0;$j<count($proveedores) && $fails==false;$j++)
				{
					//echo $proveedores[$j]['inputIdProv'].",".$proveedores[$j]['emailprov'];
					$fails = fun_compras_A_dcotizaprov($id,$iddcotiza,$proveedores[$j]['inputIdProv'],$proveedores[$j]['emailprov'], '0','0', 'X');
				}
			}
		}

		if(!$fails)
		{
			for($j=0;$j<count($proveedores) && $fails==false;$j++)
			{
				$path_Cotizacion="http://200.94.201.196/SAF/Compras/Cotizaciones/Cotizacion.php?p=".$proveedores[$j]['inputIdProv']."&c=".$id;
				
				$fails= armaYEnviaEmail($proveedores[$j]['emailprov'],$proveedores[$j]['nomprov'], $path_Cotizacion);
			}
		}
	}
}
else
{
	echo "Error no recibio datos;";
}

echo $fails;

function fun_creaCotizacion($requi,$usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_mcotiza(?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$requi,&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)." sp_compras_A_mcotiza ".$requi.",".$usuario);
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			$fecha = $row['fecha'];
			$hora = $row['hora'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($id,$fecha,$fails,$hora);
}

function fun_compras_A_dcotiza($cotizacion,$iddrequisi,$cantidad, $observacion)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_dcotiza(?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$cotizacion,&$iddrequisi,&$cantidad,&$observacion);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)." sp_compras_A_mrequisi2 ".print_r($params,true). $entregaen.",".$observaciones.",".$usuario);
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	sqlsrv_close($conexion);
	return array($id,$fails);
}

function fun_compras_A_dcotizaprov($cotizacion,$iddcotiza,$prov, $email, $cantidad,$precio, $observacion)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$id = "";
	$hora = "";

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_dcotizaprov(?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$cotizacion,&$iddcotiza,&$prov, &$email, &$cantidad,&$precio, &$observacion);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)."sp_compras_A_dcotizaprov (".$cotizacion.",".$iddcotiza.",".$prov.",".$email.",".$cantidad.",".$precio.",".$observacion);
	}
	
	
	sqlsrv_close( $conexion);
	return $fails;
}
/////////


function armaYEnviaEmail($email, $nomprov, $path_Cotizacion)
{
	$mail             = new PHPMailer();
	
	$body             = "
<html>
	<head>
		<title>E-Mail HTML</title>
	</head>
	<body>
		<table>
			<tr><td>FOMENTO METROPOLITANO DE MONTERREY</td></tr>
			<tr><td>FOMERREY</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>Solicitud de Cotización</td></tr>
			<tr><td><a href='$path_Cotizacion'>Click aqui para llenar cotización</a></td></tr>
		</table>
	</body>
</html>";//file_get_contents('contents.html');//$mail->getFile('contents.html');
	$body             = eregi_replace("[\]",'',$body);
	
	$mail->IsSMTP();
	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier GMAIL
	$mail->Host       = "mail.fomerrey.gob.mx";//"smtp.gmail.com";      // sets GMAIL as the SMTP server
	$mail->Port       = 2525;//465;                   // set the SMTP port
	
	$mail->Username   = "fernando.llanas@fomerrey.gob.mx";//"jfllanas@gmail.com";// GMAIL username
	$mail->Password   = "F163fer";            //"salvadorllanas27";// GMAIL password
	
	$mail->From       = "fernando.llanas@fomerrey.gob.mx"; //"jfllanas@gmail.com";//
	$mail->FromName   = "Departamento de Compras Fomerrey";
	$mail->Subject    = "Solicitud de Cotización FOMERREY";
	$mail->AltBody    = "Solicitud de Cotización: \n $path_Cotizacion"; //Text Body
	$mail->WordWrap   = 50; // set word wrap
	
	$mail->MsgHTML($body);
	
	$mail->AddReplyTo("fernando.llanas@fomerrey.gob.mx" , "Webmaster");
	
	//$mail->AddAttachment("/path/to/file.zip");             // attachment
	//$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment
	
	$mail->AddAddress($email,$nomprov);//"First Last");
	//$mail->AddAddress("gabriela.flores@fomerrey.gob.mx","First Last");
	$mail->IsHTML(true); // send as HTML
	
	if(!$mail->Send()) {
	  echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
	  echo "Message has been sent";
	}

}
?>