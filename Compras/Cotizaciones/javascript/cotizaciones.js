function calcularSubtotalEnRenglon(rowindex)
{
	var table=document.getElementById("precios");
	
	var cantidad=0.0;
	var precio=0.0;
	var cell = table.rows[rowindex].cells[j];
	for (var j = 0; j <table.rows[rowindex].cells.length ;j++)
	{
		var cell = table.rows[rowindex].cells[j];
		for (var k = 0; k < cell.childNodes.length; k++) 
		{
			var mynode = cell.childNodes[k];
											
			if(mynode.name=="cantidad")
				if(mynode.value.length>0)
					cantidad = parseFloat(mynode.value.replace(/,/g,""));
				
			if(mynode.name=="precio")
				if(mynode.value.length>0)
					precio=  parseFloat(mynode.value.replace(/,/g,""));
				
			
		}
	}
	document.getElementById('subtotal').value =  new oNumero(redondeo2decimales(cantidad * precio)).formato(2,true);

	sumTotalDProveedores();
}

function sumTotalDProveedores()
{
	var table=document.getElementById("precios");
	
	var existe=false;
	
	var sumprov1 = 0.0;

	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length && existe==false; i++) 
	{  //Iterate through all but the first row
		for (var j = 0; j <table.rows[i].cells.length && existe==false ;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="subtotal")
					if(mynode.value.length>0)
						sumprov1 = sumprov1 + parseFloat(mynode.value.replace(/,/g,""));
					
				
			}
		}
	}
	
	
	document.getElementById('total').value = redondeo2decimales(sumprov1);
	asignarFormato(document.getElementById('total'));
}

function validaForma()
{
	var cotizacionfile = document.getElementById('cotizacionfile').value;
	if(cotizacionfile.length<=0)
	{	alert("Favor de agregar cotización.");
		return(false);
	}

	var formCotizacion= document.getElementById('formCotizacion');
	formCotizacion.action = "php_ajax/guardarcotizacion.php";
	formCotizacion.submit();
}
//Objeto oNumero
function oNumero(numero)
{
	//Propiedades
	this.valor = numero || 0
	this.dec = -1;
	//Métodos
	this.formato = numFormat;
	this.ponValor = ponValor;
	//Definición de los métodos
	function ponValor(cad)
	{
		if (cad =='-' || cad=='+') return
			if (cad.length ==0) return
			if (cad.indexOf('.') >=0)
				this.valor = parseFloat(cad);
			else
				this.valor = parseInt(cad);
	}
	function numFormat(dec, miles)
	{
		var num = this.valor, signo=3, expr;
		var cad = ""+this.valor;
		var ceros = "", pos, pdec, i;
		for (i=0; i < dec; i++)
			ceros += '0';

		pos = cad.indexOf('.')
		if (pos < 0)
		{
				cad = cad+"."+ceros;
		}
		else
		{
			pdec = cad.length - pos -1;
			if (pdec <= dec)
			{
				for (i=0; i< (dec-pdec); i++)
					cad += '0';
			}
			else
			{
				num = num*Math.pow(10, dec);
				num = Math.round(num);
				num = num/Math.pow(10, dec);
				cad = new String(num);
			}
		}
		//alert("Valorcito:"+cad)
		pos = cad.indexOf('.');

		if (pos < 0 || pos == undefined) 
			pos = cad.length;
		
		if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
			   signo = 4;
		
		if (miles && pos > signo)
			do
			{
					expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
					cad.match(expr)
					cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
			}while (cad.indexOf(',') > signo)
			
		if (dec<0) 
			cad = cad.replace(/\./,'')
		return cad;
	}
}//Fin del objeto oNumero:

function redondeo2decimales(numero)
{
	var original=parseFloat(numero);
	var result=Math.round(original*100)/100 ;
	return result;
}

function asignarFormato(objecto)
{
	if(objecto.value.indexOf(".",0)>0)
	{
		var numero = new oNumero(objecto.value.replace(/,/g,""));
		
		var numDec = (objecto.value.length-1) - objecto.value.indexOf(".",0);
		if(numDec<=2)
			objecto.value= numero.formato(numDec,true);
		else
			objecto.value= numero.formato(2,true);
	}
	else
	{
		var numero = new oNumero(objecto.value.replace(/,/g,""));
		objecto.value= numero.formato(-1,true);
	}
}

function revisaSaldo(saldo, object)
{
	if(saldo<object.value)
	{	
		object.value=saldo;
		alert("La cantidad debe del producto debe ser menor o igual a "+saldo);
		object.focus()
	}
}

function addProveedor()
{
	var provname1= document.getElementById('provname1').value;
	var provid1=  document.getElementById('provid1').value;
	var emailocu= document.getElementById('emailocu').value;

	var Table = document.getElementById('proveedores');
	var contRow = Table.rows.length;
	var d= contRow % 2;
	//Create the new elements
	var NewRow = document.createElement("tr");
	//NewRow.class="d<?php echo ($i % 2);?>"
	NewRow.setAttribute('class', 'd'+d);

	var cellNomProv = document.createElement("td");
	var cellemailprov = document.createElement("td");
	var celleliminarprov = document.createElement("td");
	

	var inputnomprov = document.createElement("input");
	inputnomprov.type="text";
	inputnomprov.id="nomprov";
	inputnomprov.name="nomprov";
	inputnomprov.value=provname1;
	inputnomprov.style.width="90%";
	inputnomprov.className = 'caja_toprint';
	inputnomprov.readOnly=true;

	var inputEmailprov = document.createElement("input");
	inputEmailprov.type= "text";
	inputEmailprov.id= "emailprov";
	inputEmailprov.name= "emailprov";
	inputEmailprov.value= emailocu;
	inputEmailprov.style.width="90%";

	var inputIdProv = document.createElement("input");
	inputIdProv.type= "hidden";
	inputIdProv.id= "IdProv";
	inputIdProv.name= "IdProv";
	inputIdProv.value= provid1;

	var inputBotonElim = document.createElement("input");
	inputBotonElim.type="button";
	inputBotonElim.title="javascript:eliminar_Proveedor(event,this);";
	inputBotonElim.value="-";
	inputBotonElim.setAttribute("onClick", "eliminar_Proveedor(event,this);");
	
	//Add textboxes to cells
	cellNomProv.appendChild(inputnomprov);
	cellNomProv.appendChild(inputIdProv);
	cellemailprov.appendChild(inputEmailprov);
	celleliminarprov.appendChild(inputBotonElim);

	NewRow.appendChild(cellNomProv);
	NewRow.appendChild(cellemailprov);
	NewRow.appendChild(celleliminarprov);

	//Add row to table
	Table.appendChild(NewRow);

	document.getElementById('provname1').value="";
	document.getElementById('provid1').value="";
	document.getElementById('emailocu').value="";
	//document.getElementById('txtSearch').value="";
	//document.getElementById('busProd').value="";
}

function eliminar_Proveedor( e , s )
{
	if(!confirm("Seguro desea eliminar de la lista de proveedores a cotizar."))
		return false;
	//var table=document.getElementById( 'tmenus' );
	var rowindexA= s.parentNode.parentNode.rowIndex-1;
	//alert(rowindexA+","+ document.getElementById("menus").rows.length);
	document.getElementById("proveedores").deleteRow( rowindexA );
}

function getProductos()
{
	
	var table = document.getElementById('productos');
	var contRow = table.rows.length;
	var TotaProd= new Array( );

	var seenvia=false;
	var iddrequisi="";
	var prod="";
	var requisi="";
	var cantidad="";
	var obs="";
	var countPartidas = 0;
	for (var i = 0; i < table.rows.length ; i++) 
	{  
		//cadena_productosCcompara+='array(';
		for (var j = 0; j <table.rows[i].cells.length  ;j++)
		{
			
			var cell = table.rows[i].cells[j];
			
			for (var k = 0; k < cell.childNodes.length ; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="cheEnviar")
					seenvia = mynode.checked;	

				if(mynode.name=="iddrequisi")
					iddrequisi = mynode.value;		
				
				if(mynode.name=="requisi")
					requisi = mynode.value;		
		
				if(mynode.name=="prod")
					prod = mynode.value;

				if(mynode.name=="cantidad")
					cantidad = mynode.value;

				if(mynode.name=="obs")
					obs = mynode.value;
	
			}
		}

		if( seenvia==true  )
		{
			//alert("entro "+ i);
			TotaProd[countPartidas] = new Object;
			TotaProd[countPartidas]['iddrequisi']	= iddrequisi;
			TotaProd[countPartidas]['requisi']			= requisi;			
			TotaProd[countPartidas]['prod']		= prod;
			TotaProd[countPartidas]['cantidad']	= cantidad;
			TotaProd[countPartidas]['obs']	= obs;
			countPartidas++;
		}

	}

return TotaProd;
}

function getProveedores()
{
	
	var table = document.getElementById('proveedores');
	var contRow = table.rows.length;
	var TotaProv= new Array( );

	var seenvia=false;
	var iddrequisi="";
	var prod="";
	var requisi="";
	var cantidad="";
	var inputIdProv=""
	var	emailprov="";
	var nomprov="";
	var countPartidas = 0;
	for (var i = 0; i < table.rows.length ; i++) 
	{  
		//cadena_productosCcompara+='array(';
		for (var j = 0; j <table.rows[i].cells.length  ;j++)
		{
			
			var cell = table.rows[i].cells[j];
			
			for (var k = 0; k < cell.childNodes.length ; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="IdProv")
					inputIdProv = mynode.value;	
			
				if(mynode.name=="emailprov")
					emailprov = mynode.value;	
			
				if(if(mynode.name=="nomprov")
					nomprov  = mynode.value;
			}
		}

		
		TotaProv[countPartidas] = new Object;
		TotaProv[countPartidas]['inputIdProv']	= inputIdProv;
		TotaProv[countPartidas]['emailprov']	= emailprov;
		TotaProv[countPartidas]['nomprov']	= nomprov;
		//alert(TotaProv[countPartidas]['emailprov']+", "+TotaProv[countPartidas]['inputIdProv']);
		countPartidas++;

	}

return TotaProv;
}


function guardarYEnviarCotizaciones()
{

	var prodACotizar = getProductos();
	var provACotizar = getProveedores();
	var requisicion = document.getElementById('txtSearch').value;
	alert(requisicion);
/*	for(var i=0;i<prodACotizar.length;i++)
	{
		if(prodACotizar[i]['emailprov'].length<=0)
		{	alert("Error no existen datos de un producto."); return false;}
	}
*/
	for(var i=0;i<provACotizar.length;i++)
	{
		//alert(provACotizar[i]['emailprov']);
		if(provACotizar[i]['emailprov'].length<=0)
		{	alert("Existe un proveedor sin especificar su email."); return false;}
	}
	var datas = {
				Aproductos : prodACotizar,			
				Aprovedores: provACotizar
        };
		//requisi: requisicion
		//alert(datas);
        jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/guarda_cotizacionYEnviaEmail.php',
				data:          datas,
				success: function(resp) 
					{
						alert(resp);
						/*if( resp.length ) 
						{
							var myArray = eval(resp);
							if(myArray.length>0)
							{
								//alert(myArray[0].fallo);
								if(myArray[0].fallo==false)
								{
									var id_Ccomp = myArray[0].id;
									alert("El Cuadro Comparativo "+id_Ccomp+" se Realizo Satisfactoriamente!.");
									//alert("ordendCompra.php?id_ccomp="+id_Ccomp);
									location.href="ordendCompra.php?id_ccomp="+id_Ccomp;
									window.open('php_ajax/crear_ccomp_pdf.php?id='+id_Ccomp,'Comparativo','width=800,height=600');
									//validapresupuesto(id_Ccomp);
									document.getElementById('id_ccomp').value=id_Ccomp;
									
									//document.getElementById('id_Ccomp').style.visibility='visible';
									
									//document.form_Ccompara.submit();//document.getElementById('form_requisicion').submit;
									//location.reload(true);
									//document.getElementById('GenOrComp').disabled=false;
								}
								else
								{
									alert("Error en proceso de actualizacion!.");
								}
							}
						}*/
					}
			   });
}