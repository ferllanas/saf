<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);

$prov="";
if(isset($_REQUEST['p']))
{
	$prov=$_REQUEST['p'];
}

$cotizacion="";
if(isset($_REQUEST['c']))
{
	$cotizacion=$_REQUEST['c'];
}

$conexion = sqlsrv_connect($server,$infoconexion);
if( $conexion)
{
	$command= "SELECT * FROM compramprovs WHERE prov=$prov";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['prov']= trim($row['prov']);
			$datos[$i]['oldprov']= utf8_decode($row['oldprov']);
			$datos[$i]['nomprov']= utf8_decode($row['nomprov']);
			$datos[$i]['contacto']= utf8_decode($row['contacto']);
			$datos[$i]['puesto']= utf8_decode($row['puesto']);
			$datos[$i]['calle']= utf8_decode($row['calle']);
			$datos[$i]['colonia']= utf8_decode($row['colonia']);
			$datos[$i]['ciudad']= utf8_decode($row['ciudad']);
			$datos[$i]['tel_1']=trim($row['tel_1']);
			$datos[$i]['tel_2']= trim($row['tel_2']);
			$datos[$i]['tel_3']= trim($row['tel_3']);
			$datos[$i]['rfc']= trim($row['rfc']);
			$datos[$i]['email']= trim($row['email']);
			//$datos[$i]['cveinterbco']=$row['cveinterbco'];
			//$datos[$i]['cta_banco']=$row['cta_banco'];
			//$datos[$i]['banco']=$row['banco'];
			$i++;
		}
	}

	$command= "SELECT c.obsfome,e.unidad, a.cotiza, b.id as idcotizaprov, b.cantprov, c.cantfome
				FROM compramcotiza a 
				INNER JOIN compradcotizaprov b ON a.cotiza=b.cotiza
				INNER JOIN compradcotiza c ON c.id=b.iddcotiza
				INNER JOIN compradrequisi d ON d.id=c.iddrequisi
				INNER JOIN compradproductos e ON e.prod=d.prod 
				WHERE a.cotiza=$cotizacion AND b.prov=$prov AND a.estatus<30";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$drequisi[$i]['id']= trim($row['id']);
			$drequisi[$i]['requisi']= utf8_decode(trim($row['requisi']));
			$drequisi[$i]['nomprod']= utf8_decode(trim($row['nomprod']));
			$drequisi[$i]['descrip']= utf8_decode(trim($row['descrip']));
			$drequisi[$i]['unidad']= utf8_decode($row['unidad']);
			$drequisi[$i]['cantidad']= utf8_decode($row['cantidad']);
			$i++;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Cotizaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="javascript/cotizaciones.js"></script>
</head>

<body>
<form id="formCotizacion" name="formCotizacion" enctype="multipart/form-data">
<table width="100%">
	<tr>
			<td class="TituloDForma" width="100%">Cotizaciones<hr class="hrTitForma"></td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr><td class="subtituloverde12" colspan="2">Información de Proveedor</td></tr>
				<tr><td class="texto8" colspan="2">Nombre: <?php echo $datos[0]['nomprov'];?></td></tr>
				<tr><td class="texto8" colspan="2">Contacto: <?php echo $datos[0]['contacto'];?></td></tr>
				<tr><td class="texto8" colspan="2">Dirección: <?php echo $datos[0]['calle'];?>, <?php echo $datos[0]['colonia'];?>, <?php echo $datos[0]['ciudad'];?></td></tr>
				<tr><td class="texto8" colspan="2">RFC: <?php echo $datos[0]['rfc'];?></td></tr>
				<tr><td class="texto8" colspan="2">Tefonos: <?php echo $datos[0]['tel_1'];?>, <?php echo $datos[0]['tel_2'];?>, <?php echo $datos[0]['tel_3'];?></td></tr>
				<tr><td class="texto8" width="50%">E-mail: <?php echo $datos[0]['email'];?></td> <td class="texto8" width="50%" align="left">Tiepo de entrega:<input type="text" id="tiementrega" name="tiementrega" value="30 DIAS" align="center"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="subtituloverde12">Requisición</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<thead>
					<th class="subtituloverde8">Producto</th>
					<th class="subtituloverde8">Unidad</th>
					<th class="subtituloverde8">Cantidad</th>
					<th class="subtituloverde8">Descripción</th>
					<th class="subtituloverde8">Cantidad a cotizar</th>
					<th class="subtituloverde8">Precio</th>
					<th class="subtituloverde8">Subtotal</th>
				</thead>
				<tbody  class="resultadobusqueda" id="precios">
					<?php
						if(count($drequisi)>0)
						for($i=0;$i<count($drequisi);$i++)
						{
					?>
					<tr id="filaOculta" class="d<?php echo ($i % 2);?>">
						<td class="texto8" width="25%" align="left"><?php echo $drequisi[$i]['nomprod']; if(strlen($drequisi[0]['descrip'])>0) echo ", ".$drequisi[0]['descrip'];?></td>
						<td class="texto8" width="10%"><?php echo $drequisi[$i]['unidad'];?></td>
						<td class="texto8" width="10%"><?php echo $drequisi[$i]['cantidad'];?></td>
						<td class="texto8" width="25%"><input type="text" name="descripcion" id="descripcion" value="" width="100%" style="width:100%; "></td>
						<td class="texto8" width="10%"><input type="text" name="cantidad" id="cantidad" value="<?php echo $drequisi[$i]['cantidad'];?>" onKeyUp="javascript:calcularSubtotalEnRenglon(<?php echo $i;?>);" width="100%" style="width:100%; "></td>
						<td class="texto8" width="10%"><input type="text" name="precio" id="precio" value="0.0" width="100%" style="width:100%; " onKeyUp="javascript:calcularSubtotalEnRenglon(<?php echo $i;?>);"></td>
						<td class="texto8" width="10%"><input type="text" name="subtotal" id="subtotal" readonly="true" value="0.0" width="100%" style="width:100%; " class="caja_toprint" align="middle"></td>
					</tr>
				<?php }
						else
							echo "<tr><td>La cotizacion ya ha sido llenada</td></tr>
				?>
				</tbody>
				<tfoot>
					<tr>
						<TD><input type="button" onClick=""></TD>
						<TD>&nbsp;</TD>
						<TD align="right" title="Es necesario anexar la cotización con firma y folio del proveedor.">Cotización </TD>
						<TD align="left" title="Es necesario anexar la cotización con firma y folio del proveedor."> <input type="file" name="cotizacionfile" id="cotizacionfile"></TD>
						<TD>&nbsp;</TD>
						<TD align="right">TOTAL:</TD>
						<TD align="left"><input id="total" name="total" type="text" class="caja_toprint texto12" value="0.0" align="middle"></TD>
					</tr>
				</tfoot>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center"><input type="button" id="file" name="file" value="Enviar Cotización" onClick="validaForma()"></td>
	</tr>
</table>
</form>
</body>
</html>
