
<?php
//include('../../../pdf/class.ezpdf.php');
require_once("../../../dompdf/dompdf_config.inc.php");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");

	$Entregaren= "";
	$observaciones= "";
	$fecha_req="";
	$hora_req= "";
	$numeconomico= "";
	$placa= "";
			
	$usuario = "0";//$_COOKIE['ID_my_site'];//"001110";//
	$nomEncuestador="";
	
	$datos=array();
	$datos[0]['fallo']=true;
	
	$idReq="";
	if(isset($_REQUEST['id_req']))
		$idReq = $_REQUEST['id_req'];

	$numeconomico= "";
	$placa= "";

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$command="SELECT *, convert(varchar(10),falta,110) as fech , convert(varchar(10),halta,114) as hor FROM compramrequisi where requisi=$idReq";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode=  $command." ". FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//print_r($row);
			$Entregaren= trim($row['entregaen']);
			$observaciones= trim($row['observa']);
			$fecha_req= trim($row['fech']);
			$hora_req= trim($row['hor']);
			$usuario= trim($row['usuario']);
			$numeconomico= trim($row['numeconomico']);
			$placa= trim($row['placa']);
			$i++;
		}
	}
	//sqlsrv_close( $conexion);


$firmaquien="";
if(isset($_REQUEST['firmaquien']))
	$firmaquien=$_REQUEST['firmaquien'];
			

$numempfirmaquien="0";
if(isset($_REQUEST['numempfirmaquien']))
	$numempfirmaquien=$_REQUEST['numempfirmaquien'];
									
$autoriza="";
$vobo="";
$APRUEBA="";
$AREA_AUT_REQ="";
$AREA_VOBO_REQ="";
$areadirector="";
list($autoriza,$vobo, $APRUEBA, $AREA_AUT_REQ, $AREA_VOBO_REQ)=fun_ObtieneFirmasAutoriza($usuario);

	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
list($depto,$coord ,$dir,$nomdepto ,$nomdir)=fun_ObtieneCabezalesSolicitante($usuario);

$numCalreqiosi=fun_ObtieneNumCalidadReq();

list($directorName, $areadirector) = fun_ObtieneFirmasDirector($numempfirmaquien);

//Arma fecha
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$month="";
$days="";
$years="";
$horayseg="";
$milseg="";
if(strlen($fecha_req)>0)
{
	list($month,$days,$years)=explode('-', $fecha_req);
	list($horayseg,$milseg)=explode('.', $hora_req);
}
//echo $horayseg.",".$milseg;
$diadesem="";
$ffecha = "";

if(strlen($month))
{
	$diadesem=date("w");
	$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
}

$datos= func_ObtenerProdDRequisi($idReq);//Obtiene los productos de la requisicion




$dompdf = new DOMPDF();
//$pdf->page_text(60, 10, $chapter, $font, 10, array(0,0,0)); $font = Font_Metrics::get_font("Helvetica", "normal"); 
$pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	$textod=utf8_encode("P�gina {PAGE_NUM}");
	$textop=utf8_encode("Requisici�n");
	$pdf->page_text(25, 760, "$textop '.$idReq.'", "", 12, array(0,0,0)); 
	$pdf->page_text(550, 760, "Pagina {PAGE_NUM}", "", 12, array(0,0,0)); 
	
 } 

</script> ';  


//echo $observaciones;
$str="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
			margin:  0.25in 0.5in 0.5in 0.5in;
		}
		
		td {padding: 0;}
		
		#twmaarco
		{
			border:1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		
		.texto16b{ 
			font-size:16pt;
			font-weight:bold;
		}
		.texto14{ 
			font-size:14pt;
		}
		.texto14b{ 
			font-size:15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size:12pt;
			font-weight:bold;

		}
		.texto12{ 
			font-size:12pt;
		}
		.texto11{
			font-size:11pt;
		}
		.texto10{
			font-size:10pt;
		}
		</style>

</head>

<body>
<table width='100%'>
	<tr>
		<TD align='right' class='texto12'>$numCalreqiosi</TD>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='25%'><img src='../../../$imagenPDFPrincipal' style='width:150px; height:57px'></td>
					<td width='75%'>
						<table width='100%'>
							<tr>
								<td align='left' class='texto16b'><b>Fomento Metropolitano de Monterrey</b></td>
							</tr>
							<tr>
								<td align='left' class='texto16b'><b>".utf8_encode("Direcci�n de Administraci�n y Finanzas")."</b></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='right' class='texto14' colspan=2>".utf8_encode("Requisici�n")." No. $idReq</td>
				</tr>
				<tr>
					<td align='right' class='texto10' colspan=2><strong>Fecha:</strong> $ffecha, $horayseg</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align='left'>
			<table width='100%'>
				<tr>
					<td width='50' class='texto12bold'><b>Solicitante:</b></td><td class='texto12bold'><b>$depto $nomdepto</b></td>
				</tr>
				<tr>
					<td>&nbsp;</td><td class='texto12bold'><b>$dir $nomdir</b></td>
				</tr>
			</table
		></td>
	</tr>
	<tr>
		<td width='100%' >
			<table width='100%' name='twmaarco' id='twmaarco'>
				<tr>
					<td width='90' style='border-left: 0pt ; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'><strong>Entregar En</strong></td>
					<td style='border-left: 1pt solid black; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'>".htmlspecialchars($Entregaren)."</td>
				</tr>
				<tr>
					<td width='90' style='border-left: 0pt solid black; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'><strong>Observaciones / Uso y fecha en que se requiere el material</strong></td>
					<td style='border-left: 1pt solid black; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'>".htmlspecialchars($observaciones);
		if(strlen($numeconomico)>0 && (int)$numeconomico>0)
				$str.= utf8_encode(",N�mero Economico:")."".$numeconomico;

		if(strlen($placa)>0 && (int)$numeconomico>0)
				$str.=", PLACA: ".$placa;

				$str.="</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<thead>
					<tr>
						<th width='10%' class='texto12bold'>Cantidad</th>
						<th width='10%' class='texto12bold'>Unidad</th>
						<th width='70%' class='texto12bold'>".utf8_encode("Descripci�n")."</th>
					</tr>
					<tr>
						<th width='10%'><hr></th>
						<th width='10%'><hr></th>
						<th width='70%'><hr></th>
					</tr>
				</thead>

				<tbody>";
					for($i=0;$i<count($datos);$i++)
					{
						$str.='	<tr>
								<td align="center" valign="top">'.$datos[$i]['cantidad'].'</td>
								<td align="center" valign="top">'.$datos[$i]['unidad'].'</td>
								<td>'.$datos[$i]['Descripcion'].'</td>
							</tr>';
					}

				$str.="</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<table width='100%'>
				<tr>
					<td width='130px'><hr></td>
					<td width='10px'>&nbsp;</td>
					<td width='130px'><hr></td>
					<td width='10px'>&nbsp;</td>
					<td width='130px'><hr></td>
					<td width='10px'>&nbsp;</td>
					<td width='130px'><hr></td>
				</tr>
				<tr>
					<td width='130px' align='center'>Area Solicitante</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center' style='font-size:8pt'>".htmlspecialchars(utf8_encode("Aprobaci�n"))."</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center' style='font-size:8pt'>".htmlspecialchars(utf8_encode("Autorizaci�n"))."</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center' style='font-size:8pt'>Vo. Bo.</td>
				</tr>
				<tr>
					<td width='130px' align='center' style='font-size:8pt'></td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center'>".htmlspecialchars(utf8_encode("Director Area Solicitante"))."</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center' ><table width='100%'><tr><td align='center' >".htmlspecialchars(utf8_encode("Director Administraci�n"))." </td></tr><tr><td align='center' >y Finanzas</td></tr></table></td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center'>".htmlspecialchars(utf8_encode($AREA_VOBO_REQ))."</td>
				</tr>
				<tr>
					<td width='130px' align='center'>".htmlspecialchars($firmaquien)."</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center'>".htmlspecialchars($directorName).//htmlspecialchars($APRUEBA). //. cambio 13/06/2012 01:01:01 pm
					"</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center'>".htmlspecialchars($autoriza)."</td>
					<td width='10px'>&nbsp;</td>
					<td width='130px' align='center'>".htmlspecialchars($vobo)."</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
".$pageheadfoot."
</body>
</html>";

//echo	$str;

$dompdf->load_html($str);

$dompdf->render();
	  
$pdf = $dompdf->output(); 
file_put_contents("../pdf_files/".$idReq.".pdf", $pdf);

//$dompdf->stream("../pdf_files/".$idReq.".pdf");// $domPDF->stream($pdf, array('Attachment'=>0));
$datos[0]['path']="../pdf_files/".$idReq.".pdf";
$datos[0]['fallo']=false;
$datos[0]['id']=$idReq;

fun_cambiaPathDeRequisicion($datos[0]['path'],$idReq);

echo json_encode($datos);

//Funcion que llama procedimiento que obtiene codigo de requisicion
function fun_ObtieneNumCalidadReq()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=10;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			echo tsql_callSP;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}

//
function func_ObtenerProdDRequisi($idReq)
{
	global $server,$odbc_name,$username_db ,$password_db;
	
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$command="SELECT  dr.unidad, dr.cantidad, p.prod,p.nomprod, p.ctapresup, c.NOMCTA,dr.descrip FROM compradrequisi dr LEFT JOIN compradproductos p ON dr.prod=p.prod LEFT JOIN presupcuentasmayor c ON p.ctapresup=c.cuenta where c.estatus<'9000' AND dr.requisi='$idReq' ORDER BY dr.mov ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode=  $command." ". FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['Descripcion']= trim(htmlspecialchars(utf8_encode($row['nomprod'])))." ".trim(htmlspecialchars ($row['descrip']));
			//$datos[$i]['cuenta']= trim($row['ctapresup']);
			//$datos[$i]['Nombre']= trim($row['concepto_2']);
			//$datos[$i]['Nombre']= trim($row['NOMCTA']);
			$i++;
		}
	}
	sqlsrv_close( $conexion);
	
	return $datos;
}
///Funcion que cambia la ruta de la requisicion
function fun_cambiaPathDeRequisicion($path,$idrequesi)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
			
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="UPDATE compramrequisi SET path='$path' WHERE requisi='$idrequesi'";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		echo $tsql_callSP." ";
		 die( print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;

}

function fun_ObtieneCabezalesSolicitante($usuario)
{
global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
			
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto COLLATE DATABASE_DEFAULT where numemp=$usuario";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 echo $tsql_callSP ;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			$depto = $row['depto'];
			$coord = $row['coord'];
			$dir = $row['dir'];
			$nomdepto = $row['nomdepto'];
			$nomdir = $row['nomdir'];
			//$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($depto,$coord ,$dir,$nomdepto ,$nomdir );

}
//Funcion que llama procedimiento almacenado para obtener	
function fun_ObtieneFirmasAutoriza($usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="sp_Autoriza_requisi";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	$APRUEBA="";
	$AREA_AUT_REQ="";
	$AREA_VOBO_REQ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			$autoriza = $row['AUTORIZA'];
			$VoBo = $row['VOBO'];
			$APRUEBA=$row['APRUEBA'];
			$AREA_AUT_REQ=$row['AREA_AUT_REQ'];
			$AREA_VOBO_REQ=$row['AREA_VOBO_REQ'];
			//$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($autoriza,$VoBo, $APRUEBA, $AREA_AUT_REQ, $AREA_VOBO_REQ);
}

//Funcion que llama procedimiento almacenado para obtener	
function fun_ObtieneFirmasDirector($numemp)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$director="";
	$areadirector="";
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_director_area(?)}";//Arma el procedimeinto almacenado
	$params = array(&$numemp);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$director = $row['director'];
			$areadirector = $row['areadirector'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($director,$areadirector);

}
?>

