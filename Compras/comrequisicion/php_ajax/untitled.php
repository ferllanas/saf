<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
			margin:  0.25in 0.5in 0.5in 0.5in;
		}
		
		td {padding: 0;}
		
		#twmaarco
		{
			border: 1pt solid black;
		}
		
		.textobold{
			font-weight:bold;
		}
		
		.texto16b{ 
			font-size: 16pt;
			font-weight:bold;
font-weight:bold;
		}
		
		.texto14b{ 
			font-size: 15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size: 12pt;
			font-weight:bold;
		}
		.texto12{ 
			font-size: 12pt;
		}
		.texto11{
			font-size: 11pt;
		}
		</style>

</head>

<body>
<table width='100%'>
	<tr>
		<TD align='right' class='texto12'>$numCalreqiosi</TD>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='25%'><img src='../../../<?php echo $imagenPDFPrincipal;?>' style='width:150px; height:57px'></td>
					<td width='75%'>
						<table width='100%'>
							<tr>
								<td align='left' class='texto16b'><b>Fomento Metropolitano de Monterrey</b></td>
							</tr>
							<tr>
								<td align='left' class='texto16b'><b>".utf8_encode("Dirección de Administración y Finanzas")."</b></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='right' class='texto14b' colspan=2>".utf8_encode("Requisición")." No. $idReq</td>
				</tr>
				<tr>
					<td align='right' class='texto12' colspan=2><strong class='texto12bold'>Fecha:</strong> $ffecha, $horayseg</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align='left'>
			<table width='100%'>
				<tr>
					<td width='50' class='texto12bold'><b>Solicitante:</b></td><td class='texto12bold'><b>$depto $nomdepto</b></td>
				</tr>
				<tr>
					<td>&nbsp;</td><td class='texto12bold'><b>$dir $nomdir</b></td>
				</tr>
			</table
		></td>
	</tr>
	<tr>
		<td width='100%' >
			<table width='100%' name='twmaarco' id='twmaarco'>
				<tr>
					<td width='90' style='border-left: 0pt ; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'><strong>Entregar En</strong></td>
					<td style='border-left: 1pt solid black; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'>".utf8_encode($Entregaren)."</td>
				</tr>
				<tr>
					<td width='90' style='border-left: 0pt solid black; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'><strong>Observaciones / Uso y fecha en que se requiere el material</strong></td>
					<td style='border-left: 1pt solid black; border-bottom: 0pt; border-top: 0pt; border-right:0pt;'>".utf8_encode($observaciones)."</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<thead>
					<tr>
						<th width='10%' class='texto12bold'>Cantidad</th>
						<th width='10%' class='texto12bold'>Unidad</th>
						<th width='70%' class='texto12bold'>Descripción</th>
					</tr>
					<tr>
						<th width='10%'><hr></th>
						<th width='10%'><hr></th>
						<th width='70%'><hr></th>
					</tr>
				</thead>

				<tbody>";
					for($i=0;$i<count($datos);$i++)
					{
						$str.='	<tr>
								<td align="center" valign="top">'.$datos[$i]['cantidad'].'</td>
								<td align="center" valign="top">'.$datos[$i]['unidad'].'</td>
								<td>'.utf8_encode($datos[$i]['Descripcion']).'</td>
							</tr>';
					}

				$str.="</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<table width='100%'>
				<tr>
					<td width='150px'><hr></td>
					<td width='50px'>&nbsp;</td>
					<td width='150px'><hr></td>
					<td width='50px'>&nbsp;</td>
					<td width='150px'><hr></td>
				</tr>
				<tr>
					<td width='150px' align='center'>Solicitante</td>
					<td width='50px'>&nbsp;</td>
					<td width='150px' align='center'>Autorización</td>
					<td width='50px'>&nbsp;</td>
					<td width='150px' align='center'>Vo. Bo.</td>
				</tr>
				<tr>
					<td width='150px' align='center'>$firmaquien</td>
					<td width='50px'>&nbsp;</td>
					<td width='150px' align='center'>$autoriza</td>
					<td width='50px'>&nbsp;</td>
					<td width='150px' align='center'>$vobo</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
".$pageheadfoot."
</body>
</html>