<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{
	$tiporeq = $_GET['tiporeq'];
	$terminos = explode(" ",$_GET['q']);
	//$termino=$_GET['q'];
	$command= "SELECT a.prod, a.nomprod, a.cve,a.unidad, a.ctapresup, b.nomcta  FROM compradproductos a LEFT JOIN presupcuentasmayor b ON a.ctapresup=b.cuenta INNER JOIN compradprod_tiporeq c on c.producto=a.prod";
	
	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if(!$paso)
		{
			$command.=" WHERE nomprod LIKE '%".$terminos[$i]."%'";
			$paso=true;	
		}
		else
		{
			$command.=" AND nomprod LIKE '%".$terminos[$i]."%'";	
			$paso=true;	
		}
	}

	if(strlen($tiporeq)>0)
		if($paso)
			$command.=" AND c.tiporeq = $tiporeq";	
		else
			$command.=" WHERE c.tiporeq = $tiporeq";	

	$command.=" AND ctapresup<>'0' AND ctapresup is not null  AND a.estatus<9000 ORDER BY nomprod ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$datos[$i]['id']= trim($row['prod']);
			//$datos[$i]['valor']= htmlentities( utf8_decode( trim($row['nomprod'])));
			//$datos[$i]['unidad']= trim($row['unidad']);
			$sct.=		htmlentities(trim($row['nomprod']))."@".
						htmlentities(trim($row['prod']))."@".
						htmlentities(trim($row['unidad']))."@".
						htmlentities(trim($row['ctapresup']))."@".
						htmlentities(trim($row['nomcta'])).
						"\n";
			$i++;
		}
	}
	echo $sct;

}
?>