<?php


if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");

	$numprov = $_REQUEST['numprov'];
	$fi = $_REQUEST['fi'];
	$ff = $_REQUEST['ff'];
	$fi2 = $_REQUEST['fi2'];
	$ff2 = $_REQUEST['ff2'];
	$sw1 = $_REQUEST['sw1'];
	$sw2 = $_REQUEST['sw2'];
	
	list($dd,$mm,$aa)= explode('/',$fi);
	$fi=$aa.'-'.$mm.'-'.$dd;
				
	list($dd2,$mm2,$aa2)= explode('/',$ff);
	$ff=$aa2.'-'.$mm2.'-'.$dd2;
	
	list($dd,$mm,$aa)= explode('/',$fi2);
	$fi2=$aa.'-'.$mm.'-'.$dd;
				
	list($dd2,$mm2,$aa2)= explode('/',$ff2);
	$ff2=$aa2.'-'.$mm2.'-'.$dd2;	
	$datos=array();
	$consulta ="";
	$excel="";
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if ($conexion)
	{		
		
			$consulta = "select requisi,CONVERT(varchar(12),freq, 103) as freq,orden,CONVERT(varchar(12),forden, 103) as forden,
			prov,nomprov,tot from v_compra_req_orden where ";
			if($numprov>0)
			{
				$consulta .="prov=$numprov";
				if ($sw1==1 || $sw2==1)
				{
					$consulta .= " and ";
				}			
			}

			
			if ($sw1==1)
			{
				$consulta .= "freq >= CONVERT(varchar(12),'$fi', 103) and freq <=CONVERT(varchar(12),'$ff', 103) ";
			}
			
			if ($sw1==1 && $sw2==1)
			{
				$consulta .= " and ";
			}
			
			if ($sw2==1)
			{
				$consulta .= "forden >= CONVERT(varchar(12),'$fi2', 103) and freq <=CONVERT(varchar(12),'$ff2', 103) ";
			}
				
		
		//echo $consulta;
		$R = sqlsrv_query( $conexion,$consulta);
		if ( $R === false)
		{ 
			$resoponsecode="02";
			die($consulta."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($R);
			$i=0;
			//$excel='Requisision'."\t".'Fecha Requisicion'."\t".'Orden de Compra'."\t".'Fecha de Orden de Compra'."\t".'Proveedor'."\t".'Total'."\t"."\n";
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['requisi']= trim($row['requisi']);
				$datos[$i]['freq']= trim($row['freq']);
				$datos[$i]['orden']= trim($row['orden']);
				$datos[$i]['forden']= trim($row['forden']);
				$datos[$i]['prov']= trim($row['prov']);
				$datos[$i]['nomprov']= trim($row['nomprov']);
				$datos[$i]['tot']= number_format(trim($row['tot']),2);
				$datos[$i]['excel'].=$datos2[$i]['requisi']."\t".$datos2[$i]['freq']."\t".$datos2[$i]['orden']."\t".$datos2[$i]['forden']."\t".$datos2[$i]['nomprov']."\t".$datos2[$i]['tot']."\n";
				$i++;
			}
		}
	}
echo json_encode($datos);
?>