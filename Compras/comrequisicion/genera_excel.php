<?php


if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	
	$EXCEL="";
	$fi=01/03/2014;
	$ff=20/03/2014;
	$fi2=01/03/2014;
	$ff2=20/03/2014;

	if(isset($_REQUEST['fi'])<1)
		$sw1=0;
		
	if(isset($_REQUEST['fi2'])<1)
		$sw2=0;
		
	
	if(isset($_REQUEST['numprov']))
		$numprov=$_REQUEST['numprov'];

	if(isset($_REQUEST['fi']))
		$fi = $_REQUEST['fi'];

	if(isset($_REQUEST['ff']))		
		$ff = $_REQUEST['ff'];
	
	if(isset($_REQUEST['fi2']))	
		$fi2 = $_REQUEST['fi2'];
		
	if(isset($_REQUEST['ff2']))		
		$ff2 = $_REQUEST['ff2'];
		
	if(isset($_REQUEST['sw1']))		
		$sw1 = $_REQUEST['sw1'];

	if(isset($_REQUEST['sw2']))		
		$sw2 = $_REQUEST['sw2'];
	
	list($dd,$mm,$aa)= explode('/',$fi);
	$fi=$aa.'-'.$mm.'-'.$dd;
				
	list($dd2,$mm2,$aa2)= explode('/',$ff);
	$ff=$aa2.'-'.$mm2.'-'.$dd2;
	
	list($dd,$mm,$aa)= explode('/',$fi2);
	$fi2=$aa.'-'.$mm.'-'.$dd;
				
	list($dd2,$mm2,$aa2)= explode('/',$ff2);
	$ff2=$aa2.'-'.$mm2.'-'.$dd2;	
	$datos=array();
	$datos2=array();
	$consulta ="";
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	if ($numprov>0)
	{
		
		if ($conexion)
		{		
			$consulta .= "select requisi,CONVERT(varchar(12),freq, 103) as freq,orden,CONVERT(varchar(12),forden, 103) as forden,prov,nomprov,tot from v_compra_req_orden where ";
			if($numprov>0)
			{
				$consulta .="prov=$numprov";
				if ($sw1==1 || $sw2==1)
				{
					$consulta .= " and ";
				}			
			}
			if ($sw1==1)
			{
				$consulta .= "freq >= CONVERT(varchar(12),'$fi', 103) and freq <=CONVERT(varchar(12),'$ff', 103) ";
			}
			
			if ($sw1==1 && $sw2==1)
			{
				$consulta .= " and ";
			}
			
			if ($sw2==1)
			{
				$consulta .= "forden >= CONVERT(varchar(12),'$fi2', 103) and freq <=CONVERT(varchar(12),'$ff2', 103) ";
			}
			
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos2[$i]['requisi']= trim($row['requisi']);
				$datos2[$i]['freq']= trim($row['freq']);
				$datos2[$i]['orden']= trim($row['orden']);
				$datos2[$i]['forden']= trim($row['forden']);
				$datos2[$i]['prov']= trim($row['prov']);
				$datos2[$i]['nomprov']= trim($row['nomprov']);
				$datos2[$i]['tot']= number_format(trim($row['tot']),2);
				if ($i==0)
				{
					//$excel.="No. Requisicion"."\t"."Fecha de Requisicion"."\t"."Orden de Compra"."\t"."Fecha de Orden de C"."\t"."  Proveedor  "."\t"."  Importe  "."\n";				
				}
				$excel.=$datos2[$i]['requisi']."\t".$datos2[$i]['freq']."\t".$datos2[$i]['orden']."\t".$datos2[$i]['forden']."\t".$datos2[$i]['nomprov']."\t".$datos2[$i]['tot']."\n";
				$i++;
			}
		}
	}
//echo json_encode($datos);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd"><head>
<title>Consulta de Requisiciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/busquedaincprov.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
</head>




<?PHP
  $EXCEL.='
<table name="enc" id="enc" width="89%" height="10%" align="center" border="0">
  <tr>
    <th width="70" align="center" class="subtituloverde">No. Requisici&oacute;n </th>
    <th width="111" align="center" class="subtituloverde">Fecha de Requisici&oacute;n </th>
    <th width="91" align="center" class="subtituloverde">No. de Orden de Compra </th>
    <th width="109" align="center" class="subtituloverde">Fecha de Orden de Compra </th>
    <th width="331" align="center" class="subtituloverde">Proveedor</th>
    <th width="100" align="center" class="subtituloverde">Total</th>
  </tr>
	
	';
	
	<?php
		for($i=0;$i<count($datos);$i++)
		{
	?> 
	
	<?PHP
	$EXCEL.=' 
	<body>
	<tr>
		<td align="center">'<?php echo $datos[$i]['requisi'];?>
		<?PHP $EXCEL.='</td>
		<td align="center">'<?php echo $datos[$i]['freq'];?>
		<?PHP $EXCEL.='</td>
		<td align="center">'<?php echo $datos[$i]['orden'];?>
		<?PHP $EXCEL.='</td>
		<td align="center">'<?php echo $datos[$i]['forden'];?>
		<?PHP $EXCEL.='</td>
		<td align="left">'<?php echo $datos[$i]['nomprov'];?>
		<?PHP $EXCEL.='</td>
		<td align="right">'<?php echo $datos[$i]['tot'];?>
		<?PHP $EXCEL.='</td>
	</tr>
	</body>
	</table>';
$file="ReporteRequisiciones.xls";

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
echo $EXCEL;
?>	   
