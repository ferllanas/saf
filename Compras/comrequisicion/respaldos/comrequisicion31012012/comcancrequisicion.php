<?php
require_once("../../Administracion/globalfuncions.php");
require_once("../../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
	
	$usuario = $_COOKIE['ID_my_site'];
	
	$idarea="";
	if(isset($_COOKIE['depto']))
		$idarea=$_COOKIE['depto'];
	//echo $idarea;
	$requisi="";
	if(isset($_REQUEST['requisi']))
			$requisi=$_REQUEST['requisi'];

	$fecini="";
	if(isset($_REQUEST['fecini']))
			$fecini=$_REQUEST['fecini'];
	$fecinicpy=$fecini;
	$fecfin="";
	if(isset($_REQUEST['fecfin']))
		$fecfin=$_REQUEST['fecfin'];		
	$fecfincpy=$fecfin;
	
	$observaciones="";
	if(isset($_REQUEST['observaciones']))
		$observaciones=$_REQUEST['observaciones'];	

	$descripcion="";
	if(isset($_REQUEST['descripcion']))
		$descripcion=$_REQUEST['descripcion'];	
	
	$opcionbuscar="";
	if(isset($_REQUEST['opcionbuscar']))
		$opcionbuscar=$_REQUEST['opcionbuscar'];	
	
	$reqConsulta="";
	$datos=array();
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$comando="";
	if($conexion  && isset($_REQUEST['opcionbuscar']))
	{///&& ( isset($_REQUEST['fecini']) || isset($_REQUEST['fecfin']) )
		$commando="SELECT req FROM menumusuarios WHERE usuario='$usuario'";
		//echo $commando."<br>";
		$stmt = sqlsrv_query( $conexion, $commando);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			if(sqlsrv_has_rows($stmt))
			{
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
				{
					$reqConsulta = $row['req'];
				}
			}
		}
		

		$comando.="SELECT DISTINCT a.requisi , a.entregaen,a.observa,a.depto,b.nomdepto,a.coord,b.nomcoord,a.dir,b.nomdir, 
							CONVERT(varchar(10),a.falta,103)as faltaf,a.estatus,a.path,
							stuff((SELECT ',' + cast(x1.cuadroc as varchar(50))   FROM compramcuadroc x1 
									LEFT JOIN compradcuadroc b ON x1.cuadroc=b.cuadroc 
									LEFT JOIN compradrequisi c ON b.iddrequisi=c.id WHERE c.requisi = a.requisi AND x1.estatus<9000  GROUP BY x1.cuadroc
									for xml path('') ),1,1,'')as cuadroc,
							stuff((SELECT ',' + cast(  xa.orden as varchar(50))   FROM compramordenes xa LEFT JOIN compramcuadroc x1 ON x1.cuadroc=xa.cuadroc
									LEFT JOIN compradcuadroc b ON x1.cuadroc=b.cuadroc 
									LEFT JOIN compradrequisi c ON b.iddrequisi=c.id WHERE c.requisi = a.requisi AND xa.estatus<9000 GROUP BY xa.orden
									for xml path('') ),1,1,'')as orden
					FROM compramrequisi a 
					LEFT JOIN nominamdepto b ON a.depto=b.depto";
		switch($opcionbuscar)
		{									
			case 'optrequisi':
					$comando.="	WHERE a.requisi =".$requisi;
				break;
			case 'optObserva':
					$comando.="	WHERE a.observa LIKE '%".$observaciones."%'";
				break;
			case 'optdescrip':
					$comando.=" INNER JOIN compradrequisi c ON c.requisi=a.requisi WHERE c.descrip LIKE '%$descripcion%'";
				break;
			case 'optfecha':
					$comando.="";
					if(strlen($fecini)>0 )
					{
						$fecini = convertirFechaEuropeoAAmericano($fecini);
						$comando.=" WHERE a.falta>='".$fecini."'";			
						$entro=true;
					}
					
					if(strlen($fecini)>0)
					{
						$fecfin = convertirFechaEuropeoAAmericano($fecfin);
						if($entro)
							$comando.=" AND a.falta>='".$fecini."'";		
						else
							$comando.=" WHERE a.falta>='".$fecini."'";				
					}

				break;
			default:
				break; 
		}
		
		//echo "!".$reqConsulta."!";
		switch($reqConsulta)
		{
			case '0':
					$comando.=" AND a.usuario='$usuario'";
			break;
			case '20':
					$comando.=" AND a.depto='$idarea'";
			break;
			case '80':
					$comando.=" ";
			break;
		}
		
		$entro=false;
		//Busqueda de Productos
		
		$comando .=" ORDER BY a.requisi";
		//echo $comando;
		$stmt = sqlsrv_query( $conexion, $comando);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n".$comando;
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['requisi'] = $row['requisi'];
				$datos[$i]['entregaen'] = utf8_decode(trim($row['entregaen']));
				$datos[$i]['observa'] = utf8_decode(trim($row['observa']));
				$datos[$i]['depto'] = $row['depto'];
				$datos[$i]['nomdepto'] = utf8_decode(trim($row['nomdepto']));
				$datos[$i]['coord'] =$row['coord'];
				$datos[$i]['nomcoord'] = utf8_decode(trim($row['nomcoord']));
				$datos[$i]['dir'] = $row['dir'];
				$datos[$i]['nomdir'] = utf8_decode(trim($row['nomdir']));
				$datos[$i]['falta'] = $row['faltaf'];
				$datos[$i]['estatus'] = $row['estatus'];
				$datos[$i]['path'] =$row['path'];
				$datos[$i]['cuadroc'] = $row['cuadroc'];
				$datos[$i]['orden'] =$row['orden'];
				//$datos[$i]['ccestatus'] =$row['ccestatus'];
				//$datos[$i]['oestatus'] =$row['oestatus'];
				$i++;
			}
		}
	}
	else
	{
		$fecinicpy= date("01/m/Y");
		$fecfincpy= date("d/m/Y");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Requisiciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="body.css">-->
<link href="../../include/estilos.css" rel="stylesheet" type="text/css">		
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/funciones.js"></script>
<script language="javascript" src="javascript/compcanreq.js"></script>
</head>

<body >
<form id="form_prensa"  method='POST' style="width:100%" action="comcancrequisicion.php" >
<table width="100%" border="0">
  	<tr >
  		<td colspan="2" align="center" class="TituloDForma">Requisiciones<hr class="hrTitForma">
	</tr>
	<tr>
		<td width="71%">
			<table width="98%">
				<tr>
					<td width="3%"><input type="radio" name="opcionbuscar" id="optrequisi" value="optrequisi" <?php if($opcionbuscar=="optrequisi") echo "checked";?>></td>
					<td colspan="2">
					<table width="100%">
						<tr>
							<td width="150" class="texto8">N&uacute;mero de Requisición:</td>
							<td class="texto8"><input type="text" name="requisi" id="requisi" value="<?php echo $requisi;?>" style="width:99%;" class="text" onFocus="javascript:$('optrequisi').checked=true">
							</td>
						</tr>
					</table>
				</tr>
				<tr>
					<td width="3%"><input type="radio" name="opcionbuscar" id="optObserva" value="optObserva" <?php if($opcionbuscar=="optObserva") echo "checked";?>></td>
					<td colspan="2">
					<table width="100%">
						<tr>
							<td width="150" class="texto8">Observaci&oacute;n de Requisición:</td>
							<td class="texto8"><input type="text" name="observaciones" id="observaciones" value="<?php echo $observaciones;?>" style="width:99%;" class="text" onFocus="javascript:$('optObserva').checked=true">
							</td>
						</tr>
					</table>
				</tr>
				<tr>
					<td><input type="radio" name="opcionbuscar" id="optdescrip" value="optdescrip" <?php if($opcionbuscar=="optdescrip") echo "checked";?>></td>
					<td colspan="2">
						<table width="100%">
							<tr>
								<td width="150" class="texto8">Descripción de Producto:</td>
								<td class="texto8"><input type="text" name="descripcion" id="descripcion" value="<?php echo $descripcion;?>" style="width:99%;" class="text"  onFocus="javascript:$('optdescrip').checked=true">
								</td>
							</tr>
						</table>
					</td>	
				</tr>
				<tr>
					<td><input type="radio" name="opcionbuscar"  id="optfecha" value="optfecha" <?php if($opcionbuscar=="optfecha") echo "checked";?>></td>
				  	<td width="21%" class="texto8" scope="row" >Fecha Inicial:
                    <input name="fecini" type="text" size="7" id="fecini" value="<?php echo $fecinicpy;?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:$('optfecha').checked=true">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"><!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td>
				  <td width="76%" class="texto8" scope="row" >Fecha Final:
                    <input name="fecfin" type="text" size="7" id="fecfin" value="<?php echo $fecfincpy;?>" class="texto8" maxlength="10"  style="width:70"  onFocus="javascript:$('optfecha').checked=true">
					<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Calendario" align="absmiddle">
                    <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td>
				
				</tr>
		  </table>
		</td>
	  <td width="29%" valign="bottom">
        <input name="button" type="submit" value="Buscar"><!-- onClick="buscar_requisicion();"-->
	   </td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<thead >
		<th width="5%" class="subtituloverde8" >Requisición</th>
		<th width="5%" class="subtituloverde8">Fecha</th>
		<th width="35%" class="subtituloverde8">Entregar</th>
		<th width="35%" class="subtituloverde8">Observaciones</th>
		<th width="10%" class="subtituloverde8">&nbsp;</th>
	</thead>
	<tbody class="resultadobusqueda">
	<?php
		for($i=0;$i<count($datos);$i++)
		{
	?>
		<tr class="d<?php echo ($i % 2);?>">
			<td class="texto8" align="center"><?php echo $datos[$i]['requisi'];?></td>
			<td class="texto8" align="center"><?php echo $datos[$i]['falta'];?></td>
			<td class="texto8"><?php echo $datos[$i]['entregaen'];?></td>
			<td class="texto8"><?php echo $datos[$i]['observa'];?></td>
			<td class="texto8" align="center">
					 <img src="../../imagenes/consultar.jpg" onClick="javascript:window.open('comrequisicion/<?php echo $datos[$i]['path'];?>')" title="Click aqui para ver la requisición impresa."> 
								<?php 
									if( (int)$datos[$i]['estatus'] < 90 )
									{ 
											//echo  strlen( $datos[$i]['orden']);
										if( strlen( $datos[$i]['orden'] )>0 && $datos[$i]['orden']!="null")
										{
											echo "<a title='La Requisición se encuentra actualmente sobre la Orden de Compra ".$datos[$i]['orden']."' class='texto8'> OC:".$datos[$i]['orden']."</a>";
										}
										else
										{
											if( strlen( $datos[$i]['cuadroc'])>0 && $datos[$i]['cuadroc']!="null" )
											{
												echo "<a title='La Requisición se encuentra actualmente sobre el Cuadro Comparativo ".$datos[$i]['cuadroc']."' class='texto8'> CC:".$datos[$i]['cuadroc']."</a>";
											}
											else
											{ ?>
											
											<img src="../../imagenes/eliminar.jpg" onClick="cancelarRequi(<?php echo $datos[$i]['requisi'];?>)" title="Click aqui para cancelar la requisición.">
									<?php }
										}
									}
									else {?>
											<img src="../../imagenes/borrar.jpg"  title="Esta requisición se encuentra cancelada.">
									<?php }?>
													
				</td>
		</tr>
	<?php
		}
	?>
	</tbody>
</table>
</form>
</body>
</html>
