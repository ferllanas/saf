//Elimina una menu
function eliminar_ProductoARequisicion( e , s )
{
	//var table=$( 'tmenus' );
	var rowindexA=s.parentNode.parentNode.rowIndex-1;
	//alert(rowindexA+","+ $('menus').rows.length);
	$('menus').deleteRow( rowindexA );
	
	if( $('menus').rows.length<=0)
	{
		//document.getElementById('tiporeq').readOnly=false;
	 	document.getElementById('treqvar').value ="";

	}
}

//Agrega una nueva menu en la lista despencientes
function agrega_ProductoARequisicion()
{	
	var usuario=$('usuario').value;
	var des_ent=$('des_ent').value;
	var can_ent=$('can_ent').value;
	
	//alert("!"+$('busProd').value+"!")
	if($('busProd').value.length<=0)
	{
		alert('Ingresa el nombre del producto que deseas solicitar.!');
		return false;
	}
	
	if(can_ent.length<=0)
	{
		alert('Ingresa la cantidad del producto que desea solicitar.!');
		return false;
	}
	
	var Table = document.getElementById('menus');
	
	//Get reference to table body.
	//var TableBody = Table.firstChild;
	
	//Create the new elements
	var NewRow = document.createElement("tr");
	
	var cellNomProd = document.createElement("td");
	var celluniProd = document.createElement("td");
	var cellDescProd = document.createElement("td");
	var cellCantidadProd = document.createElement("td");
	var cellQuitarCell = document.createElement("td");
	
	
	var inputIdProd = document.createElement("input");
	inputIdProd.type="hidden";
	inputIdProd.id="idprod";
	inputIdProd.name="idprod";
	inputIdProd.value=$('busProd').value;
	
	var inputUniProd = document.createElement("input");
	inputUniProd.type="text";
	inputUniProd.id="uniprod";
	inputUniProd.name="uniprod";
	inputUniProd.readOnly = true;
	inputUniProd.className="caja_toprint";
	inputUniProd.style.width="50px";
	inputUniProd.style.align="center";
	inputUniProd.value=$('unidaddPro').value;
	
	var inputnomProd = document.createElement("input");
	inputnomProd.type="text";
	inputnomProd.id="producto";
	inputnomProd.readOnly = true;
	inputnomProd.name="producto";
	inputnomProd.className="caja_toprint";
	inputnomProd.style.width="100%";
	inputnomProd.value=$('txtSearch').value;
	
	var inputDescProd = document.createElement("input");
	inputDescProd.type="text";
	inputDescProd.id="desc";
	inputDescProd.name="desc";
	inputDescProd.className ="texto8";
	inputDescProd.style.width="98%";
	inputDescProd.style.align="center";
	inputDescProd.value = des_ent;
	
	var inputCantidadProd = document.createElement("input");
	inputCantidadProd.type="text";
	inputCantidadProd.id="cantidad";
	inputCantidadProd.name="cantidad";
	inputCantidadProd.className ="texto8";
	inputCantidadProd.style.width="60px";
	inputCantidadProd.style.align="center";
	inputCantidadProd.value= can_ent;
	inputCantidadProd.setAttribute("onKeypress", "javascript:return aceptarSoloNumeros(this, event);");
	inputCantidadProd.setAttribute("onKeyUp", "javascript:asignaFormatoSiesNumerico(this,event);");

	var inputbotonBorrar = document.createElement("input");
	inputbotonBorrar.type="button";
	inputbotonBorrar.style.width="10px";
	inputbotonBorrar.style.align="center";
	inputbotonBorrar.setAttribute("onclick", "javascript:eliminar_ProductoARequisicion(event,this);");
	inputbotonBorrar.value="-";
	
	//Add textboxes to cells
	cellNomProd.appendChild(inputIdProd);
	cellNomProd.appendChild(inputnomProd);
	celluniProd.appendChild(inputUniProd);
	cellDescProd.appendChild(inputDescProd);
	cellCantidadProd.appendChild(inputCantidadProd);
	cellQuitarCell.appendChild(inputbotonBorrar);
	
	//Add elements to row.
	NewRow.appendChild(cellNomProd);
	NewRow.appendChild(celluniProd);
	NewRow.appendChild(cellDescProd);
	NewRow.appendChild(cellCantidadProd);
	NewRow.appendChild(cellQuitarCell);
	
	//Add row to table
	Table.appendChild(NewRow);

	$('txtSearch').value="";
	$('busProd').value="";
	$('des_ent').value="";
	$('can_ent').value="";
	
	//document.getElementById('tiporeq').readOnly=true;
	var idtipoRequiSelected = document.getElementById('tiporeq').selectedIndex;//document.getElementById('tiporeq').options[].value;
	document.getElementById('treqvar').value = idtipoRequiSelected;
	
}




function cambiar_UnidadesParaProducto(productoSelected,resp)
{
	//alert("ASASASAS ");
	/*if(resp=='null')
			alert("No existen resultados para estos criterios.");
		//got an array of suggestions.
		alert(resp);
		var myArray = eval(resp.responseText);*/
}

function validaDatos()
{
	if(checkForm($('form_requisicion')))//Esta funcion se encuentra en el archivo funciones.js
	{
		return false;
	}
	else
		return true;
}

function GuardarRequisicion()
{
	document.getElementById('btnguardar').disabled=true;
	document.getElementById('btnguardar').setAttribute("disabled", "disabled");
	if(!validaDatos())
	{
		alert("Favor de revisar la informaci�n.");
		document.getElementById('btnguardar').removeAttribute("disabled");
		return false;
	}
	
	var placa="";
	var numeconomico="";
	var idtipoRequiSelected=document.getElementById('tiporeq').options[document.getElementById('tiporeq').selectedIndex].value;
	
	if(idtipoRequiSelected==4)
	{
		placa=document.getElementById('placa').value;
		if(placa.length<=0)
		{
			if(!confirm("No ha ingresado el n�mero placa del vehiculo, �Desea continuar?"))
			{
				document.getElementById('btnguardar').disabled=false;
				document.getElementById('btnguardar').setAttribute("disabled", "");
				return;
			}
		}

		numeconomico=document.getElementById('numeconomico').value;
		if(numeconomico.length<=0)
		{
			if(!confirm("No ha ingresado el n�mero econ�mico del vehiculo, �Desea continuar?")){
				document.getElementById('btnguardar').disabled=false;
				document.getElementById('btnguardar').setAttribute("disabled", "");
				return;
			}
		}
	}	

	var id_req=$('id_req').value;
	
	if(id_req.length>0)
	{
		alert("No puede registrarse de nuevo esta requisici�n, intente creando una nueva requisici�n, y cancele esta si es necesario!.");
		document.getElementById('btnguardar').removeAttribute("disabled");
		return false;
	}
		
	var usuario=$('usuario').value;
	var depto=$('pswd').options[$('pswd').selectedIndex].value;
	var departamento=$('pswd').options[$('pswd').selectedIndex].text;//$('pswd').value;//"";//
	var entreganen=$('entreganen').value;
	var observaciones=$('observaciones').value;
	if(	observaciones.length<=0)
	{	
		alert("Favor de ingresar alguna Observaci�n.");
		document.getElementById('btnguardar').removeAttribute("disabled");
		return;
	}

	var table=$('menus');
	
	var TotaProd= new Array( );
	var countPartidas=0;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="idprod")
				{
					producto=mynode.value;
				}
					
													
				if(mynode.name=="cantidad")
				{
					if(mynode.value.length>0)
					{
						cantidad=mynode.value;
						if(parseFloat(cantidad)<=0)
						{
							alert("Existe un producto sin especificar la cantidad que se requiere. Favor ingresar cantidad.");
							document.getElementById('btnguardar').removeAttribute("disabled");
							return false;
						}
					}
					else
					{
						alert("Existe un producto sin especificar la cantidad que se requiere. Favor ingresar cantidad.");
						document.getElementById('btnguardar').removeAttribute("disabled");
						return false;
					}
				}
				if(mynode.name=="desc"){
					descripcion=addslashes(mynode.value);
				}
				
				if(mynode.name=="uniprod"){
					uniprod=mynode.value;
				}
				
			}
		}
		
		
		TotaProd[countPartidas] = new Object;
		TotaProd[countPartidas]['producto']	= producto;
		TotaProd[countPartidas]['cantidad']			= cantidad;			
		TotaProd[countPartidas]['descripcion']		= descripcion;
		TotaProd[countPartidas]['uniprod']	= uniprod;
		countPartidas++;
		
	}
	

	
	//Verifica que halla agregado productos a la requisicion
	if(countPartidas<=0)
	{
		alert('Favor de agregar almenos un producto.');
		document.getElementById('btnguardar').removeAttribute("disabled");
		return false;
	}
	
	var datas = {
				usuario: usuario,
				depto: depto,
				departamento: departamento,
				observaciones: observaciones,
				entreganen: entreganen,
				id_req: id_req,
				prods: TotaProd,
				placa : placa,
				numeconomico : numeconomico,
				numempfirmaquien: $('firmaquien').options[$('firmaquien').selectedIndex].value
    };

	jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/comrequisicion_CrearRequisicion.php',
				data:          datas,
				dataType: 'json',
				success: function(resp) 
				{
					
						console.log(resp);
						if(resp.length>0)
						{
							if(resp[0].fallo==false)
							{
								$('fecha_req').value= resp[0].fecha;
								$('hora_req').value= resp[0].hora;
								alert("La requisicion "+resp[0].id+" ha sido generada!.");
								$('id_req').value=resp[0].id;
								$('msgnumreq').style.visibility='visible';
								$('id_req').style.visibility='visible';
							
								var datas = {
											id_req : resp[0].id,
											numempfirmaquien: 	$('firmaquien').options[$('firmaquien').selectedIndex].value,
											firmaquien: 		$('firmaquien').options[$('firmaquien').selectedIndex].text,
											observaciones:		observaciones,
											entreganen: 		entreganen,
											hora_req:			resp[0].hora,
											fecha_req:			resp[0].fecha
								};
								
								document.getElementById('btnguardar').removeAttribute("disabled");
								window.open('php_ajax/open_requisicion_dompdf.php?id_req='+resp[0].id);
								location.reload(true);
								/*jQuery.ajax({
											type:           'post',
											cache:          false,
											url:            'php_ajax/crear_reqwdompdf.php',
											data:          datas,
											dataType: 'json',
											success: function(respuesta) 
											{
												if( respuesta.length ) 
												{
													if(respuesta.length>0)
													{
														if(respuesta[0].fallo==false)
														{
															window.open('php_ajax/ver_reqpdf.php?path='+respuesta[0].path+'&requi='+respuesta[0].id);
														}
													}
												}
											}
										});*/
								
							}
							else
							{
								alert("Error en proceso de actualizacion!.");
								document.getElementById('btnguardar').removeAttribute("disabled");
							}
						}
					}
			});
}
function GuardarRequisicionesp()
{
	
	if(!validaDatos())
	{
		alert("Favor de revisar la informaci�n.");
		return false;
	}
	
	var placa="";
	var numeconomico="";
	var idtipoRequiSelected=document.getElementById('tiporeq').options[document.getElementById('tiporeq').selectedIndex].value;
	
	if(idtipoRequiSelected==4)
	{
		placa=document.getElementById('placa').value;
		if(placa.length<=0)
		{
			if(!confirm("No ha ingresado el n�mero placa del vehiculo, �Desea continuar?"))
				return;
		}

		numeconomico=document.getElementById('numeconomico').value;
		if(numeconomico.length<=0)
		{
			if(!confirm("No ha ingresado el n�mero econ�mico del vehiculo, �Desea continuar?"))
				return;
		}
	}	

	var id_req=$('id_req').value;
	
	if(id_req.length>0)
	{
		alert("No puede registrarse de nuevo esta requisici�n, intente creando una nueva requisici�n, y cancele esta si es necesario!.");
		return false;
	}
		
	var usuario=$('usuario').value;
	var depto=$('pswd').options[$('pswd').selectedIndex].value;
	var departamento=$('pswd').options[$('pswd').selectedIndex].text;//$('pswd').value;//"";//
	var entreganen=$('entreganen').value;
	var observaciones=$('observaciones').value;
	if(	observaciones.length<=0)
	{	
		alert("Favor de ingresar alguna Observaci�n.");
		return;
	}

	var table=$('menus');
	
	var TotaProd= new Array( );
	var countPartidas=0;
	//Ciclo para obtener los productos de la requisicion
	for (var i = 0; i < table.rows.length; i++) 
	{  //Iterate through all but the first row
		
		for (var j = 0; j <table.rows[i].cells.length;j++)
		{
			var cell = table.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="idprod")
				{
					producto=mynode.value;
				}
					
													
				if(mynode.name=="cantidad")
				{
					if(mynode.value.length>0)
					{
						cantidad=mynode.value;
						if(parseFloat(cantidad)<=0)
						{
							alert("Existe un producto sin especificar la cantidad que se requiere. Favor ingresar cantidad.");
							return false;
						}
					}
					else
					{
						alert("Existe un producto sin especificar la cantidad que se requiere. Favor ingresar cantidad.");
						return false;
					}
				}
				if(mynode.name=="desc"){
					descripcion=addslashes(mynode.value);
				}
				
				if(mynode.name=="uniprod"){
					uniprod=mynode.value;
				}
				
			}
		}
		
		
		TotaProd[countPartidas] = new Object;
		TotaProd[countPartidas]['producto']	= producto;
		TotaProd[countPartidas]['cantidad']			= cantidad;			
		TotaProd[countPartidas]['descripcion']		= descripcion;
		TotaProd[countPartidas]['uniprod']	= uniprod;
		countPartidas++;
		
	}
	

	
	//Verifica que halla agregado productos a la requisicion
	if(countPartidas<=0)
	{
		alert('Favor de agregar almenos un producto.');
		return false;
	}
	
	var datas = {
				usuario: usuario,
				depto: depto,
				departamento: departamento,
				observaciones: observaciones,
				entreganen: entreganen,
				id_req: id_req,
				prods: TotaProd,
				placa : placa,
				numeconomico : numeconomico
    };

	//alert(datas);
	jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/comrequisicion_CrearRequisicion.php',
				data:          datas,
				success: function(resp) 
				{
					if( resp.length ) 
					{
						//alert(resp);
						var myArray = eval(resp);
						if(myArray.length>0)
						{
							//alert(myArray[0].fallo);
							if(myArray[0].fallo==false)
							{
								$('fecha_req').value= myArray[0].fecha;
								$('hora_req').value= myArray[0].hora;
								alert("La requisicion "+myArray[0].id+" ha sido generada!.");
								$('id_req').value=myArray[0].id;
								$('msgnumreq').style.visibility='visible';
								$('id_req').style.visibility='visible';
								/*alert("php_ajax/crear_reqpdf.php?idreq="+myArray[0].id+
								"&firmaquien="+$('firmaquien').options[$('firmaquien').selectedIndex].value+
								"&observaciones="+observaciones+
								"&entreganen="+entreganen+
								"&fecha_req="+myArray[0].fecha+
								"&hora_req="+myArray[0].hora);*/
								//javascript:window.open('php_ajax/crear_reqpdf.php'?id_req='+myArray[0].id+'&firmaquien='+$('firmaquien').options[$('firmaquien').selectedIndex].value+'&observaciones='+observaciones+'&entreganen='+entreganen+'&fecha_req='+myArray[0].fecha+'&hora_req='+myArray[0].hora)
								var datas = {
											id_req : myArray[0].id,
											numempfirmaquien: 2460,
											firmaquien: 'LIC. EDUARDO MEDINA CARDENAS',
											observaciones:observaciones,
											entreganen: entreganen,
											hora_req:myArray[0].hora,
											fecha_req:myArray[0].fecha
								};
							
								//alert(datas);
								jQuery.ajax({
											type:           'post',
											cache:          false,
											url:            'php_ajax/crear_reqwdompdf2.php',
											data:          datas,
											success: function(respuesta) 
											{
												if( respuesta.length ) 
												{
													//alert(respuesta);
													var pdfreqs = eval(respuesta);
													//var pdfreqs = eval(respuesta);
													if(pdfreqs.length>0)
													{
														//alert(myArray[0].fallo);
														if(pdfreqs[0].fallo==false)
														{
															//alert(pdfreqs[0].path+", "+pdfreqs[0].id);
															window.open('php_ajax/ver_reqpdf.php?path='+pdfreqs[0].path+'&requi='+pdfreqs[0].id);
														}
													}
												}
											}
										});
								//$('btnguardar').disabled = true;
								//window.open('php_ajax/crear_reqpdf.php?id_req='+myArray[0].id+'&firmaquien='+$('firmaquien').options[$('firmaquien').selectedIndex].value+'&observaciones='+observaciones+'&entreganen='+entreganen+'&fecha_req='+myArray[0].fecha+'&hora_req='+myArray[0].hora);
								//$('btnguardar').disabled = true;

								//alert("php_ajax/crear_reqpdf.php?idreq="+myArray[0].id+"&firmaquien="+$('firmaquien').options[$('firmaquien').selectedIndex].value);
								//document.form_requisicion.submit();//$('form_requisicion').submit;
								//location.reload(true);
								//window.open('php_ajax/crear_reqpdf.php?id_req='+myArray[0].id+'&firmaquien='+$('firmaquien').options[$('firmaquien').selectedIndex].value+'&observaciones='+observaciones+'&entreganen='+entreganen+'&fecha_req='+myArray[0].fecha+'&hora_req='+myArray[0].hora);
								//$('btnguardar').disabled = true;

								//alert("php_ajax/crear_reqpdf.php?idreq="+myArray[0].id+"&firmaquien="+$('firmaquien').options[$('firmaquien').selectedIndex].value);
								//document.form_requisicion.submit();//$('form_requisicion').submit;
								//location.reload(true);
							}
							else
							{
								alert("Error en proceso de actualizacion!.");
							}
						}
					}
				}
			});
}

function busquedaIncrementalProductos(inputTermino)
{
	//new Ajax.Updater('coches', 'php_ajax/query.php?q='+this.value, {method: 'get' })
	var termino=inputTermino.value;
	var productoDom=$('producto');
	productoDom.options.length = 0;
	
	//alert('php_ajax/query.php?q='+termino);
	new Ajax.Request('php_ajax/query.php?q='+termino,
					{onSuccess : function(resp) 
						{
							if( resp.responseText ) 
							{
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									for( var ii = 0; ii < myArray.length; ii++ ) 
									{
										var popular1 = new Option( myArray[ii].valor,myArray[ii].id,"","");
										popular1.title=myArray[ii].valor;
										productoDom[ii] =popular1;
									}
									
									productoDom.size =10;
								}
							}
						}
					});
}


function tomarValorDComboyCambiarInput(combo,input)
{
	if(combo.selectedIndex >= 0)
		input.value = combo.options[combo.selectedIndex].text;
		
	combo.size=0;
}

//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('txtSearch').value);
		var tipo = escape(document.getElementById('tiporeq').value);
        searchReq.open("GET",'php_ajax/query.php?q=' + str +'&tiporeq='+tipo,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
        ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[4]+'; '+tok[3]+'; '+tok[0]+'; ' +tok[2]+';'+tok[1]+' ">' + tok[3] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    document.getElementById('txtSearch').value = value;
	var tok =idprod.split(";");
	$('busProd').value = tok[4];
	$('unidaddPro').value = tok[3];
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
}

function addslashes (str) {
    // Escapes single quote, double quotes and backslash characters in a string with backslashes  
    // 
    // version: 1102.614
    // discuss at: http://phpjs.org/functions/addslashes    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +   improved by: marrtins
    // +   improved by: Nate
    // +   improved by: Onno Marsman    // +   input by: Denny Wardhana
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   improved by: Oskar Larsson H�gfeldt (http://oskar-lh.name/)
    // *     example 1: addslashes("kevin's birthday");
    // *     returns 1: 'kevin\'s birthday'    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");  

}


function muestra_placayNumEco()
{

	if( document.getElementById('treqvar').value.length > 0 )
	{
		document.getElementById('tiporeq').selectedIndex=document.getElementById('treqvar').value;
		document.getElementById('tiporeq').options[document.getElementById('treqvar').value].selected = true; 


	}

	var idtipoRequiSelected=document.getElementById('tiporeq').options[document.getElementById('tiporeq').selectedIndex].value;
	if(idtipoRequiSelected==4)
	{
		document.getElementById('placa').readOnly = false;
		document.getElementById('numeconomico').readOnly=false;
	}
	else
	{
		document.getElementById('placa').readOnly=true;
		document.getElementById('placa').value="";
		document.getElementById('numeconomico').readOnly=true;
		document.getElementById('numeconomico').value="";
	}
}

