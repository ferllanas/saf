//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv1() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv1 = getXmlHttpRequestObjectProv1(); 

function searchProveedor1() {
	$('numprov').value = 0;
	//limpia_tabla();
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqProv1.readyState == 4 || searchReqProv1.readyState == 0)
	{
        var str = escape(document.getElementById('provname1').value);
		//alert('php_ajax/queryproveedorincrem.php?q=' + str);
        searchReqProv1.open("GET",'php_ajax/queryproveedorincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv1.onreadystatechange = handleSearchSuggestProv1;
        searchReqProv1.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv1() {
	
	//alert("aja 1");
    if (searchReqProv1.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv')
        ss.innerHTML = '';
		//alert(searchReqProv1.responseText);
        var str = searchReqProv1.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOverProv1(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProv1(this);" ';
				suggest += "onclick='javascript:setSearchProv1(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverProv1(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv1(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv1(value,idprod) 
{
	//alert("aja");
    document.getElementById('provname1').value = value;
	var tok =idprod.split(";");
	$('numprov').value = tok[0];
	document.getElementById('search_suggestProv').innerHTML = '';
//	valida_factura();
}

function trae_datos()
{
	
	var sw1=0;
	var sw2=0;
	var numprov = document.getElementById('numprov').value;
	var fi = document.getElementById('fecini').value;
	var ff = document.getElementById('fecfin').value;
	var fi2 = document.getElementById('fecini2').value;
	var ff2 = document.getElementById('fecfin2').value;
	if (fi.length <1 && ff.length <1 && fi2.length <1 && ff2.length <1)
	{
		alert("Debe Selecionar algun Rango de Fechas");
		return false;
	}
	if (fi.length>1 && ff.length<1)
	{
		document.getElementById('fecfin').value=document.getElementById('fecini').value;
	}

	if (ff.length>1 && fi.length<1)
	{
		document.getElementById('fecini').value=document.getElementById('fecfin').value;
	}

	if (fi2.length>1 && ff2.length<1)
	{
		document.getElementById('fecfin2').value=document.getElementById('fecini2').value;
	}

	if (ff2.length>1 && fi2.length<1)
	{
		document.getElementById('fecini2').value=document.getElementById('fecfin2').value;
	}

	if (fi.length<1)
	{
		sw1=0;
	}
	else
	{
		sw1=1;
	}
	
	if (fi2.length<1)
	{
		sw2=0;
	}
	else
	{
		sw2=1;
	}
//alert('php_ajax/busca_datos_req.php?numprov='+numprov+"&fi="+fi+"&ff="+ff+"&fi2="+fi2+"&ff2="+ff2+"&sw1="+sw1+"&sw2="+sw2);
	new Ajax.Request('php_ajax/busca_datos_req.php?numprov='+numprov+"&fi="+fi+"&ff="+ff+"&fi2="+fi2+"&ff2="+ff2+"&sw1="+sw1+"&sw2="+sw2,
					 {onSuccess : function(resp) 
					 {
						//alert(resp);
						 if( resp.responseText ) 
							{
								var tabla= $('datos');//Asigna a la variable tabla el objeto provsT(cuerpo de la tabla) que se encuentra declarado en buscaprov.php

								var numren=tabla.rows.length;
								for(var i = numren-1; i >=0; i--)
								{
									tabla.deleteRow(i);
								}
								
								var myArray = eval(resp.responseText);
								if(myArray.length>0)
								{
									 document.getElementById("btn_excel").style.visibility = "visible";

									for(var i = 0; i <myArray.length; i++)
									{
										var newrow = tabla.insertRow(i);
										newrow.className ="fila";
										newrow.scope="row";
										
										var newcell = newrow.insertCell(0); 
										newcell.width="70";
										newcell.className ="texto8";
										newcell.innerHTML =  myArray[i].requisi;
										newcell.align ="center";

										var newcell1 = newrow.insertCell(1); 
										newcell1.width="111";
										newcell1.className ="texto8";
										newcell1.innerHTML =  myArray[i].freq;
										newcell1.align ="center";

										var newcell2 = newrow.insertCell(2); 
										newcell2.width="89";
										newcell2.className ="texto8";
										newcell2.innerHTML =  myArray[i].orden;
										newcell2.align ="center";
										
										var newcell3 = newrow.insertCell(3); 
										newcell3.width="105";
										newcell3.className ="texto8";
										newcell3.innerHTML =  myArray[i].forden;
										newcell3.align ="center";

										var newcell4 = newrow.insertCell(4); 
										newcell4.width="322";
										newcell4.className ="texto8";
										newcell4.innerHTML =  myArray[i].nomprov;
										newcell4.align ="center";

										var newcell5 = newrow.insertCell(5); 
										newcell5.width="91";
										newcell5.className ="texto8";
										newcell5.innerHTML =  myArray[i].tot;
										newcell5.align ="right";
										
										
									}
								}
								else
								{
									alert("No existen resultados para estos criterios.");
								}
								
						}
					  }
				});	
			
	
}


function enviar_excel()
{

	var numprov = document.getElementById('numprov').value;
	var fi = document.getElementById('fecini').value;
	var ff = document.getElementById('fecfin').value;
	var fi2 = document.getElementById('fecini2').value;
	var ff2 = document.getElementById('fecfin2').value;
	if (fi.length<1)
	{
		sw1=0;
	}
	else
	{
		sw1=1;
	}
	
	if (fi2.length<1)
	{
		sw2=0;
	}
	else
	{
		sw2=1;
	}	
	//alert("busq_requisicion3.php?numprov="+numprov+"&fi="+fi+"&ff="+ff+"&fi2="+fi2+"&ff2="+ff2+"&sw1="+sw1+"&sw2="+sw2);
	location.href="busq_requisicion3.php?numprov="+numprov+"&fi="+fi+"&ff="+ff+"&fi2="+fi2+"&ff2="+ff2+"&sw1="+sw1+"&sw2="+sw2;
	limpia();

}

function limpia()
{
	var tabla= $('datos');

	var numren=tabla.rows.length;
	for(var i = numren-1; i >=0; i--)
	{
		tabla.deleteRow(i);
	}
	var sw1=0;
	var sw2=0;
	document.getElementById('numprov').value=0;
	document.getElementById('fecini').value="";
	document.getElementById('fecfin').value="";
	document.getElementById('fecini2').value="";
	document.getElementById('fecfin2').value="";
	document.getElementById('provname1').value="";
	document.getElementById("btn_excel").style.visibility = "hidden";
}
