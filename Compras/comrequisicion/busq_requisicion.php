<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	require_once("../../connections/dbconexion.php");

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

//	$usuario = $_COOKIE['ID_my_site']; 
	$nombre='';
	//$usuario = '001349';
	//$depto='1232';
	$numprov=0;
	$excel="";
 ?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Consulta de Requisiciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/busquedaincprov.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>


</head>

<body>
<p class="seccionNota">Reporte de Requisiciones </p>

<hr class="hrTitForma">
<table width="90%" border="1" align="center">
  <tr class="subtituloverde">
    <td width="249" height="22"><div align="center"><strong>Proveedor
    </strong></div></td>
    <td width="218"><div align="center"><strong>Requisicion</strong></div></td>
    <td width="206"><div align="center"><strong>Orden de Compra</strong></div></td>
    <td width="55">Accion</td>
  </tr>
  <tr>
    <td><span class="texto10">
</span>
      <div align="left" style="z-index:1; position:relative; width:height: 24px;">
        <input class="texto8" type="text" id="provname1" name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="1" value="<?php echo $nombre;?>">
        <div id="search_suggestProv" style="z-index:2; position:absolute;" > </div>
      </div>
    <span class="texto10">    </span></td>
    <td class="texto10">Fec. Ini
      <input type="text"  name="fecini" id="fecini" size="8">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle">
      <!--{literal}-->
      <script type="text/javascript">
			Calendar.setup({
				inputField     :    "fecini",		// id of the input field
				ifFormat       :    "%d/%m/%Y",		// format of the input field
				button         :    "f_trigger1",	// trigger for the calendar (button ID)
				//onClose        :    fecha_cambio,
				singleClick    :    true
			});
		</script>
Fec.Final
<input type="text" name="fecfin" id="fecfin" size="8">
<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle">
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger2",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
<!--{literal}--></td>
    <td class="texto10">Fec. Ini
      <input type="text"  name="fecini2" id="fecini2" size="8">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger3" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle">
      <!--{literal}-->
      <script type="text/javascript">
			Calendar.setup({
				inputField     :    "fecini2",		// id of the input field
				ifFormat       :    "%d/%m/%Y",		// format of the input field
				button         :    "f_trigger3",	// trigger for the calendar (button ID)
				//onClose        :    fecha_cambio,
				singleClick    :    true
			});
		</script>
Fec.Final
<input type="text" name="fecfin2" id="fecfin2" size="8">
<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger4" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"> </td>
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin2",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger4",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
			<td align="center" class="texto10"><input type="button" name="enviar" id="enviar" value="Buscar" onClick="trae_datos()"></td>

  </tr>

</table>

<table align="center" width="90%" border="0">
  <tr>
    <td align="right">
		<form>
		  <input type="hidden" name="numprov" id="numprov" value="<?php echo $numprov;?>">
		<input type = "button" name="btn_excel" id="btn_excel" value =" Exportar a Excell" style="visibility:hidden" onClick="enviar_excel()">
		</form>
	</td>
  </tr>
</table>

<table name="enc" id="enc" width="89%" height="10%" align="center" border="0">
  <tr>
    <th width="70" align="center" class="subtituloverde">No. Requisici&oacute;n </th>
    <th width="111" align="center" class="subtituloverde">Fecha de Requisici&oacute;n </th>
    <th width="91" align="center" class="subtituloverde">No. de Orden de Compra </th>
    <th width="109" align="center" class="subtituloverde">Fecha de Orden de Compra </th>
    <th width="331" align="center" class="subtituloverde">Proveedor</th>
    <th width="100" align="center" class="subtituloverde">Total</th>
  </tr>
</table>
<div style="overflow: scroll; width: 100%; height :400px; align:center;"> 
	<table name="master" id="master" width="90%" height="10%" border=0 align="center" >
	 <thead>
	</thead>
	  <tbody id="datos" name="datos" style="size:auto ">
	<?php
		for($i=0;$i<count($datos);$i++)
		{
	?>
  <tr class="d<?php echo ($i % 2);?>">
    <td width="73" align="center"><?php echo $datos[$i]['requisi'];?></td>
    <td width="110" align="center"><?php echo $datos[$i]['freq'];?></td>
    <td width="91" align="center"><?php echo $datos[$i]['orden'];?></td>
    <td width="112" align="center"><?php echo $datos[$i]['forden'];?></td>
    <td width="330" align="left"><?php echo $datos[$i]['nomprov'];?></td>
    <td width="106" align="right"><?php echo $datos[$i]['tot'];?></td>

  </tr>
<?php
	}
?>	  
	  </tbody>
	</table>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
