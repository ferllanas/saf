<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	require_once("../../connections/dbconexion.php");

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	$EXCEL="";
	/*$fi=" ";
	$ff=" ";
	$fi2=" ";
	$ff2=" ";
	$sw1=0;
	$sw2=0;
	$numprov=4984;
	$fi="01/03/2014";
	$ff="20/03/2014";
	$fi2="";
	$ff2="";*/


	
	if(isset($_REQUEST['numprov']))
		$numprov=$_REQUEST['numprov'];

	if(isset($_REQUEST['fi']))
		$fi = $_REQUEST['fi'];

	if(isset($_REQUEST['ff']))		
		$ff = $_REQUEST['ff'];
	
	if(isset($_REQUEST['fi2']))	
		$fi2 = $_REQUEST['fi2'];
		
	if(isset($_REQUEST['ff2']))		
		$ff2 = $_REQUEST['ff2'];

	if(strlen($fi)<1)
		$sw1 = 0;
		else
		$sw1 = 1;
		
	if(strlen($fi2)<1)		
		$sw2 = 0;
		else
		$sw2 = 1;	

	if(strlen($fi)>1)
	{
		list($dd,$mm,$aa)= explode('/',$fi);
		$fi=$aa.'-'.$mm.'-'.$dd;
					
		list($dd2,$mm2,$aa2)= explode('/',$ff);
		$ff=$aa2.'-'.$mm2.'-'.$dd2;
	}
	
	if(strlen($fi2)>1)
	{
		list($dd,$mm,$aa)= explode('/',$fi2);
		$fi2=$aa.'-'.$mm.'-'.$dd;
					
		list($dd2,$mm2,$aa2)= explode('/',$ff2);
		$ff2=$aa2.'-'.$mm2.'-'.$dd2;
	}	

	$consulta ="";
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$datos=array();
	$datos2=array();
	//if ($numprov>0)
	//{
		
		if ($conexion)
		{		
			$consulta .= "select requisi,CONVERT(varchar(12),freq, 103) as freq,orden,CONVERT(varchar(12),forden, 103) as forden,prov,nomprov,tot from v_compra_req_orden where ";
			if($numprov>0)
			{
				$consulta .="prov=$numprov";
				if ($sw1==1 || $sw2==1)
				{
					$consulta .= " and ";
				}			
			}
			if ($sw1==1)
			{
				$consulta .= "freq >= CONVERT(varchar(12),'$fi', 103) and freq <=CONVERT(varchar(12),'$ff', 103) ";
			}
			
			if ($sw1==1 && $sw2==1)
			{
				$consulta .= " and ";
			}
			
			if ($sw2==1)
			{
				$consulta .= "forden >= CONVERT(varchar(12),'$fi2', 103) and freq <=CONVERT(varchar(12),'$ff2', 103) ";
			}

			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos2[$i]['requisi']= trim($row['requisi']);
				$datos2[$i]['freq']= trim($row['freq']);
				$datos2[$i]['orden']= trim($row['orden']);
				$datos2[$i]['forden']= trim($row['forden']);
				$datos2[$i]['prov']= trim($row['prov']);
				$datos2[$i]['nomprov']= trim($row['nomprov']);
				$datos2[$i]['tot']= number_format(trim($row['tot']),2);
				$i++;
			}			
			
		}
	//}
//echo $consulta;

  $EXCEL.='
<table width="89%">
  <tr>
    <td align="center">No. Requisici&oacute;n </td>
    <td align="center">Fecha de Requisici&oacute;n </td>
    <td align="center">No. de Orden de Compra </th>
    <td align="center">Fecha de Orden de Compra </td>
    <td align="center">Proveedor</td>
    <td align="center">Total</td>
  </tr>';
  
	for($i=0;$i<count($datos2);$i++)
	{
	
		$EXCEL.='		
			<tr>
				<td width="70" align="center">'.$datos2[$i]['requisi'].'</td>
				<td width="111" align="center">'.$datos2[$i]['freq'].'</td>
				<td width="89" align="center">'.$datos2[$i]['orden'].'</td>
				<td width="105" align="center">'.$datos2[$i]['forden'].'</td>
				<td width="322" align="center">'.$datos2[$i]['nomprov'].'</td>
				<td width="91" align="right">'.$datos2[$i]['tot'].'</td>
			</tr>';
	}
		$EXCEL.='
</table>

';
$file="ReporteRequisiciones.xls";

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
echo $EXCEL;
//header('Location: busq_requisicion.php');
?>	   
