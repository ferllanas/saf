<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	require_once("../../connections/dbconexion.php");

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	$excel="";
	$numprov=0;
	/*$fi=" ";
	$ff=" ";
	$fi2=" ";
	$ff2=" ";
	$sw1=0;
	$sw2=0;*/
	
	if(isset($_REQUEST['fi'])<1)
		$sw1=0;
		
	if(isset($_REQUEST['fi2'])<1)
		$sw2=0;
		
	
	if(isset($_REQUEST['numprov']))
		$numprov=$_REQUEST['numprov'];

	if(isset($_REQUEST['fi']))
		$fi = $_REQUEST['fi'];

	if(isset($_REQUEST['ff']))		
		$ff = $_REQUEST['ff'];
	
	if(isset($_REQUEST['fi2']))	
		$fi2 = $_REQUEST['fi2'];
		
	if(isset($_REQUEST['ff2']))		
		$ff2 = $_REQUEST['ff2'];
		
		
		
	if($fi<1)		
		$sw1 = 0;
		
	if($fi2<1)		
		$sw2 = 0;		

	if($fi<1)
	{
		list($dd,$mm,$aa)= explode('/',$fi);
		$fi=$aa.'-'.$mm.'-'.$dd;
					
		list($dd2,$mm2,$aa2)= explode('/',$ff);
		$ff=$aa2.'-'.$mm2.'-'.$dd2;
	}
	
	if($fi2<1)
	{
		list($dd,$mm,$aa)= explode('/',$fi2);
		$fi2=$aa.'-'.$mm.'-'.$dd;
					
		list($dd2,$mm2,$aa2)= explode('/',$ff2);
		$ff2=$aa2.'-'.$mm2.'-'.$dd2;
	}	

	$consulta ="";

	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$datos=array();
	$datos2=array();
	if ($numprov>0)
	{
		
		if ($conexion)
		{		
			$consulta .= "select requisi,CONVERT(varchar(12),freq, 103) as freq,orden,CONVERT(varchar(12),forden, 103) as forden,prov,nomprov,tot from v_compra_req_orden where ";
			if($numprov>0)
			{
				$consulta .="prov=$numprov";
				if ($sw1==1 || $sw2==1)
				{
					$consulta .= " and ";
				}			
			}
			if ($sw1==1)
			{
				$consulta .= "freq >= CONVERT(varchar(12),'$fi', 103) and freq <=CONVERT(varchar(12),'$ff', 103) ";
			}
			
			if ($sw1==1 && $sw2==1)
			{
				$consulta .= " and ";
			}
			
			if ($sw2==1)
			{
				$consulta .= "forden >= CONVERT(varchar(12),'$fi2', 103) and freq <=CONVERT(varchar(12),'$ff2', 103) ";
			}
			
			$R = sqlsrv_query( $conexion,$consulta);
			$i=0;
			while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
			{
				$datos2[$i]['requisi']= trim($row['requisi']);
				$datos2[$i]['freq']= trim($row['freq']);
				$datos2[$i]['orden']= trim($row['orden']);
				$datos2[$i]['forden']= trim($row['forden']);
				$datos2[$i]['prov']= trim($row['prov']);
				$datos2[$i]['nomprov']= trim($row['nomprov']);
				$datos2[$i]['tot']= number_format(trim($row['tot']),2);
				if ($i==0)
				{
					//$excel.="No. Requisicion"."\t"."Fecha de Requisicion"."\t"."Orden de Compra"."\t"."Fecha de Orden de C"."\t"."  Proveedor  "."\t"."  Importe  "."\n";				
				}
				$excel.=$datos2[$i]['requisi']."\t".$datos2[$i]['freq']."\t".$datos2[$i]['orden']."\t".$datos2[$i]['forden']."\t".$datos2[$i]['nomprov']."\t".$datos2[$i]['tot']."\n";
				$i++;
			}
		}
	}
 ?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>


<title>Consulta de Requisiciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/busquedaincprov.js"></script>



<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

</head>



<body>
<p class="seccionNota">Reporte de Requisiciones</p>

<hr class="hrTitForma">
<table width="90%" border="1" align="center">
  <tr class="subtituloverde">
    <td width="249" height="22"><div align="center"><strong>Proveedor
    </strong></div></td>
    <td width="218"><div align="center"><strong>Requisicion</strong></div></td>
    <td width="206"><div align="center"><strong>Orden de Compra</strong></div></td>
    <td width="55">Accion</td>
  </tr>
  <tr>
    <td><span class="texto10">
</span>
      <div align="left" style="z-index:1; position:relative; width:height: 24px;">
        <input class="texto8" type="text" id="provname1" name="provname1" style="width:90%;" onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="1" value="<?php echo $nombre;?>">
        <div id="search_suggestProv" style="z-index:2; position:absolute;" > </div>
      </div>
    <span class="texto10">    </span></td>
    <td class="texto10">Fec. Ini
      <input type="text"  name="fecini" id="fecini" size="8" value="<?php echo $fi; ?>">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle">
      <!--{literal}-->
      <script type="text/javascript">
			Calendar.setup({
				inputField     :    "fecini",		// id of the input field
				ifFormat       :    "%d/%m/%Y",		// format of the input field
				button         :    "f_trigger1",	// trigger for the calendar (button ID)
				//onClose        :    fecha_cambio,
				singleClick    :    true
			});
		</script>
Fec.Final
<input type="text" name="fecfin" id="fecfin" size="8" value="<?php echo $ff; ?>">
<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger2" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle">
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger2",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
<!--{literal}--></td>
    <td class="texto10">Fec. Ini
      <input type="text"  name="fecini2" id="fecini2" size="8" value="<?php echo $fi2; ?>">
      <img src="../../calendario/img.gif" width="16" height="16" id="f_trigger3" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle">
      <!--{literal}-->
      <script type="text/javascript">
			Calendar.setup({
				inputField     :    "fecini2",		// id of the input field
				ifFormat       :    "%d/%m/%Y",		// format of the input field
				button         :    "f_trigger3",	// trigger for the calendar (button ID)
				//onClose        :    fecha_cambio,
				singleClick    :    true
			});
		</script>
Fec.Final
<input type="text" name="fecfin2" id="fecfin2" size="8" value="<?php echo $ff2; ?>">
<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger4" style="cursor: pointer;" title=""%Y/%m/%d"" align="absmiddle"> </td>
			<script type="text/javascript">
				Calendar.setup({
					inputField     :    "fecfin2",		// id of the input field
					ifFormat       :    "%d/%m/%Y",		// format of the input field
					button         :    "f_trigger4",	// trigger for the calendar (button ID)
					//onClose        :    fecha_cambio,
					singleClick    :    true
				});
			</script>  
	<td align="center" class="texto10"><input type="button" name="enviar" id="enviar" value="Enviar" onClick="trae_datos()">
		    <input type="hidden" name="numprov" id="numprov" value="<?php echo $numprov;?>"></td>

  </tr>

</table>

<table width="89%" border="0" align="center">
  <tr>
    <td width="204">
	<form action="ficheroExcel.php" method = "POST">
	<input type="text" name="export" id="export" value="<?php echo $excel;?>"/>
	<input type = "submit" value ="Exportar a Excell">
	</form>
	</td>
  </tr>
</table>

</table>

	<table name="enc" id="enc" width="89%" height="10%" align="center" border="0">
	
	  <tr>
		<th width="70" align="center" class="subtituloverde">No. Requisici&oacute;n </th>
		<th width="111" align="center" class="subtituloverde">Fecha de Requisici&oacute;n </th>
		<th width="91" align="center" class="subtituloverde">No. de Orden de Compra </th>
		<th width="109" align="center" class="subtituloverde">Fecha de Orden de Compra </th>
		<th width="331" align="center" class="subtituloverde">Proveedor</th>
		<th width="100" align="center" class="subtituloverde">Total</th>
	  </tr>
	  <tbody name="datos" id="datos">
		  <tr >
			<td align="center"><?php echo $datos[$i]['requisi'];?></td>
			<td align="center"><?php echo $datos[$i]['freq'];?></td>
			<td align="center"><?php echo $datos[$i]['orden'];?></td>
			<td align="center"><?php echo $datos[$i]['forden'];?></td>
			<td align="left"><?php echo $datos[$i]['nomprov'];?></td>
			<td align="right"><?php echo $datos[$i]['tot'];?></td>
		  </tr>
	  </tbody>
	</table>
	<div style="overflow: scroll; width: 100%; height :400px; align:center;"></div>

</body>
</html>

