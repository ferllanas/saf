//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv2() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv2 = getXmlHttpRequestObjectProv2(); 

function searchProveedor2() {
	document.getElementById('provid2').value = 0;
	document.getElementById('nomproveedor2').value=  "Proveedor 2";
	document.getElementById('nomproveedor2').title=  "Proveedor 2";
    if (searchReqProv2.readyState == 4 || searchReqProv2.readyState == 0) {
        var str = escape(document.getElementById('provname2').value);
        searchReqProv2.open("GET",'php_ajax/queryproveedorincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv2.onreadystatechange = handleSearchSuggestProv2;
        searchReqProv2.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv2() {
	
	//alert("aja 1");
    if (searchReqProv2.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv2')
        ss.innerHTML = '';
		//alert(searchReqProv2.responseText);
        var str = searchReqProv2.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				//alert(tok[0]);
				var suggest = '<div onmouseover="javascript:suggestOverProv2(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProv2(this);" ';
				suggest += "onclick='javascript:setSearchProv2( this.innerHTML , this.title );' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverProv2(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv2(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv2(value,idprod) 
{
	//alert("aja");
    document.getElementById('provname2').value = value;
	var tok =idprod.split(";");
	document.getElementById('provid2').value = tok[0];
	document.getElementById('nomproveedor2').value=  value;
	document.getElementById('nomproveedor2').title=  value;
    document.getElementById('search_suggestProv2').innerHTML = '';
}