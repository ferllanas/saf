//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv3() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv3 = getXmlHttpRequestObjectProv3(); 

function searchProveedor3() {
	document.getElementById('provid3').value = 0;
	document.getElementById('nomproveedor3').value=  "Proveedor 3";
	document.getElementById('nomproveedor3').title=  "Proveedor 3";
    if (searchReqProv3.readyState == 4 || searchReqProv3.readyState == 0) {
        var str = escape(document.getElementById('provname3').value);
        searchReqProv3.open("GET",'php_ajax/queryproveedorincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv3.onreadystatechange = handleSearchSuggestProv3;
        searchReqProv3.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv3() {
	
	//alert("aja 1");
    if (searchReqProv3.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv3')
        ss.innerHTML = '';
		//alert(searchReqProv3.responseText);
        var str = searchReqProv3.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOverProv3(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProv3(this);" ';
				suggest += "onclick='javascript:setSearchProv3(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverProv3(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv3(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv3(value,idprod) 
{
	//alert("aja");
    document.getElementById('provname3').value = value;
	var tok =idprod.split(";");
	document.getElementById('provid3').value = tok[0];
	document.getElementById('nomproveedor3').value=  value;
	document.getElementById('nomproveedor3').title=  value;
    document.getElementById('search_suggestProv3').innerHTML = '';
}