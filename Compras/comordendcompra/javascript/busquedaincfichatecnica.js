//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv1() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv1 = getXmlHttpRequestObjectProv1(); 

function searchCuadroComparativo() {
	//$('provid1').value = 0;
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqProv1.readyState == 4 || searchReqProv1.readyState == 0)
	{
        var str = escape(document.getElementById('id_ccomp').value);
        searchReqProv1.open("GET",'php_ajax/busquedaincfichatecnica.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv1.onreadystatechange = handleSearchSuggestCC;
        searchReqProv1.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestCC() {
	
	//alert("aja 1");
    if (searchReqProv1.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestCC')
        ss.innerHTML = '';
		//alert(searchReqProv1.responseText);
        var str = searchReqProv1.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		var estatusFtecnica=false;
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				
				var tok = str[i].split(";");
				
				if(tok[2]>0)
					estatusFtecnica=true;
					
				var suggest = '<div onmouseover="javascript:suggestOverCC(this);" ';
				suggest += 'onmouseout="javascript:suggestOutCC(this);" ';
				suggest += "onclick='javascript:setSearchCC(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverCC(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutCC(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchCC(value,idprod) 
{
	///alert(idprod);
    document.getElementById('id_ccomp').value = value;
	var tok =idprod.split(";");
	/*if(tok[1]>=0)
	{
		alert("Este Cuadro comparativo ya existe en la Ficha Tecnica.");
	}*/
	
		document.getElementById('search_suggestCC').innerHTML = '';
		document.getElementById('ficha_tecnica').submit();
}



function grabar_ficha_tecnica()
{
	var cuadro_comp = document.getElementById('id_ccomp').value;
	var tabla= document.getElementById('datos');
	console.log(tabla);
	var TotaProd= new Array( );
	var countPartidas=0;
	deshabilitar();
	
	
	
	for (var i = 0; i < tabla.rows.length; i++) 
	{  //Iterate through all but the first row
		
		for (var j = 0; j <tabla.rows[i].cells.length;j++)
		{
			var cell = tabla.rows[i].cells[j];
			for (var k = 0; k < cell.childNodes.length; k++) 
			{
				var mynode = cell.childNodes[k];
												
				if(mynode.name=="id_descri")
				{
					id_descri0=mynode.value;
				}
				if(mynode.name=="descri")
				{
					descri0=mynode.value;
				}
			}
		}
		TotaProd[countPartidas] = new Object;
		TotaProd[countPartidas]['id_descri0']	= id_descri0;	
		TotaProd[countPartidas]['descri0']		= descri0;
		countPartidas++;
	}
	var datas = {
				cuadro_comp: cuadro_comp,
				id_descri0: id_descri0,
				descri0: descri0,
				prods: TotaProd
    };
	
	//console.log(datas);
	//alert(datas);
	jQuery.ajax({
				type:           'post',
				cache:          false,
				url:            'php_ajax/alta_ficha_tecnica.php',
				data:          datas,
				error:			function(xhr,status,error)
				{console.log(xhr);console.log(status);console.log(error)},
				success: function(resp) 
				{
					if( resp.length ) 
					{
						//alert(resp);
						console.log(resp);
						var myArray = eval(resp);
						//alert(myArray);
						if(myArray.length>0)
						{
							window.open('php_ajax/open_FichaTecnicadomPDF.php?id_ccomp='+cuadro_comp);
								//alert(myArray[0].string);
								//alert(resp);
								//resp=resp.replace('"','');
								alert(resp);		
								location.href='ficha_tecnica.php';
						}
					}
				}
			});	
	
	
	
}



function deshabilitar()
{
	document.getElementById('enviar').disabled=true;
}

function recargar()
{
	
	location.href='ficha_tecnica.php';
}