//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv4() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv4 = getXmlHttpRequestObjectProv4(); 

function searchProveedor4() {
	document.getElementById('provid4').value = 0;
	document.getElementById('nomproveedor4').value=  "Proveedor 4";
	document.getElementById('nomproveedor4').title=  "Proveedor 4";
    if (searchReqProv4.readyState == 4 || searchReqProv4.readyState == 0) {
        var str = escape(document.getElementById('provname4').value);
        searchReqProv4.open("GET",'php_ajax/queryproveedorincrem.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv4.onreadystatechange = handleSearchSuggestProv4;
        searchReqProv4.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestProv4() {
	
	//alert("aja 1");
    if (searchReqProv4.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestProv4')
        ss.innerHTML = '';
		//alert(searchReqProv4.responseText);
        var str = searchReqProv4.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				var suggest = '<div onmouseover="javascript:suggestOverProv4(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProv4(this);" ';
				suggest += "onclick='javascript:setSearchProv4(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverProv4(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProv4(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProv4(value,idprod) 
{
	//alert("aja");
    document.getElementById('provname4').value = value;
	var tok =idprod.split(";");
	document.getElementById('provid4').value = tok[0];
	document.getElementById('nomproveedor4').value=  value;
	document.getElementById('nomproveedor4').title=  value;
    document.getElementById('search_suggestProv4').innerHTML = '';
}