function buscar_ccomps()
{
	var fecini = document.getElementById('fecini').value;
	var fecfin = document.getElementById('fecfin').value;

	new Ajax.Request('php_ajax/buscar_ccomps.php?fecini='+fecini+'&fecfin='+fecfin,
	{
		onSuccess : function(resp) 
		{
			if( resp.responseText ) 
			{
				//alert(resp.responseText);
				var myArray = eval(resp.responseText);
				if(myArray.length>0)
				{
					if(myArray[0].fallo==false)
					{
						alert("Error en proceso de actualizacion!.");
					}
					else
					{
						var Table = document.getElementById('cuadrocs');
						var numren=Table.rows.length;
						for(var i = numren-1; i >=0; i--)
						{
									Table.deleteRow(i);
						}
						
						for( var ii = 0; ii < myArray.length; ii++ ) 
						{
							//Agrega producto de requisicion a tabla
							//if(!existeProdEnTabla(myArray[ii].id))
								agregar_cuadroc(myArray[ii].cuadroc,
												myArray[ii].observa,
												myArray[ii].falta,
												myArray[ii].estatus);
						}
					}
				}
			}
		}
	});
}

function agregar_cuadroc(cuadroc,observa,falta, estatus)
{
	var Table = document.getElementById('cuadrocs');
	
	var contRow = Table.rows.length;
	//Create the new elements
	var newrow = Table.insertRow(contRow);

	var newcell3 = newrow.insertCell(0);
	newcell3.innerHTML ="<a href='CuadroComparativo.php?id_cuadroc="+cuadroc+"' class='texto8'>"+cuadroc+"</a>";

	var newcell3 = newrow.insertCell(1);
	newcell3.innerHTML ="<a class='texto8'>"+observa+"</a>";

	var newcell3 = newrow.insertCell(2);
	newcell3.innerHTML ="<a class='texto8'>"+falta+"</a>";

	var newcell3 = newrow.insertCell(3);
	newcell3.innerHTML ="<a href='pdf_files/cuadroc_"+cuadroc+".pdf' class='texto8'>Ver Cuadro Comparativo</a>";

	var newcell3 = newrow.insertCell(4);
	if(estatus=="0")
		newcell3.innerHTML ="<a href='php_ajax/cancela_cuadroc.php?id_cuadroc="+cuadroc+"' class='texto8'>cancelar</a>";
	else
		if(estatus=="10")
			newcell3.innerHTML ="<a class='texto8'>En Orden de compra</a>";
		else
			if(estatus=="9000")
				newcell3.innerHTML ="<a class='texto8'>CANCELADA</a>";
	

}