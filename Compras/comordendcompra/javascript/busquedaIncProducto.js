//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggestProd() {
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('nomprod').value);
        searchReq.open("GET",'php_ajax/busquedaIncProducto.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handlesearchSuggestProd;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handlesearchSuggestProd() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
        ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOverProd(this);" ';
				suggest += 'onmouseout="javascript:suggestOutProd(this);" ';
				suggest += "onclick='javascript:setSearchProd(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[4]+'; '+tok[3]+'; '+tok[0]+'; ' +tok[2]+';'+tok[1]+' ">' + tok[3] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOverProd(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutProd(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchProd(value,idprod) {
    document.getElementById('nomprod').value = value;
	var tok =idprod.split(";");
	$('busProd').value = tok[4];
	//$('unidaddPro').value = tok[3];
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
}