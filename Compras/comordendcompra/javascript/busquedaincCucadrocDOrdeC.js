//Funciones para busqueda avanzada , autocompletar de productos
function getXmlHttpRequestObjectProv1() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReqProv1 = getXmlHttpRequestObjectProv1(); 

function searchCuadroComparativo() {
	//$('provid1').value = 0;
	//$('nomproveedor1').value=  "Proveedor 1";
	//$('nomproveedor1').title=  "Proveedor 1";

    if (searchReqProv1.readyState == 4 || searchReqProv1.readyState == 0)
	{
        var str = escape(document.getElementById('id_ccomp').value);
        searchReqProv1.open("GET",'php_ajax/busquedaincCuadrocPGenOC.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReqProv1.onreadystatechange = handleSearchSuggestCC;
        searchReqProv1.send(null);
    }        
} 

//Called when the AJAX response is returned.
function handleSearchSuggestCC() {
	
	//alert("aja 1");
    if (searchReqProv1.readyState == 4) 
	{
        var ss = document.getElementById('search_suggestCC')
        ss.innerHTML = '';
		//alert(searchReqProv1.responseText);
        var str = searchReqProv1.responseText.split("\n");
		//alert(str.length);
		//if(str.length<50)
		   for(i=0; i < str.length - 1 && i<50; i++) {
				//alert(str);
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split(";");
				var suggest = '<div onmouseover="javascript:suggestOverCC(this);" ';
				suggest += 'onmouseout="javascript:suggestOutCC(this);" ';
				suggest += "onclick='javascript:setSearchCC(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+';'+tok[2]+'">' + tok[0] + '</div>';
				ss.innerHTML += suggest;
			}//
    }
}
//Mouse over function
function suggestOverCC(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOutCC(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearchCC(value,idprod) 
{
    document.getElementById('id_ccomp').value = value;
	var tok =idprod.split(";");
	document.getElementById('search_suggestCC').innerHTML = '';
	document.getElementById('ordencompra').submit();
	//location.href='CuadroComparativo.php?id_cuadroc='+value;
}
