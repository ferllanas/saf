<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ordenes de Cuenta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<?php
include('../../../pdf/class.ezpdf.php');
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
//$usarioCreador = $_COOKIE['ID_my_site'];	

$fname="";
$idcuadroc="000000".$_REQUEST['id_ccomp']."!!!";
if(isset($_REQUEST['id_ccomp']))
	$idcuadroc=$_REQUEST['id_ccomp'];
									
//echo $idcuadroc;
$autoriza="";
$vobo="";
$orden="";
		$forden="";
		$horden="";
		$prov="";
		$ordenprov="";
		$nomprov="";
		$depto="";
		$nomdepto="";
		$entregarEn="";
		$calle="";
		$colonia="";
		$ciudad="";
		$tel_1="";
		$tel_2="";
		$tel_3="";
$seleccion="";
/// conexion a base de datos
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
	
$sqlcommand="select distinct(a.orden) ,a.prov,convert(varchar, a.halta, 108) as halta ,a.ordenprov,convert(varchar, a.falta, 103) as falta, b.nomprov , 
					f.depto,  g.nomdepto, f.entregaen, b.calle, b.colonia, b.ciudad, b.tel_1, b.tel_2, b.tel_3, c.seleccion as selprov, h.descrip as seleccion, i.ppresupuestal, j.descrip
				from  compramordenes a
				INNER JOIN compramprovs b ON b.prov=a.prov
				INNER JOIN compramcuadroc c ON a.cuadroc=c.cuadroc
				INNER JOIN compradcuadroc d ON d.cuadroc = a.cuadroc
				INNER JOIN compradrequisi e ON e.id=d.iddrequisi
				INNER JOIN compramrequisi f ON f.requisi=e.requisi
				INNER JOIN nomemp.dbo.nominamdepto g ON g.depto COLLATE DATABASE_DEFAULT=f.depto COLLATE DATABASE_DEFAULT
				left join compramseleccion h ON h.id=c.seleccion 
				left join compramppresupuestal i ON i.id = a.ppresupuestal
				left join compramtipoprecio j ON j.tipopcio=a.tipopcio
				where a.cuadroc=$idcuadroc and a.estatus=0";
$stmt = sqlsrv_query( $conexion,$sqlcommand);
if ( $stmt === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	 die( $sqlcommand."' ".print_r( sqlsrv_errors(), true));
}
else
{
	//echo "OK ALLRIGHT";
	$docPDF =& new Cezpdf('LATTER','landscape');
	$docPDF->selectFont('../../../fonts/arial.afm');
	$datacreator = array ('Title'=>'Ordenes de compra de Cuador comparativo $idcuadroc',
							'Author'=>'Fomerrey',
							'Subject'=>'Ordenes de compra de Cuador comparativo $idcuadroc',
							'Creator'=>'fernando.llanas@fomerrey.gob.mx',
							'Producer'=>'http://blog.unijimpe.net');
	$docPDF->addInfo($datacreator);
	$docPDF->ezSetCmMargins(1,0.5,0.5,0.5);
	//$docPDF->ezText(" $idcuadroc");
	//Obtiene la cantidad de renglones para evitar colocar una pagina de mas en el documento pdf
	//$numrows= sqlsrv_num_rows($stmt);
	//if( $numrows <=0)
		//echo "no existen resultado. ".$numrows." !!";

	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	{
		//echo "OK ALLRIGHT 2";
		//print_r($row);
		
		$orden= trim($row['orden']);
		$forden= $row['falta'];
		$horden= $row['halta'];
		$prov= trim($row['prov']);
		$ordenprov= trim($row['ordenprov']);
		$nomprov= utf8_decode(trim($row['nomprov']));
		$depto= trim($row['depto']);
		$nomdepto= utf8_decode(trim($row['nomdepto']));
		$entregarEn= utf8_decode(trim($row['entregaen']));
		$calle= utf8_decode(trim($row['calle']));
		$colonia= utf8_decode(trim($row['colonia']));
		$ciudad= utf8_decode(trim($row['ciudad']));
		$tel_1= trim($row['tel_1']);
		$tel_2= trim($row['tel_2']);
		$tel_3= trim($row['tel_3']);
		$seleccion= utf8_decode(trim($row['seleccion']));
		//echo $row['ppresupuestal'];
		$ppresupuestal= trim($row['ppresupuestal']);
		//echo $ppresupuestal;
		$tipoPrecio= utf8_decode(trim($row['descrip']));
		$selprov= trim($row['selprov']);
		
		//echo $orden.",".$forden.",".$horden.",".$prov.",".$ordenprov.",".$nomprov.",".$depto.",". $nomdepto."<br>";
		//echo $horden;
		list($tiempoentr,$condiciones)=getCondicionesYTiempoProv($prov,$idcuadroc);
		creapdfInDividuales($orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc,$seleccion, $ppresupuestal, $tipoPrecio, $selprov);	
		
		creapdfPaImprimir($docPDF,$orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc,$seleccion, $ppresupuestal, $tipoPrecio, $selprov);
		$docPDF->ezNewPage();
	}

	
		//if( $i < ($numrows))
			
	$pdfcode = $docPDF->output();
	$dir = '../pdf_files';
	if(!file_exists($dir)){
		mkdir($dir, 0777);
	}
	
	$fname = $dir.'/OrdenesCompDeCuadroc_'.$idcuadroc.'.pdf';//tempnam(,'PDF_').//
	//echo $dir.'/'.$fml.'.pdf';
	$fp = fopen($fname, 'w');
	fwrite($fp,$pdfcode);
	fclose($fp);
	
	//if(!fun_cambiaPathDeRequisicion($fname,$orden))
	//$docPDF->ezStream();
}


function getCondicionesYTiempoProv($prov,$idcuadroc)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="SELECT * FROM compramcuadroc WHERE cuadroc=$idcuadroc";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$tiempoentr="";
	$condiciones =  "";
	
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			if($prov==$row['prov1'])
			{
				$tiempoentr=$row['tentrega1'];
				$condiciones =  $row['condiciones1'];
			}
			
			if($prov==$row['prov2'])
			{
				$tiempoentr=$row['tentrega2'];
				$condiciones =  $row['condiciones2'];
			}
			
			if($prov==$row['prov3'])
			{
				$tiempoentr=$row['tentrega3'];
				$condiciones =  $row['condiciones3'];
			}
			
			if($prov==$row['prov4'])
			{
				$tiempoentr=$row['tentrega4'];
				$condiciones =  $row['condiciones4'];
			}
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($tiempoentr, $condiciones );
}

function creapdfPaImprimir($docPDF,$orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc,$seleccion, $ppresupuestal, $tipoPrecio, $selprov)
{	
	global $server,$odbc_name,$username_db ,$password_db, $imagenPDFPrincipal;

	//http://localhost/fome/imagenes/encabezadoFnuevo.jpg
	//$docPDF->addJpegFromFile('../../../imagenes/encabezadoFnuevo.jpg',  20, 800, 100,300);
	$docPDF->addJpegFromFile('../../../$imagenPDFPrincipal', 20, 530, 148.5, 57, array('gap'));
	//ob_end_clean(); 
	//$docPDF->addJpegFromFile('../../../'.$imagenPDFSecundaria,460, 940, 100.5, 57,array('gap'));
	//ob_end_clean(); //opcional
	//Arma fecha
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	//$hora_req = "";
	//$fecha_req = "";
	list($month,$days,$years)=split('[/.-]', $forden);
	list($horayseg,$milseg)=split('[:]', $horden);
	//echo $horayseg.",".$milseg;
	$diadesem=date("w");
	
	$docPDF->setColor(0, 0 ,0);
	$docPDF->ezText("Fomento Metropolitano de Monterrey",18,array('justification'=>'center'));
	$docPDF->ezText("Direcci�n de Administraci�n y Finanzas",14,array('justification'=>'center'));
	$docPDF->ezText("Orden de Compra No. ".$orden,16,array('justification'=>'left'));
	//$ffecha = $dias[(int)$diadesem].", $days de ".$meses[((int)$month)-1]." de $years";
	$docPDF->ezText("<b>Fecha:</b> ".$forden .", ". $horden ,10,array('justification'=>'right'));//date("d/m/Y")
	
	
	unset ($opciones_tablaProv);
	//// mostrar las lineas
	$opciones_tablaProv['showLines']=1;
	//// mostrar las cabeceras
	$opciones_tablaProv['showHeadings']=0;
	//// lineas sombreadas
	$opciones_tablaProv['shaded']= 0;
	//// tama�o letra del texto
	$opciones_tablaProv['fontSize']= 10;
	 //// color del texto
	$opciones_tablaProv['textCol'] = array(0,0,0);
	//// tama�o de las cabeceras (texto)
	$opciones_tablaProv['titleFontSize'] = 12;
	//// margen interno de las celdas
	$opciones_tablaProv['rowGap'] = 3;
	$opciones_tablaProv['colGap'] = 3;
	$opciones_tablaProv['width'] = "800";
	$opciones_tablaProv['shadeCol'] = array(0.8,0.9,0.9);
	$opciones_tablaProv['cols'] = array( 'titulo' => array('justification' => 'left', 'width'=>'400' ), "cuestion" => array('justification' => 'left', 'width'=>'400') );
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

		////creamos un nuevo array en el que pondremos un borde=1
	///y las cabeceras de la tabla las pondremos ocultas
	unset ($opciones_tabla);
	//// mostrar las lineas
	$opciones_tabla['showLines']=0;
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=1;
	//// lineas sombreadas
	$opciones_tabla['shaded']= 0;
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 10;
	 //// color del texto
	$opciones_tabla['textCol'] = array(0,0,0);
	//// tama�o de las cabeceras (texto)
	$opciones_tabla['titleFontSize'] = 12;
	//// margen interno de las celdas
	$opciones_tabla['rowGap'] = 3;
	$opciones_tabla['colGap'] = 3;
	$opciones_tabla['width'] = "800";
	//$opciones_tabla['xPos'] = 500;
	$opciones_tabla['shadeCol'] = array(0.8,0.9,0.9);
	$opciones_tabla['cols'] = array( 'id' => array('justification' => 'left', 'width'=>'400' ), "nombre" => array('justification' => 'left', 'width'=>'400') );

	$datosproveedor= array();
	$datosproveedor[0]['id']= $prov."	".$nomprov;
	if(strlen($calle)>0)
		$datosproveedor[0]['id'].="\n".$calle;
	if(strlen($colonia)>0)
		$datosproveedor[0]['id'].="\n".$colonia;
	if(strlen($colonia)>0)
		$datosproveedor[0]['id'].="\n".$ciudad;
	if(strlen($tel_1."".$tel_2."".$tel_3)>0)
		$datosproveedor[0]['id'].="\n".$tel_1.",".$tel_2.",".$tel_3;
							
	$datosproveedor[0]['nombre']="Entregar en:".$entregarEn;
	
	$titles = array('id'=>'Proveedor:<b>'.$prov."	".$nomprov.'</b>', 'nombre'=>'Solicitante:<b>'. $depto."	".$nomdepto.'</b>');
	$docPDF->ezTable($datosproveedor,$titles,"",$opciones_tabla); 	
	$docPDF->ezText("\n" ,10);
	////creamos un nuevo array en el que pondremos un borde=1
	///y las cabeceras de la tabla las pondremos ocultas
	unset ($opciones_tabla);
	//// mostrar las lineas
	$opciones_tabla['showLines']=1;
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=1;
	//// lineas sombreadas
	$opciones_tabla['shaded']= 1;
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 8;

	$opciones_tabla['width'] = "800";
	$titles = array('requisi'=>'<b># Req.</b>','ctapresup'=>'<b>ctapresup</b>','cantidad'=>'<b>Cantidad</b>', 'unidad'=>'<b>Unidad</b>', 'descprod'=>'<b>Descripci�n</b>',
					'precio'=>'<b>P.U.</b>','subtotal'=>'<b>Sub-Total</b>','iva'=>'<b>I.V.A.</b>','total'=>'<b>Total</b>');
	
	  
	
	$opciones_tabla['cols'] = array( 'requisi' => array('justification' => 'center', 'width'=>'60' ), 
									'ctapresup' => array('justification' => 'center', 'width'=>'60' ), 
									"cantidad" => array('justification' => 'center', 'width'=>'60'), 
									"unidad" => array('justification' => 'left', 'width'=>'60'),
									"descprod" => array('justification' => 'left', 'width'=>'300' ), 
									"precio" => array('justification' => 'center', 'width'=>'60'), 
									"subtotal" => array('justification' => 'center', 'width'=>'60'),
									"iva" => array('justification' => 'center', 'width'=>'60'), 
									"total" => array('justification' => 'left', 'width'=>'60'));

	list($datos, $seleccion1)= func_ObtenerProdDorden($orden,$ordenprov);//Obtiene los productos de la requisicion	

	
	$totalImporte=0.0;
	$totalSubtotal=0.0;
	$totalIva=0.0;
	$totalTotal=0.0;
	for($i=0;$i<count($datos);$i++)
	{
		$pprecio=$datos[$i]['precio'];
		$psubtotal=$datos[$i]['subtotal'];
		$piva=$datos[$i]['iva'];
		$ptotal=$datos[$i]['total'];
		$totalImporte += (double)$pprecio;
		$totalSubtotal += (double)$psubtotal;
		$totalIva += (double)$piva;
		$totalTotal += (double)$ptotal;
	}
	//Da formato a los valores para el detalle del articulo
	for($i=0;$i<count($datos);$i++)
	{
		$datos[$i]['precio']=	number_format($datos[$i]['precio'],2);
		$datos[$i]['subtotal']=	number_format($datos[$i]['subtotal'],2);
		$datos[$i]['iva']=		number_format($datos[$i]['iva'] ,2);
		$datos[$i]['total']=	number_format($datos[$i]['total'],2);
	}	

	$docPDF->ezTable($datos,$titles,"",$opciones_tabla);  
	/////////////////////////////
	//Tabla de totales
	/////////////////////////////
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=0;

	//echo $totalImporte;
	$datos_totales=array(  array( "requisi" => '', 
							"ctapresup" => '', 
							"cantidad" => "", 
							"unidad" => "",
							"descprod" => "<b>Total</b>", 
							"precio" => number_format($totalImporte,2), 
							"subtotal" => number_format($totalSubtotal,2),
							"iva" => number_format($totalIva,2), 
							"total" => number_format($totalTotal,2)));
	$docPDF->ezTable($datos_totales,$titles,"",$opciones_tabla);  
	//Agregar una tabla con el neto
	$datos_totales=array(  array( "requisi" => '', 
							"ctapresup" => '', 
							"cantidad" => "", 
							"unidad" => "",
							"descprod" => "", 
							"precio" => "", 
							"subtotal" => "",
							"iva" =>"", 
							"total" => number_format($totalTotal,2)));
	$docPDF->ezTable($datos_totales,$titles,"",$opciones_tabla);  

//echo num2letras($totalTotal);
	/////

	$TotalCadena =num2letras(redondear_dos_decimal($totalTotal));
	$docPDF->ezText("*************(".$TotalCadena.")*************\n\n" ,10);
	
	//Especiales
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=1;
	$opciones_tabla['width'] = "600";
	$opciones_tabla['xPos'] = 300;
	$opciones_tabla['cols'] = array( 'tiempoentrega' => array('justification' => 'center', 'width'=>'100' ), 
									"condiciones" => array('justification' => 'center', 'width'=>'100' ), 
									"cuadroc" => array('justification' => 'center', 'width'=>'100') ,
									"seleccion" => array('justification'=>'center', 'width'=>'300'));
									
	$titlesDEspeciales = array('tiempoentrega'=>'<b>Tiempo de Entrega</b>', 'condiciones'=>'<b>Condiciones</b>', 'cuadroc'=>'<b>Cuadro Comparativo</b>',"seleccion" => 'Selecci�n');
	$datosEspeciales=array( array("tiempoentrega"=>$tiempoentr,"condiciones"=>$condiciones,"cuadroc"=>$idcuadroc,"seleccion"=>$seleccion));
	$docPDF->ezTable($datosEspeciales,$titlesDEspeciales,"",$opciones_tabla);  
	////////FIRMAS///////
	$docPDF->ezText("\n" ,10);
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=0;
	$opciones_tabla['shaded']= 0;
	$opciones_tabla['width'] = "800";
	$opciones_tabla['xPos'] = 415;
	$opciones_tabla['fontSize']= 10;
	$opciones_tabla['cols'] = array( 'solicitante' => array('justification' => 'center', 'width'=>'200' ), 
									'jefecompras' => array('justification' => 'center', 'width'=>'200' ), 
									"Autorizacion" => array('justification' => 'center', 'width'=>'200' ), 
									"VoBo" => array('justification' => 'center', 'width'=>'200') );
	$titlesfirmas = array('solicitante'=>'<b>solicitante</b>','jefecompras'=>"Jefe de Compras", 'Autorizacion'=>'<b>Autorizacion</b>', 'VoBo'=>'<b>VoBo</b>');
	$datosfirmas=array( array("solicitante"=>"______________________________",'jefecompras'=>"___________________________",
								"Autorizacion"=>"___________________________","VoBo"=>"____________________________"),
						array("solicitante"=>"Nombre, Firma y Fecha de Recibido", 'jefecompras'=>"Jefe de Compras", "Autorizacion"=>"Director de Administraci�n",        "VoBo"=>"Aplicacion Presupuestal"),
						);
	$docPDF->ezTable($datosfirmas,$titlesfirmas,"",$opciones_tabla);  
	
	//LEYENDA
	$docPDF->ezText("" ,5,array('justification'=>'right'));
	$opciones_tabla['showLines']=1;
	$opciones_tabla['showHeadings']=0;
	$opciones_tabla['fontSize']= 6;
	$opciones_tabla['xPos'] = 415;
	$opciones_tabla['cols'] = array( 'leyenda' => array('justification' => 'left', 'width'=>'800' ));
	$datosleyenda= array(array('leyenda'=>"ENVIESE LA FACTURA CON ORIGINAL Y COPIA ACOMPA�ADAS DE LA ORDEN CORRESPONDIENTE, HAGASE MENCION DEL NUMERO DE ORDEN DE COMPRA EN REMISIONES, FACTURAS Y PAQUETES AL ENTREGAR LA MERCANCIA RECABESE, FIRMA Y NOMBRE DE QUIEN RECIBE Y SELLO DE ALMACEN."));
	$docPDF->ezTable($datosleyenda,array('leyenda'=>'LEYENDA'),"", $opciones_tabla);  
	
	$docPDF->ezText("" ,12,array('justification'=>'right'));
	//1
		$docPDF->ezText("- La partida presupuestal es $ppresupuestal." ,6,array('justification'=>'left'));
	//2
		$docPDF->ezText("- Los precios son $tipoPrecio." ,6,array('justification'=>'left'));
	//3
		$docPDF->ezText("- En caso de incurrir en los supuestos que marca el articulo 33 Bis del C�digo Fiscal del Estado de Nuevo Le�n acepta y autoriza a 'FOMERREY' para que proceda a efectuar las retenciones que procedan a efecto de garantizar el pago de las contribuciones omitidas." ,6,array('justification'=>'left'));
	//4
	if($selprov=="1")
		$docPDF->ezText("- Se aplicar� una pena, cuyo porcentaje diario ser� del 1% (uno por ciento), que se multiplicar� por el n�mero de d�as de atraso y el resultado se multiplicar� a su vez, por el valor de los bienes entregados con atraso, aceptando EL PROVEEDOR, de dar lugar a este supuesto, se dduzca del importe de la factura que presente para su cobro, el monto de la penalizaci�n. De acuerdo a la siguiente f�rmula: f�rmula: (pd)x(nda)x(vbepa) = pca donde: pd: (1 %) penalizaci�n diaria nda: n�mero de d�as de atraso vbepa: valor del bien entregado con atraso pca: pena convencional aplicable El monto m�ximo a penalizar ser� del 10% Las penas se har�n efectivas descontandose de los pagos que  LA CONVOCANTE tenga pendientes de efectuar al participante, independiente de que LA CONVOCANTE opte por hacer efectivas las garant�as otorgadas por el participante.\n" ,6,array('justification'=>'left'));

	//Numero de calidad
	$docPDF->ezText("" ,12,array('justification'=>'right'));
	$numCalreqiosi=fun_ObtieneNumCalidadReq();
	$docPDF->ezText("$numCalreqiosi" ,12,array('justification'=>'right'));
	
	
}




function creapdfInDividuales($orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc, $seleccion, $ppresupuestal, $tipoPrecio, $selprov)
{	
	global $server,$odbc_name,$username_db ,$password_db;

	$doc =& new Cezpdf('LATTER','landscape');
	
	$doc->selectFont('../../../fonts/arial.afm');
	$datacreator = array ('Title'=>'Creacion de PDF de la notificacion','Author'=>'Fomerrey','Subject'=>'Creacion de PDF de la notificacion','Creator'=>'fernando.llanas@fomerrey.gob.mx','Producer'=>'http://blog.unijimpe.net');
	$doc->addInfo($datacreator);
	$doc->ezSetCmMargins(0.01,0.5,0.5,0.5);
	
	creapdfPaImprimir($doc,$orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc, $seleccion, $ppresupuestal, $tipoPrecio, $selprov);
	
	$pdfcode = $doc->output();
	$dir = '../pdf_files';
	if(!file_exists($dir)){
		mkdir($dir, 0777);
	}
	
	$fname = $dir.'/OrdCompra_'.$orden.'.pdf';//tempnam(,'PDF_').//
	//echo $dir.'/'.$orden.'.pdf';
	$fp = fopen($fname, 'w');
	fwrite($fp,$pdfcode);
	fclose($fp);
	
	fun_cambiaPathDeRequisicion($fname,$orden);
	//$doc->ezStream();
}
//Funcion que llama procedimiento que obtiene codigo de requisicion
function fun_ObtieneNumCalidadReq()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=11;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}

//
function func_ObtenerProdDorden($orden,$ordenprov)
{
	global $server,$odbc_name,$username_db ,$password_db;
	
	$datos=array();
	$seleccion="";
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$command = "select b.requisi,a.iddrequisi,b.prod,a.observa as descprod, 
				a.cantidad,b.unidad,a.ivapct,a.precio".$ordenprov.",a.subtotal".$ordenprov." , a.ctapresup, a.observa
				from compradcuadroc a 
				left join compradrequisi b on a.iddrequisi=b.id
				left join compradproductos c on b.prod=c.prod
				left join compramordenes f on a.cuadroc=f.cuadroc 
				where orden=$orden and check".$ordenprov."=1";//left join compramseleccion g ON g.id=a.seleccion , g.descrip as seleccion
				//echo $command;
	//echo $command;
	$stmt = sqlsrv_query( $conexion,$command);
	if ( $stmt === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($stmt);
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			
			$datos[$i]['requisi']= trim($row['requisi']);
			$datos[$i]['ctapresup']= substr( trim($row['ctapresup']),0,3);		
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['descprod']= utf8_decode(trim($row['descprod']));//trim($row['descprod']).",".
			$datos[$i]['precio']= trim($row['precio'.$ordenprov]);
			$datos[$i]['subtotal']= trim($row['subtotal'.$ordenprov]);
			$datos[$i]['iva'] = ($datos[$i]['subtotal']*$row['ivapct'])/100;
			$datos[$i]['total']=$datos[$i]['subtotal']+$datos[$i]['iva'];			
			//datos[$i]['observa']= trim($row['observa']);
			$seleccion= "";//trim($row['seleccion']);
			$i++;
		}
	}
	sqlsrv_close( $conexion);	
	return array($datos, $seleccion);
}
///Funcion que cambia la ruta de la requisicion
function fun_cambiaPathDeRequisicion($path,$orden)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
			
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="UPDATE compramordenes SET path='$path' WHERE orden='$orden'";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;

}

function fun_ObtieneCabezalesSolicitante($usuario)
{
global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
			
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto COLLATE DATABASE_DEFAULT where numemp=$usuario";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			$depto = $row['depto'];
			$coord = $row['coord'];
			$dir = $row['dir'];
			$nomdepto = $row['nomdepto'];
			$nomdir = $row['nomdir'];
			//$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($depto,$coord ,$dir,$nomdepto ,$nomdir );

}
//Funcion que llama procedimiento almacenado para obtener	
function fun_ObtieneFirmasAutoriza($usuario)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="sp_Autoriza_requisi";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			$autoriza = $row['AUTORIZA'];
			$VoBo = $row['VOBO'];
			//$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($autoriza,$VoBo);
}
?>
<body>
<embed src='<?php echo $fname;?>#toolbar=1&navpanes=0&scrollbar=1' width='100%' height='800px'>
</body>
</html>
