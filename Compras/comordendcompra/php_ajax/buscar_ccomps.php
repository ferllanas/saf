<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
$conexion = sqlsrv_connect($server,$infoconexion);

$datos=array();
$fecini="";
if(isset($_GET['fecini']))
	$fecini=$_GET['fecini'];

$fecfin="";
if(isset($_GET['fecfin']))
	$fecfin=$_GET['fecfin'];

$command= "SELECT a.cuadroc, a.estatus, CONVERT(varchar(50),a.falta, 103) as falta,a.observa
				FROM compramcuadroc a ";	

if(strlen($fecini)>0)
{
	$command.= " WHERE a.falta>='$fecini'";
	if(strlen($fecfin)>0)
		$command.= " AND a.falta<='$fecfin'";
}
else
	if(strlen($fecfin)>0)
		$command.= " WHERE a.falta<='$fecfin'";
		
$command.= " ORDER BY cuadroc";

$getProducts = sqlsrv_query( $conexion_srv,$command);
if ( $getProducts === false)
{ 
	$resoponsecode="02";
	die($command."". print_r( sqlsrv_errors(), true));
}
else
{
	$resoponsecode="Cantidad rows=".count($getProducts);
	$i=0;
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
	{
		$datos[$i]['cuadroc']= trim($row['cuadroc']);
		$datos[$i]['falta']= trim($row['falta']);
		$datos[$i]['observa']= trim($row['observa']);
		$datos[$i]['estatus']= trim($row['estatus']);
		$i++;
	}
}
echo json_encode($datos);
?>