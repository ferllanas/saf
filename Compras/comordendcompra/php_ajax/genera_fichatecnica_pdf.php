<?php
require_once("../../../dompdf-master/dompdf_config.inc.php");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");

$usuario = $_COOKIE['ID_my_site'];

$partspresup=array();
$tipopcios=array();
$selecciones=array();
$datos=array();
$datosTot=array();
$parpresup="";
$tipopcio="";
$seleccion="";

$numeroCC="";
if(isset($_REQUEST['id_ccomp']))
	$numeroCC=$_REQUEST['id_ccomp'];

$nada= "";
$prov1= "";
$prov2= "";
$prov3= "";
$prov4= "";
$falta= "";
$halta= "";
$observacc="";	


	$command="select id, ppresupuestal, descrip FROM compramppresupuestal where estatus=0";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$partspresup[$i]['id']= trim($row['id']);
				$partspresup[$i]['ppresupuestal']= utf8_decode(trim($row['ppresupuestal']));
				$partspresup[$i]['descrip']= utf8_decode(trim($row['descrip']));
				$i++;
			}
		}

	$command="select tipopcio,  descrip FROM compramtipoprecio where estatus=0";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$tipopcios[$i]['tipopcio']= trim($row['tipopcio']);
				$tipopcios[$i]['descrip']= utf8_decode(trim($row['descrip']));
				
				if($tipopcio=="")
				{
					if($tipopcios[$i]['descrip']=="Fijos")
					{
						$tipopcio = $tipopcios[$i]['tipopcio'];
					}
				}
				$i++;
			}
		}

	$command= "select id, descrip from compramseleccion where estatus=0";
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion_srv, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$selecciones[$i]['id'] =trim($lrow['id']);
			$selecciones[$i]['descrip']=utf8_decode(trim($lrow['descrip']));
			$i++;
		}
	}

$continua=false;
//echo $numeroCC;
if(strlen($numeroCC)>0)
{
	
	//Nombres de proveedores
	$command="select a.cuadroc,a.observa,a.prov1,b.nomprov as nom1,a.prov2,c.nomprov as nom2,a.prov3,d.nomprov as nom3,a.prov4,e.nomprov as nom4 , 
			CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta, seleccion
			from compramcuadroc a 
			left join compramprovs b on a.prov1=b.prov
			left join compramprovs c on a.prov2=c.prov
			left join compramprovs d on a.prov3=d.prov
			left join compramprovs e on a.prov4=e.prov
			where a.cuadroc='$numeroCC' ";//AND a.estatus=0
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$continua=true;
			$nada= " ";
			$prov1= trim($row['nom1']);
			$prov2= trim($row['nom2']);
			$prov3= trim($row['nom3']);
			$prov4= trim($row['nom4']);
			$falta= trim($row['falta']);
			$halta= trim($row['halta']);
			$observacc=utf8_decode(trim($row['observa']));			
			$seleccion= trim($row['seleccion']);
		}
	}
	$numrequisi=0;
	$nomdepto="";
	if($continua==true)
	{	
		//Partidas de cuadro comparativo
		$command="select d.id, d.acepta, b.requisi, mr.depto, de.nomdepto,   a.cantidad,b.unidad,d.descrip as descripcion,a.precio1,a.total1,a.check1,
			a.precio2,a.total2,a.check2,a.precio3,a.total3,a.check3,
			a.precio4,a.total4,a.check4, d.acepta from compradcuadroc a 
			LEFT JOIN compradrequisi b on a.iddrequisi= b.id 
			LEFT JOIN compradproductos c on b.prod=c.prod 
			LEFT JOIN compramrequisi mr ON b.requisi=mr.requisi
			LEFT JOIN [nomemp].[dbo].nominamdepto de ON  de.depto COLLATE DATABASE_DEFAULT= mr.depto COLLATE DATABASE_DEFAULT 
			INNER JOIN compradcc_ftecnica d ON d.iddcuadroc=a.id
			
			where a.cuadroc=$numeroCC AND d.estatus<9000";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$numrequisi=$row['requisi'];
				$nomdepto= utf8_decode(trim($row['nomdepto']));
				$datos[$i]['cantidad']= trim($row['cantidad']);
				$datos[$i]['unidad']= trim($row['unidad']);
				$datos[$i]['descripcion']= utf8_decode(trim($row['descripcion']));
				$datos[$i]['acepta']=$row['acepta'];
				//$datos[$i]['precio1']= number_format(trim($row['precio1']),2);
				if($row['check1']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					if($row['acepta']==0)
						$noaceptada="";
					else
						$aceptada="";
						
					$datos[$i]['total1']= "<input type='radio' name='aceptaProd1_".$i."' id='aceptaProd1_".$i."_A'  value='".$row['id'].",0' $aceptada>";					
					$datos[$i]['precio1']= "<input type='radio' name='aceptaProd1_".$i."' id='aceptaProd1_".$i."_N' value='".$row['id'].",1' $noaceptada>";//number_format(trim($row['precio1']),2);
				}
				else	
				{
					$datos[$i]['total1']= "&nbsp;";//$datos[$i]['check1']= trim($row['check1']);
					$datos[$i]['precio1']= "&nbsp;";
				}
					
				//$datos[$i]['precio2']= number_format(trim($row['precio2']),2);
				if($row['check2']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					if($row['acepta']==0)
						$noaceptada="";
					else
						$aceptada="";
						
					$datos[$i]['total2']= "<input type='radio' name='aceptaProd2_".$i."' id='aceptaProd2_".$i."_A'  value='".$row['id'].",0' $aceptada>";//number_format(trim($row['total2']),2).
					$datos[$i]['precio2']= "<input type='radio' name='aceptaProd2_".$i."' id='aceptaProd2_".$i."_N' value='".$row['id'].",1' $noaceptada>";//number_format(trim($row['total2']),2).
				}
				else	
				{
					$datos[$i]['total2']= "&nbsp;";
					$datos[$i]['precio2']= "&nbsp;";
				}
				//$datos[$i]['check2']= trim($row['check2']);
				
				if($row['check3']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					if($row['acepta']==0)
						$noaceptada="";
					else
						$aceptada="";
						
					$datos[$i]['total3']= "<input type='radio' name='aceptaProd3_".$i."' id='aceptaProd3_".$i."_A' value='".$row['id'].",0' $aceptada>";//number_format(trim($row['total2']),2).//number_format(trim($row['total3']),2).
					$datos[$i]['precio3']="<input type='radio' name='aceptaProd3_".$i."' id='aceptaProd3_".$i."_N' value='".$row['id'].",1' $noaceptada>";// number_format(trim($row['precio3']),2);
				}
				else	
				{
					
					$datos[$i]['total3']= "&nbsp;";//number_format(trim($row['total2']),2).//number_format(trim($row['total3']),2).
					$datos[$i]['precio3']="&nbsp;";// number_format(trim($row['precio3']),2);
				}
				//$datos[$i]['check3']= trim($row['check3']);
				
				if($row['check4']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					if($row['acepta']==0)
						$noaceptada="";
					else
						$aceptada="";
					$datos[$i]['total4']= "<input type='radio' name='aceptaProd4_".$i."'  id='aceptaProd4_".$i."_A' value='".$row['id'].",0' $aceptada>";//number_format(trim($row['total4']),2).
					$datos[$i]['precio4']= "<input type='radio' name='aceptaProd4_".$i."' id='aceptaProd4_".$i."_N' value='".$row['id'].",1' $noaceptada>";
				}
				else	
				{
					$datos[$i]['total4']= "&nbsp;";//number_format(trim($row['total4']),2).
					$datos[$i]['precio4']= "&nbsp;";
				}
				//$datos[$i]['check4']= trim($row['check4']);
				
				
				$i++;
			}
		}	
	
		$command="select SUM(total1) as dato1,SUM(total2) as dato2,SUM(total3) as dato3,SUM(total4) as dato4 from compradcuadroc where cuadroc=$numeroCC and estatus=0 group by cuadroc ";
		//echo $command;
		$getProducts = sqlsrv_query($conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datosTot[0]['nada']= "Total Cotizado";
				$datosTot[0]['dato1']= number_format(trim($row['dato1']),2);
				$datosTot[0]['dato2']= number_format(trim($row['dato2']),2);
				$datosTot[0]['dato3']= number_format(trim($row['dato3']),2);
				$datosTot[0]['dato4']= number_format(trim($row['dato4']),2);			
				
			}
		}
		$command="
		select sum(case when  check1=1	then total1 else 0 end) as dato1,
			sum(case when  check2=1	then total2 else 0 end) as dato2,
			sum(case when  check3=1	then total3 else 0 end) as dato3,
			sum(case when  check4=1	then total4 else 0 end) as dato4 from compradcuadroc where cuadroc=$numeroCC and estatus=0 group by cuadroc ";
		//echo $command;
		$getProducts = sqlsrv_query($conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			//$datos=array();		
			while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datosTot[1]['nada']= "Total a Ordenar";
				$datosTot[1]['dato1']= trim($row['dato1']);
				$datosTot[1]['dato2']= trim($row['dato2']);
				$datosTot[1]['dato3']= trim($row['dato3']);
				$datosTot[1]['dato4']= trim($row['dato4']);			
				
			}
		}
	}
}

/*	
				
              
*//*table{
border:1px solid black;
page-break-before:always;
page-break-after:always;
}

table td{
border:1px solid black;
}
border-top: 2px solid #888888;
margin-bottom: 0.5em;
margin-top: 0.5em;
width: auto;

thead th {
border-bottom: 1px solid #880000;
color: #111111;
}

tbody th {
border-bottom: 1px solid #FFFFFF;
text-align: left;
}

tbody th {
background: none repeat scroll 0 0 #E6E6E6;
color: #333333;
}
*/
$string='
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
body{
	font-size: 7px;
}
table {
  width:100%;
}

thead {
  background-color: #eeeeee;
}

tbody {
  background-color: #ffffee;
}

th,td {
  padding: 3pt;
}

table.separate {
  border-collapse: separate;
  border-spacing: 5pt;
  border: 3pt solid #33d;
}

table.separate td {
  border: 2pt solid #33d;
}

table.collapse {
  width:100%;
  border-collapse: collapse;
  border: 1pt solid black;  
}

table.collapse td {
  border: 1pt solid black;
}

table.oculto {
	border: 0px;
}

table.oculto td {
  border: 0px;
}

td.borderNo {
	border: 0px;
}

td.borders {
  border: 1pt solid black;
}

</style>
</head>

<body>
<table width="100%" class="collapse">
		<tr><td width="100%" align="center" class="borderNo"><h1>FICHA TECNICA</h1></td></tr>
		<tr><td width="100%" align="center" class="borderNo" valign="top">Adjunto al presente sirvase encontrar original de las Propuestas(cotizaciones) con el fin de que sea formulado el Dictamen T&eacute;cnico(An&aacute;lisis de las Propuestas por parte del area solicitante) correspondiente y as&iacute; proseguir con los tramites de la adquisici&oacute;n solicitada.</td></tr>
</table>
<table width="100%" class="hidden">
    	<tr>
			<td width="100%" style="width: 100%;" >
				<table width="100%">
					<tr>
						<td width="50%" align="left">No. DE REQUISICI&oacute;N:'. $numrequisi.'</td>
						<td width="50%" align="center" ><u>NOMBRE DEL PROVEEDOR</u></td>
					</tr>
				</table>
			</td>
		</tr>
</table>
<table width="100%" class="hidden">
					<tr >
						<td colspan="8" width="50%"  >AREA SOLICITANTE: '.$nomdepto.'</td>
						<td colspan="2" width="12.5%" class="borders" align="center" >'.$prov1.'</td>
						<td colspan="2" width="12.5%" class="borders" align="center">'. $prov2.'</td>
						<td colspan="2" width="12.5%" class="borders" align="center">'. $prov3.'</td>
						<td colspan="2" width="12.5%" class="borders" align="center">'. $prov4.'</td>
					</tr>
</table>
		
<table width="100%" class="collapse">
	<thead width="100%">
		<tr>
			<td style="width:6.25%;" align="center">PARTIDA</td>
			<td style="width:43.75%;" align="center">DESCRIPCION DE LOS MATERIALES</td>
			<td style="width:6.25%;" style="width:"  align="center">CUMPLE</td>
			<td style="width:6.25%;" align="center">NO CUMPLE</td>
			<td style="width:6.25%;" align="center">CUMPLE</td>
			<td style="width:6.25%;" align="center">NO CUMPLE</td>
			<td style="width:6.25%;" align="center">CUMPLE</td>
			<td style="width:6.25%;" align="center">NO CUMPLE</td>
			<td style="width:6.25%;" align="center">CUMPLE</td>
			<td style="width:6.25%;" align="center">NO CUMPLE</td>
		</tr>
	</thead>
	<tbody >
   ';
	
		
		if( $datos!=null) 
			for($i=0;$i<count($datos);$i++){ 
	$string.='<tr>
			<td align="center">'.($i+1).'</td>
			<td align="left">'.utf8_encode($datos[$i]['descripcion']).'</td>
			<td align="center">'; 
		if($datos[$i]['acepta']==1) 
		{$string.=' <h1>X</h1>'; }
	$string.='</td>';
	$string.='<td  align="center">'; 
		if($datos[$i]['acepta']==0)
		{ $string.=' <h1>X</h1>';}
	$string.='</td>';
	$string.='<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
			<td align="center">&nbsp;</td>
		</tr>';
			}/**/
	$string.='</tbody>
</table>
<table>
    <tr>
		<td>
			<table>
				<tr>
					<td align="left">IMPORTE DE LA REQUISICI&oacute;N<P>
					IMPORTE COTIZADO<p>
					DIFERENCIA
					</td>
				</tr>
			</table>
		</td>
    </tr>
</table>
<table>
    <tr>
    	<td width="100%" >
        	<table width="100%" class="noborders" >
            	<tr>
                	<td width="50%" align="center" >
                    	<table  width="50%" class="noborders">
                            <tr>
                                <td  width="20%" align="center" >FIRMA DE CONFORMIDAD</td>
                            </tr>
                            <tr>
                                <td  width="20%" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td  width="20%" ><HR /></td>
                            </tr>
                            <tr>
                                <td  width="20%" align="center" >AREA SOLICITANTE(NOMBRE Y FIRMA)</td>
                            </tr>
                       	</table>
                   </td>
                   <td width="50%" valign="bottom" align="right">MO00809-1</td>
            </table>
        </td>
    </tr>
</table>
</body>
</html>';
//echo $string;

$dompdf = new DOMPDF();
$dompdf->set_paper('letter', "landscape");
$dompdf->load_html($string);

$dompdf->render();
	  
$pdf = $dompdf->output(); 
$dir = '../pdf_files';
if(!file_exists($dir))
{
	mkdir($dir, 0777);
}


$dompdf->stream("../pdf_files/ficha_tecnica.pdf");// $domPDF->stream($pdf, array('Attachment'=>0));
?>