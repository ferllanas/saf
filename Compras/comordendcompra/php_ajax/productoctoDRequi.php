<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
//echo $_GET['idrequi']."!!!";
if(isset($_GET['idrequi']) && $conexion)
{
	$termino=$_GET['idrequi'];
	$command= "SELECT a.id, a.prod, a.descrip, a.cantidad, a.unidad, a.saldo, a.requisi, b.nomprod 
				FROM compradrequisi a 
				INNER JOIN compradproductos b on a.prod=b.prod 
				WHERE requisi='$termino' AND a.saldo>0 ORDER BY id ASC";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode = "Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']= trim($row['id']);
			//echo htmlentities(utf8_decode(trim($row['descrip'])));
			//echo eregi_replace("[\n|\r|\n\r]", ' ', utf8_encode(trim($row['descrip'])));
			//echo trim($row['descrip']).", ". trim($row['nomprod']).">>".htmlentities( utf8_decode(trim($row['descrip'])))."\n";
			$datos[$i]['prod']= htmlentities( utf8_decode(trim($row['prod'])));
			$datos[$i]['descrip']= htmlentities(utf8_decode(trim($row['descrip'])));
			$datos[$i]['cantidad']= trim($row['saldo']);//Cantidad
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['saldo']= trim($row['saldo']);
			$datos[$i]['requisi']= trim($row['requisi']);
			$datos[$i]['nomprod']= htmlentities( trim($row['nomprod']));
			$i++;
		}
	}
	echo json_encode($datos);

}
?>