<?php
	require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$usuario = $_COOKIE['ID_my_site'];
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$command="";
$countModif=0;
if( isset( $_REQUEST['datos'] ) && isset($_REQUEST['datosini']) )
{
	$datos=$_REQUEST['datos'];
	$datosini=$_REQUEST['datosini'];
	for($i=0;$i<count($datos);$i++)
	{
		
		if( valorInicialDiferente($datos[$i],$datosini) )
		{
			
			$tsql_callSP ="{call sp_compras_evalua_dcc_ftecnica(?,?,?)}";//Arma el procedimeinto almacenado
			$params = array(&$datos[$i]['id'],&$datos[$i]['acepta'],&$usuario,);//Arma parametros de entrada
			$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
			/*echo "sp_compras_A_mcuadroc($provid1,$tentrega1,$condicion1,$provid2,$tentrega2,$condicion2,$provid3,$tentrega3,$condicion3,
							$provid4,$tentrega4,$condicion4,$usuario,$observaciones)";*/
			$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
			if( $stmt === false )
			{
				 $fails= "Error in statement execution.\n";
				 $fails=true;
				 die( print_r( sqlsrv_errors(), true));
			}	
			if(!$fails)
			{
				// Retrieve and display the first result. 
				/*while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
				{
					$id = $row['folio'];	
					$fecha = $row['fecha'];
					$hora = $row['hora'];					
				}
				sqlsrv_free_stmt( $stmt);*/
			}

			/*$command="UPDATE compradcc_ftecnica SET estatus=10, acepta=".$datos[$i]['acepta']." WHERE  id= ".$datos[$i]['id'];
			//echo $command;
			$countModif++;
			$stmt2 = sqlsrv_query( $conexion,$command);
			if( $stmt2 === false)
			{
				echo "Error in executing statement 3.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}*/
		}
	}
}
$reques['error']=0;
$reques['msg']='Se ha modificado la ficha tecnica con exito.';
$reques['command']=$command;
$reques['modificados']=$countModif;

echo json_encode($reques);

function valorInicialDiferente($comparar,$datosini)
{
	for($j=0;$j<count($datosini);$j++)
	{
		if( $comparar['id'] == $datosini[$j]['id'] )
		{
			if( $comparar['acepta'] != $datosini[$j]['aceptada'])
			{
				//echo "!".$comparar['acepta'] ."!=". $datosini[$j]['aceptada'];
				return true;
			}
		}
	}
	return false;
}
?>