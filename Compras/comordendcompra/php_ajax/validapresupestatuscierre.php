<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	
	$cierre="";//Observaciones
	//if(isset($_GET['cuadroc']));
	//	$cuadroc=$_GET['cuadroc'];		
	list($fails,$cierre)=func_validapresupestatuscierre();	//,$Autorizado	$cuadroc
	$datos=array();//prepara el aray que regresara		
	$datos[0]['fallo']=$fails;
	$datos[0]['cierre']=trim($cierre);	
	//$datos[0]['Autorizado']=trim($Autorizado);	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array

//funcion que registra una nueva requisicion
function func_validapresupestatuscierre()
{
	global $server,$odbc_name,$username_db ,$password_db;
	$fails=false;
	$Autorizado='';
	$texto = "";	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_presup_c_estatus_cierre()}";//Arma el procedimeinto almacenado
	$params = array(&$cuadroc);//Arma parametros de entrada
	//echo "sp_presup_valida_xcuadroc ".$cuadroc;
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		// die( print_r( sqlsrv_errors(), true));
	}	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//$Autorizado = $row['PROCEDE'];	
			$ctas = $row['cierre'];							
		}
		sqlsrv_free_stmt( $stmt);
	}

	sqlsrv_close( $conexion);
	return array($fails,$ctas);//,$Autorizado
}

?>