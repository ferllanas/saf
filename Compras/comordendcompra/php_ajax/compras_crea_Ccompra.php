<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$id="";
	
	$seleccion = "";//Variable id del usuario
	if(isset($_POST['seleccion']));
		$seleccion=$_POST['seleccion'];

	$usuario = "";//Variable id del usuario
	if(isset($_POST['usuario']));
		$usuario=$_POST['usuario'];
			
	$observaciones="";//Observaciones
	if(isset($_POST['observaciones']));
		$observaciones=$_POST['observaciones'];
	// datos de los Proveedores	
	$provid1="";
	if(isset($_POST['provid1']));
		$provid1=$_POST['provid1'];
		
	$provid2="";
	if(isset($_POST['provid2']));
		$provid2=$_POST['provid2'];
		
	$provid3="";
	if(isset($_POST['provid3']));
		$provid3=$_POST['provid3'];
		
	$provid4="";
	if(isset($_POST['provid4']));
		$provid4=$_POST['provid4'];
		
	// datos de las Condiciones
	$condicion1="";//
	if(isset($_POST['condicion1']));
		$condicion1=$_POST['condicion1'];
		
	$condicion2="";//
	if(isset($_POST['condicion2']));
		$condicion2=$_POST['condicion2'];
		
	$condicion3="";//
	if(isset($_POST['condicion3']));
		$condicion3=$_POST['condicion3'];
		
	$condicion4="";//
	if(isset($_POST['condicion4']));
		$condicion4=$_POST['condicion4'];
		
	// datos de los Tiempos de ENtrega
	$tentrega1="";//
	if(isset($_POST['tentrega1']));
		$tentrega1=$_POST['tentrega1'];
		
	$tentrega2="";//
	if(isset($_POST['tentrega2']));
		$tentrega2=$_POST['tentrega2'];
		
	$tentrega3="";//
	if(isset($_POST['tentrega3']));
		$tentrega3=$_POST['tentrega3'];
		
	$tentrega4="";//
	if(isset($_POST['tentrega4']));
		$tentrega4=$_POST['tentrega4'];
	
	$productos="";//variable que almacena la cadena de los productos
	if(isset($_POST['prods']))
		$productos=$_POST['prods'];//Obtiene cadena de productos

	$prod = "";	
	$ivapct = "";
	$ctapresup = "";
	$fecha="";
	$fails=false;
	$hora="";
	$infoconexion=array( 'UID' => $username_db, 'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	//print_r($productos);
	if (is_array( $productos )) // Pregunta se es una array la variable productos
	{
		//exit("salio por aca");
		//Crear en tabla de requisicion
		list($id,$fecha,$fails,$hora)= fun_creaCcompara($usuario,$observaciones,$provid1,$provid2,$provid3,$provid4,
														$condicion1,$condicion2,$condicion3,$condicion4,$tentrega1,
														$tentrega2,$tentrega3,$tentrega4, $seleccion); //crea nueva requisicion
		//echo "<br><b>".$id."</b></br>";
		if(!$fails)
			for($i=0;$i<count($productos);$i++)
			{
				//Agrega producto a requisicion
				$consulta="select a.prod,b.ivapct,b.ctapresup from compradrequisi a left join compradproductos b on a.prod=b.prod where a.id=".$productos[$i]['idprodEnRequi'];
				$stmt = sqlsrv_query($conexion, $consulta);
				//print_r($stmt);
				if( $stmt === false )
				{
					 $fails= "Error in statement execution.\n";
					 $fails=true;
					//die( print_r( sqlsrv_errors(), true));
				}
				
				if(!$fails)
				{
					// Retrieve and display the first result. 
					
					while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
					{
						//print_r($row);
						$prod = $row['prod'];	
						$ivapct = $row['ivapct'];
						$ctapresup = $row['ctapresup'];					
					}
					
					sqlsrv_free_stmt( $stmt);
				}


				
					$fails=func_creaProductoDCcompara($id, 
										$productos[$i]['idprodEnRequi'], 
										$productos[$i]['cantidad'], 
										$ivapct,
										str_replace(",","",$productos[$i]['presUniprovA']),
										str_replace(",","",$productos[$i]['cantxPresUniA']),
										$productos[$i]['checkSelectPropuesta'],
										str_replace(",","",$productos[$i]['presUniprovB']),
										str_replace(",","",$productos[$i]['cantxPresUniB']),
										$productos[$i]['checkSelectPropuestaB'],
										str_replace(",","",$productos[$i]['presUniprovC']),
										str_replace(",","",$productos[$i]['cantxPresUniC']),
										$productos[$i]['checkSelectPropuestaC'],
										str_replace(",","",$productos[$i]['presUniprovD']),
										str_replace(",","",$productos[$i]['cantxPresUniD']),
										$productos[$i]['checkSelectPropuestaD'],
										$ctapresup,
										$productos[$i]['observa']);	
		}
	}
	
	$datos=array();//prepara el aray que regresara
	$datos[0]['id']=trim($id);// regresa el id de la
	$datos[0]['fecha']=trim($fecha);
	$datos[0]['fallo']=$fails;
	$datos[0]['hora']=trim($hora);
	
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_creaCcompara($usuario,$observaciones,$provid1,$provid2,$provid3,$provid4,$condicion1,$condicion2,$condicion3,$condicion4,$tentrega1,$tentrega2,$tentrega3,$tentrega4,$seleccion)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$fecha = "";
	$hora ="";
	$id = "";
	
	
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_mcuadroc(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	$params = array(&$provid1,&$tentrega1,&$condicion1,&$provid2,&$tentrega2,&$condicion2,&$provid3,&$tentrega3,&$condicion3,
					&$provid4,&$tentrega4,&$condicion4,&$usuario,&$observaciones, &$seleccion);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	/*echo "sp_compras_A_mcuadroc($provid1,$tentrega1,$condicion1,$provid2,$tentrega2,$condicion2,$provid3,$tentrega3,$condicion3,
					$provid4,$tentrega4,$condicion4,$usuario,$observaciones)";*/
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$id = $row['folio'];	
			$fecha = $row['fecha'];
			$hora = $row['hora'];					
		}
		sqlsrv_free_stmt( $stmt);
	}

	//echo "<br>".$id."</br>";
	sqlsrv_close( $conexion);
	return array($id,$fecha,$fails,$hora);
}

//Funcion para registrar el p�roductoa  la requisicion
function func_creaProductoDCcompara($id,
$idprodEnRequi,
$cantidad, 
$ivapct,
$presUniprov1,
$cantxPresUni1,
$checkSelectPropuesta,
$presUniprov2,
										$cantxPresUni2,
										$checkSelectPropuesta1,
										$presUniprov3,
										$cantxPresUni3,
										$checkSelectPropuesta2,
										$presUniprov4,
										$cantxPresUni4,
										$checkSelectPropuesta3,
										$ctapresup,
										$observa)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	//$id="";//Store proc regresa id de la nueva requisicion
	//$fecha=""; //fecha de la nueva requisicion
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_dcuadroc(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$productos[$i]['producto'], $productos[$i]['cantidad'], $productos[$i]['descripcion'],'0',$productos[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	if($checkSelectPropuesta=='true')
	{
		$check1=1;
	}
	else
	{
		$check1=0;
	}
	if($checkSelectPropuesta1=='true')
	{
		$check2=1;
	}
	else
	{
		$check2=0;
	}
	if($checkSelectPropuesta2=='true')
	{
		$check3=1;
	}
	else
	{
		$check3=0;
	}
	if($checkSelectPropuesta3=='true')
	{
		$check4=1;
	}
	else
	{
		$check4=0;
	}
	$params = array(&$id,&$idprodEnRequi,&$cantidad, &$ivapct,&$presUniprov1,&$cantxPresUni1,&$check1,&$presUniprov2,
										&$cantxPresUni2,
										&$check2,
										&$presUniprov3,
										&$cantxPresUni3,
										&$check3,
										&$presUniprov4,
										&$cantxPresUni4,
										&$check4,
										&$ctapresup,
										&$observa);
	/*echo "	call sp_compras_A_dcuadroc($id,$idprodEnRequi,$cantidad, $ivapct,$presUniprov1,$cantxPresUni1,$check1,$presUniprov2,
										$cantxPresUni2,
										$check2,
										$presUniprov3,
										$cantxPresUni3,
										$check3,
										$presUniprov4,
										$cantxPresUni4,
										$check4,
										$ctapresup)";*/
										//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close( $conexion);
	return $fails;
}
?>