<?php
require_once("../../../dompdf/dompdf_config.inc.php");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$usuario = getNombre($_COOKIE['ID_my_site']);

$idccomp="";
if(isset($_REQUEST['id']))
	$idccomp=$_REQUEST['id'];
	
$totalprov1=0.00;
$totcompprov1=0.00;
$totalprov2=0.00;
$totcompprov2=0.00;
$totalprov3=0.00;
$totcompprov3=0.00;
$totalprov4=0.00;
$totcompprov4=0.00;
$observa="";
$falta="";
$halta="";

$proveedores=array();

//////////////////////////////////////////////////////////////////////////////////////
$command="select a.cuadroc,a.observa,a.prov1,b.nomprov as nom1,a.prov2,c.nomprov as nom2,a.prov3,d.nomprov as nom3,a.prov4,e.nomprov as nom4 , 
			CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta
			from compramcuadroc a 
			left join compramprovs b on a.prov1=b.prov
			left join compramprovs c on a.prov2=c.prov
			left join compramprovs d on a.prov3=d.prov
			left join compramprovs e on a.prov4=e.prov
			where a.cuadroc='$idccomp'";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$proveedores[$i]['nada']= " ";
			$proveedores[$i]['nom1']= trim($row['nom1']);
			$proveedores[$i]['nom2']= trim($row['nom2']);
			$proveedores[$i]['nom3']= trim($row['nom3']);
			$proveedores[$i]['nom4']= trim($row['nom4']);
			$falta= trim($row['falta']);
			$halta= trim($row['halta']);
			$observa=utf8_decode(trim($row['observa']));			
			$i++;
		}
	}

$partidas=array();
$command="select a.cantidad,b.unidad,a.observa as descripcion,a.precio1,a.total1,a.check1,
		a.precio2,a.total2,a.check2,a.precio3,a.total3,a.check3,
		a.precio4,a.total4,a.check4 from compradcuadroc a 
		left join compradrequisi b on a.iddrequisi= b.id 
		left join compradproductos c on b.prod=c.prod where a.cuadroc=$idccomp";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$partidas[$i]['cantidad']= number_format(trim($row['cantidad']));
			$partidas[$i]['unidad']= trim($row['unidad']);
				//echo "!".$row['descripcion']."!";
			$partidas[$i]['descripcion']= utf8_decode(trim($row['descripcion']));
			
			$partidas[$i]['precio1']= number_format(trim($row['precio1']),2);
			if($row['check1']==1)
				$partidas[$i]['total1']= number_format(trim($row['total1']),2)." X";
			else	
				$partidas[$i]['total1']= number_format(trim($row['total1']),2);//$partidas[$i]['check1']= trim($row['check1']);
			$partidas[$i]['precio2']= number_format(trim($row['precio2']),2);
			//$partidas[$i]['total2']= trim($row['total2']);
			if($row['check2']==1)
				$partidas[$i]['total2']= number_format(trim($row['total2']),2)." X";
			else	
				$partidas[$i]['total2']=number_format( trim($row['total2']),2);
			//$partidas[$i]['check2']= trim($row['check2']);
			$partidas[$i]['precio3']= number_format(trim($row['precio3']),2);
			if($row['check3']==1)
				$partidas[$i]['total3']= number_format(trim($row['total3']),2)." X";
			else	
				$partidas[$i]['total3']= number_format(trim($row['total3']),2);
			//$partidas[$i]['check3']= trim($row['check3']);
			$partidas[$i]['precio4']= number_format(trim($row['precio4']),2);
			if($row['check4']==1)
				$partidas[$i]['total4']= number_format(trim($row['total4']),2)." X";
			else	
				$partidas[$i]['total4']= number_format(trim($row['total4']),2);
			//$partidas[$i]['check4']= trim($row['check4']);
			
			
			$i++;
		}
	}	

$totalesCotizado=array();
$command="select SUM(total1) as dato1,SUM(total2) as dato2,SUM(total3) as dato3,SUM(total4) as dato4 from compradcuadroc where cuadroc=$idccomp group by cuadroc ";
	$getProducts = sqlsrv_query($conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		;		
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$totales[0]['nada']= "Total Cotizado";
			$totalesCotizado[0]['dato1']= number_format(trim($row['dato1']),2);
			$totalesCotizado[0]['dato2']= number_format(trim($row['dato2']),2);
			$totalesCotizado[0]['dato3']= number_format(trim($row['dato3']),2);
			$totalesCotizado[0]['dato4']= number_format(trim($row['dato4']),2);			
			
		}
	}

$totalAOrdenar=array();
$command="select sum(case when  check1=1	then total1 else 0 end) as dato1,
		sum(case when  check2=1	then total2 else 0 end) as dato2,
		sum(case when  check3=1	then total3 else 0 end) as dato3,
		sum(case when  check4=1	then total4 else 0 end) as dato4 from compradcuadroc where cuadroc=$idccomp group by cuadroc ";
	$getProducts = sqlsrv_query($conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$totalAOrdenar[0]['nada']= "Total a Ordenar";
			$totalAOrdenar[0]['dato1']= number_format(trim($row['dato1']),2);
			$totalAOrdenar[0]['dato2']= number_format(trim($row['dato2']),2);
			$totalAOrdenar[0]['dato3']= number_format(trim($row['dato3']),2);
			$totalAOrdenar[0]['dato4']= number_format(trim($row['dato4']),2);		
			
		}
	}

	$tiempoEntrega=array();
	$command="select tentrega1,tentrega2,tentrega3,tentrega4 from compramcuadroc where cuadroc='$idccomp'";
	$getProducts = sqlsrv_query($conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
				
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$tiempoEntrega[0]['nada']= "Tiempo de Entrega";
			$tiempoEntrega[0]['dato1']= trim($row['tentrega1']);
			$tiempoEntrega[0]['dato2']= trim($row['tentrega2']);
			$tiempoEntrega[0]['dato3']= trim($row['tentrega3']);
			$tiempoEntrega[0]['dato4']= trim($row['tentrega4']);			
			
		}
	}
	
	$condiciones=array();
	$command="select condiciones1,condiciones2,condiciones3,condiciones4 from compramcuadroc where cuadroc='$idccomp'";
	$getProducts = sqlsrv_query($conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			//$condiciones[2]['nada']= "Condiciones";
			$condiciones[0]['dato1']= trim($row['condiciones1']);
			$condiciones[0]['dato2']= trim($row['condiciones2']);
			$condiciones[0]['dato3']= trim($row['condiciones3']);
			$condiciones[0]['dato4']= trim($row['condiciones4']);			
			
		}
	}

$numCalCuadro=fun_ObtieneNumCalidadCuadro( );

$reviso_n="";$reviso_p ="";$autorizo_n ="";$autorizo_p="";
list($reviso_n, $reviso_p,$autorizo_n,$autorizo_p)=fun_ObtieneRevisionAutorizacion( );

$HTML=armar_htmlstring($idccomp,$proveedores,$partidas,$totalesCotizado,$totalAOrdenar, $numCalCuadro, $falta, $halta, $observa, $condiciones, $tiempoEntrega,$reviso_n, $reviso_p,$autorizo_n,$autorizo_p, $usuario);
$dompdf = new DOMPDF();
$dompdf->load_html($HTML);
$dompdf->set_paper('letter', "landscape");
$dompdf->render();
$pdf = $dompdf->output();

$tmpfile = tempnam(".", "dompdf_.pdf");
file_put_contents($tmpfile, $pdf ); // Replace $smarty->fetch()
rename($tmpfile,$tmpfile.".pdf");
header('Content-Type: application/pdf');
header('Content-Disposition: attachment; filename="cuadrocomparativo_$idccomp.pdf"');
header("Location: ".basename($tmpfile.".pdf"));

$files = glob('*.tmp.pdf'); // get all file names
foreach($files as $file){ // iterate files
  if(is_file($file) && basename($tmpfile.".pdf")!= basename($file))
    unlink($file); // delete file
}
/*
$dompdf->set_paper('letter', "landscape");
$dompdf->load_html($HTML);

$dompdf->render();
$dompdf->stream("cuadro comparativo.pdf");	
*/	  
/*$pdf = $dompdf->output(); 
$dir = '../pdf_files';
if(!file_exists($dir))
{
	mkdir($dir, 0777);
}
//ile_put_contents("../pdf_files/".$idReq.".pdf", $pdf);
file_put_contents( $dir.'/cuadroc_'.$idccomp.'.pdf', $pdf);//"../pdf_files/".$idReq.".pdf");
*/
//$dompdf->stream("../pdf_files/".$idReq.".pdf");// $domPDF->stream($pdf, array('Attachment'=>0));
$datos[0]['path']=$dir.'/cuadroc_'.$idccomp.'.pdf';
$datos[0]['fallo']=false;
$datos[0]['id']=$idccomp;

echo json_encode($datos);

//echo $HTML;

function armar_htmlstring($idccomp,$proveedores,$partidas,$totalesCotizado,$totalAOrdenar, $numCalCuadro, $falta, $halta, $observa, $condiciones, $tiempoEntrega, $reviso_n, $reviso_p,$autorizo_n,$autorizo_p, $usuario)
{
	global $imagenPDFPrincipal;
$pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	$textod=utf8_encode("Página {PAGE_NUM}");
	$textop=utf8_encode("Cuadro comparativo");
	$pdf->page_text(25, 580, "$textop '.$idccomp.'", "", 12, array(0,0,0)); 
	$pdf->page_text(650, 580, "Pagina {PAGE_NUM}", "", 12, array(0,0,0)); 
	
 } 
</script> '; //$pdf->page_text(650, 592, "MO.2000.2044.1", "", 12, array(0,0,0));

$html="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
			margin:  0.25in 0.5in 0.5in 0.5in;
		}

		#twmaarco
		{
			border-collapse:collapse;
		}
		
		#twmaarco td
		{
			border:1pt solid black;
			border-collapse:collapse;
			padding:2px 2px 2px 2px; 
		}

		#twmaarco th
		{
			border:1pt solid black;
			border-collapse:collapse;
			padding:2px 2px 2px 2px; 
		}

		#twmaarco tr.d0 td {
			background-color: #FFFFFF;
		}
		#twmaarco tr.d1 td {
			background-color: #EAEAEA;
		}

		td {padding: 0;}
		
		

		.textobold{
			font-weight:bold;
		}
		
		.texto16b{ 
			font-size:16pt;
			font-weight:bold;
		}
		.texto14{ 
			font-size:14pt;
		}
		.texto14b{ 
			font-size:15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size:12pt;
			font-weight:bold;

		}
		.texto12{ 
			font-size:12pt;
		}
		.texto11{
			font-size:11pt;
		}
		.texto10{
			font-size:10pt;
		}
		</style>

</head>

<body>
<table width='100%'>
	<tr>
		<TD align='right' class='texto12'>$numCalCuadro</TD>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='20%'><img src='../../../$imagenPDFPrincipal' style='width:150px; height:57px'></td>
					<td width='60%'>
						<table width='100%'>
							<tr>
								<td align='center' class='texto14b'><b>Fomento Metropolitano de Monterrey</b></td>
							</tr>
							<tr>
								<td align='center' class='texto14b'><b>".utf8_encode("Dirección de Administración y Finanzas")."</b></td>
							</tr>
						</table>
					</td>
					<td width='20%'>&nbsp;</td>
				</tr>
				<tr>
					<td align='left' class='texto14' colspan=3>".utf8_encode("Cuadro Comparativo")." No. $idccomp</td>
				</tr>
				<tr>
					<td align='right' class='texto10' colspan=3><strong>Fecha:</strong> $falta, $halta</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%' name='twmaarco' id='twmaarco'>
				<tr>
					<td colspan='3'>&nbsp;</td>
					<td colspan='2' width='100' align='center'>".utf8_encode($proveedores[0]['nom1'])."</td>
					<td colspan='2' width='100' align='center'>".utf8_encode($proveedores[0]['nom2'])."</td>
					<td colspan='2' width='100' align='center'>".utf8_encode($proveedores[0]['nom3'])."</td>
					<td colspan='2' width='100' align='center'>".utf8_encode($proveedores[0]['nom4'])."</td>
				</tr>
				<tr height='1px'>
					<td colspan='11' height='1px'></td>
				</tr>
			<thead>
				<tr>
					<th align='center' width='50'>Cantidad</th>
					<th align='center' width='50'>Unidad</th>
					<th align='center' width='170' style='max-width:170px'>".utf8_encode('Descripción')."</th>
					<th align='center' width='50'>P.U.</th>
					<th align='center' width='50'>Importe</th>
					<th align='center' width='50'>P.U.</th>
					<th align='center' width='50'>Importe</th>
					<th align='center' width='50'>P.U.</th>
					<th align='center' width='50'>Importe</th>
					<th align='center' width='50'>P.U.</th>
					<th align='center' width='50'>Importe</th>
				</tr>
			</thead>";
			for($i=0; $i<count($partidas);$i++)
			{
				$html.="<tr class='d".($i%2)."'>
					<td align='center' width='50'>".$partidas[$i]['cantidad']."</td>
					<td align='center' width='50'>".$partidas[$i]['unidad']."</td>";
					
					$partidas[$i]['descripcion'] = str_replace ("," , ", ",$partidas[$i]['descripcion']);
				if(strlen($partidas[$i]['descripcion'])>0)
					$html.="<td width='170'>".utf8_encode($partidas[$i]['descripcion'])."</td>";
				else
					$html.="<td width='170'>&nbsp;</td>";

					$html.="<td align='center' width='50'>".$partidas[$i]['precio1']."</td>
					<td align='center' width='50'>".$partidas[$i]['total1']."</td>
					<td align='center' width='50'>".$partidas[$i]['precio2']."</td>
					<td align='center' width='50'>".$partidas[$i]['total2']."</td>
					<td align='center' width='50'>".$partidas[$i]['precio3']."</td>
					<td align='center' width='50'>".$partidas[$i]['total3']."</td>
					<td align='center' width='50'>".$partidas[$i]['precio4']."</td>
					<td align='center' width='50'>".$partidas[$i]['total4']."</td>
				</tr>";
			}

		$html.="<tr>
					<td colspan='3' align='right'>Total Cotizado<br>Total a ordenar</td>
					<td colspan='2' align='center'>".$totalesCotizado[0]['dato1']."<br>".$totalAOrdenar[0]['dato1']."</td>
					<td colspan='2' align='center'>".$totalesCotizado[0]['dato2']."<br>".$totalAOrdenar[0]['dato2']."</td>
					<td colspan='2' align='center'>".$totalesCotizado[0]['dato3']."<br>".$totalAOrdenar[0]['dato3']."</td>
					<td colspan='2' align='center'>".$totalesCotizado[0]['dato4']."<br>".$totalAOrdenar[0]['dato4']."</td>
				</tr>
				<tr>
					<td colspan='3' align='right'>Tiempo de entrega<br>Condiciones</td>
					<td colspan='2' align='center'>".$tiempoEntrega[0]['dato1']."<br>".$condiciones[0]['dato1']."</td>
					<td colspan='2' align='center'>".$tiempoEntrega[0]['dato2']."<br>".$condiciones[0]['dato2']."</td>
					<td colspan='2' align='center'>".$tiempoEntrega[0]['dato3']."<br>".$condiciones[0]['dato3']."</td>
					<td colspan='2' align='center'>".$tiempoEntrega[0]['dato4']."<br>".$condiciones[0]['dato4']."</td>
				</tr>
			</table> 
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Observaciones: $observa</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style='width=100%' align='center' width='100%'>
			<table style='width=100%' align='center' width='100%'>
				<tr>
					<td style='width=30%'><hr></td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'><hr></td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'><hr></td>
				</tr>
				<tr><td style='width=30%'>Formul&oacute;</td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'>Revis&oacute;</td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'>Autoriz&oacute;</td>
				</tr>
				<tr>
					<td style='width=30%'>".utf8_encode($usuario)."</td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'>".utf8_encode($reviso_n)."</td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'>".utf8_encode($autorizo_n)."</td>
				</tr>
				<tr><td style='width=30%'>&nbsp;</td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width=30%'>".utf8_encode($reviso_p)."</td>
					<td style='width=5%'>&nbsp;</td>
					<td style='width:200px'>".utf8_encode($autorizo_p)."</td>
				</tr>
				
			</table></td>
	</tr>
</table>
".$pageheadfoot."
</body>
</html>";

return $html;
}

function fun_ObtieneRevisionAutorizacion( )
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$reviso_n=""; $reviso_p="";$autorizo_n="";$autorizo_p="";
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=12;
	$tsql_callSP ="SELECT * FROM v_firmas_cc";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$reviso_n=$row['revisa_n'];
			$reviso_p=$row['revisa_p'];
			$autorizo_n=$row['autoriza_n'];
			$autorizo_p=$row['autoriza_p'];
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($reviso_n, $reviso_p,$autorizo_n,$autorizo_p);
}
function fun_ObtieneNumCalidadCuadro()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=12;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}

function getNombre($numemp)
{
	
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=12;
	$tsql_callSP ="SELECT LTRIM(RTRIM(b.TITULO))+' '+LTRIM(RTRIM(b.nomemp)) as firmaquien 
			FROM  v_nomina_empleadostodos b 
			where b.numemp='$numemp';";
			//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//echo $tsql_callSP;
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['firmaquien'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}
?>
