<?php
require_once("../../../dompdf-master/dompdf_config.inc.php");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");

$usuario = $_COOKIE['ID_my_site'];;

$partspresup=array();
$tipopcios=array();
$selecciones=array();
$datos=array();
$datos2=array();
$parpresup="";
$tipopcio="";
$seleccion="";

$numeroCC="";
if(isset($_REQUEST['id_ccomp']))
	$numeroCC=$_REQUEST['id_ccomp'];

$nada= "";
$prov1= "";
$prov2= "";
$prov3= "";
$prov4= "";
$falta= "";
$halta= "";
$observacc="";	


	$command="select id, ppresupuestal, descrip FROM compramppresupuestal where estatus=0";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$partspresup[$i]['id']= trim($row['id']);
				$partspresup[$i]['ppresupuestal']= utf8_decode(trim($row['ppresupuestal']));
				$partspresup[$i]['descrip']= utf8_decode(trim($row['descrip']));
				$i++;
			}
		}

	$command="select tipopcio,  descrip FROM compramtipoprecio where estatus=0";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$tipopcios[$i]['tipopcio']= trim($row['tipopcio']);
				$tipopcios[$i]['descrip']= utf8_decode(trim($row['descrip']));
				
				if($tipopcio=="")
				{
					if($tipopcios[$i]['descrip']=="Fijos")
					{
						$tipopcio = $tipopcios[$i]['tipopcio'];
					}
				}
				$i++;
			}
		}

	$command= "select id, descrip from compramseleccion where estatus=0";
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion_srv, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$selecciones[$i]['id'] =trim($lrow['id']);
			$selecciones[$i]['descrip']=utf8_decode(trim($lrow['descrip']));
			$i++;
		}
	}

$continua=false;
//echo $numeroCC;
if(strlen($numeroCC)>0)
{
	
	//Nombres de proveedores
	$command="select a.cuadroc,a.observa,a.prov1,b.nomprov as nom1,a.prov2,c.nomprov as nom2,a.prov3,d.nomprov as nom3,a.prov4,e.nomprov as nom4 , 
			CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta, seleccion
			from compramcuadroc a 
			left join compramprovs b on a.prov1=b.prov
			left join compramprovs c on a.prov2=c.prov
			left join compramprovs d on a.prov3=d.prov
			left join compramprovs e on a.prov4=e.prov
			where a.cuadroc='$numeroCC' ";//AND a.estatus=0
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$continua=true;
			$nada= " ";
			$prov1= trim($row['nom1']);
			$prov2= trim($row['nom2']);
			$prov3= trim($row['nom3']);
			$prov4= trim($row['nom4']);
			$falta= trim($row['falta']);
			$halta= trim($row['halta']);
			$observacc=utf8_decode(trim($row['observa']));			
			$seleccion= trim($row['seleccion']);
		}
	}
	
	if($continua==true)
	{	
		//Partidas de cuadro comparativo
		$command="select d.id, d.acepta, a.cantidad,b.unidad,d.descrip as descripcion,a.precio1,a.total1,a.check1, 
a.precio2,a.total2,a.check2,a.precio3,a.total3,a.check3, a.precio4,a.total4,a.check4 , 
b.requisi as iddrequisi, f.nomdepto 
from compradcuadroc a 
left join compradrequisi b on a.iddrequisi= b.id 
left join compradproductos c on b.prod=c.prod 
INNER JOIN compradcc_ftecnica d ON d.iddcuadroc=a.id 
left join compramrequisi e on e.requisi= b.requisi 
left JOIN nomemp.[dbo].nominamdepto f ON f.depto COLLATE DATABASE_DEFAULT = e.depto COLLATE DATABASE_DEFAULT 
where a.cuadroc=$numeroCC and d.estatus<9000 ORDER BY b.id ASC";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			$contRequi=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				
				if($i==0)
				{
					$datos[$contRequi]['iddrequisi']=$row['iddrequisi'];
					$datos[$contRequi]['nomdepto']=$row['nomdepto'];
					
					$datos[$contRequi]['productos']=array();
				}
				else{
					if($datos[$contRequi]['iddrequisi']!=$row['iddrequisi'])
					{
						$contRequi++;
						$datos[$contRequi]['iddrequisi']=$row['iddrequisi'];
						$datos[$contRequi]['nomdepto']=$row['nomdepto'];
						$datos[$contRequi]['productos']=array();
					}
				}
				
				$items=array();
				$items['cantidad']= trim($row['cantidad']);
				$items['unidad']= trim($row['unidad']);
				$items['descripcion']= utf8_decode(trim($row['descripcion']));
				
				$items['id']= trim($row['id']);
				$items['cantidad']= trim($row['cantidad']);
				$items['unidad']= trim($row['unidad']);
				$items['descripcion']= utf8_decode(trim($row['descripcion']));
				
				//$datos[$i]['precio1']= number_format(trim($row['precio1']),2);
				if($row['check1']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					$datos2[$i]['aceptada']= $row['acepta'];
					if($row['acepta']==0){
						$noaceptada="";
					}
					else{
						$aceptada="";
					}
					
							   
					$items['total1']= "<input type='radio' name='aceptaProd1_".$i."' id='aceptaProd1_".$i."_A'  value='".$row['id'].",0' $aceptada>";					
					$items['precio1']= "<input type='radio' name='aceptaProd1_".$i."' id='aceptaProd1_".$i."_N' value='".$row['id'].",1' $noaceptada>";//number_format(trim($row['precio1']),2);
				}
				else	
				{
					$items['total1']= "&nbsp;";//$datos[$i]['check1']= trim($row['check1']);
					$items['precio1']= "&nbsp;";
				}
					
				//$datos[$i]['precio2']= number_format(trim($row['precio2']),2);
				if($row['check2']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					$datos2[$i]['aceptada']= $row['acepta'];
					if($row['acepta']==0){
						$noaceptada="";
					}
					else{
						$aceptada="";
					}
						
					$items['total2']= "<input type='radio' name='aceptaProd2_".$i."' id='aceptaProd2_".$i."_A'  value='".$row['id'].",0' $aceptada>";//number_format(trim($row['total2']),2).
					$items['precio2']= "<input type='radio' name='aceptaProd2_".$i."' id='aceptaProd2_".$i."_N' value='".$row['id'].",1' $noaceptada>";//number_format(trim($row['total2']),2).
				}
				else	
				{
					$items['total2']= "&nbsp;";
					$items['precio2']= "&nbsp;";
				}
				//$datos[$i]['check2']= trim($row['check2']);
				
				if($row['check3']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					$datos2[$i]['aceptada']= $row['acepta'];
					if($row['acepta']==0){
						$noaceptada="";
					}
					else{
						$aceptada="";
					}
						
					$items['total3']= "<input type='radio' name='aceptaProd3_".$i."' id='aceptaProd3_".$i."_A' value='".$row['id'].",0' $aceptada>";//number_format(trim($row['total2']),2).//number_format(trim($row['total3']),2).
					$items['precio3']="<input type='radio' name='aceptaProd3_".$i."' id='aceptaProd3_".$i."_N' value='".$row['id'].",1' $noaceptada>";// number_format(trim($row['precio3']),2);
				}
				else	
				{
					
					$items['total3']= "&nbsp;";//number_format(trim($row['total2']),2).//number_format(trim($row['total3']),2).
					$items['precio3']="&nbsp;";// number_format(trim($row['precio3']),2);
				}
				
				if($row['check4']==1)
				{
					$aceptada=" checked ";
					$noaceptada=" checked ";
					$datos2[$i]['aceptada']= $row['acepta'];
					if($row['acepta']==0){
						$noaceptada="";
					}
					else{
						$aceptada="";
					}
					$items['total4']= "<input type='radio' name='aceptaProd4_".$i."'  id='aceptaProd4_".$i."_A' value='".$row['id'].",0' $aceptada>";
					$items['precio4']= "<input type='radio' name='aceptaProd4_".$i."' id='aceptaProd4_".$i."_N' value='".$row['id'].",1' $noaceptada>";
				}
				else	
				{
					$items['total4']= "&nbsp;";
					$items['precio4']= "&nbsp;";
				}
				
				array_push($datos[$contRequi]['productos'],$items);
				$i++;
			}
		}	
	}
}
//print_r($datos);
//echo json_encode($datos2);
$htmls="";
$htmls.='
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Aprobaci&oacute;n d Ficha T&eacute;cnica</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="javascript/detalleAceptarProddCcomparativo.js"></script>
<script language="javascript" src="javascript/comordendcompra.js"></script>
<script>
	var js_data = "<?php echo json_encode($datos2); ?>";
	var js_obj_data = JSON.parse(js_data );
	
	console.log(js_obj_data);
</script>
<style>
table, td{
	 border-collapse: collapse;
}
</style>

</head>

';
for($contRequi=0; $contRequi<count($datos);$contRequi++)
{
$htmls.='<body tyle="page-break-after: always;">

<table width="100%" style="border: 1px black solid;"><tr>
		<td align="center" width="100%" style="border: 1px black solid;">
        	<table width="100%" style="border: 1px black solid;">
				<tr>
					<td align="center">FICHA TECNICA</td>
				</tr>
        		<tr>
					<td align="center" style="margin-left:150px; font-size:9px;">Adjunto al presente sirvase encontrar original de las Propuestas (cotizaciones) con el fin de que se aformulado el Dictamen T&eacute;cnico (An&aacute;lisis de las Propuestas por parte del area solicitante) correspondiente y as&iacute; proseguir con los tramites de la adquisici&oacute;n solicitada.</td>
				</tr>
            </table>
        </td>
	</tr>
</table>
<table width="100%" style="border: 1px black solid;">
				<thead>
				<TR>
					<td colspan="2" width="50%">No. DE REQUISICIÓN: '.$datos[$contRequi]['iddrequisi'].'</td>
					<td colspan="4" width="50%" align="center"><u>NOMBRE DEL PROVEEDOR</u></td>
					</TR>
				</thead>
				<TR>
					<td colspan="2" width="50%">AREA SOLICITANTE: '.$datos[$contRequi]['nomdepto'].'</td>
					<td colspan="1" width="12.5%" class="titulosCabezales" style="border: 1px black solid; font-size:9px;" align="center">'.$prov1.'</td>
					<td colspan="1" width="12.5%" class="titulosCabezales" style="border: 1px black solid; font-size:9px;" align="center">'.$prov2.'</td>
					<td colspan="1" width="12.5%" class="titulosCabezales" style="border: 1px black solid; font-size:9px;" align="center">'.$prov3.'</td>
					<td colspan="1" width="12.5%" class="titulosCabezales" style="border: 1px black solid; font-size:9px;" align="center">'.$prov4.'</td>
				</TR>
			</table>

<table width="100%" style="border: 1px black solid;">
				<thead >
					<tr class="subtituloverde8">
						<th width="5%" align="center" style="border: 1px black solid;font-size:9px;">PARTIDA</th>
						<th width="45%" align="center" style="border: 1px black solid;font-size:9px;">DESCRIPCI&Oacute;N DE LOS MATERIALES</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">No Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">No Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">No Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">Cumple</th>
						<th width="6.25%" style="border: 1px black solid;font-size:9px;">No Cumple</th>
					</tr>
				</thead>
				<tbody class="resultadobusqueda" id="tresultados" style="border: 1px black solid;">';
						if( $datos!=null) 
						for($i=0;$i<count($datos[$contRequi]['productos']);$i++){ 
						
						$htmls.='<tr id="filaOculta" class="d'.($i % 2).'" >
							<td width="5%" style="border: 1px black solid;font-size:9px;" align="center">'.($i+1).'</td>
							<td width="45%" style="border: 1px black solid;font-size:9px;">'.utf8_decode($datos[$contRequi]['productos'][$i]['descripcion']).'</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							<td width="6.25%" style="border: 1px black solid;">&nbsp;</td>
							</tr>';
					 } 
				$htmls.='</tbody>
			</table>

	<table width="100%" style="border-collapse: collapse;">
	
	<tr>
		<td style="border: 1px black solid;">&nbsp;</td>
	</tr>
	<tr>
    	<td style="border: 1px black solid; font-size:9px;">
		IMPORTE DE LA REQUISICION<BR>
        IMPORTE COTIZADO<BR>
        DIFERENCIA
        <td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
    	<td width="100%">
        	<table width="100%">
				<tr>
					<td width="30%">
						<table>
							<tr><td align="center" style="font-size:10px;">FIRMA DE CONFORMIDAD</td></TR>
							<tr><td>&nbsp;</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td><HR></td></tr>
							<tr><td align="center" style="font-size:10px;">AREA SOLICITANTE(NOMBRE Y FIRMA)</td></tr>
						</table>
					</td>
					<td width="30%">&nbsp;</td>
					<td width="30%" style="vertical-align:bottom;text-align:right ; font-size:10px;">MO00809-1</td>
				</tr>
            </table>
        </td>
    <tr>
</table></body>
';
}

$htmls.='</html>';

//echo $htmls;
$dompdf = new DOMPDF();
$dompdf->load_html($htmls);
$dompdf->set_paper('legal', "landscape");
$dompdf->render();
$pdf = $dompdf->output();

$tmpfile = tempnam(".", "dompdf_.pdf");
file_put_contents($tmpfile, $pdf ); // Replace $smarty->fetch()
rename($tmpfile,$tmpfile.".pdf");
header('Content-Type: application/pdf');
header('Content-Disposition: attachment; filename="FichaTecnicaDeCC_$id_ccomp.pdf"');
header("Location: ".basename($tmpfile.".pdf"));

$files = glob('*.tmp.pdf'); // get all file names
foreach($files as $file){ // iterate files
  if(is_file($file) && basename($tmpfile.".pdf")!= basename($file))
    unlink($file); // delete file
}
?>