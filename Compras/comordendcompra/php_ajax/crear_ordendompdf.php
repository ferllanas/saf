<?php
require_once("../../../dompdf/dompdf_config.inc.php");
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

//////////////////////////////////
//Definicin de variables
$autoriza="";
$vobo="";
$orden="";
$forden="";
$horden="";
$prov="";
$ordenprov="";
$nomprov="";
$depto="";
$nomdepto="";
$entregarEn="";
$calle="";
$colonia="";
$ciudad="";
$tel_1="";
$tel_2="";
$tel_3="";
$seleccion="";
$fname="";

$HTMLDocs="";
//Obtiene el id del cuadro comprarativo de donde se generara las ordenes de compra
$idcuadroc="000000".$_REQUEST['id_ccomp']."!!!";
if(isset($_REQUEST['id_ccomp']))
	$idcuadroc=$_REQUEST['id_ccomp'];

$datos=array();
//Arma query para traer los datos de las ordenes de compra que se generaron apartir del cuadro comparativo
$sqlcommand="select distinct(a.orden) ,a.prov,convert(varchar, a.halta, 108) as halta ,a.ordenprov,convert(varchar, a.falta, 103) as falta, b.nomprov , 
					f.depto,  g.nomdepto, f.entregaen, b.calle, b.colonia, b.ciudad, b.tel_1, b.tel_2, b.tel_3, c.seleccion as selprov, h.descrip as seleccion, i.ppresupuestal, j.descrip
				from  compramordenes a
				INNER JOIN compramprovs b ON b.prov=a.prov
				INNER JOIN compramcuadroc c ON a.cuadroc=c.cuadroc
				INNER JOIN compradcuadroc d ON d.cuadroc = a.cuadroc
				INNER JOIN compradrequisi e ON e.id=d.iddrequisi
				INNER JOIN compramrequisi f ON f.requisi=e.requisi
				INNER JOIN nomemp.dbo.nominamdepto g ON g.depto COLLATE DATABASE_DEFAULT=f.depto COLLATE DATABASE_DEFAULT
				left join compramseleccion h ON h.id=c.seleccion 
				left join compramppresupuestal i ON i.id = a.ppresupuestal
				left join compramtipoprecio j ON j.tipopcio=a.tipopcio
				where a.cuadroc=$idcuadroc ";//and a.estatus=0
				
//echo $sqlcommand;
$stmt = sqlsrv_query( $conexion,$sqlcommand);
if ( $stmt === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	 die( $sqlcommand."' ".print_r( sqlsrv_errors(), true));
}
else
{
	$i=0;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	{
	
		$orden= trim($row['orden']);
		$forden= $row['falta'];
		$horden= $row['halta'];
		$prov= trim($row['prov']);
		$ordenprov= trim($row['ordenprov']);
		$nomprov= utf8_decode(trim($row['nomprov']));
		$depto= trim($row['depto']);
		$nomdepto= utf8_decode(trim($row['nomdepto']));
		$entregarEn= utf8_decode(trim($row['entregaen']));
		$calle= utf8_decode(trim($row['calle']));
		$colonia= utf8_decode(trim($row['colonia']));
		$ciudad= utf8_decode(trim($row['ciudad']));
		$tel_1= trim($row['tel_1']);
		$tel_2= trim($row['tel_2']);
		$tel_3= trim($row['tel_3']);
		$seleccion= utf8_decode(trim($row['seleccion']));
		//echo $row['ppresupuestal'];
		$ppresupuestal= trim($row['ppresupuestal']);
		//echo $ppresupuestal;
		$tipoPrecio= utf8_decode(trim($row['descrip']));
		$selprov= trim($row['selprov']);


		list($tiempoentr,$condiciones) = getCondicionesYTiempoProv($prov,$idcuadroc);
		$HTML=armar_htmlstring($orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc,$seleccion, $ppresupuestal, $tipoPrecio, $selprov);	
		$HTMLDocs.=$HTML;
		/*creapdfPaImprimir($docPDF,$orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc,$seleccion, $ppresupuestal, $tipoPrecio, $selprov);
		$docPDF->ezNewPage();*/
		$dompdf = new DOMPDF();
		$dompdf->set_paper('letter', "landscape");
		$dompdf->load_html($HTML);
		
		$dompdf->render();
			  
		$pdf = $dompdf->output(); 
		$dir = '../pdf_files';
		if(!file_exists($dir))
		{
			mkdir($dir, 0777);
		}
		//ile_put_contents("../pdf_files/".$idReq.".pdf", $pdf);
		file_put_contents( $dir.'/OrdCompra_'.$orden.'.pdf', $pdf);//"../pdf_files/".$idReq.".pdf");
		
		$datos[$i]['path']=$dir.'/OrdCompra_'.$orden.'.pdf';
		$datos[$i]['fallo']=false;
		$datos[$i]['id']=$orden;
		
		$i++;
	}
	echo json_encode($datos);
	/*$dompdf = new DOMPDF();
	$dompdf->set_paper('letter', "landscape");
	$dompdf->load_html($HTMLDocs);
	
	$dompdf->render();
		  
	$pdf = $dompdf->output(); 
	$dir = '../pdf_files';
	if(!file_exists($dir))
	{
		mkdir($dir, 0777);
	}

	file_put_contents( $dir.'/OrdenesCompDeCuadroc_'.$idcuadroc.'.pdf', $pdf);

	*/
}
function armar_htmlstring($orden,$forden,$horden,$prov,$ordenprov,
							$nomprov,$depto, $nomdepto,$entregarEn,$tiempoentr,
							$condiciones, $calle, $colonia, $ciudad ,$tel_1,   $tel_2, $tel_3,$forden,$horden,$idcuadroc,$seleccion, $ppresupuestal, $tipoPrecio, $selprov)
{
global $imagenPDFPrincipal;
$datosproveedor= $prov."	".$nomprov;
if(strlen($calle)>0)
	$datosproveedor.="\n".$calle;
if(strlen($colonia)>0)
	$datosproveedor.="\n".$colonia;
if(strlen($colonia)>0)
	$datosproveedor.="\n".$ciudad;
if(strlen($tel_1."".$tel_2."".$tel_3)>0)
	$datosproveedor.="\n".$tel_1.",".$tel_2.",".$tel_3;

$numCalreqiosi = fun_ObtieneNumCalidadReq();

$pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	$textod=utf8_encode("P�gina {PAGE_NUM}");
	$textop=utf8_encode("Orden de Compra");
	$pdf->page_text(25, 580, "$textop '.$orden.'", "", 12, array(0,0,0)); 
	$pdf->page_text(650, 580, "Pagina {PAGE_NUM}", "", 12, array(0,0,0)); 
	
	
 } 
</script> '; //$pdf->page_text(650, 592, "MO.2000.2044.1", "", 12, array(0,0,0));

$html="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
			margin:  0.25in 0.5in 0.5in 0.5in;
		}

		#twmaarco
		{
			border-collapse:collapse;
		}

		#twmaarco th
		{
			border:1pt solid black;
			border-collapse:collapse;
			padding:2px 2px 2px 2px; 
		}
		#twmaarco td
		{
			border:1pt solid black;
			border-collapse:collapse;
			padding:4px 4px 4px 4px; 
		}
		
		#twmaarco td.tdSinborder
		{
			border:0pt;
		}

		#twmaarco tr.d0 td {
			background-color: #FFFFFF;
		}
		#twmaarco tr.d1 td {
			background-color: #EAEAEA;
		}

		td {padding: 0;}
		
		.textobold{
			font-weight:bold;
		}
		
		.texto16b{ 
			font-size:16pt;
			font-weight:bold;
		}
		.texto14{ 
			font-size:14pt;
		}
		.texto14b{ 
			font-size:15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size:12pt;
			font-weight:bold;

		}
		.texto12{ 
			font-size:12pt;
		}
		.texto11{
			font-size:11pt;
		}
		.texto10{
			font-size:10pt;
		}
		.texto6{
			font-size:6pt;
		}
		.texto7{
			font-size:7pt;
		}
		.texto8{
			font-size:8pt;
		}
		</style>

</head>

<body>
<table width='100%'>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='20%'><img src='../../../$imagenPDFPrincipal' style='width:150px; height:57px'></td>
					<td width='60%'>
						<table width='100%'>
							<tr>
								<td align='center' class='texto14b'><b>Fomento Metropolitano de Monterrey</b></td>
							</tr>
							<tr>
								<td align='center' class='texto14b'><b>".utf8_encode("Direcci�n de Administraci�n y Finanzas")."</b></td>
							</tr>
						</table>
					</td>
					<td width='20%' class='texto12' style'valign:top;' align='right'>$numCalreqiosi</td>
				</tr>
				<tr>
					<td align='left' class='texto14' colspan=3>".utf8_encode("Orden de Compra ")." No. $orden</td>
				</tr>
				<tr>
					<td align='right' class='texto10' colspan=3><strong>Fecha:</strong> $forden, $horden</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table  width='100%'>
				<tr>
					<td width='50%'>Proveedor: <b>".utf8_encode($prov)." ".utf8_encode($nomprov)."</b></td>
					<td  width='50%'>Solicitante:<b> ".utf8_encode($depto)." ".utf8_encode($nomdepto)."</b></td>
				</tr>
				<tr>
					<td  width='50%'>".utf8_encode($datosproveedor)."</td>
					<td  width='50%'>Entregar en: ".utf8_encode($entregarEn)."</td>
				</tr>
			</table>
		</td>
	</tr>
<tr height='2'><td height='2'></td></tr>
	<tr>
		<td width='100%'>
			<table width='100%' name='twmaarco' id='twmaarco' class='texto8'>
				<thead>
				<tr>
					<th align='center' width='40'># Req.</th>
					<th align='center' width='40'>Ctapresup</th>
					<th align='center' width='40'>Cantidad</th>
					<th align='center' width='40'>Unidad</th>
					<th align='center' width='300'>".utf8_encode('Descripci�n')."</th>
					<th align='center' width='40'>P.U.</th>
					<th align='center' width='40'>Sub-Total</th>
					<th align='center' width='40'>I.V.A.</th>
					<th align='left' width='40'>Total</th>
				</tr>
				</thead>
				<tr height='1px'>
					<td colspan='9' height='1px'></td>
				</tr>";
			list($partidas, $seleccion1)= func_ObtenerProdDorden($orden,$ordenprov);
			for($i=0; $i<count($partidas);$i++)
			{
				$html.="<tr class='d".($i%2)."'>
					<td align='center' width='40'>".$partidas[$i]['requisi']."</td>
					<td align='center' width='40'>".$partidas[$i]['ctapresup']."</td>
					<td align='center' width='40'>".$partidas[$i]['cantidad']."</td>
					<td align='center' width='40'>".$partidas[$i]['unidad']."</td>";
				if(strlen($partidas[$i]['descprod'])>0)
					$html.="<td width='300' style''>".utf8_encode($partidas[$i]['descprod'])."</td>";
				else
					$html.="<td width='300'>&nbsp;</td>";

					$html.="<td align='center' width='40'>".number_format($partidas[$i]['precio'],2)."</td>
					<td align='center' width='40'>".number_format($partidas[$i]['subtotal'],2)."</td>
					<td align='center' width='40'>".number_format($partidas[$i]['iva'],2)."</td>
					<td align='left' width='40'>".number_format($partidas[$i]['total'],2)."</td>
				</tr>";
			}

			$totalImporte=0.0;
			$totalSubtotal=0.0;
			$totalIva=0.0;
			$totalTotal=0.0;
			for($i=0;$i<count($partidas);$i++)
			{
				$pprecio=$partidas[$i]['precio'];
				$psubtotal=$partidas[$i]['subtotal'];
				$piva=$partidas[$i]['iva'];
				$ptotal=$partidas[$i]['total'];
				$totalImporte += (double)$pprecio;
				$totalSubtotal += (double)$psubtotal;
				$totalIva += (double)$piva;
				$totalTotal += (double)$ptotal;
			}
//echo "<br>".$totalTotal;
$totalTotal=redondear_dos_decimal($totalTotal);
//echo "<br>".$totalTotal." pos=".strpos(".",$totalTotal);
if( fmod($totalTotal, 1.0)==0)
	$totalTotal=$totalTotal.".00";

$TotalCadena =num2letras($totalTotal);
//echo "<br>".$totalTotal;

		$html.="<tr class='tdSinborder'>
					<td align='center' colspan='4' class='tdSinborder'>&nbsp;</td>
					<td align='left' class='tdSinborder'>Total</td>
					<td align='center' class='tdSinborder'>".number_format($totalImporte,2)."</td>
					<td align='center' class='tdSinborder'>".number_format($totalSubtotal,2)."</td>
					<td align='center' class='tdSinborder'>".number_format($totalIva,2)."</td>
					<td align='left' class='tdSinborder'>".number_format($totalTotal,2)."</td>
				</tr>
				<tr class='tdSinborder'>
					<td colspan='8' align='center' class='tdSinborder'>&nbsp;</td>
					<td align='left' class='tdSinborder'>".number_format($totalTotal,2)."</td>
				</tr>
			</table> 
		</td>
	</tr>
	
	<tr>
		<td>*************($TotalCadena)*************</td>
	</tr>
	<tr>
		<td> </td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%' class='texto8'>
				<tr>
					<td width='100' align='center'>Tiempo de entrega</td>
					<td width='100' align='center'>Condiciones</td>
					<td width='100' align='center'>Cuadro comparativo</td>
					<td width='100' align='center'>".utf8_encode("Selecci�n")."</td>
				</tr>
				<tr>
					<td width='100' align='center'>$tiempoentr</td>
					<td width='100' align='center'>$condiciones</td>
					<td width='100' align='center'>$idcuadroc</td>
					<td width='100' align='center'>$seleccion</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width='100%' >
				<tr>
					<td width='20%'><hr></td>
					<td width='5%'>&nbsp;</td>
					<td width='20%'><hr></td>
					<td width='5%'>&nbsp;</td>
					<td width='20%'><hr></td>
					<td width='5%'>&nbsp;</td>
					<td width='20%'><hr></td>
				</tr>
				<tr>
					<td width='20%' align='center'>Nombre, Firma y Fecha de Recibido</td>
					<td width='5%'>&nbsp;</td>
					<td width='20%' align='center'>".utf8_encode("Coordinador de recursos materiales")."</td>
					<td width='5%'>&nbsp;</td>
					<td width='20%' align='center'>".utf8_encode("Direcci�n de Administraci�n")."</td>
					<td width='5%'>&nbsp;</td>
					<td width='20%' align='center'>".utf8_encode("Aplicaci�n Presupuestal")."</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class='texto6'>
			<table name='twmaarco' id='twmaarco'>
				<tr>
					<td>".utf8_encode("ENVIESE LA FACTURA CON ORIGINAL Y COPIA ACOMPA�ADAS DE LA ORDEN CORRESPONDIENTE, HAGASE MENCION DEL NUMERO DE ORDEN DE COMPRA EN REMISIONES, FACTURAS Y PAQUETES AL ENTREGAR LA MERCANCIA RECABESE, FIRMA Y NOMBRE DE QUIEN RECIBE Y SELLO DE ALMACEN.")."
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class='texto6'>- La partida presupuestal es $ppresupuestal.</td>
	</tr>
	<tr>
		<td class='texto6'>- Los precios son $tipoPrecio.</td>
	</tr>
	<tr>
		<td class='texto6'>".
			utf8_encode("- En caso de incurrir en los supuestos que marca el articulo 33 Bis del C�digo Fiscal del Estado de Nuevo Le�n acepta y autoriza a 'FOMERREY' para que proceda a efectuar las retenciones que procedan a efecto de garantizar el pago de las contribuciones omitidas.").
		"</td>
	</tr>";
	if($selprov=="1")
		$html.="<tr><td class='texto6'>".utf8_encode("- Se aplicar� una pena, cuyo porcentaje diario ser� del 1% (uno por ciento), que se multiplicar� por el n�mero de d�as de atraso y el resultado se multiplicar� a su vez, por el valor de los bienes entregados con atraso, aceptando EL PROVEEDOR, de dar lugar a este supuesto, se dduzca del importe de la factura que presente para su cobro, el monto de la penalizaci�n. De acuerdo a la siguiente f�rmula: f�rmula: (pd)x(nda)x(vbepa) = pca donde: pd: (1 %) penalizaci�n diaria nda: n�mero de d�as de atraso vbepa: valor del bien entregado con atraso pca: pena convencional aplicable El monto m�ximo a penalizar ser� del 10% Las penas se har�n efectivas descontandose de los pagos que  LA CONVOCANTE tenga pendientes de efectuar al participante, independiente de que LA CONVOCANTE opte por hacer efectivas las garant�as otorgadas por el participante.").
				"</td></tr>";

		$html.="
	</table>
".$pageheadfoot."
</body>
</html>";

return $html;
}

function getCondicionesYTiempoProv($prov,$idcuadroc)
{
	global $server , $odbc_name , $username_db , $password_db;

	$fails=false;
	
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="SELECT * FROM compramcuadroc WHERE cuadroc=$idcuadroc";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$tiempoentr="";
	$condiciones =  "";
	
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			if($prov==$row['prov1'])
			{
				$tiempoentr=$row['tentrega1'];
				$condiciones =  $row['condiciones1'];
			}
			
			if($prov==$row['prov2'])
			{
				$tiempoentr=$row['tentrega2'];
				$condiciones =  $row['condiciones2'];
			}
			
			if($prov==$row['prov3'])
			{
				$tiempoentr=$row['tentrega3'];
				$condiciones =  $row['condiciones3'];
			}
			
			if($prov==$row['prov4'])
			{
				$tiempoentr=$row['tentrega4'];
				$condiciones =  $row['condiciones4'];
			}
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($tiempoentr, $condiciones );
}

//Funcion que llama procedimiento que obtiene codigo de requisicion
function fun_ObtieneNumCalidadReq()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=11;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}

function func_ObtenerProdDorden($orden,$ordenprov)
{
	global $server,$odbc_name,$username_db ,$password_db;
	
	$datos=array();
	$seleccion="";
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$command = "select b.requisi,a.iddrequisi,b.prod,a.observa as descprod, c.nomprod as seg,
				a.cantidad,b.unidad,a.ivapct,a.precio".$ordenprov.",a.subtotal".$ordenprov." , a.ctapresup, a.observa
				from compradcuadroc a 
				left join compradrequisi b on a.iddrequisi=b.id
				left join compradproductos c on b.prod=c.prod
				left join compramordenes f on a.cuadroc=f.cuadroc 
				where orden=$orden and check".$ordenprov."=1";//left join compramseleccion g ON g.id=a.seleccion , g.descrip as seleccion
				//echo $command;
	//echo "<br><br>".$command."<br><br>";
	$stmt = sqlsrv_query( $conexion,$command);
	if ( $stmt === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($stmt);
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			
			$datos[$i]['requisi']= trim($row['requisi']);
			$datos[$i]['ctapresup']= substr( trim($row['ctapresup']),0,3);		
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['descprod']= utf8_decode(trim($row['descprod']));//trim($row['descprod']).",".
			
			$nomprod = trim($row['seg']);
			
			
			$pos= strrpos($datos[$i]['descprod'],$nomprod);
			if($pos === false)
			{
				$datos[$i]['descprod']=$nomprod."".$datos[$i]['descprod'];
			}
			
			$datos[$i]['precio']= trim($row['precio'.$ordenprov]);
			$datos[$i]['subtotal']= trim($row['subtotal'.$ordenprov]);
			$datos[$i]['iva'] = ($datos[$i]['subtotal']*$row['ivapct'])/100;
			$datos[$i]['total']=$datos[$i]['subtotal']+$datos[$i]['iva'];			
			//datos[$i]['observa']= trim($row['observa']);
			$seleccion= "";//trim($row['seleccion']);
			$i++;
		}
	}
	sqlsrv_close( $conexion);	
	return array($datos, $seleccion);
}
?>
