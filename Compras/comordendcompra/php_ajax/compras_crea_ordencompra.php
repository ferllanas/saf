<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	$id="";
	//$id_req
	$usuario = "";//Variable id del usuario
	if(isset($_GET['usuario']));
		$usuario=$_GET['usuario'];
			
	$id_cuadroc="";//
	if(isset($_GET['cuadroc']));
		$id_cuadroc=$_GET['cuadroc'];

	$tipopcio="0";//
	if(isset($_GET['tipopcio']));
		$tipopcio=$_GET['tipopcio'];

	$parpresup="0";//
	if(isset($_GET['parpresup']));
		$parpresup=$_GET['parpresup'];

	$fecha="";
	$fails=false;
	$hora="";
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	list($error,$id_cuadroc)= fun_creaOrdCompra($usuario,$id_cuadroc, $tipopcio,$parpresup); //crea nueva requisicion
		
	$datos=array();//prepara el aray que regresara
	$datos[0]['id']=trim($id_cuadroc);// regresa el id de la
	$datos[0]['fallo']=$error;
	
	
	echo json_encode($datos);//Esta instruccion regresa el array en forma de que el javascript pueda interpretarlo como un array
	

/////
//funcion que registra una nueva requisicion
function fun_creaOrdCompra($usuario,$id_cuadroc, $tipopcio,$parpresup)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails = false;
	$error = false;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	///primer Proveedor
	$sqlcommmand="select a.cuadroc,sum(a.sdototal) as total,b.prov1,1 as orden from compradcuadroc a 
					left join compramcuadroc b on a.cuadroc=b.cuadroc 
					where a.check1=1 and a.cuadroc=$id_cuadroc group by a.cuadroc,b.prov1";
	$stmt = sqlsrv_query($conexion,$sqlcommmand);
	if( $stmt === false )
	{
		 $fails=true;
	}	
	if(!$fails)
	{		 
		//echo "primer ".count($stmt);
		if(count($stmt)>0)
		{
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$prov = $row['prov1'];	
				$orden = $row['orden'];
				$total = $row['total'];
				$cuadroc= $row['cuadroc'];
				$error=fun_ins_ordencompra($prov,$orden,$total,$usuario,$cuadroc,$tipopcio,$parpresup);
				if($error)
					return array($error,$id_cuadroc);								
			}
			sqlsrv_free_stmt( $stmt);		
			
		}
	}
	
	///Segundo Proveedor
	$sqlcommmand="select a.cuadroc,sum(a.sdototal) as total,b.prov2,2 as orden from compradcuadroc a 
					left join compramcuadroc b on a.cuadroc=b.cuadroc 
					where a.check2=1 and a.cuadroc=$id_cuadroc group by a.cuadroc,b.prov2";
	$stmt = sqlsrv_query($conexion,$sqlcommmand);
	if( $stmt === false )
	{
		 $fails=true;
		 //die( print_r( sqlsrv_errors(), true));
	}	
	if(!$fails)
	{		 
		if(count($stmt)>0)
		{
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$prov = $row['prov2'];	
				$orden = $row['orden'];
				$total = $row['total'];
				$cuadroc= $row['cuadroc'];
				$error=fun_ins_ordencompra($prov,$orden,$total,$usuario,$cuadroc,$tipopcio,$parpresup);
				if($error)
					return array($error,$id_cuadroc);								
			}
			sqlsrv_free_stmt( $stmt);		
		}
	}
	
	/// Tercer Proveedor
	$sqlcommmand="select a.cuadroc,sum(a.sdototal) as total,b.prov3,3 as orden from compradcuadroc a 
					left join compramcuadroc b on a.cuadroc=b.cuadroc 
					where a.check3=1 and a.cuadroc=$id_cuadroc group by a.cuadroc,b.prov3";
	$stmt = sqlsrv_query($conexion,$sqlcommmand);
	if( $stmt === false )
	{
		 $fails=true;
		 //die( print_r( sqlsrv_errors(), true));
	}	
	if(!$fails)
	{		 
		if(count($stmt)>0)
		{
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$prov = $row['prov3'];	
				$orden = $row['orden'];
				$total = $row['total'];
				$cuadroc= $row['cuadroc'];								
				$error=fun_ins_ordencompra($prov,$orden,$total,$usuario,$cuadroc,$tipopcio,$parpresup);
				// fun_ins_ordencompra($prov,$orden,$total,$usuario,$cuadroc)
				if($error)
					return array($error,$id_cuadroc);
			}
			sqlsrv_free_stmt( $stmt);		
		}
	}
	
	///Cuarto Proveedor
	$sqlcommmand="select a.cuadroc,sum(a.sdototal) as total,b.prov4,4 as orden from compradcuadroc a 
					left join compramcuadroc b on a.cuadroc=b.cuadroc 
					where a.check4=1 and a.cuadroc=$id_cuadroc group by a.cuadroc,b.prov4";
	$stmt = sqlsrv_query($conexion,$sqlcommmand);
	if( $stmt === false )
	{
		 $fails=true;
		 //die( print_r( sqlsrv_errors(), true));
	}	
	if(!$fails)
	{		 
		if(count($stmt)>0)
		{
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
			{
				$prov = $row['prov4'];	
				$orden = $row['orden'];
				$total = $row['total'];
				$cuadroc= $row['cuadroc'];				
				$error=fun_ins_ordencompra($prov,$orden,$total,$usuario,$cuadroc,$tipopcio,$parpresup);
				if($error)
					return array($error,$id_cuadroc);				
			}
			sqlsrv_free_stmt( $stmt);		
		}
	}	
	return array($error,$id_cuadroc);
}

//Funcion que ineserta la orden de compra
function fun_ins_ordencompra($prov,$orden,$total,$usuario,$cuadroc,$tipopcio,$parpresup)
{

	global $server,$odbc_name,$username_db ,$password_db;

	$error=false;
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_A_mordenes2(?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado

	$params = array(&$prov,&$orden,&$total,&$usuario,&$cuadroc,&$tipopcio,&$parpresup);	
//print_r($params);
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $error=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	sqlsrv_close($conexion);
	return $error;
}
?>