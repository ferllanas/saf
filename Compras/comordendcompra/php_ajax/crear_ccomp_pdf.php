<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<?php
include('../../../pdf/class.ezpdf.php');
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
//$usarioCreador = $_COOKIE['ID_my_site'];	


if(isset($_GET['id']))
	$idccomp=$_GET['id'];
	
$totalprov1=0.00;
$totcompprov1=0.00;
$totalprov2=0.00;
$totcompprov2=0.00;
$totalprov3=0.00;
$totcompprov3=0.00;
$totalprov4=0.00;
$totcompprov4=0.00;
$observa="";
$falta="";
$halta="";
$datos=array();


//////////////////////////////////////////////////////////////////////////////////////
$command="select a.cuadroc,a.observa,a.prov1,b.nomprov as nom1,a.prov2,c.nomprov as nom2,a.prov3,d.nomprov as nom3,a.prov4,e.nomprov as nom4 , 
			CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta
			from compramcuadroc a 
			left join compramprovs b on a.prov1=b.prov
			left join compramprovs c on a.prov2=c.prov
			left join compramprovs d on a.prov3=d.prov
			left join compramprovs e on a.prov4=e.prov
			where a.cuadroc='$idccomp'";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['nada']= " ";
			$datos[$i]['nom1']= trim($row['nom1']);
			$datos[$i]['nom2']= trim($row['nom2']);
			$datos[$i]['nom3']= trim($row['nom3']);
			$datos[$i]['nom4']= trim($row['nom4']);
			$falta= trim($row['falta']);
			$halta= trim($row['halta']);
			$observa=utf8_decode(trim($row['observa']));			
			$i++;
		}
	}
	//Declaracion General
	$doc =& new Cezpdf('LATTER','landscape'); //Declaracion de tipo de Hoja, Orientacion	
	$doc->selectFont('../../../fonts/arial.afm'); //Tipo de Fuente
	$doc->ezSetCmMargins(0.01,0.5,0.5,0.5); //Margenes
	$datacreator = array ('Title'=>'Creacion de PDF de la notificacion','Author'=>'Fomerrey','Subject'=>'Creacion de PDF de la notificacion','Creator'=>'fernando.llanas@fomerrey.gob.mx','Producer'=>'http://blog.unijimpe.net');
	$doc->addInfo($datacreator);
	
	
	$doc->addJpegFromFile('../../../$imagenPDFPrincipal', 20, 530, 148.5, 57, array('gap'));
	ob_end_clean(); //opcional
	$doc->setColor(0, 0 ,0);
	$doc->ezText("Fomento Metropolitano de Monterrey",18,array('justification'=>'center'));
	$doc->ezText("Direcci�n de Administraci�n y Finanzas",14,array('justification'=>'center'));
	$doc->ezText("Cuadro Comparativo No. $idccomp",16,array('justification'=>'left'));
	$doc->ezText("<b>Fecha:</b> ".$falta."  <b>Hora:</b>" .$halta ,10,array('justification'=>'right'));
	//$doc->ezText("\n\n" ,10);
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	//imprime Proveedores de Cuadro Comparativo.
	
	$doc->ezText("" ,8);//presupcuentasmayor, //where estatus<9000
	
	////creamos un nuevo array en el que pondremos un borde=1
	///y las cabeceras de la tabla las pondremos ocultas
	unset ($opciones_tabla);
	
	//// mostrar las lineas
	$opciones_tabla['showlines']=1;
	
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=0;
	
	//// lineas sombreadas
	$opciones_tabla['shaded']= 1;
	
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 8;
	
	//// color del texto
	//$opcio
	 //// color del texto
	$opciones_tabla['textCol'] = array(0,0,0);
	
	//// tama�o de las cabeceras (texto)
	$opciones_tabla['titleFontSize'] = 7;
	
	//// margen interno de las celdas
	$opciones_tabla['rowGap'] = 3;
	$opciones_tabla['colGap'] = 3;
	$opciones_tabla['cols'] = array( 'nada' => array('justification' => 'center', 'width'=>'320' ), 
									 'nom1' => array('justification' => 'center', 'width'=>'120' ), 
									 'nom2' => array('justification' => 'center', 'width'=>'120' ), 
									 'nom3' => array('justification' => 'center', 'width'=>'120' ), 
									 'nom4' => array('justification' => 'center', 'width'=>'120' ) );
	
	
	$titles = array('nada' =>'','nom1'=>'<b>Cantidad</b>', 'nom2'=>'<b>Unidad</b>', 'nom3'=>'<b>Descripci�n</b>', 'nom4'=>'<b>Cuenta</b>');
	$doc->ezTable($datos,$titles,"",$opciones_tabla);  

	
	////creamos un nuevo array en el que pondremos un borde=1
	///y las cabeceras de la tabla las pondremos ocultas
	unset ($opciones_tabla);
	
	//// mostrar las lineas
	$opciones_tabla['showlines']=1;
	
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=1;
	
	//// lineas sombreadas
	$opciones_tabla['shaded']= 1;
	
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 10;
	
	//// color del texto
	//$opcio
	 //// color del texto
	$opciones_tabla['textCol'] = array(0,0,0);
	
	//// tama�o de las cabeceras (texto)
	$opciones_tabla['titleFontSize'] = 6;
	
	//// margen interno de las celdas
	$opciones_tabla['rowGap'] = 3;
	$opciones_tabla['colGap'] = 3;
	//// Tama�o de columnas
	
	$opciones_tabla['cols'] = array( 'cantidad' => array('justification' => 'center', 'width'=>'60' ), 
									"unidad" => array('justification' => 'center', 'width'=>'60'), 
									"descripcion" => array('justification' => 'left', 'width'=>'200'),
									"precio1" => array('justification' => 'center', 'width'=>'60' ), 
									"total1" => array('justification' => 'center', 'width'=>'60'),									
									"precio2" => array('justification' => 'center', 'width'=>'60' ), 
									"total2" => array('justification' => 'center', 'width'=>'60'), 
									"precio3" => array('justification' => 'center', 'width'=>'60' ), 
									"total3" => array('justification' => 'center', 'width'=>'60'), 
									"precio4" => array('justification' => 'center', 'width'=>'60' ), 
									"total4" => array('justification' => 'center', 'width'=>'60'));
	//imprime Detalle de Cuadro Comparativo.
	$command="select a.cantidad,b.unidad,a.observa as descripcion,a.precio1,a.total1,a.check1,
		a.precio2,a.total2,a.check2,a.precio3,a.total3,a.check3,
		a.precio4,a.total4,a.check4 from compradcuadroc a 
		left join compradrequisi b on a.iddrequisi= b.id 
		left join compradproductos c on b.prod=c.prod where a.cuadroc=$idccomp";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['descripcion']= utf8_decode(trim($row['descripcion']));
			
			$datos[$i]['precio1']= number_format(trim($row['precio1']),2);
			if($row['check1']==1)
				$datos[$i]['total1']= number_format(trim($row['total1']),2)." X";
			else	
				$datos[$i]['total1']= number_format(trim($row['total1']),2);//$datos[$i]['check1']= trim($row['check1']);
			$datos[$i]['precio2']= number_format(trim($row['precio2']),2);
			//$datos[$i]['total2']= trim($row['total2']);
			if($row['check2']==1)
				$datos[$i]['total2']= number_format(trim($row['total2']),2)." X";
			else	
				$datos[$i]['total2']=number_format( trim($row['total2']),2);
			//$datos[$i]['check2']= trim($row['check2']);
			$datos[$i]['precio3']= number_format(trim($row['precio3']),2);
			if($row['check3']==1)
				$datos[$i]['total3']= number_format(trim($row['total3']),2)." X";
			else	
				$datos[$i]['total3']= number_format(trim($row['total3']),2);
			//$datos[$i]['check3']= trim($row['check3']);
			$datos[$i]['precio4']= number_format(trim($row['precio4']),2);
			if($row['check4']==1)
				$datos[$i]['total4']= number_format(trim($row['total4']),2)." X";
			else	
				$datos[$i]['total4']= number_format(trim($row['total4']),2);
			//$datos[$i]['check4']= trim($row['check4']);
			
			
			$i++;
		}
	}	
	///$opciones_tabla['xOrientation']='center';
	//// Titulos de TAbla								
	$titles = array('cantidad'=>'<b>Cantidad</b>', 'unidad'=>'<b>Unidad</b>', 'descripcion'=>'<b>Descripci�n</b>',
					 'precio1'=>'<b>P.U.</b>','total1'=>'<b>Importe</b>', 
					 'precio2'=>'<b>P.U.</b>','total2'=>'<b>Importe</b>', 
					 'precio3'=>'<b>P.U.</b>','total3'=>'<b>Importe</b>',
					 'precio4'=>'<b>P.U.</b>','total4'=>'<b>Importe</b>', );
	$doc->ezTable($datos,$titles,"",$opciones_tabla);  
//// para Precios de Proveedor
	
	//imprime datos extra Cuadro Comparativo.
	
	////creamos un nuevo array en el que pondremos un borde=1
	///y las cabeceras de la tabla las pondremos ocultas
	unset ($opciones_tabla);
	
	//// mostrar las lineas
	$opciones_tabla['showlines']=1;
	
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=0;
	
	//// lineas sombreadas
	$opciones_tabla['shaded']= 0;
	
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 10;
	
	//// color del texto
	//$opcio
	 //// color del texto
	$opciones_tabla['textCol'] = array(0,0,0);
	
	//// tama�o de las cabeceras (texto)
	$opciones_tabla['titleFontSize'] = 7;
	
	//// margen interno de las celdas
	$opciones_tabla['rowGap'] = 3;
	$opciones_tabla['colGap'] = 3;
	$opciones_tabla['cols'] = array( 'nada' => array('justification' => 'right', 'width'=>'320' ), 
									 'dato1' => array('justification' => 'center', 'width'=>'120' ), 
									 'dato2' => array('justification' => 'center', 'width'=>'120' ), 
									 'dato3' => array('justification' => 'center', 'width'=>'120' ), 
									 'dato4' => array('justification' => 'center', 'width'=>'120' ) );
	
	$titles = array('nada' =>'','dato1'=>'<b>tentrega1</b>', 'dato2'=>'<b>Unidad</b>', 'dato3'=>'<b>Descripci�n</b>', 'dato4'=>'<b>Cuenta</b>');
	$command="select SUM(total1) as dato1,SUM(total2) as dato2,SUM(total3) as dato3,SUM(total4) as dato4 from compradcuadroc where cuadroc=$idccomp group by cuadroc ";
	//echo $command;
	$getProducts = sqlsrv_query($conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$datos=array();		
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[0]['nada']= "Total Cotizado";
			$datos[0]['dato1']= number_format(trim($row['dato1']),2);
			$datos[0]['dato2']= number_format(trim($row['dato2']),2);
			$datos[0]['dato3']= number_format(trim($row['dato3']),2);
			$datos[0]['dato4']= number_format(trim($row['dato4']),2);			
			
		}
	}
	$command="
select sum(case when  check1=1	then total1 else 0 end) as dato1,
		sum(case when  check2=1	then total2 else 0 end) as dato2,
		sum(case when  check3=1	then total3 else 0 end) as dato3,
		sum(case when  check4=1	then total4 else 0 end) as dato4 from compradcuadroc where cuadroc=$idccomp group by cuadroc ";
	//echo $command;
	$getProducts = sqlsrv_query($conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		//$datos=array();		
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[1]['nada']= "Total a Ordenar";
			$datos[1]['dato1']= trim($row['dato1']);
			$datos[1]['dato2']= trim($row['dato2']);
			$datos[1]['dato3']= trim($row['dato3']);
			$datos[1]['dato4']= trim($row['dato4']);			
			
		}
	}
	$doc->ezTable($datos,$titles,"",$opciones_tabla);
	
	unset ($opciones_tabla);
	
	//// mostrar las lineas
	$opciones_tabla['showlines']=1;
	
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=0;
	
	//// lineas sombreadas
	$opciones_tabla['shaded']= 0;
	
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 10;
	
	//// color del texto
	//$opcio
	 //// color del texto
	$opciones_tabla['textCol'] = array(0,0,0);
	
	//// tama�o de las cabeceras (texto)
	$opciones_tabla['titleFontSize'] = 7;
	
	//// margen interno de las celdas
	$opciones_tabla['rowGap'] = 3;
	$opciones_tabla['colGap'] = 3;
	$opciones_tabla['cols'] = array( 'nada' => array('justification' => 'right', 'width'=>'320' ), 
									 'dato1' => array('justification' => 'left', 'width'=>'120' ), 
									 'dato2' => array('justification' => 'left', 'width'=>'120' ), 
									 'dato3' => array('justification' => 'left', 'width'=>'120' ), 
									 'dato4' => array('justification' => 'left', 'width'=>'120' ) );
	$datos=array();
	$titles = array('nada' =>'','dato1'=>'<b>tentrega1</b>', 'dato2'=>'<b>Unidad</b>', 'dato3'=>'<b>Descripci�n</b>', 'dato4'=>'<b>Cuenta</b>');
	
	$command="select tentrega1,tentrega2,tentrega3,tentrega4 from compramcuadroc where cuadroc='$idccomp'";
	//echo $command;
	$getProducts = sqlsrv_query($conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
				
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[1]['nada']= "Tiempo de Entrega";
			$datos[1]['dato1']= trim($row['tentrega1']);
			$datos[1]['dato2']= trim($row['tentrega2']);
			$datos[1]['dato3']= trim($row['tentrega3']);
			$datos[1]['dato4']= trim($row['tentrega4']);			
			
		}
	}
	
	$command="select condiciones1,condiciones2,condiciones3,condiciones4 from compramcuadroc where cuadroc='$idccomp'";
	//echo $command;
	$getProducts = sqlsrv_query($conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[2]['nada']= "Condiciones";
			$datos[2]['dato1']= trim($row['condiciones1']);
			$datos[2]['dato2']= trim($row['condiciones2']);
			$datos[2]['dato3']= trim($row['condiciones3']);
			$datos[2]['dato4']= trim($row['condiciones4']);			
			
		}
	}
	//print_r($datos);
	//$doc->ezText("" ,8);//presupcuentasmayor, //where estatus<9000
	
	$doc->ezTable($datos,$titles,"",$opciones_tabla);  
	
	$doc->ezText("\n<b>Observaciones: <b>$observa",12,array('justification'=>'left'));
	
	$numCalCuadro=fun_ObtieneNumCalidadCuadro();
	$doc->ezText("\n\n<b>$numCalCuadro</b>" ,12,array('justification'=>'right'));

	$pdfcode = $doc->output();
	$dir = '../pdf_files';
	if(!file_exists($dir))
	{
		mkdir($dir, 0777);
	}
	
	$fname = $dir.'/cuadroc_'.$idccomp.'.pdf';//tempnam(,'PDF_').//
	$fp = fopen($fname, 'w');
	fwrite($fp,$pdfcode);
	fclose($fp);
	$doc->ezStream();

function fun_ObtieneNumCalidadCuadro()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=12;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}


?>
<body>
</body>
</html>
