<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
$observa ="";
$prov1 = "";
$prov2 ="";
$prov3 ="";
$prov4 ="";
$tentrega1 ="";
$tentrega2 ="";
$tentrega3 ="";
$tentrega4 ="";
$condiciones1 ="";
$condiciones2 ="";
$condiciones3 ="";
$condiciones4 ="";
$nomprov1="";
$nomprov2="";
$nomprov3="";
$nomprov4="";
$depto ="";
$coord ="";
$dir ="";
$seleccion="";
$nomdepto ="";
$nomdir ="";
$totalProv1=0.0;
$totalProv2=0.0;
$totalProv3=0.0;
$totalProv4=0.0;
$productos = array();
$selecciones= array();
$usuario = $_COOKIE['ID_my_site'];
$str_productos="";//variable que almacena la cadena de los productos

$numreq="";
if(isset($_REQUEST['txtSearch']))
	$numreq=$_REQUEST['txtSearch'];

$selectCotiza=array();
if(isset($_REQUEST['selectCotiza']))
	$selectCotiza=$_REQUEST['selectCotiza'];

$provs=array();
if(isset($_REQUEST['prov']))
	$provs=$_REQUEST['prov'];

$cotizas=array();
if(isset($_REQUEST['cotiza']))
	$cotizas=$_REQUEST['cotiza'];
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($conexion)
{
	//Busqueda de datos del departamento
	$command= "select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto COLLATE DATABASE_DEFAULT where numemp='$usuario'";
	$stmt2 = sqlsrv_query( $conexion, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$depto =trim($lrow['depto']);
			$coord =trim($lrow['coord']);
			$dir =trim($lrow['dir']);
			$nomdepto =utf8_decode(trim($lrow['nomdepto']));
			$nomdir =utf8_decode(trim($lrow['nomdir']));
			$i++;
		}
	}
	///Obtiene proveedores
	
	$command= "select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto COLLATE DATABASE_DEFAULT where numemp='$usuario'";
	$stmt2 = sqlsrv_query( $conexion, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$depto =trim($lrow['depto']);
			$coord =trim($lrow['coord']);
			$dir =trim($lrow['dir']);
			$nomdepto =utf8_decode(trim($lrow['nomdepto']));
			$nomdir =utf8_decode(trim($lrow['nomdir']));
			$i++;
		}
	}

	///Obtienelas selecciones
	$command= "select id, descrip from compramseleccion where estatus=0";
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$selecciones[$i]['id'] =trim($lrow['id']);
			$selecciones[$i]['descrip']=utf8_decode(trim($lrow['descrip']));
			
			if($seleccion=="")
			{
				if($selecciones[$i]['descrip']=="Adjudicacion Directa")
				{
					$seleccion = $selecciones[$i]['id'];
				}
			}
			$i++;
		}
	}
	
	if(isset($_GET['id_cuadroc']) )
	{
		if(strlen($_GET['id_cuadroc']))
		{	
			$id_cuadroc=$_GET['id_cuadroc'];
			$command= "select * from compramcuadroc WHERE cuadroc=".$id_cuadroc;
			$stmt2 = sqlsrv_query( $conexion, $command);
			if( $stmt2 === false)
			{
				echo "Error in executing statement 3.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}
			
			if(sqlsrv_has_rows($stmt2))
			{
				$i=0;
				while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
				{
					$observa =utf8_decode(trim($lrow['observa']));
					$prov1 =trim($lrow['prov1']);
					$prov2 =trim($lrow['prov2']);
					$prov3 =trim($lrow['prov3']);
					$prov4 =trim($lrow['prov4']);
					$tentrega1 =trim($lrow['tentrega1']);
					$tentrega2 =trim($lrow['tentrega2']);
					$tentrega3 =trim($lrow['tentrega3']);
					$tentrega4 =trim($lrow['tentrega4']);
					$condiciones1 =trim($lrow['condiciones1']);
					$condiciones2 =trim($lrow['condiciones2']);
					$condiciones3 =trim($lrow['condiciones3']);
					$condiciones4 =trim($lrow['condiciones4']);
					$seleccion =trim($lrow['seleccion']);
					$i++;
				}
			}
			
			///Obtiene el nombre del proveedor1
			$command= "select nomprov from compramprovs WHERE prov=".$prov1;
			$stmt2 = sqlsrv_query( $conexion, $command);
			if( $stmt2 === false)
			{
				echo "Error in executing statement 3.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}
			if(sqlsrv_has_rows($stmt2))
			{
				$i=0;
				while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
				{
					$nomprov1 =trim($lrow['nomprov']);
				}
			}
			///Obtiene el nombre del proveedor2
			$command= "select nomprov from compramprovs WHERE prov=".$prov2;
			$stmt2 = sqlsrv_query( $conexion, $command);
			if( $stmt2 === false)
			{
				echo "Error in executing statement 3.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}
			if(sqlsrv_has_rows($stmt2))
			{
				$i=0;
				while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
				{
					$nomprov2 =trim($lrow['nomprov']);
				}
			}

			///Obtiene el nombre del proveedor3
			$command= "select nomprov from compramprovs WHERE prov=".$prov3;
			$stmt2 = sqlsrv_query( $conexion, $command);
			if( $stmt2 === false)
			{
				echo "Error in executing statement 3.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}
			if(sqlsrv_has_rows($stmt2))
			{
				$i=0;
				while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
				{
					$nomprov3 =trim($lrow['nomprov']);
				}
			}
		
			

			///Obtiene el nombre del proveedor4
			$command= "select nomprov from compramprovs WHERE prov=".$prov4;
			$stmt2 = sqlsrv_query( $conexion, $command);
			if( $stmt2 === false)
			{
				echo "Error in executing statement 3.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}
			if(sqlsrv_has_rows($stmt2))
			{
				$i=0;
				while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
				{
					$nomprov4 =trim($lrow['nomprov']);
				}
			}

			///
			if(isset($_GET['id_cuadroc']))
			{
			
				$command= "SELECT a.id      ,a.cuadroc
							  ,a.iddrequisi
							  ,a.cantidad
							  ,a.precio1
							  ,a.check1
							  ,a.subtotal1
							  ,a.precio2
							  ,a.check2
							  ,a.subtotal2
							  ,a.precio3
							  ,a.check3
							  ,a.subtotal3
							  ,a.precio4
							  ,a.check4
							  ,a.subtotal4
							  --,a.ctapresup
							  ,a.sdocant
							  --,a.sdototal
							  ,b.prod
							  ,b.descrip
							  ,c.nomprod
							  ,c.unidad
								, a.observa
  							FROM compradcuadroc a LEFT JOIN compradrequisi b  ON a.iddrequisi=b.id LEFT JOIN compradproductos c ON c.prod=b.prod WHERE  a.cuadroc=".$id_cuadroc;
				$stmt2 = sqlsrv_query( $conexion, $command);
				if( $stmt2 === false)
				{
					echo "Error in executing statement 3.\n";
					print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
				}
				
				if(sqlsrv_has_rows($stmt2))
				{
					$i=0;
					while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
					{
						$productos[$i]['iddrequisi'] =trim($lrow['iddrequisi']);
						$productos[$i]['cantidad'] =trim($lrow['cantidad']);
						$productos[$i]['sdocant'] =trim($lrow['sdocant']);

						$productos[$i]['prod'] =trim($lrow['prod']);
						$productos[$i]['descrip'] =trim($lrow['descrip']);
						$productos[$i]['observa'] =trim($lrow['observa']);
						$productos[$i]['nomprod'] =trim($lrow['nomprod']);
						$productos[$i]['unidad'] =trim($lrow['unidad']);

						$productos[$i]['precio1'] =trim($lrow['precio1']);
						$productos[$i]['precio2'] =trim($lrow['precio2']);
						$productos[$i]['precio3'] =trim($lrow['precio3']);
						$productos[$i]['precio4'] =trim($lrow['precio4']);

						$productos[$i]['subtotal1'] =trim($lrow['subtotal1']);
						$productos[$i]['subtotal2'] =trim($lrow['subtotal2']);
						$productos[$i]['subtotal3'] =trim($lrow['subtotal3']);
						$productos[$i]['subtotal4'] =trim($lrow['subtotal4']);
						
						//$productos[$i]['total1'] =trim($lrow['total1']);
						//$productos[$i]['total2'] =trim($lrow['total2']);
						//$productos[$i]['total3'] =trim($lrow['total3']);
						//$productos[$i]['total4'] =trim($lrow['total4']);

						$productos[$i]['check1'] =trim($lrow['check1']);
						$productos[$i]['check2'] =trim($lrow['check2']);
						$productos[$i]['check3'] =trim($lrow['check3']);
						$productos[$i]['check4'] =trim($lrow['check4']);
						$i++;
					}
				}
			}	
			
		}
	}
	else
	{
		//echo "MAMA";
		if(isset($_REQUEST['txtSearch']))
		{
			

			$command= "SELECT a.id, a.prod, a.descrip, a.cantidad, a.unidad, a.saldo, a.requisi, b.nomprod 
			FROM compradrequisi a 
			INNER JOIN compradproductos b on a.prod=b.prod WHERE a.requisi='$numreq' ORDER BY a.id ASC";
	
			
			$getProducts = sqlsrv_query( $conexion_srv,$command);
			if ( $getProducts === false)
			{ 
				$resoponsecode="02";
				die($command."". print_r( sqlsrv_errors(), true));
			}
			else
			{
				$resoponsecode = "Cantidad rows=".count($getProducts);
				$i=0;
				while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
				{
					$productos[$i]['iddrequisi']= trim($row['id']);
					$productos[$i]['prod']= htmlentities( utf8_decode( trim( $row['prod'] ) ) );
					$productos[$i]['descrip']= htmlentities( utf8_decode( trim( $row['descrip'] ) ) );
					$productos[$i]['observa']= htmlentities( utf8_decode( trim( $row['nomprod'] ) ) ).", ".$productos[$i]['descrip'];
				
					$productos[$i]['cantidad']= trim($row['saldo']);//Cantidad
					$productos[$i]['sdocant'] = $productos[$i]['cantidad'];
					$productos[$i]['unidad']= trim($row['unidad']);
					$productos[$i]['saldo']= trim($row['saldo']);
					$productos[$i]['requisi']= trim($row['requisi']);
					$productos[$i]['nomprod']= htmlentities( trim($row['nomprod']) );
				
					$provs="";
					for($j=0;$j<count($selectCotiza);$j++)
					{
						list($idprov, $idcotiza)= explode(";", $selectCotiza[$j]);
						$cmd2="SELECT a.*, c.nomprov FROM compradcotizaprov a 
								INNER JOIN compradcotiza b ON b.id=a.iddcotiza
								INNER JOIN compramprovs c ON c.prov=a.prov
								WHERE a.prov=$idprov AND a.cotiza=$idcotiza AND b.iddrequisi=".$productos[$i]['iddrequisi'];
						//echo $cmd2; 
						$cotizacionesdprov = sqlsrv_query( $conexion_srv,$cmd2);
						if ( $cotizacionesdprov === false)
						{ 
							echo $cmd2;
							die($command."". print_r( sqlsrv_errors(), true));
						}
						else
						{
							while( $row2 = sqlsrv_fetch_array( $cotizacionesdprov, SQLSRV_FETCH_ASSOC))
							{
								
								$productos[$i]['prov'.($j+1)]= htmlentities( utf8_decode( trim( $row2['prov'] ) ) );
								$productos[$i]['nomprov'.($j+1)]= htmlentities( utf8_decode( trim( $row2['nomprov'] ) ) );
								$productos[$i]['precio'.($j+1)]= htmlentities( utf8_decode( trim( $row2['precio'] ) ) );
								$productos[$i]['cantprov'.($j+1)]= htmlentities( utf8_decode( trim( $row2['cantprov'] ) ) );
								$productos[$i]['subtotal'.($j+1)]= htmlentities( utf8_decode( trim( $row2['subtotal'] ) ) );
								$productos[$i]['obsprov'.($j+1)]= htmlentities( utf8_decode( trim( $row2['obsprov'] ) ) );
								switch('nomprov'.($j+1))
								{
									case "nomprov1":
										$nomprov1= $productos[$i]['nomprov'.($j+1)];
										$prov1=$productos[$i]['prov'.($j+1)];
									break;
									case "nomprov2":
										$nomprov2= $productos[$i]['nomprov'.($j+1)];
										$prov2=$productos[$i]['prov'.($j+1)];
									break;
									case "nomprov3":
										$nomprov3= $productos[$i]['nomprov'.($j+1)];
										$prov3=$productos[$i]['prov'.($j+1)];
									break;
									case "nomprov4":
										$nomprov4= $productos[$i]['nomprov'.($j+1)];
										$prov4=$productos[$i]['prov'.($j+1)];
									break;
								}
							}
						}
					}
					$i++;
				}
			}
		}
	}
}
?>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Cuadro Comparativo</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>

<script language="javascript" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script language="javascript" src="../../prototype/jQuery.js"></script>

<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
<script language="javascript" src="javascript/comordendcompra.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/busquedaincProv2.js"></script>
<script language="javascript" src="javascript/busquedaincProv3.js"></script>
<script language="javascript" src="javascript/busquedaincProv4.js"></script>
</head>
<body style="z-index:1; ">
<form id="form_Ccompara"  name ="form_Ccompara" style="width:100%"  metdod="post"  action="" >
<table style="width:100%">
	<tr>
	<td colspan="4">
		<table width="100%">
			<tr>
				<td align="center" width="100%" class="TituloDForma">Cuadro Comparativo<hr class="hrTitForma"></td>
			</tr>
		</table> 									
	</td>
	</tr>
	<tr>
	 	<td class="texto8" width="100%" colspan="4">	
			<table width="100%">
				<tr>
				<td class="texto8" width="50%"><b>Departamento:</b><?php echo $nomdepto;?><input type="hidden"  style="width:50%" id="pswd"  name="pswd" value="<?php echo $nomdepto;?>" class="caja_toprint" tabindex="-1" readonly="true"></td>
				<td class="texto8" width="50%">Seleccion: <select id="seleccion" name="seleccion" style="width:300px; " >
					<?php //<?php echo count($selecciones);
						
						for($i=0;$i<count($selecciones); $i++)
						{
							if($seleccion==$selecciones[$i]['id'])
								echo "<option value='".$selecciones[$i]['id']."' title='".$selecciones[$i]['descrip']."' SELECTED>".$selecciones[$i]['descrip']."</option>";
							else	
								echo "<option value='".$selecciones[$i]['id']."' title='".$selecciones[$i]['descrip']."'>".$selecciones[$i]['descrip']."</option>";
						}
					?>
					</select><input type="hidden" style="width:90% " id="usuario"  name="usuario"  value="<?php echo $usuario;?>" class="required">
				</td>
				</tr>
			</table>
		</td>	
	</tr>
	<tr>
		<td class="texto8" width="100%">
			<table width="100%">
				<tr>
					<td width="10%"><b>Observaciones: </b></td>
					<td width="90%"><input type="text" size="60"  id="observa"  name="observa" class="text8" tabindex="1" value="<?php echo $observa;?>" style="width:100%;" maxlength="300"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="texto8"><b>Selecci&oacute;n de Proveedores:</b></td>
	</tr>
	<tr>
		<td colspan="4">
			<table width="100%">
				<tr>
					<td width="25%" align="center" class="texto8">
						Proveedor1:
						  <div align="left" style="z-index:1; position:absolute; width:25%;">     
										<input class="texto8" type="text" id="condicion1" name="condicion1" style="width:100%;"  value="<?php if(strlen($condiciones1)>0) echo $condiciones1; else echo '30 DIAS'; ?>" autocomplete="off" tabindex="2">									
					  </div>				  </td>
					<td width="25%" align="center" class="texto8">
						Proveedor2:
						<div align="left" style="z-index:1;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="condicion2" name="condicion2" style="width:100%;"  value="<?php if(strlen($condiciones2)>0) echo $condiciones2; else echo '30 DIAS'; ?>" autocomplete="off" tabindex="5">										
						</div>  
					</td>
					<td width="25%" align="center" class="texto8">
						Proveedor3:
						<div align="left" style="z-index:1;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="condicion3" name="condicion3" style="width:100%;"  value="<?php if(strlen($condiciones3)>0) echo $condiciones3; else echo '30 DIAS'; ?>" autocomplete="off" tabindex="8">										
						</div>  
					</td>
					<td width="25%" align="center" class="texto8">
						Proveedor4:
						<div align="left" style="z-index:1;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="condicion4" name="condicion4" style="width:100%;"  value="<?php if(strlen($condiciones4)>0) echo $condiciones4; else echo '30 DIAS'; ?>" autocomplete="off" tabindex="11">										
						</div>  
					</td>
				</tr>
				<tr>
					<td width="25%" align="center" class="texto8">
						Tiempo de Entrega:
						  <div align="left" style="z-index:1; position:absolute; width:25%;">     
                            <input class="texto8" type="text" id="tentrega1" name="tentrega1" style="width:100%;"  value="<?php if(strlen($tentrega1)>0) echo $tentrega1; else echo 'INMEDIATA'; ?>" autocomplete="off" tabindex="3">
</div>				  </td>
					<td width="25%" align="center" class="texto8">
						Tiempo de Entrega:
						<div align="left" style="z-index:1;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="tentrega2" name="tentrega2" style="width:100%;"  value="<?php if(strlen($tentrega2)>0) echo $tentrega2; else echo 'INMEDIATA'; ?>" autocomplete="off" tabindex="6">										
						</div>  
					</td>
					<td width="25%" align="center" class="texto8">
						Tiempo de Entrega:
						<div align="left" style="z-index:1;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="tentrega3" name="tentrega3" style="width:100%;"  value="<?php if(strlen($tentrega3)>0) echo $tentrega3; else echo 'INMEDIATA'; ?>" autocomplete="off" tabindex="9">										
						</div>  
					</td>
					<td width="25%" align="center" class="texto8">
						Tiempo de Entrega:
						<div align="left" style="z-index:1;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="tentrega4" name="tentrega4" style="width:100%;"  value="<?php if(strlen($tentrega4)>0) echo $tentrega4; else echo 'INMEDIATA'; ?>" autocomplete="off" tabindex="12">										
						</div>  
					</td>
				</tr>
				<tr>				
					<td width="25%" align="center" class="texto8">
						Proveedor 1:<div align="left" style="z-index:3; position:absolute; width:25%;">     
										<input class="texto8" type="text" id="provname1" name="provname1" style="width:100%;"  onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="4" value="<?php if(strlen($nomprov1)>=0) echo $nomprov1; else echo 'Proveedor 1';?>">
										<input type="hidden" id="provid1" name="provid1" value ="<?php if(strlen($prov1)>0) echo $prov1; else echo '0'; ?>">
										<div id="search_suggestProv" style="z-index:4;" > </div>
									</div>  
					</td>
					<td width="25%" align="center" class="texto8">
						Proveedor 2:
						<div align="left" style="z-index:3;  position:absolute; width:25%;">     
										<input class="texto8" type="text" id="provname2" name="provname2" style="width:100%;"  onKeyUp="searchProveedor2(this);" autocomplete="off" tabindex="7" value="<?php if(strlen($nomprov2)>=0) echo $nomprov2; else echo 'Proveedor 2';?>">
										<input type="hidden" id="provid2" name="provid2" value ="<?php if(strlen($prov2)>0) echo $prov2; else echo '0'; ?>">
							<div id="search_suggestProv2" style="z-index:4;" > </div>
						</div>  
					</td>
					<td width="25%" align="center" class="texto8">
					Proveedor 3:
						<div align="left" style="z-index:3;position:absolute; width:25%">     
										<input class="texto8" type="text" id="provname3" name="provname3" style="width:100%;"  onKeyUp="searchProveedor3(this);" autocomplete="off" tabindex="10" value="<?php if(strlen($nomprov3)>=0) echo $nomprov3; else echo 'Proveedor 3';?>">
										<input type="hidden" id="provid3" name="provid3"  value ="<?php if(strlen($prov3)>0) echo $prov3; else echo '0'; ?>">
							<div id="search_suggestProv3" style="z-index:4;" > </div>
						</div>  
					</td>
					<td width="25%" align="center" class="texto8">
					Proveedor 4:
						<div align="left" style="z-index:3; position:absolute; width:25%">     
										<input class="texto8" type="text" id="provname4" name="provname4" style="width:100%;"  onKeyUp="searchProveedor4(this);" autocomplete="off" tabindex="13" value="<?php if(strlen($nomprov4)>=0) echo $nomprov4; else echo 'Proveedor 4';?>">
										<input type="hidden" id="provid4" name="provid4" value ="<?php if(strlen($prov4)>0) echo $prov4; else echo '0'; ?>">
							<div id="search_suggestProv4" style="z-index:4;" > </div>
						</div>  
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center">
			&nbsp;
			<div align="left" style="z-index:1; position:absolute; left:0px">       
							<a class="texto8">Numero de Requisici&oacute;n: </a>
							<input name="txtSearch" type="text" id="txtSearch" size="10" width="50px" onKeyUp="searchSuggest();" autocomplete="off"/ tabindex="14">  
							<input type="hidden" id="busProd" name="busProd" >
							<input type="hidden" id="unidaddPro" name="unidaddPro" >
							<input class="texto8" type="button" value="Agregar Requisici&oacute;n" id="ad_colin2" name="ad_colin2" onClick="agrega_ProductosDreqAOrdeNCom();" tabindex="15" >  
				<br />
				<div id="search_suggest" style="z-index:2;" > </div>
			</div>    
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><input type="hidden" id="id_ccomp" name="id_ccomp" value=""></td>
	</tr>
</table>
<table border="0" id="tmenudusuario" name="tmenudusuario" width="100%" >
<tr>
	<th class="subtituloverde8" width="10%">Observaciones</th>
	<!--<th class="subtituloverde8" width="5%">Unidad</th>
	<th class="subtituloverde8" width="25%">Descripcion</th>-->
	<th class="subtituloverde8" width="5%">Cantidad a Ordenar</th>
	<th class="subtituloverde8" width="10%">
		<table width="100%">
			<tr>
				<td colspan="2" width="100%">
					<input type="text" class="caja_toprint_blanco" id="nomproveedor1" name="nomproveedor1" VALUE="<?php if(strlen($nomprov1)>=0) echo $nomprov1; else echo 'Proveedor 1';?>" style="width:100%;" tabindex="-1">
				</td>
			</tr>
			<tr>
				<td width="40%"  align="center">
					Prec Uni.
				</td >
				<td width="40%" align="center">
					Total</td>
			</tr>
		</table>
	</th>
	<th class="subtituloverde8" width="10%">
		<table width="100%">
			<tr>
				<td colspan="2" width="100%">
					<input type="text" class="caja_toprint_blanco" id="nomproveedor2" name="nomproveedor2" VALUE="<?php if(strlen($nomprov2)>=0) echo $nomprov2; else echo 'Proveedor 2';?>" style="width:100%;" tabindex="-1">
				</td>
			</tr>
			<tr>
				<td width="40%"  align="center">
					Prec Uni.
				</td >
				<td width="40%"  align="center">
					Total</td>
			</tr>
		</table>
	</th>
	<th class="subtituloverde8" width="10%">
	<table width="100%">
			<tr>
				<td colspan="2" width="100%">
					<input type="text" class="caja_toprint_blanco" id="nomproveedor3" name="nomproveedor3" VALUE="<?php if(strlen($nomprov3)>=0) echo $nomprov3; else echo 'Proveedor 3';?>" style="width:100%;" tabindex="-1">
				</td>
			</tr>
			<tr>
				<td width="40%"  align="center">
					Prec Uni.
				</td >
				<td width="40%"  align="center">
					Total</td>
			</tr>
		</table>
	</th>
	<th class="subtituloverde8" width="10%">
		<table width="100%">
			<tr>
				<td colspan="2" width="100%">
					<input type="text" class="caja_toprint_blanco" id="nomproveedor4" name="nomproveedor4" VALUE="<?php if(strlen($nomprov4)>=0) echo $nomprov4; else echo 'Proveedor 4';?>" style="width:100%;" tabindex="-1">
				</td>
			</tr>
			<tr>
				<td width="40%">
					Prec Uni.
				</td >
				<td width="40%">
					Total</td>
			</tr>
		
		</table>
	</th>
	<th class="subtituloverde8" width="10%">Producto</th>
</tr>
<tbody id="menus" name="menus" class="resultadobusqueda">
	<?php  
		if( $productos!=null) 
		for($i=0;$i<count($productos);$i++){ 
	?>
		<tr id="filaOculta" class="d<?php echo ($i % 2);?>">
		<td width="10%" class="texto8">
			 <input type="hidden" id="idprodEnRequi<?php echo $i; ?>" name="idprodEnRequi<?php echo $i; ?>"  value="<?php echo $productos[$i]['iddrequisi']; ?>">
			 <input type="hidden" id="idprod<?php echo $i; ?>" name="idprod<?php echo $i; ?>"  value="<?php echo $productos[$i]['prod'] ; ?>">
             <input type="text" class="texto8" id="observa<?php echo $i; ?>2" name="observa<?php echo $i; ?>" width="100%" style="width:100%" value="<?php
				$descprod="";
				//if(strlen($productos[$i]['nomprod'])>0)
					//$descprod.=$productos[$i]['nomprod'];

				if(strlen($productos[$i]['observa'])>0)
					$descprod.= utf8_decode($productos[$i]['observa']);

				//if(strlen($productos[$i]['unidad'])>0)
					//$descprod.=$productos[$i]['descrip'];
				
				echo $descprod;
			?>"
			tabindex="<?php echo $i+14;?>">
</td>
		<td width="5%"><input type="text" id="cantidad<?php echo $i; ?>" name="cantidad<?php echo $i; ?>"  value="<?php if(isset($productos[$i]['sdocant'])) echo $productos[$i]['sdocant'];/*.",". $i+count($productos)*/;?>" style="width:90% " onKeyUp="javascript:calcularSubtotalEnRenglon(<?php echo $i;?>);" tabindex="<?php echo 14+$i+(count($productos)*2);?>" ></td>
		<td width="10%">
						<input type="text" id="presUniprovA<?php echo $i;?>" name="presUniprovA<?php echo $i;?>" style="width:35% " 
							onKeyUp="javascript:calcularTotal(this,event,'cantidad<?php echo $i;?>','cantxPresUniA<?php echo $i;?>');" 
							value="<?php if(isset($productos[$i]['precio1']))echo $productos[$i]['precio1'];else echo 0.0;?>" 
							onKeyPress="javascript:return aceptarSoloNumeros(this, event)"
							tabindex="<?php echo 14+$i+(count($productos)*3);?>">
						<input type="text" id="cantxPresUniA<?php echo $i;?>" name="cantxPresUniA<?php echo $i;?>" style="width:35% " readonly value="<?php if(isset($productos[$i]['subtotal1'])){echo $productos[$i]['subtotal1'];$totalProv1+=$productos[$i]['subtotal1'];}else echo 0.0;  ?>" disabled tabindex="-1"><input type="checkbox" id="checkSelectPropuestaA<?php echo $i;?>" name="checkSelectPropuestaA<?php echo $i;?>" onChange="javascript:CambiarAllRow(this);" <?php if(isset($productos[$i]['check1']) && $productos[$i]['check1']==1) echo "checked";?> tabindex="-1">
		</td>
		<td width="10%">
						<input type="text" id="presUniprovB<?php echo $i;?>" name="presUniprovB<?php echo $i;?>" style="width:35% " 
								onKeyUp="javascript:calcularTotal(this,event,'cantidad<?php echo $i;?>','cantxPresUniB<?php echo $i;?>');" 
								value="<?php if(isset($productos[$i]['precio2']))echo $productos[$i]['precio2'];else echo 0.0;?>" 
								tabindex="<?php echo 14+$i+(count($productos)*4);?>"
								onKeyPress="javascript:return aceptarSoloNumeros(this, event)">
						<input type="text" id="cantxPresUniB<?php echo $i;?>" name="cantxPresUniB<?php echo $i;?>" style="width:35% " readonly value="<?php if(isset($productos[$i]['subtotal2'])){echo $productos[$i]['subtotal2'];$totalProv2+=$productos[$i]['subtotal2'];}else echo 0.0;?>" disabled tabindex="-1"><input type="checkbox" id="checkSelectPropuestaB<?php echo $i;?>" name="checkSelectPropuestaB<?php echo $i;?>" onChange="javascript:CambiarAllRow(this);" <?php if(isset($productos[$i]['check2']) && $productos[$i]['check2']==1) echo "checked";?> tabindex="-1">
		</td>
		<td width="10%"><input type="text" id="presUniprovC<?php echo $i;?>" name="presUniprovC<?php echo $i;?>" style="width:35% " 
								onKeyUp="javascript:calcularTotal(this,event,'cantidad<?php echo $i;?>','cantxPresUniC<?php echo $i;?>');" 
								value="<?php if(isset($productos[$i]['precio3']))echo $productos[$i]['precio3'];else echo 0.0;?>" 
								tabindex="<?php echo 14+$i+(count($productos)*5);?>"
								onKeyPress="javascript:return aceptarSoloNumeros(this, event)">
						<input type="text" id="cantxPresUniC<?php echo $i;?>" name="cantxPresUniC<?php echo $i;?>" style="width:35% " readonly value="<?php if(isset($productos[$i]['subtotal3'])){echo $productos[$i]['subtotal3'];$totalProv3+=$productos[$i]['subtotal3'];}else echo 0.0;?>" disabled tabindex="-1"><input type="checkbox" id="checkSelectPropuestaC<?php echo $i;?>" name="checkSelectPropuestaC<?php echo $i;?>" onChange="javascript:CambiarAllRow(this);" <?php if(isset($productos[$i]['check3']) && $productos[$i]['check3']==1) echo "checked";?> tabindex="-1">
		</td>
		<td width="10%"><input type="text" id="presUniprovD<?php echo $i;?>" name="presUniprovD<?php echo $i;?>" style="width:35% " 
								onKeyUp="javascript:calcularTotal(this,event,'cantidad<?php echo $i;?>','cantxPresUniD<?php echo $i;?>');" 
								value="<?php if(isset($productos[$i]['precio4']))echo $productos[$i]['precio4'];else echo 0.0;?>" 
								tabindex="<?php echo 14+$i+(count($productos)*6);?>"
								onKeyPress="javascript:return aceptarSoloNumeros(this, event)">
						<input type="text" id="cantxPresUniD<?php echo $i;?>" name="cantxPresUniD<?php echo $i;?>" style="width:35% " readonly value="<?php if(isset($productos[$i]['subtotal4'])) {echo $productos[$i]['subtotal4'];$totalProv4+=$productos[$i]['subtotal4'];}else echo 0.0;?>" disabled tabindex="-1"><input type="checkbox" id="checkSelectPropuestaD<?php echo $i;?>" name="checkSelectPropuestaD<?php echo $i;?>" onChange="javascript:CambiarAllRow(this);" <?php if(isset($productos[$i]['check4']) && $productos[$i]['check4']==1) echo "checked";?> tabindex="-1">
		</td>
		<td class="texto8"><?php echo htmlentities($productos[$i]['nomprod']).",". utf8_decode($productos[$i]['descrip'])	;?>
		</td>
		</tr>
	<?php } ?>
	</tbody >
	<tbody id="TotalesS" name="TotalesS" >
		<th width="10%">&nbsp;</th>
		<!--<th width="5%">&nbsp;</th>
		<th width="25%">&nbsp;</th>-->
		<th width="5%">&nbsp;</th>
		<th width="10%" align="right"><input type="100%" id="totalprov1" name="totalprov1" readonly tabindex="-1" value="<?php echo $totalProv1;?>"></th>
		<th width="10%" align="right"><input type="100%" id="totalprov2" name="totalprov2" readonly tabindex="-1" value="<?php echo $totalProv2;?>"></th>
		<th width="10%" align="right"><input type="100%" id="totalprov3" name="totalprov3" readonly tabindex="-1" value="<?php echo $totalProv3;?>"></th>
		<th width="10%" align="right"><input type="100%" id="totalprov4" name="totalprov4" readonly tabindex="-1" value="<?php echo $totalProv4;?>"></th>
	</tbody>
</table>

<table id="busqueda_ef" width="800pt">
	<!--<tr>
		<td colspan="4">
			<table border="0"  width="100%">
				<tr>
					<th width="15%">&nbsp;</th>
					<th width="5%">&nbsp;</th>
					<th width="15%">&nbsp;</th>
					<th width="5%">&nbsp;</th>
					<th width="10%"></td>
					<td width="10%"><input type="100%" id="totalprov1" name="totalprov1" readonly></td>
					<td width="10%"><input type="100%" id="totalprov2" name="totalprov2" readonly></td>
					<td width="10%"><input type="100%" id="totalprov3" name="totalprov3" readonly></td>
					<td width="10%"><input type="100%" id="totalprov4" name="totalprov4" readonly></td>
					<td width="6%">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>-->
	<tr>
		<td align="left" width="50%"><input type="button"  value="Seleccionar mejor proveedor" onClick="SeleccionarMejorProveedor()" class="caja_entrada" tabindex="500">
		<input type="button"  value="Seleccionar mejor precio" onClick="SeleccionarMejorPrecio()" class="caja_entrada" tabindex="501"></td>
		<td align="left" width="50%"><input type="button"  value="Guardar Cuadro Comparativo" onClick="guardarcuadrocomparativo()" class="caja_entrada" tabindex="502">
		<!--<input type="button"  value="Generar Orden de Compra" id="GenOrComp" name="GenOrComp" onClick="generaordendcompra()" class="caja_entrada" disabled tabindex="503">-->
		</td>
	</tr>
</table>
</form>

</body>
</html>