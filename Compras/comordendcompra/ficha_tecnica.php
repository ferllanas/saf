<?php
include('../../Administracion/globalfuncions.php');
require_once("../../connections/dbconexion.php");

$usuario = $_COOKIE['ID_my_site'];

$partspresup=array();
$tipopcios=array();
$selecciones=array();
$datos=array();
$datosTot=array();
$parpresup="";
$tipopcio="";
$seleccion="";

$numeroCC="";
if(isset($_REQUEST['id_ccomp']))
	$numeroCC=$_REQUEST['id_ccomp'];

$nada= "";
$prov1= "";
$prov2= "";
$prov3= "";
$prov4= "";
$falta= "";
$halta= "";
$observacc="";	
$resp=0;
$continua=false;
//echo $numeroCC;
if(strlen($numeroCC)>0)
{
	
	//Nombres de proveedores
	$command="select a.cuadroc,a.observa,a.prov1,b.nomprov as nom1,a.prov2,c.nomprov as nom2,a.prov3,d.nomprov as nom3,a.prov4,e.nomprov as nom4 , 
			CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta, seleccion
			from compramcuadroc a 
			left join compramprovs b on a.prov1=b.prov
			left join compramprovs c on a.prov2=c.prov
			left join compramprovs d on a.prov3=d.prov
			left join compramprovs e on a.prov4=e.prov
			where a.cuadroc='$numeroCC' ";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$continua=true;
			$nada= " ";
			$prov1= trim($row['nom1']);
			$prov2= trim($row['nom2']);
			$prov3= trim($row['nom3']);
			$prov4= trim($row['nom4']);
			$falta= trim($row['falta']);
			$halta= trim($row['halta']);
			$observacc=utf8_decode(trim($row['observa']));			
			$seleccion= trim($row['seleccion']);			
		}
	}
	
	
	if($continua==true)
	{	
		$i=0;
		$consulta="select * from compradcc_ftecnica where cuadroc=$numeroCC and estatus<9000";
		$getProducts = sqlsrv_query( $conexion_srv,$consulta);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			die($command."". print_r( sqlsrv_errors(), true));
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			
			
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$i++;
			}
			$resp=$i;
		}
		//echo $resp."ME LLEVA";
		if ($resp>0)
		{
			$command="select a.cantidad,b.unidad,a.id,d.descrip as descripcion,a.check1,a.check2,a.check3,a.check4 
			from compradcuadroc a left join compradrequisi b on a.iddrequisi= b.id 
			left join compradproductos c on b.prod=c.prod 
			left join compradcc_ftecnica d on a.id=d.iddcuadroc where a.cuadroc=$numeroCC and d.estatus<9000 order by a.id";
		}
		else
		{
		//Partidas de cuadro comparativo
			$command="select a.cantidad,b.unidad,a.id,a.observa as descripcion,a.check1,
			a.check2,a.check3,a.check4 from compradcuadroc a 
			left join compradrequisi b on a.iddrequisi= b.id 
			left join compradproductos c on b.prod=c.prod where a.cuadroc=$numeroCC ";
		}	
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['cantidad']= trim($row['cantidad']);
				$datos[$i]['unidad']= trim($row['unidad']);
				$datos[$i]['id']= trim($row['id']);
				$datos[$i]['descripcion']= utf8_decode(trim($row['descripcion']));
				if($row['check1']==1)
					$datos[$i]['total1']="*";
				else	
					$datos[$i]['total1']= "";//$datos[$i]['check1']= trim($row['check1']);
				if($row['check2']==1)
					$datos[$i]['total2']= "*";
				else	
					$datos[$i]['total2']= "";
				if($row['check3']==1)
					$datos[$i]['total3']= "*";
				else	
					$datos[$i]['total3']= "";
				if($row['check4']==1)
					$datos[$i]['total4']= "*";
				else	
					$datos[$i]['total4']= "";

				$i++;
			}
		}	
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Orden de Compra</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="javascript/busquedaincfichatecnica.js"></script>
<script language="javascript" src="javascript/comordendcompra.js"></script>
<script>
	$( document ).ready(function() {
// Handler for .ready() called.
<?php if ($resp>0) echo "alert('Ya existe cuadro comparativo en ficha tecnica.')";?>
});
</script>
<style type="text/css">
<!--
.Estilo1 {font-size: 36px}
-->
</style>
</head>

<body>
<form id="ficha_tecnica"  style="width:100%;z-index:1;" action="ficha_tecnica.php">
<table width="100%">
	<tr>
		<td align="center" width="100%" class="TituloDForma">Ficha Tecnica
		  <hr class="hrTitForma"></td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr><td width="111" class="texto8">Cuadro Comparativo:</td><td width="1104"><div align="left" style="z-index:6; position:relative;  width: 147px;">       
										<input name="id_ccomp" type="text" id="id_ccomp" size="50px" width="100%" onKeyUp="searchCuadroComparativo();" value="<?php echo $numeroCC;?>" autocomplete="off"/ tabindex="14" onFocus="javascript:$('optnumCuadro').checked=true">  
										<div id="search_suggestCC" style="z-index:5; position:absolute;" > </div>
					  				</div> <input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario;?>"></td>
				</tr>
				<tr><td width="111" class="texto8">&nbsp;</td>
					<td><span class="texto9">
					</span></td>
				</tr>
				<tr>
					<td class="texto8" width="111" >&nbsp;</td>
				  <td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<thead>
					<tr class="subtituloverde8">
						<td width="2%">Cantidad</td>
						<td width="2%">Unidad</td>
						<td width="15%">Descripción</td>
						<td width="2%">
							<table width="100%"><tr><td width="100%"><?php echo $prov1;?></td></tr>
							</table>
						</td>
						<td width="2%">
							<table width="100%"><tr><td width="100%"><?php echo $prov2;?></td></tr>
							</table>
						</td>
						<td width="2%">
							<table width="100%"><tr><td width="100%"><?php echo $prov3;?></td></tr>
							</table>
						</td>
						<td width="2%">
							<table width="100%"><tr><td width="100%"><?php echo $prov4;?></td></tr>
							</table>
						</td>
					</tr>
				</thead>
				<tbody name="datos" id="datos" class="resultadobusqueda">
					<?php  
						if( $datos!=null) 
						for($i=0;$i<count($datos);$i++){ 
					?>
						<tr id="filaOculta" class="d<?php echo ($i % 2);?>">
							<td><?php echo $datos[$i]['cantidad'];?></td>
							<td><?php echo $datos[$i]['unidad'];?></td>
							<td>
								<input name="id_descri" id="id_descri" type="hidden" value="<?php echo $datos[$i]['id'];?>">
								<input class="texto9" type="text" name="descri" id="descri" size="80px" value="<?php echo $datos[$i]['descripcion'];?>">
							</td>
						
						  <td align="center" class="Estilo1"><?php echo $datos[$i]['total1'];?></td>
						  <td align="center" class="Estilo1"><?php echo $datos[$i]['total2'];?></td>
						  <td align="center" class="Estilo1"><?php echo $datos[$i]['total3'];?></td>
						  <td align="center" class="Estilo1"><?php echo $datos[$i]['total4'];?></td>
				  </tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<?php  
						if( $datos!=null) 
						for($i=0;$i<count($datosTot);$i++){ 
					?>
					<tr>
						<td colspan="3" align="right" class="texto10">&nbsp;</td>
						<td colspan="1" align="right" class="texto10">&nbsp;</td>
						<td colspan="1" align="right" class="texto10">&nbsp;</td>
						<td colspan="1" align="right" class="texto10">&nbsp;</td>
						<td colspan="1" align="right" class="texto10">&nbsp;</td>
					</tr>
					<?php } ?>
				</tfoot>
			</table>
		</td>
	</tr>
	<tr>
	  <td align="center">
          <input type="button" id="enviar" name="enviar" onClick="grabar_ficha_tecnica()" value="Guardar"  class="caja_entrada" tabindex="503">
          <input type="button" id="enviar2" name="enviar2" onClick="" value="_______________"  class="caja_entrada" tabindex="503" style="visibility:hidden ">
          <input type="button" id="enviar22" name="enviar22" onClick="recargar()" value="Cancelar"  class="caja_entrada" tabindex="503">
	  </td>
	</tr>
</table>
</form>
</body>
</html>
