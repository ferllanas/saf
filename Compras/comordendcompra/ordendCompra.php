<?php
include('../../Administracion/globalfuncions.php');
require_once("../../connections/dbconexion.php");

$usuario = $_COOKIE['ID_my_site'];;

$partspresup=array();
$tipopcios=array();
$selecciones=array();
$datos=array();
$datosTot=array();
$parpresup="";
$tipopcio="";
$seleccion="";

$numeroCC="";
if(isset($_REQUEST['id_ccomp']))
	$numeroCC=$_REQUEST['id_ccomp'];

$nada= "";
$prov1= "";
$prov2= "";
$prov3= "";
$prov4= "";
$falta= "";
$halta= "";
$observacc="";	


	$command="select id, ppresupuestal, descrip FROM compramppresupuestal where estatus=0";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$partspresup[$i]['id']= trim($row['id']);
				$partspresup[$i]['ppresupuestal']= utf8_decode(trim($row['ppresupuestal']));
				$partspresup[$i]['descrip']= utf8_decode(trim($row['descrip']));
				$i++;
			}
		}

	$command="select tipopcio,  descrip FROM compramtipoprecio where estatus=0";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$tipopcios[$i]['tipopcio']= trim($row['tipopcio']);
				$tipopcios[$i]['descrip']= utf8_decode(trim($row['descrip']));
				
				if($tipopcio=="")
				{
					if($tipopcios[$i]['descrip']=="Fijos")
					{
						$tipopcio = $tipopcios[$i]['tipopcio'];
					}
				}
				$i++;
			}
		}

	$command= "select id, descrip from compramseleccion where estatus=0";
	//echo $command;
	$stmt2 = sqlsrv_query( $conexion_srv, $command);
	if( $stmt2 === false)
	{
		echo "Error in executing statement 3.\n";
		print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
	}
	if(sqlsrv_has_rows($stmt2))
	{
		$i=0;
		while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
		{
			$selecciones[$i]['id'] =trim($lrow['id']);
			$selecciones[$i]['descrip']=utf8_decode(trim($lrow['descrip']));
			$i++;
		}
	}

$continua=false;
//echo $numeroCC;
if(strlen($numeroCC)>0)
{
	
	//Nombres de proveedores
	$command="select a.cuadroc,a.observa,a.prov1,b.nomprov as nom1,a.prov2,c.nomprov as nom2,a.prov3,d.nomprov as nom3,a.prov4,e.nomprov as nom4 , 
			CONVERT(varchar(12),a.falta,103)as falta,CONVERT(varchar(15),a.halta,108) as halta, seleccion
			from compramcuadroc a 
			left join compramprovs b on a.prov1=b.prov
			left join compramprovs c on a.prov2=c.prov
			left join compramprovs d on a.prov3=d.prov
			left join compramprovs e on a.prov4=e.prov
			where a.cuadroc='$numeroCC' AND a.estatus=0";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$continua=true;
			$nada= " ";
			$prov1= trim($row['nom1']);
			$prov2= trim($row['nom2']);
			$prov3= trim($row['nom3']);
			$prov4= trim($row['nom4']);
			$falta= trim($row['falta']);
			$halta= trim($row['halta']);
			$observacc=utf8_decode(trim($row['observa']));			
			$seleccion= trim($row['seleccion']);
		}
	}
	
	if($continua==true)
	{	
		//Partidas de cuadro comparativo
		$command="select a.cantidad,b.unidad,a.observa as descripcion,a.precio1,a.total1,a.check1,
			a.precio2,a.total2,a.check2,a.precio3,a.total3,a.check3,
			a.precio4,a.total4,a.check4 from compradcuadroc a 
			left join compradrequisi b on a.iddrequisi= b.id 
			left join compradproductos c on b.prod=c.prod where a.cuadroc=$numeroCC ";
		//echo $command;
		$getProducts = sqlsrv_query( $conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['cantidad']= trim($row['cantidad']);
				$datos[$i]['unidad']= trim($row['unidad']);
				$datos[$i]['descripcion']= utf8_decode(trim($row['descripcion']));
				
				$datos[$i]['precio1']= number_format(trim($row['precio1']),2);
				if($row['check1']==1)
					$datos[$i]['total1']= number_format(trim($row['total1']),2)." X";
				else	
					$datos[$i]['total1']= number_format(trim($row['total1']),2);//$datos[$i]['check1']= trim($row['check1']);
				$datos[$i]['precio2']= number_format(trim($row['precio2']),2);
				//$datos[$i]['total2']= trim($row['total2']);
				if($row['check2']==1)
					$datos[$i]['total2']= number_format(trim($row['total2']),2)." X";
				else	
					$datos[$i]['total2']=number_format( trim($row['total2']),2);
				//$datos[$i]['check2']= trim($row['check2']);
				$datos[$i]['precio3']= number_format(trim($row['precio3']),2);
				if($row['check3']==1)
					$datos[$i]['total3']= number_format(trim($row['total3']),2)." X";
				else	
					$datos[$i]['total3']= number_format(trim($row['total3']),2);
				//$datos[$i]['check3']= trim($row['check3']);
				$datos[$i]['precio4']= number_format(trim($row['precio4']),2);
				if($row['check4']==1)
					$datos[$i]['total4']= number_format(trim($row['total4']),2)." X";
				else	
					$datos[$i]['total4']= number_format(trim($row['total4']),2);
				//$datos[$i]['check4']= trim($row['check4']);
				
				
				$i++;
			}
		}	
	
		$command="select SUM(total1) as dato1,SUM(total2) as dato2,SUM(total3) as dato3,SUM(total4) as dato4 from compradcuadroc where cuadroc=$numeroCC and estatus=0 group by cuadroc ";
		//echo $command;
		$getProducts = sqlsrv_query($conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datosTot[0]['nada']= "Total Cotizado";
				$datosTot[0]['dato1']= number_format(trim($row['dato1']),2);
				$datosTot[0]['dato2']= number_format(trim($row['dato2']),2);
				$datosTot[0]['dato3']= number_format(trim($row['dato3']),2);
				$datosTot[0]['dato4']= number_format(trim($row['dato4']),2);			
				
			}
		}
		$command="
		select sum(case when  check1=1	then total1 else 0 end) as dato1,
			sum(case when  check2=1	then total2 else 0 end) as dato2,
			sum(case when  check3=1	then total3 else 0 end) as dato3,
			sum(case when  check4=1	then total4 else 0 end) as dato4 from compradcuadroc where cuadroc=$numeroCC and estatus=0 group by cuadroc ";
		//echo $command;
		$getProducts = sqlsrv_query($conexion_srv,$command);
		if ( $getProducts === false)
		{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			//$datos=array();		
			while( $row = sqlsrv_fetch_array($getProducts, SQLSRV_FETCH_ASSOC))
			{
				$datosTot[1]['nada']= "Total a Ordenar";
				$datosTot[1]['dato1']= trim($row['dato1']);
				$datosTot[1]['dato2']= trim($row['dato2']);
				$datosTot[1]['dato3']= trim($row['dato3']);
				$datosTot[1]['dato4']= trim($row['dato4']);			
				
			}
		}
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Orden de Compra</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" src="../../prototype/jQuery.js"></script>
<script language="javascript" src="javascript/busquedaincCucadrocDOrdeC.js"></script>
<script language="javascript" src="javascript/comordendcompra.js"></script>
</head>

<body>
<form id="ordencompra"  style="width:100%;z-index:1;" action="ordendCompra.php">
<table width="100%">
	<tr>
		<td align="center" width="100%" class="TituloDForma">Orden de Compra<hr class="hrTitForma"></td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr><td width="111" class="texto8">Cuadro Comparativo:</td><td width="1104"><div align="left" style="z-index:6; position:relative;  width: 147px;">       
										<input name="id_ccomp" type="text" id="id_ccomp" size="50px" width="100%" onKeyUp="searchCuadroComparativo();" value="<?php echo $numeroCC;?>" autocomplete="off"/ tabindex="14" onFocus="javascript:$('optnumCuadro').checked=true">  
										<div id="search_suggestCC" style="z-index:5; position:absolute;" > </div>
					  				</div> <input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario;?>"><?php if(!$continua && isset($_REQUEST['id_ccomp'])) echo "<b class='texto8rojo'>El cuadro comparativo ya se encuentra en una Orden de Compra.</b>";?></td>
				</tr>
				<tr><td width="111" class="texto8">Partida Presupuestal:</td>
					<td><select name="parpresup" id="parpresup">
						<?php
							for($i=0;$i<count($partspresup);$i++)
								if($partspresup[$i]['id']==$parpresup)
									echo "<option value='".$partspresup[$i]['id']."' title='".$partspresup[$i]['ppresupuestal']." ".$partspresup[$i]['descrip']."' SELECTED>".$partspresup[$i]['ppresupuestal']." ".$partspresup[$i]['descrip']."</option>";
								else
									echo "<option value='".$partspresup[$i]['id']."' title='".$partspresup[$i]['ppresupuestal']." ".$partspresup[$i]['descrip']."'>".$partspresup[$i]['ppresupuestal']." ".$partspresup[$i]['descrip']."</option>";

						?>
						</select></td>
				</tr>
				<tr><td width="111" class="texto8">Los Precios son:</td>
					<td><select name="tipopcio" id="tipopcio">
						<?php
							for($i=0;$i<count($tipopcios);$i++)
								if($tipopcios[$i]['id']==$tipopcio)
									echo "<option value='".$tipopcios[$i]['tipopcio']."' title='".$tipopcios[$i]['descrip']."' SELECTED>".$tipopcios[$i]['descrip']."</option>";
								else
									echo "<option value='".$tipopcios[$i]['tipopcio']."' title='".$tipopcios[$i]['descrip']."'>".$tipopcios[$i]['descrip']."</option>";

						?>
						</select></td>
				</tr>
				<tr>
					<td class="texto8" width="111" >Seleccion:</td>
					<td> 
				  <select id="seleccion" name="seleccion" style="width:300px; "  disabled>
                    <?php //<?php echo count($selecciones);
						
						for($i=0;$i<count($selecciones); $i++)
						{
							if($seleccion==$selecciones[$i]['id'])
								echo "<option value='".$selecciones[$i]['id']."' title='".$selecciones[$i]['descrip']."' SELECTED>".$selecciones[$i]['descrip']."</option>";
							else	
								echo "<option value='".$selecciones[$i]['id']."' title='".$selecciones[$i]['descrip']."'>".$selecciones[$i]['descrip']."</option>";
						}
					?>
                  </select></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<thead>
					<tr class="subtituloverde8">
						<td width="2%">Cantidad</td>
						<td width="2%">Unidad</td>
						<td width="15%">Descripción</td>
						<td width="2%">
							<table width="100%"><tr><td colspan="2"><?php echo $prov1;?></td></tr>
									<tr><td width="50%">P.U.</td><td width="50%">Importe</td></tr>
							</table>
						</td>
						<td width="2%">
							<table width="100%"><tr><td colspan="2"><?php echo $prov2;?></td></tr>
									<tr><td width="50%">P.U.</td><td width="50%">Importe</td></tr>
							</table>
						</td>
						<td width="2%">
							<table width="100%"><tr><td colspan="2"><?php echo $prov3;?></td></tr>
									<tr><td width="50%">P.U.</td><td width="50%">Importe</td></tr>
							</table>
						</td>
						<td width="2%">
							<table width="100%"><tr><td colspan="2"><?php echo $prov4;?></td></tr>
									<tr><td width="50%">P.U.</td><td width="50%">Importe</td></tr>
							</table>
						</td>
					</tr>
				</thead>
				<tbody class="resultadobusqueda">
					<?php  
						if( $datos!=null) 
						for($i=0;$i<count($datos);$i++){ 
					?>
						<tr id="filaOculta" class="d<?php echo ($i % 2);?>">
							<td><?php echo $datos[$i]['cantidad'];?></td>
							<td><?php echo $datos[$i]['unidad'];?></td>
							<td><?php echo $datos[$i]['descripcion'];?></td>
							<td><table width="100%"><tr><td width="50%" style=" border-right:1px solid black;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['precio1'];?></td><td width="50%" style=" border-right:0px;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['total1'];?></td></tr></table></td>
							<td><table width="100%"><tr><td width="50%" style=" border-right:1px solid black;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['precio2'];?></td><td width="50%" style=" border-right:0px;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['total2'];?></td></tr></table></td>
							<td><table width="100%"><tr><td width="50%" style=" border-right:1px solid black;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['precio3'];?></td><td width="50%" style=" border-right:0px;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['total3'];?></td></tr></table></td>
							<td><table width="100%"><tr><td width="50%" style=" border-right:1px solid black;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['precio4'];?></td><td width="50%" style=" border-right:0px;; border-top:0px; border-left:0px; border-bottom:0px;"><?php echo $datos[$i]['total4'];?></td></tr></table></td>
							</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<?php  
						if( $datos!=null) 
						for($i=0;$i<count($datosTot);$i++){ 
					?>
					<tr>
						<td colspan="3" align="right" class="texto10"><?php echo $datosTot[$i]['nada'];?></td>
						<td colspan="1" align="right" class="texto10"><?php echo $datosTot[$i]['dato1'];?></td>
						<td colspan="1" align="right" class="texto10"><?php echo $datosTot[$i]['dato2'];?></td>
						<td colspan="1" align="right" class="texto10"><?php echo $datosTot[$i]['dato3'];?></td>
						<td colspan="1" align="right" class="texto10"><?php echo $datosTot[$i]['dato4'];?></td>
					</tr>
					<?php } ?>
				</tfoot>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center"><input type="button" id="GenOrComp" name="GenOrComp" onClick="validaYGeneraOrdedCompra()" value="Generar Orden de Compra"  class="caja_entrada" tabindex="503"></td>
	</tr>
</table>
</form>
</body>
</html>
