<?php
require_once("../../connections/dbconexion.php");
validaSession(getcwd());
require_once(getDirAtras(getcwd())."Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
////
$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
////
$numeroCC="";

if(isset($_REQUEST['txtSearch']))
	$numeroCC=$_REQUEST['txtSearch'];

$nomprod="";
if(isset($_REQUEST['nomprod']))
	$nomprod=$_REQUEST['nomprod']; 

$busProd="";
if(isset($_REQUEST['busProd']))
	$busProd=$_REQUEST['busProd'];

$numOrdenCompra="";
if(isset($_REQUEST['numOrdenCompra']))
	$numOrdenCompra=$_REQUEST['numOrdenCompra'];

$opcionbuscar="";
if(isset($_REQUEST['opcionbuscar']))
	$opcionbuscar=$_REQUEST['opcionbuscar'];


$provname1="";
if(isset($_REQUEST['provname1']))
	$provname1=$_REQUEST['provname1'];

$provid1="";
if(isset($_REQUEST['provid1']))
	$provid1=$_REQUEST['provid1'];

$fecini2="";
if(isset($_REQUEST['fecini2']))
	$fecini2=$_REQUEST['fecini2'];
$fecinicpy=$fecini2;

$fecfin="";
if(isset($_REQUEST['fecfin']))
	$fecfin=$_REQUEST['fecfin'];
$fecfincpy=$fecfin;


$conexion = sqlsrv_connect($server,$infoconexion);

$datos=array();

if($conexion  && isset($_REQUEST['opcionbuscar']))
{

	$command= "SELECT  b.orden, a.cuadroc, b.estatus, CONVERT(varchar(50),b.falta, 103) as falta,a.observa,
	stuff((SELECT ',' + cast( xa.surtido as varchar(50)) FROM compramsurtido xa 
				WHERE xa.orden = b.orden AND xa.estatus<9000 GROUP BY xa.surtido for xml path('') ),1,1,'')as surtido 
FROM compramordenes b
INNER JOIN compramcuadroc a  on a.cuadroc= b.cuadroc ";	
	
	if($opcionbuscar=="optFecha")
	{
		if(strlen($fecini2)>0)
		{
			$fecini2 = convertirFechaEuropeoAAmericano($fecini2);
			$command.= " WHERE b.falta>='$fecini2'";
			if(strlen($fecfin)>0)
			{
				$fecfin = convertirFechaEuropeoAAmericano($fecfin);
				$command.= " AND b.falta<='$fecfin'";
			}
		}
		else
			if(strlen($fecfin)>0)
			{
				$fecfin = convertirFechaEuropeoAAmericano($fecfin);
				$command.= " WHERE b.falta<='$fecfin'";
			}
	}
	else
	{
		if($opcionbuscar=="optObserva")
		{
			$command.= " WHERE b.prov =$provid1 ";
		}
		else
		{
			if($opcionbuscar=="optnumOrden")
				$command.= " WHERE b.orden=$numOrdenCompra ";
			else
				if($opcionbuscar=="optProducto")
					$command.= " WHERE b.orden IN (SELECT a.orden
									  FROM compradordenes a 
									  LEFT JOIN compradcuadroc b ON a.iddcuadroc=b.id 
									  LEFT JOIN compradrequisi c ON b.iddrequisi=c.id WHERE c.prod=$busProd) ";
				else
					$command.= " WHERE a.cuadroc='$numeroCC' ";
		}
	}
			
	$command.= " ORDER BY b.orden";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['cuadroc']= trim($row['cuadroc']);
			$datos[$i]['falta']= trim($row['falta']);
			$datos[$i]['observa']=utf8_decode( trim($row['observa']));
			$datos[$i]['estatus']= trim($row['estatus']);
			//echo $datos[$i]['estatus']."<br>";
			$datos[$i]['orden']= trim($row['orden']);
			$datos[$i]['surtido']= trim($row['surtido']);
			$i++;
		}
	}
}
else
{
	$fecinicpy= date("01/m/Y");
	$fecfincpy= date("d/m/Y");
}

?>
<html>
<head>
<title>Cuadro Comparativos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo getDirAtras(getcwd());?>x-jquery-plugins/functiones_Globales.js"></script>		

<?php  if($dependenciaTest) { ?>
<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>css/estilos.css">
 <?php  } ?>
 
 	
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo getDirAtras(getcwd());?>calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar-setup.js"></script>
<!-- Calendario -->

<!--<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="<?php echo getDirAtras(getcwd());?>prototype/prototype.js"></script>
<script language="javascript" src="javascript/buscar_ccomp.js"></script>-->
<script language="javascript" src="javascript/busquedaincCucadrocDOrdeC.js"></script>
<script language="javascript" src="javascript/busqIncreNumOrdenDCompra.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/busquedaIncProducto.js"></script>

<!--  MENU NUEVO -->
<?php  if(!$dependenciaTest) { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>css/x-estilos.css">
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/component.css" />
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/modernizr.custom.js"></script>
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/jquery.dlmenu.js"></script>
    <script>
     $(function() {
            $( '#dl-menu' ).dlmenu({
                animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
            });
        });
    </script>
<?php } ?>
<!--  MENU NUEVO -->
</head>
<?php  if(!$dependenciaTest) include_once( getDirAtras(getcwd())."indexMenu.php");?>
<body >
<form id="form_prensa"  method='POST' style="width:100%;z-index:1;" action="comcanordencompra.php" enctype='multipart/form-data' >
<table width="100%" border="0">
  	<tr >
  		<td colspan="2" align="center" class="TituloDForma">Ordenes de Compra Busqueda<input type="hidden" id='nivel' name='nivel' value='<?php echo $_REQUEST['nivel'];?>'>
  		  <hr class="hrTitForma"></td>		
	</tr>
	<tr>
		<td width="467">
			<table>
				<tr>
			  		<td width="30" class="texto8">
						<input name="opcionbuscar" id="optnumOrden" type="radio" value="optnumOrden" <?php if($opcionbuscar=="optnumOrden") echo "checked";?>>
				    </td>
					<td width="510"  scope="row" >
						<table>
							<tr>
								<td class="texto8" align="left" width="147px">Numero Orden de Compra:</td>
								<td width="147px"><div align="left" style="z-index:8; position:relative;  width: 147px;">
												<input type="text" id="numOrdenCompra" name="numOrdenCompra" style="width:100%;" value="<?php echo $numOrdenCompra;?>" 
												onKeyPress="return aceptarSoloNumeros(this, event);"
												onKeyUp="busqIncreNumOrdenDCompra(this);" 
												autocomplete="off"
												onFocus="javascript:document.getElementById('optnumOrden').checked=true">
										<div id="search_suggestOrdenCompra" style="z-index:7; position:absolute;"  > </div>
									</div>
									<!--<div align="left" style="z-index:1; position:relative; width: 147px;">       
										<input name="txtSearch" type="text" id="txtSearch" size="50px" width="50px" onKeyUp="searchProveedor1();" value="<?php echo $numeroCC;?>" autocomplete="off"/ tabindex="14" onFocus="javascript:document.getElementById('optnumOrden').checked=true">  
										<div id="search_suggest" style="z-index:2;" > </div>
					 				 </div> -->
								</td>
							</tr>
						</table>
						
						</td>
			    	</tr>
				<tr>
				<tr>
			  		<td width="30" class="texto8"><input name="opcionbuscar" id="optnumCuadro" type="radio" value="optnumCuadro" <?php if($opcionbuscar=="optnumCuadro") echo "checked";?>></td>
					<td width="510"  scope="row" >
						<table>
							<tr>
								<td class="texto8" align="left" width="147px">Numero Cuadro Comparativo</td>
								<td width="147px"><div align="left" style="z-index:6; position:relative;  width: 147px;">       
										<input name="txtSearch" type="text" id="txtSearch" size="50px" width="100%" onKeyUp="searchCuadroComparativo();" value="<?php echo $numeroCC;?>" autocomplete="off"/ tabindex="14" onFocus="javascript:document.getElementById('optnumCuadro').checked=true">  
										<div id="search_suggestCC" style="z-index:5; position:absolute;" > </div>
					  				</div>
								</td>
							</tr>
						 </table>
					</td>
			    </tr>
				<tr>
				  	<td width="30" class="texto8">
					<input name="opcionbuscar" id="optObserva" type="radio" value="optObserva" <?php if($opcionbuscar=="optObserva") echo "checked";?>>
					</td>
					<td  width="510" class="texto8" scope="row">		
						<table > 
							<tr> 
								<td width="150" class="texto8">Proveedor:</td>  
								<td class="texto8" align="left" width="147px"><div align="left" style="z-index:4; position:relative; width:147px;">     
										<input class="texto8" type="text" id="provname1" name="provname1" style="width:250px;"  onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="4" value="<?php if(strlen($provname1)>=0) echo $provname1; else echo '';?>" onFocus="javascript:document.getElementById('optObserva').checked=true">
										<input type="hidden" id="provid1" name="provid1" value ="<?php if(strlen($provid1)>0) echo $provid1; else echo '0'; ?>">
										<div id="search_suggestProv" style="z-index:3; position:absolute; width:350px;" > </div>
									</div> 
								</td>
							</tr>
					  </table>	
					</td>
				</tr>
				<tr>
				  	<td width="30" class="texto8">
					<input name="opcionbuscar" id="optProducto" type="radio" value="optProducto" <?php if($opcionbuscar=="optProducto") echo "checked";?>>
					</td>
					<td  width="510" class="texto8" scope="row">		
						<table > 
							<tr> 
								<td width="150" class="texto8">Producto:</td>  
								<td class="texto8" align="left" width="147px"> <div align="left" style="z-index:2; position:relative; width:147px">
										<input class="texto8" name="nomprod" type="text" id="nomprod" size="40" style="width:250px;"  onKeyUp="searchSuggestProd();" autocomplete="off" value="<?php if(strlen($nomprod)) echo $nomprod;?>" onFocus="javascript:document.getElementById('optProducto').checked=true"/>  
											<input class="texto8" name="busProd" type="hidden" id="busProd" size="40" style="width:250px;"  onKeyUp="searchSuggestProd();" autocomplete="off" value="<?php if(strlen($busProd)) echo $busProd;?>"/>  
										<div id="search_suggest" style="z-index:1; position:absolute; width:350px;" > </div>
		   		   					 </div>
								</td>
							</tr>
					  </table>	
					</td>
				</tr>
				<tr>
			  	  <td width="30" class="texto8">
					<input name="opcionbuscar" id="optFecha" value="optFecha" type="radio"  <?php if($opcionbuscar=="optFecha") echo "checked";?>>		      		  
				</td>
				  	<td width="510" class="texto8" scope="row" > 
						<table width="100%"> <tr><td class="texto8" width="50%">Fecha Inicial:
						<input name="fecini2" type="text" size="7" id="fecini2" value="<?php echo $fecinicpy;?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:document.getElementById('optFecha').checked=true">
						<img src="<?php echo getDirAtras(getcwd());?>calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title="Date selector" align="absmiddle" onClick="javascript:document.getElementById('optFecha').checked=true" > 
						<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini2",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script></td>
						<td class="texto8" width="50%">Fecha Final:<input name="fecfin" type="text" size="7" id="fecfin" value="<?php echo $fecfincpy;?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:document.getElementById('optFecha').checked=true">
						<img src="<?php echo getDirAtras(getcwd());?>calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Date selector" align="absmiddle" > 
				  <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>				 
					</td></tr></table> 
				</td>
			  </tr>
				
				
			</table>
		</td>
	  <td width="789" valign="bottom">
        <input name="button" type="submit" onClick= value="Buscar" value="buscar"><!--"buscar_ccomps();"-->
</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr>
		<th width="10%" class="subtituloverde8">Orden de Compra </th>
		<th width="50%" class="subtituloverde8">Observaciones</th>
		<th width="10%" class="subtituloverde8">Cuadro Comparativo</th>
		<th width="10%" class="subtituloverde8">Fecha</th>
		<th width="20%" class="subtituloverde8">&nbsp;</th>
	</tr>
	<tbody class="resultadobusqueda">
		<?php
			for($i=0;$i<count($datos);$i++)
			{
		?>
			<tr class="d<?php echo ($i % 2);?>">
				<td width="10%" class="texto8" align="center"><?php echo $datos[$i]['orden'];?></td>
				<td width="50%" class="texto8"><?php echo $datos[$i]['observa'];?></td>
				<td width="10%" class="texto8" align="center"><?php echo $datos[$i]['cuadroc'];?></td>
				<td width="10%" class="texto8" align="center"><?php echo $datos[$i]['falta'];?></td>
				<td width="10%" class="texto8" align="center">	
                <?php if(revisaPrivilegiosPDF($privilegios))
			{
				if(file_exists('php_ajax/open_cuadrocomp_dompdf.php?id='.$datos[$i]['cuadroc']))
				{
				?>		 
					<img src="<?php echo getDirAtras(getcwd());?>imagenes/consultar.jpg" onClick="javascript:window.open('pdf_files/OrdCompra_<?php echo $datos[$i]['orden'];?>.pdf')" title="Click aqui para ver la Orden de Compra. ../pdf_files/OrdCompra_<?php echo $datos[$i]['orden'];?>.pdf">
<?php 
				}
				else
				{
					?>
					<img src="<?php echo getDirAtras(getcwd());?>imagenes/consultar.jpg" onClick="javascript:window.open('php_ajax/open_ordendecompra_dompdf.php?orden=<?php echo $datos[$i]['orden'];?>&time=<?php microtime(true);?>', 'clearcache=yes')" title="FFF Click aqui para ver la requisici&oacute;n impresa.">
					<?php
				}
?>						 
					 
					<?php 
			}
						if( (int)$datos[$i]['estatus'] < 9000)
						{ 
							if( strlen( $datos[$i]['surtido'] )>0 && $datos[$i]['surtido']!="null")
							{
								echo "<a title='La orden de Compra tiene actualmente surtidos ".$datos[$i]['surtido']."' class='texto8'> Surtido:".$datos[$i]['surtido']."</a>";
							}
							else
							{
								if( revisaPrivilegiosBorrar($privilegios)){
								?>
								
								<img src="<?php echo getDirAtras(getcwd());?>imagenes/eliminar.jpg" onClick="javascript:if(confirm('�Esta seguro que desea cancelar esta orden de compra?.'))location.href='php_ajax/comcancelacionordcomp.php?orden=<?php echo $datos[$i]['orden'];?>'" title="Click aqui para cancelar la Orden de compra.">
						<?php 
								}
							}
						}
						else {?>
								<img src="<?php echo getDirAtras(getcwd());?>imagenes/borrar.jpg"  title="Esta Orden de Compra se encuentra cancelada.">
						<?php }?>
				</td>
			</tr>
		<?php
			}
		?>
	</tbody>
</table>
</form>
</body>
</html>
