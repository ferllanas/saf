<?php
require_once("../../connections/dbconexion.php");
validaSession(getcwd());
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion


$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
	
$numeroCC="";

if(isset($_REQUEST['txtSearch']))
	$numeroCC=$_REQUEST['txtSearch'];

$opcionbuscar="";
if(isset($_REQUEST['opcionbuscar']))
	$opcionbuscar=$_REQUEST['opcionbuscar'];

$observaciones="";
if(isset($_REQUEST['observaciones']))
	$observaciones=$_REQUEST['observaciones'];

$fecini2="";
if(isset($_REQUEST['fecini2']))
	$fecini2=$_REQUEST['fecini2'];
$fecinicpy=$fecini2;

$fecfin="";
if(isset($_REQUEST['fecfin']))
	$fecfin=$_REQUEST['fecfin'];
$fecfincpy=$fecfin;


$conexion = sqlsrv_connect($server,$infoconexion);

$datos=array();

if($conexion  && isset($_REQUEST['opcionbuscar']))
{

	$command= "SELECT a.cuadroc, a.estatus, CONVERT(varchar(50),a.falta, 103) as falta,a.observa,
					stuff((SELECT ',' + cast(  xa.orden as varchar(50))   FROM compramordenes xa WHERE xa.cuadroc = a.cuadroc AND xa.estatus<9000 GROUP BY xa.orden
									for xml path('') ),1,1,'')as orden

					FROM compramcuadroc a ";	//, b.orden  LEFT JOIN compramordenes b on a.cuadroc=  b.cuadroc
	
	if($opcionbuscar=="optFecha")
	{
		if(strlen($fecini2)>0)
		{
			$fecini2 = convertirFechaEuropeoAAmericano($fecini2);
			$command.= " WHERE a.falta>='$fecini2'";
			if(strlen($fecfin)>0)
			{
				$fecfin = convertirFechaEuropeoAAmericano($fecfin);
				$command.= " AND a.falta<='$fecfin'";
			}
		}
		else
			if(strlen($fecfin)>0)
			{
				$fecfin = convertirFechaEuropeoAAmericano($fecfin);
				$command.= " WHERE a.falta<='$fecfin'";
			}
	}
	else
	{
		if($opcionbuscar=="optObserva")
		{
			$command.= " WHERE a.observa LIKE '%".utf8_encode($observaciones)."%'";
		}
		else
		{
			$command.= " WHERE a.cuadroc='$numeroCC' ";
		}
	}
			
	$command.= " ORDER BY a.cuadroc";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['cuadroc']= trim($row['cuadroc']);
			$datos[$i]['falta']= trim($row['falta']);
			$datos[$i]['observa']=utf8_decode( trim($row['observa']));
			$datos[$i]['estatus']= trim($row['estatus']);
			$datos[$i]['orden']= trim($row['orden']);
			$i++;
		}
	}
}
else
{
	$fecinicpy= date("01/m/Y");
	$fecfincpy= date("d/m/Y");
}

?>
<html>
<head>
<title>Cuadro Comparativos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo getDirAtras(getcwd());?>x-jquery-plugins/functiones_Globales.js"></script>		

<?php  if($dependenciaTest) { ?>
<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>css/estilos.css">
 <?php  } ?>
 
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo getDirAtras(getcwd());?>calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="<?php echo getDirAtras(getcwd());?>calendario/calendar-setup.js"></script>
<!-- Calendario -->

<!--
<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
-->
<script language="javascript" src="javascript/buscar_ccomp.js"></script>
<script language="javascript" src="javascript/busquedaincCucadroc.js"></script>
<!--  MENU NUEVO -->
<?php  if(!$dependenciaTest) { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>css/x-estilos.css">
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/css/component.css" />
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/modernizr.custom.js"></script>
    <script src="<?php echo getDirAtras(getcwd());?>PlantillaCSS/ResponsiveMultiLevelMenu/js/jquery.dlmenu.js"></script>
    <script>
     $(function() {
            $( '#dl-menu' ).dlmenu({
                animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
            });
        });
    </script>
<?php } ?>
<!--  MENU NUEVO -->
</head>
<?php  if(!$dependenciaTest) include_once( getDirAtras(getcwd())."indexMenu.php");?>
<body >
<form id="form_prensa"  method='POST' style="width:100%" action="buscar_ccomp.php" enctype='multipart/form-data'>
<table width="100%" border="0">
  	<tr >
  		<td colspan="2" align="center" class="TituloDForma">Cuadro Comparativo Busqueda<input type="hidden" id='nivel' name='nivel' value='<?php echo $_REQUEST['nivel'];?>'>
  		  <hr class="hrTitForma"></td>		
	</tr>
	<tr>
		<td width="39%">
			<table width="564">
				<tr>
				  	<td width="238" class="texto8">
						<input name="opcionbuscar" id="optnumCuadro" type="radio" value="optnumCuadro" <?php if($opcionbuscar=="optnumCuadro") echo "checked";?>>
						 Numero Cuadro Comparativo:</td>
					<td width="314" class="texto8" scope="row" >
						<div align="left" style="z-index:1; position:absolute; <?php if($dependenciaTest) echo "left:216px; top:45px;";else echo "left:250px; top:95px;";?> width: 147px;">       
							<input name="txtSearch" type="text" id="txtSearch" size="50px" width="50px" onKeyUp="searchProveedor1();" value="<?php echo $numeroCC;?>" autocomplete="off"/ tabindex="14" onFocus="javascript:document.getElementById('optnumCuadro').checked=true" class="texto8">  
							<div id="search_suggest" style="z-index:2;" > </div>
					  </div> 
					</td>
			    </tr>
				<tr>
				  <td width="238" class="texto8"><input name="opcionbuscar" id="optObserva" type="radio" value="optObserva" <?php if($opcionbuscar=="optObserva") echo "checked";?>>
				    Observaci&oacute;n:</td>
				  <td  width="314" class="texto8" scope="row" ><input type="text" name="observaciones" onFocus="javascript:document.getElementById('optObserva').checked=true" value="<?php echo $observaciones;?>" class="texto8"></td>
			  </tr>
				<tr>
				  	<td width="238" class="texto8"><input name="opcionbuscar" id="optFecha" value="optFecha" type="radio"  <?php if($opcionbuscar=="optFecha") echo "checked";?>>
			      		 Fecha Inicial:
						<input name="fecini2" type="text" size="7" id="fecini2" value="<?php echo $fecinicpy;?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:document.getElementById('optFecha').checked=true">
						<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title="Date selector" align="absmiddle">  
						<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini2",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
						</td><td class="texto8">Fecha Final:<input name="fecfin" type="text" size="7" id="fecfin" value="<?php echo $fecfincpy;?>"  maxlength="10"  style="width:70" onFocus="javascript:document.getElementById('optFecha').checked=true" class="texto8">
						<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Date selector" align="absmiddle"> 
						<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
					</td>
			  </tr>
			</table>
		</td>
	  <td width="61%" valign="bottom">
        <input name="button" type="submit"  value="Buscar"><!--"buscar_ccomps();"-->
</td>
	</tr>
</table>
<table id="busqueda_ef" width="100%">
	<tr>
		<th width="10%" class="subtituloverde8">Num. Cuadro</th>
		<th width="50%" class="subtituloverde8">Observaciones</th>
		<th width="10%" class="subtituloverde8">Fecha</th>
		<th width="20%" class="subtituloverde8">&nbsp;</th>
	</tr>
	<tbody class="resultadobusqueda">
		<?php
			for($i=0;$i<count($datos);$i++)
			{
		?>
			<tr class="d<?php echo ($i % 2);?>">
				<td width="10%" class="texto8" align="center"><a href='CuadroComparativo.php?id_cuadroc=<?php echo $datos[$i]['cuadroc'];?>' class='texto8' title="Click aqui para ver el cuador comparativo"><?php echo $datos[$i]['cuadroc'];?></a></td>
				<td width="50%" class="texto8"><?php echo $datos[$i]['observa'];?></td>
				<td width="10%" class="texto8" align="center"><?php echo $datos[$i]['falta'];?></td>
				<td width="20%" class="texto8" align="center">			
 <?php if(revisaPrivilegiosPDF($privilegios))
			{
				if(file_exists('pdf_files/cuadroc_'.$datos[$i]['cuadroc'].'.pdf '))
				{
				?>		 

					<img src="../../imagenes/consultar.jpg" onClick="javascript:window.open('pdf_files/cuadroc_<?php echo $datos[$i]['cuadroc'];?>.pdf')" title="Click aqui para ver el cuadro comparativo."> 
<?php 
				}
				else
				{
					?>
					<img src="<?php echo getDirAtras(getcwd());?>imagenes/consultar.jpg" onClick="javascript:window.open('php_ajax/open_cuadrocomp_dompdf.php?id=<?php echo $datos[$i]['cuadroc'];?>&time=<?php microtime(true);?>', 'clearcache=yes')" title="FFF Click aqui para ver la requisici&oacute;n impresa.">
					<?php
				}

			}
						if( (int)$datos[$i]['estatus'] < 90 )
						{ 
							if( strlen( $datos[$i]['orden'] )>0 && $datos[$i]['orden']!="null")
							{
								echo "<a title='El cuadro comparativo se encuentra actualmente sobre la Orden de Compra ".$datos[$i]['orden']."' class='texto8'> OC:".$datos[$i]['orden']."</a>";
							}
							else
							{
								if( revisaPrivilegiosBorrar($privilegios)){
								?>
								
								<img src="../../imagenes/eliminar.jpg"onClick="javascript:if(confirm('�Seguro desea cancelar este cuadro comparativo?'))location.href='php_ajax/cancela_cuadroc.php?id_cuadroc=<?php echo $datos[$i]['cuadroc'];?>'" title="Click aqui para cancelar la requisici�n.">
						<?php 
								}
							}
						}
						else {?>
								<img src="../../imagenes/borrar.jpg"  title="Esta requisici�n se encuentra cancelada.">
						<?php }?>
				</td>
			</tr>
		<?php
			}
		?>
	</tbody>
</table>
</form>
</body>
</html>
