<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/busqIncreNumOrdenDCompra.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/buscarOrdenesDCompra.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>

<style type="text/css" media="screen">
            body 
			{
                font-size:10;
      			font-family:verdana;
            }
            .suggest_link {
                background-color: #FFFFFF;
                border: 0px solid #000000;
                padding: 2px 6px 2px 6px;
                
            }                 
            .suggest_link_over {
                background-color: #FF7F00;
                color: white;
                border: 0px solid #000000;
                padding: 2px 6px 2px 6px;
                border-bottom: none;
            }
            .search_suggestOrdenCompra 
			{
                position: absolute; 
				overflow:visible;
				z-index:2;
                right: 577px;
       			cursor: pointer;        
        		width:195px;
                background-color: #FFFFFF;
                text-align: left;
       			border: 1px solid #000000;
				padding:1px 1px 1px 1px ;
        		border-style: hidden solid solid solid;  
      		}
        </style> 
</head>
	
<body>
	<table width="100%">
		<tr>
			<td align="center" colspan="3" class="TituloDForma">Surtido: Busqueda de Ordenes de Compra<hr class="hrTitForma">
			</td>
		</tr>
		<tr>
			<td  align="left" width="12%" class="texto8">N�mero de Orden de Compra:
			</td>
			<td align="left" width="49%"><div align="left" style="z-index:4; position:absolute; width:25%; top: 53px; left: 163px;">     
										<input type="text" id="idordercompra" name="idordercompra" style="width:100%;"  onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="busqIncreNumOrdenDCompra(this);" autocomplete="off">
										<div id="search_suggestOrdenCompra" style="z-index:5;" > </div>
									</div>  </td>
			<td width="39%" align="center"><input type="button" value="Buscar" onClick="buscarOrdenesDCompra();"></td>
		</tr>
		<tr>
			<td align="left" width="12%" class="texto8">
				Nombre del Proveedor:
			</td>
			<td colspan="2"><div align="left" style="z-index:1; position:absolute; width:25%; top: 80px; left: 163px;">     
										<input type="text" id="provname1" name="provname1" style="width:100%;"  onKeyUp="searchProveedor1(this);" autocomplete="off">
										<input type="hidden" id="provid1" name="provid1" >
										<div id="search_suggestProv" style="z-index:2;" > </div>
									</div> </td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
	<table	id="OrdenesCompra" name="OrdenesCompra" width="100%">
		<tr>
			<th width="12.5%" class="subtituloverde">Orden de Compra</th>
			<th width="25%" class="subtituloverde">Proveedor</th>
			<th width="50%" class="subtituloverde">Observaciones</th>
			<th width="12.5%" class="subtituloverde">Fecha</th>
		</tr>
		<tbody id="resOrdenesCompra" name="resOrdenesCompra">	
		</tbody>
	</table>
</body>
</html>
