function buscarOrdenesDCompra()
{
	var nomProveedor = $('provname1').value;
	if(nomProveedor.length<=0)
	{
		 $('provid1').value="";
	}
	var idordencompra = $('idordercompra').value;
	var idProveedor = $('provid1').value;
	
	
	//alert('php_ajax/buscarOrdenesDCompra.php?idOrdenCompra='+idordencompra+'&idProveedor='+idProveedor);
				
	new Ajax.Request('php_ajax/buscarOrdenesDCompra.php?idOrdenCompra='+idordencompra+
				'&idProveedor='+idProveedor,
				{onSuccess : function(resp) 
					{
						if( resp.responseText ) 
						{
							//alert(resp.responseText);
							var myArray = eval(resp.responseText);
							if(myArray.length>0)
							{
								var Table = $('resOrdenesCompra');
								for(var i = Table.rows.length-1; i >=0; i--)
								{
									Table.deleteRow(i);
								}
								for(var i=0;i<myArray.length;i++)
								{
									agregar_OrdenDCompraATabla(myArray[i].id , myArray[i].observa , myArray[i].prov , myArray[i].falta);
								}
							}
							else
							{
								alert("No obtuvo resultados!.");
							}
						}
						else
						{
								alert("Jajajajaja!.");
						}
					}
				});
}

//Agrega una nueva menu en la lista despencientes
function agregar_OrdenDCompraATabla(idorden,observaciones,proveedor,fecha)
{	
	var Table = $('resOrdenesCompra');//Se inserta en el body
	
	//Create the new elements
	var NewRow = document.createElement("tr");
	var cellidorden = document.createElement("td");
	cellidorden.align="center";
	var cellobservaciones = document.createElement("td");
	cellobservaciones.align="center";
	var cellproveedor = document.createElement("td");
	cellproveedor.align="center";
	var cellfecha = document.createElement("td");
	cellfecha.align="center";
	
	var inputIdOrdenC = document.createElement("a");
    inputIdOrdenC.setAttribute("href", 'surtido.php?idorden='+idorden);
	inputIdOrdenC.setAttribute('class', 'texto8');
    inputIdOrdenC.appendChild(document.createTextNode(idorden));

	var inputObservaciones = document.createElement("a");
	inputObservaciones.setAttribute('class', 'texto8');
	inputObservaciones.setAttribute('with', '100%');
    inputObservaciones.appendChild(document.createTextNode(observaciones));
	
	var inputproveedor = document.createElement("a");
	inputproveedor.setAttribute('class', 'texto8');
    inputproveedor.appendChild(document.createTextNode(proveedor));
	
	var inputFecha = document.createElement("a");
	inputFecha.setAttribute('class', 'texto8');
    inputFecha.appendChild(document.createTextNode(fecha));
	
	//Add textboxes to cells
	cellidorden.appendChild(inputIdOrdenC);
	cellobservaciones.appendChild(inputproveedor);
	cellproveedor.appendChild(inputObservaciones); 
	cellfecha.appendChild(inputFecha);
	//Add elements to row.
	NewRow.appendChild(cellidorden);
	NewRow.appendChild(cellobservaciones);
	NewRow.appendChild(cellproveedor);
	NewRow.appendChild(cellfecha);
	//Add row to table
	Table.appendChild(NewRow);
}
