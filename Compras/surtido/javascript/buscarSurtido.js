function buscarSurtido()
{
	var nomProveedor = $('provname1').value;
	if(nomProveedor.length<=0)
	{
		 $('provid1').value="";
	}
	var idordencompra = $('idordercompra').value;
	var idProveedor = $('provid1').value;
	//alert('php_ajax/buscarSurtidosxProvOIdCompra.php?idOrdenCompra='+idordencompra+'&idProveedor='+idProveedor);
	new Ajax.Request('php_ajax/buscarSurtidosxProvOIdCompra.php?idOrdenCompra='+idordencompra+
				'&idProveedor='+idProveedor,
				{onSuccess : function(resp) 
					{
						//alert(resp.responseText);
						if( resp.responseText ) 
						{
							//
							var myArray = eval(resp.responseText);
							if(myArray.length>0)
							{
								var Table = $('res_Surtidos');
								for(var i = Table.rows.length-1; i >=0; i--)
								{
									Table.deleteRow(i);
								}
								
								for(var i=0;i<myArray.length;i++)
								{
									//alert(i+" = "+myArray[i].id+","+myArray[i].observa+","+myArray[i].prov+","+myArray[i].falta);
									agregar_OrdenDCompraATabla2(myArray[i].id , myArray[i].orden , myArray[i].observa , myArray[i].nomprov , myArray[i].falta);
								}
							}
							else
							{
								alert("No obtuvo resultados!.");
							}
						}
					}
				});
}

//Agrega una nueva menu en la lista despencientes
function agregar_OrdenDCompraATabla2(idsurtido,idorden,observaciones,proveedor,fecha)
{	
	var Table = $('res_Surtidos');//Se inserta en el body
	
	//Create the new elements
	var NewRow = document.createElement("tr");
	
	var cellidsurtido = document.createElement("td");
	cellidsurtido.align="center";
	var cellidorden = document.createElement("td");
	cellidorden.align="center";
	var cellobservaciones = document.createElement("td");
	cellobservaciones.align="center";
	var cellproveedor = document.createElement("td");
	cellproveedor.align="center";
	var cellfecha = document.createElement("td");
	cellfecha.align="center";
	
	var inputIdSurtido = document.createElement("a");
    inputIdSurtido.setAttribute("href", 'surtido.php?idsurtido='+idsurtido);
	inputIdSurtido.setAttribute('class', 'texto8');
    inputIdSurtido.appendChild(document.createTextNode(idsurtido));

	var inputIdOrden = document.createElement("a");
	inputIdOrden.setAttribute('class', 'texto8');
	inputIdOrden.setAttribute('with', '100%');
    inputIdOrden.appendChild(document.createTextNode(idorden));
	
	var inputObservaciones = document.createElement("a");
	inputObservaciones.setAttribute('class', 'texto8');
    inputObservaciones.appendChild(document.createTextNode(proveedor));
	
	var inputproveedor = document.createElement("a");
	inputproveedor.setAttribute('class', 'texto8');
    inputproveedor.appendChild(document.createTextNode(observaciones));
	
	var inputFecha = document.createElement("a");
	inputFecha.setAttribute('class', 'texto8');
    inputFecha.appendChild(document.createTextNode(fecha));
	
	//Add textboxes to cells
	cellidsurtido.appendChild(inputIdSurtido);
	cellidorden.appendChild(inputIdOrden);
	cellobservaciones.appendChild(inputproveedor);
	cellproveedor.appendChild(inputObservaciones); 
	cellfecha.appendChild(inputFecha);
	
	//Add elements to row.
	NewRow.appendChild(cellidsurtido);
	NewRow.appendChild(cellidorden);
	NewRow.appendChild(cellobservaciones);
	NewRow.appendChild(cellproveedor);
	NewRow.appendChild(cellfecha);
	//Add row to table
	Table.appendChild(NewRow);

}
