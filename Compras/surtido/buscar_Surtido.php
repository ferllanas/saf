<?php
require_once("../../connections/dbconexion.php");
validaSession(getcwd());
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');


////
$usuario = $_COOKIE['ID_my_site'];

$idPantalla=0;
if(isset($_REQUEST['nivel']))
{
	$idPantalla=$_REQUEST['nivel'];
	$hour = time() + 144000; 
	setcookie('lastPantalla', $idPantalla, $hour); 
}
else
	if(isset($_COOKIE['lastPantalla']))
		$idPantalla=$_COOKIE['lastPantalla'];

$privilegios= get_Privilegios($usuario, $idPantalla);
////

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$fecinicpy= date("01/m/Y");
$fecfincpy= date("d/m/Y");

$idsurtido="";
if(isset($_REQUEST['idsurtido']))
	$idsurtido = $_REQUEST['idsurtido'];

$opcionbuscar="";
if(isset($_REQUEST['opcionbuscar']))
	$opcionbuscar = $_REQUEST['opcionbuscar'];

$idOrdenCompra="0";
if(isset($_REQUEST['idordercompra']) && strlen($_REQUEST['idordercompra'])>0)
	$idOrdenCompra = $_REQUEST['idordercompra'];


$provname1="";
if(isset($_REQUEST['provname1']))
	$provname1 = $_REQUEST['provname1'];

$idProveedor="";
if(isset($_REQUEST['provid1']))
	$idProveedor = $_REQUEST['provid1'];

$fecfin="";
if(isset($_REQUEST['fecfin']))
	$fecfin = $_REQUEST['fecfin'];

$fecini2="";
if(isset($_REQUEST['fecini2']))
	$fecini2 = $_REQUEST['fecini2'];

$entro=false;
$datos= array();
//echo $idProveedor;
if($conexion && strlen($opcionbuscar)>0)
{
	$command= "SELECT c.surtido, a.orden,  a.prov, convert(varchar, c.falta, 103) as falta, b.nomprov , c.observa, c.estatus,
				stuff((SELECT ',' + cast(  xa.vale as varchar(50))   FROM egresosdvale xa WHERE xa.tipodocto = 1 AND xa.numdocto=c.surtido AND xa.estatus<9000 GROUP BY xa.vale
									for xml path('') ),1,1,'')as vale
				FROM compramsurtido c 
				LEFT JOIN compramordenes a on c.orden=a.orden
				LEFT JOIN compramprovs b on a.prov=b.prov ";
	switch($opcionbuscar)
	{			
		
		case "optnumsurtido":
				$command.= " WHERE c.surtido = '$idsurtido'";
				$entro=true;
			break;
			
		case "optnumOrden":
				$command.= " WHERE a.orden = '$idOrdenCompra'";
				$entro=true;
			break;
		case "optnomProv":
				$command.= " WHERE  a.prov=$idProveedor";
				$entro=true;
			break;
		case "optFecha":
				if(strlen($fecini2)>0)
				{
					$fecinicpy= $fecini2;
					$fecini2 = convertirFechaEuropeoAAmericano($fecini2);
					$command.= " WHERE c.falta>='$fecini2'";
					if(strlen($fecfin)>0)
					{
						$fecfincpy= $fecfin;
						$fecfin = convertirFechaEuropeoAAmericano($fecfin);
						$command.= " AND c.falta<='$fecfin'";
					}
					
				}
				else
					if(strlen($fecfin)>0)
					{
						$fecfincpy= $fecfin;
						$fecfin = convertirFechaEuropeoAAmericano($fecfin);
						$command.= " WHERE c.falta<='$fecfin'";
					}
					
				$entro=true;
			break;
		default:
			break;
	}
		
	if($entro == true)
		$command.= "  AND a.estatus<9000";
	else	
		$command.= "  WHERE a.estatus<9000";
	$command.= "  ORDER bY surtido ASC";
	
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']= trim($row['surtido']);
			$datos[$i]['observa']= trim($row['observa']);
			$datos[$i]['orden']= trim($row['orden']);
			//echo "<br>!!!!".$row['orden'];
			$datos[$i]['nomprov']= trim($row['nomprov']);
			$datos[$i]['falta']= trim($row['falta']);
			$datos[$i]['estatus']= trim($row['estatus']);
			$i++;
		}
	}
	//print_r($datos);
}
//echo $opcionbuscar;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Buscar Surtidos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/busqIncreNumOrdenDCompra.js"></script>
<script language="javascript" src="javascript/busquedaincProv1.js"></script>
<script language="javascript" src="javascript/buscarSurtido.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
</head>
	
<body>
<form method='POST' style="width:100%" action="buscar_Surtido.php" enctype='multipart/form-data'  >
	<table width="100%">
		<tr>
			<td align="center" colspan="4" class="TituloDForma">Surtido: Busqueda de Surtidos<input type="hidden" id='nivel' name='nivel' value='<?php echo $_REQUEST['nivel'];?>'>
			  <hr class="hrTitForma">
			</td>
		</tr>
        <tr>
        	<td align="left" width="2%" class="texto8"><input name="opcionbuscar" id="optnumsurtido" type="radio" value="optnumsurtido" <?php if($opcionbuscar=="optnumsurtido") echo "checked";?>></td>
			<td align="left" width="13%" class="texto8">N&uacute;mero de Orden de Surtido:
			</td>
			<td align="left" width="30%"><div align="left" style="z-index:4; position:absolute; width:25%; top: 40px;">     
										<input type="text" id="idsurtido" name="idsurtido" style="width:100%;" value="<?php echo $idsurtido;?>" 
												onKeyPress="return aceptarSoloNumeros(this, event);"
												autocomplete="off"
												onFocus="javascript:$('optnumsurtido').checked=true">
										<div id="search_suggestOrdenCompra" style="z-index:5;" > </div>
									</div>  </td>
			
        </tr>
		<tr>
			<td align="left" width="2%" class="texto8"><input name="opcionbuscar" id="optnumOrden" type="radio" value="optnumOrden" <?php if($opcionbuscar=="optnumOrden") echo "checked";?>></td>
			<td align="left" width="13%" class="texto8">N&uacute;mero de Orden de Compra:
			</td>
			<td align="left" width="30%"><div align="left" style="z-index:4; position:absolute; width:25%; top: 62px;">     
										<input type="text" id="idordercompra" name="idordercompra" style="width:100%;" value="<?php echo $idOrdenCompra;?>" 
												onKeyPress="return aceptarSoloNumeros(this, event);"
												autocomplete="off"
												onFocus="javascript:$('optnumOrden').checked=true">
										<div id="search_suggestOrdenCompra" style="z-index:5;" > </div>
									</div>  </td>
			<td width="55%" align="left"><input type="submit" value="Buscar" ></td>
		</tr>
		<tr>
			<td align="left" width="2%" class="texto8"><input name="opcionbuscar" id="optnomProv" type="radio" value="optnomProv" <?php if($opcionbuscar=="optnomProv") echo "checked";?>></td>
			<td align="left" width="13%" class="texto8">
				Nombre del Proveedor:
			</td>
			<td colspan="2"><div align="left" style="z-index:1; position:absolute; width:25%; top: 85px;">     
										<input type="text" id="provname1" name="provname1" style="width:100%;"  value="<?php echo $provname1;?>"   
										onKeyUp="searchProveedor1(this);" 
										autocomplete="off"
										onFocus="javascript:$('optnomProv').checked=true">
										<input type="hidden" id="provid1" name="provid1" value="<?php echo $idProveedor;?>">
										<div id="search_suggestProv" style="z-index:2;" > </div>
									</div> </td>
		</tr>
		<tr>
			<td align="left" width="2%"  class="texto8"><input name="opcionbuscar" id="optFecha" type="radio" value="optFecha" <?php if($opcionbuscar=="optfecha") echo "checked";?>></td>
			<td align="left" colspan="3" class="texto8"><table width="100%"> <tr><td class="texto8" width="19%">Fecha Inicial:
						<input name="fecini2" type="text" size="7" id="fecini2" value="<?php echo $fecinicpy;?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:$('optFecha').checked=true">
						<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger1" style="cursor: pointer;" title="Date selector" align="absmiddle" onClick="javascript:$('optFecha').checked=true" > 
						<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecini2",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger1",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script></td>
						<td class="texto8" width="81%">Fecha Final:<input name="fecfin" type="text" size="7" id="fecfin" value="<?php echo $fecfincpy;?>" class="texto8" maxlength="10"  style="width:70" onFocus="javascript:$('optFecha').checked=true">
						<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger" style="cursor: pointer;" title="Date selector" align="absmiddle" > 
				  <script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecfin",		// id of the input field
													ifFormat       :    "%d/%m/%Y",		// format of the input field
													button         :    "f_trigger",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>				 
					</td></tr></table> 
			</td>
		</tr>
	</table>
	<table	id="OrdenesCompra" name="OrdenesCompra" width="100%">
		<tr>
			<th width="10%" class="subtituloverde">Surtidos</th>
			<th width="10%" class="subtituloverde">Orden de Compra</th>
			<th width="15%" class="subtituloverde">Proveedor</th>
			<th width="25%" class="subtituloverde">Observaciones</th>
			<th width="5%" class="subtituloverde">Fecha</th>
			<th width="5%" class="subtituloverde">&nbsp;</th>
		</tr>
		<tbody class="resultadobusqueda">
		<?php
			for($i=0;$i<count($datos);$i++)
			{
		?>
			<tr class="d<?php echo ($i % 2);?>">
				<td width="10%" class="texto8" align="center"><?php echo $datos[$i]['id'];?></td>
				<td width="10%" class="texto8" align="center"><?php echo $datos[$i]['orden'];?></td>
				<td width="15%" class="texto8" align="center"><?php echo $datos[$i]['nomprov'];?></td>
				<td width="25%" class="texto8" align="left">&nbsp;<?php echo $datos[$i]['observa'];?></td>
				<td width="5%" class="texto8" align="center"><?php echo $datos[$i]['falta'];?></td> 
				<td width="20%" class="texto8">
                <?php 
						if(revisaPrivilegiosBorrar($privilegios))
						{
				?>
					<img src="../../imagenes/consultar.jpg" onClick="javascript:window.open('php_ajax/open_openSurtidodomPDF.php?id_dsurtido=<?php echo $datos[$i]['id'];?>')" title="Click aqui para ver el surtido."> 
					<?php 
						}
						
						if( (int)$datos[$i]['estatus'] < 9000 )
						{ 
							/*if( strlen( $datos[$i]['orden'] )>0 && $datos[$i]['orden']!="null")
							{
								echo "<a title='El cuadro comparativo se encuentra actualmente sobre la Orden de Compra ".$datos[$i]['orden']."' class='texto8'> OC:".$datos[$i]['orden']."</a>";
							}
							else
							{*/
							if(revisaPrivilegiosBorrar($privilegios))
							{
				?>
								
								<img src="../../imagenes/eliminar.jpg" onClick="javascript:if(confirm('�Esta seguro que desea cancelar esta surtido?.'))location.href='php_ajax/cancela_surtido.php?surtido=<?php echo $datos[$i]['id'];?>'"  title="Click aqui para cancelar el surtido.">
						<?php 
							}
						}
						else {?>
								<img src="../../imagenes/borrar.jpg"  title="Este surtido se encuentra cancelado.">
						<?php }?>
				</td>
			</tr>
		<?php
			}
		?>
		</tbody>
	</table>

</form>
</body>
</html>
