<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$idOrdenCompra="";
if(isset($_GET['idOrdenCompra']))
	$idOrdenCompra = $_GET['idOrdenCompra'];

$idProveedor="";
if(isset($_GET['idProveedor']))
	$idProveedor = $_GET['idProveedor'];
	
$entro=false;
$datos= array();
if($conexion)
{
	$command= "SELECT c.surtido, a.orden,  a.prov, convert(varchar, a.falta, 103) as falta, b.nomprov , c.observa
				FROM compramsurtido c 
				LEFT JOIN compramordenes a on c.orden=a.orden
				LEFT JOIN compramprovs b on a.prov=b.prov ";
	if(strlen($idOrdenCompra)>0)
	{
		$command.= " WHERE a.orden = '$idOrdenCompra'";
		$entro= true;
		if(strlen($idProveedor)>0)
		{
			$command.= " AND a.prov=$idProveedor";
			$entro= true;
		}
	}
	else
	{
		if(strlen($idProveedor)>0)
		{
			$command.= " WHERE  a.prov=$idProveedor";
			$entro= true;
		}
	}
	
	if($entro == true)
		$command.= "  AND a.estatus<9000";
	else	
		$command.= "  WHERE a.estatus<9000";

	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$datos[$i]['id']= trim($row['surtido']);
			$datos[$i]['observa']= trim($row['observa']);
			
			$datos[$i]['orden']= trim($row['orden']);
			$datos[$i]['nomprov']= trim($row['nomprov']);
			$datos[$i]['falta']= trim($row['falta']);
			$i++;
		}
	}
	echo json_encode($datos);
}
?>