<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$idOrdenCompra="";
if(isset($_GET['idOrdenCompra']))
	$idOrdenCompra = $_GET['idOrdenCompra'];

$idProveedor="";
if(isset($_GET['idProveedor']))
	$idProveedor = $_GET['idProveedor'];
	
$entro=false;
$datos= array();
if($conexion)
{
	$command= "SELECT a.orden, SUM(d.sdocant) as sdocant, SUM(d.sdototal) as sdototal,a.prov, b.nomprov,convert(varchar, a.falta, 103) as falta, c.observa 
				FROM compramordenes a 
				LEFT JOIN compramprovs b on a.prov=b.prov 
				LEFT JOIN compramcuadroc c on a.cuadroc=c.cuadroc
				LEFT JOIN compradcuadroc d on d.cuadroc=c.cuadroc";
	if(strlen($idOrdenCompra)>0)
	{
		$command.= " WHERE a.orden = '$idOrdenCompra'";
		$entro= true;
		if(strlen($idProveedor)>0)
		{
			$command.= " AND a.prov=$idProveedor";
			$entro= true;
		}
	}
	else
	{
		if(strlen($idProveedor)>0)
		{
			$command.= " WHERE  a.prov=$idProveedor";
			$entro= true;
		}
	}
	
	if($entro == true)
		$command.= "  AND a.saldo>0 AND a.estatus=0";
	else	
		$command.= "  WHERE a.saldo>0 AND a.estatus=0";

	$command.=" GROUP BY a.orden,  b.nomprov, a.prov, a.falta, c.observa";
	
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."<br>". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//echo $row['falta'];
			if((float)$row['sdocant']>0.0 && (float)$row['sdocant']>0.0)
			{
				$datos[$i]['id']= trim($row['orden']);
				$datos[$i]['observa']= utf8_encode(trim($row['observa']));
				$datos[$i]['prov']= utf8_encode(trim($row['nomprov']));
				$datos[$i]['falta']= trim($row['falta']);
				$i++;
			}
		}
	}
	echo json_encode($datos);
}
?>