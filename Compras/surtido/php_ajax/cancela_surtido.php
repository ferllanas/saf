<?php
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	
	if(!isset($_COOKIE['ID_my_site']) || strlen($_COOKIE['ID_my_site'])<0)
		die("No existe un usuario logeado. Intente salir del sistema y registrase en la pagina de inicio.");

	$usuario = $_COOKIE['ID_my_site'];
	
	$surtido="";
	if(isset($_REQUEST['surtido']))
		$surtido=$_REQUEST['surtido'];
	//echo $surtido.", ".$usuario;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="{call sp_compras_B_surtido(?,?)}";//Arma el procedimeinto almacenado
	//func_creaProductoDRequisicion($id,$productos[$i]['producto'], $productos[$i]['cantidad'], $productos[$i]['descripcion'],'0',$productos[$i]['uniprod'])
	// @requisi numeric, @prod numeric, @descrip varchar(200), @cant numeric(13,2),@mov numeric, @unidad varchar(10)
	$params = array(&$surtido, &$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	//print_r($params);
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);

	$fails=false;
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
			
		 die(  print_r($params)."". print_r( sqlsrv_errors(), true));
	}
	
	if(!$fails)
	{
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$resp = $row['mensaje'];
			if(trim($resp)=="Surtido CANCELADO")
			{
				echo "El surtido ".$surtido." ha sido cancelado.";
			}
		}
	}
	
	
?>