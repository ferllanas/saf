<?php
require_once("../../../connections/dbconexion.php");
require_once("../../../dompdf/dompdf_config.inc.php");
include('../../../Administracion/globalfuncions.php');

$usarioCreador = $_COOKIE['ID_my_site'];	

$dsurtido= array();
$dTotales= array();
$dTotales[0]['subtotal']= 0.0;
$dTotales[0]['subtotalivapct']=0.0;
$dTotales[0]['total']= 0.0;


$id_dsurtido=$_REQUEST['id_dsurtido'];
if(isset($_REQUEST['id_dsurtido']))
	$id_dsurtido=$_REQUEST['id_dsurtido'];
									

/// conexion a base de datos
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
	
$sqlcommand="SELECT c.prov, g.nomprov , CONVERT(char(12),a.falta,103) falta ,a.orden, e.prod, 
			f.nomprod, e.descrip, b.cantidad, d.ivapct, a.observa ,
			d.check1, d.check2, d.check3, d.check4, d.precio1, d.precio2, d.precio3 , d.precio4
FROM compramsurtido a 
	INNER JOIN compradsurtido b ON a.surtido=b.surtido 
	INNER JOIN compramordenes c ON a.orden=c.orden 
	INNER JOIN compradcuadroc d ON d.id=b.iddcuadroc
	INNER JOIN compradrequisi e ON d.iddrequisi=e.id
	INNER JOIN compradproductos f ON f.prod=e.prod
	INNER JOIN compramprovs g ON g.prov=c.prov
	WHERE a.surtido=$id_dsurtido ";//AND a.estatus=0
$stmt = sqlsrv_query( $conexion,$sqlcommand);
if ( $stmt === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	 die( print_r( sqlsrv_errors(), true));
}
else
{
	

	$i=0;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	{
		
		$orden= trim($row['orden']);
		$forden= $row['falta'];
		$prov = trim($row['prov']);
		$nomprov= trim($row['nomprov']);
		$observa= utf8_decode(trim($row['observa']));

		$dsurtido[$i]['nomprod']="";
		if(strlen($row['nomprod'])>0)
			$dsurtido[$i]['nomprod'].=ascii_to_entities(trim($row['nomprod']));

		if(strlen($row['descrip'])>0)
			$dsurtido[$i]['nomprod'].=", ".ascii_to_entities(trim($row['descrip']));

		//echo "<br>".$dsurtido[$i]['nomprod']."!";
		$dsurtido[$i]['ivapct']= (double)$row['ivapct'];
		$dsurtido[$i]['cantidad']= (double)$row['cantidad'];
	
		if((int)$row['check1']==1)
			$dsurtido[$i]['precio']= (double)($row['precio1']);

		if((int)$row['check2']==1)
			$dsurtido[$i]['precio']= (double)($row['precio2']);

		if((int)$row['check3']==1)
			$dsurtido[$i]['precio']= (double)($row['precio3']);

		if((int)$row['check4']==1)
			$dsurtido[$i]['precio']= (double)($row['precio4']);
		
		$dTotales[0]['subtotal']= 		$dTotales[0]['subtotal']+ 		($dsurtido[$i]['cantidad'] * $dsurtido[$i]['precio']);
		$dTotales[0]['subtotalivapct']= $dTotales[0]['subtotalivapct']+	($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 0+($dsurtido[$i]['ivapct'] / 100))));
		$dTotales[0]['total']= 			$dTotales[0]['total']+ 			($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 1+($dsurtido[$i]['ivapct'] / 100))));

		$dsurtido[$i]['subtotal'] = number_format(redondear_dos_decimal(($dsurtido[$i]['cantidad'] * $dsurtido[$i]['precio'])),2);
		$dsurtido[$i]['total'] = number_format(redondear_dos_decimal($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 1+($dsurtido[$i]['ivapct'] / 100)))),2);
		$dsurtido[$i]['ivapct'] = number_format(redondear_dos_decimal($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 0+($dsurtido[$i]['ivapct'] / 100)))),2);
		$i++;
	}
	//Formato a Totales
	/*echo $dTotales[0]['subtotal'];
	echo "<br>".$dTotales[0]['subtotalivapct'];
	echo  "<br>".$dTotales[0]['total'];*/
	$dTotales[0]['subtotal']= 		number_format(redondear_dos_decimal(	$dTotales[0]['subtotal']		), 2);
	$dTotales[0]['subtotalivapct']= number_format(redondear_dos_decimal(	$dTotales[0]['subtotalivapct']	), 2);
	$dTotales[0]['total']=			number_format(redondear_dos_decimal(	$dTotales[0]['total']			), 2);
	
	$nombreUsuario="";
	$sqlcommand=" SELECT nombre, appat, apmat FROM nomemp.dbo.nominadempleados WHERE numemp COLLATE DATABASE_DEFAULT = (SELECT usuario FROM compramsurtido WHERE surtido=$id_dsurtido)";
	$stmt = sqlsrv_query( $conexion,$sqlcommand);
	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		 die( print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$nombreUsuario=trim($row['nombre'])." ".trim($row['appat'])." ".trim($row['apmat']);
		}
	}

	//if(count($dsurtido)>0)
	$HTML = creapdfPaImprimir($orden,$forden,$prov ,$nomprov,$observa,$dsurtido,$id_dsurtido,$nombreUsuario,$dTotales);
	$dompdf = new DOMPDF();
	$dompdf->set_paper('letter', "landscape");
	$dompdf->load_html($HTML);
	$dompdf->render();
	$pdf = $dompdf->output(); 
	
	$tmpfile = tempnam(".", "dompdf_.pdf");
	file_put_contents($tmpfile, $pdf ); // Replace $smarty->fetch()
	rename($tmpfile,$tmpfile.".pdf");
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment; filename="Surtidos"');
	header("Location: ".basename($tmpfile.".pdf"));
	
	$files = glob('*.tmp.pdf'); // get all file names
	foreach($files as $file){ // iterate files
	  if(is_file($file) && basename($tmpfile.".pdf")!= basename($file))
		unlink($file); // delete file
	}
	/*$dir = '../pdf_files';
	if(!file_exists($dir))
	{
		mkdir($dir, 0777);
	}
	//ile_put_contents("../pdf_files/".$idReq.".pdf", $pdf);
	file_put_contents( $dir.'/acusedsurtido_'.$id_dsurtido.'.pdf', $pdf);//"../pdf_files/".$idReq.".pdf");
	
	//$dompdf->stream("../pdf_files/".$idReq.".pdf");// $domPDF->stream($pdf, array('Attachment'=>0));
	$datos[0]['path']=$dir.'/acusedsurtido_'.$id_dsurtido.'.pdf';
	$datos[0]['fallo']=false;
	$datos[0]['id']=$id_dsurtido;
	*/
	//echo json_encode($datos);
	//echo "OK ALLRIGHT";
	/*$docPDF =& new Cezpdf('LATTER','landscape');//
	$docPDF->selectFont('../../../fonts/arial.afm');
	$datacreator = array ('Title'=>'Ordenes de compra de Cuador comparativo',
							'Author'=>'Fomerrey',
							'Subject'=>'Ordenes de compra de Cuador comparativo',
							'Creator'=>'fernando.llanas@fomerrey.gob.mx',
							'Producer'=>'http://blog.unijimpe.net');
	$docPDF->addInfo($datacreator);
	$docPDF->ezSetCmMargins(1,0.5,0.5,0.5);
	
	$pdfcode = $docPDF->output();
	$dir = '../pdf_files';
	if(!file_exists($dir)){
		mkdir($dir, 0777);
	}
	
	$fname = $dir.'/acusedsurtido_'.$id_dsurtido.'.pdf';
	$fp = fopen($fname, 'w');
	fwrite($fp,$pdfcode);
	fclose($fp);*/
	
}


function creapdfPaImprimir($orden,$forden,$prov ,$nomprov,$observa,$dsurtido,$id_dsurtido,$usarioCreador, $dTotales)
{	
	global $server,$odbc_name,$username_db ,$password_db, $imagenPDFPrincipal, $NomCompletodependencia;
	
$numCalreqiosi=fun_ObtieneNumCalidadReq();

$pageheadfoot='<script type="text/php"> 
if ( isset($pdf) ) 
{ 	$textod=utf8_encode("P�gina {PAGE_NUM}");
	$textop=utf8_encode("Surtido");
	$pdf->page_text(25, 580, "$textop '.$id_dsurtido.'", "", 12, array(0,0,0)); 
	$pdf->page_text(650, 580, "Pagina {PAGE_NUM}", "", 12, array(0,0,0)); 
	
	
 } 
</script> '; //$pdf->page_text(650, 592, "MO.2000.2044.7", "", 12, array(0,0,0)); 12062013

$html="<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv='Content-Type' content='text/html; charset=UTF8'>
<style>
		body 
		{
			font-family:'Arial';
			font-size:9;
			margin:  0.25in 0.5in 0.5in 0.5in;
		}

		#twmaarco
		{
			border-collapse:collapse;
		}
		
		#twmaarco td
		{
			font-size:8;
			border:1pt solid black;
			border-collapse:collapse;
			padding:2px 2px 2px 2px; 
		}

		#twmaarco th
		{
			font-size:8;
			border:1pt solid black;
			border-collapse:collapse;
			padding:2px 2px 2px 2px; 
		}
		#twmaarco tfoot td
		{
			font-size:8;
			border:0pt solid black;
			border-collapse:collapse;
			padding:2px 2px 2px 2px; 
		}

		#twmaarco tr.d0 td {
			background-color: #FFFFFF;
		}
		#twmaarco tr.d1 td {
			background-color: #EAEAEA;
		}

		td {padding: 0;}
		
		

		.textobold{
			font-weight:bold;
		}
		
		.texto16b{ 
			font-size:16pt;
			font-weight:bold;
		}
		.texto14{ 
			font-size:14pt;
		}
		.texto14b{ 
			font-size:15pt;
			font-weight:bold;
		}
		
		.texto12bold
		{
			font-size:12pt;
			font-weight:bold;

		}
		.texto12{ 
			font-size:12pt;
		}
		.texto11{
			font-size:11pt;
		}
		.texto10{
			font-size:10pt;
		}
		</style>

</head>
<body>
<table width='100%'>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='20%' align='left'><img src='".getDirAtras(getcwd())."".$imagenPDFPrincipal."' style='width:150px; height:57px'></td>
					<td width='60%' align='center'>
						<table width='100%' align='center'>
							<tr>
								<td align='center' class='texto14b' width='100%'><b>".$NomCompletodependencia."</b></td>
							</tr>
							<tr>
								<td align='center' class='texto14b' width='100%'><b>".utf8_encode('Direcci�n de Administraci�n y Finanzas')."</b></td>
							</tr>
						</table>
					</td>
					<TD align='right' class='texto12' width='20%'>$numCalreqiosi</TD>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align='left' class='texto14' >".utf8_encode("Surtido de Materiales")." No. $id_dsurtido</td>
	</tr>
	<tr>
		<td align='right' class='texto10' ><strong>Fecha:</strong> $forden</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='20%' align='left'>Orden de Compra: $orden </td>
					<td width='80%' align='right'>Proveedor: $nomprov</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%' name='twmaarco' id='twmaarco'>
				<thead>
					<tr>
						<th>Producto</th>
						<th>Cantidad</th>
						<th>Precio</th>
						<th>Sub-Total</th>
						<th>I.V.A.</th>
						<th>Total</th>
					</tr>
				</thead>
				
				<tbody>";
				for($i=0;$i<count($dsurtido);$i++)
				{
					$html.="<tr class='d".($i%2)."'>
						<td>".$dsurtido[$i]['nomprod']."</td>
						<td align='center'>".$dsurtido[$i]['cantidad']."</td>
						<td align='center'>$".$dsurtido[$i]['precio']."</td>
						<td align='center'>$".$dsurtido[$i]['subtotal']."</td>
						<td align='center'>$".$dsurtido[$i]['ivapct']."</td>
						<td align='center'>$".$dsurtido[$i]['total']."</td>
					</tr>";
				}


	$html.="</tbody>
				<tfoot>
					<tr > 
						<td colspan=3>&nbsp;</td>
						<td align='center'>$".$dTotales[0]['subtotal']."</td>
						<td align='center'>$".$dTotales[0]['subtotalivapct']."</td>
						<td align='center'>$".$dTotales[0]['total']."</td>
					</tr>
				</tfoot></table >
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		</td>
	</tr>
	<tr>
		<td width='100%'>
			<table width='100%'>
				<tr>
					<td width='30%'></td>
					<td width='40%' align='center'><hr><br>Nombre, Firma quien recibio<br>$usarioCreador</td>
					<td width='30%'></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
".$pageheadfoot."
</body>
</html>";

return $html;
/*	//http://localhost/fome/imagenes/encabezadoFnuevo.jpg
	//$docPDF->addJpegFromFile('../../../imagenes/encabezadoFnuevo.jpg',  20, 800, 100,300);
	$docPDF->addJpegFromFile(getDirAtras(getcwd())."".$imagenPDFPrincipal, 20, 530, 148.5, 57, array('gap'));
	//ob_end_clean(); 
	//$docPDF->addJpegFromFile('../../../'.$imagenPDFSecundaria,460, 940, 100.5, 57,array('gap'));
	//ob_end_clean(); //opcional
	//Arma fecha
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	//$hora_req = "";
	//$fecha_req = "";
	//list($month,$days,$years)=split('[/.-]', $forden);
	//list($horayseg,$milseg)=split('[:]', $horden);
	//echo $horayseg.",".$milseg;
	$dateX=date('d-m-Y');;
	//$diadesem=date("w");
	
	$docPDF->setColor(0, 0 ,0);
	$docPDF->ezText("Fomento Metropolitano de Monterrey",18,array('justification'=>'center'));
	$docPDF->ezText("Direcci�n de Administraci�n y Finanzas",14,array('justification'=>'center'));
	$docPDF->ezText("Surtido de Materiales: ".$id_dsurtido,18,array('justification'=>'left'));
	$docPDF->ezText("Fecha:             $forden",10,array('justification'=>'right'));
	$docPDF->ezText("<b>Orden de Compra</b>: $orden                  <b>Proveedor<b>:  $nomprov\n",12,array('justification'=>'left'));


	unset ($opciones_tabla);
	//// mostrar las lineas
	$opciones_tabla['showLines']=1;
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=1;
	//// lineas sombreadas
	$opciones_tabla['shaded']= 1;
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 8;

	$opciones_tabla['width'] = "100";

	$titles = array('nomprod'=>'<b>Producto</b>',
					'cantidad'=>'<b>Cantidad</b>', 
					'precio'=>'<b>Precio</b>',
					'subtotal'=>'<b>Sub-Total</b>',
					'ivapct'=>'<b>I.V.A.</b>', 
					'total'=>'<b>Total</b>',
					);
	
	  
	
	$opciones_tabla['cols'] = array( 'nomprod' => array('justification' => 'left', 'width'=>'400' ), 
									"cantidad" => array('justification' => 'center', 'width'=>'50'), 
									"precio" => array('justification' => 'left', 'width'=>'60'),
									"subtotal" => array('justification' => 'center', 'width'=>'70'),
									"ivapct" => array('justification' => 'left', 'width'=>'50' ), 
									"total" => array('justification' => 'center', 'width'=>'70')
										);
	$docPDF->ezTable($dsurtido,$titles,"",$opciones_tabla); 	
	
/////////////////////////////
	//Tabla de totales
	/////////////////////////////
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=0;


	//echo $totalImporte;
	$datos_totales=array(  array( 'nomprod'=>'',
								'cantidad'=>'', 
								'precio'=>'',
								'subtotal'=>$dTotales[0]['subtotal'],
								'ivapct'=>$dTotales[0]['subtotalivapct'], 
								'total'=>$dTotales[0]['total']
							));
	$docPDF->ezTable($datos_totales,$titles,"",$opciones_tabla);  
	
	
	///Firma de Recibido
	$docPDF->ezText("\n" ,10);
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=0;
	$opciones_tabla['shaded']= 0;
	$opciones_tabla['width'] = "800";
	$opciones_tabla['xPos'] = 415;
	$opciones_tabla['fontSize']= 10;
	$opciones_tabla['cols'] = array( 'recibido' => array('justification' => 'center', 'width'=>'200' ));
	$titlesfirmas = array('recibido'=>'<b>Recibido Por</b>');
	$datosfirmas=array( array("recibido"=>"______________________________"),
						array("recibido"=>"Nombre, Firma quien recibio \n$usarioCreador"),//
						);
	$docPDF->ezTable($datosfirmas,$titlesfirmas,"",$opciones_tabla); 
	//Numero de calidad
	$docPDF->ezText("" ,12,array('justification'=>'right'));
	
	$docPDF->ezText("$numCalreqiosi" ,12,array('justification'=>'right'));
	//$docPDF->ezText("<b>Proveedor<b>: $nomprov 							<b>Orden de Compra</b>: $orden\n",10,array('justification'=>'left'));*/
}




//Funcion que llama procedimiento que obtiene codigo de requisicion
function fun_ObtieneNumCalidadReq()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=13;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}


function fun_ObtieneCabezalesSolicitante($usuario)
{
global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
			
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto COLLATE DATABASE_DEFAULT where numemp=$usuario";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			$depto = $row['depto'];
			$coord = $row['coord'];
			$dir = $row['dir'];
			$nomdepto = $row['nomdepto'];
			$nomdir = $row['nomdir'];
			//$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($depto,$coord ,$dir,$nomdepto ,$nomdir );

}

?>
