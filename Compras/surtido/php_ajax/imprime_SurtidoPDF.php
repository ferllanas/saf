<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Ordenes de Cuenta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<?php
include('../../../pdf/class.ezpdf.php');
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
$usarioCreador = $_COOKIE['ID_my_site'];	

$dsurtido= array();
$dTotales= array();
$dTotales[0]['subtotal']= 0.0;
$dTotales[0]['subtotalivapct']=0.0;
$dTotales[0]['total']= 0.0;


$id_dsurtido=$_REQUEST['id_dsurtido'];
if(isset($_REQUEST['id_dsurtido']))
	$id_dsurtido=$_REQUEST['id_dsurtido'];
									

/// conexion a base de datos
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
	
$sqlcommand="SELECT c.prov, g.nomprov , CONVERT(char(12),a.falta,103) falta ,a.orden, e.prod, 
			f.nomprod, e.descrip, b.cantidad, d.ivapct, a.observa ,
			d.check1, d.check2, d.check3, d.check4, d.precio1, d.precio2, d.precio3 , d.precio4
FROM compramsurtido a 
	INNER JOIN compradsurtido b ON a.surtido=b.surtido 
	INNER JOIN compramordenes c ON a.orden=c.orden 
	INNER JOIN compradcuadroc d ON d.id=b.iddcuadroc
	INNER JOIN compradrequisi e ON d.iddrequisi=e.id
	INNER JOIN compradproductos f ON f.prod=e.prod
	INNER JOIN compramprovs g ON g.prov=c.prov
	WHERE a.surtido=$id_dsurtido AND a.estatus=0";
$stmt = sqlsrv_query( $conexion,$sqlcommand);
if ( $stmt === false)
{ 
	$resoponsecode="02";
	$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
	 die( print_r( sqlsrv_errors(), true));
}
else
{
	//echo "OK ALLRIGHT";
	$docPDF =& new Cezpdf('LATTER','landscape');//
	$docPDF->selectFont('../../../fonts/arial.afm');
	$datacreator = array ('Title'=>'Ordenes de compra de Cuador comparativo',
							'Author'=>'Fomerrey',
							'Subject'=>'Ordenes de compra de Cuador comparativo',
							'Creator'=>'fernando.llanas@fomerrey.gob.mx',
							'Producer'=>'http://blog.unijimpe.net');
	$docPDF->addInfo($datacreator);
	$docPDF->ezSetCmMargins(1,0.5,0.5,0.5);
	

	$i=0;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
	{
		
		$orden= trim($row['orden']);
		$forden= $row['falta'];
		$prov = trim($row['prov']);
		$nomprov= utf8_encode(trim($row['nomprov']));
		$observa= utf8_encode(trim($row['observa']));

		$dsurtido[$i]['nomprod']= utf8_encode(trim($row['nomprod'])).", ".utf8_encode(trim($row['descrip']));
		$dsurtido[$i]['ivapct']= (double)$row['ivapct'];
		$dsurtido[$i]['cantidad']= (double)$row['cantidad'];
	
		if((int)$row['check1']==1)
			$dsurtido[$i]['precio']= (double)($row['precio1']);

		if((int)$row['check2']==1)
			$dsurtido[$i]['precio']= (double)($row['precio2']);

		if((int)$row['check3']==1)
			$dsurtido[$i]['precio']= (double)($row['precio3']);

		if((int)$row['check4']==1)
			$dsurtido[$i]['precio']= (double)($row['precio4']);
		
		$dTotales[0]['subtotal']= 		$dTotales[0]['subtotal']+ 		($dsurtido[$i]['cantidad'] * $dsurtido[$i]['precio']);
		$dTotales[0]['subtotalivapct']= $dTotales[0]['subtotalivapct']+	($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 0+($dsurtido[$i]['ivapct'] / 100))));
		$dTotales[0]['total']= 			$dTotales[0]['total']+ 			($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 1+($dsurtido[$i]['ivapct'] / 100))));

		$dsurtido[$i]['subtotal'] = number_format(redondear_dos_decimal(($dsurtido[$i]['cantidad'] * $dsurtido[$i]['precio'])),2);
		$dsurtido[$i]['total'] = number_format(redondear_dos_decimal($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 1+($dsurtido[$i]['ivapct'] / 100)))),2);
		$dsurtido[$i]['ivapct'] = number_format(redondear_dos_decimal($dsurtido[$i]['cantidad'] * ($dsurtido[$i]['precio'] * ( 0+($dsurtido[$i]['ivapct'] / 100)))),2);
		$i++;
	}
	//Formato a Totales
	/*echo $dTotales[0]['subtotal'];
	echo "<br>".$dTotales[0]['subtotalivapct'];
	echo  "<br>".$dTotales[0]['total'];*/
	$dTotales[0]['subtotal']= 		number_format(redondear_dos_decimal(	$dTotales[0]['subtotal']		), 2);
	$dTotales[0]['subtotalivapct']= number_format(redondear_dos_decimal(	$dTotales[0]['subtotalivapct']	), 2);
	$dTotales[0]['total']=			number_format(redondear_dos_decimal(	$dTotales[0]['total']			), 2);
	
	$nombreUsuario="";
	$sqlcommand=" SELECT nombre, appat, apmat FROM nominadempleados WHERE numemp = (SELECT usuario FROM compramsurtido WHERE surtido=$id_dsurtido)";
	$stmt = sqlsrv_query( $conexion,$sqlcommand);
	if ( $stmt === false)
	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		 die( print_r( sqlsrv_errors(), true));
	}
	else
	{
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$nombreUsuario=utf8_encode(trim($row['nombre']))." ".utf8_encode(trim($row['appat']))." ".utf8_encode(trim($row['apmat']));
		}
	}

	//if(count($dsurtido)>0)
		creapdfPaImprimir($docPDF,$orden,$forden,$prov ,$nomprov,$observa,$dsurtido,$id_dsurtido,$nombreUsuario,$dTotales);


	$pdfcode = $docPDF->output();
	$dir = '../pdf_files';
	if(!file_exists($dir)){
		mkdir($dir, 0777);
	}
	
	$fname = $dir.'/acusedsurtido_'.$id_dsurtido.'.pdf';
	$fp = fopen($fname, 'w');
	fwrite($fp,$pdfcode);
	fclose($fp);
	
}


function creapdfPaImprimir($docPDF,$orden,$forden,$prov ,$nomprov,$observa,$dsurtido,$id_dsurtido,$usarioCreador, $dTotales)
{	
	global $server,$odbc_name,$username_db ,$password_db;
	global $imagenPDFPrincipal, $imagenPDFSecundaria;

	//http://localhost/fome/imagenes/encabezadoFnuevo.jpg
	//$docPDF->addJpegFromFile('../../../imagenes/encabezadoFnuevo.jpg',  20, 800, 100,300);
	$docPDF->addJpegFromFile(getDirAtras(getcwd())."".$imagenPDFPrincipal, 20, 530, 148.5, 57, array('gap'));
	//ob_end_clean(); 
	//$docPDF->addJpegFromFile('../../../'.$imagenPDFSecundaria,460, 940, 100.5, 57,array('gap'));
	//ob_end_clean(); //opcional
	//Arma fecha
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	//$hora_req = "";
	//$fecha_req = "";
	//list($month,$days,$years)=split('[/.-]', $forden);
	//list($horayseg,$milseg)=split('[:]', $horden);
	//echo $horayseg.",".$milseg;
	$dateX=date('d-m-Y');;
	//$diadesem=date("w");
	
	$docPDF->setColor(0, 0 ,0);
	$docPDF->ezText("".$NomCompletodependencia."",18,array('justification'=>'center'));
	$docPDF->ezText("Direcci�n de Administraci�n y Finanzas",14,array('justification'=>'center'));
	$docPDF->ezText("Surtido de Materiales: ".$id_dsurtido,18,array('justification'=>'left'));
	$docPDF->ezText("Fecha:             $forden",10,array('justification'=>'right'));
	$docPDF->ezText("<b>Orden de Compra</b>: $orden                  <b>Proveedor<b>:  $nomprov\n",12,array('justification'=>'left'));


	unset ($opciones_tabla);
	//// mostrar las lineas
	$opciones_tabla['showLines']=1;
	//// mostrar las cabeceras
	$opciones_tabla['showHeadings']=1;
	//// lineas sombreadas
	$opciones_tabla['shaded']= 1;
	//// tama�o letra del texto
	$opciones_tabla['fontSize']= 8;

	$opciones_tabla['width'] = "100";

	$titles = array('nomprod'=>'<b>Producto</b>',
					'cantidad'=>'<b>Cantidad</b>', 
					'precio'=>'<b>Precio</b>',
					'subtotal'=>'<b>Sub-Total</b>',
					'ivapct'=>'<b>I.V.A.</b>', 
					'total'=>'<b>Total</b>',
					);
	
	  
	
	$opciones_tabla['cols'] = array( 'nomprod' => array('justification' => 'left', 'width'=>'400' ), 
									"cantidad" => array('justification' => 'center', 'width'=>'50'), 
									"precio" => array('justification' => 'left', 'width'=>'60'),
									"subtotal" => array('justification' => 'center', 'width'=>'70'),
									"ivapct" => array('justification' => 'left', 'width'=>'50' ), 
									"total" => array('justification' => 'center', 'width'=>'70')
										);
	$docPDF->ezTable($dsurtido,$titles,"",$opciones_tabla); 	
	
/////////////////////////////
	//Tabla de totales
	/////////////////////////////
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=0;


	//echo $totalImporte;
	$datos_totales=array(  array( 'nomprod'=>'',
								'cantidad'=>'', 
								'precio'=>'',
								'subtotal'=>$dTotales[0]['subtotal'],
								'ivapct'=>$dTotales[0]['subtotalivapct'], 
								'total'=>$dTotales[0]['total']
							));
	$docPDF->ezTable($datos_totales,$titles,"",$opciones_tabla);  
	
	
	///Firma de Recibido
	$docPDF->ezText("\n" ,10);
	$opciones_tabla['showLines']=0;
	$opciones_tabla['showHeadings']=0;
	$opciones_tabla['shaded']= 0;
	$opciones_tabla['width'] = "800";
	$opciones_tabla['xPos'] = 415;
	$opciones_tabla['fontSize']= 10;
	$opciones_tabla['cols'] = array( 'recibido' => array('justification' => 'center', 'width'=>'200' ));
	$titlesfirmas = array('recibido'=>'<b>Recibido Por</b>');
	$datosfirmas=array( array("recibido"=>"______________________________"),
						array("recibido"=>"Nombre, Firma quien recibio \n$usarioCreador"),//
						);
	$docPDF->ezTable($datosfirmas,$titlesfirmas,"",$opciones_tabla); 
	//Numero de calidad
	$docPDF->ezText("" ,12,array('justification'=>'right'));
	$numCalreqiosi=fun_ObtieneNumCalidadReq();
	$docPDF->ezText("$numCalreqiosi" ,12,array('justification'=>'right'));
	//$docPDF->ezText("<b>Proveedor<b>: $nomprov 							<b>Orden de Compra</b>: $orden\n",10,array('justification'=>'left'));
}




//Funcion que llama procedimiento que obtiene codigo de requisicion
function fun_ObtieneNumCalidadReq()
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;

	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$codigo=13;
	$tsql_callSP ="{call sp_config_C_codigo(?)}";//Arma el procedimeinto almacenado
	$params = array(&$codigo);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);//
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$descrip ="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			$descrip = $row['descrip'];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return $descrip;
}


function fun_ObtieneCabezalesSolicitante($usuario)
{
global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	$depto = "";
	$coord = "";
	$dir = "";
	$nomdepto = "";
	$nomdir = "";
			
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$tsql_callSP ="select a.depto,a.coord, a.dir,b.nomdepto,b.nomdir  from nominadempleados a left join nominamdepto b on a.depto=b.depto where numemp=$usuario";//Arma el procedimeinto almacenado
	//$params = array(&$usuario);//Arma parametros de entrada
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
	
	$stmt = sqlsrv_query($conexion, $tsql_callSP);//, $params
	if( $stmt === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true));
	}
	
	$autoriza ="";
	$VoBo="";
	if(!$fails)
	{
		//echo "lalalalal";
		// Retrieve and display the first result. 
		//$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
			//echo  $row['AUTORIZA'];
			$depto = $row['depto'];
			$coord = $row['coord'];
			$dir = $row['dir'];
			$nomdepto = $row['nomdepto'];
			$nomdir = $row['nomdir'];
			//$i++;
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
	return array($depto,$coord ,$dir,$nomdepto ,$nomdir );

}

?>
<body>
<embed src='<?php echo $fname;?>#toolbar=1&navpanes=0&scrollbar=1' width='100%' height='800px'>
</body>
</html>
