<?php
require_once("../../connections/dbconexion.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
					
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$llenarAPartirdeidSurtido=false;
$idSurtido="";
$idOrdenCompra="";
if(isset($_GET['idorden']))
	$idOrdenCompra = $_GET['idorden'];
	
if(isset($_GET['idSurtido']))
{	$idSurtido = $_GET['idSurtido'];
	$llenarAPartirdeidSurtido=true;
}	
$observa_surtido="";
$calAlmacen= array();


$prov="";
$ordenprov= "";
$datos= array();
			
$command="";
if($conexion)
{
	//Productos de orden de compra
	if($llenarAPartirdeidSurtido==true)
	{
		
		$command= "select  a1.prov , a1.ordenprov, a1.estatus, a1.orden from compramordenes a1
		left join compradcuadroc a on a1.cuadroc = a.cuadroc 
		left join compramsurtido c ON c.orden= a1.orden
		where c.surtido=$idSurtido";
					/*select b.prod, c.nomprod, b.descrip, c.ctapresup, a1.prov , a1.ordenprov, a1.estatus,  a.*
					from compramordenes a1
					left join compradcuadroc a on a1.cuadroc=a.cuadroc
					left join compradrequisi b on a.iddrequisi=b.id
					left join compradproductos c on b.prod=c.prod where a1.orden==";*/
	}
	else
	{
		$command= "select  a1.prov , a1.ordenprov, a1.estatus from compramordenes a1
		left join compradcuadroc a on a1.cuadroc=a.cuadroc where a1.orden=$idOrdenCompra";
	}

	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//echo $row['falta'];
			$prov= trim($row['prov']);
			$ordenprov= trim($row['ordenprov']);
			$estusordencompra= trim($row['estatus']);
			if($llenarAPartirdeidSurtido==true)
				$idOrdenCompra=trim($row['orden']);
			/*if( "1" == trim($row['cuadroc'.$datos[$i]['ordenprov']]))
			{
				$datos[$i]['id']= trim($row['cuadroc']);
				$datos[$i]['descrip']= trim($row['descrip']);
				$datos[$i]['iddrequisi']= trim($row['iddrequisi']);
				$datos[$i]['cantidad']= trim($row['cantidad']);
				$datos[$i]['nomprod']= trim($row['nomprod']);
				$datos[$i]['prov']= trim($row['prov']);
				$i++;
			}*/
		}
		
		$datos= func_ObtenerProdDorden($idOrdenCompra,$ordenprov,$prov);
	}
	
	//Obtiene los departamente para envio
	$calAlmacen= array();
	$command= "SELECT * FROM compramalmacen where estatus<9000";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//echo $row['falta'];
			$calAlmacen[$i]['id']= trim($row['id']);
			$calAlmacen[$i]['nombre']= trim($row['nombre']);
			$calAlmacen[$i]['esalmacen']= "si";
			$i++;
		}
	}
	
	$command= "SELECT * FROM configmccostos where estatus<9000";	
	//echo $command;
	$getProducts = sqlsrv_query( $conexion,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($getProducts);
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			//echo $row['falta'];
			$calAlmacen[$i]['id']= trim($row['ccostos']);
			$calAlmacen[$i]['nombre']= trim($row['descrip']);
			$calAlmacen[$i]['esalmacen']= "no";
			$i++;
		}
	}
}

function func_ObtenerProdDorden($orden,$ordenprov,$prov)
{
	global $server,$odbc_name,$username_db ,$password_db;
	
	$datos=array();
	//echo $server.",".$username_db.",".$password_db.",".$odbc_name;
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	
	$command = "select a.cuadroc, a.id, ltrim(rtrim(c.nomprod)) as nomprod, ltrim(rtrim(b.descrip)) as descrip,
				a.iddrequisi,a.cantidad, a.sdocant,  b.requisi,b.prod, 
				b.unidad,a.ivapct,a.precio".$ordenprov.",a.subtotal".$ordenprov.", d.ccostos
				from compradcuadroc a 
				left join compradrequisi   b on a.iddrequisi= b.id
				left join compramrequisi   d on d.requisi   = b.requisi
				left join compradproductos c on b.prod      = c.prod
				left join compramordenes   f on a.cuadroc   = f.cuadroc 
				where orden=$orden and check".$ordenprov."=1 AND a.sdocant>0";
				//echo $command;
	//echo $command;
	$stmt = sqlsrv_query( $conexion,$command);
	if ( $stmt === false)
   	{ 
		$resoponsecode="02";
		die($command."". print_r( sqlsrv_errors(), true));
	}
	else
	{
		$resoponsecode="Cantidad rows=".count($stmt);
		$i=0;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			
			$datos[$i]['id']= trim($row['id']);
			$datos[$i]['descrip']= trim($row['descrip']);
			$datos[$i]['iddrequisi']= trim($row['iddrequisi']);
			$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['nomprod']= trim($row['nomprod']);
			$datos[$i]['prov']= $prov;
			$datos[$i]['ccostos']= trim($row['ccostos']);
			$datos[$i]['sdocant']= trim($row['sdocant']);
			/*$datos[$i]['cantidad']= trim($row['cantidad']);
			$datos[$i]['unidad']= trim($row['unidad']);
			$datos[$i]['descprod']= trim($row['descprod']);
			$datos[$i]['precio']= trim($row['precio'.$ordenprov]);
			$datos[$i]['subtotal']= trim($row['subtotal'.$ordenprov]);
			//$datos[$i]['ivapct']=$row['ivapct'];
			$datos[$i]['iva']=$datos[$i]['subtotal']*$row['ivapct'];
			$datos[$i]['total']=$datos[$i]['subtotal']+$datos[$i]['iva'];			*/
			$i++;
		}
	}
	sqlsrv_close( $conexion);	
	return $datos;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Surtido de Orden de Compra</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/guardaeImprimesurtido.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
</head>
<body>
<?php 
	//if(count($datos)>0)
		//print_r($datos);
?>
<table width="100%">
	<tr>
		<td align="center" width="100%" class="TituloDForma">Surtido de Orden de Compra: <?php echo $idOrdenCompra;?>
  		  <hr class="hrTitForma"> 
			<input type="hidden" id="idOrdenCompra" name="idOrdenCompra" value="<?php echo $idOrdenCompra;?>">
			<input type="hidden" id="idprov" name="idprov" value="<?php if(count($datos)>0)echo $datos[0]['prov'];?>">
		</td>
	</tr>
	<tr>
		<td align="left" class="texto8" width="100%">
			<table width="100%" >
				<tr>
					<td width="6%">Observaciones:</td>
					<td width="94%" align="left"><input type="text" id="observa_surtido" name="observa_surtido" value="<?php echo $observa_surtido;?>"  style="width:95%"></td>
				</tr>
			</table
	></tr>
	<tr>
		<td>
			<table width="100%">
				<tr>
					<th width="25%" class="subtituloverde">Producto</th>
					<th width="35%" class="subtituloverde">Descripci&oacute;n</th>
					<th width="5%" class="subtituloverde">Cantidad</th>
					<th width="5%" class="subtituloverde">Cantidad Surtida</th>
					<th width="25%" class="subtituloverde">Almacena en</th>
					<th></th>
				</tr>
				<tbody id="prodAEntregar">
					<?php
						if( (int) $estusordencompra == 9000 )
						{
					?>
							<tr><td colspan="5" align="center" bgcolor="#FF6600" style="font-size:14px; font:Arial, Helvetica, sans-serif; color:#FFFFFF">No puede surtir esta orden de compra se encuentra cancelada.</td></tr>
					<?php	
						}
						else
							if(count($datos)<=0)
							{
					?>
							<tr><td colspan="5" align="center" bgcolor="#FF6600" style="font-size:14px; font:Arial, Helvetica, sans-serif; color:#FFFFFF">Ya se han surtidos los productos de esta orden de compra.</td></tr>
				<?php	
						}
						else
						for($i=0;$i<count($datos);$i++)
						{
					?>
						<tr>
							<td class='texto8' width="25%">
													<input type="hidden" id="iddcuadroc" name="iddcuadroc" value="<?php echo $datos[$i]['id'];?>">
													<?php echo $datos[$i]['nomprod']; ?>
							</td>
							<td class="texto8" width="35%"> <?php echo utf8_decode($datos[$i]['descrip']); ?></td>
							<td class='texto8' width="5%" align="center">
																<?php echo $datos[$i]['sdocant']; ?>
																<input type="hidden" name="cantidad" id="cantidad" value="<?php echo $datos[$i]['cantidad'];?>">
																<input type="hidden" name="sdocant"  id="sdocant"  value="<?php echo $datos[$i]['sdocant']; ?>"></td>
							<td width="5%"><input type='text' id='cantidadSurtida' name='cantidadSurtida'  style="width:100%" value="<?php echo $datos[$i]['sdocant']; ?>" class="texto8" onKeyPress="return aceptarSoloNumeros(this, event);" ></td>
							<td width="25%"><SELECT id="seAlmacenaEn" name="seAlmacenaEn" style="width:100%" class="texto8">
						<?php 
							for($j=0;$j<count($calAlmacen);$j++)
							{
								if($datos[$i]['ccostos']==$calAlmacen[$j]['id'])
									echo "<option value='".$calAlmacen[$j]['id'].",".$calAlmacen[$j]['esalmacen']."' title='".$calAlmacen[$j]['nombre'].",".$calAlmacen[$j]['esalmacen']."' SELECTED>".$calAlmacen[$j]['nombre']."</option>";
								else
									echo "<option value='".$calAlmacen[$j]['id'].",".$calAlmacen[$j]['esalmacen']."' title='".$calAlmacen[$j]['nombre'].",".$calAlmacen[$j]['esalmacen']."'>".$calAlmacen[$j]['nombre']."</option>";
							}
						?>
								</SELECT> 
							</td>
							<td></td>
						</tr>
					<?php	} ?>
				</tbody>
			</table>
		</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td align="center" width="100%"> 	<input type="button" onClick="GuardaSurtido();" value="Guardar">
		</td>
	</tr>
</table>
</body>
</html>
