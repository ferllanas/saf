<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml2/DTD/xhtml1-strict.dtd">
<?php
	require_once("connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
					date_default_timezone_set('America/Mexico_City');
?>
<html>
    <head>
        <title>Men&uacute; Izquierdo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta name="title" content="Fomerrey">
        <meta http-equiv="title" content="Fomerrey">
        <meta name="description" content="Fomerrey">
        <meta name="keywords" content="Fomerrey">
        <meta http-equiv="description" content="Fomerrey">
        <meta http-equiv="keywords" content="Fomerrey">
        
        <link type="text/css" href="css/estilos.css" rel="stylesheet">
        <script type="text/javascript" src="menuaccordion/jquery.min.js"></script>
        <script type="text/javascript" src="menuaccordion/ddaccordion.js">
            /***********************************************
            * Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
            * Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
            * This notice must stay intact for legal use
            ***********************************************/
        </script>
    
       
	    <script type="text/javascript">
        
            ddaccordion.init({
                headerclass: "headerbar", //Shared CSS class name of headers group
                contentclass: "submenu", //Shared CSS class name of contents group
                revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc] [] denotes no content
                onemustopen: true, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: false, //Should contents open by default be animated into view?
                persiststate: true, //persist state of opened contents within browser session?
                toggleclass: ["", "selected"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "normal", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            })
            
        </script> 
    
        <style type="text/css">    
            .urbangreymenu {
                width: 150px;
				 /*width of menu*/
				
            }
            
            .urbangreymenu .headerbar{
                font: bold 11px Arial, Verdana, Helvetica, sans-serif;
                color: white;
                background: #005200 /*url(IMAGEN/botones/arrowstop.gif)*/ no-repeat 2px 5px; /*last 2 values are the x and y coordinates of bullet image*/
                margin-bottom: 1px; /*bottom spacing between header and rest of content*/
                text-transform: none;
                padding: 5px 5px 5px 5px; /*31px is left indentation of header text*/
				border-bottom: 1px solid #FFFFFF; 
				
            }
            
            .urbangreymenu .headerbar a{
                text-decoration: none;
                color: white;
                display: block;
				
            }
            
            .urbangreymenu ul
			{
                list-style-type: none;               
				position: relative;
			    margin: 0;
                padding: 0;
                margin-bottom: 0; /*bottom spacing between each UL and rest of content*/
            }
            
            .urbangreymenu ul li{
                padding-bottom: 1px; /*bottom spacing between menu items*/
            }
            
            .urbangreymenu ul li a{
                font: normal 11px Arial, Verdana, Helvetica, sans-serif;
                color: black;
                background: #8BB381;
                display: block;
                padding: 5px 0;
                line-height: 15px;
                padding-left: 10px; /*link text is indented 8px*/
				padding-right: 5px;
                text-decoration: none;
				
            }
            
            .urbangreymenu ul li a:visited{
                color: black;
            }
            
            .urbangreymenu ul li a:hover{ /*hover state CSS*/
                color: white;
                font: bold;
                /*background: black;*/
                background:#8BB381;
            }
			ul#menu-horizontal li 
			{
				float: left;
				display: inline;
				position: relative;
			}
    
        </style>
    </head>
    <body>
	<div style="border:1; " class="texto8">
		<?php 
				$paso=true;
				$IdUsuario = $_COOKIE['ID_my_site']; 
		 		$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
				$conexion = sqlsrv_connect($server,$infoconexion);		 
				
		?>
	</div>
	<?php 		
		echo "<div class='urbangreymenu'>";
		//echo "<ul class='submenu'>";	
		
		if ($conexion)
		{ 
 			$consulta = "select a.*,datalength(rtrim(ltrim(a.nivel))) as nivel,b.descrip,b.programa, CONVERT(numeric,a.nivel) as f  from menudnivel a left join menummenu b ON 
						 a.nivel = b.nivel where a.usuario= '$IdUsuario'  ORDER BY f ASC";
						// echo $consulta;	
			$rs3 = sqlsrv_query( $conexion,$consulta);
			if (!$rs3)
			{ 
				$mensaje = $mensaje."Error in SQL";
			}
			else
			{
				while( $row = sqlsrv_fetch_array( $rs3, SQLSRV_FETCH_ASSOC))
				{
					$descrip = trim($row['descrip']);
					$prog = trim($row['programa']);
					$nivel = trim($row['nivel']);
					if(strlen($prog) <=0)
					{
						if(!$paso)
						{
							echo "</ul>";
							echo "<div class='headerbar'><a href='#' >$descrip</a></div>";	
							echo "<ul class='submenu'>";
						}
						else
						{
							echo "<div class='headerbar'><a href='#' >$descrip</a></div>";	
							echo "<ul class='submenu'>";
							$paso=false;				
						}
								
					}
					else
					{
						echo "<li><a href='$prog' target='centro'>$descrip</a></li>";
						$paso=false;						
					}					
				}
			}
		}
		echo "</ul>";
		echo "<div class='headerbar'><a href='soporte.html' target='centro' >SOPORTE</a></div>" ;
		echo "<div class='headerbar'><a href='login.php' target='cuerpo'>SALIR DEL SISTEMA</a></div>" ;
		echo "</div>";		
	?>	
   </body>
</html>