<?php
	require_once("../../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	$paso=false;
	if(isset($_REQUEST['area']))
		$area = $_REQUEST['area'];
	if(isset($_REQUEST['opEstatus']))
		$opEstatus = $_REQUEST['opEstatus'];
	//echo $opEstatus;
	$datos=array();
	if($conexion)
	{
		if($area==='0000')
		{
			if($opEstatus=='9000')
			{
				$comando="select a.iddoc,a.folio,a.anio,a.NoOficio,a.idremitente,b.remitente,a.area,a.turnado,a.ruta,a.observaciones,a.estatus,a.tipo ,
						rtrim(ltrim(c.titulo)) + ' ' +rtrim(ltrim(c.nombre)) + ' ' + rtrim(ltrim(c.appat)) + ' '+ rtrim(ltrim(c.apmat)) as destianatrio,d.nomdepto
						from control_doc a 
						left join remitentes b on a.idremitente=b.idremitente 
						left join nomemp.dbo.nominadempleados c on a.turnado=c.numemp collate database_default
						left join nomemp.dbo.nominamdepto d on a.area=d.depto collate database_default";
			}
			else
		{				
				$comando="select a.iddoc,a.folio,a.anio,a.NoOficio,a.idremitente,b.remitente,a.area,a.turnado,a.ruta,a.observaciones,a.estatus,a.tipo ,
							rtrim(ltrim(c.titulo)) + ' ' +rtrim(ltrim(c.nombre)) + ' ' + rtrim(ltrim(c.appat)) + ' '+ rtrim(ltrim(c.apmat)) as destianatrio,d.nomdepto
							from control_doc a 
							left join remitentes b on a.idremitente=b.idremitente 
							left join nomemp.dbo.nominadempleados c on a.turnado=c.numemp collate database_default
							left join nomemp.dbo.nominamdepto d on a.area=d.depto collate database_default where a.estatus='$opEstatus'";				
			}
		}
		else
		{
			if($opEstatus=='9000')
			{
				$comando="select a.iddoc,a.folio,a.anio,a.NoOficio,a.idremitente,b.remitente,a.area,a.turnado,a.ruta,a.observaciones,a.estatus,a.tipo ,
						rtrim(ltrim(c.titulo)) + ' ' +rtrim(ltrim(c.nombre)) + ' ' + rtrim(ltrim(c.appat)) + ' '+ rtrim(ltrim(c.apmat)) as destianatrio,d.nomdepto
						from control_doc a 
						left join remitentes b on a.idremitente=b.idremitente 
						left join nomemp.dbo.nominadempleados c on a.turnado=c.numemp collate database_default
						left join nomemp.dbo.nominamdepto d on a.area=d.depto collate database_default where a.area=$area  ";
			}
			else
			{				
				$comando="select a.iddoc,a.folio,a.anio,a.NoOficio,a.idremitente,b.remitente,a.area,a.turnado,a.ruta,a.observaciones,a.estatus,a.tipo ,
						rtrim(ltrim(c.titulo)) + ' ' +rtrim(ltrim(c.nombre)) + ' ' + rtrim(ltrim(c.appat)) + ' '+ rtrim(ltrim(c.apmat)) as destianatrio,d.nomdepto
						from control_doc a 
						left join remitentes b on a.idremitente=b.idremitente 
						left join nomemp.dbo.nominadempleados c on a.turnado=c.numemp collate database_default
						left join nomemp.dbo.nominamdepto d on a.area=d.depto collate database_default where a.area=$area and a.estatus='$opEstatus'";				
			}
			
		}
//		echo $comando;
		$getProducts = sqlsrv_query( $conexion,$comando);
		if ( $getProducts == false)
     	{ 
			$resoponsecode="02";
			$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		}
		else
		{
			$resoponsecode="Cantidad rows=".count($getProducts);
			$i=0;
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
            {
            	$datos[$i]['iddoc'] = html_entity_decode(trim($row['iddoc']));
				$datos[$i]['tipo'] = html_entity_decode(trim($row['tipo']));
				$datos[$i]['folio'] = html_entity_decode(trim($row['folio']));
				$datos[$i]['anio'] = $row['anio'];//substr($row->fecha,0,10);
				$datos[$i]['idremitente'] =  $row['idremitente'];//$row->mov;
				$datos[$i]['area']= $row['nomdepto'];//$row->idcontacto;
				$datos[$i]['observaciones'] = html_entity_decode(trim($row['observaciones']));//$row->observa;
				$datos[$i]['remitente'] =  html_entity_decode(trim($row['remitente']));//$row->nomtramite;
				$datos[$i]['ruta']= html_entity_decode(trim($row['ruta']));		
				$datos[$i]['estatus']= html_entity_decode(trim($row['estatus']));	
				$datos[$i]['turnado']= utf8_encode(trim($row['destianatrio']));	
				$i++;
            }
		}
		sqlsrv_free_stmt( $getProducts );
	}
	else
	{
			$datos[0]['error']="1";
			$datos[0]['string']="no hay conexion";
	}
	echo json_encode($datos);
?>