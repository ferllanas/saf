<?php
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Seguimiento de Visitas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<!--<link rel="stylesheet" type="text/css" href="body.css">-->
<link href="../../include/estilos.css" rel="stylesheet" type="text/css">		
<!-- Calendario -->
<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../../calendario/calendar.js"></script>
<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>

<script language="javascript" type="text/javascript"></script>
<script language="javascript" src="../../prototype/prototype.js"></script>
<script language="javascript" src="javascript/funciones.js"></script>
<script language="javascript" src="javascript/segdocumentos.js"></script>


</head>

<body >
<table width="100%" border="0">
  	<tr >
  		<td colspan="2" width="90%" align="center" class="subtituloverde12">Seguimiento de Documentos </td>		
	</tr>	
	<tr>
		<td class="texto8">Area Destinado: 
		  <select name="area" id="area" size="1" >
				 <?php 
						echo "<option value='0000'>Todas</option>";
						$consulta="select * from nomemp.dbo.nominamdepto where estatus<'90' order by nomdepto";
						$getProducts = sqlsrv_query( $conexion,$consulta);
						while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
						{
							$descrip = $row['nomdepto'];//$row->nomestatus ;
							$id = $row['depto'];//$row->estatus;												
							echo "<option value='$id'>$descrip</option>";		
						}							
					?>     
			  </select>
		 </td>			
		</tr>		
		<tr>
			<td class="texto8"><input type="radio" id="opEstatusT" name="opEstatus"  value="T" checked  > Todas <input type="radio" id="opEstatusRem" name="opEstatus"  value="REM"  > Remitidas <input type="radio" id="opEstatusRec" name="opEstatus"  value="REC"  > Recibidas <input type="radio" id="opEstatusC" name="opEstatus"  value="C"  > Contestadas</td>		
			<td><input name="button" type="button" onClick="buscar_docs();" value="Buscar"></td>   
		</tr>					
</table>
<table  width="100%">
	 <thead>
		 <tr class="subtituloverde">
			 <th>Folio</th>
			 <th>Remitente</th>	
			 <th>Area Destino</th>	
             <th>Destinatario</th>		 
			 <th>Situaci&oacute;n</th>	
			 <th>Documento</th> 
		 </tr>
	 </thead>	
 	<tbody id="busqueda_ef" class="resultadobusqueda"></tbody> 
</table>
</body>
</html>
