
<?php
//include('../../../pdf/class.ezpdf.php');
include('../../../Administracion/globalfuncions.php');
require_once("../../../connections/dbconexion.php");
//require_once("../../../connections/dbconexionfomeadmin.php");
require_once("../../../dompdf/dompdf_config.inc.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
$dompdf = new DOMPDF();
$dompdf->set_paper('letter');
if(isset($_GET["id"]))
{
	$id=$_GET["id"];
	$anio=$_GET["anio"];	
	$consulta="select a.iddoc,a.folio,a.anio,a.NoOficio,a.idremitente,b.remitente,b.dependencia,b.puesto, CONVERT(varchar(50),a.fecha,103) as fecha , CONVERT(varchar(50),a.FechaOf,103) as FechaOf ,a.ruta,a.observaciones,a.sintesis,a.turnado,a.estatus,a.tipo from control_doc a 
			left join remitentes b on a.idremitente=b.idremitente where a.folio='$id' and a.anio='$anio' ";
			//echo $consulta;
	$getProducts = sqlsrv_query( $conexion,$consulta);
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))//while($row = odbc_fetch_object($dbRes))
	{
		$folio = $row['iddoc'];
		$tipo = $row['tipo'];
		$fecha = $row['fecha'];
		$fechaOf = $row['FechaOf'];
		$foliodir = trim($row['tipo']).".".trim($row['folio'])."/".trim($row['anio']);
		$foliodir2 =  trim($row['folio'])."_".trim($row['anio']);
		$NoOficio = $row['NoOficio'];
		$sintesis = $row['sintesis'];
		$observa = $row['observaciones'];
		$idremitente =  $row['idremitente'];
		$remitente =  $row['remitente'];
		$dependencia =  $row['dependencia'];
		$puesto =  $row['puesto'];
		$turnado =  $row['turnado'];
		$ruta =  $row['ruta'];
		$estatus= $row['estatus'];				
		//$conexionemp = sqlsrv_connect($serverfomeadmin,$infoconexionfomeadmin);			
		//if ($conexionemp)
		//{									
			$consulta = "select a.numemp, a.nombre, a.appat, a.apmat, a.depto,b.nomdepto  from  nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto=b.depto
						 WHERE numemp ='$turnado'";									
			$R = sqlsrv_query( $conexion,$consulta);
			if ( $R === false)
			{ 
				$resoponsecode="02";
				die($consulta."". print_r( sqlsrv_errors(), true));
			}
			else
			{
				while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
				{
					$nomturnado=trim($row['nombre'])." ".trim($row['appat'])." ".trim($row['apmat']);
					$depto=trim($row['depto']);
					$nomdepto=trim($row['nomdepto']);
				}
			}
		//}
	}
}
else
{
		echo "No existe el Oficio";
		$folio = "";
		$fecha = "";
		$fechaOf = "";
		$foliodir =  "";
		$NoOficio= "";
		$observa = "";
		$sintesis = "";
		$idremitente =  "";
		$remitente =  "";
		$dependencia =  "";
		$puesto =  "";
		$estatus= "";
		$turnado="";
		$nomturnado="";
		$depto="";
		$nomdepto="";
}
$htmlstr="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
		<meta content='no-cache' http-equiv='Pragma' />
		<meta content='no-cache' http-equiv='Cache-Control' />
		<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
		<title>Control de Oficios</title>
		<script language='javascript' type='text/javascript'></script>
		<script language='javascript' src='javascript/prototype.js'></script>
		<script language='javascript' src='javascript/funciones_contdocs.js'></script>
	</head>
	<body style='width:601px;' >	
	  <table>
               <tr >
			   		<td >&nbsp;</td>
					<td colspan='3'><img src='../../../$imagenPDFPrincipal' width='162' height='56'/></td>
				</tr>
				 <tr>
					<td >&nbsp;</td>
			      	<td class='texto10' scope='row'>Control No:</td>
				  	<td class='texto10'>$foliodir</td>
				</tr>   
			    <tr>
					<td >&nbsp;</td>
			      	<td class='texto10' scope='row'></td>
				  	<td class='texto10'>$fecha</td>
				</tr>    
				<tr>
			      <td>&nbsp;</td>
				</tr>
				<tr>					
			      	<td class='texto12' colspan='3' scope='row'><strong>REMISI&Oacute;N DE DOCUMENTOS</strong></td>				  
				</tr>
				<tr>
			      <td>&nbsp;</td>
				</tr>				
				<tr>
					<td >&nbsp;</td>
					<td class='texto10' scope='row'>No de Oficio: </td>
					<td class='texto10'> $NoOficio</td>	  
				</tr>
				 <tr>
			      	<td >&nbsp;</td>
					<td class='texto10' scope='row'>Fecha del Oficio:</td>
				  	<td class='texto10'>$fechaOf</td>
				</tr>
				<tr>
			      <td>&nbsp;</td>
				</tr>
				<tr> 				           
               		<td >&nbsp;</td>
					<td class='texto10' ></td>
					<td class='texto11'> <strong> $remitente.- $puesto $dependencia </strong></td>
			    </tr>
				<tr> 				           
               		<td >&nbsp;</td>
					<td class='texto10'></td>
					<td class='texto11'><strong> $sintesis </strong></td>
			    </tr>				
				<tr>
			      <td>&nbsp;</td>
				</tr>
				<tr>				
                  	<td >&nbsp;</td>
					<td class='texto11' scope='row' ><strong>TURNADO A: </strong></td>
				  	<td class='texto11'><strong>$nomturnado/$nomdepto</strong></td>
				</tr>		
				<tr>
			      <td>&nbsp;</td>
				</tr>					
				<tr>				
                  	<td >&nbsp;</td>
					<td class='texto11' scope='row' ><strong>PARA SEGUIMIENTO </strong></td>				  	
				</tr>		
				<tr>
			      <td>&nbsp;</td>
				</tr>						
				<tr>				
                  	<td >&nbsp;</td>
					<td class='texto11' scope='row' ><strong>OBSERVACIONES:</strong></td>
                  	<td class='texto11'  >$observa</td>  				                  
                </tr>	
				<tr>
			      <td>&nbsp;</td>
				</tr>			
				<tr>				
                  	<td >&nbsp;</td>
					<td class='texto11' scope='row' ><strong>ATENTAMENTE</strong></td>                 
                </tr>
				<tr>
			      <td>&nbsp;</td>
				</tr>					
				<tr>
			      <td>&nbsp;</td>
				</tr>					
				<tr>				
                  	<td >&nbsp;</td>
					<td class='texto11' scope='row' colspan='2' align='center'><strong>LIC. JOSE ALEJANDRO ESPINOZA EGUIA</strong></td>                 
                </tr>
				<tr>				
                  	<td >&nbsp;</td>
					<td class='texto11' scope='row' colspan='2' align='center'><strong>ENCARGADO DEL DESPACHO DE LOS ASUNTOS DE LA DIR. EJECUTIVA</strong></td>                 
                </tr>	
				<tr>				
                  	<td >&nbsp;</td>
					<td colspan='2' align='right' scope='row' ><strong>MO03702</strong></td>                 
                </tr>				
              </table>			
	</body>
</html>
";
//echo $htmlstr;
$dompdf->load_html($htmlstr);
$dompdf->render();
$pdf = $dompdf->output(); 
//$datos[0]['FILE'][$j-1]="pdf_files/cheques_a_imprimir_".$num_che.".pdf";
$ruta="../../uploads/".$foliodir2.".pdf";
file_put_contents($ruta, $pdf);
//header('location:'.$ruta);

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
		<meta content='no-cache' http-equiv='Pragma' />
		<meta content='no-cache' http-equiv='Cache-Control' />
		<link type='text/css' href='../../../css/estilos.css' rel='stylesheet'>
		<title>Control de Oficios</title>
		<script language='javascript' type='text/javascript'></script>
		<script language='javascript' src='javascript/prototype.js'></script>
		<script language='javascript' src='javascript/funciones_contdocs.js'></script>
	</head>
	<body style='width:601px;' >	
	  <table>
               <tr >
			   		<td >&nbsp;</td>
					<td colspan='3'><img src='../../../<?php echo $imagenPDFPrincipal;?>' width='162' height='56'/></td>
				</tr>
				 <tr>
					<td >&nbsp;</td>
			      	<td class='texto10' scope='row'>Control No:</td>
				  	<td class='texto10'><strong><?php echo $foliodir; ?></strong></td>
				</tr>
				<tr>
					<td >&nbsp;</td>
			      	<td class='texto10' scope='row'>Documento:</td>
				  	<td class='texto10' align="center"><a href="<?php echo $ruta; ?>" target="_blank"><img src="../../../imagenes/Acrobat.png" width="35" height="41"></a></td>
				</tr>   
		</table>
	</body>
</html>