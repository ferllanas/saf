<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	$conexion = sqlsrv_connect($server,$infoconexion);
//***** para documentos *******
$target_path = "../../../imagenes/uploads/contOficios/";
$Imag = $_FILES['userfile']['name'];
	
if ($Imag)
{
	$Ext = pathinfo($Imag, PATHINFO_EXTENSION); 
	$Max = $_FILES['userfile']['size'];
	$tmp_name = $_FILES['userfile']['tmp_name'];				
	$error=$_FILES['userfile']["error"];
}
else
{
	echo "No existe el Documento";
	//die("error en el archivo");
}
$feccont=$_REQUEST['feccont'];
$noficio = $_REQUEST['noficio'];
$fecoficio = $_REQUEST['fecoficio'];
$idremitente = $_REQUEST['idremitente'];
$nomemp = $_REQUEST['nomemp'];
$idarea = $_REQUEST['idarea'];
$sintesis=$_REQUEST['sintesis'];
$opEstatus=$_REQUEST['opEstatus'];

echo $opEstatus;


$observa = $_REQUEST['observa'];

$widregistro=0;
$wfecha =  date('Y-m-d',time());
$whora =  date('H:i',time());
$anio =  date('Y',time());

	if ($idarea == "" | $idremitente == "" | $observa == ""  ) 
	{
		exit("No se llenaron campos necesarios");
	}
	else
	{
		if ($conexion)
		{ 			
					
			//// consecutivo
			// ultimo movimiento
			
			$mov=0;
			$consulta = "select folio from catfoliodocs where anio=$anio and tipo='$opEstatus'";
			$getProducts=sqlsrv_query( $conexion,$consulta);
			
			while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
			{
				$mov = $row['folio'];									
			}
			if($mov==0)
			{
				$wmov=1;
			}
			else
			{
				$wmov=$mov + 1;
			}
			
			if ($Imag)
			{
	
				//carga el archivo
				if(($Ext != "PDF" & $Ext != "pdf" &  $Ext != "png" & $Ext != "jpg" & $Ext != "bmp" & $Ext != "tif" & $Ext != "jpeg" & $Ext != "gif" &
					$Ext != "PNG" & $Ext != "JPG" & $Ext != "BMP" & $Ext != "TIF" & $Ext != "JPEG" & $Ext != "GIF"  & $Ext != "DOC"  & $Ext != "doc"  & $Ext != "DOCX"  & $Ext != "docx"
					 & $Ext != "XLSX"  & $Ext != "xlsx"  & $Ext != "XLS"  & $Ext != "xls" & $Ext != "PPT"  & $Ext != "ppt"  & $Ext != "PPTX"  & $Ext != "pptx"))
				{	 
					exit("el formato del documento no es valido");
				}			
				$path_parts = $tmp_name;
				//$fextension =  $Ext;			
				$newfile = $target_path."".$wmov."_".$anio.".".$Ext;
				//Path del archivo para guardar en la base de datos
				$pathtoDB = "imagenes/uploads/contOficios/".$wmov."_".$anio.".".$Ext;
				
				//echo $newfile."<br>".$pathtoDB;
				if(!file_exists($newfile)) //Si no existe el archivo previamente
				{
					if(move_uploaded_file($_FILES['userfile']['tmp_name'], $newfile))
						$mensajeAcciones="El documento ".$newfile." a sido enviado exitosamente.";
					else
					{
						$mensajeAcciones="El documento ".$newfile." tuvo errores al tratar de enviarlo.";
						die($mensajeAcciones);
					}
				}
				else
				{
					if(move_uploaded_file($_FILES['userfile']['tmp_name'], $newfile))//if(rename("../".$docdigitales[$i]['path'], $newfile))
						$mensajeAcciones="El documento ".$newfile." a sido enviado exitosamente.";
					else
					{
						$mensajeAcciones="El documento ".$newfile." tuvo errores al tratar de enviarlo.";
						die($mensajeAcciones);
					}
				}			
			}
			else
			{
				$pathtoDB="";
			}
			// consecutivo de registro
			$consulta="";
			$consulta = "insert into control_doc(folio,anio,NoOficio,FechaOf,idremitente,turnado,area,fecha,estatus,ruta,observaciones,sintesis,tipo)";
			$consulta = $consulta."values ($wmov,$anio,'$noficio','$fecoficio',$idremitente,'$nomemp','$idarea','$wfecha','0000','$pathtoDB','$observa','$sintesis','$opEstatus')";		
			//echo $consulta;	
			sqlsrv_query( $conexion,$consulta);
			
			$consulta = "update catfoliodocs  set folio =$wmov where anio=$anio and tipo='$opEstatus'";
			$getProducts=sqlsrv_query( $conexion,$consulta);
			//echo $consulta;
			echo "<span class='texto8' scope='row'>Oficio $wmov"."_"."$anio fue Agregado satisfactoriamente!!.</span>";
			echo "<br><br>";
			echo "<br>";
			echo "<br>";
			echo "<br>";
			header('location:crear_pdf.php?id='.$wmov.'&anio='.$anio);
			//echo $opEstatus;
			//echo "sp".$opEstatusSP;
		}
		else
		{ 
			exit ("Falló la conexión de base de datos") ;
	    }
		sqlsrv_close($conexion);
    }
?>
