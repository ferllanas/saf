<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
	require_once("../../../connections/dbconexion.php");
	require_once("../../../Administracion/globalfuncions.php");
	$conexion = sqlsrv_connect($server,$infoconexion);
//***** para documentos *******
$target_path = "../../../imagenes/uploads/contOficios/";
$Imag = $_FILES['userfile']['name'];
	
if ($Imag)
{
	$Ext = pathinfo($Imag, PATHINFO_EXTENSION); 
	$Max = $_FILES['userfile']['size'];
	$tmp_name = $_FILES['userfile']['tmp_name'];				
	$error=$_FILES['userfile']["error"];
}
else
{
	echo "No existe el Documento";
	die("error en el archivo");
}



$id = $_REQUEST['id'];
$contesta=$_REQUEST['chkcontesta'];
$resp=$_REQUEST['resp'];



	if ($id == "" | $resp == "" | !$contesta ) 
	{
		exit("No se llenaron campos necesarios");
	}
	else
	{
		if(($Ext != "PDF" & $Ext != "pdf" &  $Ext != "png" & $Ext != "jpg" & $Ext != "bmp" & $Ext != "tif" & $Ext != "jpeg" & $Ext != "gif" 
				& $Ext != "PNG" & $Ext != "JPG" & $Ext != "BMP" & $Ext != "TIF" & $Ext != "JPEG" & $Ext != "GIF" & $Ext != "GIF" 
				& $Ext != "DOC"  & $Ext != "doc"  & $Ext != "DOCX"  & $Ext != "docx" & $Ext != "XLSX"  & $Ext != "xlsx"  & $Ext != "XLS" 
				& $Ext != "xls" & $Ext != "PPT"  & $Ext != "ppt"  & $Ext != "PPTX"  & $Ext != "pptx"))
			{	 
				exit("el formato del documento no es valido");
			}			
			$path_parts = $tmp_name;
			//$fextension =  $Ext;			
			$newfile = $target_path."R".$id.".".$Ext;
			//Path del archivo para guardar en la base de datos
			$pathtoDB = "imagenes/uploads/contOficios/R".$id.".".$Ext;
			
			//echo $newfile."<br>".$pathtoDB;
			if(!file_exists($newfile)) //Si no existe el archivo previamente
			{
				if(move_uploaded_file($_FILES['userfile']['tmp_name'], $newfile))
					$mensajeAcciones="El documento ".$newfile." a sido enviado exitosamente.";
				else
				{
					$mensajeAcciones="El documento ".$newfile." tuvo errores al tratar de enviarlo.";
					die($mensajeAcciones);
				}
			}
			else
			{
				if(move_uploaded_file($_FILES['userfile']['tmp_name'], $newfile))//if(rename("../".$docdigitales[$i]['path'], $newfile))
					$mensajeAcciones="El documento ".$newfile." a sido enviado exitosamente.";
				else
				{
					$mensajeAcciones="El documento ".$newfile." tuvo errores al tratar de enviarlo.";
					die($mensajeAcciones);
				}
			}			
		if ($conexion)
		{ 						
			// consecutivo de registro
			$consulta = "update control_doc set estatus='0002' ,respuesta='$resp',rutaresp='$pathtoDB' where iddoc=$id";	
			//echo $consulta;
			sqlsrv_query( $conexion,$consulta);
			
			//echo $consulta;
			echo "<span class='texto8' scope='row'>Oficio Contestado satisfactoriamente!!.</span>";
			echo "<br><br>";
			echo "<br>";
			echo "<br>";
			echo "<br>";
//			header('location:crear_pdf.php?id='.$wmov.'&anio='.$anio);
		}
		else
		{ 
			exit ("Falló la conexión de base de datos") ;
	    }
		sqlsrv_close($conexion);
    }
?>
