<?php
	require_once("../../connections/dbconexionFome.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta content="no-cache" http-equiv="Pragma" />
		<meta content="no-cache" http-equiv="Cache-Control" />
		<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
		<style type="text/css">
			<!--Estilo1 {font-family: Arial, Helvetica, sans-serif}.Estilo3 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; }.Estilo4 {font-size: 24px}			-->
				</style>
			<font face="Arial">
				<title>Control de Oficios</title>
				
			<style type="text/css">
					#altas span.errorphp{color:red; margin:0px; padding:0px; text-align:center;}
				h1 {
					margin: 0;
					background-color: ;
					color: white;
					font-size: 25px;
					padding: 3px 5px 3px 10px;
					border-bottom-widtd: 1px; 
					border-bottom-style: solid; 
					border-bottom-color: white;
					border-top-widtd: 1px; 
					border-top-style: solid; 
					border-top-color: white;
					background-repeat: repeat;
					font-weight: bolder;
					background-image: nombre.png;
					font-family: Georgia, "Times New Roman", Times, serif;
				} 
			</style>
		<!--<link rel="stylesheet" type="text/css" href="body.css">-->
		<!-- Calendario -->
		<link rel="stylesheet" type="text/css" media="all" href="../../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
		<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
		<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
		<script language="javascript" type="text/javascript"></script>
		<script language="javascript" src="javascript/prototype.js"></script>
		<script language="javascript" src="javascript/funciones_contdocs.js">
		function popUp() 
		{
			day = new Date();
			id = day.getTime();
			window.open("alta_remitente.php", "", "toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=250,left = 600,top = 325");
		}
		</script>
		
	</head>
	<body>	
	<h1 align="center" class="subtituloverde12"> Remision de Documento</h1>
		 
			<form  width="95%"  name="altas" method="POST" action="php_ajax/alta_de_documento.php" enctype="multipart/form-data">			
              <table widtd="90%">
                <tr>
                  <td class="subtituloverde" colspan="2"><div align="center"><span >INFORMACION GENERAL </span></div></td>                 
                </tr>
				<tr>
                  <td class="texto8" colspan="2"><input type="radio" id="opEstatusCI" name="opEstatus"  value="CI" checked  > Control Interno <input type="radio" id="opEstatusSP" name="opEstatus"  value="SP"  > Secretaria Particular </td>                 
                </tr>
			    <tr>
			      <td class="texto8" scope="row">Fecha control:</td>
				  <td><input name="feccont" type="text" size="7" id="feccont" value="<?php echo date('Y/m/d',time());?>" class="required" maxlength="10"  style="width:70">&nbsp;<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger_fin" style="cursor: pointer;" title="Date selector" align="absmiddle">
											<!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "feccont",		// id of the input field
													ifFormat       :    "%Y/%m/%d",		// format of the input field
													button         :    "f_trigger_fin",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td></tr>				
				<tr>
					<td class="texto8" scope="row">No de Oficio: </td>
					<td class="texto8"> <input id="noficio" name="noficio" type="text"  size="20"  />						
					</td>	  
				</tr>
				 <tr>
			      <td class="texto8" scope="row">Fecha del Oficio:</td>
				  <td><input name="fecoficio" type="text" size="7" id="fecoficio" value="<?php echo date('Y/m/d',time());?>" class="required" maxlength="10"  style="width:70">&nbsp;<img src="../../calendario/img.gif" width="16" height="16" id="f_trigger_of" style="cursor: pointer;" title="Date selector" align="absmiddle">
											<!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecoficio",		// id of the input field
													ifFormat       :    "%Y/%m/%d",		// format of the input field
													button         :    "f_trigger_of",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td></tr>
				<tr> 				           
               <td class="texto8" style="width:220px; ">
							Remitente:</td>
							<td><div align="left" style="z-index:1; position:static; left:4px; top: 200px; width:250px">  <input class="texto8" name="txtSearch" type="text" id="txtSearch" size="70" style="width:150px;"  onKeyUp="searchSuggest();" autocomplete="off"/><input  name="idremitente" type="hidden" id= "idremitente" size="68" maxlength="70" />  
							<div id="search_suggest" style="z-index:2;" > </div>
						  </div>
						</td>
			    </tr>
				<tr>				
                  <td class="texto8" scope="row" ><div align="left">Dependencia: </div></td>
                  <td ><input  name="dependencia" type="text" id= "dependencia" size="68" maxlength="70" readonly="readonly"/></td>                  
                </tr>
				<tr>				
                  <td class="texto8" scope="row" ><div align="left">Puesto: </div></td>
                  <td ><input  name="puesto" type="text" id= "puesto" size="68" maxlength="70" readonly="readonly"/></td>                  
                </tr>
				<tr>				
                  <td class="texto8" scope="row" ><div align="left">Turnado: </div></td>
				  <td><div align="left" style="z-index:1; position:static; left:4px; top: 200px; width:250px">  <input class="texto8" name="txtturna" type="text" id="txtturna" size="60" style="width:150px;"  onKeyUp="searchSuggestEmp();" autocomplete="off"/><input  name="nomemp" type="hidden" id= "nomemp" size="68" maxlength="70" />  
							<div id="search_suggest_emp" style="z-index:2;" > </div>
						  </div></td></tr>
				<tr>				
                  <td class="texto8" scope="row" ><div align="left">Area: </div></td>
                  <td ><input  name="nomarea" type="text" id= "nomarea" size="68" maxlength="70" readonly="readonly"/><input  name="idarea" type="hidden" id= "idarea" size="68" maxlength="70" /></td>                  
                </tr>		
				<tr>				
                  <td class="texto8" scope="row" ><div align="left">Sintesis: </div></td>
                  <td ><textarea  name="sintesis" type="text" id= "sintesis" rows="5" cols="51"  /></textarea> </td>                  
                </tr>				
				<tr>				
                  <td class="texto8" scope="row" ><div align="left">Observaciones: </div></td>
                  <td ><textarea  name="observa" type="text" id= "observa" rows="5" cols="51"  /></textarea> </td>                  
                </tr>				
				<tr >
				  <td colspan="2">
							<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                            <input type="file" id="userfile" name="userfile" size="75pts"/></td>
				</tr>
				<tr>
                  <td scope="row"><div align="right">
                      <input  type="submit" id="Enviar" name="Enviar" value="Guardar "  />
                  </div></td>
                  <td width="225" scope="row"><div align="left"><input name="restablecer2" type="reset" id="restablecer2" value="Restablecer" />
                  </div></td>
                </tr>
              </table>
			 
			</form>			  
	</body>
</html>
