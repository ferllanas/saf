<?php
	require_once("../../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta content="no-cache" http-equiv="Pragma" />
		<meta content="no-cache" http-equiv="Cache-Control" />
		<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
		<style type="text/css">
			<!--Estilo1 {font-family: Arial, Helvetica, sans-serif}.Estilo3 {font-family: Arial, Helvetica, sans-serif; font-weight: bold; }.Estilo4 {font-size: 24px}			-->
				</style>
			<font face="Arial">
				<title>Control de Oficios</title>
				
			<style type="text/css">
					#altas span.errorphp{color:red; margin:0px; padding:0px; text-align:center;}
				h1 {
					margin: 0;
					background-color: ;
					color: white;
					font-size: 25px;
					padding: 3px 5px 3px 10px;
					border-bottom-widtd: 1px; 
					border-bottom-style: solid; 
					border-bottom-color: white;
					border-top-widtd: 1px; 
					border-top-style: solid; 
					border-top-color: white;
					background-repeat: repeat;
					font-weight: bolder;
					background-image: nombre.png;
					font-family: Georgia, "Times New Roman", Times, serif;
				} 
			</style>
		<!--<link rel="stylesheet" type="text/css" href="body.css">-->
		<!-- Calendario -->
		<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
		<script type="text/javascript" src="../../calendario/calendar.js"></script>
		<script type="text/javascript" src="../../calendario/lang/calendar-esp.js"></script>
		<script type="text/javascript" src="../../calendario/calendar-setup.js"></script>
		<script language="javascript" type="text/javascript"></script>
		<script language="javascript" src="javascript/prototype.js"></script>
		<script language="javascript" src="javascript/funciones_contdocs.js"></script>
	</head>
	<body>	
	<h1 align="center" class="subtituloverde12"> Remision de Documento</h1>
		 
			<?php
				if(isset($_GET["id"]))
				{
					$id=$_GET["id"];
					$consulta="select a.iddoc,a.folio,a.anio,a.NoOficio,a.tipo,a.idremitente,b.remitente,b.dependencia,b.puesto, CONVERT(varchar(50),a.fecha,103) as fecha , CONVERT(varchar(50),a.FechaOf,103) as FechaOf ,a.ruta,a.rutaresp,a.observaciones,a.sintesis,a.respuesta,a.turnado,a.estatus from control_doc a 
							left join remitentes b on a.idremitente=b.idremitente where a.iddoc='$id' ";
					$getProducts = sqlsrv_query( $conexion,$consulta);
					while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))//while($row = odbc_fetch_object($dbRes))
					{
						$folio = $row['iddoc'];
						$fecha = $row['fecha'];
						$fechaOf = $row['FechaOf'];
						$foliodir =  $row['tipo'].".".trim($row['folio'])."/".trim($row['anio']);
						$NoOficio= $row['NoOficio'];
						$observa = $row['observaciones'];
						$sintesis = $row['sintesis'];
						$idremitente =  $row['idremitente'];
						$remitente =  $row['remitente'];
						$dependencia =  $row['dependencia'];
						$puesto =  $row['puesto'];
						$turnado =  $row['turnado'];
						$ruta =  $row['ruta'];
						$ruta2 =  $row['rutaresp'];
						$estatus= $row['estatus'];		
						$resp= $row['respuesta'];	
						if($estatus=='0000')
						{	
							$consulta="update control_doc set estatus='0001' where iddoc='$id' ";
							$getProducts = sqlsrv_query( $conexion,$consulta);
						}		
														
							$consulta = "select a.numemp, a.nombre, a.appat, a.apmat, a.depto,b.nomdepto  from  nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto=b.depto
										 WHERE numemp ='$turnado'";									
							$R = sqlsrv_query( $conexion,$consulta);
							if ( $R === false)
							{ 
								$resoponsecode="02";
								die($consulta."". print_r( sqlsrv_errors(), true));
							}
							else
							{
								while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
								{
									$nomturnado=trim($row['nombre'])." ".trim($row['appat'])." ".trim($row['apmat']);
									$depto=trim($row['depto']);
									$nomdepto=trim($row['nomdepto']);
								}
							}
						
					}
				}
				else
				{
						echo "No existe el Oficio";
						$folio = "";
						$fecha = "";
						$fechaOf = "";
						$foliodir =  "";
						$NoOficio= "";
						$sintesis = "";
						$observa = "";
						$idremitente =  "";
						$remitente =  "";
						$dependencia =  "";
						$puesto =  "";
						$estatus= "";
						$turnado="";
						$nomturnado="";
						$depto="";
						$nomdepto="";
				}
			?>
			<form  width="100%"  name="altas" method="POST" action="php_ajax/modifica_documento.php" enctype="multipart/form-data">			
              <table widtd="100%">
                <tr>
                  <td class="subtituloverde" colspan="2"><div align="center"><span >INFORMACION DEL OFICIO <?php echo $foliodir;?> </span></div></td>                 
                </tr>
			    
				<tr>
			      <td class="texto9" scope="row">Fecha control:</td>
				  <td class="texto9"><?php echo $fecha;?></td>
				</tr>
				<tr>
					<td></td>
				</tr>				
				<tr>
					<td class="texto9" scope="row">No de Oficio: </td>
					<td class="texto9"><strong> <?php echo $NoOficio;?> </strong></td>	  
				</tr>
				<tr>
					<td></td>
				</tr>				
				 <tr>
			      <td class="texto9" scope="row">Fecha del Oficio:</td>
				  <td class="texto9"><?php echo $fechaOf;?></td></tr>
				<tr> 				           
               		<td class="texto9"  style="width:120px; ">Remitente:</td>
					<td class="texto9"><strong><?php echo $remitente;?></strong></td>
			    </tr>
				<tr>
					<td></td>
				</tr>				
				<tr>				
                  <td class="texto9" scope="row" ><div align="left">Dependencia: </div></td>
                  <td class="texto9"><?php echo $dependencia;?></td>                  
                </tr>
				<tr>
					<td></td>
				</tr>				
				<tr>				
                  <td class="texto9" scope="row" >Puesto: </td>
                  <td class="texto9"><?php echo $puesto;?></td>                  
                </tr>
				<tr>
					<td></td>
				</tr>				
				<tr>				
                  <td class="texto9" scope="row" >Turnado: </td>
				  <td class="texto9"><strong> <?php echo $nomturnado;?></strong></td>
				</tr>
				<tr>
					<td></td>
				</tr>				
				<tr>				
                  <td class="texto9" scope="row" >Area: </td>
                  <td class="texto9"><?php echo $nomdepto;?></td>                  
                </tr>
				<tr>
					<td>&nbsp;</td>
				</tr>				
				<tr>				
                  <td class="texto9" scope="row" >Sintesis: </td>
                  <td class="texto9" style=" width:500px "><?php echo $sintesis;?></td>  				                  
                </tr>
				<tr>
					<td>&nbsp;</td>
				</tr>									
				<tr>				
                  <td class="texto9" scope="row" >Observaciones: </td>
                  <td class="texto9"><?php echo $observa;?> </td>  				                  
                </tr>		
				<tr>
					<td>&nbsp;</td>
				</tr>				
				<tr>				
                  <td class="texto9" scope="row" ><div align="left">Archivo: </div></td>
                  <td ><a href="<?php echo "../../".$ruta; ?>" target="_blank"><img src="../../imagenes/Acrobat.png" width="35" height="41"></a></td>  				                  
                </tr>
				<?php 
				if($estatus=='0002')
				{
				echo "		
				<tr>				
                  <td class='texto9' scope='row' ><div align='left'>Respuesta: </div></td>
                  <td class='texto9'>$resp</td>  
				  <td><input type='hidden' id='id' name='id' value='$id'  /></td>				                  
                </tr>
				<tr>				
                  <td class='texto9' scope='row' ><div align='left'>Evidencia: </div></td>
                  <td ><a href='../../$ruta2' target='_blank'><img src='../../imagenes/Acrobat.png' width='35' height='41'></a></td>  				                  
                </tr>";
				}
				else
				{
				echo "<tr>				
                  <td class='texto9' scope='row'>Contestar <input type='checkbox' id='chkcontesta' name='chkcontesta'> </td>              
                </tr>			
				<tr>				
                  <td class='texto9' scope='row' ><div align='left'>Respuesta: </div></td>
                  <td class='texto9'><textarea  name='resp' type='text' id= 'resp'  rows='5' cols='51'  /></textarea> </td>  
				  <td><input type='hidden' id='id' name='id' value='$id'  /></td>				                  
                </tr>
				<tr >
				<td colspan='2'>
							<input type='hidden' name='MAX_FILE_SIZE' value='2097152' />
                            <input type='file' id='userfile' name='userfile' size='75pts'/></td>
				</tr>
				<tr>
                  <td scope='row'><div align='right'>
                      <input  type='submit' id='Enviar' name='Enviar' value='Guardar '  />
                  </div></td>
                  <td width='225' scope='row'><div align='left'><input name='restablecer2' type='reset' id='restablecer2' value='Restablecer' />
                  </div></td>
                </tr>";
				}
				?>		
			<tr><td><input type="button" value="Regresar" onclick="javascript: location.href='../seguimiento/seg_docs.php';"/></td></tr>	
              </table>			 
			</form>			  
	</body>
</html>
