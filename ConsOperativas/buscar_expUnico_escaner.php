<?php
require_once("../connections/dbconexionFome.php");
require_once("../Administracion/globalfuncions.php");
$conexion = sqlsrv_connect($server,$infoconexion);

if(!$conexion)
	die("Error de conexion a base de datos.");




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta content="no-cache" http-equiv="Pragma" />
<meta content="no-cache" http-equiv="Cache-Control" />
<script src="../javascript_globalfunc/191jquery.min.js"></script>
<script language="javascript" src="../Administracion/javascript/funciones_globales.js"></script>
<link href="../css/estilos.css" rel="stylesheet" type="text/css">

<link type="text/css" href="../css/reveal.css" rel="stylesheet">
<script type="text/javascript" src="../javascript_globalfunc/jquery.reveal.js"></script>

		<link type="text/css" href="../css/estilos.css" rel="stylesheet">
		

 <!-- Menu Desplegable -->
    <script>
     $(function() {
            $( '#dl-menu' ).dlmenu({
                animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
            });
        });
    </script>

<script src="divhide.js"></script>
<script src="jquery.tmpl.js"></script>
<script>
    $(function()
    {
		$("#btnbuscar").click(function(){
					
				var solnombre= $('#solnombre').val(); // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
                var solappat= $('#solappat').val();
				var solapmat= $('#solapmat').val();
				var data = 'solnombre=' + solnombre;     // Toma el valor de los datos, que viene del input						
                $.post('busqueda_por_nombre.php',data, function(resp)
                { //Llamamos el arch ajax para que nos pase los datos
               		console.log(resp);
					$('#bodyRes').empty();
					$('#tmpl_asigna').tmpl(resp).appendTo('#bodyRes'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							 // los va colocando en la forma de la tabla
				}, 'json');  // Json es una muy buena opcion
		});
       
    });   
</script>

<script id="tmpl_asigna" type="text/x-jquery-tmpl">   
    <tr>
        {{if fml}}
            <td  align="center" class="texto12"><a href='buscar_expUnico_escaner.php?fml=${fml}'>${fml}</a></td>
            <td class="texto12">${nts}</td>
            <td class="texto12">${solnombre} ${solappat} ${solapmat}  </td>
            <td class="texto12">${fecasig}</td> 			
        {{else}}
            <td colspan="2">No existen resultados</td>
        {{/if}}
    </tr>
</script>   
</head>
<script>
$(document).ready(function(){
	//Se le da acci�n al formulario
	
	$('#btsubmit').click(function(e){
		checkSubmit();
		console.log("Buscar x Nombre");
		e.preventDefault();
		//Se verifica si el usuario a ingresado algun valor
		var $solnombre =  $('#solnombre').val();
		var $solappat = $('#solappat').val();
		var $solapmat = $('#solapmat').val();
		
		
		console.log( $solnombre +','+ $solappat + ',' + $solapmat);
		if($solnombre.length == 0){
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Regresa he intenta de nuevo. No se detecto el Nombre del Solicitante.</div>')
				.show(500);
				return false;
		}
		
		if($solappat.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor selecciona El Apeido Paterno del Solicitante.</div>')
				.show(500);
				return false;
		}		
		
		if($solapmat.length ==0)
		{
			$('#modalInfo')
				.hide()
				.html('<div class="infoModal infoError"><b>Precaucion</b><br>Por favor de ingresar el Apeido Materno del Solicitante.</div>')
				.show(500);
				return false;
		}		
		
		var $datos={
			prod: $solnombre,
			cta: $solappat,
			descrip: $solapmat
		}
		
			$.ajax({
				type: 'post',
				data: $datos,
				url: 'buscanombre.php',
				dataType: 'json',
				beforeSend: function(){
					$('input',this).each(function(){
						$(this).attr('disabled','disabled');
					});
				},
				error: function(xhr, status, error){
				console.log(xhr.responseText);
					$('#modalInfo')
						.hide()
						.html('<div class="infoModal infoError"><b>Warning</b><br>El nombre no se encontro.</div>')
						.show(500);
						habilita_btsubmit();
				},
				success: function($msg){
					if($msg.error == 1){
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoError"><b>Precauci&oacute;n</b><br>'+$msg.msg+'</div>')
							.show(500);
							habilita_btsubmit();
					}else{
						$('#modalInfo')
							.hide()
							.html('<div class="infoModal infoCorrecto"><b>Success!</b><br>El Producto ha sido actualizado con exito.</div>')
							.show(500);
						//Se cierra el modal
						habilita_btsubmit();
						buscar_prodsincta();
						setTimeout("cerrarModal()",1300);
						
					}
				}
			});
		
	});
});



</script>
<script>
function enviar_expunico()
{
	
	var op=  $("input:radio[name='opbusc']:checked'").val();//document.getElementById("opbusc").value;
	if(op==0)
	{
		var fracc=document.getElementById("fracc").value;
		var mzna=document.getElementById("mzna").value;
		var lote=document.getElementById("lote").value;
		var fml=fracc+mzna+lote;
		location.href="buscar_expUnico_escaner.php?fml="+fml;
	}
	else
	{
				var nombre= $('#solnombre').val(); // Se crea variable para tomar informacion del combo, misma que sera unida al data				  	
                var solappat= $('#solappat').val();
				var solapmat= $('#solapmat').val();
				var solnombre =nombre+'.'+solappat+'.'+solapmat;
				var data = 'solnombre=' + solnombre; 
				$.post('busqueda_por_nombre.php',data, function(resp)
                { //Llamamos el arch ajax para que nos pase los datos
               		console.log(resp);
					$('#bodyRes').empty();
					$('#tmpl_asigna').tmpl(resp).appendTo('#bodyRes'); // Checa el plugin de templating para Java, tomando los productos del script de abajo y 
							 // los va colocando en la forma de la tabla
				}, 'json');  
		popUp(solnombre,solappat,solapmat);
	}
}
function popUp(solnombre,solappat,solapmat) 
{
	console.log("Entro");
	var SA=solnombre.replace(/&COLLAS/g,'"');
	SA=SA.replace(/&COLLA/g,"'");
	$('#myModal').reveal();
	$('#modalInfo')
				.hide()
				.html('');
				
	
}
function validadocs()
{
	
	var observa= document.getElementById('observa').value;
	var usuario= document.getElementById('usuario').value;
	if(usuario.length<=0)
	{
		alert("Capture el Usuario del Documento");
		return;
	}
	var fml= document.getElementById('fml').value;
		var datas = {
			pfml : fml,
			pobserva : observa,
			pusuario : usuario
	};
	//alert("AAAAA");
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'guardarverificacont.php',
						dataType: "json",
						data: datas,
						success: function(resp)
						{
							console.log(resp);
							if(resp.error!=0)
							{
								alert(resp.error);
								if(resp.id!=0)
									location.href="buscar_expUnico_escaner.php";
							}
							
						}
					
					});
}
function validaerrordocs()
{
	
	var observa= document.getElementById('observa').value;
	var usuario= document.getElementById('usuario').value;
	if(usuario.length<=0)
	{
		alert("Capture el Usuario del Documento");
		return;
	}
	var fml= document.getElementById('fml').value;
		var datas = {
			pfml : fml,
			pobserva : observa,
			pusuario : usuario
	};
	//alert("AAAAA");
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'guardarerrorverificacont.php',
						dataType: "json",
						data: datas,
						success: function(resp)
						{
							console.log(resp);
							if(resp.error!=0)
							{
								alert(resp.error);
								if(resp.id!=0)
									location.href="buscar_expUnico_escaner.php";
							}
							
						}
					
					});
}
function FocusFML()
{
	document.getElementById('fracc').focus();
}
function FocusSol()
{
	document.getElementById('solnombre').focus();
}
</script>
<body>
<form method="post" action="javascript: enviar_expunico();" enctype="multipart/form-data" >
<table width="100%">
	<tr><td align="center" colspan="6" class="TituloDForma">Busqueda expediente �nico</td></tr>
	<tr>
		<td width="11%" class="texto10"><input type="radio" name="opbusc" id="0" value="0" onClick="FocusFML();"/>F-M-L</td>
		<td width="89%" ><input type="text" id="fracc" maxlength="4"  size="6px"	value="<?php echo $fracc;?>" name="fracc" onBlur="addCeros(this)" class="caja_grande" > - <input type="text" id="mzna" maxlength="4" size="6px" 	value="<?php echo $mzna?>" name="mzna" onBlur="addCeros(this)" class="caja_grande" > - <input type="text" id="lote" maxlength="12"  size="6px" value="<?php echo $lote;?>" name="lote"  class="caja_grande" onBlur="addCeros(this) ">
	</td>
	</tr>
    <tr>
		<td width="11%" class="texto10"><input type="radio" name="opbusc" id="1" value="1" onClick="FocusSol();"/> Solicitante</td>
		<td width="89%" class="caja_grande" ><input type="text" id="solnombre" maxlength="20"  size="25px"	value="<?php echo $solnombre;?>" name="solnombre"  class="caja_grande" > 
		<input type="text" id="solappat" maxlength="20" size="25px" 	value="<?php echo $solappat?>" name="solappat"  class="caja_grande" >  
		<input type="text" id="solapmat" maxlength="20"  size="25px" value="<?php echo $solapmat;?>" name="solapmat"  class="caja_grande" > 
		<input type="button" name="busc" id="busc" value="Buscar" onClick="javascript: enviar_expunico(); "/>
      </td>
	</tr>
 <tr><td colspan="6"><hr class="hrTitForma"></td></tr>
</table>
</form>


<?php 
$llave=false;
$fml="";
if(isset($_GET['fml']))										
	$fml=$_GET['fml'];
	
$conexion = sqlsrv_connect( $server , $infoconexion );
if(!$conexion) 
{	
	die("Error en la conexion");
}
?>
  
			<table summary="" cellpadding="0" cellspacing="0" width="100%" border="0">
				<tr>
				  <td align="center" colspan="8" class="TituloDForma">Datos del Documento <?php echo substr($fml,0,4)."-".substr($fml,4,4)."-".substr($fml,8,4);?>
				    <hr class="hrTitForma">
				</tr>
			</table>
	<tr>
		<td width="100%" height="100"><table  id="mainTable" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" style="border-collapse: collapse" >
		  <?php
						
					
						$consulta ="select SUBSTRING(a.fml,1,4) as fracc,a.*,b.solncompleto,b.conyncompleto,c.nomcol,c.mun,d.descripcion,f.descripcion as sitlote,e.descripcion as dtipolote from tecnicdlotes a 
left join estsocdfolese b on a.folioese=b.folioese 
left join tecnicmfracc c on SUBSTRING(a.fml,1,4)=c.fracc 
left join catsmscat f  on a.cvesit=f.scat
left join catsmscat e  on a.tipolote=e.scat
left join catsmscat d on c.mun=d.scat  where a.fml='$fml' and a.cvesit<'21609000'";
			
						$dbRes = sqlsrv_query($conexion, $consulta);				
						while($row = sqlsrv_fetch_array($dbRes))
						{
							$nombres=$row['solncompleto'];
							if(strlen($row['fml']))
							{	
								$fracc=$row['fracc'];
								$nomfracc=$row['nomcol'];
								$mun=$row['descripcion'];
								$fml=$row['fml'];
								$fml1="Fracc. ".substr($fml,0,4); 
								$mzna = substr($fml,4,4);
								$lote = substr($fml,8,4);
								$sitlote=$row['sitlote'];
								$dtipolote=$row['dtipolote'];
								
								
//								$fml1=substr($fml,0,4)."-".substr($fml,4,4)."-".substr($fml,8,4);
							}
							if(strlen($row['folioese']))
								$folioese=$row['folioese'];
								
							if(strlen($row['nts']))
								$nts=$row['nts'];
								$aream2=$row['aream2'];
								$solicitante=utf8_encode($row['solncompleto']);
								$conyuge=$row['conyncompleto'];
								$tipocont= substr($row['tipocont'],4,4);
								$contrato=$row['contrato'];
								$foliocont=$row['foliocont'];
								$feccont=$row['feccont'];
								
								//$fcon = substr($feccont,6,2)."/". substr($feccont,4,2)."/". substr($feccont,0,4);
								//$nummen=$row['nummen'];
								//$fvenpm=$row['fecvenpm'];
								//$fecvenpm =  substr($fvenpm,6,2)."/". substr($fvenpm,4,2)."/". substr($fvenpm,0,4);
								//$pctif=$row['pctif'];
								//$preciom2=$row['preciom2'];
								//$impter=$row['impter'];
						}
					
			?>
		  <tr>
		    <td width="109" height="25px">&nbsp;</td>
		    <td align='left' ><font class ='texto12'><b>Fraccionamiento:</b> <?php echo $fracc." ".$nomfracc;?></font></td>
		    <td align='left' ><font class ='texto12' ><b>Municipio</b><?php echo $mun;?></font></td>
		    <td align='left'><font class ='texto12' ></font></td>
	      </tr>
		  <tr>
		    <td  height="35px">&nbsp;</td>
		    <td ><font class ='texto12'><b>Manzana: </b><?php echo $mzna;?> <b>Lote: </b><?php echo $lote;?></font></td>
		    <td ><font class ='texto12'><b>Tipo: </b><?php echo $nts;?></font></td>
		    <td ><font class ='texto12'><b>No. de Solicitud: </b><?php echo $nts;?></font></td>
		    <td ><font class ='texto12'></font></td>
	      </tr>
		  <tr>
		    <td  height="35px">&nbsp;</td>
		    <td ><font class ='texto12'><b>No. Usuario:</b><?php echo $nts;?></font></td>
		    <td ><font class ='texto12'></font></td>
		    <td ><font class ='texto12'><b>Area M2:</b><?php echo $aream2;?> m2</font></td>
	      </tr>
		  <tr>
		    <td  height="35px">&nbsp;</td>
		    <td ><font class ='texto12'><b>Solicitante: </b><?php echo $solicitante;?></font></td>
		    <td ><font class ='texto12'><b>Conyuge:</b> <?php echo $conyuge;?></font></td>
		    <td ><font class ='texto12'></font></td>
	      </tr>
		  <tr>
		    <td colspan="8"><table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
		      <tr>
		        <td align="center" colspan="8" class="TituloDForma"><hr class="hrTitForma"></td>
	          </tr>
		      </table></td>
	      </tr>
		  <tr>
		    <td  height="35px">&nbsp;</td>
		    <td ><font class ='texto12'><b>Situacion del Lote : <?php echo $sitlote;?></b></font></td>
		    <td ><font class ='texto12'><b>Tipo Lote: <?php echo $dtipolote;?></b></font></td>
	      </tr>
		  <!--<tr>
		    <td  height="35px">&nbsp;</td>
		    <td ><font class ='texto12'><b>Fecha de Contrataci�n: </b><?php echo $fcon;?></font></td>
	      </tr>-->
  <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8"><table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
      <tr>
        <td align="center" colspan="8" class="TituloDForma"><hr class="hrTitForma"></td>
      </tr>
    </table></td>
        </table>
		  <div id="myModal" class="reveal-modal xlarge">
	<div class="mywrapper">
    	<div id="modalInfo"></div>
		 <div class="divLogoHeader"></div>
		<div id="myModalId">
        	<form name="modalFormPass" id="modalFormPass" method="post">
            	<div class="modalItem">
                	<table>
                    	<tr>
                        	<td align="center" class="subtituloverde12" >F-M-L</td>
                            <td align="center" class="subtituloverde12">NTS</td>
                            <td align="center" class="subtituloverde12">Solicitante</td>
                            <td align="center" class="subtituloverde12">Fecha de Asignacion </td> 
                        </tr>
                       <tbody name="bodyRes" id="bodyRes">
                            <tr <?php if($i==0) echo 'class="first"';?>>
                            </tr>
                      </tbody>
                        												            	
                  </table>
					<div class="clear"></div>
                    <div class="modalItemRight"><input tabindex="6" type="hidden" readonly name="prod" id= "prod" value="<?php echo $prod; ?>" size="40" /></div>
                   
                    <div class="clear"></div>
                </div>
                
            </form>
    	</div>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</body>
</html>
