<?php
require_once("../connections/dbconexion.php");
require_once("../Administracion/globalfuncions.php");
$conexion = sqlsrv_connect($server,$infoconexion);

if(!$conexion)
	die("Error de conexión a base de datos.");

$tipobusqueda="";
if(isset($_REQUEST['tipobusqueda']))
	$tipobusqueda=trim($_REQUEST['tipobusqueda']);

$nombre="";
if(isset($_REQUEST['nombre']))
	$nombre=trim($_REQUEST['nombre']);

$appat="";
if(isset($_REQUEST['appat']))
	$appat=trim($_REQUEST['appat']);

$apmat="";
if(isset($_REQUEST['apmat']))
	$apmat=trim($_REQUEST['apmat']);

$fracc="";
if(isset($_REQUEST['fracc']))
	$fracc=trim($_REQUEST['fracc']);

$manzana="";
if(isset($_REQUEST['m']))
	$manzana=trim($_REQUEST['m']);

$lote="";
if(isset($_REQUEST['l']))
	$lote=trim($_REQUEST['l']);

$datos=array();

$consulta="SELECT a.fml,a.fracc ,substring(a.fml,5,4) as mza,substring(a.fml,9,4) as lote , a.aream2 as areaasig,a.tipolote,a.nts,a.folioese,    a.preciom2, 
			a.pagoini,b.solnombre,b.solappat,b.solapmat,b.solncompleto,
			b.conynombre,b.conyappat,b.conyapmat, b.curpsol,b.curpcony,a.calle + ' ' + a.exterior as direccion, d.nomcol,d.mun, e.descripcion 
			FROM tecnicdlotes a 
			left join estsocdfolese b on a.folioese = b.folioese  
			left join tecnicmfracc d on a.fracc=d.fracc
			left join catsmscat e on d.mun=e.scat 
			 WHERE a.cvesit<'21079000' ";


if(isset($_REQUEST['tipobusqueda']))
{
	switch($tipobusqueda)
	{
		case "tipobnombres":
			if(strlen($nombre)>0)
				$consulta.=" AND b.solnombre LIKE '%$nombre%'";

			if(strlen($appat)>0)
				$consulta.=" AND b.solappat LIKE '%$appat%'";
			
			if(strlen($apmat)>0)
				$consulta.=" AND b.solappat LIKE '%$apmat%'";

		break;
		case "tipobfml":
			if(strlen($fracc)>0 && strlen($manzana)>0 && strlen($lote)>0)
				$consulta.=" AND a.fml='".$fracc."".$manzana."".$lote."'";
			else
				if(strlen($fracc)>0)
					$consulta.=" AND a.fracc='$fracc'";

		break;
	}
	//echo $consulta;
	$getProducts = sqlsrv_query( $conexion , $consulta);
	$i=0;
	while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
	{	if ($row)
		{
			$datos[$i]['fml']=trim($row['fml']);
			$datos[$i]['nts']=trim($row['nts']);
			$datos[$i]['folioese']=trim($row['folioese']);
			$datos[$i]['solnombre']=trim($row['solnombre']);
			$datos[$i]['solappat']=trim($row['solappat']);
			$datos[$i]['solapmat']=trim($row['solapmat']);
			$datos[$i]['conynombre']=trim($row['conynombre']);
			$datos[$i]['conyappat']=trim($row['conyappat']);
			$datos[$i]['conyapmat']=trim($row['conyapmat']);
			$datos[$i]['direccion']=trim($row['direccion']);
			$datos[$i]['nomcol']=trim($row['nomcol']);
			$datos[$i]['descripcion']=trim($row['descripcion']);
			$i++;
		}
	}
}




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script language="javascript" src="../javascript_fwk/jQuery.js"></script>
<script language="javascript" src="../javascript/prototype.js"></script>
<script language="javascript" src="../Administracion/javascript/funciones_globales.js"></script>
<link href="../css/estilos.css" rel="stylesheet" type="text/css">
</head>

<body>
<form method="post" action="cons_info.php" enctype="multipart/form-data" >
<table width="100%">
	<tr>
	  <td align="center" colspan="6" class="TituloDForma">Consulta de Asignaciones
	    <hr class="hrTitForma"></td></tr>
	<tr>
		<td width="5%"><input type="radio" name="tipobusqueda" id="tipobnombres" value="tipobnombres"></td>
		<td width="5%" class="texto8">Nombre:</td>
		<td width="20%" class="texto8"><input type="text" id="nombre" name="nombre" value="<?php echo $nombre;?>"  class="caja_entrada" onFocus="javascript: document.getElementById('tipobnombres').checked=true; "></td>
		<td width="20%" class="texto8"><input type="text" id="appat" name="appat"	value="<?php echo $appat;?>"  class="caja_entrada" onFocus="javascript: document.getElementById('tipobnombres').checked=true; "></td>
		<td width="20%" class="texto8"><input type="text" id="apmat" name="apmat" value="<?php echo $apmat;?>" class="caja_entrada" onFocus="javascript: document.getElementById('tipobnombres').checked=true; "></td>
		<td width="20%">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%"><input type="radio" name="tipobusqueda" id="tipobfml" value="tipobfml"></td>
		<td width="5%" class="texto8">F-M-L</td>
		<td width="20%"><input type="text" id="fracc" maxlength="4" 	value="<?php echo $fracc;?>" name="fracc" onBlur="addCeros(this)" class="caja_entrada" onFocus="javascript: document.getElementById('tipobfml').checked=true; "></td>
		<td width="20%"><input type="text" id="m" 	maxlength="4"	value="<?php echo $manzana;?>" name="m" onBlur="addCeros(this)" class="caja_entrada" onFocus="javascript: document.getElementById('tipobfml').checked=true; "></td>
		<td width="20%"><input type="text" id="l" 	maxlength="4"	value="<?php echo $lote;?>" name="l" onBlur="addCeros(this)" class="caja_entrada" onFocus="javascript: document.getElementById('tipobfml').checked=true; "></td>
		<td width="20%"><input type="submit"  value="Buscar" class="caja_entrada"></td>
	</tr>
</table>
</form>
<table class="resultadobusqueda" width="100%">
	<thead width="100%">
		<tr class="subtituloverde">
			<th width="33%">NTS</th>
			<th width="33%">F-M-L</th>
			<th width="33%">Solicitante</th>
		</tr>
	</thead>
	<tbody  width="100%">
		<?php 
			for($i=0;$i<count($datos);$i++)
			{
		?>
		<tr class="d<?php echo ($i%2);?>">
			<th width="33%" class="texto8"><?php echo format464($datos[$i]['nts']);?></th>
			<th width="33%" class="texto8"><a href="Expediente.php?fml=<?php echo $datos[$i]['fml'];?>&nts=<?php echo $datos[$i]['nts'];?>"><?php echo format444($datos[$i]['fml']);?></a> </th>
			<th width="33%" class="texto8"><?php echo $datos[$i]['solnombre']." ".$datos[$i]['solappat']." ".$datos[$i]['solapmat'];?></th>
		</tr>
		<?php 
			}
		?>
	</tbody>
</table>
</body>
</html>
