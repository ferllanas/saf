<?php require_once("../connections/dbconexion.php");
	require_once("../Administracion/globalfuncions.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$nts='';
$fml='';
$folioese='';

$saldototalNumber=0.0;
$vencido = "";
$mora = "";	
$mora1 = "";	
$Importe = "";	
$nombres="";
$conynombre="";
$direccion="";
$colonia = "";
$municipio="";
$area="";
$precio="";
?>
<html>
<head>
<title>REGISTRO DE IM�GENES</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../css/estilos.css" rel="stylesheet">
<link href="../css/estilosReportesCartera.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../css/tablesscrollbodyLotes.css" rel="stylesheet">
<script type="text/javascript" src="javascript/jQuery.js"></script> 
<script type="text/javascript" src="javascript/sortableWScroll/sortable.js"></script>
</head>

<body>

<?php 
/*include 'evaluser.php';*/ 
$llave=false;
$fml="";
$nts="";	
if(isset($_REQUEST['nts']))										
	$nts=$_REQUEST['nts'];		

if(isset($_GET['fml']))										
	$fml=$_GET['fml'];
	
if(!isset($_GET['nts']) && !isset($_GET['fml']))
{
	die("Error no se han recibido los datos necesarios favor de reportarlo al Departamento de Desarrollo de Sistemas.");
}
	
	
$conexion = sqlsrv_connect( $server , $infoconexion );
if(!$conexion) 
{	
	die("Error en la conexion");
}
?>
  
			<table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr><td align="center" colspan="6" class="TituloDForma">EXPEDIENTE 
				  <hr class="hrTitForma">
				</tr>
			</table>
	<tr>
		<td width="100%" >
			<div style="width:100%; overflow:auto;height:170;">
				<table  id="mainTable" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" style="border-collapse: collapse" border="0">
				<th colspan="3" align="center" class="subtituloverde">DATOS DEL SOLICITANTE  </th>	
					<?php
						
					
						$consulta ="SELECT a.fml,a.fracc ,substring(a.fml,5,4) as mza,substring(a.fml,9,4) as lote , a.aream2 as areaasig,a.tipolote,a.nts,a.folioese,    a.preciom2,a.nummen, a.impeng,a.impfin,a.impmen,
			a.pagoini,a.impter,b.solnombre,b.solappat,b.solapmat,b.solncompleto,
			b.conynombre,b.conyappat,b.conyapmat, b.conyncompleto,b.curpsol,b.curpcony,a.calle + ' # ' + a.exterior as dir, d.nomcol,d.mun, e.descripcion,			a.folioasig, a.nummen, a.fecasig, (SELECT descripcion FROM catsmscat x WHERE a.tipolote=x.scat) as tipolote,
			f.descripcion as sitlote
			FROM tecnicdlotes a 
			left join estsocdfolese b on a.folioese = b.folioese  
			left join tecnicmfracc d on a.fracc=d.fracc
			left join catsmscat e on d.mun=e.scat 
			left join catsmscat f on a.cvesit=f.scat
			 WHERE a.fml ='$fml' 
			and b.cvesit<'20029000' and a.cvesit<'21079000'  ORDER BY a.cvesit";
			
						$dbRes = sqlsrv_query($conexion, $consulta);				
						while($row = sqlsrv_fetch_array($dbRes))
						{
							//print_r($row);
							$nombres=$row['solncompleto'];
							$conynombre=$row['conyncompleto'];
							$direccion=utf8_decode($row['dir']);
							$colonia = $row['nomcol'];
							$municipio=$row['descripcion'];
							$area=$row['areaasig'];
							$precio=$row['preciom2'];
							$pagoini=$row['pagoini'];
							$impter=$row['impter'];
							$nummen=$row['nummen'];
							$impeng=$row['impeng'];
							$impfin=$row['impfin'];
							$impmen=$row['impmen'];
							$tipolote=$row['tipolote'];
							$sitlote=$row['sitlote']; 
							if(strlen($row['fml']))
							{	
								
								$fml=$row['fml'];	
								$fml1=substr($fml,0,4)."-".substr($fml,4,4)."-".substr($fml,8,4);
							}
							if(strlen($row['folioese']))
								$folioese=$row['folioese'];
								
							if(strlen($row['nts']))
								$nts=$row['nts'];
						}
						
						
					if(strlen($nts)>0)
					{						
						$consulta = "SELECT * from  estsocdfolnts  WHERE nts='$nts' and cvesit<'20029000' ";
						//echo $consulta;
						
						$dbRes = sqlsrv_query($conexion, $consulta);				
						while($row2 = sqlsrv_fetch_array($dbRes))
						{
							$fecsol=$row2['fecsol'];
							$fecasig=$row2['fecasig'];
							$fecsol1=substr($fecsol,6,2)."/".substr($fecsol,4,2)."/".substr($fecsol,0,4);
							$fecasig1=substr($fecasig,6,2)."/".substr($fecasig,4,2)."/".substr($fecasig,0,4);
							//$fml=$row->fml;
							//$folioese=$row->folioese;							
						}
					}

			?>
						<tr><td width="11">&nbsp;</td></tr>
                        <tr>
                        <td>&nbsp;</td>
                        <td height='20' align='left'><font class ='texto10' >F-M-L:</font></td>
						<td  height='20' align='left'><b><font class ='texto12'><?php echo  $fml1;?></font></b></td>
						</tr>
                         <tr>
                        <td>&nbsp;</td>
                        <td height='20' align='left'><font class ='texto10' >Situacion de Lote:</font></td>
						<td  height='20' align='left'><b><font class ='texto12'><?php echo  $sitlote;?></font></b></td>
						</tr>
						<tr>
                        <td>&nbsp;</td>
						<td height='20' align='left'><font class ='texto10'>Folio de solicitud:</font></td>
						<td height='20' align='left'><font class ='texto10'><?php echo format464($nts);?></font></td>
						</tr>
						<tr>
                        <td>&nbsp;</td>
						<td height='20' align='left' ><font class ='texto10'>Folio de estudio socieconomico:</font></td>
						<td  height='20' align='left'><font class ='texto10'><?php echo format446($folioese);?></font></td>
						</tr>
                        <tr>
                        <td>&nbsp;</td>
						<td height='20' width="203" align='left'><font class ='texto10'>Nombre:</font></td>
						<td height='20' align='left'><font class ='texto10'><b><?php echo $nombres;?></b></font></td>
						</tr>
                        
                        <td>&nbsp;</td>
						<td height='20' align='left'><font class ='texto10'>Conyuge:</font></td>
						<td  height='20' width="959" align='left'><font class ='texto10'><b><?php echo $conynombre;?></b></font></td>
						</tr>
						<tr>
                        <td>&nbsp;</td>
                        <td height='20' align='left' width="120"><font class ='texto10'>Fecha de solicitud:</font></td>
						<td colspan ='1' height='20' width="120" align='left'><font class ='texto10'><?php echo $fecsol1;?></font></td>
						</tr>
					</font>
				</table>
			</div>
		</td>
	</tr>
</table>
			<table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr bgcolor="#FFFFFF">
					<th width="100%" align="center" class="subtituloverde">INFORMACION DEL LOTE <?php echo $fml1; ?> </th>
				</tr>
			</table>
			<tr>
		<td width="100%" height="100">
			<div style="width:100%; overflow:auto;">
				<table  id="mainTable" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" style="border-collapse: collapse" >
					
					
						<tr><td width="38">&nbsp;</td></tr>
                        <tr>
                       <td>&nbsp;</td>
						<td height='35' align='left' width="180"><font class ='texto10'>Fecha de asignacion de Lote:</font></td>
						<td width="171" height='20' colspan ='1' align='left'><font class ='texto10'><?php echo  $fecasig1;?></font></td>
						</tr>	
                        <tr>
                        <td>&nbsp;</td>
                        <td  class="texto10"> Direcci�n:</td><td colspan="3" class="texto10"><?php echo $direccion;?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        <td  class="texto10">&nbsp; </td><td colspan="3" class="texto10"><?php echo $colonia;?></td>
                        </tr>
                         <tr>
                        <td>&nbsp;</td>
                        <td class="texto10">&nbsp; </td><td colspan="3" class="texto10"><?php echo $municipio." N.L.";?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        <td height='35' class="texto10"> Tipo Lote:</td><td class="texto10"><b><?php echo $tipolote;?></b></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        <td height='35' class="texto10"> Area M2:</td><td class="texto10"><?php echo $area;?></td>
                        <td width="112" class="texto10"> Precio M2:</td><td width="653" class="texto10"><?php echo '$'.number_format($precio,2,'.',',');?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        <td height='35' class="texto10"> Importe de Lote:</td><td class="texto10"><b><?php echo '$'.number_format($impter,2,'.',',');?></b></td>
                        </tr>
                        <tr>
                         <td>&nbsp;</td>
                        <td height='35' class="texto10"> Enganche:</td><td class="texto10"><?php echo '$'.number_format($impeng,2,'.',',');?></td>
                        <td width="112" class="texto10"> Importe a Financiar:</td><td width="653" class="texto10"><?php echo'$'.number_format($impfin,2,'.',',');?></td>
                        </tr>
                        <tr>
                         <td>&nbsp;</td>
                        <td height='35' class="texto10"> Numero de Mensualidades:</td><td class="texto10"><?php echo $nummen;?></td>
                        <td width="112" class="texto10"> Mensualidad:</td><td width="653" class="texto10"><?php echo'$'.number_format($impmen,2,'.',',');?></td>
                        </tr>
					</font>
				</table>
			</div>
		</td>
		</tr>
		<td>&nbsp;</td>
		<!--<table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr bgcolor="#FFFFFF">
					<th width="100%" align="center" class="subtituloverde">CARTERA</th>
				</tr>
			</table>
			<tr>
		<td width="100%" height="100">
			<div style="width:100%; overflow:auto;">
				<table  id="mainTable" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" style="border-collapse: collapse" border="1">
					
					<?php
						if($fml)
						{
							$tsql_callSP ="{call sp_venc_mora_gral(?)}";//Arma el procedimeinto almacenado
							$params = array(&$fml);//Arma parametros de entrada
							$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
							
							$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
							if( $stmt === false )
							{
								 die( print_r( sqlsrv_errors(), true));
							}	
							else
							{
								//echo "Rows affectd: ".sqlsrv_rows_affected($stmt)."-----\n";
							
								$next_result = sqlsrv_next_result($stmt);
							
								// Retrieve and display the first result. 
								if( $next_result )
								while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
								{
									$vencido = '$ '.number_format($row['vencido'],2);
									$mora = '$ '.number_format($row['intmor'],2);	
									$mora1 = $row['intmor'];	
									$Importe = '$ '.number_format($row['impmen'],2);					
								}
								sqlsrv_free_stmt( $stmt);
							}
							
							$consulta ="sp_saldo '".$fml."'";
							$dbRes3 = sqlsrv_query( $conexion,$consulta);
							
							$row = sqlsrv_fetch_array( $dbRes3, SQLSRV_FETCH_ASSOC);
							if ($row)
							{
								
								$saldogral1 = $row['saldo'];
								$saldogral ='$ '.number_format($row['saldo'],2);
								$saldototal= '$ '.number_format($saldogral1 + $mora1,2);
								$saldototalNumber=$saldogral1+$mora1;
								$Pagado = trim($row['pagado']);
							}
							else
							{
								$saldototalNumber = 0.00;
								$saldogral1 = 0.00;
								$saldogral ='$ 0.00';
								$saldototal= '$ 0.00';
							}
							
							$consulta ="select * from carterdconvenio where fml='".$fml."' and cvesit <'22709000'";
							$dbRes2 = sqlsrv_query($conexion, $consulta);
							$row = sqlsrv_fetch_array($dbRes2);
							if ($row)
							{
								$fecconvi = $row['fecconvi'];
								$fecconvi1=substr($fecconvi,6,2)."/".substr($fecconvi,4,2)."/".substr($fecconvi,0,4);
								
								$mensconv = $row['mensconv'];
								$Importe= '$ '.number_format($mensconv,2,'.',',');
								$convenio = $row['convenio'];
								$mesplazo = $row['mesplazo'];	
								//$Importe = '$ '.number_format($row->impmen,2,'.',',');			
							}
							else
							{
								$fecconvi = "";
								$fecconvi1="";
								$mensconv = 0;
								$Importe= '$ 0.00';
								$convenio = "";
								$mesplazo = "";
							}
							/*$consulta ="sp_venc_mora_gral '".$fml."'";
							$dbRes2 = sqlsrv_query($conexion, $consulta);
							$row = sqlsrv_fetch_array($dbRes2);
							if ($row)
							{
								$vencido = '$ '.number_format($row['vencido'],2,'.',',');
								if($row->intmor!=null)
									$mora = '$ '.number_format($row['intmor'],2,'.',',');	
									
								$mora1 = $row->intmor;	
								//$Importe = '$ '.number_format($row->impmen,2,'.',',');			
							}
							else
							{
								$vencido = '$ 0.00 ';
								$mora = '$ 0.00';
								$mora1 = 0;	
								//$Importe = '$ 0.00';
							}
							$consulta ="sp_saldo '".$fml."'";
							//secho $consulta;
							$dbRes3 = sqlsrv_query($conexion, $consulta);
							$row = sqlsrv_fetch_array($dbRes3);
							if ($row)
							{
								//echo "GBY"
								$Importe= '$ '.number_format($row['impmen'],2,'.',',');//$row->impmen;
								$saldogral1 = $row['saldo'];
								$saldogral ='$ '.number_format($row['saldo'],2,'.',',');
								$saldototal= '$ '.number_format($saldogral1 + $mora1,2,'.',',');								
							}
							else
							{
								$Importe = '$ 0.00';
								$saldogral1 = 0.00;
								$saldogral ='$ 0.00';
								$saldototal= '$ 0.00';
							}
							
							*/
?>
							<tr>
							<td height='20' align='left' width="120"><font class ='texto8'>Saldo</font></td>
							<td colspan ='2' height='20' align='left' class ='texto8'><?php echo $saldogral;?></td>
							</tr>
							<tr>
							<td height='20' align='left'><font class ='texto8'>Saldo vencido:</font></td>
							<td colspan ='2' height='20' align='left'><font class ='texto8'><?php echo $vencido;?></font></td>
							</tr>
							<tr>
								<td height='20' align='left'><font class ='texto8'>Intereses moratorios:</font></td>
								<td height='20' align='left'><font class ='texto8'><?php echo $mora;?></font></td>
							</tr>
							<tr>
								<td height='20' align='left'><font class ='texto8'>Salto total:</font></td>
								<td align='left' class="texto8" title="Permite revisar el detalle de los cargos."><a href="Cartera/php_ajax/cargos.php?idfml=<?php echo $fml;?>"><?php echo $saldototal;?></a></td>
							</tr>
							<tr>
							<td height='20' align='left'><font class ='texto8'><b>Convenio de Pago</b></font></td>							
							</tr>
							<tr>
							<td height='20' align='left'><font class ='texto8'>Convenio:</font></td>
							<td height='20' align='left'><font class ='texto8'><?php echo $convenio;?></font></td>
							</tr>
							<tr>
							<td height='20' align='left'><font class ='texto8'>Fecha de convenio:</font></td>
							<td height='20' align='left'><font class ='texto8'><?php echo $fecconvi1;?></font></td>
							</tr>
							<tr>
							<td height='20' align='left'><font class ='texto8'>Mensualidad:</font></td>
							<td height='20' align='left'><font class ='texto8'><?php echo $Importe;?></font></td>
							</tr>
							<tr>
							<td height='20' align='left'><font class ='texto8'>Plazo del convenio:</font></td>
							<td height='20' align='left'><font class ='texto8'><?php echo $mesplazo;?></font></td>
							</tr>
			<?php			}
						
					?>
					</font>
				</table>-->
			</div>
		</td>
		</tr>
			
	<TR><TD>&nbsp;</TD></TR>
    <tr>
      <td><input type="button" value="Regresar" onClick="javascript: location.href='cons_info.php'"/></td></tr>
	<tr>
		<td width="100%" align="center"  class="subtituloverde">
			<table cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr bgcolor="#FFFFFF">
					<th width="100%" align="center" class="subtituloverde">HISTORIAL DE LOTE </th>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%">
			<table  width="700" align="center">
				<thead>
					<tr>
						<th width="200" class="texto8">Descripci�n</th>
						<th width="100" class="texto8">Fecha</th>
						<th width="200" class="texto8">Solicitante</th>
						<th width="200" class="texto8">Conyuge</th>
					</tr>
				</thead>
<?php
$seguimientos=array();
$consulta = "SELECT c.descripcion,a.fecsit, b.solncompleto, b.conyncompleto,a.folioese, a.cvesit  FROM tecnicdlotes a 
LEFT JOIN estsocdfolese b  ON a.folioese=b.folioese 
LEFT JOIN catsmscat c ON a.cvesit=c.scat  WHERE a.fml= '$fml' and a.cvesit>='21079000'";

$dbRes = sqlsrv_query($conexion, $consulta);		
if($dbRes!==null)
{
	$i=0;
	while($row = sqlsrv_fetch_array($dbRes))
	{
		$seguimientos[$i]['descripcion'] = trim($row['descripcion']);
		$seguimientos[$i]['fecsit'] = trim($row['fecsit']);
		$seguimientos[$i]['solncompleto'] = trim($row['solncompleto']);
		$seguimientos[$i]['conyncompleto'] = trim($row['conyncompleto']);
		$i++;	
	}
}
?>
				<tbody class="resultadobusqueda">
			<?php 
				if(count($seguimientos)>0)
					for($i=0;$i<count($seguimientos);$i++)
					{
					?>
						<tr  class="d<?php echo ($i%2);?>">
							<th class="texto8"><?php echo $seguimientos[$i]['descripcion'];?></th>
							<th class="texto8"><?php echo fechSegAEuropea($seguimientos[$i]['fecsit']);?></th>
							<th class="texto8"><?php echo $seguimientos[$i]['solncompleto'];?></th>
							<th class="texto8"><?php echo $seguimientos[$i]['conyncompleto'];?></th>
						</tr>
				<?php }
					?>
					
				</tbody>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
