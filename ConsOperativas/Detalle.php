<?php require_once("../../connections/dbconexion.php");
	require_once("../../Administracion/globalfuncions.php");

if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	
$nts='';
$fml='';
$folioese='';

$saldototalNumber=0.0;
$vencido = "";
$mora = "";	
$mora1 = "";	
$Importe = "";		
?>
<html>
<head>
<title>REGISTRO DE IMÁGENES</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link type="text/css" href="../../css/estilos.css" rel="stylesheet">
<link href="../../css/estilosReportesCartera.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../../css/tablesscrollbodyLotes.css" rel="stylesheet">
<script type="text/javascript" src="../javascript/jQuery.js"></script> 
<script type="text/javascript" src="../javascript/sortableWScroll/sortable.js"></script>
</head>

<body>

<?php 
/*include 'evaluser.php';*/ 
$llave=false;
$fml="";
$nts="";	
if(isset($_REQUEST['nts']))										
	$nts=$_REQUEST['nts'];		

if(isset($_GET['fml']))										
	$fml=$_GET['fml'];
	
if(!isset($_GET['nts']) && !isset($_GET['fml']))
{
	die("Error no se han recibido los datos necesarios favor de reportarlo al Departamento de Desarrollo de Sistemas.");
}
	
	
$conexion = sqlsrv_connect( $server , $infoconexion );
if(!$conexion) 
{	
	die("Error en la conexion");
}
?>
  
			<table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr>
				  <td align="center" colspan="8" class="TituloDForma">Datos del Documento <?php echo substr($fml,0,4)."-".substr($fml,4,4)."-".substr($fml,8,4);?>
				    <hr class="hrTitForma">
				</tr>
			</table>
	<tr>
		<td width="100%" height="100">
			
				<table  id="mainTable" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" style="border-collapse: collapse" >
					
					<?php
						
					if(strlen($nts)>0)
					{						
						$consulta = "SELECT a.nts,a.folioese,a.fml,rtrim(ltrim(b.solnombre))+' '+rtrim(ltrim(b.solappat))+' '+rtrim(ltrim(b.solapmat)) 
						as nombres from  estsocdfolnts a left join estsocdfolese b on a.folioese=b.folioese WHERE a.nts='$nts' and a.cvesit<'20029000' ORDER BY b.solnombre, b.solappat, b.solapmat";
						
						$dbRes = sqlsrv_query($conexion, $consulta);				
						while($row = sqlsrv_fetch_array($dbRes))
						{
							$nombres=$row['nombres'];
							if(strlen($row['fml']))
							{	
								
								$fml=$row['fml'];	
								$fml1="Fracc. ".substr($fml,0,4)." Mzna. ".substr($fml,4,4)." Lote ".substr($fml,8,4);
							}
							if(strlen($row['folioese']))
								$folioese=$row['folioese'];							
						}
					}
					else
					{
						$consulta ="SELECT a.fml,a.fracc ,substring(a.fml,5,4) as mza,substring(a.fml,9,4) as lote , a.aream2 as areaasig,a.tipolote,a.nts,a.folioese,    a.preciom2, 
			a.pagoini,b.solnombre,b.solappat,b.solapmat,b.solncompleto,
			b.conynombre,b.conyappat,b.conyapmat, b.curpsol,b.curpcony,a.calle + ' ' + a.exterior as direccion, d.nomcol,d.mun, e.descripcion ,
			a.folioasig, a.nummen, a.fecasig, (SELECT descripcion FROM catsmscat x WHERE a.tipolote=x.scat) as tipolote
			FROM tecnicdlotes a left join estsocdfolese b on a.folioese = b.folioese  
			left join tecnicmfracc d on a.fracc=d.fracc
			left join catsmscat e on d.mun=e.scat 
			 WHERE a.fml ='$fml' and a.cvesit<'21609000'   
			and b.cvesit<'20029000' and a.cvesit<'21079000'  ORDER BY a.cvesit";
			
						$dbRes = sqlsrv_query($conexion, $consulta);				
						while($row = sqlsrv_fetch_array($dbRes))
						{
							$nombres=$row['solncompleto'];
							if(strlen($row['fml']))
							{	
								
								$fml=$row['fml'];
								$fml1="Fracc. ".substr($fml,0,4)." Mzna. ".substr($fml,4,4)." Lote ".substr($fml,8,4);
//								$fml1=substr($fml,0,4)."-".substr($fml,4,4)."-".substr($fml,8,4);
							}
							if(strlen($row['folioese']))
								$folioese=$row['folioese'];
								
							if(strlen($row['nts']))
								$nts=$row['nts'];
						}
					}
			?>
						
                        <tr>
                        <td width="109">&nbsp;</td>
						<td height='20' align='left' width="120"><font class ='texto12'><b>Nombre:</b></font></td>
						<td width="283" height='20' colspan ='1' align='left'><font class ='texto12'><?php echo $nombres;?></font></td>
                        <td height='20' align='left' width="147" colspan="2"><font class ='texto12' ><b><?php echo  $fml1;?></b></font></td>
						
						</tr>
						<tr>
                        <td>&nbsp;</td>
						<td height='20' align='left' width="120"><font class ='texto12'><b>Solicitud:</b></font></td>
						<td height='20' align='left'><font class ='texto12'><?php echo $nts;?></font></td>
						<td height='20' align='left' width="120"><font class ='texto12'><b>Estudio Socieconomico:</b></font></td>
						<td  height='20' align='left'><font class ='texto12'><?php echo $folioese;?></font></td>
						</tr>
					</font>
					<tr bgcolor="#FFFFFF">
					<th width="100%" colspan="8" valign="center" class="TituloDForma">Datos de Cartera</th>
				</tr>
			
					
					<?php
						if($fml)
						{
							$tsql_callSP ="{call sp_venc_mora_gral(?)}";//Arma el procedimeinto almacenado
							$params = array(&$fml);//Arma parametros de entrada
							$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 minutos
							
							$stmt = sqlsrv_query($conexion, $tsql_callSP, $params);
							if( $stmt === false )
							{
								 die( print_r( sqlsrv_errors(), true));
							}	
							else
							{
								//echo "Rows affectd: ".sqlsrv_rows_affected($stmt)."-----\n";
							
								$next_result = sqlsrv_next_result($stmt);
							
								// Retrieve and display the first result. 
								if( $next_result )
								while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
								{
									$vencido = '$ '.number_format($row['vencido'],2);
									$mora = '$ '.number_format($row['intmor'],2);	
									$mora1 = $row['intmor'];	
									$Importe = '$ '.number_format($row['impmen'],2);					
								}
								sqlsrv_free_stmt( $stmt);
							}
							
							$consulta ="sp_saldo '".$fml."'";
							$dbRes3 = sqlsrv_query( $conexion,$consulta);
							
							$row = sqlsrv_fetch_array( $dbRes3, SQLSRV_FETCH_ASSOC);
							if ($row)
							{
								
								$saldogral1 = $row['saldo'];
								$saldogral ='$ '.number_format($row['saldo'],2);
								$saldototal= '$ '.number_format($saldogral1 + $mora1,2);
								$saldototalNumber=$saldogral1+$mora1;
								$Pagado = trim($row['pagado']);
							}
							else
							{
								$saldototalNumber = 0.00;
								$saldogral1 = 0.00;
								$saldogral ='$ 0.00';
								$saldototal= '$ 0.00';
							}
							
							$consulta ="select * from carterdconvenio where fml='".$fml."' and cvesit <'22709000'";
							$dbRes2 = sqlsrv_query($conexion, $consulta);
							$row = sqlsrv_fetch_array($dbRes2);
							if ($row)
							{
								$fecconvi = $row['fecconvi'];
								$fecconvi1=substr($fecconvi,6,2)."/".substr($fecconvi,4,2)."/".substr($fecconvi,0,4);
								
								$mensconv = $row['mensconv'];
								$Importe= '$ '.number_format($mensconv,2,'.',',');
								$convenio = $row['convenio'];
								$mesplazo = $row['mesplazo'];	
								//$Importe = '$ '.number_format($row->impmen,2,'.',',');			
							}
							else
							{
								$fecconvi = "";
								$fecconvi1="";
								$mensconv = 0;
								$Importe= '$ 0.00';
								$convenio = "";
								$mesplazo = "";
							}
							
?>
							<tr>
                            <td width="109">&nbsp;</td>
							<td height='20' align='left' width="120"><font class ='texto12'><b>Saldo</b></font></td>
							<td  align='left' class ='texto12'><?php echo $saldogral;?></td>
							
								<td width="159" height='20' align='left'><font class ='texto12'><b>Salto total:</b></font></td>
								<td width="673" align='left' class="texto12" title="Permite revisar el detalle de los cargos."><a href="Cartera/php_ajax/cargos.php?idfml=<?php echo $fml;?>"><?php echo $saldototal;?></a></td>
							</tr>
							
			<?php			}
						
					?>
					</font>
			
			<tr bgcolor="#FFFFFF">
				<th width="100%" colspan="8" align="center" class="TituloDForma">Datos de Escrituración</th>
			</tr>
	
					
					<?php
					$ssql = "SELECT * from escritdescritura";
					$ssql = $ssql." WHERE fml='$fml' and cvesit < '23109000' ";
					$username = $_COOKIE['ID_my_site']; 
					$dbRes20 = sqlsrv_query($conexion, $ssql); 
					$i = 0;    
					
					//echo $ssql;
					while($row = sqlsrv_fetch_array($dbRes20))
					{
						$expcat=$row['expcat'];
						$folesc=$row['folesc'];
						$numesc=$row['numesc'];
						$regbajo=$row['regbajo'];
						$regvol=$row['regvol'];
						$reglibro=$row['reglibro'];
						$regsec=$row['regsec'];
						//echo $numesc;
	?>
						<tr>
							<td width="109">&nbsp;</td>
                            <td height='20' align='left'><font class ='texto12'><b>Expediente catastral</b></font></td>
							<td colspan ='1' height='20' align='left'><font class ='texto12'><?php echo $expcat;?></font></td>
							<td height='20' align='left'><font class ='texto12'><b>Folio de escritura:</b></font></td>
							<td colspan ='1' height='20' align='left'><font class ='texto12'><?php echo $folesc;?></font></td>
							</tr>
							<tr>
                            <td width="109">&nbsp;</td>
							<td height='20' align='left'><font class ='texto12'><b>Numero de escritura:</b></font></td>
							<td colspan ='1' height='20' align='left' class ='texto12'><?php echo $numesc;?></td>
							<td height='20' align='left' colspan="1">
								<table width="100%">
									<tr>
										<td colspan ='1' height='32' align='left' width="100"><font class ='texto10'><b>Bajo</b></font></td>
										<td colspan ='1' height='32' align='left'><font class ='texto10'><?php echo $regbajo;?></font></td>
										<td height='32' align='left' width="100"><font class ='texto10'><b>Volumen:</b></font></td>
										<td colspan ='1' height='32' align='left'><font class ='texto10'><?php echo $regvol;?></font></td>
									</tr>
									<tr>
										<td height='20' align='left'><font class ='texto10'><b>Libro:</b></font></td>
										<td colspan ='1' height='20' align='left'><font class ='texto10'><?php echo $reglibro;?></font></td>
										<td height='20' align='left'><font class ='texto10'><b>Secci&oacute;n:</b></font></td>
										<td colspan ='1' height='20' align='left'><font class ='texto10'><?php echo $regsec;?></font></td>
									</tr>
								</table>
							</td>
							</tr>
		<?php			}		
					?>
					</font>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<table summary="" cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr bgcolor="#FFFFFF">
					<th width="100%" align="center" class="subtituloverde">SEGUIMIENTO DE ESCRITURAS</th>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%">
			<table width="400" align="center" >
				<thead>
					<tr>
						<th align="center" class="texto10" width="100">Etapa</th>    
						<th align="center" class="texto10" width="200">Descripción</th>   
						<th align="center" class="texto10" width="100">Fecha</th> 
					</tr>
				</thead>
				<tbody class="resultadobusqueda">
				<?php
					$consulta ="select * from escritetafml where fml='".$fml."'";
					//echo $consulta;
					$dbRes3 = sqlsrv_query($conexion, $consulta);
					//$row = odbc_fetch_object($dbRes3);
					$i=0;
					while($row = sqlsrv_fetch_array($dbRes3))
					{
		
						$etapa =  $row['etapa'];
						$descripcion =$row['descripcion'];
						$fecha= $row['fecha'];
					?>
					
						<tr class="d<?php echo ($i%2);?>">
							<td align='center' class="texto10" width="100"><?php echo $etapa;?></td>    
							<td align="left"class="texto10" width="200"><?php echo $descripcion;?></td>   
							<td align='center' class="texto10" width="100"><?php echo fechSegAEuropea($fecha);?></td>
						</tr>
				<?php		
						$i++;
					}
			?>
		 		</tbody>
			</table>
		</td>
	</tr>
    <tr>
    	<td>
        <?php
		$datos=array();
		$tsql_callSP = "SELECT CONVERT(varchar(10),fecha_estudio,103) as feccap, descripcion, id   FROM estudiofisico a
				LEFT JOIN catsmscat b ON  a.condiciones=b.scat
				WHERE a.fml='$fml' ORDER BY id ASC";//Arma el procedimeinto almacenado
			
			//echo $tsql_callSP;
			$stmt = sqlsrv_query($conexion, $tsql_callSP);
			if( $stmt === false )
			{
				 die( print_r( sqlsrv_errors(), true));
			}	
			
			$i=0;
			while( $row = sqlsrv_fetch_array( $stmt , SQLSRV_FETCH_ASSOC))
			{
				$datos[$i]['feccap'] = trim($row['feccap']);
				$datos[$i]['descripcion'] = trim($row['descripcion']);
				$datos[$i]['idef'] = trim($row['id']);
				$i++;
			} 
		?>
        	                </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
	
	<TR><TD>&nbsp;</TD></TR>
	<tr>
		<td width="100%" align="center"  class="subtituloverde">
			<table cellpadding="0" cellspacing="0" width="100%"align="center" border="0">
				<tr bgcolor="#FFFFFF">
					<th width="100%" align="center" class="subtituloverde">HISTORIAL DE LOTE </th>
				</tr>
			</table>
		</td>
	</tr>

    <tr>
                    <td class="texto10" valign="middle">Observaciones:</td>
                    <td><textarea></textarea></td>
                    <td valign="middle" class="texto10">Usuario:</td><td><input type="password" id="usuario" name="usuario" /></td>
                    <td><input type="button" value="Correcto" onClick="javascript: location.href='buscar_expUnico_escaner.php'" />
                    <input type="button" value="Incorrecto" onClick="javascript: location.href='buscar_expUnico_escaner.php'" /></td>
                    </tr>
</table>
</body>
</html>
