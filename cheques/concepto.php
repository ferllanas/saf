<?php
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
			date_default_timezone_set('America/Mexico_City');
			
	require_once("../connections/dbconexion.php");
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);

	$folio = trim($_REQUEST['folio']);
	//$concepto = "";
	$command= "select * from egresosmsolche WHERE folio=$folio";
	$stmt = sqlsrv_query( $conexion,$command);
	$i=0;
	while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
	{
		$concepto=utf8_decode($row['concepto']);
		$c_definitivo=utf8_decode($row['c_definitivo']);
		$i++;
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="../prototype/prototype.js"></script>
<script language="javascript" src="../prototype/jQuery.js"></script>

<script language="javascript">
	function graba_concepto()
	{
		var folio=document.getElementById('folio').value;	
		var connvo=document.getElementById('concepto_nvo').value;
		//location.href="php_ajax/cambia_concepto.php?folio="+folio+"&connvo="+connvo;
					var datas = {
						 pfolio: folio,
						 pconnvo: connvo,
						 };
					
					jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'php_ajax/cambia_concepto.php',
						dataType: "json",
						data: datas,
						error: function(request,error) 
						{
						 console.log(arguments);
						 alert ( " Can't do because: " + error );
						},
						success: function(resp)
						{ console.log(arguments);
							var myObj = resp;
							
							///NOT how to print the result and decode in html or php///
							console.log(myObj);
	
							if(resp.length>0)
							{
								window.close();
							}
							else
							{
								alert("Actualizacion Rechazada.");
							}
						}
						
						});
	}

	function cerrar()
	{
		window.close();
	}


</script>

<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="660" height="325" border="0" align="center" bordercolor="#000000">
  <tr>
    <td height="35" colspan="2" class="subtituloverde12"><div align="center"><strong class="texto13B">Modificaci&oacute;n de Concepto </strong></div></td>
  </tr>
  <tr>
    <td width="89" height="35" class="texto10">Folio:</td>
    <td width="555"><input name="folio" id="folio" type="text" size="10" value="<?php echo $folio;?>" readonly>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto10">Concepto Actual </td>
    <td><span class="texto8"><span class="texto8" style="z-index:0;">
      <textarea name="conc" id="conc" readonly cols="150" rows="5" class="texto8"><?php echo $concepto;?></textarea>
    </span></span></td>
  </tr>
  <tr>
    <td class="texto10">Concepto Nuevo </td>
    <td><span class="texto8"><span class="texto8" style="z-index:0;">
      <textarea name="concepto_nvo" id="concepto_nvo" cols="150" rows="5" class="texto8"><?php echo $c_definitivo;?></textarea>
    </span></span></td>
  </tr>
  <tr>
    <td height="39" class="texto10"><input type="button" name="grabar" value="Guardar" onClick="graba_concepto()"></td>
    <td align="center"><span class="texto10">
      <input type="button" name="Submit2" value="Cancelar" onClick="cerrar()">
    </span></td>
  </tr>
</table>
</body>
</html>
