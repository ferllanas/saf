<?php
// Archivo de consultas de Solicitud de Cheques
include_once 'lib/ez_sql_core.php'; 
//include_once 'lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
				$command= "select a.folio, CONVERT(varchar(12),a.fpago, 103) as fpago, b.nomprov, a.descrip,a.total, a.estatus from egresosmcheque a 
											LEFT JOIN compramprovs b ON a.prov=b.prov WHERE a.folio LIKE '%" . substr($_REQUEST['query'],1) . "%' and a.estatus<50 order by
											a.folio";
	}
	if($opcion==2)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
				

				$command= "select a.folio, CONVERT(varchar(12),a.fpago, 103) as fpago,b.nomprov, a.descrip,a.total, a.estatus from egresosmcheque a ";
				$command .="LEFT JOIN compramprovs b ON a.prov=b.prov WHERE nomprov LIKE '%" . $nom1 . "%' and a.estatus<50";
											if($cuenta>1)
											{
												$command .="and b.nomprov LIKE '%" . $nom2 . "%'";
											}
											$command .=" order by a.folio";
	}
	
	//echo $command;
	if($opcion==3)
	{
				$command= "select a.folio, CONVERT(varchar(12),a.fpago, 103) as fpago,b.nomprov, a.descrip,a.total, a.estatus, c.nomprov from egresosmcheque a 
								LEFT compramprovs c ON  a.prov=c.prov 
								LEFT nominamdepto b ON a.depto=b.depto 
								WHERE b.nomdepto LIKE '%" . substr($_REQUEST['query'],1) . "%' and a.estatus<50 order by a.folio";
	}		
	//	echo $opcion;
	if($opcion==4)
 	{
				$command= "select a.folio, CONVERT(varchar(12),a.fpago, 103) as fpago,b.nomprov, a.descrip,a.total, a.estatus from egresosmcheque a 
											INNER JOIN compramprovs b ON a.prov=b.prov WHERE a.descrip LIKE '%" . substr($_REQUEST['query'],1) . "%' and a.estatus<50 order
											by a.folio";

     }
	//echo "Antes de pasar al 5";
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!
				$fecini=substr($_REQUEST['query'],1,11);
				$fecfin=substr($_REQUEST['query'],11,11);
				$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				
				//echo $fini." _ ".$ffin;
				
				$command= "select a.folio, CONVERT(varchar(12),a.fpago, 103) as fpago,b.nomprov, a.descrip,a.total, a.estatus 
							from egresosmcheque a INNER JOIN compramprovs b ON a.prov=b.prov where a.falta >= '" . $fini . "' and a.falta <='" . $ffin . "' and a.estatus<50";
				 
	}	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$datos[$i]['folio']=trim($row['folio']);
					$datos[$i]['fpago']=trim($row['fpago']);					
					$datos[$i]['nomprov']=trim($row['nomprov']);
					$datos[$i]['total']=trim($row['total']);
					$datos[$i]['descrip']=utf8_decode($row['descrip']);
					$datos[$i]['estatus']=trim($row['estatus']);
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el jason
?>