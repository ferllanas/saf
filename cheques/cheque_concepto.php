<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../prototype/prototype.js"></script>
<script language="javascript" src="javascript/busqueda2.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../../javascript_globalfunc/funcionesGlobales.js"></script>
</head>

<body>

<table width="100%" border="0">
  <tr class="texto10">
    <td colspan="7" class="TituloDForma">Concepto Definitivo de Solicitud de Cheques </td>
  </tr>
  <tr class="texto10">
    <td height="24" colspan="7"><hr class="hrTitForma"></td>
  </tr>
  <tr class="texto10">
    <td width="77"><div align="right"><strong># Solicitud </strong></div></td>
    <td width="100">
	  <div align="left" id="fol" name="fol" style="visibility:visible; position:relative; width:100; z-index:1;">
      	<input type="text" name="folio" id="folio" size="10" onKeyUp="searchcontabx();" autocomplete="off" tabindex="1">
		<div id="search_suggestcontabx" style="z-index:2;" > </div>	 
	  </div>
    </td>
    <td width="289"><div align="right"><strong>Factura</strong></div></td>
    <td width="236"><input type="text" name="factura" id="factura" size="10"></td>
    <td width="73"><div align="right"></div></td>
    <td width="59">&nbsp;</td>
    <td width="85">&nbsp;
    </td>
  </tr>
  <tr class="texto10">
    <td><div align="right"><strong>Beneficiario</strong></div></td>
    <td colspan="3"><input type="text" name="beneficiario" id="beneficiario" size="100"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0">
  <tr class="texto10">
    <td width="77"><div align="right"><strong>Subtotal</strong></div></td>
    <td width="145"><input type="text" name="subtotal" id="subtotal" size="10"></td>
    <td width="26"><div align="right"><strong>Iva</strong></div></td>
    <td width="78"><input type="text" name="iva" id="iva" size="10"></td>
    <td width="128"><div align="right"><strong>Importe</strong></div></td>
    <td width="469"><input type="text" name="importe" id="importe" size="10"></td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" height="342" border="0">
  <tr>
    <td class="texto12"><strong>Concepto</strong></td>
  </tr>
  <tr>
    <td><span class="texto8"><span class="texto8" style="z-index:0;">
      <textarea name="concepto" id="concepto" tabindex="7" cols="230" rows="8" class="texto8"></textarea>
    </span></span></td>
  </tr>
  <tr>
    <td class="texto12">&nbsp;</td>
  </tr>
  <tr>
    <td class="texto12"><strong>Concepto Definitivo </strong></td>
  </tr>
  <tr>
    <td><span class="texto8"><span class="texto8" style="z-index:0;">
      <textarea name="c_definitivo" id="c_definitivo" tabindex="7" cols="230" rows="8" class="texto8"></textarea>
    </span></span></td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td><hr class="hrTitForma"></td>
  </tr>
</table>
<table width="446" height="33" border="0" align="center">
  <tr>
    <td width="223"><input type="button" align="left" name="guardar" id="guardar" value="Actualizar" onClick="Actualiza()">
    </td>
    <td width="207"><input type="button" align="right" name="deshacer" id="deshacer" value="Deshacer" onClick="deshacer()"></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
