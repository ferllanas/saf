<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	require_once("../../connections/dbconexion.php");
	
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

$usuario = $_COOKIE['ID_my_site'];
//$usuario='001349';
$folio = 0;
$folio = trim($_REQUEST['folio']);
$opago = trim($_REQUEST['opago']);

if ($conexion)
{		
	$ejecuta ="{call sp_egresos_B_dopago_presup(?,?)}";
	$variables = array(&$folio,&$usuario);
	$R = sqlsrv_query($conexion, $ejecuta, $variables);
	//echo $ejecuta;
	//print_r($variables);

	if( $R === false )
	{
		 $fails= "Error in statement execution.\n";
		 $fails=true;
		 print_r($variables);
		 die( print_r( sqlsrv_errors(), true));
	}
	$fails=false;
}

header('Location: ../cancela_contab_presup.php');
?>