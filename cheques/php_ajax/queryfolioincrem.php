<?php
require_once("../../connections/dbconexion.php");
require_once("../../Administracion/globalfuncions.php");
if (version_compare(PHP_VERSION, '5.1.0', '>='))
	date_default_timezone_set('America/Mexico_City');
	

$sct="";
$datos= array();
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($_GET['q'] && $conexion)
{

	$terminos = explode(" ",$_GET['q']);
	//$termino=$_GET['q'];
	$command= "SELECT a.folio,a.concepto,a.subtotal,a.iva,a.importe,a.prov,a.factura,a.c_definitivo,";
	$command.= "b.nomprov from egresosmsolche a left join compramprovs b on a.prov=b.prov WHERE";
	$paso=false;
	for($i=0;$i<count($terminos);$i++)
	{
		if (count($terminos)==1)
		{
			if (is_numeric($terminos[0]))
				$command.=" a.folio LIKE '%".$terminos[$i]."%'";
			else
				$command.=" a.concepto LIKE '%".$terminos[0]."%'";
		}		
	}
	$command.=" AND a.estatus<50  ORDER BY a.folio ASC";
	//echo $command;
	$getProducts = sqlsrv_query( $conexion_srv,$command);
	if ( $getProducts === false)
   	{ 
		$resoponsecode="02";
		$descriptioncode= FormatErrors( sqlsrv_errors()) ; 
		//echo $descriptioncode;
	}
	else
	{
	
		$resoponsecode="Cantidad rows=".count($getProducts);
		//$i=0;
		while( $row = sqlsrv_fetch_array( $getProducts, SQLSRV_FETCH_ASSOC))
		{
			$sct.= htmlentities(trim($row['folio']))."@".trim($row['concepto'])."@".trim($row['subtotal'])."@".trim($row['iva'])."@".trim($row['importe'])."@".trim($row['factura'])."@".trim($row['prov'])."@".trim($row['nomprov'])."@".trim($row['c_definitivo'])."\n";
		}
	}
	echo $sct;

}
?>