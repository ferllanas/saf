<?php
/*$html="
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<style type='text/css'>
	body 
	{	
		font-family:'Arial';
		font-size:7;
	}
	</style>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>Emision de Cheques</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
	<link type='text/css' href='../../css/estilos.css' rel='stylesheet'>
	<script language='javascript' src='../../prototype/jQuery.js'></script>
	<script language='javascript' src='../../prototype/prototype.js'></script>
</head>

<body>
";*/
    if (version_compare(PHP_VERSION, "5.1.0", ">="))
 	require_once("../../dompdf/dompdf_config.inc.php");
	date_default_timezone_set("America/Mexico_City");
	require_once("../../connections/dbconexion.php");
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	include("../../Administracion/globalfuncions.php");

 // Se rescata la informacion del Javascript comcheque.js
//	$numemp = $_REQUEST["numemp"];
//	$nomemp = $_REQUEST["nomemp"];

	$numprov = $_REQUEST["numprov"];
	$cvedepto = $_REQUEST["cvedepto"];
	$subtotal = $_REQUEST["subtotal"];
	$iva = $_REQUEST["iva"];
	$importe = $_REQUEST["importe"];
	$concepto = utf8_encode($_REQUEST["concepto"]);
	$usuario = $_REQUEST["usuario"];
	$uresp = $_REQUEST["uresp"];
	$nomresp = $_REQUEST["nomresp"];
	$provname1 = $_REQUEST["provname1"];
	$factura = $_REQUEST["factura"];
	$nomfirma = $_REQUEST["nomfirma"];
	$nomdir2 = $_REQUEST["nomdir"];
	$nomdepto = $_REQUEST["nomdepto"];
	$nomcoord = $_REQUEST["nomcoord"];
	$rc = $_REQUEST["rc"];

	$subtotal = str_replace(",","",$subtotal);
	$subtotal = number_format($subtotal,2,".","");
	
	$iva = str_replace(",","",$iva);
	$iva = number_format($iva,2,".","");

	$importe = str_replace(",","",$importe);
	$importe = number_format($importe,2,".","");
	
	$ret_car =  $_REQUEST["ret_car"];
	if($ret_car==1)
	{
		$tiporc='-';
	}
	if($ret_car==2)
	{
		$tiporc='+';
	}
	
	$rc = str_replace(",","",$rc);
	$rc = number_format($rc,2,".","");
	
	
	//echo $importe;
	
//    F.SP Regresa ya los datos con informacion, separando los que regresa el SP y los que ya vienen desde la captura de la Forma
	  list($folio,$fecha,$hora,$nomdir) = fun_creaCheque($numprov,$cvedepto,$importe,$concepto, $usuario, $uresp, $factura,$subtotal,$iva,$nomfirma,$ret_car,$rc);//firma);

	$subtotal= number_format($subtotal,2);
	$iva= number_format($iva,2);

	$myimport = str_replace(",","",$importe);
   	$TotalImporte =num2letras($myimport); // Numeros a letras
	$importe= number_format($importe,2);
	
	//echo .'***('.$TotalImporte.')***'.\n\n.'
/*	
$html.="
<table width='600' border='0' align='center'>
   	    <tr>
		  <td width='100%'>
             <table width='100%' border='0'>
				<p></p>
				  <tr>
					<td align='left' width='20'><img src='../../imagenes/fomerrey.png' width='200' height='76' /></td>
					<td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
					  COORDINADOR DE EGRESOS Y CONTROL PATRIMONIAL<BR />
					  SOLICITUD DE CHEQUE - GENERAL</b></td>
					<td align='right' width='20'><img src='../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
				  </tr>
		      </table>			  
						<table width='100%'>
						   <tr>
							<td width='100%' align='left' class='texto9'><b>DIRECCION :</b> ". $nomdir ."</td><br>
						   </tr>
						   <tr>
							<td width='70%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
							 <td width='30%' align='right' class='texto8'><b>FECHA:</b>".$fecha."</td>
						   </tr>
						</table>			
            <table width='100%' border='0'>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto13B'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto8'><b>FACTURA:</b> ".$factura."</td>
              </tr>
            </table>	
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto9'><b>BENEFICIARIO:</b> ".$provname1."</td>
				<td width='50%' align='right' class='texto9'><b> IMPORTE: </b> ".$importe." </td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto8'><b>CANTIDAD CON LETRA:</b>". strtoupper($TotalImporte) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>".utf8_decode($concepto)."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='31%'>&nbsp;</td>
                <td width='4%'>&nbsp;</td>
                <td width='20%'>&nbsp;</td>
                <td width='4%'>&nbsp;</td>
                <td width='40%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.<br>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td align='center' class='texto6'>&nbsp;</td>
                <td class='texto6'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center'  valign='bottom' class='texto8'><hr></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center'  valign='bottom' class='texto8'><hr></td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomfirma."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='20%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' valign='top' class='texto8'><B>".$nomdepto."</B></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' valign='top' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>              
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'><b></b></td>
              </tr>
			  
            </table>            
	 	  </td>
  	    </tr>  
	    <tr>
			<td width='100%'>
             <table width='100%' border='0'>
              <tr>
                <td align='left' width='20'><img src='../../imagenes/fomerrey.png' width='200' height='76' /></td>
                <td class='texto9' align='center'><b>DIRECCION DE ADMINISTRACION Y FINANZAS<br />
                  COORDINADOR DE EGRESOS Y CONTROL PATRIMONIAL<BR />
                SOLICITUD DE CHEQUE - GENERAL</b></td>
                <td align='right' width='20'><img src='../../".$imagenPDFSecundaria."' width='169' height='101' /></td>
              </tr>
            </table>
						<table width='100%'>
						   <tr>
							<td width='100%' align='left' class='texto9'><b>DIRECCION :</b> ". $nomdir ."</td><br>
						   </tr>
						   <tr>
							<td width='70%' align='left' class='texto9'><b>COORDINACION :</b> ". $nomcoord ."</td>
							 <td width='30%' align='right' class='texto8'><b>FECHA:</b>".$fecha."</td>
						   </tr>
						</table>			
            <table width='100%' border='0'>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto13B'>SOLICITUD No. ".$folio."</td>
                <td width='50%' align='right' class='texto8'><b>FACTURA:</B> ".$factura."</td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td width='50%' align='left' class='texto9'><B>BENEFICIARIO:</B> ".$provname1."</td>
				<td width='50%' align='right' class='texto9'><b> IMPORTE: </b> ".$importe." </td>
              </tr>
        </table>
            <table width='100%' border='0'>
              <tr>
                <td align='left' class='texto8'>  </tr>
              <tr>
              <td width='100%' align='left' class='texto8'><b>CANTIDAD CON LETRA:</b>". strtoupper($TotalImporte) ."</td>  </tr>
                <tr>
                  <td align='left' class='texto8'>      </tr>
            </table>
            <table width='100%' border='0'>
              <tr>
                <td width='100%' align='left' class='texto8'><b>CONCEPTO:</b></td>
              </tr>
              <tr>
                <td align='left' class='texto7'>".utf8_decode($concepto)."</td>
              </tr>
        </table>
			<hr>
        <table width='100%' border='0'>
      <tr>
                <td width='31%'>&nbsp;</td>
                <td width='4%'>&nbsp;</td>
                <td width='20%'>&nbsp;</td>
                <td width='4%'>&nbsp;</td>
                <td width='40%'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'><b>SOLICITANTE</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>Vo. Bo.<br>CUENTA PRESUPUESTAL</b></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><b>AUTORIZA</b></td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto6'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
              </tr>
              <tr>
                <td align='center' valign='bottom' class='texto8' ><hr></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'>&nbsp;</td>
                <td class='texto8'>&nbsp;</td>
                <td align='center'  valign='bottom' class='texto8'><hr></td>
              </tr>
              <tr>
                <td align='center' class='texto8'><B>".$nomfirma."</B></td>
                <td class='texto8'>&nbsp;</td>
                <td width='20%' align='center' class='texto8'><B>CUENTA CONTABLE</B></td>
                <td class='texto8'>&nbsp;</td>
                <td align='center' class='texto8'><B>".$nomresp."</B></td>
              </tr>
              <tr>
                <td width='30%' align='center' valign='top' class='texto8'><B>".$nomdepto."</B></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' class='texto8'></td>
                <td width='10%' class='texto8'>&nbsp;</td>
                <td width='30%' align='center' valign='top' class='texto8'><b>DIRECCION DE ADMINISTRACION Y FINANZAS</b></td>
              </tr>
            </table>
    <table width='100%' border='0'>
              
              <tr>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'>&nbsp;</td>
                <td class='texto7'>&nbsp;</td>
                <td align='left' class='texto8'></td>
             </tr>
            </table>
	 	  </td>
  	    </tr>  
	  </table>
	</body>
</html>";	
*/
  //echo $html;
  /*

// Variables de impresión del sistema de Almacen a PDF

  $dompdf = new DOMPDF();
  $dompdf->set_paper("letter","portrait");
  $dompdf->load_html($html);
  $dompdf->render();
 
  $pdf = $dompdf->output(); 
  file_put_contents("../pdf_files/$folio.pdf", $pdf);
 // $dompdf->stream("../pdf_files/$folio.pdf");
*/ 
	echo json_encode($folio);
  
// Se genera la informacion a insertar en el sp_egresos_A_msolche
//function fun_creaCheque($numprov,$depto,$importe,$concepto, $usuario, $uresp, $nomresp, factura) se incluira factura en el store

//Recibo los valores a insertar en el procedimiento almacenado ....
function fun_creaCheque($numprov,$cvedepto,$importe,$concepto, $usuario, $uresp, $factura, $subtotal, $iva,$firma,$ret_car,$rc)
{
	global $server,$odbc_name,$username_db ,$password_db;

	$fails=false;
	
	
	//echo $server.','.$username_db.','.$password_db.','.$odbc_name;
	$infoconexion=array("UID" => $username_db,"PWD" => $password_db, "Database" => $odbc_name);//Arma cadena de conexion
	$conexion = sqlsrv_connect($server,$infoconexion);//Crea la conexion
	




	// A.SP Una vez almacenados los datos en la variable del procedimiento almacenado se procede a llamarlo para indicar cuantos campos se van a ingresar
	$cheqsql_callSP ="{call sp_egresos_A_msolche(?,?,?,?,?,?,?,?,?,?,?)}";//Arma el procedimeinto almacenado (Llamdo al procedimiento)


	$params = array(&$numprov,&$cvedepto,&$importe,&$concepto, &$usuario, &$uresp, &$factura,&$subtotal,&$iva,&$ret_car,&$rc);//Arma parametros de entrada al sp_egresos_A_msolche
	$options = array("QueryTimeout"=>180);//agrega un tiempo de espera de 180 segundos
	
	$stmt = sqlsrv_query($conexion, $cheqsql_callSP, $params);
    // Verifica que informacion trae $stmt
	if( $stmt === false )
	{
		 $fails="Error in statement execution.\n";
		 $fails=true;
		 die( print_r( sqlsrv_errors(), true)." sp_egresos_A_msolche ". print_r($params,true));
	}
	
	if(!$fails)
	{
		// B.SP Arrastra la iformacion desde el SP
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ))
		{
		// C.SP Antes de recibir los datos del procedimiento almacenado se validan las variables		
			$id = $row["folio"];
			$fecha = $row["fecha"];
			$hora = $row["hora"];
			$nomdir = $row["nomdir"];
		// D.SP En este caso los valores que entrega el procedimiento almacenado se les puede dar un formato especial acorde a las necesidades
			list($mes, $dia, $anio) = explode("/", $fecha);
			$meses="ENEFEBMARABRMAYJUNJULAGOSEPOCTNOVDIC";
			$fecha=$dia."/".substr($meses,$mes*3-3,3)."/".$anio ;
			//$cvedepto = $row["cvedepto"];
			
		}
		sqlsrv_free_stmt( $stmt);
	}

	
	sqlsrv_close( $conexion);
		// E.SP En esta momento regresa la informacion a almacenar en el Array
	return array($id,$fecha,$hora,$nomdir);
}
?>