<?php
// Archivo de consultas de Solicitud de Cheques
include_once 'lib/ez_sql_core.php'; 
//include_once 'lib/ez_sql_mysql.php';

if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
// Inicia el Switch de busqueda 
$fecini = "";
$datos=array();
$opcion= substr($_REQUEST['query'],0,1);
$query=$_REQUEST['query'];
$fecini=substr($_REQUEST['query'],1,10);
$fecfin=substr($_REQUEST['query'],11,11);

$nom1='';
$nom2='';
	//echo $opcion;
	if($opcion==1)
	{
				$command= "select x.opago, x.folio as fol, a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, 
							b.nomprov, a.c_definitivo as concepto,a.importe, x.estatus from egresosmopago x left join egresosmsolche a on x.folio=a.folio 
							LEFT JOIN compramprovs b ON a.prov=b.prov WHERE a.folio LIKE '%" . substr($_REQUEST['query'],1) . "%' and 
							x.estatus=0 order by a.folio";
	}
	if($opcion==2)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
				

				$command= "select x.opago, x.folio as fol,a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomprov, a.c_definitivo as concepto,  
							a.importe, a.estatus from egresosmopago x left join egresosmsolche a on x.folio=a.folio 
							LEFT JOIN compramprovs b ON a.prov=b.prov WHERE b.nomprov LIKE '%" . substr($_REQUEST['query'],1) . "%' and x.estatus=0 ";
											if($cuenta>1)
											{
												$command .="and b.nomprov LIKE '%" . $nom2 . "%'";
											}
											$command .=" order by a.folio";
	}
	
	//echo $command;
	if($opcion==3)
	{
				$cuenta = count(explode(" ", substr($_REQUEST['query'],1)));
				if($cuenta>1)
				{
				    list($nom1, $nom2) = explode(" ", substr($_REQUEST['query'],1));
				}
				else
				{
					$nom1=substr($_REQUEST['query'],1);
				}
	
				$command= "select x.opago, x.folio as fol, a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomdepto, a.c_definitivo as concepto, 
								a.importe, a.estatus, c.nomprov from egresosmopago x left join egresosmsolche a on x.folio=a.folio 
								LEFT join compramprovs c ON  a.prov=c.prov LEFT join nominamdepto b ON a.depto=b.depto 
								WHERE b.nomdepto LIKE '%" . substr($_REQUEST['query'],1) . "%' and x.estatus=0 order by a.folio";
	}		
	//	echo $opcion;
	if($opcion==4)
 	{
				$command= "select x.opago, x.folio as fol, a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomprov, 
							a.c_definitivo as concepto,a.importe, a.estatus from egresosmopago x left join egresosmsolche a on x.folio=a.folio 
							INNER JOIN compramprovs b ON a.prov=b.prov WHERE a.concepto LIKE '%" . substr($_REQUEST['query'],1) . "%' and x.estatus=0 order
							by a.folio";

     }
	//echo "Antes de pasar al 5";
	if($opcion==5)
 	{			// Desconcatena las fechas, que se han unido en la cadena, muy interesante !!!

				//$fini=substr($fecini,6,4).substr($fecini,3,2).substr($fecini,0,2);
				//$ffin=substr($fecfin,6,4).substr($fecfin,3,2).substr($fecfin,0,2);
				//echo $fini." _ ".$ffin;
				list($dd,$mm,$aa)= explode('/',$fecini);
				$fecini=$aa.'-'.$mm.'-'.$dd;
				
				list($dd2,$mm2,$aa2)= explode('/',$fecfin);
				$fecfin=$aa2.'-'.$mm2.'-'.$dd2;
				
				$command= "select x.opago, x.folio as fol, a.folio, CONVERT(varchar(12),a.falta, 103) as fecha, b.nomprov, a.c_definitivo as concepto,a.importe,a.estatus 
							from egresosmopago x left join egresosmsolche a on x.folio=a.folio INNER JOIN compramprovs b ON a.prov=b.prov 
							where a.falta >= CONVERT(varchar(12),'$fecini', 103) 
							and a.falta <=CONVERT(varchar(12),'$fecfin', 103) and x.estatus=0 order by fecha";
				 
	}	
				//echo $command;
				$stmt2 = sqlsrv_query( $conexion,$command);
				$i=0;
				while( $row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC))
				{
					// Comienza a realizar el arreglo, trim elimina espacios en blanco		
					$datos[$i]['folio']=trim($row['folio']);
					$datos[$i]['fecha']=trim($row['fecha']);					
					$datos[$i]['nomprov']=trim($row['nomprov']);
					//$datos[$i]['importe']=trim($row['importe']);
					$datos[$i]['importe'] = number_format(trim($row['importe']), 2);
					$datos[$i]['concepto']=utf8_decode($row['concepto']);
					$datos[$i]['estatus']=trim($row['estatus']);
					$i++;
				}
					//$proveedor = $db->get_results);
					// realiza la consulta a regresar los datos a la forma, los porcentajes son para definir autosearch

echo json_encode($datos);   // Los codifica con el json
?>