<?php
if (version_compare(PHP_VERSION, '5.1.0', '>='))
		date_default_timezone_set('America/Mexico_City');
		
require_once("../connections/dbconexion.php");
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

// Informacion desde solcheque
	$usuario = $_COOKIE['ID_my_site']; //Misma variable para Almacen
	$depto = $_COOKIE['depto']; //Misma variable para Almacen
	$privsolche = $_COOKIE['privsolche']; //Misma variable para Almacen

//	$privsolche = 80;
//	$depto = 2010;
//	$usuario = 2294;

	$x_depto = $depto;
	$cvedepto="";
	$concepto = "";
	
	// Variables que vienen desde la vista
    $importe=(0);
	
	$numemp="";
	$nomemp="";
	
	$catego="";
	$nomcatego="";
	$depto="";
	$nomdepto="";
	$estatus="";
	$nomprov = "";
	

// Datos para recuperar información del usuario

	$coorduser="";
	$appatuser="";
	$nomuser="";
	$apmatuser="";
	
	
	$numprov = "";
	$provid1 = "";
	$provname1 = "";
	$factura=""; // Campo solicitado por Said el 26 de Octubre 2011
	//$prov3="";

    // Campos de la tabla Egresosmsolche
	$folio="";          // Numero de cheque 
	$fecha_cheque="";   // Fecha de emision del cheque
	$coord_solic = "";  // Coordinacion que solicita
	
	// Informacion de la vista
	$uresp = "";        // Usuario responsable de autorizar el cheque
	$productos= array();
	$personasAutori=array(); // Aqui es donde entraria la vista
	$tiposDRequi=array();
	$datosx=array();
	$datos_iva=array();
	
	$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
	$conexion = sqlsrv_connect($server,$infoconexion);
	if($conexion)
	{
		//Busqueda de los proveedores
		$command= "SELECT *  FROM v_respsolche b";	
		$stmt = sqlsrv_query( $conexion, $command);
		if( $stmt === false)
		{
			echo "Error in executing statement 3.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		
		if(sqlsrv_has_rows($stmt))
		{
			$i=0;
            // Se valida que la informacion en $stmt
			while( $lrow = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
			{
			    
 				    //Esta variable se pone cuando uno va a desplegar una tabla con informacion de detalle, como por ejemplo una factura.
					//$productos[$i]['cve'] =trim($lrow['cve']);
  				    $catego = trim($lrow['catego']);
  				    $nomcatego = trim($lrow['nomcatego']);
  				    $nomdepto = trim($lrow['nomdepto']);					
  				    $estatus = trim($lrow['estatus']);
					$uresp = trim($lrow['numemp']);	
					$depto = trim($lrow['depto']);	
					$nomemp = trim($lrow['nomemp']); // se cambia de $nomemp a $nomemp
 					$i++;
			}
		}
		
		//Busqueda de datos de la vista en donde se ligara al numero de depto para poner el responsable del area SOLICITANTE
		$command= "select a.depto,a.coord, b.nomdepto, a.nombre, a.appat, a.apmat from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto COLLATE DATABASE_DEFAULT 
				   where a.numemp='$usuario'";
		$stmt2 = sqlsrv_query( $conexion, $command);
		if( $stmt2 === false)
		{
			echo "Error in executing statement 1.\n";
			print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
		}
		else
		{
			if(sqlsrv_has_rows($stmt2))
			{
				$i=0;
				while( $lrow = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC))
				{
					$depto = trim($lrow['depto']);
					$coord_solic = trim($lrow['coord']);
					//$dir = trim($lrow['dir']);
					$nomdepto = trim($lrow['nomdepto']);
					$i++;
					//echo "$nomdepto1";
				}
			}
		}
	}

// -------------------------------------------------------------------------------------------------------------------
if ($conexion)
{		
	$consulta = "select * from nomemp.dbo.nominamdepto where ";

	switch($privsolche)
		{
			case 0:
				 $consulta .= " depto=$x_depto and ";
				break;
			case 20:
				$wdepto=substr($depto,0,2);
				$consulta .= "left(depto,2)=$wdepto and ";
				break;
			case 80:
				$consulta .= "";
				break;
		}
		$consulta .= "estatus < '90'";
		//echo $consulta;
	$R = sqlsrv_query( $conexion,$consulta);
	
	$i=0;
	while( $row = sqlsrv_fetch_array( $R, SQLSRV_FETCH_ASSOC))
	{
		$datosx[$i]['depto']= trim($row['depto']);
		$datosx[$i]['nomdepto']= trim($row['nomdepto']);
		$i++;
	}
}
//--------------------------------------------------------------------------------------------------------------------


// Termina info de solcheque

// Obtiene la informacion del proveedor, mediante AutoSearch


$prov = '';
$nomdir2 = '';
if(isset($_REQUEST['numprov']))
	$prov = $_REQUEST['numprov'];

$nombre ="";
if(isset($_REQUEST['provname1']))
	$nombre = $_REQUEST['provname1'];
$datos=array();
//echo $nombre.",".$prov;
	if ($conexion && (isset($_REQUEST['provname1']) || isset($_REQUEST['provname1'])))
	{				
		if( $prov > 0)
		{
			$consulta = "select * from compramprovs where prov=$prov and estatus<'9000'";
		}
		else
		{
			$consulta = "select * from compramprovs where nomprov like '%$nombre%' and estatus<'9000'";
		}
		
	}
 
// Checa muy bien esta variable par hacer el autosearch


$depto = '';
if(isset($_REQUEST['numdepto']))
	$depto = $_REQUEST['numdepto'];

$nomdepto ="";
if(isset($_REQUEST['nomdeptoReq']))
	$nomdepto = $_REQUEST['nomdeptoReq'];
	
$datos=array();
//echo $nombre.",".$prov;
	if ($conexion && (isset($_REQUEST['nomdeptoReq']) || isset($_REQUEST['nomdeptoReq'])))
	{				
		if( $prov > 0)
		{
			$consulta = "select * from compramprovs where prov=$prov and estatus<'9000'";
		}
		else
		{
			$consulta = "select * from compramprovs where nomprov like '%$nombre%' and estatus<'9000'";
		}
		
	}
 
 

/*************************************************************************************************************/
// Vamos a intentar traernos los datos del depto del empleado (Puesto y Nombre)
//Busqueda de datos de la vista en donde se ligara al numero de depto para poner el responsable del area SOLICITANTE
$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);
if($conexion)
{
		$comuser= "select b.nomdepto, a.nombre, a.appat, a.apmat from nomemp.dbo.nominadempleados a left join nomemp.dbo.nominamdepto b on a.depto COLLATE DATABASE_DEFAULT=b.depto  COLLATE DATABASE_DEFAULT
				   where a.numemp='$usuario'";
		$stmtuser = sqlsrv_query( $conexion, $comuser);
		if( $stmtuser === false)
			{
				echo "Error in executing statement 1.\n";
				print_r( sqlsrv_errors());//die( print_r( sqlsrv_errors(), true));
			}
		else
			{
				if(sqlsrv_has_rows($stmtuser))
				{
					$i=0;
					while( $lrow = sqlsrv_fetch_array( $stmtuser, SQLSRV_FETCH_ASSOC))
					{
						$coorduser =trim($lrow['nomdepto']);
						$nomuser =trim($lrow['nombre']);
						$appatuser =trim($lrow['appat']);
						$apmatuser =trim($lrow['apmat']);
						$i++;
					}
				}
			}
}
/*************************************************************************************************************/

		
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Solicitud de Cheques</title>
<meta />
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../prototype/jQuery.js"></script>
<script language="javascript" src="../prototype/prototype.js"></script>
<script language="javascript" src="javascript/busqueda2.js"></script>
<script language="javascript" src="javascript/chequebusqIncProv.js"></script>
<script type="text/javascript" src="javascript/comcheque.js"></script></script>
<script language="javascript" src="../javascript_globalfunc/funciones_AHojaDEstilos.js"></script>
<script language="javascript" src="../javascript_globalfunc/funcionesGlobales.js"></script>

<style type="text/css">
<!--
.Estilo1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>

<body>
	  

<!-- form id="form_requisicion"  name ="form_requisicion" style="width:100%"  metdod="post"  action="php_ajax/crear_cheqpdf.php?" -->
<table width="103%" class="resultadobusqueda">
  
	<tbody id="provsT" class="resultadobusqueda">
	<?php
		/*for($i=0;$i<count($datos);$i++)
		{
		}*/
	?>
	</tbody>
</table>
<table width="100%" border="0">

  <tr>
    <td colspan="2"  align="center" class="TituloDForma">Solicitud de Cheque General
	</td>
  </tr>
  <tr>
    <td colspan="2"  align="center" class="TituloDForma"><span class="texto8 Estilo1"><span class="texto8">
      <input type="text"  style="width:60%" id="nomdepto"  name="nomdepto" value="<?php echo $depto . $nomdepto;?>" onBlur="searchdepto(this.value)" class="caja_toprint texto8"  >
    </span></span></td>
  </tr>
  <tr>
    <td colspan="2"  align="center" class="TituloDForma"><div align="center">
      <input type="hidden" size="5" value="<?php echo $usuario ;?>" name="usuario" border="0" tabindex="1" id="usuario" class="texto11B" readonly>
        <span class="texto11B"><span class="texto8">
        <input type="hidden" id="nomemp" size="50" name="nomemp" value="<?php echo $nomemp;?>">
        </span></span><?php echo $nombre;?>&nbsp;<span class="texto8"><strong>
        <input type="hidden" id="uresp" size="50" name="uresp" value="<?php echo $uresp;?>">
        <input type="hidden" id="nomemp" size="50" name="nomemp" value="<?php echo $nomemp;?>">
        </strong></span><span class="texto10">Director que realiza Autorizaci&oacute;n:</span><span class="texto8">
        <input type="text" id="nomcatego" size="50" tabindex="8" name="nomcatego" value="<?php echo $nomemp;?>" class="caja_toprint texto10" readonly>
    </span></div></td>
  </tr>
  <tr>
    <td colspan="2"  align="center" ><hr class="hrTitForma"></td>
  </tr>
  <tr>
	<td width="47%"  class="texto10">
		<div align="left" style="z-index:4; position:relative; width:590px; top: 0px; left: 0px; height: 24px;" onDblClick="activaDepto()">
	    	Departamento:
			<input type="text" class="texto8" tabindex="1" name="cvedepto" id="cvedepto" style="width:340px;" onKeyUp="searchdepto(this);" autocomplete="off" value="<?php echo $cvedepto;?>">
			<a href="javascript:activaDepto()" class="texto8">Habilita el campo<a/>
			<div id="search_suggestdepto" style="z-index:2;"> </div>
		</div>
		
			    <input class="texto8" type="hidden" id="depto" name="depto" size="10" value="<?php echo $depto;?>">			    
			    <input class="texto8" type="hidden" id="usuario" name="usuario" size="10" value="<?php echo $usuario;?>">
			 
			  <input class="texto8" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">	  <span class="texto8">
			  <input type="text"  style="width:80%" id="nomdepto"  name="nomdepto" disabled value="<?php echo $depto . $nomdepto;?>" onBlur="searchdepto(this.value)" class="caja_toprint texto8"  >
			  </span>
			  
		</td>

    <td width="43%">
          <span class="texto10">Firmas autorizadas:</span> 
		  <select name="firma" id="firma" class="texto8" disabled onChange="busca()" tabindex="2">
            <?php
			//echo "<option value=''> </option>";
			for($i = 0 ; $i<count($datosx);$i++)
			{
				echo "<option value=".$datosx[$i]["numemp"].">".$datosx[$i]["numemp"]."      ".$datosx[$i]["nombre"]."</option>\n";
			//echo "<option value=".$datosx[$i]['nombre']."></option>";
			}
		?>
          </select>
          <!--  <input class="texto8" type="hidden" id="nomfirma" name="nomfirma" size="10" value="<?php echo $datosx[$i]['nomemp'];?>">-->
          <input class="texto8" type="hidden" id="numfirma" name="numfirma" size="10" >
          <input class="texto8" type="hidden" id="nomfirma" name="nomfirma" size="10" >
          <input class="texto8" type="hidden" id="nomdepto2" width="41" name="nomdepto2" size="10" value="<?php echo $datos[$i]['nomdir'];?>">          
          <input class="texto8" type="hidden" id="nomcoord" name="nomcoord" size="10" value="<?php echo $datos[$i]['nomcoord'];?>">
      <input class="texto8" type="hidden" id="nomdir" name="nomdir" size="10" value="<?php echo $nomdir2;?>">	</td>
  </tr>
</table>
<table width="97%" border="0">
  <tr>
    <td width="10" style="top:20px; ">&nbsp;</td>
    <td width="527" class="texto10">
	<div align="left" class="texto8A" style="z-index:1; position:relative; width:510px; top:15px; left:0px;" onDblClick="activaProv()">
	<span class="texto9">Beneficiario</span>
		<input class="texto8" type="text" id="provname1" name="provname1" disabled size="25" style="width:345px;"  onKeyUp="searchProveedor1(this);" autocomplete="off" tabindex="3" value="">
		<a href="javascript:activaProv()" class="texto8">Habilita el campo <a/>         
		<input type="hidden" id="provid1" name="provid1" value ="<?php if(strlen($provid1)>0) echo $provid1; else echo '0'; ?>">
		<input class="texto8" tabindex="1" type="hidden" id="numprov" name="numprov" size="10" value="<?php echo $prov;?>">
		<div id="search_suggestProv" style="z-index:4;" > </div>
	</div>	
	</td>
    <td align="center" width="69"><p class="texto9">Factura</p>
      <p class="texto9">        
        <input class="texto9" size="12" tabindex="4" name="factura" id="factura" disabled/>
      </p></td>
    <td  align="center"width="54" class="texto9"><p>Subtotal</p>
      <p><input class="texto9" size="8" name="subtotal" tabindex="5" id="subtotal" disabled onBlur="cal_iva()" onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
      </p></td>
    <td align="center" width="72" class="texto9"><p>Iva</p>
      <p>        
        <input class="texto9" size="6" name="iva" tabindex="6" id="iva" onBlur="mod_iva()" disabled onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
      </p></td>
    <td width="129" class="texto9"><table width="100%" style="width:100%; " border="0">
      <tr class="texto8">
        <td width="60%"><input name="radio" id="radio1" tabindex="7" onClick="retencion_cargo()" type="radio" value="1">Retencion</td>
         <td width="40%"> <input name="radio" id="radio2" tabindex="8" onClick="retencion_cargo()" type="radio" value="2">Cargo</td>
      </tr>
	  <tr>
	  <td colspan=2 align="center"><input align="bottom" class="texto9" size="8" name="rc" tabindex="9" id="rc" disabled onBlur="act_retencion_cargo()" value=0.00 onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
      </td>
	  </tr>
    </table>    
    </td>
    <td align="center" width="68" class="texto9"><p>Importe</p>
      <p align="center">        
        <input class="texto9" size="8" name="importe" tabindex="10" id="importe" readonly onKeyPress="return aceptarSoloNumeros(this, event);" onKeyUp="asignaFormatoSiesNumerico(this, event)"/>
      </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="960" height="100" border="0">
  <tr>
    <td><span class="texto10">Concepto:</span><span class="texto8"><br>
        <span class="texto8" style="z-index:0;">
        <textarea name="concepto" id="concepto" tabindex="11" disabled cols="240" rows="5" class="texto8"></textarea>
        </span></span></td>
  </tr>
</table>
<table width="97%" border="0">
  <tr>
    <td><input type="button"  value="Guardar datos" name="btnguardar" id="btnguardar" disabled tabindex="12" align="middle" onClick="guardar_datos()"></td>
    <td align="right"><input  type="button" name="Limpiar información" id="Limpiar información" onClick="limpia_pantalla()" value="Limpiar informaci&oacute;n"></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
