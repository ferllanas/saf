<?php

require("../connections/dbconexion.php");
	if (version_compare(PHP_VERSION, '5.1.0', '>='))
			date_default_timezone_set('America/Mexico_City');

$infoconexion=array('UID' => $username_db,'PWD' => $password_db, 'Database' => $odbc_name);
$conexion = sqlsrv_connect($server,$infoconexion);

?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Oswald|Open+Sans:400,300,700,600|Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../css/admin.css">
<link rel="stylesheet" href="../../css/biopop.css">

<script type="text/javascript" src="ajaximage/scripts/jquery.min.js"></script>
<script type="text/javascript" src="ajaximage/scripts/jquery.form.js"></script>
<script src="javascript/prensa.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" media="all" href="../calendario/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="../calendario/calendar.js"></script>
<script type="text/javascript" src="../calendario/lang/calendar-esp.js"></script>
<script type="text/javascript" src="../calendario/calendar-setup.js"></script>
		

<script type="text/javascript" src="javascript/jquery-ui-1.8.17.custom.min.js"></script>
	<script type="text/javascript" src="javascript/jquery-ui-timepicker-addon.js"></script>
	<link type="text/css" href="javascript/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />

    <script type="text/javascript">
	$(function() {
	    $("#txtFecha").datepicker();
	});
    </script>

<!-- Load jQuery build -->

<script type="text/javascript" >
 $(document).ready(function() { 
		    $('#photoimg1').live('change', function()
			{ 
				$("#preview1").html('');
				$("#preview1").html('<img src="ajaximage/loader.gif" alt="Uploading...."/>');
				$("#imageform1").ajaxForm({
					target: '#preview1'
					}).submit();

				});
			}); 
</script>

<style>

body
{
font-family:arial;
}
.preview
{
width:800px;
border:solid 1px #dedede;
padding:10px;
}
#preview
{
color:#cc0000;
font-size:12px
}
#portapdf { 
    width: 800px; 
    height: 800px; 
    border: 1px solid #484848; 
    margin: 0 auto; 
} 

</style>

<title>Sintesis de Prensa</title>
</head>

<body style="background:#f8f8f8;">
<table width="100%">

<!--<form action="../Copia de catTipodeEntrega/guardar.php" method="post" accept-charset="latin1">-->
<table width="100%" align="left">
	<tr>
    	<td id="titulo" colspan="3">
        <div class="clr"></div>
        <div id="" class="line"></div>
        </td>
    </tr>
	<tr>
        <td colspan="5" ><h3>Síntesis de Prensa</h3></td>
     </tr>
     <tr>
      <td class="texto8" scope="row">Fecha de Publicación:
				  <input name="fecpub" type="text" size="7" id="fecpub" value="<?php echo date('Y/m/d',time());?>" class="required" maxlength="10"  style="width:70">&nbsp;<img src="../calendario/img.gif" width="16" height="16" id="f_trigger_fin" style="cursor: pointer;" title="Date selector" align="absmiddle">
											<!--{literal}-->
											<script type="text/javascript">
												Calendar.setup({
													inputField     :    "fecpub",		// id of the input field
													ifFormat       :    "%Y/%m/%d",		// format of the input field
													button         :    "f_trigger_fin",	// trigger for the calendar (button ID)
													//onClose        :    fecha_cambio,
													singleClick    :    true
												});
											</script>
				</td>
     </tr>
	 <tr>
        
        <td col id="preview-img" valign="top" align="center" style="width:80% ; height:80%" ><?php if(strlen($imagen)>0) {?>
        	<div id="portapdf">
           	 <object data="../<?php echo $imagen;?>" type="application/pdf" width="800px" height="800px"></object>
            </div>
			<?php if($id>0)
				{
					;
					?>
                        <form id="imageform1" method="post" enctype="multipart/form-data" action='ajaximage/ajaximage.php'>
                               <input type="file" name="photoimg1" id="photoimg1" />
                       </form>
                       <div id='preview1'>
						</div>
                       
            <?php
				} }else { 
					?>
                        <form id="imageform1" method="post" enctype="multipart/form-data" action='ajaximage/ajaximage.php'>
                                <input type="file" name="photoimg1" id="photoimg1" />
                       </form>
                       <div id='preview1'>
						</div>
                       
           		<?php } 
		?>
       </td>
       	<td colspan="2"><p>
       	  <input class="btn"  type="button" value="Registrar" onclick="cargasintesis()" />
       	  </p>
       	  <p>&nbsp;</p>
       	  <p>
       	    <input class="btn-suprim" type="button" value="Cancelar" onclick="javascript: <?php if($id>0) echo "location.href='prensa.php?id=$id'"; else echo"location.href='prensa.php'"?>"/>
        </p></td>
    
  </tr>
    <tr>
    <td colspan="1">&nbsp;
    </td>
    </tr>
</table>
<!--</form>-->
</body>
</html>