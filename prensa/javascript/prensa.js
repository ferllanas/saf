function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Tu navegador no es compatible. Intenta usando Mozilla Firefox, Apple Safari o Google Chrome");
    }
} 

var searchReq = getXmlHttpRequestObject(); 

function searchSuggest() {
	//alert("error");
    if (searchReq.readyState == 4 || searchReq.readyState == 0) {
        var str = escape(document.getElementById('nominst').value);
		//alert(str);
		searchReq.open("GET",'php_ajax/query.php?q=' + str ,true)//'searchSuggest.php?search=' + str, true);
        searchReq.onreadystatechange = handleSearchSuggest;
        searchReq.send(null);
    }  
	
} 

//Called when the AJAX response is returned.
function handleSearchSuggest() {
	 var ss = document.getElementById('search_suggest')
    if (searchReq.readyState == 4) 
	{
       
       if(searchReq.responseText.length<1);
		{
			document.getElementById('institucion').value = '';	
			//document.getElementById('nomsubcuenta').value = '';
		}
	   ss.innerHTML = '';
        var str = searchReq.responseText.split("\n");
		//alert(searchReq.responseText+ str.length);
		//if(str.length<31)
		if(str.length>0)
		{
		   for(i=0; i < str.length - 1 && i<30; i++) {
				
				//Build our element string.  This is cleaner using the DOM, but
				//IE doesn't support dynamically added attributes.
				var tok = str[i].split("@");
				
				var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
				suggest += 'onmouseout="javascript:suggestOut(this);" ';
				suggest += "onclick='javascript:setSearch(this.innerHTML,this.title);' ";
				suggest += 'class="suggest_link" title="'+tok[1]+'; '+tok[0]+' ">' + tok[1] +' '+ tok[0] + '</div>';

				ss.innerHTML += suggest;
			}//
			document.getElementById('search_suggest').className = 'sugerencia_Busqueda_Marco';
		}
		else
		{	
			document.getElementById('search_suggest').innerHTML = '';
			document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
		}
    }
	else
	{	
		document.getElementById('search_suggest').innerHTML = '';
		document.getElementById('search_suggest').className = 'sin_sugerencia_deBusqueda';
	}
}
//Mouse over function
function suggestOver(div_value) {
    div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
    div_value.className = 'suggest_link';
}
//Click function
function setSearch(value,idprod) {
    //document.getElementById('nomsubcuenta').value = idprod;
	var tok =idprod.split(";");	
	document.getElementById('institucion').value = tok[0];	
	document.getElementById('nominst').value = tok[1];
	//$('#Concepto').text(tok[1]);
    document.getElementById('search_suggest').innerHTML = '';
	document.getElementById('search_suggest').className='sin_sugerencia_deBusqueda';
	document.getElementById('cursos').focus();
}

var nav4 = window.Event ? true : false;
function ValidNum(evt){
// Backspace = 8, Enter = 13, ’0′ = 48, ’9′ = 57, ‘.’ = 46
var key = nav4 ? evt.which : evt.keyCode;
return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
}
function eliminadocumentos(id)
{
	//alert(id);
	var idcandidato = document.getElementById("id").value;
	location.href="eliminacandidatodocumento.php?id="+id+"&idcandidato="+idcandidato;
}

function editadocumentos()
{
	var id = document.getElementById("id").value;
	location.href="editarcandidatodocs.php?idcandidato="+id;
}
function cargasintesis()
{
	var id= $('#id').val();
	var fecha= $('#fecpub').val();
	if(fecha.length<=0)
	{
		alert("Fecha Invalida");
		return
	}
	var path= $('#photoimg1').val();
	if(path.length<=0)
	{
		alert("Seleccione un Archivo para agregar");
		return
	}
	
	var datas = {
		pid: id,
		pfecha: fecha,
		ppath: path
	}
	//alert("AAAAA");
	jQuery.ajax({
						type: 'post',
						cache:	false,
						url:'guardarsintesis.php',
						//dataType: "json",
						data: datas,
						success: function(resp)
						{
							console.log(resp);
							if(resp.error!=0)
							{
								alert(resp.error);
								if(confirm("¿Desea Capturar Otra Síntesis?"))
								{
									location.href="cargarsintesis.php";
								}
								else
								{
									if(resp.id!=0)
										location.href="prensa.php?id="+id;
								}
							}
							
						}
					
					});
}
